@name Wigglestick - Inverse Kinematics
@inputs 
@outputs 
@persist 
@trigger 

if (first()) {
    runOnTick(1)
    
    holoCreate(0)
    holoScaleUnits(0, vec(4, 4, 20))
    
}

holoAng(0, ang(cos(curtime()*400)*90, 0, 0))
AnchorPoint = entity():pos()
Pitch = holoEntity(0):angles():pitch()
holoPos(0, AnchorPoint + vec(sin(Pitch)*10, 0, cos(Pitch)*10))
#8, 400, 90, 16
