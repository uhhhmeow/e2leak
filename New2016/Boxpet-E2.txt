@name Boxpet-E2
@model models/hunter/blocks/cube05x05x05.mdl
@persist [Fl,Fr,Bl,Br]:entity [Pos,Dir,Base,Start,End,Time]:array [Arm_Length,I]:number
 
if ( first() ) {
   
    Arm_Length = 25
 
    function entity legCreate(Index_Start) {
        holoCreate(Index_Start) #1
        holoParent(Index_Start,entity())
        holoScaleUnits(Index_Start,vec(5))
        holoCreate(Index_Start+1,holoEntity(Index_Start):toWorld(vec(Arm_Length,0,0))) #2
        holoParent(Index_Start+1,Index_Start)
        holoScaleUnits(Index_Start+1,vec(5))
        holoCreate(Index_Start+2,holoEntity(Index_Start+1):toWorld(vec(Arm_Length,0,0))) #3
        holoParent(Index_Start+2,Index_Start+1)
        holoScaleUnits(Index_Start+2,vec(5))
        holoCreate(Index_Start+3,holoEntity(Index_Start):toWorld(vec(Arm_Length/2,0,0))) #4
        holoAng(Index_Start+3,holoEntity(Index_Start):toWorld(ang(90,0,0)))
        holoScaleUnits(Index_Start+3,vec(3,3,Arm_Length))
        holoParent(Index_Start+3,Index_Start)
        holoCreate(Index_Start+4,holoEntity(Index_Start+1):toWorld(vec(Arm_Length/2,0,0))) #5
        holoAng(Index_Start+4,holoEntity(Index_Start+2):toWorld(ang(90,0,0)))
        holoScaleUnits(Index_Start+4,vec(3,3,Arm_Length))
        holoParent(Index_Start+4,Index_Start+1)  
        return holoEntity(Index_Start)
    }
 
    function void drawArm(Target:vector,Base:vector,Index_Start) {
        local Hypo = clamp(Target:distance(Base),0,Arm_Length*2)
        local A_Ang = -acos((Arm_Length^2 + Arm_Length^2 - Hypo^2)/(2*Arm_Length^2))
        local B_Ang = -acos((Arm_Length^2 + Hypo^2 - Arm_Length^2)/(2*Arm_Length*Hypo))
        holoAng(Index_Start,(Target-Base):toAngle()+ang(180-A_Ang-B_Ang,0,0))
        holoAng(Index_Start+1,holoEntity(Index_Start):toWorld(ang(A_Ang+180,0,0)))
    }
   
    runOnTick(1)
    rangerFilter(players())
    rangerFilter(entity())
    rangerFilter(entity():getConstraints())
    rangerPersist(1)
    #seatSpawn("Seat_Jeep",1)
    entity():setMass(50)
   
    Dir = array(vec(1,1,0),vec(1,-1,0),vec(-1,1,0),vec(-1,-1,0))
    Base = array(vec(12,12,-12),vec(12,-12,-12),vec(-12,12,-12),vec(-12))
       
    Fl = legCreate(1)
        holoPos(1,entity():toWorld(Base[1,vector]))
        holoColor(1,vec(0,255,0))
    Fr = legCreate(6)
        holoPos(6,entity():toWorld(Base[2,vector]))
    Bl = legCreate(11)
        holoPos(11,entity():toWorld(Base[3,vector]))
    Br = legCreate(16)
        holoPos(16,entity():toWorld(Base[4,vector]))
        holoColor(16,vec(255,0,0))
   
   
   
    for ( I = 1 , 4 ) { Pos[I,vector] = rangerOffset(60,entity():toWorld(Dir[I,vector]*30),vec(0,0,-1)):pos() }
   
    drawArm(Pos[1,vector],Fl:pos(),1)
    drawArm(Pos[2,vector],Fr:pos(),6)
    drawArm(Pos[3,vector],Bl:pos(),11)
    drawArm(Pos[4,vector],Br:pos(),16)
   
    timer("Animate",100)
}
elseif( clk("Animate") ) {
    I+=1
    if ( I > 4 ) { I = 1 }
    R = rangerOffset(55,entity():toWorld(Dir[I,vector]*30),-entity():up())
   
    if ( R:hit() & R:pos():distance(Pos[I,vector]) > 5 ) { Pos[I,vector] = R:pos()+entity():vel()/8 }
    elseif( !R:hit() ) { Pos[I,vector] = entity():toWorld(Dir[I,vector]*25+vec(0,0,-10)) }
   
    drawArm(Pos[1,vector],Fl:pos(),1)
    drawArm(Pos[2,vector],Fr:pos(),6)
    drawArm(Pos[3,vector],Bl:pos(),11)
    drawArm(Pos[4,vector],Br:pos(),16)
   
    timer("Animate",75)
}  
 
if ( rangerOffsetHull(40,entity():boxCenterW(),-entity():up(),vec(12,12,1)):hit() ) {
    Avg = (Pos[1,vector]+Pos[2,vector]+Pos[3,vector]+Pos[4,vector])/4
    Ang = clamp((Avg - entity():boxCenterW()):toAngle()+ang(-90,0,0),ang(-45,-360,-45),ang(45,360,45))
   
    V = Avg+vec(0,0,30) - entity():pos()
    Dist = entity():pos():distance(owner():pos())
    if ( Dist > 130 ) { V+=(owner():pos()-entity():pos()):normalized():setZ(0)*45 }
    applyForce((V*3-entity():vel())*entity():mass())
    entity():applyTorque((entity():toLocal(rotationVector(quat(Ang:setYaw((owner():boxCenterW()-entity():pos()):toAngle():yaw()))/quat(entity()))+entity():pos())*100 - entity():angVelVector()*25)*entity():inertia())
}
