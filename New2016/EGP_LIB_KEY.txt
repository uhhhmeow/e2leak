# EGP Manip
# By: Hell Fire 6/24/2014
 
#[
 -10/3/2014-
Added - egpGrid
]#
function number wirelink:egpClick(Player:entity,ID)
{
    local Pos=This:egpPos(ID)
    local Half=This:egpSize(ID)/2
    return inrange(This:egpCursor(Player),Pos-Half,Pos+Half)
}
 
function void wirelink:egpGrid(Divisions,Scale,Pos:vector2)
{
    Scale = 1/Scale * 100
    Grid_size = 500/Divisions
    for(I=0,Divisions)
    {
        # EGP index 500+ dedicated to grid rendering
        This:egpLine(I+100,vec2((Grid_size*I/Scale)+5,512/Scale)+Pos,vec2((Grid_size*I/Scale)+5,1/512 * Scale)+Pos), This:egpColor(I+100, vec(30,30,30))
        This:egpLine(I+200,vec2(512/Scale,(Grid_size*I/Scale)+5)+Pos,vec2(0,(Grid_size*I/Scale)+5)+Pos), This:egpColor(I+200, vec(30,30,30))
    }
}
 
function wirelink:buildKeyboard(Pos:vector2)
{
 
 
    This:egpBox(65,vec2(256,300+100)+Pos,vec2(500,200))
    This:egpColor(65,vec(0,0,0))
    This:egpAlpha(65,100)
    for(I=1,10) {
        This:egpBox(I+27,vec2(0+(I*45.9),260+100)+Pos,vec2(40,30))
        This:egpColor(I+27,vec(35,35,35))
    }
   
    for(I=1,9) {
        This:egpBox(I+37,vec2(10+(I*45.75),300+100)+Pos,vec2(40,30))
        This:egpColor(I+37,vec(35,35,35))
    }
   
    for(I=1,7) {
        This:egpBox(I+46,vec2(40+(I*45.75),340+100)+Pos,vec2(40,30))
        This:egpColor(I+46,vec(35,35,35))
    }
    This:egpBox(54,vec2(270,380+100)+Pos,vec2(250,30))
    This:egpColor(54,vec(35,35,35))
    This:egpBox(59,vec2(420,340+100)+Pos,vec2(67,30))
    This:egpColor(59,vec(35,35,35))
   
 
    This:egpText(61,"Q         W         E         R         T         Y         U         I         O         P",vec2(37,350)+Pos)
    This:egpText(62,"A         S         D         F         G         H         J         K         L",vec2(50,390)+Pos)
    This:egpText(63,"Z         X         C         V         B         N        M",vec2(80,430)+Pos)
    This:egpText(64,"Space",vec2(256,470)+Pos)
    This:egpBox(56,vec2(435,220+100)+Pos,vec2(95,30))
    This:egpColor(56,vec(35,35,35))
    This:egpText(57,"Done",vec2(415,310)+Pos)
    This:egpColor(57,vec(165,126,71))
    This:egpText(58,"",vec2(30,260)+Pos)
    This:egpColor(58,vec(165,126,71))
    This:egpText(60,"Del",vec2(410,430)+Pos)
   
}
 
   
function string string:backspace()
{
    return This:sub(1,This:length()-1)
}
 
function string wirelink:keyboardInput(User:entity)
{
    local Letters = array("q","w","e","r","t","y","u","i","o","p","a","s","d","f","g","h","j","k","l","z","x","c","v","b","n","m")
    Cursor = This:egpCursor(User)
 
    # Keys
    if(Cursor:y()<370&Cursor:y()>340&Cursor:x()<490) # row 1
    {
        return Letters:string(round((Cursor:x()-10)/45))
    }
    elseif(Cursor:y()<410&Cursor:y()>380&Cursor:x()<440&Cursor:x()>40) # row 2
    {
        return Letters:string(round((Cursor:x()-15)/45)+10)
    }
    elseif(Cursor:y()<450&Cursor:y()>420)
    {
        return Letters:string(round((Cursor:x()-28)/48)+19)
    }
    # Delete key [
    if(This:egpClick(User,59))
    {
        return "[Backspace]"
    }
    if(This:egpClick(User,54))
    {
        return " "
    }
   
    # Close Keyboard
    if(This:egpClick(User,56))
    {
        #removeKeyboard()
        return "[Done]"
    }
   
}
