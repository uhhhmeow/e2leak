@name MediumMech V4
@inputs Cam:wirelink
@outputs 
@persist [E Seat None]:entity [Pos1 Pos2 Pos3 Pos4 R1 R2 R3 R4 T1 T2 T3 T4]:vector [ ]:angle Parent M1 M2 M3 M4  LR FB  Height MouthOpen
@trigger none
interval(70)
runOnLast(1)
if(last()){
E:propDelete()   
}
if(!E){
E = propSpawn("models/hunter/blocks/cube05x05x05.mdl",entity():pos()+vec(0,0,50),1)
E:setMass(50000)
}
if(!Seat){
     findByClass("prop_vehicle_prisoner_pod")
     findSortByDistance(E:pos()) 
if(find():owner()==owner()){
 Seat = find()  
}   
}

if(Seat){
  Driver = Seat:driver()
W = Driver:keyForward()
A = Driver:keyLeft()
S = Driver:keyBack()
D = Driver:keyRight()
Space = Driver:keyJump()
Shift = Driver:keySprint()  
Alt = Driver:keyWalk()
M1A = Driver:keyAttack1()
M2A = Driver:keyAttack2()
RRR = Driver:keyReload()
} 
if(Seat&Parent == 0){
    Seat:propFreeze(1)
Seat:setPos(E:toWorld(vec(20,0,10)))
Seat:setAng(E:toWorld(ang(0,-90,0)))
timer("parent",600)
}
if(clk("parent")){
 Seat:parentTo(E)   
 Parent = 1
}
if(!Seat){
Parent = 0
}


local StepC = 10
local RightLeft = 30
if(!Shift){
 Step  = 80
}
if(Shift&!(A|D)){
Step = 130
}
local Up = 60
if(!Shift){
 SpeedStep =4
}else{
SpeedStep = 9
}
if(!Shift){
 Time = 300
}
if(Shift){
Time = 220
}
if(first()|duped()){
    T1 = E:toWorld(vec(40,RightLeft,0))   
    T2 = E:toWorld(vec(40,-RightLeft,0))
        T3 = E:toWorld(vec(-60,RightLeft,0))   
    T4 = E:toWorld(vec(-60,-RightLeft,0))
       Pos1 = E:pos()
Pos2 = E:pos()
  Pos3 = E:pos()
 Pos4 = E:pos()
}
if(changed(E)){
    function entity holop(HT,Vectos:vector){
 holoPos(HT,Vectos)   
}
     function entity holoa(Ho,Angl:angle){
 holoAng(Ho,Angl)   
}
 timer("1",0)
            function entity c(Index:number,Posi:vector,Scale:vector,Angle:angle,Colo:vector,Model:string,Parent:entity,Alpha:number){
 holoCreate(Index) holoPos(Index,Posi) holoScale(Index,Scale) holoAng(Index,Angle) holoColor(Index,Colo) holoModel(Index,Model) 
holoParent(Index,Parent) holoAlpha(Index,Alpha)
return holoEntity(Index)  
}
function vector entity:applyf(Vector1:vector){
 This:applyForce(Vector1)   
}
function angle entity:applya(Angle1:angle){
 This:applyAngForce(Angle1)   
}
function number co(A, B, C){
return acos((A^2 + B^2 - C^2) / (2*A*B))
}
function ik(L1,L2,Hip:vector,End:vector,I1,I2,Bs:entity){
local Axis=Bs:toLocalAxis(End-Hip)
local Angle=Axis:toAngle():setRoll(-bearing(Hip,Bs:angles(),End))
local Dist=min(Axis:length(),L1+L2)
local Quat=quat(Angle)*qRotation(vec(0,90+co(Dist,L1,L2),0))
holoa(I1,Bs:toWorld(Quat:toAngle()))
holoa(I2,holoEntity(I1):toWorld(ang(co(L2, L1, Dist)+180,0,0)))
}
   
c(1,E:toWorld(vec(40,RightLeft,0)),vec(1),E:toWorld(ang()),vec(255),"",None,0)
c(2,E:toWorld(vec(40,-RightLeft,0)),vec(1),E:toWorld(ang()),vec(255),"",None,0)

c(3,E:toWorld(vec(-60,RightLeft,0)),vec(1),E:toWorld(ang()),vec(255),"",None,0)
c(4,E:toWorld(vec(-60,-RightLeft,0)),vec(1),E:toWorld(ang()),vec(255),"",None,0)

c(5,E:toWorld(vec(40,RightLeft,0)),vec(1),E:toWorld(ang()),vec(255),"",E,0)
c(6,E:toWorld(vec(40,-RightLeft,0)),vec(1),E:toWorld(ang()),vec(255),"",E,0)
c(7,E:toWorld(vec(-70,RightLeft,0)),vec(1),E:toWorld(ang()),vec(255),"",E,0)
c(8,E:toWorld(vec(-70,-RightLeft,0)),vec(1),E:toWorld(ang()),vec(255),"",E,0)

c(9,holoEntity(5):toWorld(vec(0,0,50)),vec(1),E:toWorld(ang()),vec(255),"",holoEntity(5),0)
c(10,holoEntity(6):toWorld(vec(0,-0,50)),vec(1),E:toWorld(ang()),vec(255),"",holoEntity(6),0)
c(11,holoEntity(7):toWorld(vec(0,0,50)),vec(1),E:toWorld(ang()),vec(255),"",holoEntity(7),0)
c(12,holoEntity(8):toWorld(vec(0,-0,50)),vec(1),E:toWorld(ang()),vec(255),"",holoEntity(8),0)
14
c(13,holoEntity(9):toWorld(vec(8,0,50)),vec(1.4,2.5,1.2),E:toWorld(ang()),vec(255),"models/props_combine/combine_emitter01.mdl",holoEntity(9),255)
c(14,holoEntity(10):toWorld(vec(8,-0,50)),vec(1.4,2.5,1.2),E:toWorld(ang()),vec(255),"models/props_combine/combine_emitter01.mdl",holoEntity(10),255)
c(15,holoEntity(11):toWorld(vec(0,0,50)),vec(1),E:toWorld(ang()),vec(255),"0",holoEntity(11),0)
c(16,holoEntity(12):toWorld(vec(0,-0,50)),vec(1),E:toWorld(ang()),vec(255),"0",holoEntity(12),0)

c(40000,holoEntity(15):toWorld(vec(5,0,2)),vec(1.2,2.5,1.2),E:toWorld(ang()),vec(255),"models/props_combine/combine_emitter01.mdl",holoEntity(15),255)
c(30000,holoEntity(16):toWorld(vec(5,-0,2)),vec(1.2,2.5,1.2),E:toWorld(ang()),vec(255),"models/props_combine/combine_emitter01.mdl",holoEntity(16),255)

c(400,holoEntity(40000):toWorld(vec(-25,0,25)),vec(2.5),E:toWorld(ang(-90,90,0)),vec(255),"models/items/combine_rifle_ammo01.mdl",holoEntity(40000),0)
c(300,holoEntity(30000):toWorld(vec(-25,0,25)),vec(2.5),E:toWorld(ang(90,90,0)),vec(255),"models/items/combine_rifle_ammo01.mdl",holoEntity(30000),0)

c(4001,holoEntity(40000):toWorld(vec(-5,-15,5)),vec(2.5),E:toWorld(ang(90,90,0)),vec(255),"models/items/combine_rifle_ammo01.mdl",holoEntity(40000),0)
c(3001,holoEntity(30000):toWorld(vec(-5,15,5)),vec(2.5),E:toWorld(ang(-90,90,0)),vec(255),"models/items/combine_rifle_ammo01.mdl",holoEntity(30000),0)

c(1000,E:toWorld(vec(40,RightLeft,0)),vec(1),E:toWorld(ang()),vec(255),"",E,0)
c(2000,E:toWorld(vec(40,-RightLeft,0)),vec(1),E:toWorld(ang()),vec(255),"",E,0)
c(3000,E:toWorld(vec(-60,RightLeft,0)),vec(1),E:toWorld(ang()),vec(255),"",E,0)
c(4000,E:toWorld(vec(-60,-RightLeft,0)),vec(1),E:toWorld(ang()),vec(255),"",E,0)
#model#
#hips#
c(17,holoEntity(5):toWorld(vec(0,-15,50)),vec(2.5),E:toWorld(ang(90,90,0)),vec(255),"models/items/combine_rifle_ammo01.mdl",holoEntity(5),255)
c(18,holoEntity(6):toWorld(vec(0,15,50)),vec(2.5),E:toWorld(ang(-90,90,0)),vec(255),"models/items/combine_rifle_ammo01.mdl",holoEntity(6),255)
c(19,holoEntity(7):toWorld(vec(0,-15,50)),vec(2.5),E:toWorld(ang(90,90,0)),vec(255),"models/items/combine_rifle_ammo01.mdl",holoEntity(7),255)
c(20,holoEntity(8):toWorld(vec(0,15,50)),vec(2.5),E:toWorld(ang(-90,90,0)),vec(255),"models/items/combine_rifle_ammo01.mdl",holoEntity(8),255)

c(21,E:toWorld(vec(40,-15+RightLeft,0)),vec(2.5),E:toWorld(ang(90,90,0)),vec(255),"models/items/combine_rifle_ammo01.mdl",holoEntity(5),255)
c(22,E:toWorld(vec(40,15-RightLeft,0)),vec(2.5),E:toWorld(ang(-90,90,0)),vec(255),"models/items/combine_rifle_ammo01.mdl",holoEntity(6),255)
c(23,E:toWorld(vec(-70,-15+RightLeft,0)),vec(2.5),E:toWorld(ang(90,90,0)),vec(255),"models/items/combine_rifle_ammo01.mdl",holoEntity(7),255)
c(24,E:toWorld(vec(-70,15-RightLeft,0)),vec(2.5),E:toWorld(ang(-90,90,0)),vec(255),"models/items/combine_rifle_ammo01.mdl",holoEntity(8),255)
######
c(25,holoEntity(5):toWorld(vec(0,-0,45)),vec(0.7,0.9,0.6),E:toWorld(ang(180,0,0)),vec(255),"models/props_combine/breenpod.mdl",holoEntity(5),255)
c(26,holoEntity(6):toWorld(vec(0,0,45)),vec(0.7,0.9,0.6),E:toWorld(ang(180,0,0)),vec(255),"models/props_combine/breenpod.mdl",holoEntity(6),255)
c(27,holoEntity(7):toWorld(vec(-10,-0,20)),vec(0.08,0.20,0.09),E:toWorld(ang(90,0,0)),vec(255),"models/props_combine/CombineTrain01a.mdl",holoEntity(7),255)
c(28,holoEntity(8):toWorld(vec(-10,0,20)),vec(0.08,0.20,0.09),E:toWorld(ang(90,0,0)),vec(255),"models/props_combine/CombineTrain01a.mdl",holoEntity(8),255)

c(29,holoEntity(9):toWorld(vec(-0,-0,-0)),vec(1,3,1),E:toWorld(ang(0,0,0)),vec(255),"models/props_combine/combine_light002a.mdl",holoEntity(9),255)
c(30,holoEntity(10):toWorld(vec(-0,0,-0)),vec(1,3,1),E:toWorld(ang(0,0,0)),vec(255),"models/props_combine/combine_light002a.mdl",holoEntity(10),255)
c(31,holoEntity(11):toWorld(vec(0,-0,0)),vec(1,3,0.8),E:toWorld(ang(0,0,0)),vec(255),"models/props_combine/combine_light002a.mdl",holoEntity(11),255)
c(32,holoEntity(12):toWorld(vec(0,0,0)),vec(1,3,0.8),E:toWorld(ang(0,0,0)),vec(255),"models/props_combine/combine_light002a.mdl",holoEntity(12),255)

c(33,holoEntity(11):toWorld(vec(-0,0,25)),vec(3),E:toWorld(ang(0,0,0)),vec(255),"models/items/combine_rifle_ammo01.mdl",holoEntity(11),255)
c(34,holoEntity(12):toWorld(vec(-0,-0,25)),vec(3),E:toWorld(ang(0,0,0)),vec(255),"models/items/combine_rifle_ammo01.mdl",holoEntity(12),255)
######
#body#

c(35,E:toWorld(vec(30,-0,0)),vec(0.8),E:toWorld(ang(0,0,0)),vec(55),"models/xqm/jetenginemedium.mdl",E,255)

c(36,E:toWorld(vec(-20,10,-9)),vec(0.2,0.4,0.2),E:toWorld(ang(70,90,0)),vec(255),"models/props_combine/combine_barricade_med01a.mdl",E,255)
c(37,E:toWorld(vec(-40,10,-6)),vec(0.2,0.4,0.2),E:toWorld(ang(70,90,0)),vec(255),"models/props_combine/combine_barricade_med01a.mdl",E,255)
c(38,E:toWorld(vec(-60,10,-3)),vec(0.2,0.4,0.2),E:toWorld(ang(70,90,0)),vec(255),"models/props_combine/combine_barricade_med01a.mdl",E,255)

c(39,E:toWorld(vec(-20,-10,-9)),vec(0.2,0.4,0.2),E:toWorld(ang(70,-90,0)),vec(255),"models/props_combine/combine_barricade_med01a.mdl",E,255)
c(40,E:toWorld(vec(-40,-10,-6)),vec(0.2,0.4,0.2),E:toWorld(ang(70,-90,0)),vec(255),"models/props_combine/combine_barricade_med01a.mdl",E,255)
c(41,E:toWorld(vec(-60,-10,-3)),vec(0.2,0.4,0.2),E:toWorld(ang(70,-90,0)),vec(255),"models/props_combine/combine_barricade_med01a.mdl",E,255)

c(42,E:toWorld(vec(-50,-0,-7)),vec(0.2,0.4,0.8),E:toWorld(ang(100,0,0)),vec(255),"models/props_combine/combine_barricade_med01a.mdl",E,255)
c(43,E:toWorld(vec(-0,-0,-0)),vec(4.2,4.4,6.8),E:toWorld(ang(90,180,0)),vec(255),"models/items/combine_rifle_ammo01.mdl",E,255)

c(44,E:toWorld(vec(20,-0,10)),vec(0.4,0.4,0.7),E:toWorld(ang(-92,0,0)),vec(255),"models/props_combine/combine_barricade_med02b.mdl",E,255)

c(45,E:toWorld(vec(10,-30,10)),vec(0.4,0.4,0.7),E:toWorld(ang(0,-90,0)),vec(255),"models/props_combine/combine_barricade_short01a.mdl",E,255)
c(46,E:toWorld(vec(10,30,10)),vec(0.4,0.4,0.7),E:toWorld(ang(0,90,0)),vec(255),"models/props_combine/combine_barricade_short01a.mdl",E,255)
c(47,E:toWorld(vec(0,-28,8)),vec(0.4,0.4,0.6),E:toWorld(ang(0,-90,0)),vec(255),"models/props_combine/combine_barricade_short01a.mdl",E,255)
c(48,E:toWorld(vec(0,28,8)),vec(0.4,0.4,0.6),E:toWorld(ang(0,90,0)),vec(255),"models/props_combine/combine_barricade_short01a.mdl",E,255)
c(49,E:toWorld(vec(-20,-26,6)),vec(0.4,0.4,0.5),E:toWorld(ang(0,-90,0)),vec(255),"models/props_combine/combine_barricade_short01a.mdl",E,255)
c(50,E:toWorld(vec(-20,26,6)),vec(0.4,0.4,0.5),E:toWorld(ang(0,90,0)),vec(255),"models/props_combine/combine_barricade_short01a.mdl",E,255)
c(51,E:toWorld(vec(-40,-26,6)),vec(0.4,0.8,0.3),E:toWorld(ang(0,-90,0)),vec(255),"models/props_combine/combine_barricade_short01a.mdl",E,255)
c(52,E:toWorld(vec(-40,26,6)),vec(0.4,0.8,0.3),E:toWorld(ang(0,90,0)),vec(255),"models/props_combine/combine_barricade_short01a.mdl",E,255)

c(53,E:toWorld(vec(10,0,-20)),vec(0.2,0.3,0.3),E:toWorld(ang(90,0,0)),vec(255),"models/props_combine/combine_barricade_med02a.mdl",E,255)

c(54,E:toWorld(vec(50,0,15)),vec(0.8),E:toWorld(ang(90,90,0)),vec(255),"models/combine_helicopter/helicopter_bomb01.mdl",E,255)

c(55,E:toWorld(vec(35,-6,-15)),vec(2),E:toWorld(ang(40,-30,0)),vec(255),"models/items/combine_rifle_ammo01.mdl",E,255)
c(56,E:toWorld(vec(35,6,-15)),vec(2),E:toWorld(ang(40,30,0)),vec(255),"models/items/combine_rifle_ammo01.mdl",E,255)
c(57,E:toWorld(vec(35,0,-15)),vec(2),E:toWorld(ang(50,0,0)),vec(255),"models/items/combine_rifle_ammo01.mdl",E,255)

c(58,holoEntity(54):toWorld(vec(8,0,5)),vec(0.2),E:toWorld(ang(90,90,0)),vec(255),"models/props_combine/combine_barricade_med02a.mdl",holoEntity(54),255)
c(59,E:toWorld(vec(50,-10,20)),vec(0.5),E:toWorld(ang(90,90,0)),vec(255),"0",holoEntity(54),255)
c(60,holoEntity(54):toWorld(vec(-3,0,-15)),vec(1,1,1),E:toWorld(ang(90,90,0)),vec(255),"models/props_combine/breenlight.mdl",holoEntity(54),255)

######
}
ik(50,50,holoEntity(5):pos(),holoEntity(1):pos(),5,9,E)
ik(50,50,holoEntity(6):pos(),holoEntity(2):pos(),6,10,E)
ik(50,50,holoEntity(7):pos(),holoEntity(4):pos(),7,11,E)
ik(50,50,holoEntity(8):pos(),holoEntity(3):pos(),8,12,E)


if(RRR){
 if(MouthOpen < 80){
MouthOpen = MouthOpen * 1 + 4
}   
if(MouthOpen<40){
holoa(59,holoEntity(54):toWorld(ang(0+MouthOpen,0,0)))
}

}
if(!RRR&MouthOpen > 1){
    holoa(59,holoEntity(54):toWorld(ang(0+MouthOpen,0,0)))

MouthOpen = MouthOpen * 1 -4
}
HeadAngle = clamp(E:toLocal(Driver:eyeAngles()),ang(-45,-55,-180),ang(45,55,180))
    holoa(54, E:toWorld(HeadAngle) + ang(-90,0,0))

holoa(40000,ang(0,E:angles():yaw(),0))
holoa(30000,ang(0,E:angles():yaw(),0))
holoa(13,ang(0,E:angles():yaw(),0))
holoa(14,ang(0,E:angles():yaw(),0))


      rangerFilter(E)
 R1 = rangerOffset(90,E:toWorld(vec(60+(W-S)*Step,RightLeft+(A-D)*Step,0)),vec(0,0,-1)):position() 

rangerFilter(E)
 R2 = rangerOffset(90,E:toWorld(vec(60+(W-S)*Step,-RightLeft+(A-D)*Step,0)),vec(0,0,-1)):position()  

        rangerFilter(E)
 R3 = rangerOffset(90,E:toWorld(vec(-60+(W-S)*Step,-RightLeft+(A-D)*Step,0)),vec(0,0,-1)):position() 

rangerFilter(E)
R4 = rangerOffset(90,E:toWorld(vec(-60+(W-S)*Step,RightLeft+(A-D)*Step,0)),vec(0,0,-1)):position()  


Pos1 += (tickInterval()*SpeedStep) * clamp(T1 - Pos1,-25,50) * StepC 
Pos2 += (tickInterval()*SpeedStep) * clamp(T2 - Pos2,-25,50) * StepC 
Pos3 += (tickInterval()*SpeedStep) * clamp(T3 - Pos3,-25,50) * StepC 
Pos4 += (tickInterval()*SpeedStep) * clamp(T4 - Pos4,-25,50) * StepC 
holop(1000,E:toWorld(vec(60+(W-S)*Step,RightLeft+(A-D)*Step,0)))
holop(2000,E:toWorld(vec(60+(W-S)*Step,-RightLeft+(A-D)*Step,0)))
holop(3000,E:toWorld(vec(-60+(W-S)*Step,-RightLeft+(A-D)*Step,0)))
holop(4000,E:toWorld(vec(-60+(W-S)*Step,RightLeft+(A-D)*Step,0)))
if(holoEntity(1):pos():distance(R1)>7){
holop(1,Pos1 + E:up() * -sin(T1:distance(Pos1) / M1 * 180 - 180) * clamp(StepC,0,Up))
}
if(holoEntity(2):pos():distance(R2)>7){
holop(2,Pos2 + E:up() * -sin(T2:distance(Pos2) / M2 * 180 - 180) * clamp(StepC,0,Up))
}
if(holoEntity(3):pos():distance(R3)>7){
holop(3,Pos3 + E:up() * -sin(T3:distance(Pos3) / M3 * 180 - 180) * clamp(StepC,0,Up))
}
if(holoEntity(4):pos():distance(R4)>7){
holop(4,Pos4 + E:up() * -sin(T4:distance(Pos4) / M4 * 180 - 180) * clamp(StepC,0,Up))
}
if(!Shift){
if(clk("1")){
    T1 = E:toWorld(vec(60+(W-S)*Step,RightLeft+(A-D)*Step,-(holoEntity(1000):pos():distance(R1) - 5)))   
    T3 = E:toWorld(vec(-60+(W-S)*Step+(-Step/2*Shift),-RightLeft+(A-D)*Step,-(holoEntity(3000):pos():distance(R3) - 5)))
    M1 = T1:distance(Pos1)
    M3 = T3:distance(Pos3)
    StepC = clamp((M1 + M3) / 6,0,50)
    timer("2",Time)
}
if(clk("2")){
    T2 = E:toWorld(vec(60+(W-S)*Step ,-RightLeft+(A-D)*Step,-(holoEntity(2000):pos():distance(R2) - 5)))   
    T4 = E:toWorld(vec(-60+(W-S)*Step+(-Step/2*Shift) ,RightLeft+(A-D)*Step,-(holoEntity(4000):pos():distance(R4) - 5)))
    M2 = T2:distance(Pos2)
    M4 = T4:distance(Pos4)
    StepC = clamp((M2 + M4) / 6,0,50)
    timer("1",Time)
}
}else{

if(clk("1")){
    T1 = E:toWorld(vec(60+(W-S)*Step,RightLeft+(A-D)*Step,-(holoEntity(1000):pos():distance(R1) - 5)))   
    T2 = E:toWorld(vec(60+(W-S)*Step,-RightLeft+(A-D)*Step,-(holoEntity(2000):pos():distance(R2) - 5)))
    M1 = T1:distance(Pos1)
    M2 = T3:distance(Pos2)
    StepC = clamp((M1 + M2) / 6,0,50)
    timer("2",Time)
}
if(clk("2")){
        T3 = E:toWorld(vec(-60+(W-S)*Step+(-Step/2*Shift) ,-RightLeft+(A-D)*Step,-(holoEntity(3000):pos():distance(R3) - 5)))   
    T4 = E:toWorld(vec(-60+(W-S)*Step+(-Step/2*Shift) ,RightLeft+(A-D)*Step,-(holoEntity(4000):pos():distance(R4) - 5)))
    M3 = T3:distance(Pos3)
    M4 = T4:distance(Pos4)
    StepC = clamp((M3 + M4) / 6,0,50)
    timer("1",Time)
}
}

if(Shift){
 MAL = 6 
}else{
MAL= 4
}
local Vector = (holoEntity(1):pos()+holoEntity(2):pos()+holoEntity(3):pos()+holoEntity(4):pos())/4

local Vect = (((Vector + vec(0,0,Height)) - (E:pos()-vec(0,0,0)))*MAL )*E:mass()
local Vel =-E:vel()*E:mass()*1
local Y = vec(Driver:eye():dot(E:right()),Driver:eye():dot(E:right()),0):y()
 
    if(Space){
     JumpAng = 15   
    }
    if(Alt){
     Height = 40   
    }else{
 Height = 60   
}
E:applya(((ang(JumpAng,E:angles():yaw()-Y*30,0)-E:angles())*E:mass()*5)-E:angVel()*E:mass()*2)
if(!Shift){
E:applyf(Vect*2 + Vel)
}else{
E:applyf(E:forward()*E:mass()*500*(W-S)*!(A|D)+Vel+Vect)
}

