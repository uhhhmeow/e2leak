@name flaps
@inputs 
@outputs 
@persist 
@trigger 

E=entity()
E:setAlpha(255)

Angle = ang(45, 45, 0)
Offset = vec(10, 0, 5)
Amount = 12
Step = 360 / Amount

E2Pos = entity():pos()
E2Ang = entity():angles()

Holo = 0

for(I=0, 360, Step){    
    TwistedAng = entity():toWorld(ang(0, I, 0))
    NewAng = toWorldAng(Offset, Angle, E2Pos, TwistedAng)
    NewPos = toWorld(Offset, Angle, E2Pos, TwistedAng)
    
    print(NewPos)
    
    H = holoCreate(Holo)
    holoPos(Holo, NewPos)
    holoAng(Holo, NewAng)
    holoParent(Holo, E)
    holoDisableShading(Holo, 0)
    holoShadow(Holo, 0)
    holoMaterial(Holo, "models/debug/debugwhite")
    holoScale(Holo, vec(0.1))
    Holo++
}
