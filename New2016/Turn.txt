@persist O:entity

if (first()) {
    runOnTick(1)
    holoCreate(1)
    holoModel(1, "hqicosphere2")
    holoScale(1, vec(1, 1, 7)
} elseif (findCanQuery()) {
    O = findPlayerByName("War")
}

holoPos(1, O:shootPos())
