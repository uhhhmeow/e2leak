@name Bull This Is A BUTTON!
@inputs Egp:wirelink EgpUser:entity
@trigger EgpUser
if(first()){
function number egpClick(Player:entity,ID){
        local Pos=Egp:egpPos(ID)
        local Half=Egp:egpSize(ID)/2
        return inrange(Egp:egpCursor(Player),Pos-Half,Pos+Half)
    }
}
 
if(changed(Egp:entity())&Egp:entity()){
    Egp:egpClear()
    Egp:egpBox(3,vec2(256,256),vec2(100,50))
}
 
if(egpClick(EgpUser,3)){
    Egp:egpColor(3,vec(255,0,0))
}
