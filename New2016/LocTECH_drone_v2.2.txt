@name LocTECH_drone_v2.2
@persist Dest:vector
@persist Players:array Gunz:array Offsets:array
@persist [DodgeBox AvoidPos]:table G:gtable
@persist Drone:entity Target:entity
@persist AvoidBox:ranger
@persist Speed OrbitRadius Offset Fire
@model models/Spacebuild/milcock7_emount1.mdl
 
if( first() | dupefinished()) {
   
    GROUP = "LocTECH"
    dsJoinGroup(GROUP)
    dsSend("FIGHTER_JOINED", GROUP, entity())
   
    G = gTable(GROUP,0)
   
    for(I=1,9) {
        if (!G[I,number]) {
            G[I,number] = 1 #set to true, for "this spot is taken"
            Offset = I * 100
             break
 
        }
    }
   
    runOnChat(1)
    runOnLast(1)
    Drone=entity()
    Drone:propGravity(0)
    Speed=80
   
    Offsets[G[1,number], number] = 100
    Offsets[G[2,number], number] = 200
    Offsets[G[3,number], number] = 300
    Offsets[G[4,number], number] = 400
    Offsets[G[5,number], number] = 500
    Offsets[G[6,number], number] = 600
    Offsets[G[7,number], number] = 700
    Offsets[G[8,number], number] = 800
    Offsets[G[9,number], number] = 900
 
 
   
#Find only owner gunz.
   
    for(I=1,Players:count()) {
        findExcludePlayerProps(Players[I,entity])
    }
   
    findAllowPlayerProps(owner())
    findIncludeClass("sc_weapon_base")
    findInSphere(Drone:pos(),100)
    findSortByDistance(Drone:pos())
    Gunz=findToArray()
   
    if (!(Gunz)) {
        print("Unable to find a gun close enough!")
       
#Lets wire those gunz
    }
    if(Gunz){
        for(I=1,Gunz:count()) {
            Gunz[I, entity]:wirelink()
            Gun=Gunz[I, wirelink]
            print(Gunz[I, entity]:toString()+" link has been made")
        }
    }
}
#See if the drone is activated
Active=gGetNum("Active")
#What happens if its active
if(Active){
    timer("boxCheck",100)
    timer("unFreeze&RadiusCheck",2000)
#Lets request stuff from rose
 
    Target=gGetEnt("Target")
    Fire=gGetNum("Fire")
 
 
    if(clk("boxCheck")){
#Filtering rangers
    rangerFilter(Drone)
    rangerFilter(Drone:getConstraints())
    rangerHitWater(1)
    rangerDefaultZero(0)
 
#We need a box to avoid
    AvoidBox = rangerOffsetHull(Drone:toWorld(vec(-Drone:boxSize():x()*10, 0, 0)),
    Drone:toWorld(vec(Drone:boxSize():x()*10, 0, 0)), Drone:boxSize()*1.8)
    }
   
#1st target is always chosen    
    Dest=Target:boxCenterW()
#What happens when we hit it
    if(AvoidBox:hit()){
       
        timer("weHitSomething",200)
    }
 
    if(clk("weHitSomething")){
 
#where did the hit come from?
        DodgeBox["UP", ranger] = rangerOffset(750,entity():pos(), Drone:up())
        DodgeBox["DOWN", ranger] = rangerOffset(750,entity():pos(), -Drone:up())
        DodgeBox["FORWARD", ranger] = rangerOffset(750,entity():pos(), Drone:forward())
        DodgeBox["FORWARDRIGHT", ranger] = rangerOffset(750,entity():pos(), Drone:forward()+Drone:right())
        DodgeBox["FORWARDLEFT", ranger] = rangerOffset(750,entity():pos(), Drone:forward()-Drone:right())
        DodgeBox["BACK", ranger] = rangerOffset(750,entity():pos(), -Drone:forward())
        DodgeBox["BACKRIGHT", ranger] = rangerOffset(750,entity():pos(), -Drone:forward()+Drone:right())
        DodgeBox["BACKLEFT", ranger] = rangerOffset(750,entity():pos(), -Drone:forward()-Drone:right())
        DodgeBox["RIGHT", ranger] = rangerOffset(750,entity():pos(), Drone:right())
        DodgeBox["LEFT", ranger] = rangerOffset(750,entity():pos(), -Drone:right())
           
 
        if(DodgeBox["UP", ranger]:hit())
        {
           
            AvoidPos[1, vector] = -Drone:up()+Drone:boxCenterW()
 
        }else{
       
            AvoidPos[1, vector] = vec(0,0,0)
        }
       
        if(DodgeBox["DOWN", ranger]:hit())
        {
            AvoidPos[2, vector] = Drone:up()+Drone:boxCenterW()
           
        }else{
       
            AvoidPos[2, vector] = vec(0,0,0)
        }
       
        if(DodgeBox["FORWARD", ranger]:hit()){
           
            AvoidPos[3, vector] =  -Drone:forward()+Drone:boxCenterW()
           
        }else{
       
            AvoidPos[3, vector] = vec(0,0,0)
        }
        if(DodgeBox["FORWARDRIGHT", ranger]:hit()){
           
            AvoidPos[4, vector] = -Drone:forward()-Drone:right()+Drone:boxCenterW()
           
        }else{
       
            AvoidPos[4, vector] = vec(0,0,0)
        }
        if(DodgeBox["FORWARDLEFT", ranger]:hit()){
           
            AvoidPos[5, vector] = -Drone:forward()+Drone:right()+Drone:boxCenterW()
           
        }else{
       
            AvoidPos[5, vector] = vec(0,0,0)
        }
        if(DodgeBox["BACK", ranger]:hit()){
           
            AvoidPos[6, vector] =  Drone:forward()+Drone:boxCenterW()
           
        }else{
       
            AvoidPos[6, vector] = vec(0,0,0)
        }
        if(DodgeBox["BACKRIGHT", ranger]:hit()){
           
            AvoidPos[7, vector] = Drone:forward()-Drone:right()+Drone:boxCenterW()
           
        }else{
       
            AvoidPos[7, vector] = vec(0,0,0)
        }
        if(DodgeBox["BACKLEFT", ranger]:hit()){
           
            AvoidPos[8, vector] = Drone:forward()+Drone:right()+Drone:boxCenterW()
           
        }else{
       
            AvoidPos[8, vector] = vec(0,0,0)
        }
        if(DodgeBox["LEFT", ranger]:hit()){
           
            AvoidPos[9, vector] =  Drone:right()+Drone:boxCenterW()
           
        }else{
       
            AvoidPos[9, vector] = vec(0,0,0)
        }
        if(DodgeBox["RIGHT", ranger]:hit()){
           
            AvoidPos[10, vector] = -Drone:right()+Drone:boxCenterW()
           
        }else{
       
            AvoidPos[10, vector] = vec(0,0,0)
        }
#lets add new movement vectors      
        for(I=1,AvoidPos:count()) {
            Dest = Dest + AvoidPos[I, vector]
        }
       
        stoptimer("weHitSomething")
       
    }
#orbit
    Circle=vec(0,1,0):rotate(0,(curtime()+Offset)*Speed,0)
 
    Vector=Dest+(Circle*OrbitRadius):setZ(Target:getSigRad()/pi())
    Pos = Target:toWorld(Target:boxCenter()+vec(0,0,-15))
    Eye = (Pos-Drone:pos()+Target:vel()):normalized()
    A = Eye:toAngle()
    TarQ = quat(A)
    CurQ = quat(Drone)
    Q = TarQ/CurQ
    T = Drone:toLocal(rotationVector(Q)+Drone:pos())
#we want to look on our target
    Drone:applyTorque((150*T - 12*Drone:angVelVector())*Drone:inertia())    
#we want to catch our target
    Drone:applyForce((((Vector-Drone:pos()))*Drone:mass()))
   
   
   
}else{
    Fire=0
}
if(Fire){
   
    for(I=1,Gunz:count()) {
        Gun=Gunz[I, wirelink]
           Gun["Fire",number]=1
    }
}
else{
   
       for(I=1,Gunz:count()) {
        Gun=Gunz[I, wirelink]
           Gun["Fire",number]=0
    }
}
if(clk("unFreeze&RadiusCheck")){
   
    DroneProp = Drone:getConstraints()
   
    for(I=1, DroneProp:count()){
      DroneProp:entity(I):propFreeze(0)
     }
     Drone:propFreeze(0)
 
    if(Target:getSigRad()>2000){
       
        OrbitRadius=Target:getSigRad()
       
    }
    elseif(Target:getSigRad()>6000){
   
        OrbitRadius=6000
    }
    else{
       
        OrbitRadius=1000
       
    }
    stoptimer("unFreeze&RadiusCheck")
}
 
if(last() | removing()) {
#lets remove deleted drone from the orbit in case someone spawn new one
    G[Offset/100,number] = 0
 
}
