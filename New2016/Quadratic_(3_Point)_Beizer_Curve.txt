@name Quadratic (3 Point) Beizer Curve
@persist PointsDone

# Local coordinates of the points to render in this order:
# Start Point, Reference Point, End Point
POINTS = array(vec(-100,-100,0),vec(0,200,200), vec(100,100,0))
SEGMENTS = 10
BASEPOINT_SIZE = 1
SEGMENT_SIZE = 0.5

E = entity()
if(first())
{
    for(I=1,3)
    {
        holoCreate(I,E:toWorld(POINTS[I,vector]),vec(1,1,1)*BASEPOINT_SIZE)
        holoModel(I,"hqicosphere2")
    }
    
    for(I=1,SEGMENTS)
    {
        Point1 = POINTS[1,vector]
        Point2 = POINTS[2,vector]
        Point3 = POINTS[3,vector]
        
        P1P2 = Point2 - Point1
        P2P3 = Point3 - Point2
        
        RefPnt1 = Point1 + (P1P2/SEGMENTS*I)
        RefPnt2 = Point2 + (P2P3/SEGMENTS*I)
        
        Vec = RefPnt2 - RefPnt1
        CurPoint = RefPnt1 + (Vec/SEGMENTS*I)
        
        holoCreate(I+3,E:toWorld(CurPoint),vec(1,1,1)*SEGMENT_SIZE)
    }
}
