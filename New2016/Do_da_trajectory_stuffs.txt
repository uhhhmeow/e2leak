@name Do da trajectory stuffs
@inputs
@outputs
@persist G A O
@trigger
 
# By Hell Fire
 
interval(100)
if(first())
{
    G = 9.8
   
    function number solveTrajectTime(U,A)
    {
        return (2*U*sin(A)) / G # time of flight
    }
    function vector solveTraject(U,A,T)
    {
        X = U * T * cos(A) # X displacment of T
        Y = U * T * sin(A) - 0.5*G*T^2 # Y displacment of T
        return vec(-X,0,Y)
       
    }
}
O++
 
U = 200
A = (sin(O)*90) + 90
T=solveTrajectTime(U,A)
 
for(I=1,T)
{
    holoCreate(I)
    holoModel(I,"hqsphere")
    holoScale(I,vec(5,5,5))
    holoColor(I,vec(sin(I*2)*255,cos(I*2)*255,0))
    holoPos(I,solveTraject(U,A,I)+entity():pos())
   
}
