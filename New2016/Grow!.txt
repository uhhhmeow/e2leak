@name Grow!
@persist Mode Spawn Health
@persist Vec:vector WarpV:vector Evolve
@persist AngDir:vector AngTime Ang:angle TPos:vector Target:entity
@persist SinSpin WarpA:angle Blink BodyFwd
@persist GrowCount PlatPos:vector Pellet:entity
@persist AvgEvolvePos:vector Stage CloseCount
@persist BodyAvg:vector LegRangers:array LegClk Alpha
@model models/hunter/misc/sphere025x025.mdl
@inputs Damage Attacker:entity
@outputs On
runOnTick(1)
if(duped()){reset()}
EggSize = 2
LegLength = 200
LegSpace = 100

runOnChat(1)

#Damage = entity():getDamage()
#Attacker = entity():getAttacker()

if(first())
{
Alpha = 255
On = 1
AngDir=entity():pos()
AngTime = 150
entity():setAlpha(0)
entity():setMass(50000)
for(I=1,4)
{
holoCreate(I)    
holoColor(I,vec(0,0,0))
}
holoModel(1,"dome2")
holoModel(2,"dome2")
holoScale(2,vec(1,1,1.5)*EggSize)
holoScale(1,vec(1,1,1)*EggSize)
holoColor(1,vec(0,0,0))
holoColor(2,vec(0,0,0))
holoPos(1,entity():pos())
holoAng(1,ang(180,0,0))
holoPos(2,entity():pos()-vec(0,0,0.5))     
    
}

if(->Damage)
{

if(changed(Attacker))
{
Target = Attacker    
} 

if(Mode==0)
{
if(Spawn>500)
{
holoModel(1,"hqicosphere2")
holoScale(1,vec(1,1,1))
holoModel(2,"hqicosphere2")
holoScale(2,vec(0.3,0.3,0.3))
holoModel(3,"hqicosphere2")
holoColor(3,vec(255,255,255))
holoScale(3,vec(0.3,0.3,0.3))
holoModel(4,"hqicosphere2")
holoColor(4,vec(255,255,255))
holoScale(4,vec(0.3,0.3,0.3))
holoColor(1,vec(0,0,0))
holoColor(2,vec(0,0,0))
holoPos(1,entity():pos())
holoParent(1,entity())
holoPos(2,holoEntity(1):pos()-holoEntity(1):forward()*6)
holoParent(2,1)
holoPos(3,holoEntity(1):pos()+holoEntity(1):forward()*3.5+holoEntity(1):right()*3.5)
holoParent(3,1)
holoPos(4,holoEntity(1):pos()+holoEntity(1):forward()*3.5-holoEntity(1):right()*3.5)
holoParent(4,1)    
    
findByClass("player")
Target = findClosest(entity():pos())    

Health = 25
Mode=1 
}
else
{
Spawn++
}   
}

if(Mode==1)
{
if(!Evolve)
{
   
rangerHitWater(1)
rangerFilter(entity())
Range = rangerOffset(6,entity():pos(),vec(0,0,-1))   
Range2=rangerOffset(8,entity():pos(),vec(sinr(curtime()*1000),cosr(curtime()*1000),0))

Vec = Vec-vec(0,0,0.1)

if(Range:hit()){Vec = vec(-1,0,2)}
if(Range2:hit())
{
Vec=Vec*vec(-1.5,-1.5,1)
}
TPos = Target:pos()
Ang = (entity():pos()-TPos):toAngle():setPitch(0)

entity():setPos(entity():toWorld(Vec))    
entity():setAng(Ang)
Health = Health-Damage

if(Health<=0)
{
holoCreate(5)
holoModel(5,"hqcylinder2")
holoScale(5,vec(5,5,0.1))
holoPos(5,entity():pos()+vec(0,0,-5))
holoAng(5,ang(0,0,0))
Evolve = 1    
WarpV = entity():pos()
WarpA = entity():angles()
}
}
else
{
SinSpin++
Sin = sin(SinSpin)
entity():setPos(WarpV+vec(0,0,Sin*50))
entity():setAng(WarpA+ang(0,Sin*5000,0))   
}
if(SinSpin==90)
{
holoModel(1,"hqicosphere2")
holoModel(4,"hqicosphere2")        
holoModel(2,"dome2")
holoModel(3,"dome2")
holoScale(1,vec(1,1,1))
holoScale(2,vec(1.1,1.1,1.1))
holoScale(3,vec(1.1,1.1,1.1))
holoScale(4,vec(0.1,0.1,0.1))
holoColor(1,vec(255,255,255))
holoColor(2,vec(0,0,0))
holoColor(3,vec(0,0,0))
holoColor(4,vec(0,0,0))
holoAng(3,ang(180,0,0))

holoPos(1,entity():pos())
holoPos(2,holoEntity(1):pos())
holoPos(3,holoEntity(1):pos())
holoPos(4,holoEntity(1):pos()+holoEntity(1):forward()*-5.5)

holoParent(1,entity())
holoParent(2,entity())
holoParent(3,entity())
holoParent(4,1)

Evolve = 0
Mode = 2
holoDelete(5)
Health = 50
}

if(SinSpin == 180)
{

}
}

if(Mode==2)
{
if(!Evolve)
{
Health=Health-Damage
Blink=Blink+1
Sin = abs(sin(Blink)*70)

holoAng(2,holoEntity(1):angles()-ang(Sin,0,0))
holoAng(3,holoEntity(1):angles()+ang(Sin+180,0,0))      

Dir = Target:shootPos()-entity():pos()

entity():setAng(Dir:toAngle())

if(entity():pos():distance(Target:shootPos())>50)
{
entity():setPos(entity():toWorld(vec(5,0,0)))
}

if(Health<=0)
{
holoCreate(5)
holoModel(5,"hqcylinder2")
holoScale(5,vec(5,5,0.1))
holoPos(5,entity():pos()+vec(0,0,-25))
holoAng(5,ang(0,0,0))

holoCreate(6)
holoModel(6,"hqcone")
holoColor(6,vec(0,0,0))

holoCreate(7)
holoModel(7,"hqcone")
holoColor(7,vec(0,0,0))

holoCreate(8)
holoModel(8,"hqcone")
holoColor(8,vec(0,0,0))

holoCreate(9)
holoColor(9,vec(0,0,0))
holoModel(9,"dome2")

for(I=10,30)
{
holoCreate(I)
holoModel(I,"hqicosphere2")
holoColor(I,vec(0,0,0))
holoScale(I,vec(0.5,0.5,0.5))
}

holoScale(9,vec(3,3,1))

rangerFilter(entity())
rangerHitEntities(0)
rangerIgnoreWorld(0)
PlatRange = rangerOffset(10000,entity():pos(),vec(0,0,-1))
PlatPos = PlatRange:position()
Norm = PlatRange:hitNormal()
holoPos(9,PlatPos)
holoAng(9,Norm:toAngle()+ang(90,0,0))

Evolve = 1    
WarpV = entity():pos()
WarpA = entity():angles()
GrowCount = 1
SinSpin = 0
}
}
else
{
    
SinSpin++
Sin = sin(SinSpin)
entity():setAng(WarpA+ang(0,Sin*5000,0))   

if(GrowCount<=2)
{
GrowCount=GrowCount+0.01
holoScale(1,vec(1,1,1)*GrowCount)
holoScale(2,vec(1.1,1.1,1.1)*GrowCount)
holoScale(3,vec(1.1,1.1,1.1)*GrowCount)
holoScale(4,vec(0.1,0.1,0.1)*GrowCount)
holoColor(1,vec(255,255,255))
holoColor(2,vec(0,0,0))
holoColor(3,vec(0,0,0))
holoColor(4,vec(0,0,0))
holoAng(3,ang(180,0,0))

holoPos(1,entity():pos())
holoPos(2,holoEntity(1):pos())
holoPos(3,holoEntity(1):pos())
holoPos(4,holoEntity(1):pos()+holoEntity(1):forward()*-5.5*GrowCount)

holoAng(6,entity():angles())
holoPos(6,entity():toWorld(vec(0,0,GrowCount*5-1)))
holoScale(6,vec(1,1,1)*GrowCount-1)

holoAng(7,entity():angles()-ang(45,0,0))
holoPos(7,entity():toWorld(vec(-GrowCount*3.5-1,0,GrowCount*3.5-1)))
holoScale(7,vec(1,1,1)*GrowCount-1)

holoAng(8,entity():angles()-ang(90,0,0))
holoPos(8,entity():toWorld(vec(-GrowCount*4-1,0,0)))
holoScale(8,vec(1,1,1)*GrowCount-1)

}
else
{
holoDelete(5)   
holoParent(6,entity())
holoParent(7,entity())
holoParent(8,entity())

}
Blink=Blink+1
BSin = abs(sin(Blink)*70)

holoAng(2,holoEntity(1):angles()-ang(BSin,0,0))
holoAng(3,holoEntity(1):angles()+ang(BSin+180,0,0))      

if(SinSpin==90&GrowCount>=1.9)
{
holoParent(6,entity())
holoParent(7,entity())
holoParent(8,entity())
holoDelete(5)

Evolve = 0
Mode = 3  
Health = 100  
}

}

}

if(Mode==3)
{
if(!Evolve)
{
for(I=10,30)
{
T = (I-10)/20    

P0 = entity():pos()
P1 = PlatPos+vec(0,0,100)
P2 = PlatPos

One = ((1-T)^2)*P0
Two = 2*(1-T)*T*P1
Three = (T^2)*P2
Bezier = One+Two+Three
holoPos(I,Bezier)
}

if(Target:pos():distance(entity():pos())<500 & BSin==0)
{
timer("shoot",1000)
if(clk("shoot"))
{
Pellet:propDelete()
Pellet = propSpawn("models/hunter/misc/sphere025x025.mdl",0)   
Pellet:setPos(holoEntity(4):pos())
Pellet:setColor(vec(0,0,0))
Pellet:setMaterial("models/debug/debugwhite")
Pellet:setMass(100)
Pellet:applyForce((Target:shootPos()-Pellet:pos())*Pellet:mass()*10000)
}  
}
else
{
Blink=Blink+1
BSin = abs(sin(Blink)*70)
holoAng(2,holoEntity(1):angles()-ang(BSin,0,0))
holoAng(3,holoEntity(1):angles()+ang(BSin+180,0,0))      
}

Health=Health-Damage

TPos = Target:shootPos()-PlatPos
NormTarget = PlatPos+(TPos:normalized()*100)
entity():setPos(NormTarget)
entity():setAng(TPos:toAngle())

if(Health<=0)
{
Evolve = 1     

for(I=1,10)
{
holoUnparent(I)        
}


AvgEvolvePos=entity():pos()+vec(0,0,200)   

}
}
else
{
if(!Stage)
{
for(I=1,30)
{
if(ops()>4500){break}
if(holoEntity(I):pos()!=AvgEvolvePos)
{
NormEvolvePos = (AvgEvolvePos-holoEntity(I):pos())
holoPos(I,holoEntity(I):pos()+NormEvolvePos/100)
}
}

NormEvolvePos = (AvgEvolvePos-entity():pos())
entity():setPos(entity():pos()+NormEvolvePos/100)

CloseCount++

if(CloseCount==300)
{
holoCreate(10)
holoCreate(11)
holoModel(10,"dome2")
holoModel(11,"dome2")
holoColor(10,vec(0,0,0))
holoColor(11,vec(0,0,0))
holoScale(10,vec(5,5,5))
holoScale(11,vec(5,5,5))
holoAng(10,ang(0,0,0))
holoAng(11,ang(180,0,0))

Stage = 1   
}
}
if(Stage==1)
{
holoPos(10,AvgEvolvePos+vec(0,0,CloseCount))
holoPos(11,AvgEvolvePos-vec(0,0,CloseCount-2.5))
CloseCount--

if(CloseCount==0)
{
Stage = 2    
GrowCount = 1
}
}
if(Stage==2)
{
GrowCount=GrowCount+0.01
holoScale(10,vec(5,5,5)*GrowCount)
holoScale(11,vec(5,5,5)*GrowCount)
if(GrowCount==5)
{
#holoAlpha(10,10)
#holoAlpha(11,10)
Stage = 3    
entity():setAng(ang(0,0,0))
holoPos(1,entity():pos())
holoPos(2,holoEntity(1):pos())
holoPos(3,holoEntity(1):pos())
holoPos(4,holoEntity(1):pos()+holoEntity(1):forward()*5.5*-5)

holoParent(1,entity())
holoParent(2,entity())
holoParent(3,entity())
holoParent(4,1)
}

}
if(Stage==3)
{
holoScale(1,vec(1,1,1)*5)
holoScale(2,vec(1.1,1.1,1.1)*5)
holoScale(3,vec(1.1,1.1,1.1)*5)
holoScale(4,vec(0.1,0.1,0.1)*5)

holoCreate(5)
holoModel(5,"hqicosphere2")
holoColor(5,vec(0,0,0))   
holoScale(5,vec(7,7,7))
holoAng(5,ang(0,0,0))
holoPos(5,entity():pos()+entity():forward()*-60)
holoParent(5,entity())
Stage = 4
}
if(Stage == 4)
{
BodyFwd++
if(BodyFwd<100)
{
entity():setPos(entity():toWorld(vec(1,0,0)))
}
else
{
Stage = 5    

holoPos(6,entity():pos()+entity():forward()*-150)
holoModel(6,"hqicosphere2")
holoScale(6,vec(10,10,10))
holoParent(6,entity())
for(I=7,9)
{
holoDelete(I)    
}
for(I=12,30)
{
holoDelete(I)    
}   
}   
}

if(Stage == 5)
{


for(I=12,35)
{
holoCreate(I)    
holoColor(I,vec(0,0,0))
}
for(I=12,23)
{
holoModel(I,"hqicosphere2")
}
for(I=24,35)
{
holoModel(I,"hqcylinder")    
holoScaleUnits(I,vec(5,5,LegLength))
}
Evolve = 0
Mode = 4
Health = 500
holoDelete(10)
holoDelete(11)
holoAng(5,ang(0,0,0))
rangerReset()

LMR1 = holoEntity(5):pos()+holoEntity(5):right()*-LegSpace
RMR1 = holoEntity(5):pos()+holoEntity(5):right()*LegSpace
LFR1 = holoEntity(5):pos()+holoEntity(5):right()*-LegSpace+holoEntity(5):forward()*LegSpace
RFR1 = holoEntity(5):pos()+holoEntity(5):right()*LegSpace+holoEntity(5):forward()*LegSpace
LBR1 = holoEntity(5):pos()+holoEntity(5):right()*-LegSpace+holoEntity(5):forward()*-LegSpace
RBR1 = holoEntity(5):pos()+holoEntity(5):right()*LegSpace+holoEntity(5):forward()*-LegSpace
LegRangers = array(LMR1,RMR1,LFR1,RFR1,LBR1,RBR1)
}

}

}
if(Mode==4)
{
if(!Evolve)
{
rangerIgnoreWorld(0)
rangerHitEntities(1)
rangerHitWater(1)
rangerFilter(entity())
LMR = rangerOffset(100000,LegRangers[1,vector],vec(0,0,-1))
RMR = rangerOffset(100000,LegRangers[2,vector],vec(0,0,-1))
LFR = rangerOffset(100000,LegRangers[3,vector],vec(0,0,-1))
RFR = rangerOffset(100000,LegRangers[4,vector],vec(0,0,-1))
LBR = rangerOffset(100000,LegRangers[5,vector],vec(0,0,-1))
RBR = rangerOffset(100000,LegRangers[6,vector],vec(0,0,-1))
    
holoPos(12,LMR:position())
holoPos(13,RMR:position())
holoPos(14,LFR:position())
holoPos(15,RFR:position())
holoPos(16,LBR:position())
holoPos(17,RBR:position())

LegPoints = array(LMR:position(),RMR:position(),LFR:position(),RFR:position(),LBR:position(),RBR:position())
BodyAvg = vec(0,0,0)
for(I=1,LegPoints:count())
{
Distance = 400
Pos=LegPoints[I,vector]
Dist_vec=(holoEntity(5):pos()-Pos)
Dist=Dist_vec:length()
Ang=Dist_vec:toAngle()

Ang2=ang(-asin(Dist/(Distance)),0,0)

holoPos(I+23,holoEntity(5):pos()+(Ang+Ang2):up()*Distance/4)
holoAng(I+23,Ang+Ang2)
holoPos(I+23+6,Pos+(Ang-Ang2):up()*Distance/4)
holoAng(I+23+6,Ang-Ang2)
holoPos(I+17,holoEntity(I+23):pos()+holoEntity(I+23):up()*100)
}

if(Target:shootPos():distance(entity():pos())>400)
{
timer("legs",1000)
if(clk("legs"))
{
LegClk = !LegClk    
}

if(LegClk)
{
#146   
LegRangers[1,vector] = LegRangers[1,vector] + entity():forward()    
LegRangers[4,vector] = LegRangers[4,vector] + entity():forward()    
LegRangers[6,vector] = LegRangers[6,vector] + entity():forward()    
        
}
else
{
#235    
LegRangers[2,vector] = LegRangers[2,vector] + entity():forward()    
LegRangers[3,vector] = LegRangers[3,vector] + entity():forward()    
LegRangers[5,vector] = LegRangers[5,vector] + entity():forward()    
}

BodyRange = rangerOffset(100000,entity():pos(),vec(0,0,-1))
BodyPos = BodyRange:position()

entity():setPos(BodyPos+vec(0,0,200)+entity():forward()/2)  
}
else
{
BodyRange = rangerOffset(100000,entity():pos(),vec(0,0,-1))
BodyPos = BodyRange:position()
entity():setPos(BodyPos+vec(0,0,200))  
timer("shoot",500)
if(clk("shoot"))
{
Pellet:propDelete()
Pellet = propSpawn("models/hunter/misc/sphere025x025.mdl",0)   
Pellet:setPos(holoEntity(4):pos())
Pellet:setColor(vec(0,0,0))
Pellet:setMaterial("models/debug/debugwhite")
Pellet:setMass(100)
Pellet:applyForce((Target:shootPos()-Pellet:pos())*Pellet:mass()*10000)
}  
  
}

TPos = Target:pos()-entity():pos()
entity():setAng(ang(0,TPos:toAngle():yaw(),0))
Blink=Blink+1
BSin = abs(sin(Blink)*70)
holoAng(2,holoEntity(1):angles()-ang(BSin,0,0))
holoAng(3,holoEntity(1):angles()+ang(BSin+180,0,0))      

Health=Health-Damage

if(Health<=0)
{
Evolve = 1    
for(I=1,35)
{
holoUnparent(I)
    
}
    
}
}

else
{
Alpha = Alpha-1
for(I=1,35)
{
Dir = holoEntity(I):pos()-entity():pos()    

holoPos(I,holoEntity(I):pos()+Dir:normalized()*2)
holoAlpha(I,Alpha)

}
if(Alpha<=0)
{
entity():setPos(Target:shootPos()+vec(0,0,100))
entity():setAng(ang(0,0,0))
reset()    
}    
}
}

}

#if(owner():lastSaid()=="induce"&chatClk()&lastSpoke()==owner())
#{
#Evolve = 1   
#}
