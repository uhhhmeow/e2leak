@name Holopad Export

#####
# Holograms authored by Erza on 01/01/2016
# Exported from Holopad 09/11/2012 (BETA 6.7) by Bubbus
# Thanks to Vercas for the original E2 export template!
#
# FOR AN EXPLANATION OF THE CODE BELOW, VISIT http://code.google.com/p/holopad/wiki/NewE2ExportFormatHOWTO
##### 

#####
# Hologram spawning data
@persist [Holos Clips]:table HolosSpawned HolosStep LastHolo TotalHolos
@persist E:entity
#####


if (first() | duped())
{
    E = entity()

    function number addHolo(Pos:vector, Scale:vector, Colour:vector4, Angles:angle, Model:string, Material:string, Parent:number)
    {
        if (holoRemainingSpawns() < 1) {error("This model has too many holos to spawn! (" + TotalHolos + " holos!)"), return 0}
        
        holoCreate(LastHolo, E:toWorld(Pos), Scale, E:toWorld(Angles))
        holoModel(LastHolo, Model)
        holoMaterial(LastHolo, Material)
        holoColor(LastHolo, vec(Colour), Colour:w())

        if (Parent > 0) {holoParent(LastHolo, Parent)}
        else {holoParent(LastHolo, E)}

        local Key = LastHolo + "_"
        local I=1
        while (Clips:exists(Key + I))
        {
            holoClipEnabled(LastHolo, 1)
            local ClipArr = Clips[Key+I, array]
            holoClip(LastHolo, I, holoEntity(LastHolo):toLocal(E:toWorld(ClipArr[1, vector])), holoEntity(LastHolo):toLocalAxis(E:toWorldAxis(ClipArr[2, vector])), 0)
            I++
        }
        
        return LastHolo
    }

    ##########
    # HOLOGRAMS
    

    #[   ]#    Holos[1, array] = array(vec(43.9810, 25.0130, 114.5670), vec(1.0000, 1.9057, 1.8632), vec4(212, 212, 212, 255), ang(0.0000, 0.0000, 0.0000), "cube", "", 0)
    #[   ]#    Holos[2, array] = array(vec(43.0710, 0.0000, 68.6204), vec(1.0000, 7.5726, 11.0076), vec4(12, 0, 127, 255), ang(0.0000, 0.0000, 0.0000), "cube", "", 0)
    #[   ]#    Holos[3, array] = array(vec(0.0000, 0.0000, 164.2805), vec(1.3290, 1.3332, 1.0000), vec4(12, 0, 127, 255), ang(0.0000, 0.0000, 0.0000), "cube", "", 0)
    #[   ]#    Holos[4, array] = array(vec(44.5950, 24.4460, 86.1850), vec(1.9306, 2.0260, 1.0000), vec4(0, 27, 127, 255), ang(90.0000, 0.0000, 0.0000), "hq_stube_thin", "", 0)
    #[   ]#    Holos[5, array] = array(vec(44.5950, -24.4460, 30.4890), vec(1.9306, 2.0260, 1.0000), vec4(0, 27, 127, 255), ang(90.0000, 0.0000, 0.0000), "hq_stube_thin", "", 0)
    #[   ]#    Holos[6, array] = array(vec(44.5950, 24.4460, 30.4890), vec(1.9306, 2.0260, 1.0000), vec4(0, 27, 127, 255), ang(90.0000, 0.0000, 0.0000), "hq_stube_thin", "", 0)
    #[   ]#    Holos[7, array] = array(vec(44.5947, -24.4464, 114.4466), vec(1.9306, 2.0260, 1.0000), vec4(0, 27, 127, 255), ang(90.0000, 0.0000, 0.0000), "hq_stube_thin", "", 0)
    #[   ]#    Holos[8, array] = array(vec(0.0000, 0.0000, 74.7716), vec(8.0463, 8.3945, 12.7923), vec4(12, 0, 127, 255), ang(0.0000, 0.0000, 0.0000), "cube", "", 0)
    #[   ]#    Holos[9, array] = array(vec(44.5950, -24.4460, 86.1850), vec(1.9306, 2.0260, 1.0000), vec4(0, 27, 127, 255), ang(90.0000, 0.0000, 0.0000), "hq_stube_thin", "", 0)
    #[   ]#    Holos[10, array] = array(vec(0.0000, 0.0000, 146.7194), vec(9.1350, 9.3611, 1.3628), vec4(12, 0, 127, 255), ang(0.0000, 0.0000, 0.0000), "cube", "", 0)
    #[   ]#    Holos[11, array] = array(vec(49.6162, 0.0000, 147.8200), vec(0.8593, 7.4205, 1.0000), vec4(0, 27, 127, 255), ang(90.0000, 0.0000, 0.0000), "hq_stube_thin", "", 0)
    #[   ]#    Holos[12, array] = array(vec(0.0390, -6.3850, 173.5100), vec(0.1813, 0.1564, 1.7388), vec4(12, 0, 127, 255), ang(0.0000, 0.0000, 0.0000), "cube", "", 0)
    #[   ]#    Holos[13, array] = array(vec(0.0000, 0.0000, 183.1316), vec(1.3290, 1.3332, 0.2069), vec4(12, 0, 127, 255), ang(0.0000, 0.0000, 0.0000), "cube", "", 0)
    #[   ]#    Holos[14, array] = array(vec(0.0388, 6.3851, 173.5100), vec(0.1813, 0.1564, 1.7388), vec4(12, 0, 127, 255), ang(0.0000, 0.0000, 0.0000), "cube", "", 0)
    #[   ]#    Holos[15, array] = array(vec(-6.3650, 0.0000, 173.5100), vec(0.1813, 0.1564, 1.7388), vec4(12, 0, 127, 255), ang(0.0000, 0.0000, 0.0000), "cube", "", 0)
    #[   ]#    Holos[16, array] = array(vec(0.0000, 0.0000, 171.3661), vec(1.0000, 1.0000, 2.0180), vec4(141, 141, 141, 255), ang(0.0000, 0.0000, 0.0000), "cube", "", 0)
    #[   ]#    Holos[17, array] = array(vec(0.0000, 0.0000, 162.4571), vec(7.0548, 6.5339, 1.0000), vec4(12, 0, 127, 255), ang(0.0000, 0.0000, 0.0000), "cube", "", 0)
    #[   ]#    Holos[18, array] = array(vec(6.3651, 0.0000, 173.5101), vec(0.1813, 0.1564, 1.7388), vec4(12, 0, 127, 255), ang(0.0000, 0.0000, 0.0000), "cube", "", 0)
    #[   ]#    Holos[19, array] = array(vec(44.5950, 24.4460, 58.3372), vec(1.9306, 2.0260, 1.0000), vec4(0, 27, 127, 255), ang(90.0000, 0.0000, 0.0000), "hq_stube_thin", "", 0)
    #[   ]#    Holos[20, array] = array(vec(44.5950, 24.4460, 114.4470), vec(1.9306, 2.0260, 1.0000), vec4(0, 27, 127, 255), ang(90.0000, 0.0000, 0.0000), "hq_stube_thin", "", 0)
    #[   ]#    Holos[21, array] = array(vec(43.9814, -25.0132, 114.5666), vec(1.0000, 1.9057, 1.8632), vec4(212, 212, 212, 255), ang(0.0000, 0.0000, 0.0000), "cube", "", 0)
    #[   ]#    Holos[22, array] = array(vec(51.9232, 0.0000, 147.8196), vec(0.6476, 7.3474, 0.7558), vec4(0, 0, 0, 255), ang(0.0000, 0.0000, 0.0000), "cube", "", 0)
    #[   ]#    Holos[23, array] = array(vec(43.1440, 24.4460, 68.5870), vec(1.0000, 2.8031, 10.8248), vec4(0, 27, 127, 255), ang(0.0000, 0.0000, 0.0000), "cube", "", 0)
    #[   ]#    Holos[24, array] = array(vec(43.1439, -24.4460, 68.5870), vec(1.0000, 2.8031, 10.8248), vec4(0, 27, 127, 255), ang(0.0000, 0.0000, 0.0000), "cube", "", 0)
    #[   ]#    Holos[25, array] = array(vec(0.0000, 0.0000, 155.3856), vec(8.1017, 7.8180, 1.0000), vec4(12, 0, 127, 255), ang(0.0000, 0.0000, 0.0000), "cube", "", 0)
    #[   ]#    Holos[26, array] = array(vec(44.5950, -24.4460, 58.3370), vec(1.9306, 2.0260, 1.0000), vec4(0, 27, 127, 255), ang(90.0000, 0.0000, 0.0000), "hq_stube_thin", "", 0)
    
    ##########
    
    TotalHolos = Holos:count()
    if (0 > holoClipsAvailable()) {error("A holo has too many clips to spawn on this server! (Max is " + holoClipsAvailable() + ")")}
}


#You may place code here if it doesn't require all of the holograms to be spawned.


if (HolosSpawned)
{
    #Your code goes here if it needs all of the holograms to be spawned!
}
else
{
    while (LastHolo <= Holos:count() & holoCanCreate() & perf())
    {
        local Ar = Holos[LastHolo, array]
        addHolo(Ar[1, vector], Ar[2, vector], Ar[3, vector4], Ar[4, angle], Ar[5, string], Ar[6, string], Ar[7, number])
        LastHolo++
    }
    
    if (LastHolo > Holos:count())
    {
        Holos:clear()
        Clips:clear()
        HolosSpawned = 1
        E:setAlpha(0)
    }

    interval(1000)
}
