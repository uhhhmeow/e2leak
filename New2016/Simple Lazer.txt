@name SimpleLazerForCaleb7

#####
# Holograms authored by Erza on 23/01/2016
# Exported from Holopad 09/11/2012 (BETA 6.7) by Bubbus
# Thanks to Vercas for the original E2 export template!
#
# FOR AN EXPLANATION OF THE CODE BELOW, VISIT http://code.google.com/p/holopad/wiki/NewE2ExportFormatHOWTO
##### 

#####
# Hologram spawning data
@persist [Holos Clips]:table HolosSpawned HolosStep LastHolo TotalHolos
@persist E:entity
#####


if (first() | duped())
{
    E = entity()

    function number addHolo(Pos:vector, Scale:vector, Colour:vector4, Angles:angle, Model:string, Material:string, Parent:number)
    {
        if (holoRemainingSpawns() < 1) {error("This model has too many holos to spawn! (" + TotalHolos + " holos!)"), return 0}
        
        holoCreate(LastHolo, E:toWorld(Pos), Scale, E:toWorld(Angles))
        holoModel(LastHolo, Model)
        holoMaterial(LastHolo, Material)
        holoColor(LastHolo, vec(Colour), Colour:w())

        if (Parent > 0) {holoParent(LastHolo, Parent)}
        else {holoParent(LastHolo, E)}

        local Key = LastHolo + "_"
        local I=1
        while (Clips:exists(Key + I))
        {
            holoClipEnabled(LastHolo, 1)
            local ClipArr = Clips[Key+I, array]
            holoClip(LastHolo, I, holoEntity(LastHolo):toLocal(E:toWorld(ClipArr[1, vector])), holoEntity(LastHolo):toLocalAxis(E:toWorldAxis(ClipArr[2, vector])), 0)
            I++
        }
        
        return LastHolo
    }

    ##########
    # HOLOGRAMS
    

    #[   ]#    Holos[1, array] = array(vec(-0.0000, 8.0224, 21.4697), vec(1.0442, 1.1955, 0.4701), vec4(0, 127, 27, 255), ang(90.0000, 90.0000, 0.0000), "torus", "", 0)
    #[   ]#    Holos[2, array] = array(vec(0.0000, -4.3626, 20.8477), vec(0.2926, 0.2921, 2.2894), vec4(0, 127, 27, 255), ang(90.0000, 0.0000, 90.0000), "hq_cylinder", "", 0)
    #[   ]#    Holos[3, array] = array(vec(0.0000, 16.3390, 20.8480), vec(0.5568, 1.0000, 0.4214), vec4(0, 127, 27, 255), ang(0.0000, 0.0000, 0.0000), "icosphere", "", 0)
    #[   ]#    Holos[4, array] = array(vec(-0.0000, 0.0000, 21.4697), vec(1.4599, 1.6939, 0.4701), vec4(0, 127, 27, 255), ang(90.0000, 90.0000, 0.0000), "torus", "", 0)
    #[   ]#    Holos[5, array] = array(vec(0.0000, 5.1961, 20.8480), vec(0.7214, 2.5914, 0.5975), vec4(0, 127, 27, 255), ang(0.0000, 0.0000, 0.0000), "icosphere", "", 0)
    #[   ]#    Holos[6, array] = array(vec(-0.0000, 15.3765, 21.4697), vec(0.7237, 0.9478, 0.4701), vec4(0, 127, 27, 255), ang(90.0000, 90.0000, 0.0000), "torus", "", 0)
    #[   ]#    Holos[7, array] = array(vec(0.0000, 0.1508, 20.8480), vec(1.0000, 1.0000, 1.0000), vec4(0, 127, 27, 255), ang(0.0000, 0.0000, 0.0000), "icosphere", "", 0)
    
    ##########
    
    TotalHolos = Holos:count()
    if (0 > holoClipsAvailable()) {error("A holo has too many clips to spawn on this server! (Max is " + holoClipsAvailable() + ")")}
}


#You may place code here if it doesn't require all of the holograms to be spawned.


if (HolosSpawned)
{
    #Your code goes here if it needs all of the holograms to be spawned!
}
else
{
    while (LastHolo <= Holos:count() & holoCanCreate() & perf())
    {
        local Ar = Holos[LastHolo, array]
        addHolo(Ar[1, vector], Ar[2, vector], Ar[3, vector4], Ar[4, angle], Ar[5, string], Ar[6, string], Ar[7, number])
        LastHolo++
    }
    
    if (LastHolo > Holos:count())
    {
        Holos:clear()
        Clips:clear()
        HolosSpawned = 1
        E:setAlpha(0)
    }

    interval(1000)
}
