@name Holopad Export

#####
# Holograms authored by Erza on 16/01/2016
# Exported from Holopad 09/11/2012 (BETA 6.7) by Bubbus
# Thanks to Vercas for the original E2 export template!
#
# FOR AN EXPLANATION OF THE CODE BELOW, VISIT http://code.google.com/p/holopad/wiki/NewE2ExportFormatHOWTO
##### 

#####
# Hologram spawning data
@persist [Holos Clips]:table HolosSpawned HolosStep LastHolo TotalHolos
@persist E:entity
#####


if (first() | duped())
{
    E = entity()

    function number addHolo(Pos:vector, Scale:vector, Colour:vector4, Angles:angle, Model:string, Material:string, Parent:number)
    {
        if (holoRemainingSpawns() < 1) {error("This model has too many holos to spawn! (" + TotalHolos + " holos!)"), return 0}
        
        holoCreate(LastHolo, E:toWorld(Pos), Scale, E:toWorld(Angles))
        holoModel(LastHolo, Model)
        holoMaterial(LastHolo, Material)
        holoColor(LastHolo, vec(Colour), Colour:w())

        if (Parent > 0) {holoParent(LastHolo, Parent)}
        else {holoParent(LastHolo, E)}

        local Key = LastHolo + "_"
        local I=1
        while (Clips:exists(Key + I))
        {
            holoClipEnabled(LastHolo, 1)
            local ClipArr = Clips[Key+I, array]
            holoClip(LastHolo, I, holoEntity(LastHolo):toLocal(E:toWorld(ClipArr[1, vector])), holoEntity(LastHolo):toLocalAxis(E:toWorldAxis(ClipArr[2, vector])), 0)
            I++
        }
        
        return LastHolo
    }

    ##########
    # HOLOGRAMS
    

    #[   ]#    Holos[1, array] = array(vec(-25.7910, -7.6380, 31.9010), vec(0.7000, 0.7000, 0.7000), vec4(0, 127, 27, 255), ang(0.0000, 0.0000, 0.0000), "hq_icosphere", "", 0)
    #[   ]#    Holos[2, array] = array(vec(-33.1315, -13.4470, 0.0000), vec(1.0000, 1.0000, 1.0000), vec4(0, 127, 27, 255), ang(0.0000, 0.0000, 0.0000), "cube", "", 0)
    #[   ]#    Holos[3, array] = array(vec(20.9109, 7.6376, 31.9013), vec(0.7000, 0.7000, 0.7000), vec4(0, 127, 27, 255), ang(0.0000, 0.0000, 0.0000), "hq_icosphere", "", 0)
    #[   ]#    Holos[4, array] = array(vec(-33.1320, 13.4470, 0.0000), vec(1.0000, 1.0000, 1.0000), vec4(0, 127, 27, 255), ang(0.0000, 0.0000, 0.0000), "cube", "", 0)
    #[   ]#    Holos[5, array] = array(vec(20.3530, 0.0000, 32.7280), vec(1.0000, 1.0000, 1.0000), vec4(0, 255, 216, 255), ang(0.0000, 0.0000, 0.0000), "cube", "", 0)
    #[   ]#    Holos[6, array] = array(vec(32.1535, 13.4472, 0.0000), vec(1.0000, 1.0000, 1.0000), vec4(0, 127, 27, 255), ang(0.0000, 0.0000, 0.0000), "cube", "", 0)
    #[   ]#    Holos[7, array] = array(vec(20.9110, -7.6380, 31.9010), vec(0.7000, 0.7000, 0.7000), vec4(0, 127, 27, 255), ang(0.0000, 0.0000, 0.0000), "hq_icosphere", "", 0)
    #[   ]#    Holos[8, array] = array(vec(32.1540, -13.4470, 0.0000), vec(1.0000, 1.0000, 1.0000), vec4(0, 127, 27, 255), ang(0.0000, 0.0000, 0.0000), "cube", "", 0)
    #[   ]#    Holos[9, array] = array(vec(0.0000, 0.0000, 30.2105), vec(1.0000, 1.0000, 1.0000), vec4(127, 0, 0, 255), ang(0.0000, 0.0000, 0.0000), "cube", "", 0)
    #[   ]#    Holos[10, array] = array(vec(47.5217, 0.0000, 35.5836), vec(1.0000, 1.0000, 1.0000), vec4(255, 161, 0, 255), ang(0.0000, 0.0000, 0.0000), "cube", "", 0)
    #[   ]#    Holos[11, array] = array(vec(-24.7160, 0.0000, 32.7280), vec(1.0000, 1.0000, 1.0000), vec4(0, 255, 216, 255), ang(0.0000, 0.0000, 0.0000), "cube", "", 0)
    #[   ]#    Holos[12, array] = array(vec(-25.7908, 7.6376, 31.9013), vec(0.7000, 0.7000, 0.7000), vec4(0, 127, 27, 255), ang(0.0000, 0.0000, 0.0000), "hq_icosphere", "", 0)
    
    ##########
    
    TotalHolos = Holos:count()
    if (0 > holoClipsAvailable()) {error("A holo has too many clips to spawn on this server! (Max is " + holoClipsAvailable() + ")")}
}


#You may place code here if it doesn't require all of the holograms to be spawned.


if (HolosSpawned)
{
    #Your code goes here if it needs all of the holograms to be spawned!
}
else
{
    while (LastHolo <= Holos:count() & holoCanCreate() & perf())
    {
        local Ar = Holos[LastHolo, array]
        addHolo(Ar[1, vector], Ar[2, vector], Ar[3, vector4], Ar[4, angle], Ar[5, string], Ar[6, string], Ar[7, number])
        LastHolo++
    }
    
    if (LastHolo > Holos:count())
    {
        Holos:clear()
        Clips:clear()
        HolosSpawned = 1
        E:setAlpha(0)
    }

    interval(1000)
}
