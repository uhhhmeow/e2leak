@name Holopad Export

#####
# Holograms authored by Erza on 02/01/2016
# Exported from Holopad 09/11/2012 (BETA 6.7) by Bubbus
# Thanks to Vercas for the original E2 export template!
#
# FOR AN EXPLANATION OF THE CODE BELOW, VISIT http://code.google.com/p/holopad/wiki/NewE2ExportFormatHOWTO
##### 

#####
# Hologram spawning data
@persist [Holos Clips]:table HolosSpawned HolosStep LastHolo TotalHolos
@persist E:entity
#####


if (first() | duped())
{
    E = entity()

    function number addHolo(Pos:vector, Scale:vector, Colour:vector4, Angles:angle, Model:string, Material:string, Parent:number)
    {
        if (holoRemainingSpawns() < 1) {error("This model has too many holos to spawn! (" + TotalHolos + " holos!)"), return 0}
        
        holoCreate(LastHolo, E:toWorld(Pos), Scale, E:toWorld(Angles))
        holoModel(LastHolo, Model)
        holoMaterial(LastHolo, Material)
        holoColor(LastHolo, vec(Colour), Colour:w())

        if (Parent > 0) {holoParent(LastHolo, Parent)}
        else {holoParent(LastHolo, E)}

        local Key = LastHolo + "_"
        local I=1
        while (Clips:exists(Key + I))
        {
            holoClipEnabled(LastHolo, 1)
            local ClipArr = Clips[Key+I, array]
            holoClip(LastHolo, I, holoEntity(LastHolo):toLocal(E:toWorld(ClipArr[1, vector])), holoEntity(LastHolo):toLocalAxis(E:toWorldAxis(ClipArr[2, vector])), 0)
            I++
        }
        
        return LastHolo
    }

    ##########
    # HOLOGRAMS
    

    #[   ]#    Holos[1, array] = array(vec(0.0000, 0.0000, 14.2153), vec(1.0000, 1.0000, 1.0000), vec4(70, 127, 63, 255), ang(0.0000, 0.0000, 0.0000), "hq_dome", "models/debug/debugwhite", 0)
    #[   ]#    Holos[2, array] = array(vec(0.0000, -3.7738, 14.1641), vec(1.0000, 1.6579, 0.2232), vec4(70, 127, 63, 255), ang(0.0000, 0.0000, 0.0000), "cube", "models/debug/debugwhite", 0)
    #[   ]#    Holos[3, array] = array(vec(0.0000, 0.0000, 31.6305), vec(1.0000, 1.0000, 0.3119), vec4(70, 127, 63, 255), ang(0.0000, 0.0000, 0.0000), "hq_rcube_thin", "models/debug/debugwhite", 0)
    #[   ]#    Holos[4, array] = array(vec(0.0000, 0.0000, -2.1227), vec(1.0000, 1.0000, 1.0000), vec4(70, 127, 63, 255), ang(0.0000, 0.0000, 0.0000), "cube", "models/debug/debugwhite", 0)
    #[   ]#    Holos[5, array] = array(vec(0.0000, -10.7147, 5.9411), vec(0.4000, 0.4000, 1.5786), vec4(127, 127, 127, 255), ang(0.0000, 0.0000, 0.0000), "cube", "models/debug/debugwhite", 0)
    #[   ]#    Holos[6, array] = array(vec(0.0000, 0.0000, 12.4971), vec(0.8000, 0.8000, 3.1380), vec4(70, 127, 63, 255), ang(0.0000, 0.0000, 0.0000), "hq_tube", "models/debug/debugwhite", 0)
    #[   ]#    Holos[7, array] = array(vec(0.0000, 0.0000, 24.7027), vec(1.0000, 1.0000, 1.0000), vec4(141, 141, 141, 255), ang(0.0000, 0.0000, 0.0000), "models/Items/combine_rifle_ammo01.mdl", "models/debug/debugwhite", 0)
    #[   ]#    Holos[8, array] = array(vec(0.0000, 0.0000, 4.6060), vec(1.0000, 1.0000, 1.0000), vec4(70, 127, 63, 255), ang(0.0000, 0.0000, 0.0000), "hq_torus", "models/debug/debugwhite", 0)
    #[   ]#    Holos[9, array] = array(vec(0.0000, 5.5657, -6.9641), vec(1.0000, 1.0000, 1.0000), vec4(53, 127, 0, 255), ang(0.0000, 90.0000, 0.0000), "models/Items/battery.mdl", "models/debug/debugwhite", 0)
    #[   ]#    Holos[10, array] = array(vec(0.0000, 0.0000, 2.9209), vec(1.0000, 1.0000, 1.0000), vec4(70, 127, 63, 255), ang(0.0000, 0.0000, 0.0000), "hq_dome", "models/debug/debugwhite", 0)
    #[   ]#    Holos[11, array] = array(vec(0.1576, -11.8802, -1.9218), vec(0.3000, 0.3000, 0.3000), vec4(127, 127, 127, 255), ang(0.0000, 0.0000, 0.0000), "models/Items/ammoCrate_Rockets.mdl", "models/debug/debugwhite", 0)
    
    ##########
    
    TotalHolos = Holos:count()
    if (0 > holoClipsAvailable()) {error("A holo has too many clips to spawn on this server! (Max is " + holoClipsAvailable() + ")")}
}


#You may place code here if it doesn't require all of the holograms to be spawned.


if (HolosSpawned)
{
    #Your code goes here if it needs all of the holograms to be spawned!
}
else
{
    while (LastHolo <= Holos:count() & holoCanCreate() & perf())
    {
        local Ar = Holos[LastHolo, array]
        addHolo(Ar[1, vector], Ar[2, vector], Ar[3, vector4], Ar[4, angle], Ar[5, string], Ar[6, string], Ar[7, number])
        LastHolo++
    }
    
    if (LastHolo > Holos:count())
    {
        Holos:clear()
        Clips:clear()
        HolosSpawned = 1
        E:setAlpha(0)
    }

    interval(1000)
}
