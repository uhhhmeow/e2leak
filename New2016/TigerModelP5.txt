@name Holopad Export

#####
# Holograms authored by Erza on 16/01/2016
# Exported from Holopad 09/11/2012 (BETA 6.7) by Bubbus
# Thanks to Vercas for the original E2 export template!
#
# FOR AN EXPLANATION OF THE CODE BELOW, VISIT http://code.google.com/p/holopad/wiki/NewE2ExportFormatHOWTO
##### 

#####
# Hologram spawning data
@persist [Holos Clips]:table HolosSpawned HolosStep LastHolo TotalHolos
@persist E:entity
#####


if (first() | duped())
{
    E = entity()

    function number addHolo(Pos:vector, Scale:vector, Colour:vector4, Angles:angle, Model:string, Material:string, Parent:number)
    {
        if (holoRemainingSpawns() < 1) {error("This model has too many holos to spawn! (" + TotalHolos + " holos!)"), return 0}
        
        holoCreate(LastHolo, E:toWorld(Pos), Scale, E:toWorld(Angles))
        holoModel(LastHolo, Model)
        holoMaterial(LastHolo, Material)
        holoColor(LastHolo, vec(Colour), Colour:w())

        if (Parent > 0) {holoParent(LastHolo, Parent)}
        else {holoParent(LastHolo, E)}

        local Key = LastHolo + "_"
        local I=1
        while (Clips:exists(Key + I))
        {
            holoClipEnabled(LastHolo, 1)
            local ClipArr = Clips[Key+I, array]
            holoClip(LastHolo, I, holoEntity(LastHolo):toLocal(E:toWorld(ClipArr[1, vector])), holoEntity(LastHolo):toLocalAxis(E:toWorldAxis(ClipArr[2, vector])), 0)
            I++
        }
        
        return LastHolo
    }

    ##########
    # HOLOGRAMS
    

    #[   ]#    Holos[1, array] = array(vec(21.5780, 7.7990, 51.7300), vec(0.8000, 0.8000, 0.8000), vec4(0, 127, 67, 255), ang(26.5408, 0.0000, 0.0000), "models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl", "", 0)
    #[   ]#    Holos[2, array] = array(vec(-13.3920, 14.6210, 41.1870), vec(1.3544, 0.7000, 0.7000), vec4(255, 255, 255, 255), ang(39.6654, 0.0001, 0.0000), "models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl", "", 0)
    #[   ]#    Holos[3, array] = array(vec(-11.7668, 14.6208, 24.7550), vec(1.3875, 0.7000, 0.7000), vec4(255, 255, 255, 255), ang(0.0000, 0.0000, 0.0000), "models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl", "", 0)
    #[   ]#    Holos[4, array] = array(vec(19.9680, -14.6210, 42.6520), vec(0.8000, 0.8000, 0.8000), vec4(255, 255, 255, 255), ang(0.0000, 0.0000, 0.0000), "models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl", "", 0)
    #[   ]#    Holos[5, array] = array(vec(52.1080, 17.8560, -0.7210), vec(1.3618, 0.6461, 1.0000), vec4(255, 255, 255, 255), ang(0.0000, 0.0000, 0.0000), "models/sprops/cuboids/height06/size_1/cube_6x6x6.mdl", "", 0)
    #[   ]#    Holos[6, array] = array(vec(-24.0110, -14.6210, 47.8060), vec(0.8000, 0.8000, 0.8000), vec4(255, 255, 255, 255), ang(39.6654, 0.0001, 0.0000), "models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl", "", 0)
    #[   ]#    Holos[7, array] = array(vec(-5.6363, 14.6208, 34.7564), vec(0.8000, 0.8000, 0.8000), vec4(255, 255, 255, 255), ang(39.6654, 0.0001, 0.0000), "models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl", "", 0)
    #[   ]#    Holos[8, array] = array(vec(-24.6270, -14.6210, 24.8030), vec(0.8000, 0.8000, 0.8000), vec4(255, 255, 255, 255), ang(0.0000, 0.0000, 0.0000), "models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl", "", 0)
    #[   ]#    Holos[9, array] = array(vec(21.7743, 14.5036, 18.2780), vec(1.5101, 1.0000, 1.4706), vec4(255, 255, 255, 255), ang(8.5898, -180.0000, -180.0000), "models/sprops/misc/fittings/bend_short_90_3.mdl", "", 0)
    #[   ]#    Holos[10, array] = array(vec(21.5781, 14.6208, 51.7298), vec(0.8000, 0.8000, 0.8000), vec4(0, 135, 255, 255), ang(26.5408, 0.0000, 0.0000), "models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl", "", 0)
    #[   ]#    Holos[11, array] = array(vec(-36.7470, -14.6210, 16.8870), vec(0.8000, 0.8000, 0.8000), vec4(255, 255, 255, 255), ang(-63.2274, 0.0000, 0.0000), "models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl", "", 0)
    #[ LeftFootFront ]#    Holos[12, array] = array(vec(44.4379, -14.6900, 0.0000), vec(0.7000, 0.7000, 0.7000), vec4(0, 127, 27, 255), ang(0.0000, 0.0000, 0.0000), "cube", "", 0)
    #[   ]#    Holos[13, array] = array(vec(-8.2870, -4.3530, 53.9520), vec(0.6000, 0.6000, 1.2827), vec4(255, 0, 0, 255), ang(90.0000, 19.5580, 0.0000), "models/sprops/cylinders/size_4/cylinder_9x24.mdl", "", 0)
    #[   ]#    Holos[14, array] = array(vec(52.1080, -11.3990, -0.7210), vec(1.3618, 0.6461, 1.0000), vec4(255, 255, 255, 255), ang(0.0000, 0.0000, 0.0000), "models/sprops/cuboids/height06/size_1/cube_6x6x6.mdl", "", 0)
    #[   ]#    Holos[15, array] = array(vec(-39.4840, 14.6208, 9.1749), vec(0.8000, 0.8000, 0.8000), vec4(255, 255, 255, 255), ang(-78.5493, 0.0000, 0.0000), "models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl", "", 0)
    #[ BackLeftFoot ]#    Holos[16, array] = array(vec(-39.7720, -13.4470, 0.0000), vec(0.7000, 0.7000, 0.7000), vec4(0, 127, 27, 255), ang(0.0000, 0.0000, 0.0000), "cube", "", 0)
    #[   ]#    Holos[17, array] = array(vec(30.1090, -14.6890, 17.7020), vec(0.8000, 0.8000, 0.8000), vec4(255, 255, 255, 255), ang(81.1530, 0.0000, 0.0000), "models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl", "", 0)
    #[   ]#    Holos[18, array] = array(vec(-19.9440, -14.6210, 52.7120), vec(0.8000, 0.8000, 0.8000), vec4(53, 127, 0, 255), ang(39.6654, 0.0001, 0.0000), "models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl", "", 0)
    #[   ]#    Holos[19, array] = array(vec(-13.3920, -14.6210, 41.1870), vec(1.3544, 0.7000, 0.7000), vec4(255, 255, 255, 255), ang(39.6654, 0.0001, 0.0000), "models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl", "", 0)
    #[   ]#    Holos[20, array] = array(vec(20.9110, -14.6900, 24.9500), vec(0.7000, 0.7000, 0.7000), vec4(0, 135, 255, 255), ang(0.0000, 0.0000, 0.0000), "hq_icosphere", "", 0)
    #[   ]#    Holos[21, array] = array(vec(-2.8842, 14.6208, 30.8019), vec(0.8000, 0.8000, 0.8000), vec4(255, 255, 255, 255), ang(0.0000, 0.0000, 0.0000), "models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl", "", 0)
    #[   ]#    Holos[22, array] = array(vec(21.5780, -14.6210, 51.7300), vec(0.8000, 0.8000, 0.8000), vec4(0, 135, 255, 255), ang(26.5408, 0.0000, 0.0000), "models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl", "", 0)
    #[   ]#    Holos[23, array] = array(vec(-5.6360, -14.6210, 34.7560), vec(0.8000, 0.8000, 0.8000), vec4(255, 255, 255, 255), ang(39.6654, 0.0001, 0.0000), "models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl", "", 0)
    #[   ]#    Holos[24, array] = array(vec(-2.8840, -14.6210, 30.8020), vec(0.8000, 0.8000, 0.8000), vec4(255, 255, 255, 255), ang(0.0000, 0.0000, 0.0000), "models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl", "", 0)
    #[   ]#    Holos[25, array] = array(vec(-39.4840, -14.6210, 9.1750), vec(0.8000, 0.8000, 0.8000), vec4(255, 255, 255, 255), ang(-78.5493, 0.0000, 0.0000), "models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl", "", 0)
    #[   ]#    Holos[26, array] = array(vec(-24.0110, 14.6210, 47.8060), vec(0.8000, 0.8000, 0.8000), vec4(255, 255, 255, 255), ang(39.6654, 0.0001, 0.0000), "models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl", "", 0)
    #[   ]#    Holos[27, array] = array(vec(-31.2080, -14.6210, 21.7980), vec(0.8000, 0.8000, 0.8000), vec4(255, 255, 255, 255), ang(-30.5899, 0.0000, 0.0000), "models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl", "", 0)
    #[   ]#    Holos[28, array] = array(vec(19.9678, 14.6208, 42.6516), vec(0.8000, 0.8000, 0.8000), vec4(255, 255, 255, 255), ang(0.0000, 0.0000, 0.0000), "models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl", "", 0)
    #[   ]#    Holos[29, array] = array(vec(-22.3370, 8.4930, 54.6960), vec(0.8000, 0.8000, 0.8000), vec4(255, 127, 127, 255), ang(39.6654, 0.0001, 0.0000), "models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl", "", 0)
    #[   ]#    Holos[30, array] = array(vec(-2.8840, -14.6210, 24.7550), vec(0.8000, 0.8000, 0.8000), vec4(127, 255, 234, 255), ang(0.0000, 0.0000, 0.0000), "models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl", "", 0)
    #[   ]#    Holos[31, array] = array(vec(43.4666, 11.3994, 1.8282), vec(2.3576, 0.6461, 1.0000), vec4(255, 255, 255, 255), ang(25.2097, 0.0000, 0.0000), "models/sprops/cuboids/height06/size_1/cube_6x6x6.mdl", "", 0)
    #[   ]#    Holos[32, array] = array(vec(-36.7473, 14.6208, 16.8868), vec(0.8000, 0.8000, 0.8000), vec4(255, 255, 255, 255), ang(-63.2274, 0.0000, 0.0000), "models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl", "", 0)
    #[   ]#    Holos[33, array] = array(vec(-11.7670, -14.6210, 24.7550), vec(1.3875, 0.7000, 0.7000), vec4(255, 255, 255, 255), ang(0.0000, 0.0000, 0.0000), "models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl", "", 0)
    #[   ]#    Holos[34, array] = array(vec(20.6839, 14.6208, 25.2609), vec(0.8000, 0.8000, 0.8000), vec4(127, 0, 0, 255), ang(81.1530, 0.0000, 0.0000), "models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl", "", 0)
    #[   ]#    Holos[35, array] = array(vec(52.1080, -17.8560, -0.7210), vec(1.3618, 0.6461, 1.0000), vec4(255, 255, 255, 255), ang(0.0000, 0.0000, 0.0000), "models/sprops/cuboids/height06/size_1/cube_6x6x6.mdl", "", 0)
    #[   ]#    Holos[36, array] = array(vec(-24.6272, 14.6208, 24.8028), vec(0.8000, 0.8000, 0.8000), vec4(255, 255, 255, 255), ang(0.0000, 0.0000, 0.0000), "models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl", "", 0)
    #[   ]#    Holos[37, array] = array(vec(34.8892, 0.0000, 47.6058), vec(1.3339, 1.2293, 0.4672), vec4(255, 242, 0, 255), ang(22.5654, 0.0000, 0.0000), "models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl", "", 0)
    #[ RightFootFront ]#    Holos[38, array] = array(vec(44.4380, 14.6900, 0.0000), vec(0.7000, 0.7000, 0.7000), vec4(0, 127, 27, 255), ang(0.0000, 0.0000, 0.0000), "cube", "", 0)
    #[   ]#    Holos[39, array] = array(vec(43.4666, 17.8558, 1.8282), vec(2.3576, 0.6461, 1.0000), vec4(255, 255, 255, 255), ang(25.2097, 0.0000, 0.0000), "models/sprops/cuboids/height06/size_1/cube_6x6x6.mdl", "", 0)
    #[   ]#    Holos[40, array] = array(vec(20.6840, -14.6210, 25.2610), vec(0.8000, 0.8000, 0.8000), vec4(127, 0, 0, 255), ang(81.1530, 0.0000, 0.0000), "models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl", "", 0)
    #[   ]#    Holos[41, array] = array(vec(-39.8980, -14.6210, 7.5160), vec(0.8000, 0.8000, 0.8000), vec4(255, 255, 255, 255), ang(-78.5493, 0.0000, 0.0000), "models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl", "", 0)
    #[   ]#    Holos[42, array] = array(vec(43.8881, 0.0000, 57.0680), vec(1.3339, 1.2293, 0.4672), vec4(255, 242, 0, 255), ang(34.8912, 0.0000, 0.0000), "models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl", "", 0)
    #[   ]#    Holos[43, array] = array(vec(34.3270, 9.3091, 55.8116), vec(0.8000, 0.4350, 0.8000), vec4(255, 242, 0, 255), ang(21.5145, 0.0000, 0.0000), "models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl", "", 0)
    #[   ]#    Holos[44, array] = array(vec(28.1109, 9.3091, 58.2620), vec(0.8000, 0.4350, 0.8000), vec4(255, 242, 0, 255), ang(21.5145, 0.0000, 0.0000), "models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl", "", 0)
    #[   ]#    Holos[45, array] = array(vec(52.1077, 11.3994, -0.7214), vec(1.3618, 0.6461, 1.0000), vec4(255, 255, 255, 255), ang(0.0000, 0.0000, 0.0000), "models/sprops/cuboids/height06/size_1/cube_6x6x6.mdl", "", 0)
    #[   ]#    Holos[46, array] = array(vec(31.0950, -14.6230, 10.5360), vec(1.0000, 1.0000, 1.0000), vec4(255, 255, 255, 255), ang(0.0000, 0.0000, 0.0000), "models/sprops/cylinders/size_2/cylinder_3x6.mdl", "", 0)
    #[   ]#    Holos[47, array] = array(vec(28.1110, -9.3090, 58.2620), vec(0.8000, 0.4350, 0.8000), vec4(255, 242, 0, 255), ang(21.5145, 0.0000, 0.0000), "models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl", "", 0)
    #[   ]#    Holos[48, array] = array(vec(19.9678, 14.6208, 33.5409), vec(0.8000, 0.8000, 0.8000), vec4(255, 255, 255, 255), ang(0.0000, 0.0000, 0.0000), "models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl", "", 0)
    #[   ]#    Holos[49, array] = array(vec(27.9831, 0.0000, 50.4757), vec(1.3339, 1.7973, 0.4672), vec4(255, 242, 0, 255), ang(22.5654, 0.0000, 0.0000), "models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl", "", 0)
    #[   ]#    Holos[50, array] = array(vec(34.3270, -9.3090, 55.8120), vec(0.8000, 0.4350, 0.8000), vec4(255, 242, 0, 255), ang(21.5145, 0.0000, 0.0000), "models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl", "", 0)
    #[   ]#    Holos[51, array] = array(vec(33.2998, 0.0000, 63.2701), vec(1.3339, 1.7973, 0.4672), vec4(255, 242, 0, 255), ang(22.5654, 0.0000, 0.0000), "models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl", "", 0)
    #[   ]#    Holos[52, array] = array(vec(12.1970, -11.8790, 53.8750), vec(1.7164, 1.4145, 1.7537), vec4(255, 248, 127, 255), ang(0.0000, -180.0000, -86.3280), "models/sprops/misc/fittings/bend_long_90_3.mdl", "", 0)
    #[   ]#    Holos[53, array] = array(vec(38.8090, 14.6890, 6.3230), vec(1.0000, 1.0000, 1.0000), vec4(255, 255, 255, 255), ang(36.3902, 0.0000, 0.0000), "models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl", "", 0)
    #[   ]#    Holos[54, array] = array(vec(12.1973, 11.8792, 53.8749), vec(1.7164, 1.4145, 1.7537), vec4(255, 248, 127, 255), ang(-0.0000, 180.0000, 86.3283), "models/sprops/misc/fittings/bend_long_90_3.mdl", "", 0)
    #[   ]#    Holos[55, array] = array(vec(19.9678, 14.6208, 44.6964), vec(0.8000, 0.8000, 0.8000), vec4(255, 255, 255, 255), ang(0.0000, 0.0000, 0.0000), "models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl", "", 0)
    #[   ]#    Holos[56, array] = array(vec(35.4349, 14.6890, 8.8097), vec(0.8000, 0.8000, 0.8000), vec4(255, 255, 255, 255), ang(36.3902, 0.0000, 0.0000), "models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl", "", 0)
    #[   ]#    Holos[57, array] = array(vec(-2.8840, 14.6210, 24.7550), vec(0.8000, 0.8000, 0.8000), vec4(127, 255, 234, 255), ang(0.0000, 0.0000, 0.0000), "models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl", "", 0)
    #[   ]#    Holos[58, array] = array(vec(43.4670, -17.8560, 1.8280), vec(2.3576, 0.6461, 1.0000), vec4(255, 255, 255, 255), ang(25.2097, 0.0000, 0.0000), "models/sprops/cuboids/height06/size_1/cube_6x6x6.mdl", "", 0)
    #[ BackRightFoot ]#    Holos[59, array] = array(vec(-39.7718, 13.4470, 0.0000), vec(0.7000, 0.7000, 0.7000), vec4(0, 127, 27, 255), ang(0.0000, 0.0000, 0.0000), "cube", "", 0)
    #[   ]#    Holos[60, array] = array(vec(-20.6119, 14.6208, 24.7550), vec(0.8000, 0.8000, 0.8000), vec4(0, 135, 255, 255), ang(0.0000, 0.0000, 0.0000), "models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl", "", 0)
    #[   ]#    Holos[61, array] = array(vec(20.9110, 14.6900, 24.9500), vec(0.7000, 0.7000, 0.7000), vec4(0, 135, 255, 255), ang(0.0000, 0.0000, 0.0000), "hq_icosphere", "", 0)
    #[   ]#    Holos[62, array] = array(vec(-20.6120, -14.6210, 24.7550), vec(0.8000, 0.8000, 0.8000), vec4(0, 135, 255, 255), ang(0.0000, 0.0000, 0.0000), "models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl", "", 0)
    #[   ]#    Holos[63, array] = array(vec(21.5780, -7.7989, 51.7300), vec(0.8000, 0.8000, 0.8000), vec4(0, 127, 67, 255), ang(26.5408, 0.0000, 0.0000), "models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl", "", 0)
    #[   ]#    Holos[64, array] = array(vec(27.6330, -14.6690, 21.5540), vec(0.8000, 0.8000, 0.8000), vec4(255, 255, 255, 255), ang(81.1530, 0.0000, 0.0000), "models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl", "", 0)
    #[   ]#    Holos[65, array] = array(vec(-32.4530, -14.6210, 21.4240), vec(0.8000, 0.8000, 0.8000), vec4(255, 255, 255, 255), ang(-30.5899, 0.0000, 0.0000), "models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl", "", 0)
    #[   ]#    Holos[66, array] = array(vec(35.4350, -14.6890, 8.8100), vec(0.8000, 0.8000, 0.8000), vec4(255, 255, 255, 255), ang(36.3902, 0.0000, 0.0000), "models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl", "", 0)
    #[ Base/Heart ]#    Holos[67, array] = array(vec(8.6515, 0.0000, 51.6487), vec(1.0000, 1.0000, 1.0000), vec4(127, 0, 0, 255), ang(0.0000, 0.0000, 0.0000), "cube", "", 0)
    #[   ]#    Holos[68, array] = array(vec(43.4670, -11.3990, 1.8280), vec(2.3576, 0.6461, 1.0000), vec4(255, 255, 255, 255), ang(25.2097, 0.0000, 0.0000), "models/sprops/cuboids/height06/size_1/cube_6x6x6.mdl", "", 0)
    #[   ]#    Holos[69, array] = array(vec(-39.8983, 14.6208, 7.5163), vec(0.8000, 0.8000, 0.8000), vec4(255, 255, 255, 255), ang(-78.5493, 0.0000, 0.0000), "models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl", "", 0)
    #[   ]#    Holos[70, array] = array(vec(30.1090, 14.6890, 17.7024), vec(0.8000, 0.8000, 0.8000), vec4(255, 255, 255, 255), ang(81.1530, 0.0000, 0.0000), "models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl", "", 0)
    #[   ]#    Holos[71, array] = array(vec(19.9680, -14.6210, 33.5410), vec(0.8000, 0.8000, 0.8000), vec4(255, 255, 255, 255), ang(0.0000, 0.0000, 0.0000), "models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl", "", 0)
    #[   ]#    Holos[72, array] = array(vec(38.8092, -14.6888, 6.3227), vec(1.0000, 1.0000, 1.0000), vec4(255, 255, 255, 255), ang(36.3902, 0.0000, 0.0000), "models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl", "", 0)
    #[   ]#    Holos[73, array] = array(vec(-8.2867, 4.3528, 53.9517), vec(0.6000, 0.6000, 1.2827), vec4(255, 0, 0, 255), ang(90.0000, -19.5582, 0.0000), "models/sprops/cylinders/size_4/cylinder_9x24.mdl", "", 0)
    #[   ]#    Holos[74, array] = array(vec(27.6331, 14.6691, 21.5541), vec(0.8000, 0.8000, 0.8000), vec4(255, 255, 255, 255), ang(81.1530, 0.0000, 0.0000), "models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl", "", 0)
    #[   ]#    Holos[75, array] = array(vec(19.9680, -14.6210, 44.6960), vec(0.8000, 0.8000, 0.8000), vec4(255, 255, 255, 255), ang(0.0000, 0.0000, 0.0000), "models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl", "", 0)
    #[   ]#    Holos[76, array] = array(vec(31.0949, 14.6231, 10.5365), vec(1.0000, 1.0000, 1.0000), vec4(255, 255, 255, 255), ang(0.0000, 0.0000, 0.0000), "models/sprops/cylinders/size_2/cylinder_3x6.mdl", "", 0)
    #[   ]#    Holos[77, array] = array(vec(-19.9437, 14.6207, 52.7119), vec(0.8000, 0.8000, 0.8000), vec4(53, 127, 0, 255), ang(39.6654, 0.0001, 0.0000), "models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl", "", 0)
    #[   ]#    Holos[78, array] = array(vec(21.7740, -14.5040, 18.2780), vec(1.5101, 1.0000, 1.4706), vec4(255, 255, 255, 255), ang(8.5898, -180.0000, -180.0000), "models/sprops/misc/fittings/bend_short_90_3.mdl", "", 0)
    #[   ]#    Holos[79, array] = array(vec(-32.4528, 14.6210, 21.4244), vec(0.8000, 0.8000, 0.8000), vec4(255, 255, 255, 255), ang(-30.5899, 0.0000, 0.0000), "models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl", "", 0)
    #[   ]#    Holos[80, array] = array(vec(-31.2080, 14.6210, 21.7980), vec(0.8000, 0.8000, 0.8000), vec4(255, 255, 255, 255), ang(-30.5899, 0.0000, 0.0000), "models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl", "", 0)
    #[   ]#    Holos[81, array] = array(vec(-22.3370, -8.4930, 54.6960), vec(0.8000, 0.8000, 0.8000), vec4(255, 127, 127, 255), ang(39.6654, 0.0001, 0.0000), "models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl", "", 0)
    #[   ]#    Holos[82, array] = array(vec(-20.9110, -17.6590, 53.4210), vec(0.7000, 0.7000, 0.7000), vec4(0, 127, 108, 255), ang(0.0000, 0.0000, 0.0000), "hq_icosphere", "", 0)
    
    ##########
    
    TotalHolos = Holos:count()
    if (0 > holoClipsAvailable()) {error("A holo has too many clips to spawn on this server! (Max is " + holoClipsAvailable() + ")")}
}


#You may place code here if it doesn't require all of the holograms to be spawned.


if (HolosSpawned)
{
    #Your code goes here if it needs all of the holograms to be spawned!
}
else
{
    while (LastHolo <= Holos:count() & holoCanCreate() & perf())
    {
        local Ar = Holos[LastHolo, array]
        addHolo(Ar[1, vector], Ar[2, vector], Ar[3, vector4], Ar[4, angle], Ar[5, string], Ar[6, string], Ar[7, number])
        LastHolo++
    }
    
    if (LastHolo > Holos:count())
    {
        Holos:clear()
        Clips:clear()
        HolosSpawned = 1
        E:setAlpha(0)
    }

    interval(1000)
}
