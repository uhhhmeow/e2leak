@name Guided ordinance
@inputs 
@outputs T:array NPC:array
@persist T:array Start End Scan
#@persist Dist1:vector Dist2:vector
@trigger 
@model models/props_junk/PopCan01a.mdl
interval(20)
if(ops()>2000){exit()}
Self=owner()
Gren="npc_grenade_frag" #Life time 2.5 seconds
Ball="prop_combine_ball" #life time 4.5 seconds
runOnChat(1)
Chat=owner():lastSaid()
Com="GO"
Homing="npc"
Aimed="aim"
Play="play"

COM=Chat:find(Com)
HOM=Chat:find(Homing)
AIMED=Chat:find(Aimed)
PLAY=Chat:find(Play)

if(COM&chatClk()){
    if(HOM){
        print("NPC homing mode engaged")
        Scan=1
    }
    if(AIMED){
        print("player guided mode engaged")
        Scan=0
    }
    if(PLAY){
        print("player homing mode engaged")
        Scan=2
    }
}

if(Scan==1){
    findInSphere(Self:pos(),30000)
    findSortByDistance(Self:pos())
    findIncludeClass("npc_")
    findIncludeClass(Ball)
    findIncludeClass(Gren)
    findExcludeClass("vort")
    findExcludeClass("gman")
    #findExcludeClass("roller")
    
    T=findToArray()
    NPC=array()
    for(C=1,T:count()){
        TT=T[C,entity] #Target Type
        if(TT:isNPC()){
            NPC:pushEntity(T[C,entity])
            T:remove(C)
        }
        
    }
    if(!NPC[1,entity]:isAlive()){
        NPC:remove(1)
    }
    
    XYZ=NPC[1,entity]:pos()#+vec(0,0,NPC[1,entity]:height()/4)
}elseif(!Scan){XYZ=Self:aimPos()
    findInCone(Self:shootPos(),Self:eye(),30000,45)
    findIncludeClass(Ball)
    findIncludeClass(Gren)
    findExcludeClass("prop_physics")  #permits function even if too many props exist
    findSortByDistance(Self:pos())
    T=findToArray()
}
if(Scan==2){
    findInCone(Self:shootPos(),Self:eye(),30000,45)
    findIncludeClass(Ball)
    findIncludeClass(Gren)
    findIncludeClass("player")
    findExcludeClass("prop_physics")
    findSortByDistance(Self:pos())
    T=findToArray()
    
    NPC=array()
    for(C=1,T:count()){
        TT=T[C,entity] #Target Type
        if(TT:isPlayer()){
            NPC:pushEntity(T[C,entity])
            T:remove(C)
        }
        
    }
    if(!NPC[1,entity]:isAlive()){
        NPC:remove(1)
    }
    
    XYZ=NPC[1,entity]:pos()
}
for(C=1,T:count()){
    E=T[C,entity]
    Speed=2000
    Off=vec()
    
    if(E:type()==Gren){Speed=20 Off=vec(0,0,20)}
    if(E:type()==Ball){Speed=1000 Off=vec(0,0,30)}
    E:applyForce(Speed*(XYZ+Off-E:pos())-E:vel()*E:mass())
    #XYZ=Self:aimPos()
    #E:applyForce(E:mass()*((XYZ-E:pos())/0.015-E:vel()))
}



#Combine Ball
#Life time: 4.3800048828125
#Distance: 16257.577116317
#Velocity: 3711.7714594595

#Grenade
#Life time: 2.5800170898438
#Distance: 16311.91020059
#Velocity: 6322.4039347652

#TC=T:count()
#if(changed(TC)){
#    if(TC){
#        Start=curtime()
#        Dist1=T[1,entity]:pos()
#    }else{
#        End=curtime()
#        Life=End-Start
#        Dist2=T[1,entity]:pos()
#        Dist=Dist1:distance(Dist2)
#        Vel=Dist/Life
#        print("Life time: "+Life)
#        print("Distance: "+Dist)
#        print("Velocity: "+Vel)
#    }
#}

