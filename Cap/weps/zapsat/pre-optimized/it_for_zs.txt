@name Interactive Targeting
#@inputs N
#@outputs Orders:string
@outputs Targets:array Passoff:entity Formation Self On ZSC:table Kill
#@outputs TID T1 T2 T3 
@persist Players:array Chat1:string Last Dist:array Passoff:entity Targets:array
@persist Play:array A B C D Self Net1 Net0 Network Command:string
@persist Once MultiCom:array On
Time = 500
interval(Time)
gSetGroup("ZapSat")
if(duped()){reset()}
if (first()) {Passoff = entity():owner() Network=0 Self=0}
entity():setColor(0,140,255)
Box = entity():isConstrainedTo()
if (Network) {   
    if (Self==1) {Command = "Zap1" Box:setColor(255,0,0)
}
if (Self==2) {Command = "Zap2" Box:setColor(0,255,0)
  
}
if (Self==3) {Command = "Zap3" Box:setColor(0,0,255)
}
if (Self==4) {Command = "Zap4" Box:setColor(255,255,0)
}
elseif (Self==0) {Command = "Zap"  Box:setColor(255,255,255)
Orders = ""
}

}
Menu = "menu"
Attack = "get" 
List = "list"
P1 = "1"
P2 = "2"
P3 = "3"
P4 = "4"
P5 = "5"
P6 = "6"
P7 = "7"
P8 = "8"
P9 = "9"
P10 = "0"
PKC = "PK"
NKC = "NK"
Pass = "pass"
Back = "back"
ModeQ = "mode"
S = " "
Wide = "zw" 
Form="form"
Tight = "zt"
NetOn = "linkup"
NetOff = "unlink"
CommandMaster = "Zap0"
PKMode="Player Kill Mode Enabled"
NKMode="NPC Kill Mode Enabled"
Cs=Command+": "
Orb = "Now Orbiting: "
Breaker="<"
Power="power"
Suicide="destruct"

ChatO = Passoff:lastSaid()
Chat = entity():owner():lastSaid()
K = curtime() - entity():owner():lastSaidWhen() < (Time*1)/1000
if (K) {Once=1}
C = Chat:find(Command)
M = Chat:find(Menu)
A = Chat:find(Attack)
L = Chat:find(List)
C1 = Chat:find(P1)
C2 = Chat:find(P2)
C3 = Chat:find(P3)
C4 = Chat:find(P4)
C5 = Chat:find(P5)
C6 = Chat:find(P6)
C7 = Chat:find(P7)
C8 = Chat:find(P8)
C9 = Chat:find(P9)
C10 = Chat:find(P10)
PK = Chat:find(PKC)
NK = Chat:find(NKC)
if(duped()|first()){NK=1}
P = Chat:find(Pass)
BK = Chat:find(Back)
Mode = Chat:find(ModeQ)
CM = Chat:find(CommandMaster)
ModeT = ChatO:find(Tight)
ModeW = ChatO:find(Wide)
Form1=Chat:find(Form)
Net1 = Chat:find(NetOn)
Net0 = Chat:find(NetOff)
Powered = Chat:find(Power)
Break = Chat:find(Breaker)
Suicided = Chat:find(Suicide)
if (K) {
if (ModeT) {Formation = 1}
if (ModeW) {Formation = 0}
if (Net1&C&Once) {Network = 1 print(Cs+"Networking Enabled") Once=0 }
if (Net0&CM&Once) {Network = 0 print(Cs+"Networking Disabled") Once=0}
if (CM&Break) {MultiCom = Chat:explode(Breaker)}
if (MultiCom:count()!=0&Self==1) {Orders=MultiCom:string(2)}
if (MultiCom:count()!=0&Self==2) {Orders=MultiCom:string(3)}
if (MultiCom:count()!=0&Self==3) {Orders=MultiCom:string(4)}
if (MultiCom:count()!=0&Self==4) {Orders=MultiCom:string(5)}
if (Break&MultiCom:count()) {
C1 = Orders:find(P1)
C2 = Orders:find(P2)
C3 = Orders:find(P3)
C4 = Orders:find(P4)
C5 = Orders:find(P5)
C6 = Orders:find(P6)
C7 = Orders:find(P7)
C8 = Orders:find(P8)
C9 = Orders:find(P9)
C10 = Orders:find(P10)
}
}





Origin = entity():owner():pos()
findInSphere(Origin,1000000)
findSortByDistance(Origin)
findIncludeClass("player")
Play = findToArray()
if ((C|CM)&K){
if (Mode&PK) {print(Cs+PKMode)}
if (Mode&NK) {print(Cs+NKMode)}
if (PK) {print(Cs+PKMode)}
if (NK) {print(Cs+NKMode)}
if (Powered&C1){On=1 print(Cs+"Powering Up")}
if (Powered&C10){On=0 print(Cs+"Powering Down")}
if (Suicided&C1){print(Cs+"Self Destructing") selfDestructAll()}
if (Form1&C1){Formation=1 print(Cs+"Tight Formation")}
if (Form1&C10){Formation=0 print(Cs+"Wide Formation")}
}
if (Network) {
An = gGetNum("An")
Bn = gGetNum("Bn")
Cn = gGetNum("Cn")
Dn = gGetNum("Dn")
if (Network & Self==0 & An==0) {Self = 1 An = 1}
if (Network & Self==0 & Bn==0) {Self = 2 Bn = 2}
if (Network & Self==0 & Cn==0) {Self = 3 Cn = 3}
if (Network & Self==0 & Dn==0) {Self = 4 Dn = 4}
gSetNum("An",An)
gSetNum("Bn",Bn)
gSetNum("Cn",Cn)
gSetNum("Dn",Dn)
} else {
Self=An=Bn=Cn=Dn=0
gDeleteNum("An")
gDeleteNum("Bn")
gDeleteNum("Cn")
gDeleteNum("Dn")
}



if ((CM|C)&M&K) { 
Players = Play:clone()


Concat = array()
if (Players:count()>0) {
Concat:setString(0,Cs)
Concat:setString(1,"Targets: ")
Concat:setString(2,P1+S)
Concat:setString(3,Players:entity(1):name())
}
if (Players:count()>1) {
Concat:setString(4,S+P2+S)
Concat:setString(5,Players:entity(2):name())
}
if (Players:count()>2) {
Concat:setString(6,S+P3+S)
Concat:setString(7,Players:entity(3):name())
}
if (Players:count()>3) {
Concat:setString(8,S+P4+S)
Concat:setString(9,Players:entity(4):name())
}
if (Players:count()>4) {
Concat:setString(10,S+P5+S)
Concat:setString(11,Players:entity(5):name())
}
if (Players:count()>5) {
Concat:setString(12,S+P6+S)
Concat:setString(13,Players:entity(6):name())
}
if (Players:count()>6) {
Concat:setString(14,S+P7+S)
Concat:setString(15,Players:entity(7):name())
}
if (Players:count()>7) {
Concat:setString(16,S+P8+S)
Concat:setString(17,Players:entity(8):name())
}
if (Players:count()>8) {
Concat:setString(18,S+P9+S)
Concat:setString(19,Players:entity(9):name())
}
if (Players:count()>9) {
Concat:setString(20,S+P10+S)
Concat:setString(21,Players:entity(10):name())
}
}

if ((CM|C)&M&K) {print(Cs+Concat:concat())}
if ((CM|C)&P&K) {

if (BK) {Passoff = entity():owner()} else {
if (C1 & !(C2|C3|C4|C5|C6|C7|C8|C9|C10)) {
Passoff = Players:entity(1) 
if (Once) {Once=0 print(Cs+Orb+Players:entity(1):name())}}

if (C2 & !(C1|C3|C4|C5|C6|C7|C8|C9|C10)) {
Passoff = Players:entity(2) 
if (Once) {Once=0 print(Cs+Orb+Players:entity(2):name())}}

if (C3 & !(C2|C1|C4|C5|C6|C7|C8|C9|C10)) {
Passoff = Players:entity(3) 
if (Once) {Once=0 print(Cs+Orb+Players:entity(3):name())}}

if (C4 & !(C2|C3|C1|C5|C6|C7|C8|C9|C10)) {
Passoff = Players:entity(4) 
if (Once) {Once=0 print(Cs+Orb+Players:entity(4):name())}}

if (C5 & !(C2|C3|C4|C1|C6|C7|C8|C9|C10)) {
Passoff = Players:entity(5) 
if (Once) {Once=0 print(Cs+Orb+Players:entity(5):name())}}

if (C6 & !(C2|C3|C4|C5|C1|C7|C8|C9|C10)) {
Passoff = Players:entity(6) 
if (Once) {Once=0 print(Cs+Orb+Players:entity(6):name())}}

if (C7 & !(C2|C3|C4|C5|C6|C1|C8|C9|C10)) {
Passoff = Players:entity(7) 
if (Once) {Once=0 print(Cs+Orb+Players:entity(7):name())}}

if (C8 & !(C1|C3|C4|C5|C6|C7|C1|C9|C10)) {
Passoff = Players:entity(8) 
if (Once) {Once=0 print(Cs+Orb+Players:entity(8):name())}}

if (C9 & !(C2|C1|C4|C5|C6|C7|C8|C1|C10)) {
Passoff = Players:entity(9) 
if (Once) {Once=0 print(Cs+Orb+Players:entity(9):name())}}

if (C10 & !(C2|C3|C1|C5|C6|C7|C8|C9|C1)) {
Passoff = Players:entity(10) 
if (Once) {Once=0 print(Cs+Orb+Players:entity(10):name())}}
}
}
if ((CM|C)& A & K) {
Targets = array()
E = 1
if (C1) {Targets:setEntity(E,Players:entity(1)) E+=1}
if (C2) {Targets:setEntity(E,Players:entity(2)) E+=1}
if (C3) {Targets:setEntity(E,Players:entity(3)) E+=1}
if (C4) {Targets:setEntity(E,Players:entity(4)) E+=1}
if (C5) {Targets:setEntity(E,Players:entity(5)) E+=1}
if (C6) {Targets:setEntity(E,Players:entity(6)) E+=1}
if (C7) {Targets:setEntity(E,Players:entity(7)) E+=1}
if (C8) {Targets:setEntity(E,Players:entity(8)) E+=1}
if (C9) {Targets:setEntity(E,Players:entity(9)) E+=1}
if (C10) {Targets:setEntity(E,Players:entity(10)) E+=1}
}
#TID = Targets:entity(N):id()
if (Targets:count()==0) {Error1 = Cs+"No Targets Spesified"} else {Error1 = ""}
if ((CM|C)& L & K) {print(Cs+Targets:concat()+Error1)}

Count = Targets:count()
D1=D2=D3=D4=D5=D6=D7=D8=D9=D10=1000000
Dist = array()
if (Count>0) {D1 = Targets:entity(1):pos():distance(Origin)} 
if (Count>1) {D2 = Targets:entity(2):pos():distance(Origin)} 
if (Count>2) {D3 = Targets:entity(3):pos():distance(Origin)} 
if (Count>3) {D4 = Targets:entity(4):pos():distance(Origin)} 
if (Count>4) {D5 = Targets:entity(5):pos():distance(Origin)} 
if (Count>5) {D6 = Targets:entity(6):pos():distance(Origin)} 
if (Count>6) {D7 = Targets:entity(7):pos():distance(Origin)} 
if (Count>7) {D8 = Targets:entity(8):pos():distance(Origin)} 
if (Count>8) {D9 = Targets:entity(9):pos():distance(Origin)} 
if (Count>9) {D10 = Targets:entity(10):pos():distance(Origin)} 
Dist:setNumber(1,D1)
Dist:setNumber(2,D2)
Dist:setNumber(3,D3)
Dist:setNumber(4,D4)
Dist:setNumber(5,D5)
Dist:setNumber(6,D6)
Dist:setNumber(7,D7)
Dist:setNumber(8,D8)
Dist:setNumber(9,D9)
Dist:setNumber(10,D10)
K1 = Dist:minIndex()
Dist:remove(K1)
K2 = Dist:minIndex()
Dist:remove(K2)
K3 = Dist:minIndex()

T1 = Targets:entity(K1):id()
T2 = Targets:entity(K2):id()
T3 = Targets:entity(K3):id()


if ((CM|C)& K & PK) {Kill=1}
if ((CM|C)& K & NK) {Kill=0}

Dis=0
ZSC:setNumber("Formation",Formation)
ZSC:setNumber("On",On)
ZSC:setEntity("Pass",Passoff)
ZSC:setNumber("Disposable",Dis)





