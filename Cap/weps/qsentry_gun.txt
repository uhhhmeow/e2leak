@name Quaternian Sentry Gun
@inputs Gun:wirelink
@outputs
@persist 
@trigger 

runOnTick(1)
findInSphere(entity():pos(),100)
findSortByDistance(entity():pos())
findIncludeClass("player")
P=findToArray()
if(P[1,entity]:pos():distance(entity():pos())<50){O=P[1,entity]}else{O=noentity()}

Offset=vec(0,0,105)

#O=owner()
G=Gun:entity()
#rangerFilter(G)
rangerHitEntities(0)
Range=1000
#Aim=rangerOffset(Range,G:pos(),O:eye()):pos()+(O:pos()-G:pos())
Aim=rangerOffset(15000,O:shootPos(),O:eye()):pos()
#G=entity():isConstrainedTo()
if(O:keyUse()){Gun["Fire",number]=1}else{Gun["Fire",number]=0}
Dir = G:pos()-(Aim)
Q = quat(Dir:toAngle())*qRotation(vec(0,0,1),180)
G:applyTorque((150*G:toLocal(rotationVector(Q/quat(G))+G:pos()) - 12*G:angVelVector())*G:inertia())
XYZ=entity():pos()+Offset
Speed=(G:mass()/4)*100
G:applyForce(Speed*(XYZ-G:pos())-G:vel()*G:mass())
