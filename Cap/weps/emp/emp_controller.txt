@name Explosive Multi Puck Controller
@inputs On T1:array
@outputs Mode Launch Timer Target Count Alive Explode
@outputs TargID TargID2 TargID3
@persist Active Explode Zenith1 Kill Regen Count Ping1 Ping2
interval(20)
if (On) {timer("Delay",1000)}
Timer = clk("Delay")
T = T1:entity(1)
T2 = T1:entity(2)
T3 = T1:entity(3)
gSetGroup("EMP")
if (!On) {gDeleteAll()}
Active = On
Ping1 = !Ping1
Ping2 = !Ping1
gSetNum("Ping1",Ping1)
gSetNum("Ping2",Ping2)
Pong = gDeleteNum("Pong")
if (Pong) {Alive = 1} else {Alive = 0}
Count = gGetNum("Count")
setColor(255,128,0)
Mode = 1
TargID = T:id()
TargID2 = T2:id()
TargID3 = T3:id()
if (TargID==0 & TargID2==0 & TargID3==0) {Target = 0} else {Target = 1}
if (TargID3==0 & TargID2!=0) {TargID3 = TargID2}
if (TargID3==0 & TargID2==0 & TargID!=0) {TargID3 = TargID TargID2 = TargID}
BaseID = entity():id()

#if (InRange) {Kill = 0 Zenith1 = 1}
R1 = Regen==3
Ready = Regen==3 & ~Regen
R = ~Regen
if (Count > 0 | !Target) {Stop = 0} else {Stop = 1}

if (Regen==3 & Timer & Target & Stop) {Launch = 1 Count += 1} else {Launch = 0}

Regen += Timer
if (Regen==4) {Regen = 0}

if (Count!=0 & !Target & On) {Explode = 1} else {Explode = 0}

if (!Alive) {gSetNum("Count",0)}
gSetNum("Active",Active)
gSetNum("Explode1",Explode)
gSetNum("Zenith1",Zenith1)
gSetNum("Kill",Kill)
gSetNum("BaseEnt",BaseID)
gSetNum("TargEnt",TargID)
gSetNum("TargEnt2",TargID2)
gSetNum("TargEnt3",TargID3)
gSetNum("MinDist",100)
