@name Homing Puck
@inputs TargetID
@outputs Vecthruster:vector Mul Explode
@persist TargetID
Pos = entity(TargetID):pos()
Self = entity():pos()
Vecthruster = Pos - Self
Distance = entity(TargetID):pos():distance(entity():pos())
if (TargetID==0) {Distance = 0}
Mul = Distance  
