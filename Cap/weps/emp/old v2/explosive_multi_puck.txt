@name Explosive Multi Puck
@inputs Ents DataR Disable
@outputs Mul F:vector Explode Alive Zenith Home Event
@persist Explode InRange Explode1 Setup Event 
interval(20)
Ents = floor(Ents)
B1 = floor(Ents / 2^0) % 2
B2 = floor(Ents / 2^1) % 2
B3 = floor(Ents / 2^2) % 2
B4 = floor(Ents / 2^3) % 2
B5 = floor(Ents / 2^4) % 2
B6 = floor(Ents / 2^5) % 2
B7 = floor(Ents / 2^6) % 2
B8 = floor(Ents / 2^7) % 2
B9 = floor(Ents / 2^8) % 2
B10 = floor(Ents / 2^9) % 2
B11 = floor(Ents / 2^10) % 2
B12 = floor(Ents / 2^11) % 2
B13 = floor(Ents / 2^12) % 2
B14 = floor(Ents / 2^13) % 2
B15 = floor(Ents / 2^14) % 2
B16 = floor(Ents / 2^15) % 2
B17 = floor(Ents / 2^16) % 2
B18 = floor(Ents / 2^17) % 2

BaseEnt = 0
BaseEnt += (B1 ? 1 : 0) * 2^0
BaseEnt += (B2 ? 1 : 0) * 2^1
BaseEnt += (B3 ? 1 : 0) * 2^2
BaseEnt += (B4 ? 1 : 0) * 2^3
BaseEnt += (B5 ? 1 : 0) * 2^4
BaseEnt += (B6 ? 1 : 0) * 2^5
BaseEnt += (B7 ? 1 : 0) * 2^6
BaseEnt += (B8 ? 1 : 0) * 2^7
BaseEnt += (B9 ? 1 : 0) * 2^8

TargEnt = 0
TargEnt += (B10 ? 1 : 0) * 2^0
TargEnt += (B11 ? 1 : 0) * 2^1
TargEnt += (B12 ? 1 : 0) * 2^2
TargEnt += (B13 ? 1 : 0) * 2^3
TargEnt += (B14 ? 1 : 0) * 2^4
TargEnt += (B15 ? 1 : 0) * 2^5
TargEnt += (B16 ? 1 : 0) * 2^6
TargEnt += (B17 ? 1 : 0) * 2^7
TargEnt += (B18 ? 1 : 0) * 2^8

DataR = floor(DataR)
Active = floor(DataR / 2^0) % 2
Explode1 = floor(DataR / 2^1) % 2

if (InRange | Disable) {Alive = 0} else {Alive = entity():id()}
TargEnt = floor(TargEnt)
BaseEnt = floor(BaseEnt)

if (!Disable) {
Base = entity(BaseEnt):pos()
ZenX = Base:x()
ZenY = Base:y()
ZenZ = Base:z() +500
ZVec = vec(ZenX,ZenY,ZenZ)

T1 = entity(TargEnt):pos()
TX = T1:x()
TY = T1:y()
TZ = T1:z()

Distance = entity(TargEnt):pos():distance(entity():pos())
ZDist = ZVec:distance(entity():pos())
BDist = Base:distance(entity():pos())

Zenith = 20 > ZDist
Home = 100 > BDist

if (Home) {Event = 0}
if (Zenith) {Event = 1}
if (Event==0)  {Kill = 0 Zenith1 = 1}
if (Event==1)  {Kill = 1 Zenith1 = 0} 

if (Zenith1) {X = ZenX Y = ZenY Z = ZenZ} elseif 
(Kill) {X = TX Y = TY Z = TZ}
if (!Setup) {X = 0 Y = 0 Z = 0 Zenith = 0 Setup = 1}
if (Active) {

#This is the stuff that makes it fly! DO NOT TOUCH!
SelfVel = entity():vel()
SelfPos = entity():pos()
T = 0.015 M = 402 K = 10 ThrK = 50
Dx = X - SelfPos:x() Dy=Y - SelfPos:y() Dz = Z - SelfPos:z()
Fx = -M / T / ThrK * (Dx * K - SelfVel:x())
Fy = -M / T / ThrK * (Dy * K - SelfVel:y())
Fz = -M / T / ThrK * (Dz * K - SelfVel:z())
F = vec(Fx,Fy,Fz)
Mul = sqrt(Fx^2 + Fy^2 + Fz^2)
#This is just controller stuff from here down Still working it out
if (Distance < 12) {InRange = 1} else {InRange = 0}

}
else {Mul = 0}
Explode = InRange | Explode1
}
