@name Nodegraph Editor
@inputs Path:array Completed Search Searcher:wirelink
@outputs Nodes:table HE Holo:array C RECORDING LINKING EDITING SCANNING CurrentLink Link:array
@outputs [Close Opened]:vector SearchIt
@persist C B D F HE HEP Loading Hologrammed Time Resolution
@persist RECORDING LINKING EDITING Hold SCANNING Validate
@persist [Cyan White Green Red Blue Yellow Orange Scale Offset Size]:vector
@persist [Co Brk Scan Filename]:string
@persist [OldPath OPA LinkStr LinkVec]:array
@trigger

runOnChat(1)
runOnFile(1)
O=owner() #Who does this chip respond to?
timer("searchit",500)
if(clk("searchit")){
    SearchIt=O:keyUse()&O:weapon():type():find("weapon_crowbar")
}
if(Searcher:entity()){
    Completed=Searcher["Completed",number]
    Search=Searcher["SEARCHING",number]
    Path=Searcher["NodeList",array]
    Closed=Searcher["Closed",table]
    Open=Searcher["Open",table]
    Searcher["Nodes",table]=Nodes
    Searcher["Search",number]=SearchIt
    
}
#Function messages
RecMsg="Recording Path: "
EditMsg="Manual Node Editing Mode: "
LinkMsg="Automatically Linking Nodes"


#Chat key word list
Com="NG" #Node graph
Record="record"
Links="link"
Edit="edit"
Load="load"
Save="save"
On1="on"
On0="off"
Help="help"
Valid="validate"

#Chat key word check list
Chat=O:lastSaid()
COM=Chat:find(Com)
RECORD=Chat:find(Record)
LINKS=Chat:find(Links)
EDIT=Chat:find(Edit)
LOAD=Chat:find(Load)
SAVE=Chat:find(Save)
ON1=Chat:find(On1)
ON0=Chat:find(On0)
HELP=Chat:find(Help)
VALID=Chat:find(Valid)

#Chat Commannd Tree, this triggers various functions via chat
if(chatClk()&COM){
    if(RECORD){
        if(ON1){
            RECORDING=1 
            print(RecMsg+On1)
            SCANNING=0
        }
        elseif(ON0){
            RECORDING=0 print(RecMsg+On0)
            SCANNING=1
        }
    }
    if(LINKS){
        LINKING=1
        print(LinkMsg)
    }
    if(EDIT){
        print("type help with this command to get help.")
        if(ON1){EDITING=1 print(EditMsg+On1)}
        elseif(ON0){EDITING=0 print(EditMsg+On0)}
        if(HELP){
            print("Manual Node Editing Mode: All actions have a 1 second timer to prevent accidents.")
            print("Hold Physgun to edit links:")
            print("To edit links take physgun, aim at first node, hold use and right click. Then aim at second node and hold use and left click. This toggles symeterical linking.")
            print("To toggle asymeterical linking crouch while preforming the second action stated above.")
            print("Hold Gravity gun to edit nodes")
            print("To move nodes: look at a node hologram so it's blue, holding use, press right click to pickup node, use+left click to drop where you look.")
            print("To add an unlinked node, look where you want the node to be and hold use + left and right click.")
        }
    }
    if(LOAD){
        LOADING=1
    }
    if(SAVE){
        SAVING=1
    }
    if(VALID){
        Validate=1
        F=0
    }
}


if(first()){
    Filename="Nodegraph_2_"+map()+".txt"
    Offset=vec(0,0,10) #How high off the ground will nodes be placed
    Resolution=800 #Used for creating and processing nodegraphs
    Nodes=table() #The master nodegraph variable
    Scan="Scan" #Timer word
    Size=vec(1,1,1)*0.5  #Hologram size
    Scale=vec(1,1,1)*0.5 #I think this is the same as Size
    
    timer(Scan,20)
    SCANNING=1
    
    #hologram colors
    Brightness=255
    Cyan=vec(0,1,1)*Brightness
    White=vec(1,1,1)*Brightness
    Green=vec(0,1,0)*Brightness
    Red=vec(1,0,0)*Brightness
    Blue=vec(0,0,1)*Brightness
    Orange=vec(1,0.5,0)*Brightness
    Yellow=vec(1,1,0)*Brightness
    
    
    
    #string formating for links
    Co=":"   #Used for separating source and destination links
    Brk="<" #used for separating individual links
}

#Node Sequence Validation
#if(Validate){
#    if(F<=round(Nodes:count()/2)){timer("valid",20)}
#    while(F<=round(Nodes:count()/2)&perf()){
#        if(Nodes[toString(F),vector]){
#            if(F==0){
#                
#            }
#        }
#    }
#
#}



#Save the nodegraph
if(SAVING){
    print("Saving "+round(Nodes:count()/2)+" nodes") 
    fileWrite(Filename,glonEncode(Nodes))
}

if(fileCanLoad()&LOADING){
    fileLoad(Filename)
    Nodes=table()    
    Time=curtime()
    #NodeKeys=array()
    SCANNING=0
    stoptimer(Scan)
    print("Loading: "+Filename)
    Loading=1   
}



if(fileLoading()&Loading){
    print("Node Graph File is loading...")
    Loading=0
    Hologrammed=0
}



if(fileLoaded()&!Hologrammed){   
    #if(!Nodes){Nodes=glonDecodeTable(fileRead(Filename))}
    if(!Nodes){
        Loadedin=curtime()-Time
        print("Node Graph loaded in: "+Loadedin+" seconds.")
        Nodes=glonDecodeTable(fileRead())
    }
#    if(NodeKeys:count()<round(Nodes:count()/2)){

#        foreach(K,V:vector=Nodes){
#            NodeKeys:pushString(K)
#        }
#    }
        
    Count=round(Nodes:count()/2)
    if(B==1){print("reading "+round(Nodes:count()/2)+" nodes.")}
    Color=Cyan    
    
    if(B<=Count){timer("load",80)}
    else{
        Hologrammed=1
        SCANNING=1
        Loading=0
        timer(Scan,1000)
    }
    
    
    while(B<=Count&perf()){
#        print("Loading Node: "+NodeKeys[B,string]+" which is "+B+"/"+Count)
#        holoCreate(NodeKeys[B,string]:toNumber(),Nodes[NodeKeys[B,string],vector],Scale,ang(),Color)
        if(Nodes[toString(B),vector]){
            BS=toString(B)
            print("Loading Node: "+BS+" which is "+B+"/"+Count)
            holoCreate(B,Nodes[BS,vector],Size,ang(),Color)
        }    
        B++
        
    }
    
}
#this is used to tell the below function what you LOOKED at last
if(clk("wait")){
    HEP=HE #HE= Holo Entity  HEP= Holo Entity Previous
}

if(clk(Scan)&SCANNING){
    timer("wait",30)
    timer(Scan,10)
    findInCone(O:shootPos(),O:eye(),500,5)
    findIncludeClass("gmod_wire_hologram")
    Holo=findToArray()
    HC=Holo:count()
    HE=holoIndex(Holo[1,entity])
    if(HC){
        HES=toString(HE)
        holoColor(HE,Blue)
        holoScale(HE,Scale*2)
        NS=toString(HE)
        Link=Nodes[NS,string]:replace(NS+Co,""):explode(Brk)
        #printTable(Link)
        for(C=2,Link:count()){
            #print("Node: "+Link[C,string])
            holoColor(Link[C,string]:toNumber(),Red)
            holoScale(Link[C,string]:toNumber(),Scale*2)
        }
    }elseif(HEP){
        NS=toString(HEP)
        Link=Nodes[NS,string]:replace(NS+Co,""):explode(Brk)
        holoColor(HEP,White)
        holoScale(HEP,Scale)
        for(C=1,Link:count()){
            holoColor(Link[C,string]:toNumber(),White)
            holoScale(Link[C,string]:toNumber(),Scale)
        }
    }
}
#this highlights the path found
if(Path:count()&Completed&!Search){
    OldPath=Path:clone()
    for(C=1,Path:count()){
        holoColor(Path[C,number],Orange)
        holoScale(Path[C,number],Scale*2)
    }
}
#
#if(Path:count()&Search&!Completed){
#    for(C=1,Path:count()){
#        holoColor(Path[C,number],Red)
#        holoColor(Path[C,number],Scale*2)
#    }
#}
#this unhighlights the path when a new search is started
if(OldPath:count()&Search&changed(Search)){
    for(C=1,Path:count()){
        holoColor(Path[C,number],White)
        holoScale(Path[C,number],Scale)
    }    
}



#automatic node linking function
if(LINKING){
    
    if(chatClk()){
        print("Linking In Progress")
        LinkVec=array()
        LinkStr=array()
        B=1 #B is my first order counting variable 
        D=1 #D is my second order counting variable
        timer("link",20)
        foreach(K,V:vector=Nodes){
            LinkVec:pushVector(V)
            LinkStr:pushString(K)
        }
    }
}
if(clk("link")){
        
    NCo=LinkStr:count() #Node Count
    if(B<=NCo){timer("link",75)}
    #print("Processing Node: "+B+"/"+NCo)
    while(B<=NCo&perf()){ 
                V=LinkVec[B,vector]
                V1=LinkVec[D,vector] 
                K=LinkStr[B,string]
                K1=LinkStr[D,string]
            if((D<=NCo)&(K!=K1)){
                rangerHitEntities(1)
                #rangerFilter(HitThis) #array of valid targets
                if((round(V:distance(V1))<=Resolution)){ #checks for nodes within X of self node
                    Ranger=rangerOffset(V,V1):hit() #tests LOS between nodes, 0=clear, 1=obstructed
                    Linkstr=Brk+K+Co+K1 #link string denotes self and linked nodes
                    if(!Ranger&!(Nodes[K,string]:find(Linkstr))){ #checks clear LOS and no redundancy
                        Nodes[K,string]=Nodes[K,string]+Linkstr
                        #print("link: "+Nodes[K,string])
                    }else{Nodes[K,string]:replace(Linkstr,"")} #bad LOS but already linked= unlinked
                }
            }
            D++
        if(D>NCo){B++ D=1 CurrentLink=B}
    }
    if(B>NCo){print("Nodes Linked!") LINKING=0}
}


NC=round(Nodes:count()/2) #Node Count

OP=round(O:pos()+Offset)  #Owner Position


#Owner Position Array, logs the last X locations the player was at over the previous executions
if(RECORDING){
    if(OPA:count()>=20){OPA:shiftVector()}
    OPA:pushVector(OP)
    timer("record",1)
}



#Auto node generation, no links
if(((clk("record")|clk("delay"))&RECORD)){
    #Jump Or Ladder detection
    JOL=!O:isOnGround()&!O:inNoclip()
    #On Ground
    OG=O:isOnGround()
    timer("record",50)

    
    #if(!Ladder){timer("ladder",20) Ladder=1}    
    
    #OL=changed(OnLadder) #On Ladder
    #if(OL){print("Accel="+$ZVel)}    
    
    I1=Resolution
    I2=I1-100
    findInSphere(O:pos(),I1+5)
    findSortByDistance(O:pos())
    findIncludeClass("hologram")
    Holo=findToArray()
    
    Color=Cyan
    HP=holoIndex(Holo[1,entity]) #Holographic Proximatey number
    timer("delay",100)    
    if(NC){
        NC1=NC+1
        Dist=Nodes[toString(HP),vector]:distance(OP)
        rangerHitEntities(1)
        rangerIgnoreWorld(0)
        rangerFilter(O)
        OLS=rangerOffset(OP,Nodes[toString(HP),vector]):hit() #Owner Line of Sight
        
    }

    if(!NC){
        Nodes["1",vector]=OP
        Nodes["1",string]="1"
        holoCreate(1,OP,Size,ang(),Color)
        
    }elseif(((Dist>I2)&(Dist<I1))|OLS){
        if(OLS){
            D=0
            
            for(C=1,OPA:count(),1){
                
                OLS=rangerOffset(OPA[C,vector],Nodes[toString(HP),vector]):hit()
                if(!OLS){D++}

                if(D==10*!OLS){OP=OPA[C,vector] break print("C: "+C+" "+OLS)}
            }
        }
        
        
        NCS=toString(NC1) #Node Count String
        #this records the node and displays a hologram
        
        if((OG&HP&(Nodes[toString(NC),vector]:distance(OP)>I2)|!OLS)&clk("delay")){
            print("Waypoint Recorded")
            Nodes[NCS,vector]=OP
            Nodes[NCS,string]=NCS
            holoCreate(NC1,OP,Size,ang(),Color)
        }
    }
}else{Ladder=0}


#Enable Node Editing
if(EDITING){
    Editing=1 
    timer("edit",1)
    if(chatClk()){print("edit mode")}
}else{Editing=0}


#move nodes, delete nodes, link nodes, manual placement, etc... 
if(Editing&clk("edit")){
timer("edit",1000)
    Wep=O:weapon():type()
    Crouch=O:isCrouch()
    Noclip=O:inNoclip()
    A1=O:keyAttack1()
    A2=O:keyAttack2()
    Use=O:keyUse()
    AimEnt=O:aimEntity()
    AimPos=round(O:aimPos()+Offset)
    Cannon=Wep:find("physcannon")
    Phys=Wep:find("physgun")
    
    #link/unlink just these 2 nodes, creates or removes a symeterical link
    if(Phys&Use&HE&!Crouch){
        if(A2&!A1){
            Hold=HE
            print("Selecting Node: "+HE)
        }
        if(A1&!A2){
            S2O=Brk+Hold+Co+HE #Self node To Other node
            O2S=Brk+HE+Co+Hold #Other node to Self node
            SNode=Nodes[toString(Hold),string] #Self Node
            ONode=Nodes[toString(HE),string] #Other Node
            Check=SNode:find(S2O)
            if(Check){
                Nodes[toString(Hold),string]=SNode:replace(S2O,"")
                Nodes[toString(HE),string]=ONode:replace(O2S,"")
                print("Nodes: "+Hold+" and "+HE+" have been unlinked.")
                print(Nodes[toString(Hold),string]+"\n"+Nodes[toString(HE),string])
            }else{
                Nodes[toString(Hold),string]=SNode+S2O
                Nodes[toString(HE),string]=ONode+O2S
                print("Nodes: "+Hold+" and "+HE+" have been linked.")
                print(Nodes[toString(Hold),string]+"\n"+Nodes[toString(HE),string])
            }
            
        }
    }
    #create half links, asymetrically linked nodes
    if(Phys&HE&Crouch){
        if(A1){
            
            print("Preparing 1 way link with node: "+Hold)
            S2O=Brk+Hold+Co+HE #Self node To Other node
            O2S=Brk+HE+Co+Hold #Other node to Self node
            SNode=Nodes[toString(Hold),string] #Self Node
            ONode=Nodes[toString(HE),string] #Other Node
            if(!SNode:find(S2O)){
                
                Nodes[toString(Hold),string]=SNode+S2O
            }
            if(ONode:find(O2S)){
                Nodes[toString(HE),string]=ONode:replace(O2S,"")
            }
            print("Node "+Hold+" is half linked to "+HE)
            print(Nodes[toString(Hold),string]+"\n"+Nodes[toString(HE),string])
        }
    }
    
    #Pick up and move nodes
    if(Cannon&Use){
        #Pickup node
        if(A2&!A1&HE){Hold=HE print("Node: "+HE+" picked up")}
        #Place node
        if(A1&!A2){
            print("Placing Node: "+Hold)
            Nodes[toString(Hold),vector]=AimPos
            holoPos(Hold,AimPos)
            findInSphere(AimPos,Resolution)
            findIncludeClass("hologram")
            #Hologram=findToArray()
            #if(!Hologram:count()){print("You've placed an orphan node!")}
        }
        if(A1&A2){
            CS=toString(round(Nodes:count()/2)+1)
            Nodes[CS,vector]=AimPos
            Nodes[CS,string]=CS
            print("Creating Node: "+CS)
            holoCreate(CS:toNumber(),AimPos,Size,ang(),Yellow)
            findInSphere(AimPos,Resolution)
            findIncludeClass("hologram")
            Hologram=findToArray()
            if(!Hologram:count()){print("You've placed an orphan node!")}
        }
    }
}


if(HE){
    Close=Closed[toString(HE),vector]
    Opened=Open[toString(HE),vector]
}

