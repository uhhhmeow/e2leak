@name Birthday Fireworks Chip Client
@inputs Start Time
@outputs A1 A2 A3 A4 A5 A6 A7 A8 A9 A10 A11 A12 Yaw
@persist Inc Timer Yaw A1 A2 A3 A4 A5 A6 A7 A8 A9 A10 A11 A12
interval(20)
Yaw = floor(entity():angles():yaw() + 180)

if (Start) {
Timer += Time
if (Yaw < 30 & Yaw > 0 & Timer==1) {A1 = 0}
if (Yaw < 60 & Yaw > 30 & Timer==2) {A2 = 0}
if (Yaw < 90 & Yaw > 60 & Timer==3) {A3 = 0}
if (Yaw < 120 & Yaw > 90 & Timer==4) {A4 = 0}
if (Yaw < 150 & Yaw > 120 & Timer==5) {A5 = 0}
if (Yaw < 180 & Yaw > 150 & Timer==6) {A6 = 0}
if (Yaw < 210 & Yaw > 180 & Timer==7) {A7 = 0}
if (Yaw < 240 & Yaw > 210 & Timer==8) {A8 = 0}
if (Yaw < 270 & Yaw > 240 & Timer==9) {A9 = 0}
if (Yaw < 300 & Yaw > 270 & Timer==10) {A10 = 0}
if (Yaw < 330 & Yaw > 300 & Timer==11) {A11 = 0}
if (Yaw < 360 & Yaw > 330 & Timer==12) {A12 = 0}

} else {
A1 = 1
A2 = 1
A3 = 1
A4 = 1
A5 = 1
A6 = 1
A7 = 1
A8 = 1
A9 = 1
A10 = 1
A11 = 1
A12 = 1
Timer = 0 
}
