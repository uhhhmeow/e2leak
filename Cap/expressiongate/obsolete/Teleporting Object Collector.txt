N@Teleporting Object Collector
I@GPSSync Self Data1 Data2 Data3 Data4 TarX TarY TarZ
O@Sync1 Newtarget
XBox=recv(GPSSync,1),YBox=recv(GPSSync,2),ZBox=recv(GPSSync,3)
XTar1=recv(Data1,1),YTar1=recv(Data1,2),ZTar1=recv(Data1,3)
XTar2=recv(Data2,1),YTar2=recv(Data2,2),ZTar2=recv(Data2,3)
XTar3=recv(Data3,1),YTar3=recv(Data3,2),ZTar3=recv(Data3,3)
XTar4=recv(Data4,1),YTar4=recv(Data4,2),ZTar4=recv(Data4,3)
XG=recv(GPSSync,1),YG=recv(GPSSync,2),ZG=recv(GPSSync,3)
Sync1=send(TarX,TarY,TarZ,Tar)
Newtarget=0

Rad=30
(TarX > XG-Rad & TarX < XG+Rad) & (TarY > YG-Rad & TarY < YG+Rad) &
(TarZ > ZG-Rad & TarZ < ZG+Rad) -> Newtarget=1 Tar=0;

Self==1 & (TarX==Xtar2 | TarX==Xtar3 | TarX==Xtar4) -> Newtarget=1 Tar=1;
Self==1 & (TarY==Ytar2 | TarY==Ytar3 | TarY==Ytar4) -> Newtarget=1 Tar=1;
Self==1 & (TarZ==Ztar2 | TarZ==Ztar3 | TarZ==Ztar4) -> Newtarget=1 Tar=1;

Self==2 & (TarX==Xtar1 | TarX==Xtar3 | TarX==Xtar4) -> Newtarget=1 Tar=1;
Self==2 & (TarY==Ytar1 | TarY==Ytar3 | TarY==Ytar4) -> Newtarget=1 Tar=1;
Self==2 & (TarZ==Ztar1 | TarZ==Ztar3 | TarZ==Ztar4) -> Newtarget=1 Tar=1;

Self==3 & (TarX==Xtar2 | TarX==Xtar1 | TarX==Xtar4) -> Newtarget=1 Tar=1;
Self==3 & (TarY==Ytar2 | TarY==Ytar1 | TarY==Ytar4) -> Newtarget=1 Tar=1;
Self==3 & (TarZ==Ztar2 | TarZ==Ztar1 | TarZ==Ztar4) -> Newtarget=1 Tar=1;

Self==4 & (TarX==Xtar2 | TarX==Xtar3 | TarX==Xtar1) -> Newtarget=1 Tar=1;
Self==4 & (TarY==Ytar2 | TarY==Ytar3 | TarY==Ytar1) -> Newtarget=1 Tar=1;
Self==4 & (TarZ==Ztar2 | TarZ==Ztar3 | TarZ==Ztar1) -> Newtarget=1 Tar=1;

