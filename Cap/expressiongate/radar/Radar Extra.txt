N@Radar Extra
I@Xi Yi Zi Center
O@Xo Yo Zo
interval(20)
Ratio = 0.02
Xc = recv(Center, 1), Yc = recv(Center, 2), Zc = recv(Center, 3)
Xo = (Xi - Xc) * Ratio + Xc
Yo = (Yi - Yc) * Ratio + Yc
Zo = (Zi - Zc) * Ratio + Zc