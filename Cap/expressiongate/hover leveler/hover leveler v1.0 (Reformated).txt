N@Hover Leveler
I@Pitch Roll Hovercontroller Sky
O@FL FR AR AL
Sc = 4 Pitch1 =-3.5 Pitch2 =3.5 Roll1= -3.5 Roll2 = 3.5
PC =(Pitch > Pitch1) - (Pitch < Pitch2)
RC = (Roll > Roll1) - (Roll < Roll2)
Ha = PC == 0 & RC == 0
Hover = Ha * Hovercontroller + Flip
I= -((90 < Roll) | (-90 > Roll))
I= ((90 > Roll) | (-90 < Roll))
Flip=Sky <40 
Flipover = Roll > 170 | Roll < -170
Flipover == 1 -> F=6;
Flipover == 0 -> F=0;
FL = sgn(PC - RC*I)/Sc + Hover - F
FR = sgn(PC + RC*I)/Sc + Hover + F
AR= sgn(-PC + RC*I)/Sc + Hover+ F
AL = sgn(-PC - RC*I)/Sc + Hover -F
