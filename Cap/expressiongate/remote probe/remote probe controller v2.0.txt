N@Remote Probe Controller
I@Left Right Forward Back Reload Space Alt Shift P2 P4 P6 P8 Active GPitch GYaw APitch AYaw Z1
O@Mul XL XR YL YR Hovcon Hover Pitch Yaw Z1
XL=XR=YL=YR=0
Mul=(Shift*100)+(1*!Shift)
A=Active -> B=(Shift*100)+(1*!Shift);
XL=1*(-Forward+Back)+A*(P4-P6)
XR=1*(-Forward+Back)+A*(-P4+P6)
YL=1*(Right-Left)
YR=1*(Right-Left)
Hover = (B/(10*Shift +1*!Shift))*(P8 - P2)
Z1 +=30
Pitch = !Alt*GPitch - Alt*APitch
Yaw = !Alt*GYaw + Alt* AYaw

Hov=Hovcon
Hovcon==1 & Space==1 & ~Space==1-> Hov=0;
Hovcon==0 & Space==1 & ~Space==1 -> Hov=1;
Hovcon=Hov
