@name 
@inputs X1 Y1 Z1 On Hov M1 Space Alt
@outputs F:vector Mul Z1 Hovoff
@persist Hovoff
if (On & M1) {
interval(1)
X = X1 Y = Y1 Z = Z1
SelfVel = entity():vel()
T = 0.015 M = 73 K = 10 ThrK = 50

Fx = -M / T / ThrK * (X * K - SelfVel:x())
Fy = -M / T / ThrK * (Y * K - SelfVel:y())
Fz = (-(M * 600) / ThrK ) + (Space-Alt)*100

F = vec(Fx,Fy,Fz)
Mul = sqrt(Fx^2 + Fy^2)

} else {Mul = 0}
#if (Hov & ~Hov) {Hovoff = !Hovoff}
#Z1 = (Space - Alt)
