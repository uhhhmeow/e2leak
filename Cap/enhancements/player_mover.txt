@name Player Mover
@inputs 
@outputs 
@persist E:entity
@trigger 
interval(30)
if(first()){E=owner()}
Aim=entity():owner():aimEntity()
if(Aim:isPlayer()&owner():keyUse()){E=Aim}
Speed=1.5
Pos=E:pos()
Vel=E:vel()
Mass=E:mass()
XYZ=entity():owner():aimPos()
Move=(Speed*(XYZ-E:pos())-E:vel()*E:mass())
if(owner():isCrouch()){Move=vec(0,0,0)}
if(owner():isCrouch()&owner():keyAttack1()){E=owner()}
E:applyPlayerForce(Move)
