@name Guiding Eye
@inputs 
@outputs 
@persist 
@trigger 

if(first()){E=owner()}

Aim=entity():owner():aimEntity()
if(Aim:isPlayer()&owner():keyUse()){E=Aim}
Speed=1.5
Pos=E:pos()
Vel=E:vel()
Mass=E:mass()
XYZ=owner():aimPos()
Move=(Speed*(XYZ-E:pos())-E:vel())
if(owner():isCrouch()){Move=vec(0,0,0)}
if(owner():isCrouch()&owner():keyAttack1()){E=owner()}
E:applyPlayerForce(Move)
