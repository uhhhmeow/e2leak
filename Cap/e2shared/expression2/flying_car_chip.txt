@name Flying car chip
@inputs W A D S R Space Alt
@outputs Turn Thrust Hover Hoveroff
@persist 
@trigger 
Turn=D-A #This means Right-Left
Thrust=W-S #This means Foward - Back
Hover=Space-Alt #This means up - down
if(R&~R){Hoveroff=!Hoveroff} #this is a toggle.  It reads like this
#If R is true( or not 0) and R just changed (you pressed it or released it)
#then Hoveroff equals the opposite of hoveroff 
#(meaning if it's 1 it's now 0, if it's 0 it's now 1)

