@name Privacy Chip
@inputs T:array D:array
@outputs 
@persist P1:array P2:array P3:array P4:array P5:array
@persist P6:array P7:array P8:array P9:array P10:array
@persist Obj:array C
@trigger all
Time=15
interval(Time)
Self=entity()
if(first()|duped()){Radius=2000}

Owner = entity():owner()
Chat = Owner:lastSaid()
K=curtime()-Owner:lastSaidWhen()<Time*2/1000

Brk="<"

Com=Chat:find("NSP")
Break=Chat:find(Brk)
Rad=Chat:find("Rad")


if(Com&K){
if(Break&Rad){Radii=Chat:explode(Brk):string(2):toNumber()
    if(Radii){Radius=abs(Radii) print("Field Radius: "+Radii)}else{
    print("Field Radius: "+Radii)  
if(Radii<0){On=0}else{On=1}}#end radii
}#End Break&Rad
}#end Com&K
findInSphere(Self:pos(),Radius)
findSortByDistance(Self:pos())
Obj=findToArray()
Count=Obj:count()

if(C<Count){C++}else{C=1}
