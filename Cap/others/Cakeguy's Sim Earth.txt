@name Cake's simple earth sim
@inputs 
@outputs Color:vector Color2:vector A1 Sun:vector Moona Storm Lighting Pos1:vector
@persist Sun:vector A Moon:vector
@trigger 
interval(10)
if(first()){
holoCreate(1)
holoScaleUnits(1,entity():isWeldedTo():boxSize())
holoPos(1,entity():pos()-vec(0,0,1))
holoModel(1,"hqcylinder2")
holoColor(1,vec(100,100,100))
holoMaterial(1,"models/props_wasteland/rockcliff02a")


holoCreate(100)
holoScale(100,vec(0.1,0.1,10))
holoColor(100,vec(2,5,255))

holoCreate(2)
holoScaleUnits(2,entity():isWeldedTo():boxSize()*vec(1,1,27))

holoModel(2,"dome2")

holoCreate(3)
holoModel(3,"hqcylinder2")
holoScale(3,vec(1,1,0.1))

holoCreate(4)
holoModel(4,"hqcylinder2")
holoScale(4,vec(1,1,0.1))


holoColor(3,vec(255,255,0))
for(K = 5,20){
    holoCreate(K)
    holoPos(K,vec(random(-20,20),random(-20,20),0)+entity():pos())
    holoScale(K,vec(0.2,0.2,1))
    
    
    
}

}


holoPos(3,Sun)
holoPos(4,Moon)
Sun = vec(0,cos(A)*43,sin(A)*43)+entity():pos()
if(owner():keyAttack1()==1){

}

if(sin(A)*255 > 0){
Color = vec(sin(A)*100,sin(A)*240,sin(A)*240)
Color2 = vec(sin(A)*150,sin(A)*150,sin(A)*150)+vec(100,100,100)}



if(sin(A)*43<0 & Moona < 250){Moona=255}elseif(sin(A)*43>-1){
    Moon = vec(random(-10,10),random(10,-10),41)+entity():pos()
    
    Moona = 0}
holoAlpha(4,Moona)


A1 = A
A = A + 0.05

holoAng(3,(holoEntity(3):pos()-entity():pos()):toAngle()+ang(90,0,0))
holoAng(4,(holoEntity(4):pos()-entity():pos()):toAngle()+ang(90,0,0))

holoColor(2,Color)
Storm = round(random(1,2000))
if(Storm == 1){
   
       holoPos(100,Pos1)
     holoAlpha(100,255)
Pos1 = vec(random(-10,10),random(10,-10),0)+entity():pos()
}
else{Pos1 = vec(random(-10,10),random(10,-10),0)+entity():pos() holoAlpha(100,0)}
holoColor(1,Color2)
for(K = 5,20){
holoColor(K,Color2)}
