@name Dark RP Gun Dispenser
@inputs C:wirelink Up Down Accept Money M16s Snipers Ak47s Shotguns
@outputs Chosen M16 Sniper Ak47 Shotgun M16ss Sniperss Ak47ss Shotgunss Hydralic
@persist V1 V2 V3 Value
@trigger all

#If first run of the expression, clear the screen
if (first()) {C:writeCell(2041,1)}
#Write top line on X=1, Y=0, text color blue, background color green
C:writeString("Killaser's Gunstore",1,0,9,900,49)

#Highlight
#If Up is pressed take 1 from value
Value-=Up
#If Down is pressed add 1 to value
Value+=Down
#Clamp Value clamp(Variable,Min,Max) and 
Value=clamp(Value,0,3)

#If Value is 0, V1 is dark greenish
if (Value==0) {V1=999 Chosen=1}
#Else V1 is black
else {V1=0}
#If Value is 1, V2 is dark greenish
if (Value==1) {V2=999 Chosen=2}
#Else V2 is black
else {V2=0}
#If Value is 2, V3 is dark greenish
if (Value==2) {V3=999 Chosen=3}
#Else V3 is black
else {V3=0}
if (Value==3) {V4=999 Chosen=4}
#Else V3 is black
else {V4=0}
#Write GUI
#Write a line, x=2, Y=3, Text color is white, background is V1
C:writeString("M16 250$",2,3,130,V1)
#Write a line, x=2, Y=4, Text color is white, background is V2
C:writeString("Sniper 500$",2,4,90,V2)
#Write a line, x=2, Y=5, Text color is white, background is V3
C:writeString("AK47 250$",2,5,900,V3) 
C:writeString("Shotgun 350$",2,6,49,V4) 
C:writeString(Money+"      ",18,15,900) 
C:writeString("Credits:",10,15,900) 
if (M16s>1&M16s<15000) {C:writeString("Instock     ",18,3,90)} else {C:writeString("Outofstock",18,3,900)}
if (Snipers>1&Snipers<15000) {C:writeString("Instock     ",18,4,90)} else {C:writeString("Outofstock",18,4,900)}
if (Ak47s>1&Ak47s<15000) {C:writeString("Instock     ",18,5,90)} else {C:writeString("Outofstock",18,5,900)}
if (Shotguns>1&Shotguns<15000) {C:writeString("Instock    ",18,6,90)} else {C:writeString("Outofstock",18,6,900)}
if (Money==250&Chosen==1&Accept&M16ss==1){M16==1}
if (M16s>1&M16s<15000) {M16ss=1} else {M16ss=0}
if (Snipers>1&Snipers<15000)  {Sniperss=1} else {Sniperss=0}
if (Ak47s>1&Ak47s<15000)  {Ak47ss=1} else {Ak47ss=0}
if (Shotguns>1&Shotguns<15000)  {Shotgunss=1} else {Shotgunss=0}
if (Money==250&Chosen==1&Accept&M16ss==1){Hydralic=100} else {Hydralic=10}
if (Money==250&Chosen==1&Accept&M16ss==1){M16=1} else {M16=0}
if (Money==500&Chosen==2&Accept&Sniperss==1){Hydralic=100} else {Hydralic=10}
if (Money==500&Chosen==2&Accept&Sniperss==1){Sniper=1} else {Sniper=0}
if (Money==250&Chosen==3&Accept&Ak47ss==1){Hydralic=100} else {Hydralic=10}
if (Money==250&Chosen==3&Accept&Ak47ss==1){Ak47=1} else {Ak47=0}
if (Money==350&Chosen==4&Accept&Shotgunss==1){Hydralic=100} else {Hydralic=10}
if (Money==350&Chosen==4&Accept&Shotgunss==1){Shotgun=1} else {Shotgun=0}
