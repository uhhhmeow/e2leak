@name Bot Deathmatch
@inputs 
@outputs [Wep PWep]:string [Player Play Tar]:array
@persist [Wep PWep]:string [N List LIST]:array Accuracy RandomAccuracy
@trigger 
@model models/props_junk/PopCan01a.mdl
O=owner()
runOnTick(1)
runOnChat(1)


Chat=O:lastSaid()
Cmd="BDM" #Bot DeathMatch Command word
Menu="menu" #get a list of players
Get="get"  #Spesify targets, ex: "BDM get 12340" is the same as "get BDM 01423" order is irrelatvant
SelfDestruct="dest" #remove the chip
Acc="acc" #accuracy value.  10=30% accuracy, 9=33% ... 6=50% ... 4=75%  3=100% 1=HEADSHOT ONLY.


if(first()){
    N=array()
    N:pushString("1")
    N:pushString("2")
    N:pushString("3")
    N:pushString("4")
    N:pushString("5")
    N:pushString("6")
    N:pushString("7")
    N:pushString("8")
    N:pushString("9")
    N:pushString("0")
    #N:pushString("N") #NPC targeting?
    List=array()
    Accuracy=7
}

if(chatClk()|first()){
    CMD=Chat:find(Cmd)
    if(CMD|first()){  #you said the command word!  Now see if you said any commands!
        MENU=Chat:find(Menu) #Did you ask for a menu of targets?
        if(first()){MENU=1} #automatically print a list when you start since you have to anyway, it saves a step
        GET=Chat:find(Get) #Did you want to attack?
        N1=array() #This is the check to see if you spoke any numbers
        SELFDESTRUCT=Chat:find(SelfDestruct)
        ACC=Chat:find(Acc) #accuracy 
        for(C=1,N:count()){
            N1:pushNumber(Chat:find(N[C,string]))  
        }
        if(ACC){
            for(C=1,N1:count()){
                if(N1[C,number]){Accuracy=N[C,string]:toNumber()}
            }
            print("Accuracy is: "+Accuracy)
        }
        if(SELFDESTRUCT){print("Self Destruct Activated") selfDestructAll()}
        if(MENU){  #This presents the owner with a 10 item list to select targets from the 10 closest players including himself.
            List=array()
            findInSphere(O:pos(),30000)
            findClipToClass("player")
            findSortByDistance(O:pos())
            List=findToArray()
            print("Avalible Targets are: ")
            for(C=1,N:count()){
                print(N[C,string]+") "+List[C,entity]:name())
                if(List:count()==C){break}
            }
            
        }
        if(GET&!first()){ #This checks that you ran "menu" and then proceeds to construct a target list from your selections
            LIST=array()
            print("Targeting: ")
            for(C=1,N:count()){
                if(N1[C,number]){
                    LIST:pushEntity(List[C,entity])
                    print(LIST:count()+": "+LIST[C,entity]:name())
                }
            }
   
        }
    }
}


#Currently this code just checks if the player switched weapons and then make note of it
PWep=Wep
Wep=O:weapon():type()


##This code sifts out all the bots from the players Or at least anyone with the name Bot in their name.
Player=players() #Everyone
Play=array() #Ultimately just Bots should remain
for(C=1,Player:count()){
    if(Player[C,entity]:name():find("Bot")){Play:pushEntity(Player[C,entity])}
}


Tar=array()
for(C=1,LIST:count()){ #Take every bot, check distance to every target and assign each bot the closest target to them
    Dist=array()
    for(D=1,LIST:count()){
        if((Play[C,entity]!=LIST[D,entity])&LIST[D,entity]:isAlive()){
            Dist:pushNumber(Play[C,entity]:pos():distance(LIST[D,entity]:pos()))
        }
    }
    Tar:pushEntity(LIST[Dist:minIndex(),entity])
    
}
#printTable(Tar)

##This equips the bots with a new gun.
for(C=1,Play:count()){#Go through every bot and strip them so the next gun will be their only gun
    if((PWep!=Wep)|(Play[C,entity]:weapon():type()!=O:weapon():type())){#This code controls the bot's weaponry
        if(Play[C,entity]:isAlive()){
                Play[C,entity]:stripWeapons()
                Laser=Play[C,entity]:eyeTrace() #Do a trace to where the bot is looking
    
                entSpawn(O:weapon():type(),Play[C,entity]:pos())
            }
        }
        
        
        
}

#### periodically change the target the bots will aim for. 
timer("Accuracy",500)
if(clk("Accuracy")){
    RandomAccuracy=randint(1,Accuracy)
    #print("aiming at target spot: "+RandomAccuracy)
}
for(C=1,Play:count()){
    Self=Play[C,entity]
    
    if(Tar[C,entity]:pos()){
        Targ=Tar[C,entity]
        Pos=Targ:pos()
        You=Self
        Spray=array()
        Spray[1,vector]=Targ:shootPos()
        Spray[2,vector]=Pos+vec(0,0,Targ:height()/2)
        Spray[3,vector]=Pos
        Spray[4,vector]=Pos+vec(sin(You:angles():yaw())*-30,cos(You:angles():yaw())*50,Targ:height()/2)
        Spray[5,vector]=Pos+vec(sin(You:angles():yaw())*30,cos(You:angles():yaw())*-50,Targ:height()/2)
        Spray[6,vector]=Pos+vec(sin(You:angles():yaw())*50,cos(You:angles():yaw())*-30,Targ:height())
        Spray[7,vector]=Pos+vec(sin(You:angles():yaw())*-50,cos(You:angles():yaw())*30,Targ:height())
        Spray[8,vector]=Pos+vec(sin(You:angles():yaw())*-30,cos(You:angles():yaw())*30,Targ:height()*0.75)
        Spray[9,vector]=Pos+vec(0,0,Targ:height()+10)
        Spray[10,vector]=Pos+vec(sin(You:angles():yaw())*30,cos(You:angles():yaw())*-30,Targ:height()*0.75)
        #for(C=1,Spray:count()){holoCreate(C,Spray[C,vector])}
        
        XYZ=Tar[C,entity]:pos()
        Target=Spray[RandomAccuracy,vector] #Tar[C,entity]:pos()+vec(0,0,Tar[C,entity]:height()/2)
        
        #Target=O:aimPos()
        
        Offset=vec(0,0,0)
        Ang=(Target-Self:shootPos()+Offset):toAngle()
    
        Self:setEyeAngle(Ang)
        
        Speed=1.5
    
        Self:applyPlayerForce(Speed*(XYZ-Self:pos())-Self:vel())
    }
}
