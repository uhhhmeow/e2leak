@name Frog Servo
@inputs
@outputs
@persist Self:entity 
@persist InertiaConstantRad:number TickTime:number
@trigger 
if (tickClk()) {

#set this angle to aim the turret
Angle = ang(45,90,0)


Qrot= qMod(quat(Angle)*inv(quat(Self))) #need to modulo it to stop angle glich
RotationNeeded = vec(2*log(Qrot))

SelfAngVel = Self:angVelVector()*pi()/180
ActualInertia = Self:inertia()*InertiaConstantRad
LocalRot = Self:toLocalAxis(RotationNeeded)
Self:applyTorque((LocalRot/TickTime - SelfAngVel)*ActualInertia)
} elseif (first()) {
    #set self to the entity you want to aim
    Self = entity(115)
    #the inertia constant Rad is the base inertia constant
    InertiaConstantRad = 1550.0030838848
    TickTime =0.015
    runOnTick(1)
}
