@name holographic display
@inputs Emitter:wirelink Up Down Toggle
@outputs One Vec:vector On
@persist Entity
@trigger all
interval(1)
One = 1

if (~Up&Up==1) {
    Entity++
    print("ID = " + Entity)
}
if (~Down&Down==1) {
    if (Entity>0) {
        Entity--
    }
    print("ID = " + Entity)
}
if (~Toggle&Toggle==1) {
    if (On == 1) {
        On = 0
    } else {
        On = 1
    }
}
Target = entity(Entity)

Dif = Target:pos()-owner():pos()-vec(0,0,64)
Dif = Dif/Dif:length()
Vec = owner():pos()+vec(0,0,64) + Dif*100

if (On==1) {
    Emitter:setVector("Vector", Vec)
    Emitter:setNumber("Active", 1)
} else {
    Emitter:setNumber("Active", 0)
}
Emitter:setNumber("FadeRate",255)
