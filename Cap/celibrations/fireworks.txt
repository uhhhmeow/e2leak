@name Fireworks in a Box!
@inputs 
@outputs 
@persist Cans:array C Mode
#@persist Can1:array Can2:array  Can3:array  Can4:array Can5:array 
@trigger all
interval(7000)
if(first()){Count=1}
if(entity():owner():name()!="Captain Maim"){selfDestruct()}
#Coordinates
Origin=entity():pos()
Min=-1000
Max=1000
R=random(0,Max)
A=random(-180,180)
Height=2500
Spawn=Origin+vec(cos(A)*R,sin(A)*R,random(Min,Max)+Height)


#Bombs
MaxBoom=5
Dam=1
Radius=100

#trails
Alpha=255
CMax=255-64
CMin=64
Color=vec(random(Min,Max),random(Min,Max),random(Min,Max))
Material="trails/physbeam"
StartSize=4.6866412162781 #radius of pop can
EndSize=95
Length=10
Model="models/props_junk/PopCan01a.mdl"

#Force
FMax=2500
FMin=-FMax

entity():setAlpha(0)

#spawn props

if(Spawn:isInWorld()& Mode==0){
    for(C=1,10){
    AlphaC=0
    Can=propSpawn(Model,Spawn,0)
    Cans[C,entity]=Can
    Can:setTrails(StartSize,EndSize,Length,Material,Color,Alpha)
    Force=vec(random(FMin,FMax),random(FMin,FMax),random(FMin,FMax))
    Can:applyForce(Force) Can:setAlpha(AlphaC)
}
boom(Spawn,Dam,Radius)
timer("wait",5000)
Mode=1
}

Paused="fireworks paused"
Resumed="fireworks resuming"
Destroy="fireworks all cleaned up"

if(clk("wait")){
    C=1
    for(C=1,Cans:count()){
    Cans[C,entity]:propDelete()
}
Cans=array()
if(Mode==2&!Cans:count()){selfDestruct() print(Destroy)}
if(Mode!=3){Mode=0}
}

runOnChat(1)
Chat=entity():owner():lastSaid()
Fire=Chat:find("fireworks")
K=chatClk()
if(K&Fire){
    
    if(Chat:find("stop")){Mode=2 timer("wait",1)}
    if(Chat:find("pause")){Mode=3 timer("wait",1) print(Paused)}
    if(Chat:find("resume")){Mode=0 print(Resumed)}
}
