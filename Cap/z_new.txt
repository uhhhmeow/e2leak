@name 
@inputs
@outputs
@persist 
@trigger 

#[
    Block comments and multi line strings have been added!
    You can see the block comment syntax in this comment.
    Two new buttons have also been added to the right click menu.
    These buttons put block comments around the current selection.
    A checkbox has been added to the control menu (the wrench icon)
    which lets you change the block comment style.

    Multi line strings have also been added.
    Using multi line strings is easy:
    TestString = "Hello world
    this is a
    multi line string
    example."

    Font and font size options have been added to the control 
    menu (the wrench at the top)!

    Foreach loops have been added! The syntax is:
    foreach(Key,Value:type = Table) { }

    Documentation and examples are available at:
    http://wiki.garrysmod.com/?title=Wire_Expression2
    The community is available at http://www.wiremod.com
]#

# Foreach loops have been added! The syntax is:
# foreach(Key,Value:type = Table) { }
#
# Data Signals have been added! These functions allow you to transmit 
# data and execute E2s remotely. Read the wiki for more information.
#
# [wirelink]s can be wired to friends without getting kicked
#
# Expression 2 now uses the friends list of any prop protection
# installed on the server to determine if a player may interact
# with your props and read your code. To allow friends to write to your
# expressions, execute the following: wire_expression2_friendwrite 1
#
# A new operator [A ?: B] has been added, shortcut for [A ? A : B]
# Example: Output = A ?: B, Output = A ?: B ?: C
#
# Documentation and examples are available at:
# http://wiki.garrysmod.com/?title=Wire_Expression2
# The community is available at http://www.wiremod.com

