@name Ragdoll Dancer
@inputs 
@outputs BonesLocal:array Bones:array 
@persist 
@trigger
interval(30)
EC=entity():isConstrainedTo()
if(!EC){selfDestruct()}
Bones=EC:bones()
for(C=0,Bones:count()+1){
    BonesLocal[C,vector]=Bones[C,bone]:toLocal(Bones[0,bone]:pos())
}



#HL2 ragdoll, 15 bone count Scruffy, alyx? 
#0=hips
#1=gut
#2=right shoulder
#3=left shoulder
#4=left elbow
#5=left wrist
#6=right elbow
#7=right wrist
#8=right hip joint
#9=right knee
#10=head 
#11=left hip joint
#12=left knee
#13=left ankle
#14=right ankle

#Vort
#0=hips
#1=gut
#2=right shoulder
#3=left shoulder
#4=left elbow
#5=left wrist
#6=right elbow
#7=neck
#8=right wrist
#9=right hip joint
#10=right knee upper
#11=right knee lower
#12=right ankle
#13=left hip joint
#14=left knee upper
#15=left knee lower
#16=left ankle
#17=head

