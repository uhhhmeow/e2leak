@name Antlion Worker Spawner
@inputs D
@outputs Count Stop Wait Feelings:string P1ID Spawn:vector Z
@persist Count Stop Wait P1:entity 
interval(500)
findInSphere(entity():pos(),10000)
findIncludeClass("npc")
Total=findToArray()
Max = 25
if (Total:count()<(Max*2)) {
D = random(0,360)
R = random(20,500)


Spawn = entity():pos()-vec(R*cos(D),R*sin(D),Z*2)


if (Count<=Max & !Stop & Spawn:isInWorld()) { 
P1 = npcSpawn("npc_antlion_worker",Spawn)

P1ID=P1:id()
entity(P1ID):npcRelationship("player","fear",99)
Feelings = entity(P1ID):npcDisp(entity():owner())}
#findInSphere(entity():pos(),10000)
#findSortByDistance(entity():pos())
#findIncludePlayer("*")
#P1:npcGoRun(find():pos())


if (Count>Max) {Stop = 1 Wait = 0 Count=20}
if (!Stop) {Count+=1}
if (Stop) {Wait+=1}
if (Stop & Wait>90) {Stop = 0 Count = 0}
}
if(lastSaid():find("no zombies")){selfDestruct()}





