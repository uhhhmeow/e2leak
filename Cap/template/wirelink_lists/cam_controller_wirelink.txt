@name Cam Controller Wirelink
@inputs CamCon:wirelink
@outputs 
@persist 
@trigger 
#Input
CamCon:setNumber("Activated",Activated)
CamCon:setNumber("Zoom",Zoom)
CamCon:setNumber("X",X)
CamCon:setNumber("Y",Y)
CamCon:setNumber("Z",Z)
CamCon:setNumber("Pitch",Pitch)
CamCon:setNumber("Yaw",Yaw)
CamCon:setNumber("Roll",Roll)
CamCon:setAngle("Angle",Angle)
CamCon:setVector("Position",Position)
CamCon:setVector("Direction",Direction)
CamCon:setVector("Velocity",Velocity)
CamCon:setEntity("Parent",Parent)

#Output
On=CamCon:number("On")
X=CamCon:number("X")
Y=CamCon:number("Y")
Z=CamCon:number("Z")
