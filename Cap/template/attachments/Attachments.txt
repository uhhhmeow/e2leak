#[QUOTE=OmicroN;141317]Hey there folks, here's a list of your favorite and maybe not so favorite model's attachment names. Enjoy!
#
#By all means, use it up cause it took a little while to type out.
#
#By the way, in case you're wondering how this is useful or how it is structured, allow me to show you some examples.
#
#[U][B]holoParentAttachment(Number,Entity,AttachmentString)[/B][/U]
#[code]holoParentAttachment(1,owner(),"eyes")[/code]
#[B]
#[U]holoPos(Number,entity():attachmentPos(String/Index))[/B][/U]
#[code]holoPos(1,owner():attachmentPos("eyes"))[/code]
#
#[U][B]holoAng(Number,entity():attachmentAng(String/Index))[/B][/U]
#[code]holoAng(1,owner():attachmentAng("eyes"))[/code]
#
#[U][B]entity():lookupAttachment(String)[/B][/U]
#[code]ID = owner():lookupAttachment("eyes")[/code]
#
#[B][U]models\airboat.mdl[/U]
#	Attachments:[/B]
#[code]muzzle
#vehicle_driver_eyes
#vehicle_feet_passenger0
#vehicle_headlight
#gun
#wheel_fl
#wheel_fr
#wheel_rl
#wheele_rr
#raytrace_fl
#raytrace_fr
#raytrace_rl
#raytrace_rr
#splash_pt
#enter1
#enter2
#enter3
#enter4
#enter5
#enter6
#enter7
#enter8
#exit1
#exit2
#exit3
#exit4
#exit5
#exit6
#exit7
#exit8
#exit9
#exit10[/code]
#
#[B][U]models\alyx.mdl[/U]
#	Attachments:[/B]
#[code]eyes
#lefteye
#righteye
#nose
#mouth
#amulet
#chest
#hips
#lefthand
#righthand
#pouch1
#pouch2
#physgun_attachment
#emp
#emp_LH
#Emp_Holster
#R_shoulderBladeAim
#L_shoulderBladeAim
#forward
#anim_attachment_RH
#anim_attachment_LH[/code]
#
#[B][U]models\antlion.mdl[/U]
#	Attachments:[/B]
#[code]leftfront
#rightfront
#leftrear
#rightrear[/code]
#
#[B][U]models\antlion_guard.mdl[/U]
#	Attachments:[/B]
#[code]attach_glow1
#attach_glow2
#0
#1
#leftfront
#rightfront[/code]
#
#[B][U]models\barnacle.mdl[/U]
#	Attachments:[/B]
#[code]TongueEnd
#TongueSplineEnd
#StrikeHeadAttach
#StrikePoint
#NeckHeight
#innards[/code]
#
#[B][U]models\barney.mdl[/U]
#	Attachments:[/B]
#[code]eyes
#mouth
#chest
#helmet_attachment
#faceplate_attachment
#forward
#anim_attachment_RH
#anim_attachment_LH
#anim_attachment_head[/code]
#
#[B][U]models\breen.mdl[/U]
#	Attachments:[/B]
#[code]eyes
#mouth
#chest
#physgun_attachment
#forward
#anim_attachment_RH
#anim_attachment_LH
#anim_attachment_head[/code]
#
#[B][U]models\buggy.mdl[/U]
#	Attachments:[/B]
#[code]levitate_target
#gun_ref
#wheel_fl
#wheel_fr
#wheel_rl
#wheel_rr
#vehicle_driver_eyes
#vehicle_feet_passenger0
#vehicle_engine
#exit1
#exit2
#exit3
#exit4
#exit5
#exit6
#exit7
#exit8
#enter1
#enter2
#enter3
#enter4
#beam_damage
#seagull_perch1
#seagull_perch2
#seagull_perch3
#taucannon
#headlight
#Muzzle[/code]
#
#[B][U]models\Combine_Scanner.mdl[/U]
#	Attachments:[/B]
#[code]light
#eyes[/code]
#
#[B][U]models\Combine_Soldier.mdl
#models\Combine_Soldier_PrisonGuard.mdl
#models\Combine_Super_Soldier.mdl[/U]
#	Attachments:[/B]
#[code]lefthand
#zipline
#eyes
#beam_damage
#anim_attachment_RH
#anim_attachment_LH[/code]
#
#[B][U]models\Combine_Strider.mdl[/U]
#	Attachments:[/B]
#[code]left foot
#right foot
#back foot
#kneeL
#kneeR
#kneeB
#left skewer
#right skewer
#BigGun
#MiniGun
#MiniGunBase
#vehicle_driver_eyes
#vehicle_driver_exit
#buster_target[/code]
#
#[B][U]models\dog.mdl[/U]
#	Attachments:[/B]
#[code]eyes
#forward
#physgun
#thumb
#pinky
#index
#bigPhys_attachment
#groundTarget
#chestTarget
#__illumPosition
#Cable1a
#Cable1b
#Cable1c
#Cable2a
#Cable2b
#Cable2c
#Cable3a
#Cable3b
#Cable3c
#Cable3d
#Cable4a
#Cable4b
#Cable4c
#Cable4d
#Cable5a
#Cable5b
#Cable6a
#Cable6b
#Cable7a
#Cable7b[/code]
#
#[B][U]models\eli.mdl[/U]
#	Attachments:[/B]
#[code]eyes
#mouth
#chest
#LabPart_attachment
#MossyHandlK_Attachment
#attach_crowbar
#attach_tongue
#forward
#anim_attachment_RH
#anim_attachment_LH
#anim_attachment_head[/code]
#
#[B][U]models\gman.mdl[/U]
#Attachments:[/B]
#[code]eyes
#mouth
#chest
#cameraeye
#mouth_left
#cheekbone_left
#attach_lShoulderAim
#attach_rShoulderAim
#forward
#anim_attachment_RH
#anim_attachment_LH
#anim_attachment_head[/code]
#
#[B][U]models\headcrab.mdl
#models\headcrabblack.mdl
#models\headcrabclassic.mdl
#models\Lamarr.mdl
#models\soldier_stripped.mdl
#models\Synth.mdl[/U][/B]
#-none
#
#[B][U]models\Kleiner.mdl[/U]
#	Attachments:[/B]
#[code]eyes
#lefteye
#righteye
#nose
#mouth
#tie
#pen
#chest
#hips
#lefthand
#righthand
#attach_lShoulderAim
#attach_rShoulderAim
#forward
#anim_attachment_RH
#anim_attachment_LH
#anim_attachment_head[/code]
#
#[B][U]models\manhack.mdl[/U]
#	Attachments:[/B]
#[code]Eye
#Light[/code]
#
#[B][U]models\monk.mdl[/U]
#	Attachments:[/B]
#[code]eyes
#mouth
#chest
#muzzle
#forward
#anim_attachment_RH
#anim_attachment_LH
#anim_attachment_head[/code]
#
#[B][U]models\mossman.mdl[/U]
#	Attachments:[/B]
#[code]eyes
#mouth
#chest
#forward
#anim_attachment_RH
#anim_attachment_LH[/code]
#
#[B][U]models\odessa.mdl[/U]
#	Attachments:[/B]
#[code]eyes
#mouth
#chest
#forward
#anim_attachment_RH
#anim_attachment_LH
#anim_attachment_head[/code]
#
#[B][U]models\Police.mdl[/U]
#	Attachments:[/B]
#[code]eyes
#Center
#Chest
#LHand
#RHand
#zipline
#manhack
#forward
#anim_attachment_RH
#anim_attachment_LH
#anim_attachment_head
#baton[/code]
#
#[B][U]models\Police_Cheaple.mdl[/U]
#	Attachments:[/B]
#[code]eyes
#Center
#Chest
#LHand
#RHand[/code]
#
#[B][U]models\stalker.mdl[/U]
#	Attachments:[/B]
#[code]eyes[/code]
#
#[B][U]models\vortigaunt.mdl[/U]
#	Attachments:[/B]
#[code]eyes
#mouth
#leftclaw
#rightclaw
#hat
#ladel_attachment
#cleaver_attachment
#alyx_attachment
#attach_lShoulderBladeAim
#attach_rShoulderBladeAim
#nectar
#forward[/code]
#
#[B][U]models\Zombie\Classic.mdl[/U]
#	Attachments:[/B]
#[code]headcrab
#eyes
#head
#chest
#maw
#Blood_Left
#Blood_Right[/code]
#
#[B][U]models\Zombie\Fast.mdl[/U]
#	Attachments:[/B]
#[code]headcrab
#head
#chest
#Blood_Left
#Blood_Right
#Cable1a
#Cable1b
#Cable2a
#Cable2b[/code]
#
#[B][U]models\Zombie\Poison.mdl[/U]
#	Attachments:[/B]
#[code]eyes
#head
#chest
#headcrab1
#headcrab2
#headcrab3
#headcrab4
#headcrab5
#Blood_Left
#Blood_Right[/code]
#
#[B][U]models\Vehicles\prisoner_pod_inner.mdl[/U]
#	Attachments:[/B]
#[code]vehicle_driver_eyes
#enter1
#exit1
#exit3
#beamFX_attachment[/code]
#
#[B][U]models\humans\Group01\
#models\humans\Group02\
#models\humans\Group03\
#models\humans\Group03m\
#Female_01.mdl
#Female_02.mdl
#Female_03.mdl
#Female_04.mdl
#Female_06.mdl
#Female_07.mdl[/U][/B]
#	[B]Attachments:[/B]
#[code]eyes
#mouth
#chest
#forward
#anim_attachment_RH
#anim_attachment_LH[/code]
#
#[B][U]models\humans\Group01\
#models\humans\Group02\
#models\humans\Group03\
#models\humans\Group03m\
#Male_01.mdl
#Male_02.mdl
#Male_03.mdl
#Male_04.mdl
#Male_05.mdl
#Male_06.mdl
#Male_07.mdl
#Male_08.mdl
#Male_09.mdl[/U][/B]
#	[B]Attachments:[/B]
#[code]eyes
#mouth
#chest
#forward
#anim_attachment_RH
#anim_attachment_LH
#anim_attachment_head[/code][/QUOTE]
# 
