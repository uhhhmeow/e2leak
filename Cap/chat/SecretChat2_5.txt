@name SC 2.5
@inputs 
@outputs Self
@persist Colors:array Last:table Chat2:table 
@persist 
@persist Self #use this number for all your networking needs
@trigger 
@model models/gibs/metal_gib4.mdl
#by Captain Maim (STEAM_0:1:8017844)
interval(250)
if(!entity():owner()){selfDestruct() Die=1}
entity():setMaterial("models/shiny")
if(duped()){reset()}
if(!clk()){
    runOnChat(1)
    GMTOffset=-7  #set this to your GMT offset if needed.
    
    Network="Secret Chat" #change this to setup an entirely unique chat network. 
    ChatChannel="Chat" #this is the variable that sets the global variable channel. 
    

    Name="Secret Chat"
    if(owner():lastSaidTeam()){Hide=1}else{Hide=0}
    
    
    hideChat(Hide) 
    gShare(1)
    
    #D-Link Network Code Copy and paste this into your contraption
    gSetGroup(Network)
    runOnLast(1)
    Diag=0
    NetMax=16 #this is the maximum number of networked items, surplus are removed
    ID="ID"
    EID=entity():id()
    EIDS=toString(EID)
    if(!Self){
        NetT=glonDecodeTable(gGetStr(ID)) Scan=gGetStr(ID) Brk="<"
            for(C=1,NetMax){if(!Scan:find(Brk+C+Brk)){Self=C break}} 
            if(!Self){if(Diag){print("Self Destructing Excess Clones!")} selfDestructAll()
            }
        NetT[EIDS,string]=Brk+Self+Brk
        gSetStr(ID,glonEncode(NetT))
            if(Diag){print("Self="+Self)} #you can remove this if you want it's diagnostic
    }
    
    SelfS=Self:toString() #this is pertains to the chat chip
    
    if(last()){
        NetT=glonDecodeTable(gGetStr(ID))
        NetT[EIDS,string]=""
        gSetStr(ID,glonEncode(NetT))
        if(!Die){
            if(Diag){print("ID "+Self+" Deleted")} #diagnostic non critical
            print("You've just removed your "+Name+" chip.")
        }
        #This pertains to the chat chip not the D-link code
        Chat2=glonDecodeTable(gGetStr(ChatChannel))
        Chat2[SelfS,string]="" 
        Chat2[SelfS,number]=0
        gSetStr(ChatChannel,glonEncode(Chat2))
        #end chat chip
    }
    #End of D-Link Network Code
    
    if(first()){ #Generating color array
    H=255
    M=128
    L=0
    CB=64
        Colors:pushVector(vec(L,L+CB,H)) #1
        Colors:pushVector(vec(L,H-64,L)) #2
        Colors:pushVector(vec(H,L,L)) #3
        Colors:pushVector(vec(H,H,L)) #4
        Colors:pushVector(vec(H-CB,M-CB,L+CB)) #5
        Colors:pushVector(vec(L,H,M)) #6
        Colors:pushVector(vec(L,M,H)) #7
        Colors:pushVector(vec(M+CB,L,H-CB)) #8
        Colors:pushVector(vec(H-CB,H-CB,H-CB)) #9
        Colors:pushVector(vec(M,M,M)) #10
        Colors:pushVector(vec(M,L,M)) #11
        Colors:pushVector(vec(L,M+CB,M)) #12
        Colors:pushVector(vec(M,M,L)) #13
        Colors:pushVector(vec(M,H,H)) #14
        Colors:pushVector(vec(H,H-CB,M-CB)) #15
        Colors:pushVector(vec(H,M,H-CB)) #16
        print("Your chat color is: "+Colors[Self,vector])
    }
    entity():setColor(Colors[Self,vector])
    
    #Time Stamp
    O=":"
    #automatic compensation for spesific users
    SID=owner():steamID()
    if(SID=="STEAM_0:1:9395361"){GMTOffset=-8} #Little Ninja
    if(SID=="STEAM_0:0:7302826"){GMTOffset=-8} #Soap
    if(SID=="STEAM_0:1:19091184"){GMTOffset=-7} #TurtleMadness
    if(SID=="STEAM_0:0:19519871"){GMTOffset=-8} #Polymorph
    if(SID=="STEAM_0:1:8017844"){GMTOffset=-7-time("isdst")} #Captain Maim
    #end compenesation resume time stamp
    
    if(time("sec")<10){Sec="0"+time("sec")}else{Sec=toString(time("sec"))}
    if(time("min")<10){Min="0"+time("min")}else{Min=toString(time("min"))}
    Hour=time("hour")+GMTOffset
    if((Hour>=(24+GMTOffset))&(Hour<=23)){Day=time("day")}else{Day=time("day")-1}
    if(Hour<0){Hour+=10-GMTOffset*2}
    Time="["+Hour+O+Min+O+Sec+"]"   
    #end stamp
    
    
    
    #### This code is how I secretly send and receive chats
    Chat2=glonDecodeTable(gGetStr(ChatChannel))
    #send chat
    if(Hide){
        Chat=owner():lastSaid()
        if(chatClk()&lastSpoke()==owner()){        
            Message=owner():name()+": "+Chat
            Chat2[SelfS,string]=Message
            Chat2[SelfS,number]=1
            gSetStr(ChatChannel,glonEncode(Chat2))
            timer("wait",100)
         }
    }
    #receive chat
    
    if(chatClk()){
    timer("read",40)
    Chat2=glonDecodeTable(gGetStr(ChatChannel))
    }
    if(clk("read")){
       for(C=1,NetMax){
            Str=C:toString()
            if(Chat2[Str,number]){
                printColor(Colors[C,vector],Time+Chat2[Str,string])
            }
        }#end for
    }
    #clear send
    if(clk("wait")){
        Chat2[SelfS,number]=0
        Chat2[SelfS,string]=""
        gSetStr(ChatChannel,glonEncode(Chat2))
    }
    ####End of send/receive code
}
