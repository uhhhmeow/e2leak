@name Numpad Passthrough
@inputs Num:wirelink Suspend
@outputs Off Com:string
@persist Off Tog Run Com:string N0 N1 N2 N3 N4 N5 N6 N7 N8 N9 Slash Astrix Minus Plus Enter Del
@persist N0p:string N1p:string N2p:string N3p:string N4p:string N5p:string N6p:string N7p:string N8p:string N9p:string
@persist Plusp:string Minusp:string Astrixp:string Enterp:string Delp:string Slashp:string
@persist N0m:string N1m:string N2m:string N3m:string N4m:string N5m:string N6m:string N7m:string N8m:string N9m:string
@persist Plusm:string Minusm:string Astrixm:string Enterm:string Delm:string Slashm:string
interval(20)
if (N0p=="") {
N0p = "+gm_special 0;" N0m = "-gm_special 0;"
N1p = "+gm_special 1;" N1m = "-gm_special 1;"
N2p = "+gm_special 2;" N2m = "-gm_special 2;"
N3p = "+gm_special 3;" N3m = "-gm_special 3;"
N4p = "+gm_special 4;" N4m = "-gm_special 4;"
N5p = "+gm_special 5;" N5m = "-gm_special 5;"
N6p = "+gm_special 6;" N6m = "-gm_special 6;"
N7p = "+gm_special 7;" N7m = "-gm_special 7;"
N8p = "+gm_special 8;" N8m = "-gm_special 8;"
N9p = "+gm_special 9;" N9m = "-gm_special 9;"
Delp = "+gm_special 10;" Delm = "-gm_special 10;"
Enterp = "+gm_special 11;" Enterm = "-gm_special 11;"
Plusp = "+gm_special 12;" Plusm = "-gm_special 12;"
Minusp = "+gm_special 13;" Minusm = "-gm_special 13;"
Astrixp = "+gm_special 14;" Astrixm = "-gm_special 14;"
Slashp = "+gm_special 15" Slashm = "-gm_special 15"
}

if (Suspend & ~Suspend) {Off = !Off}
if (!Off) {
N0 = Num:number("0")
N1 = Num:number("1")
N2 = Num:number("2")
N3 = Num:number("3")
N4 = Num:number("4")
N5 = Num:number("5")
N6 = Num:number("6")
N7 = Num:number("7")
N8 = Num:number("8")
N9 = Num:number("9")
Slash = Num:number("/")
Astrix = Num:number("*")
Minus = Num:number("-")
Plus = Num:number("+")
Enter = Num:number("enter")
Del = Num:number(".")
Com = ""
if (N0) {Com += N0p}
if (N1) {Com += N1p}
if (N2) {Com += N2p}
if (N3) {Com += N3p}
if (N4) {Com += N4p}
if (N5) {Com += N5p}
if (N6) {Com += N6p}
if (N7) {Com += N7p}
if (N8) {Com += N8p}
if (N9) {Com += N9p}
if (Enter) {Com += Enterp}
if (Plus) {Com += Plusp}
if (Minus) {Com += Minusp}
if (Del) {Com += Delp}
if (Astrix) {Com += Astrixp}
if (Slash) {Com += Slashp}
Tog = N0 | N1 | N2 | N3 | N4 | N5 | N6 | N7 | N8 | N9 | Enter | Plus | Minus | Astrix | Del | Slash
if (Tog) {concmd(Com)} else {Com = ""}
Com = ""
if (!N0) {Com += N0m}
if (!N1) {Com += N1m}
if (!N2) {Com += N2m}
if (!N3) {Com += N3m}
if (!N4) {Com += N4m}
if (!N5) {Com += N5m}
if (!N6) {Com += N6m}
if (!N7) {Com += N7m}
if (!N8) {Com += N8m}
if (!N9) {Com += N9m}
if (!Enter) {Com += Enterm}
if (!Plus) {Com += Plusm}
if (!Minus) {Com += Minusm}
if (!Del) {Com += Delm}
if (!Astrix) {Com += Astrixm}
if (!Slash) {Com += Slashm}
Run = $N0 | $N1 | $N2 | $N3 | $N4 | $N5 | $N6 | $N7 | $N8 | $N9 | $Del | $Astrix | $Plus | $Minus | $Slash | $Enter
if (Run) {concmd(Com)} else {Com = ""}

} else {
Tog = N0 | N1 | N2 | N3 | N4 | N5 | N6 | N7 | N8 | N9 | Enter | Plus | Minus | Astrix | Del | Slash
if (Tog) {concmd(N0m + N1m + N2m + N3m + N4m + N5m + N6m + N7m + N8m + N9m + Enterm + Plusm + Minusm + Astrixm + Slashm)}
N0 = 0 
N1 = 0 
N2 = 0 
N3 = 0 
N4 = 0 
N5 = 0 
N6 = 0 
N7 = 0 
N8 = 0 
N9 = 0 
Slash = 0 
Astrix = 0 
Minus = 0 
Plus = 0 
Enter = 0
Del = 0
}
