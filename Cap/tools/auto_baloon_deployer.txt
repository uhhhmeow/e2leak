@name Auto Baloon Deployer
@inputs Deployed 
@outputs Deploy Popable Weld Force Length
@persist 
@trigger 
if(first()){
    Self=entity()
    Color=vec(255,255,255)
    Material="arrowire2"
    Width=0.5
    Deployer=Self:isConstrainedTo()
    Deployer:createWire(Self,"Popable?","Popable",Width,Color,Material)
    Deployer:createWire(Self,"Lenght","Length",Width,Color,Material)
    Deployer:createWire(Self,"Force","Force",Width,Color,Material)
    Deployer:createWire(Self,"Weld?","Weld",Width,Color,Material)
    Deployer:createWire(Self,"Deploy","Deploy",Width,Color,Material)
    Self:createWire(Deployer,"Deployed","Deployed",Width,Color,Material)
    Deploy=0
    Popable=0

}
Weld=0
Length=50
Force=200

interval(10)
if(!Deployed){
if(Deploy){Deploy=0}else{Deploy=1}
}
Popable=1

