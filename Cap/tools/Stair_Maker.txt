@name Stair Maker
@inputs Stool:wirelink 
@outputs Count Config:table 
@persist Config:table [Target TargetPoints XYZPoints]:array Count Counter
@trigger 
@model models/props_junk/PopCan01a.mdl
interval(20)
G=Stool:entity()
O=owner()
E=entity()
Config=Stool["Config",table]:clone()
Stool["Mode",string]="hydraulic"
#[
findInSphere(G:pos(),10000)
findSortByDistance(entity():pos())
findIncludeModel("")]#
StairCount=4
Model="models/props_doors/door03_slotted_left.mdl"
Spacing=48 #About the width of that prop

StairMode=0 #Just make stairs? If 1 then make stairs if 2 prepare stairs for stool turret

#[runOnChat(1)
if(chatClk()&owner():lastSaid()=="!Clean up"){
    while(Counter<Target:count()&perf()){
        Target[Counter,entity]:propDelete()
        
        Counter++
    }
}]#


if(first()){
    Target=array() #What to shoot
    TargetPoints=array() #Where to shoot to add hydraulics
    XYZPoints=array() #where to be when shooting
    Count=1
}


if(StairCount>Target:count()){
    
    for(C=Target:count(),Target:count()+4){
        if(Target:count()<StairCount){
            Target:pushEntity(propSpawn(Model,E:pos()+E:forward()*300+E:right()*Spacing*C+E:up()*18*(StairMode*C+!StairMode),ang(90,0,0),1))
            #print(Target:count())
        }
    }
    
}

if((StairCount==Target:count())&(TargetPoints:count()<Target:count())){
    #This generates a one time list of all relavant points for the stool turret.
    for(C=1,Target:count()){
        MoveLeft=-Target[C,entity]:right()*(Spacing/2) #This corrects the target points so they are at the center instead of the edge of the door.
        TargetPoints:pushVector(Target[C,entity]:pos()+Target[C,entity]:forward()*18+MoveLeft+vec(0,0,-3.5)) #This says where to shoot
        XYZPoints:pushVector(Target[C,entity]:pos()-Target[C,entity]:up()*100+MoveLeft) #This says where to be when shooting
        
        #holoCreate(C,TargetPoints[C,vector])  
        #holoCreate(C+StairCount,XYZPoints[C,vector])
    }
    
}

if(G&!StairMode){
    Stool["Isolate",number]=0
    while(Count<=Target:count()&perf()&(StairCount==Target:count())){
        #rangerFilter(G)
        #Aim=rangerOffset(5000,G:pos(),G:forward()):position()
        Aim=TargetPoints[Count,vector]
        
        
        #G=Gun:entity()
        Dir = G:pos()-Aim
        Q = quat(Dir:toAngle())*qRotation(vec(0,0,1),180) #X
        #Q = quat(Dir:toAngle())*qRotation(vec(0,1,0),-90) #Z
        #Q = quat(Dir:toAngle())*qRotation(vec(0,0,1),90) #Y
        #Q = quat(Dir:toAngle())*qRotation(vec(0,0,1),-90) #-Y
        G:applyTorque((150*G:toLocal(rotationVector(Q/quat(G))+G:pos()) - 12*G:angVelVector())*G:inertia())
        
        
        #This makes it move to a target XYZ
        XYZ=XYZPoints[Count,vector]
        Speed=7*(G:mass()/4)
        G:applyForce(Speed*(XYZ-G:pos())-(G:vel()/2)*G:mass())
        
        #This verifies where it's aiming
        rangerFilter(G)
        Laser=rangerOffset(1000,G:pos(),G:forward())
        LaserPos=Laser:pos()            
        Length=(Count-1)*18
        if(Length>Stool["Config",table]["addlength",number]){
            Config["addlength",number]=(Count-1)*18
            Stool["Config",table]=Config
        }

        #This should only trigger when the stool turret is at the right place and aiming at the target point
        if((XYZ:distance(G:pos())<5)&(LaserPos:distance(TargetPoints[Count,vector])<5)){
            
            Stool["RightClick",number]=1   
            
            Count++    
            
        } 
        

    
    }
    if(StairCount==Count){
        for(C=1,Target:count()){
            Target[C,entity]:propFreeze(0)
        }
    }
}


