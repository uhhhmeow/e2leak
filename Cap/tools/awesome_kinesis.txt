@name Awesome Kinesis
@inputs 
@outputs [Laser XYZ1]:vector 
@persist E:entity
@trigger 
#This is move to vector code 
runOnTick(1)
if(owner():keyUse()){E=owner():aimEntity()}
#E=entity()
T=0.015
rangerFilter(array(E,owner()))
Laser=rangerOffset(15000,owner():shootPos(),owner():eye()):position()
XYZ=Laser
XYZ1=owner():aimPos()
Dist=XYZ-E:pos()
Vel=Dist/T
E:applyForce(E:mass()*(Vel-E:vel()))

