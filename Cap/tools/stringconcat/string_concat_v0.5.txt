@name 
@inputs Keys:wirelink
@outputs Word:string On A B C Capslock
@persist Word:string Capslock Cap
interval(100)
On=1
Keys:setNumber("ValueOn",1)
A=Keys:number("A")
B=Keys:number("B")
C=Keys:number("C")
D=Keys:number("D")
E=Keys:number("E")
F=Keys:number("F")
G=Keys:number("G")
H=Keys:number("H")
I=Keys:number("I")
J=Keys:number("J")
K=Keys:number("K")
L=Keys:number("L")
M=Keys:number("M")
N=Keys:number("N")
O=Keys:number("O")
P=Keys:number("P")
Q=Keys:number("Q")
R=Keys:number("R")
S=Keys:number("S")
T=Keys:number("T")
U=Keys:number("U")
V=Keys:number("V")
W=Keys:number("W")
X=Keys:number("X")
Y=Keys:number("Y")
Z=Keys:number("Z")

Ctrl=Keys:number("Ctrl")
Space=Keys:number("Space")
Enter=Keys:number("Enter")
Tab=Keys:number("Tab")
Shift=Keys:number("Shift")
Caps=Keys:number("Caps")

Capslock=Cap
if (Cap==1 & Caps==1) {Capslock=0}
if (Cap==0 & Caps==1) {Capslock=1}
Cap=Capslock
##if (Caps==1 & ~Caps==1) {Capslock = !Capslock}

if (Ctrl==1) {Word=Word:sub(0,0)}
if (A==1 & Capslock==1) {Word += "A"} elseif (A==1 & Capslock != 1) {Word += "a"}
if (B==1 & Capslock==1) {Word += "B"} elseif (B==1 & Capslock != 1) {Word += "b"}
if (C==1 & Capslock==1) {Word += "C"} elseif (C==1 & Capslock != 1) {Word += "c"}
if (D==1 & Capslock==1) {Word += "D"} elseif (D==1 & Capslock != 1) {Word += "d"}
if (E==1 & Capslock==1) {Word += "E"} elseif (E==1 & Capslock != 1) {Word += "e"}
if (F==1 & Capslock==1) {Word += "F"} elseif (F==1 & Capslock != 1) {Word += "f"}
if (G==1 & Capslock==1) {Word += "G"} elseif (G==1 & Capslock != 1) {Word += "g"}
if (H==1 & Capslock==1) {Word += "H"} elseif (H==1 & Capslock != 1) {Word += "h"}
if (I==1 & Capslock==1) {Word += "I"} elseif (I==1 & Capslock != 1) {Word += "i"}
if (J==1 & Capslock==1) {Word += "J"} elseif (J==1 & Capslock != 1) {Word += "j"}
if (K==1 & Capslock==1) {Word += "K"} elseif (K==1 & Capslock != 1) {Word += "k"}
if (L==1 & Capslock==1) {Word += "L"} elseif (L==1 & Capslock != 1) {Word += "l"}
if (M==1 & Capslock==1) {Word += "M"} elseif (M==1 & Capslock != 1) {Word += "m"}
if (N==1 & Capslock==1) {Word += "N"} elseif (N==1 & Capslock != 1) {Word += "n"}
if (O==1 & Capslock==1) {Word += "O"} elseif (O==1 & Capslock != 1) {Word += "o"}
if (P==1 & Capslock==1) {Word += "P"} elseif (P==1 & Capslock != 1) {Word += "p"}
if (Q==1 & Capslock==1) {Word += "Q"} elseif (Q==1 & Capslock != 1) {Word += "q"}
if (R==1 & Capslock==1) {Word += "R"} elseif (R==1 & Capslock != 1) {Word += "r"}
if (S==1 & Capslock==1) {Word += "S"} elseif (S==1 & Capslock != 1) {Word += "s"}
if (T==1 & Capslock==1) {Word += "T"} elseif (T==1 & Capslock != 1) {Word += "t"}
if (U==1 & Capslock==1) {Word += "U"} elseif (U==1 & Capslock != 1) {Word += "u"}
if (V==1 & Capslock==1) {Word += "V"} elseif (V==1 & Capslock != 1) {Word += "v"}
if (W==1 & Capslock==1) {Word += "W"} elseif (W==1 & Capslock != 1) {Word += "w"}
if (X==1 & Capslock==1) {Word += "X"} elseif (X==1 & Capslock != 1) {Word += "x"}
if (Y==1 & Capslock==1) {Word += "Y"} elseif (Y==1 & Capslock != 1) {Word += "y"}
if (Z==1 & Capslock==1) {Word += "Z"} elseif (Z==1 & Capslock != 1) {Word += "z"}
