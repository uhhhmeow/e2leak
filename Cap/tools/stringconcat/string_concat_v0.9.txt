@name 
@inputs Keys:wirelink Keyboard
@outputs Word:string On Capslock Backspace Tab
@persist Word:string Capslock Cap 
interval(100)
On=1
Keys:setNumber("ValueOn",1)
A=Keys:number("A")
B=Keys:number("B")
C=Keys:number("C")
D=Keys:number("D")
E=Keys:number("E")
F=Keys:number("F")
G=Keys:number("G")
H=Keys:number("H")
I=Keys:number("I")
J=Keys:number("J")
K=Keys:number("K")
L=Keys:number("L")
M=Keys:number("M")
N=Keys:number("N")
O=Keys:number("O")
P=Keys:number("P")
Q=Keys:number("Q")
R=Keys:number("R")
S=Keys:number("S")
T=Keys:number("T")
U=Keys:number("U")
V=Keys:number("V")
W=Keys:number("W")
X=Keys:number("X")
Y=Keys:number("Y")
Z=Keys:number("Z")

Ctrl=Keys:number("Ctrl")
Space=Keys:number("Space")
Enter=Keys:number("Enter")
Tab=Keys:number("Tab")
Shift=Keys:number("Shift")
Caps=Keys:number("Caps")
if (Keyboard==127) {Backspace = 1} else {Backspace = 0}

Capslock=Cap
if (Cap==1 & Caps==1) {Capslock=0}
if (Cap==0 & Caps==1) {Capslock=1}
Cap=Capslock
##if (Caps==1 & ~Caps==1) {Capslock = !Capslock}
Length = Word:length()
Length -= 1
if (Ctrl) {Word=Word:sub(0,0)}
if (Space) {Word += " "}
if (Backspace) {Word = Word:sub(1,Length)}
if (A & Capslock) {Word += "A"} elseif (A & Capslock != 1) {Word += "a"}
if (B & Capslock) {Word += "B"} elseif (B & Capslock != 1) {Word += "b"}
if (C & Capslock) {Word += "C"} elseif (C & Capslock != 1) {Word += "c"}
if (D & Capslock) {Word += "D"} elseif (D & Capslock != 1) {Word += "d"}
if (E & Capslock) {Word += "E"} elseif (E & Capslock != 1) {Word += "e"}
if (F & Capslock) {Word += "F"} elseif (F & Capslock != 1) {Word += "f"}
if (G & Capslock) {Word += "G"} elseif (G & Capslock != 1) {Word += "g"}
if (H & Capslock) {Word += "H"} elseif (H & Capslock != 1) {Word += "h"}
if (I & Capslock) {Word += "I"} elseif (I & Capslock != 1) {Word += "i"}
if (J & Capslock) {Word += "J"} elseif (J & Capslock != 1) {Word += "j"}
if (K & Capslock) {Word += "K"} elseif (K & Capslock != 1) {Word += "k"}
if (L & Capslock) {Word += "L"} elseif (L & Capslock != 1) {Word += "l"}
if (M & Capslock) {Word += "M"} elseif (M & Capslock != 1) {Word += "m"}
if (N & Capslock) {Word += "N"} elseif (N & Capslock != 1) {Word += "n"}
if (O & Capslock) {Word += "O"} elseif (O & Capslock != 1) {Word += "o"}
if (P & Capslock) {Word += "P"} elseif (P & Capslock != 1) {Word += "p"}
if (Q & Capslock) {Word += "Q"} elseif (Q & Capslock != 1) {Word += "q"}
if (R & Capslock) {Word += "R"} elseif (R & Capslock != 1) {Word += "r"}
if (S & Capslock) {Word += "S"} elseif (S & Capslock != 1) {Word += "s"}
if (T & Capslock) {Word += "T"} elseif (T & Capslock != 1) {Word += "t"}
if (U & Capslock) {Word += "U"} elseif (U & Capslock != 1) {Word += "u"}
if (V & Capslock) {Word += "V"} elseif (V & Capslock != 1) {Word += "v"}
if (W & Capslock) {Word += "W"} elseif (W & Capslock != 1) {Word += "w"}
if (X & Capslock) {Word += "X"} elseif (X & Capslock != 1) {Word += "x"}
if (Y & Capslock) {Word += "Y"} elseif (Y & Capslock != 1) {Word += "y"}
if (Z & Capslock) {Word += "Z"} elseif (Z & Capslock != 1) {Word += "z"}
