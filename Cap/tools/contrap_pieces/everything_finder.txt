@name Omni Entity Lister
@inputs N
@outputs Model:string EntList:array XYZ:vector Count ID Class:string 
@outputs Mass Radius Angles:angle Owner:string
@persist EntList:array E:entity
interval(500)
findClearBlackList()
findInSphere(entity():pos(),1000000)
#findSortByDistance(entity():pos())
#findIncludeClass("*")
EntList = array()
EntList = findToArray()
E = EntList:entity(N)
Model = E:model()
XYZ = E:pos()
Count = EntList:count()
ID = E:id()
Class = E:type()
Owner = E:owner():name()
Mass = E:mass()
Radius = E:radius()
Angles = E:angles()

