@name Class Finder
@inputs EntID
@outputs Class:string
@persist
Class = entity(EntID):type()
