@inputs Num:wirelink Pulser
@outputs TickTime On Com:string Run Player:string 
@persist Player:string Run
interval(100)
TickTime = 1.0
On = 1
N0 = Num:number("0")
N1 = Num:number("1")
N2 = Num:number("2")
N3 = Num:number("3")
N4 = Num:number("4")
N5 = Num:number("5")
N6 = Num:number("6")
N7 = Num:number("7")
N8 = Num:number("8")
N9 = Num:number("9")
Slash = Num:number("/")
Astrix = Num:number("*")
Minus = Num:number("-")
Plus = Num:number("+")
Enter = Num:number("enter")
Del = Num:number(".")
Team = "say_team "

if (!Del) {Start = 0}
if (Del) {Start = 10}
T1 = entity(Start + 1)  
T2 = entity(Start + 2)
T3 = entity(Start + 3)
T4 = entity(Start + 4)
T5 = entity(Start + 5)
T6 = entity(Start + 6)
T7 = entity(Start + 7)
T8 = entity(Start + 8)
T9 = entity(Start + 9)
T0 = entity(Start + 10)

S = " "
T1N = T1:name() + S
T2N = T2:name() + S
T3N = T3:name() + S
T4N = T4:name() + S
T5N = T5:name() + S
T6N = T6:name() + S
T7N = T7:name() + S
T8N = T8:name() + S
T9N = T9:name() + S
T0N = T0:name() + S


I1 = T1:id()
I2 = T2:id()
I3 = T3:id()
I4 = T4:id()
I5 = T5:id()
I6 = T6:id()
I7 = T7:id()
I8 = T8:id()
I9 = T9:id()
I0 = T0:id()

if (I1 != 0) {N11 = " [1] "} else {N11 = ""}
if (I2 != 0) {N12 = " [2] "} else {N12 = ""}
if (I3 != 0) {N13 = " [3] "} else {N13 = ""}
if (I4 != 0) {N14 = " [4] "} else {N14 = ""}
if (I5 != 0) {N15 = " [5] "} else {N15 = ""}
if (I6 != 0) {N16 = " [6] "} else {N16 = ""}
if (I7 != 0) {N17 = " [7] "} else {N17 = ""}
if (I8 != 0) {N18 = " [8] "} else {N18 = ""}
if (I9 != 0) {N19 = " [9] "} else {N19 = ""}
if (I0 != 0) {N10 = " [0] "} else {N10 = ""}
P = ")" A = "1" B = "2" C = "3" D = "4" E = "5" F = "6" G = "7" H = "8" I = "9" J = "0" Per = "." Sl = "/" Pl = "+" Mi = "-" En = "Enter" As = "*"
Ex = "!"
C01 = "hp "
C02 = "god " 
C03 = "ungod "  
C04 = "cloak "
C05 = "uncloak "
C06 = "ragdoll "
C07 = "unragdoll "
C08 = "goto "
C09 = "bring "
C10 = "tp "

C11 = "ban " 
C12 = "kick " 
C13 = "jail " 
C14 = "unjail " 
C15 = "noclip " 
C16 = "ignite " 
C17 = "maul " 
C18 = "slap "
C19 = "whip "

C20 = "slay "
C21 = "sslay "
C22 = "freeze "
C23 = "unfreeze "
C24 = "ghost "
C25 = "unghost "
C26 = "blind "
C27 = "gag "
C28 = "ungag "
C29 = "mute "
C30 = "unmute "
Next = "Next"

Key = N1 | N2 | N3 | N4 | N5 | N6 | N7 | N8 | N9 | N0
Run = 0
if (Slash & !Astrix & !Minus & !Plus & !Key) {Run = 1  Com = (Team + (N11 + T1N) + (N12 + T2N) + (N13 + T3N) + (N14 + T4N)
 + (N15 + T5N) + (N16 + T6N) + (N17 + T7N) + (N18 + T8N) + (N19 + T9N) + (N10 +T0N))} else {Com = ""}


if (Astrix & !Minus & !Plus & !Slash & !Key) {Run = 1  Com = (Team + (A + P + C01) + (B + P + C02) + (C + P + C03) + (D + P + C04) +
(E + P + C05) + (F + P + C06) + (G + P + C07) + (H + P + C08) + (I + P + C09)  + (J + P + C10) + (Mi + P + Next))} 

if (!Astrix & Minus & !Plus & !Slash & !Key) {Run = 1  Com = (Team + (A + P + C11) + (B + P + C12) + (C + P + C13) + (D + P + C14) +
(E + P + C15) + (F + P + C16) + (G + P + C17) + (H + P + C18) + (I + P + C19)  + (J + P + C20) + (Pl + P + Next))} 

if (!Astrix & !Minus & Plus & !Slash & !Key) {Run = 1  Com = (Team + (A + P + C21) + (B + P + C22) + (C + P + C23) + (D + P + C24) +
(E + P + C25) + (F + P + C26) + (G + P + C27) + (H + P + C28) + (I + P + C29)  + (J + P + C30) + (As + P + Next))} 



if (Slash & N1 & I1!=0) {Player = T1N} elseif
(Slash & N2 & I2!=0) {Player = T2N} elseif
(Slash & N3 & I3!=0) {Player = T3N} elseif
(Slash & N4 & I4!=0) {Player = T4N} elseif
(Slash & N5 & I5!=0) {Player = T5N} elseif
(Slash & N6 & I6!=0) {Player = T6N} elseif
(Slash & N7 & I7!=0) {Player = T7N} elseif
(Slash & N8 & I8!=0) {Player = T8N} elseif
(Slash & N9 & I9!=0) {Player = T9N} elseif
(Slash & N0 & I0!=0) {Player = T0N} 



if (Astrix & N1) {Run = 1 Com = (Team + Ex + C01 + Player + "100")} if (Minus & N1) {Run = 1 Com = (Team + Ex + C11 + Player)} if (Plus & N1) {Run = 1 Com = (Team + Ex + C21 + Player)}
if (Astrix & N2) {Run = 1 Com = (Team + Ex + C02 + Player)} if (Minus & N2) {Run = 1 Com = (Team + Ex + C12 + Player)} if (Plus & N2) {Run = 1 Com = (Team + Ex + C22 + Player)}
if (Astrix & N3) {Run = 1 Com = (Team + Ex + C03 + Player)} if (Minus & N3) {Run = 1 Com = (Team + Ex + C13 + Player)} if (Plus & N3) {Run = 1 Com = (Team + Ex + C23 + Player)}
if (Astrix & N4) {Run = 1 Com = (Team + Ex + C04 + Player + "255")} if (Minus & N4) {Run = 1 Com = (Team + Ex + C14 + Player)} if (Plus & N4) {Run = 1 Com = (Team + Ex + C24 + Player)}
if (Astrix & N5) {Run = 1 Com = (Team + Ex + C05 + Player)} if (Minus & N5) {Run = 1 Com = (Team + Ex + C15 + Player)} if (Plus & N5) {Run = 1 Com = (Team + Ex + C25 + Player)}
if (Astrix & N6) {Run = 1 Com = (Team + Ex + C06 + Player)} if (Minus & N6) {Run = 1 Com = (Team + Ex + C16 + Player)} if (Plus & N6) {Run = 1 Com = (Team + Ex + C26 + Player)}
if (Astrix & N7) {Run = 1 Com = (Team + Ex + C07 + Player)} if (Minus & N7) {Run = 1 Com = (Team + Ex + C17 + Player)} if (Plus & N7) {Run = 1 Com = (Team + Ex + C27 + Player)}
if (Astrix & N8) {Run = 1 Com = (Team + Ex + C08 + Player)} if (Minus & N8) {Run = 1 Com = (Team + Ex + C18 + Player)} if (Plus & N8) {Run = 1 Com = (Team + Ex + C28 + Player)}
if (Astrix & N9) {Run = 1 Com = (Team + Ex + C09 + Player)} if (Minus & N9) {Run = 1 Com = (Team + Ex + C19 + Player)} if (Plus & N9) {Run = 1 Com = (Team + Ex + C29 + Player)}
if (Astrix & N0) {Run = 1 Com = (Team + Ex + C10 + Player)} if (Minus & N0) {Run = 1 Com = (Team + Ex + C20 + Player)} if (Plus & N0) {Run = 1 Com = (Team + Ex + C30 + Player)}


if (Pulser & Run) {concmd(Com)}
