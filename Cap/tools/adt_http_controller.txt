@name ADT HTTP Controller
@inputs ADT:wirelink 
@inputs Send 
@inputs Receive
@inputs Spawn #DataOutput:string
@inputs PercentSent Percent Loaded 
@outputs B ReadEntity PercentReceived PercentSent BlockCount CanSpawn Ready [Buffer]:array DataInput:string
@persist ReadE SerialE DI DIA ClearI Run Send Spawn Buffer:array Type:string
@persist B
interval(100)
Scanned = ADT["Entity Scaned",number] 		        #After you scanned/read an Entity, this will output 1, otherwise 0
Serialized = ADT["Entity Serialised",number]      	#This will go to 1 after you serialised the Entity
DataOutput = ADT["Data Output",string] 		#This is where the serialised data blocks go out. Has to be connected to an "Data Input" to make any sense
Sending = ADT["Sending",number] 		        #This is 1 while the device send the data blocks, otherwise 0
Receiving = ADT["Receiving",number]	             	#Same as "Sending"
SOBC = ADT["Serialised Output Block Count",number] 	#This is how many blocks your serialised data has
SIBC = ADT["Serialised Input Block Count",number] 	#This is how many blocks the data you're just receiving has)
BlocksSent = ADT["Blocks Sended",number] 		#While the sending process, this outputs how many Blocks are already sent to the other device
BlocksReceived = ADT["Blocks Received",number] 		#same as "Blocks Sended", but for receiving
DIA = ADT["Deserialisable Input Available",number]	#After successfully receiving this turns to 1, means you can deserialise something
CanSpawn = ADT["Spawn Available",number] 		#After successfully deserialising this turns to 1
PieceSize = 4005 #4005=4012 #200 is default
runOnChat(1)
runOnFile(1)
Chat=owner():lastSaid()
Filename="ADT/second_dup_test.txt"
if(chatClk()&lastSpoke()==owner()){
    if(Chat:find("send it")){Sendit=1 Buffer=array()}
    if(Chat:find("read it")){ReadE=1}
    if(Chat:find("spawn it")){SpawnE=1}
    if(Chat:find("serialize")){SerialE=1}
    if(Chat:find("load it")){Load=1}
    if(Chat:find("deserial")){DI=1}
    if(Chat:find("clear it")){ClearI=1}
}else{
    Sending=SerialE=ReadE=SpawnE=0

}
if (~Loaded & Loaded!=0) {Run = 1} else {Run = 0}
#if (Send) { 
#    
#    if (!Scanned & Run) {
#        ReadE = 1 
#        SerialE = 1
#    } else {
#        ReadE = 0 
#        SerialE = 0
#    }
#    if (Scanned & Serialized & !Sending & Run) {SendData = 1} else {SendData = 0}
#}
#if (Receive) {
#    if (DIA) {DI = 1} else {DI = 0}
#    if (CanSpawn) {Ready = 1} else {Ready = 0}
#}
#if (Spawn & Ready & ~Spawn) {SpawnE = 1} else {SpawnE = 0}


PercentSent = (BlocksSent / SOBC) * 100
PercentReceived = (BlocksReceived / SIBC) * 100 


if(changed(DataOutput)&DataOutput){
    Buffer:pushString(DataOutput)
}

if(Buffer:count()&!DataOutput&!Sending){
    fileWrite(Filename,glonEncode(Buffer))
    print("Storing contraption as "+Filename+" "+Buffer:count()+" chunks were saved.")
    Buffer=array()
}

if(Load){
    fileLoad(Filename)
    print("Loading "+Filename)
}

if(fileClk()){
    Buffer=glonDecode(fileRead())
    print("Buffered "+Buffer:count()+" Chunks!"+" Ops: "+opcounter())
    timer("load",500)
    B=1
}

if(clk("load")){
    timer("load",500)
    if(B>Buffer:count()){stoptimer("load")}
    print("sending data")
    DataInput=Buffer[B,string]
    print("B "+B)
    B++
}

#[#
if(Save){
    Length=DataOutput:length()
        timer("delay",1)
        Chunks=array()
        
        for(C=1,ceil(Length/4012)){
            First=(C-1)*4012
            Second=C*4012
            Chunks[C,string]=DataOutput:sub(First,Second)
        }
        fileWrite(Filename,glonEncode(Chunks))
        print("Storing contraption as "+Filename+" "+Chunks:count()+" chunks were saved.")
        
        
}
]#


#[
if(Chunks:count()&clk("delay")){
    httpRequest("http://captainmaim:9000/Upload/upload/dupe.php?action=write&file="+Filename+"&glon="+Chunks:shiftString())
    timer("delay",3000)
    
}
]#
ADT["Read Entity",number]=ReadE #(This inputs makes the Device read in a constraption. It works similar as the right click with normal adv. dupe.)
ADT["Serialise Entity",number]=SerialE #If you trigger this input, the device will convert the scanned constraption to a string, which is then splitted into blocks. You will not see any physical effect or anything
ADT["Piece Size",number]=PieceSize #This says how big the blocks created be the Serialise function will be (in Bytes). But keep in mind, that if you want to transfer blocks with the WSR, you should use smaller blocks (like 128/256)
ADT["Start Data Sending",number]=Sendit#I think you can guess what it does
ADT["Data Input",string]=DataInput #This is where you connect a "Data Output" from another Teleporter to, to receive the serialised Data
ADT["Spawn Entity",number]=SpawnE #If you successfully received and deserialised data, you can spawn the Entity
ADT["Clear Input",number]=ClearI #With this you can clear the data you received
ADT["Deserialise Input",number]=DI
