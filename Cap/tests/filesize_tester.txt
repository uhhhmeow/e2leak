@name Filesize tester
@inputs
@outputs Length ChunkSize ETA Min Sec
@persist 
@trigger 
File="http://captainmaim:9000/Dups/users/CaptainMaim/Morthotos/adv_duplicator/STEAM_0_1_8017844/Others%20contraptions/%3DOld%20Catagories%3D/%3DBuildings%3D/latrine.txt"
httpRequest(File)
runOnHTTP(1)
if(httpClk()){
    Data=httpData()
    Length=Data:length()
    
    Chunks=array()
    CSize=4012
    for(C=1,ceil(Length/CSize)){
        First=(C-1)*CSize
        Second=C*CSize
        Chunks[C,string]=Data:sub(First,Second)
    }
    ChunkSize=Chunks:count()
    ETA=3*ChunkSize
    Min=floor(ETA/60)
    Sec=((ETA/60)-Min)*60
    if(Sec<10){Sec2="0"+Sec}else{Sec2=toString(Sec)}
    print("length "+Length+" ETA "+ETA+" Seconds, or "+Min+":"+Sec2+" Minutes")
}
