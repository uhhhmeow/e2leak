@name Data Compression Test
@inputs 
@outputs B [Path Encoded]:string Decoded Chunk
@persist B Compress Half Decompress Block Path:string Value:array
@persist Base:array #baseX  
@persist MCD:table            #Model Compression Dictionary
@trigger 
if(first()){
    #numerical data compression baseX add indexes or subtract to automatically change base
    Base=array()
    Base[0,string]="0"
    Base:pushString("1")
    Base:pushString("2")
    Base:pushString("3")
    Base:pushString("4")
    Base:pushString("5")
    Base:pushString("6")
    Base:pushString("7")
    Base:pushString("8")
    Base:pushString("9")
    Base:pushString("a")
    Base:pushString("b")
    Base:pushString("c")
    Base:pushString("d")
    Base:pushString("e")
    Base:pushString("f")
    Base:pushString("g")
    Base:pushString("h")
    Base:pushString("i")
    Base:pushString("j")
    Base:pushString("k")
    Base:pushString("l")
    Base:pushString("m")
    Base:pushString("n")
    Base:pushString("o")
    Base:pushString("p")
    Base:pushString("q")
    Base:pushString("r")
    Base:pushString("s")
    Base:pushString("t")
    Base:pushString("u")
    Base:pushString("v")
    Base:pushString("w")
    Base:pushString("x")
    Base:pushString("y")
    Base:pushString("z")
    Base:pushString("A")
    Base:pushString("B")
    Base:pushString("C")
    Base:pushString("D")
    Base:pushString("E")
    Base:pushString("F")
    Base:pushString("G")
    Base:pushString("H")
    Base:pushString("I")
    Base:pushString("J")
    Base:pushString("K")
    Base:pushString("L")
    Base:pushString("M")
    Base:pushString("N")
    Base:pushString("O")
    Base:pushString("P")
    Base:pushString("Q")
    Base:pushString("R")
    Base:pushString("S")
    Base:pushString("T")
    Base:pushString("U")
    Base:pushString("V")
    Base:pushString("W")
    Base:pushString("X")
    Base:pushString("Y")
    Base:pushString("Z")
    Base:pushString("*")
    Base:pushString("|")
    Base:pushString("~")
    Base:pushString("<")
    Base:pushString(">")
    Base:pushString("[")
    Base:pushString("]")
    Base:pushString("{")
    Base:pushString("}")
    Base:pushString("(")
    Base:pushString(")")
    Base:pushString(".")
    Base:pushString("`")
    #Base:pushString("_")
    Base:pushString("�")
    Base:pushString("�")
    Base:pushString("�")
    Base:pushString("�")
    Base:pushString("�")
    Base:pushString("�")
    Base:pushString("�")
    Base:pushString("�")
    Base:pushString("?")
    Base:pushString("�")
    Base:pushString("�")
    Base:pushString("�")
    Base:pushString("�")
    Base:pushString("?")
    Base:pushString("�")
    Base:pushString("�")
    Base:pushString("�")
    Base:pushString("�")
    Base:pushString("�")
    Base:pushString("�")
    Base:pushString("�")
    Base:pushString("�")
    Base:pushString("�")
    Base:pushString("�")
    Base:pushString("�")
    Base:pushString("_")
    
    
    #model compression Dictionary
    MCD=table()
    MCD:pushArray(array("001a","0*"))
    MCD:pushArray(array("002a","0|"))
    MCD:pushArray(array("003a","0~"))
    MCD:pushArray(array("004a","0<"))
    MCD:pushArray(array("001b","0>"))
    MCD:pushArray(array("001c","0["))
    MCD:pushArray(array("01a","1*"))
    MCD:pushArray(array("001","1|"))
    MCD:pushArray(array("025","2*"))
    MCD:pushArray(array("02a","2|"))
    MCD:pushArray(array("02b","2~"))
    MCD:pushArray(array("03a","3*"))
    MCD:pushArray(array("04a","4*"))
    MCD:pushArray(array("075","7*"))
    MCD:pushArray(array("angle","a*"))
    MCD:pushArray(array("antlion","A*"))
    MCD:pushArray(array("AntLion","a|"))
    MCD:pushArray(array("alyx","A|"))
    MCD:pushArray(array("blocks/","b*"))
    MCD:pushArray(array("combine","B*"))
    MCD:pushArray(array("black","b|"))
    MCD:pushArray(array("props_borealis/","B|"))
    MCD:pushArray(array("borealis","b~"))
    MCD:pushArray(array("props_building_details/storefront_template","B~"))
    MCD:pushArray(array("blue","b<"))
    MCD:pushArray(array("bridge","B<"))
    MCD:pushArray(array("breen","b>"))
    MCD:pushArray(array("barricade","B>"))
    MCD:pushArray(array("bucket","b<"))
    MCD:pushArray(array("bar","B<"))
    MCD:pushArray(array("bevel","b>"))
    MCD:pushArray(array("Combine","B>"))
    MCD:pushArray(array("Barney","b["))
    MCD:pushArray(array("cube","c*"))
    MCD:pushArray(array("construct/","C*"))
    MCD:pushArray(array("cone","c|"))
    MCD:pushArray(array("chess","C|"))
    MCD:pushArray(array("corkscrew","c~"))
    MCD:pushArray(array("coastertrain","C~"))
    MCD:pushArray(array("canister","c<"))
    MCD:pushArray(array("props_c17/","C<"))
    MCD:pushArray(array("chunk","c>"))
    MCD:pushArray(array("concrete","C>"))
    MCD:pushArray(array("canal","c["))
    MCD:pushArray(array("chair","C["))
    MCD:pushArray(array("cannister","c]"))
    MCD:pushArray(array("cardboard_box","C]"))
    MCD:pushArray(array("crate","c{"))
    MCD:pushArray(array("connection","C{"))
    MCD:pushArray(array("control","c}"))
    MCD:pushArray(array("chacters","C}"))
    MCD:pushArray(array("down","d*"))
    MCD:pushArray(array("door","D*"))
    MCD:pushArray(array("drawer","d|"))
    MCD:pushArray(array("desk","D|"))
    MCD:pushArray(array("dumpster","d~"))
    MCD:pushArray(array("explosive","e*"))
    MCD:pushArray(array("furniture","f*"))
    MCD:pushArray(array("fence","F*"))
    MCD:pushArray(array("filecabinet","f|"))
    MCD:pushArray(array("fuselage","F|"))
    MCD:pushArray(array("female","f~"))
    MCD:pushArray(array("fast","F~"))
    MCD:pushArray(array("Floor","f<"))
    MCD:pushArray(array("geometric/","g*"))
    MCD:pushArray(array("gibs/","G*"))
    MCD:pushArray(array("gib","g|"))
    MCD:pushArray(array("gear","G|"))
    MCD:pushArray(array("gravestone","g~"))
    MCD:pushArray(array("glass","G~"))
    MCD:pushArray(array("guard","g<"))
    MCD:pushArray(array("group","G<"))
    MCD:pushArray(array("gman_high","g>"))
    MCD:pushArray(array("hunter/","h*"))
    MCD:pushArray(array("huge","H*"))
    MCD:pushArray(array("helicopter","h|"))
    MCD:pushArray(array("headcrab","H|"))
    MCD:pushArray(array("humans","h~"))
    MCD:pushArray(array("charple","H~"))
    MCD:pushArray(array("interior","i*"))
    MCD:pushArray(array("piece","I*"))
    MCD:pushArray(array("pinion","i|"))
    MCD:pushArray(array("jetbody","j*"))
    MCD:pushArray(array("Kleiner","k*"))
    MCD:pushArray(array("katharsmodels","K*"))
    MCD:pushArray(array("combatmodels","k|"))
    MCD:pushArray(array("plastic","l*"))
    MCD:pushArray(array("large","L*"))
    MCD:pushArray(array("ladder","l|"))
    MCD:pushArray(array("classic","L|"))
    MCD:pushArray(array("misc/","m*"))
    MCD:pushArray(array("mechanics/","M*"))
    MCD:pushArray(array("metal","m|"))
    MCD:pushArray(array("machine","M|"))
    MCD:pushArray(array("medium","m~"))
    MCD:pushArray(array("male","M~"))
    MCD:pushArray(array("mossman","m|"))
    MCD:pushArray(array("Chipstiks_PCMod_Models","M|"))
    MCD:pushArray(array("oildrum","o*"))
    MCD:pushArray(array("ornament","O*"))
    MCD:pushArray(array("plates/","p*"))
    MCD:pushArray(array("plate","P*"))
    MCD:pushArray(array("panel","p|"))
    MCD:pushArray(array("props_canal","P|"))
    MCD:pushArray(array("props_citizen_tech/","p~"))
    MCD:pushArray(array("props_combine","P~"))
    MCD:pushArray(array("props_debris/","p<"))
    MCD:pushArray(array("props_docks/","P<"))
    MCD:pushArray(array("pole","p>"))
    MCD:pushArray(array("props_foliage","P>"))
    MCD:pushArray(array("props_interiors/","p["))
    MCD:pushArray(array("player","P["))
    MCD:pushArray(array("props_junk/","p]"))
    MCD:pushArray(array("props_lab/","P]"))
    MCD:pushArray(array("props_trainstation/","p{"))
    MCD:pushArray(array("props_vehicles/","P{"))
    MCD:pushArray(array("props_wasteland/","p}"))
    MCD:pushArray(array("prison","P}"))
    MCD:pushArray(array("squarecap","q*"))
    MCD:pushArray(array("roundthing","r*"))
    MCD:pushArray(array("rails","R*"))
    MCD:pushArray(array("robotics","r|"))
    MCD:pushArray(array("props_","R|"))
    MCD:pushArray(array("propeller","r~"))
    MCD:pushArray(array("Prison","R~"))
    MCD:pushArray(array("roller","r|"))
    MCD:pushArray(array("sphere","s*"))
    MCD:pushArray(array("straight","S*"))
    MCD:pushArray(array("slope","s|"))
    MCD:pushArray(array("special","S|"))
    MCD:pushArray(array("small","s~"))
    MCD:pushArray(array("sawblade","S~"))
    MCD:pushArray(array("sign","s<"))
    MCD:pushArray(array("station","S<"))
    MCD:pushArray(array("storage","s>"))
    MCD:pushArray(array("slider","S>"))
    MCD:pushArray(array("solid_steel","s["))
    MCD:pushArray(array("soldier","S["))
    MCD:pushArray(array("super","s]"))
    MCD:pushArray(array("coastertrack/","t*"))
    MCD:pushArray(array("tunnel","T*"))
    MCD:pushArray(array("turn","t|"))
    MCD:pushArray(array("tree","T|"))
    MCD:pushArray(array("trash","t~"))
    MCD:pushArray(array("track","T~"))
    MCD:pushArray(array("train","t<"))
    MCD:pushArray(array("tube","T<"))
    MCD:pushArray(array("torso","t>"))
    MCD:pushArray(array("turret","T>"))
    MCD:pushArray(array("utility","u*"))
    MCD:pushArray(array("v_models","v*"))
    MCD:pushArray(array("wood","w*"))
    MCD:pushArray(array("wire","W*"))
    MCD:pushArray(array("white","w|"))
    MCD:pushArray(array("wheel","W|"))
    MCD:pushArray(array("window","w~"))
    MCD:pushArray(array("weapon","W~"))
    MCD:pushArray(array("wing","w<"))
    MCD:pushArray(array("w_models","W<"))
    MCD:pushArray(array("props_phx","x*"))
    MCD:pushArray(array("xqm/","X*"))
    MCD:pushArray(array("phx","x|"))
    MCD:pushArray(array("xeon133","X|"))
    MCD:pushArray(array("/","."))
    MCD:pushArray(array("zombie","z*"))

    B=1
    Compress=0
    Decompress=1
    Encode=1
    Decode=1
    Count=MCD:count()
    #Path="models\Humans/Group02/Male_05.mdl"
    Path="h~.G<03.f~_07"
    Path=Path:lower()
    String="__" #saved in little edian
    #Block=8550
    Block=10100
    Number=-9 #5625 = 00? 5623 is limit
    Value=array()
    print("encoding in base "+Base:count()) #you can toss this if you want
}


if(Compress){
    if(B<=MCD:count()){timer("compress",20)}
    while(B<=MCD:count()&perf()){
        M=MCD[B,array]
        Path=Path:replace(M[1,string],M[2,string])
        B++
    }
    if(B>MCD:count()){
        Path=Path:replace("models",""):replace(".mdl","")
        Compress=0 
        stoptimer("compress")
    }
}
if(Decompress){
    if(B<=MCD:count()){timer("decompress",20)}
    while(B<=MCD:count()&perf()){
        M=MCD[B,array]
        Path=Path:replace(M[2,string],M[1,string])
        B++
    }
    if(B>MCD:count()){
        Path="models/"+Path+".mdl" 
        Decompress=0
        stoptimer("decompress")
    }
}

if(Encode){
    BASE=Base:count()
    if(Number<0){Number=abs(Number) Neg=1}else{Neg=0}
    #if(Number<0){Number=Number+Half Neg=1}else{Neg=0}
    if(Neg){Number+=(Block/2)}
    for(C=0,1){
        
        Byte=Base[floor(Number/BASE^C)%BASE,string]
        if(Byte){Value:pushString(Byte)}
    }
    #if(Neg){Value:unshiftString("-")}
    Encoded=Value:concat()
    Encode=0
}

if(Decode){
    Based=invert(Base)
    #if(String:find("-")){Neg=1 String=String:replace("-","")}else{Neg=0}
    
    for(C=1,3){
        Chunk=Based[String:index(C),number]
        Decoded+=Chunk*Base:count()^(C-1)
    }
    if(Decoded>(Block/2)){Decoded=(Decoded-(Block/2))*-1}
    #if(Neg){Decoded=Decoded*-1}
}

#    Unsigned Decoding

#    if(Decode){
#        Based=invert(Base)
#        if(String:find("-")){Neg=1 String=String:replace("-","")}else{Neg=0}
#        
#        for(C=1,3){
#            Chunk=Based[String:index(C),number]
#            Decoded+=Chunk*Base:count()^(C-1)
#        }
#        if(Neg){Decoded=Decoded*-1}
#    }

#                if(Encode){
#                    BASE=Base:count()
#                    if(Number<0){Number=abs(Number) Neg=1}else{Neg=0}
#                    #if(Number<0){Number=Number+Half Neg=1}else{Neg=0}
#                    
#                    for(C=0,1){
#                        
#                        Byte=Base[floor(Number/BASE^C)%BASE,string]
#                        if(Byte){Value:pushString(Byte)}
#                    }
#                    if(Neg){Value:unshiftString("-")}
#                    Encoded=Value:concat()
#                    Encode=0
#                }
