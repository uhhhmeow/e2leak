@name Death Sensor
@inputs 
@outputs Pos:vector Cam:angle Dead Alive Aim:vector Ang:angle AimEnt:entity Eye:vector Shootpos:vector
@persist Flip
@trigger 
runOnTick(1)
O=owner()
runOnChat(1)
Chat=O:lastSaid()
if(chatClk()){
    if(Chat=="!flip"){Flip=!Flip}
}
Aim=O:aimPos()
AimEnt=O:aimEntity()
Ang=O:angles()
Eye=O:eye()
Shootpos=O:shootPos()
Alive=O:isAlive()
Pos=O:pos()

Rotate=ang(0,Flip*180,0)
Dead=!Alive
Cam=Eye:toAngle()+Rotate
if(first()){}
if(Alive){
    if(holoEntity(1)){
        holoDelete(1)
    }
}else{
    if(holoEntity(1)){
        holoPos(1,O:shootPos()) 
        holoAng(1,O:angles()+Rotate)
    }else{
        holoCreate(1,vec()) 
        holoCustomModel(1,"models/buildables/sentry3_rockets.mdl",1)
    }
}



# Foreach loops have been added! The syntax is:
# foreach(Key,Value:type = Table) { }
#
# Data Signals have been added! These functions allow you to transmit 
# data and execute E2s remotely. Read the wiki for more information.
#
# [wirelink]s can be wired to friends without getting kicked
#
# Expression 2 now uses the friends list of any prop protection
# installed on the server to determine if a player may interact
# with your props and read your code. To allow friends to write to your
# expressions, execute the following: wire_expression2_friendwrite 1
#
# A new operator [A ?: B] has been added, shortcut for [A ? A : B]
# Example: Output = A ?: B, Output = A ?: B ?: C
#
# Documentation and examples are available at:
# http://wiki.garrysmod.com/?title=Wire_Expression2
# The community is available at http://www.wiremod.com
