@name Holo clipping test
@inputs
@outputs
@persist 
@trigger 


if(first()){
    holoCreate(1,entity():pos()+vec(0,0,10))
    holoParent(1,entity())
    holoClipEnabled(1,1)
}
holoClip(1,1,entity():pos()+vec(0,0,10),vec(90,0,0),1)
# use "\n" as a return character

# Foreach loops have been added! The syntax is:
# foreach(Key,Value:type = Table) { }
#
# Data Signals have been added! These functions allow you to transmit 
# data and execute E2s remotely. Read the wiki for more information.
#
# [wirelink]s can be wired to friends without getting kicked
#
# Expression 2 now uses the friends list of any prop protection
# installed on the server to determine if a player may interact
# with your props and read your code. To allow friends to write to your
# expressions, execute the following: wire_expression2_friendwrite 1
#
# A new operator [A ?: B] has been added, shortcut for [A ? A : B]
# Example: Output = A ?: B, Output = A ?: B ?: C
#
# Documentation and examples are available at:
# http://wiki.garrysmod.com/?title=Wire_Expression2
# The community is available at http://www.wiremod.com
