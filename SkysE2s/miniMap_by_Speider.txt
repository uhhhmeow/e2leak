@name miniMap by Speider
@inputs 
@outputs 
@persist Props:array Target:string
@trigger 
@model

runOnTick(1)
runOnChat(1)

P=players()
E=entity()

Last=owner():lastSaid():explode(" ")
if(chatClk(owner())&Last:string(1)=="/t"){
    hideChat(1)
    TempPlayer = findPlayerByName(Last:string(2))
    if (TempPlayer:isPlayer()){
        Target = TempPlayer:name()
    } else {
        Target = "LOL"
    }
}elseif(chatClk(owner())&Last:string(1)=="/c"){
    hideChat(1)
    E:propNotSolid(Last[2,string]:toNumber())
}

for(I=1,P:count()){
    Pos=P[I,entity]:pos()/95
    holoCreate(I)
    holoModel(I,P[I,entity]:model())
    holoScale(I,vec(0.2,0.2,0.2))
    holoPos(I,entity():pos()+Pos+vec(0,0,11))
    if(!Props[I,entity]){
        Props[I,entity]=propSpawn("models/hunter/plates/plate.mdl",entity():pos()+Pos+vec(0,0,35),entity():angles()+ang(0,0,0),255)
        Props[I,entity]:setAlpha(255)
    }
    
    if(P[I,entity]:name()==Target){
        Props[I,entity]:setColor(vec(255,0,0))
    }elseif(P[I,entity]:isAdmin()){
        Props[I,entity]:setColor(vec(0,0,255))
    }elseif(P[I,entity]:isSuperAdmin()){
        Props[I,entity]:setColor(vec(0,255,255))
    }elseif(owner():isSteamFriend(P[I,entity])){
        Props[I,entity]:setColor(vec(100,255,0))
    }else{
        Props[I,entity]:setColor(vec(255,255,255))
    }
    
    Props[I,entity]:propFreeze(1)
    Props[I,entity]:setPos(entity():pos()+vec(0,0,35)+Pos)
    if(owner():aimEntity()==Props[I,entity]&changed(owner():keyUse())&owner():keyUse()) {
        P[I,entity]:plyGod(0)
        P[I,entity]:plySetHealth(-75)
        P[I,entity]:plyExitVehicle()
         if(P[I,entity]:isAlive()){  
            Bomb=propSpawn("models/props_phx/ww2bomb.mdl",(P[I,entity]:pos()+vec(0,0,30)),1)
            Bomb:propBreak()
         }
        printColor("Kill " + P[I,entity]:name())
    }
}

if(first()){
    E:propNotSolid(1)
    holoCreate(199)
    holoModel(199,"models/hunter/plates/plate6x6.mdl")
    holoPos(199,entity():pos()+vec(0,0,10))
    holoColor(199,vec(0,255,0))
    holoParent(199,entity())
}
