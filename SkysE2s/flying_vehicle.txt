@name flying vehicle
@inputs 
@outputs 
@persist A:array  Max Counter Holos:array
@trigger 
runOnTick(1)
runOnChat(1)

E = entity()
Seat=E:isWeldedTo()
Driver = Seat:driver()
Mouse1 = Driver:keyAttack1()
Mouse2 = Driver:keyAttack2()
P=Driver:keyPressed("1")
L=Driver:keyPressed("L")
Shift=Driver:keySprint()

O = owner()

if(Mouse1){
   
    Seat:setPos(Driver:shootPos()+Driver:eye()*80)
}
if(Mouse1&Shift){
   Seat:setPos(Driver:shootPos()+Driver:eye()*120)  
} 

########shield########
if(first()|duped()|dupefinished()){
    Prop=propSpawn("models/props_phx/construct/metal_dome360.mdl",Seat:pos()+vec(1,1,10),0)
    weld(Prop,Seat)
    weld(Seat,Prop)
    Prop:propNotSolid(0)
    Prop:setAng(Driver:eyeAngles())
    noCollideAll(Prop,1)
    
        Prop2=propSpawn("models/props_phx/construct/metal_dome360.mdl",Seat:pos()+vec(1,1,10)+Seat:up()*0,ang(180,180,93),0)
    weld(Seat,Prop2)
    Prop2:propNotSolid(0)
    if(first()){
     Prop2:setAng(ang(0,90,0))   
    Prop:setAlpha(0)
    Prop2:setAlpha(0)
    }
    noCollideAll(Prop2,1)
    

    }
#######shield@##############
if(first()){
Max=Max+150
    
 holoCreate(1)
holoPos(1,Seat:pos()+vec(1,1,10))
holoAng(1,Driver:eyeAngles()) 
holoMaterial(1,"models/vortigaunt/pupil")
if(first()){
holoColor(1,vec(0,255,255))
}
holoModel(1,"hqsphere")
holoScale(1,vec(9,9,9))
holoParent(1,Seat)
holoEntity(1):setTrails(40,0,10,"trails/electric",vec(255,0,0),180)
}
Counter++
Ang = Driver:eyeAngles()
holoCreate(10,Seat:pos()+vec(0,0,10),vec(8,8,8),ang(),vec(150,50,15),"icosphere3")
holoPos(10,Seat:pos()+vec(0,0,10))
if(Driver){
holoAng(10,Ang+ang(0,0,90+Counter))
holoAng(10,Ang+ang(0,0,90+Counter*2.5))
}
holoMaterial(10,"models/vortigaunt/pupil")
holoParent(10,Seat)
if(first()|duped()|dupefinished()){
holoCreate(2)
holoPos(2,(holoEntity(10):pos()+vec(0,-75,0)))
holoModel(2,"models/sprops/misc/tubes/size_3/tube_36x66.mdl")
holoMaterial(2,"models/vortigaunt/pupil")
if(first()){
holoColor(2,vec(0,255,255))
}
holoAng(2,ang(0,90,0))
holoParent(2,10)
holoParent(10,2)

holoCreate(3)
holoPos(3,(holoEntity(10):pos()+vec(0,75,0)))
holoModel(3,"models/sprops/misc/tubes/size_3/tube_36x66.mdl")
holoMaterial(3,"models/vortigaunt/pupil")
if(first()){
holoColor(3,vec(0,255,255))
}
holoAng(3,ang(0,90,0))
holoParent(3,10)
holoParent(10,3)
}
Driver = Seat:driver()

if(changed(Mouse2)&Mouse2){
 Gun = propSpawn("models/props_c17/oildrum001_explosive.mdl",(holoEntity(2):pos() + vec(0,0,0)),Driver:eyeAngles()+(ang(90,0,0)),0)   
A:pushEntity(Gun)   
Gun:propGravity(0)
Gun:setMass(500000000000)
Gun:setMaterial("models/vortigaunt/pupil")
Gun:setColor(vec(0,255,255))
   AG=entity():pos()-entity():pos() + vec(0,0,60) + vec(0,0,60)
        Gun:applyForce(Driver:eye()*999999999999999999+ AG) 
        soundPlay(1,999,"hitmarkers/hit.wav")
        
         Gun2= propSpawn("models/props_c17/oildrum001_explosive.mdl",(holoEntity(3):pos() + vec(0,0,0)),Driver:eyeAngles()+(ang(90,0,0)),0)   
A:pushEntity(Gun2)   
Gun2:propGravity(0)
Gun2:setMass(500000000000)
Gun2:setMaterial("models/vortigaunt/pupil")
Gun2:setColor(vec(0,255,255))
   AG=entity():pos()-entity():pos() + vec(0,0,60) + vec(0,0,60)
        Gun2:applyForce(Driver:eye()*9999999999999999999999+ AG) 
        if(Gun&Gun2){
        soundPlay(1,999,"hitmarkers/hit.wav")
   }
}

    Gun2:setTrails(30,0,20,"trails/laser",vec(255,0,0),255)
    Gun:setTrails(30,0,20,"trails/laser",vec(255,0,0),255)

JJ = lastSaid():explode(" ")
if(chatClk(O)&JJ:string(1)=="/c"){
    Seat:setPos(O:pos()+vec(1,1,80))
    }

if(Driver){
 Seat:setMaterial("effects/ar2_altfire1b")   

 Driver:setMaterial("default")
}
if(changed(L)&L){
 propDeleteAll()
}
if(Driver:name()!=owner():name()) {
    Seat:killPod()
}
holoCreate(5)
holoPos(5,Driver:shootPos()+Driver:eye()*70)
holoModel(5,"hq_torus_thin")
if(first()){
holoColor(5,vec(1,1,1))
}
holoMaterial(5,"models/vortigaunt/pupil")
holoScale(5,vec(1,1,1))
holoAng(5,Driver:eyeAngles()+ang(90,0,0))
holoParent(5,Seat)

holoCreate(6)
holoPos(6,Seat:pos()+vec(0,0,10))
holoParent(6,Seat)
holoModel(6,"hqsphere")
holoMaterial(6,"models/props_combine/tprings_globe")
holoScale(6,vec(6,6,6))
Active = Seat:driver()
if(Active){
    Seat:hintDriver("Type !help for some indructions",5)
     Seat:hintDriver("Made by Sky[ger]",5)
}
AA = Driver:lastSaid():explode(" ")
if(chatClk(Driver)&AA:string(1)=="!help"){
        Seat:hintDriver("Type !col to changed the color of the e2!",5)
}
if(chatClk(Driver)&AA:string(2)=="!col"){
    Seat:hintDriver("Colors",5)
Seat:hintDriver("Red",5)
Seat:hintDriver("Tuerkis",5)
Seat:hintDriver("Green",5)
Seat:hintDriver("Pink",5)
Seat:hintDriver("yellow",5)
Seat:hintDriver("white",5)
Seat:hintDriver("Black",5)
Seat:hintDriver("Say !col <color> to change the color",5)
}
if(chatClk(Driver)&AA:string(3)=="!col black"){
holoColor(5,vec(1,1,1))
holoColor(6,vec(1,1,1))
holoColor(1,vec(1,1,1))
holoColor(10,vec(1,1,1))
holoColor(3,vec(1,1,1))
holoColor(2,vec(1,1,1))
Gun2:setColor(vec(0,1,1))
Gun:setColor(vec(0,1,1))
   Gun2:setTrails(30,0,20,"trails/laser",vec(1,1,1),255)
    Gun:setTrails(30,0,20,"trails/laser",vec(1,1,1),255)
}
if(chatClk(Driver)&AA:string(4)=="!col white"){
holoColor(5,vec(255,255,255))
holoColor(6,vec(255,255,255))
holoColor(1,vec(255,255,255))
holoColor(10,vec(255,255,255))
holoColor(3,vec(255,255,255))
holoColor(2,vec(255,255,255))
Gun2:setColor(vec(255,255,255))
Gun:setColor(vec(255,255,255))
   Gun2:setTrails(30,0,20,"trails/laser",vec(255,255,255),255)
    Gun:setTrails(30,0,20,"trails/laser",vec(255,255,255),255)
}
if(chatClk(Driver)&AA:string(5)=="!col yellow"){
holoColor(5,vec(255,255,0))
holoColor(6,vec(255,255,0))
holoColor(1,vec(255,255,0))
holoColor(10,vec(255,255,0))
holoColor(3,vec(255,255,0))
holoColor(2,vec(255,255,0))
Gun2:setColor(vec(255,255,0))
Gun:setColor(vec(255,255,0))
   Gun2:setTrails(30,0,20,"trails/laser",vec(255,255,0),255)
    Gun:setTrails(30,0,20,"trails/laser",vec(255,255,0),255)
}
if(chatClk(Driver)&AA:string(6)=="!col tuerkis"){
holoColor(5,vec(0,255,255))
holoColor(6,vec(0,255,255))
holoColor(1,vec(0,255,255))
holoColor(10,vec(0,255,255))
holoColor(3,vec(0,255,255))
holoColor(2,vec(0,255,255))
Gun2:setColor(vec(0,255,255))
Gun:setColor(vec(0,255,255))
   Gun2:setTrails(30,0,20,"trails/laser",vec(0,255,255),255)
    Gun:setTrails(30,0,20,"trails/laser",vec(0,255,255),255)
}
if(chatClk(Driver)&AA:string(7)=="!col red"){
holoColor(5,vec(255,0,0))
holoColor(6,vec(255,0,0))
holoColor(1,vec(255,0,0))
holoColor(10,vec(255,0,0))
holoColor(3,vec(255,0,0))
holoColor(2,vec(255,0,0))
Gun2:setColor(vec(255,0,0))
Gun:setColor(vec(255,0,0))
   Gun2:setTrails(30,0,20,"trails/laser",vec(255,0,0),255)
    Gun:setTrails(30,0,20,"trails/laser",vec(255,0,0),255)
}
Seat:propFreeze(1)
Seat:setAng(ang())
