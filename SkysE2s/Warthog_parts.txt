@name Warthog parts
@inputs Next Prev Lights Turn Brake:vector [Control Dash]:entity Active
@outputs CamoNum BodyColour:vector BodyMat:string
@persist [Chip Body Exterior Interior]:entity Spawn IntMat:string Refresh


if(dupefinished()|first()){Body= entity():isWeldedTo(),Spawn = 0,BodyColour = vec(160,230,180),CamoNum=0}
if(Spawn < 34 & !tickClk()){
    interval(100)
    Spawn++

Chip = entity()

White = vec(255,255,255)
Grey = vec(90,90,92)
CamoNum = 1


if(Spawn == 1){
   holoCreate(1, Body:toWorld(vec(98.8,7,26.8)), vec(0.1,0.7,0.3), Body:toWorld(ang(6,0,0)), White)
   holoModel(1, "cube")
   holoMaterial(1, "phoenix_storms/middle")


}elseif(Spawn == 2){
   holoCreate(2, Body:toWorld(vec(98.8,-7,26.8)), vec(0.1,0.7,0.3), Body:toWorld(ang(6,0,0)), White)
   holoModel(2, "cube")
   holoMaterial(2, "phoenix_storms/middle")
    
}elseif(Spawn == 3){###Brake Light
   holoCreate(3, Body:toWorld(vec(-102.2,24.5,33)), vec(0.1,0.96,0.4), Body:toWorld(ang(20,0,0)), Brake)
   holoModel(3, "cube")
   holoMaterial(3, "phoenix_storms/middle")  
   holoDisableShading(3,1)
}elseif(Spawn == 4){
   holoCreate(4, Body:toWorld(vec(-102.2,-24.5,33)), vec(0.1,0.96,0.4), Body:toWorld(ang(20,0,0)), Brake)
   holoModel(4, "cube")
   holoMaterial(4, "phoenix_storms/middle")
   holoDisableShading(4,1)

}elseif(Spawn == 5){#Hood
   holoCreate(5, Body:toWorld(vec(87.4,0,35.32)), vec(2.2,2.36,0.2), Body:toWorld(ang(28,0,0)), BodyColour)
   holoModel(5, "cube")

}elseif(Spawn == 6){#Windscreen
   holoCreate(6, Body:toWorld(vec(22,0,38)), vec(3.4,5.6,9), Body:toWorld(ang(90,0,0)), BodyColour)
   holoModel(6, "dome2")
   holoClipEnabled(6,1)
   holoClip(6,vec(-0.5,0,0),vec(-0.1,0,0),0)

}elseif(Spawn == 7){#Hood Left
   holoCreate(7, Body:toWorld(vec(83,16,37.7)), vec(1.6,0.2,2.6), Body:toWorld(ang(13,-68,65)),  BodyColour)
   holoModel(7, "prism")
   holoClipEnabled(7,1)
   holoClip(7,vec(0,0,8),vec(0,0,-1),0)

}elseif(Spawn == 8){#Hood Right
   holoCreate(8, Body:toWorld(vec(83,-16,37.7)), vec(1.6,0.2,2.6), Body:toWorld(ang(13,68,-65)),  BodyColour)
   holoModel(8, "prism")
   holoClipEnabled(8,1)
   holoClip(8,vec(0,0,8),vec(0,0,-1),0)
 
}elseif(Spawn == 9){#Hood Left 2
   holoCreate(9, Body:toWorld(vec(81,24.5,38.46)), vec(1.2,0.2,2), Body:toWorld(ang(16,-32,74.3)),  BodyColour)
   holoModel(9, "prism")
 
}elseif(Spawn == 10){#Hood Right 2
   holoCreate(10, Body:toWorld(vec(81,-24.5,38.46)), vec(1.2,0.2,2), Body:toWorld(ang(16,32,-74.3)),  BodyColour)
   holoModel(10, "prism")

}elseif(Spawn == 11){#Hood Left Tri
   holoCreate(11, Body:toWorld(vec(42,31,41.3)), vec(1.5,0.2,2.4), Body:toWorld(ang(6,88,-88)),  BodyColour)
   holoModel(11, "prism")

}elseif(Spawn == 12){#Hood Right Tri
   holoCreate(12, Body:toWorld(vec(42,-31,41.3)), vec(1.5,0.2,2.4), Body:toWorld(ang(-6,92,-88)),  BodyColour)
   holoModel(12, "prism")

}elseif(Spawn == 13){#Hood Left 3
   holoCreate(13, Body:toWorld(vec(68.2,30,39.6)), vec(2.04,1.5,0.2), Body:toWorld(ang(6,-2,-6)),  BodyColour)
   holoModel(13, "cube")

}elseif(Spawn == 14){#Hood Right 3
   holoCreate(14, Body:toWorld(vec(68.2,-30,39.6)), vec(2.04,1.5,0.2), Body:toWorld(ang(6,2,6)),  BodyColour)
   holoModel(14, "cube")

}elseif(Spawn == 15){#Hood Left Tri 2
   holoCreate(15, Body:toWorld(vec(68.5,39.2,37.5)), vec(0.7,0.2,2.2), Body:toWorld(ang(60,80,86)),  BodyColour)
   holoModel(15, "prism")

}elseif(Spawn == 16){#Hood Right Tri 2
   holoCreate(16, Body:toWorld(vec(68.5,-39.2,37.5)), vec(0.7,0.2,2.2), Body:toWorld(ang(60,-80,-86)),  BodyColour)
   holoModel(16, "prism")

}elseif(Spawn == 17){#Left Front 
   holoCreate(17, Body:toWorld(vec(42,37.8,37)), vec(2.5,0.2,0.9), Body:toWorld(ang(4,15,35)),  BodyColour)
   holoModel(17, "cube")
   holoClipEnabled(17,1)
   holoClip(17,vec(22,0,0),vec(-1,0,2.5),0)

}elseif(Spawn == 18){#Right Front 
   holoCreate(18, Body:toWorld(vec(42,-37.8,37)), vec(2.5,0.2,0.9), Body:toWorld(ang(4,-15,-35)),  BodyColour)
   holoModel(18, "cube")
   holoClipEnabled(18,1)
   holoClip(18,vec(22,0,0),vec(-1,0,2.5),0)

}elseif(Spawn == 19){#Side Left 
   holoCreate(19, Body:toWorld(vec(-21,38.3,22.5)), vec(2.6,0.98,1.7), Body:toWorld(ang(30,0,0)),  BodyColour)
   holoModel(19, "cube")
   holoClipEnabled(19,1)
   holoClip(19,vec(-10,0,11),vec(1,0,-1),0)

}elseif(Spawn == 20){#Side Right 
   holoCreate(20, Body:toWorld(vec(-21,-38.3,22.5)), vec(2.6,0.98,1.7), Body:toWorld(ang(30,0,0)),  BodyColour)
   holoModel(20, "cube")
   holoClipEnabled(20,1)
   holoClip(20,vec(-10,0,11),vec(1,0,-1),0)

}elseif(Spawn == 22){#Dash
   holoCreate(22, Body:toWorld(vec(70.3,0,41.6)), vec(5,0.2,6.7), Body:toWorld(ang(0,90,90)),  Grey)
   holoModel(22, "prism")
   holoClipEnabled(22,1)
   holoClip(22,vec(0,0,-1),vec(0,0,-1),0)

}elseif(Spawn == 23){#Steer 1
   holoCreate(23, Body:toWorld(vec(15,19,37)), vec(0.1,1,1), Body:toWorld(ang(10,0,180)),  Grey)
   holoModel(23, "dome")

}elseif(Spawn == 24){#Steer 2
   holoCreate(24, Body:toWorld(vec(15,19,37)), vec(1,1,1), Body:toWorld(ang(100,0,0)),  Grey)
   holoModel(24, "hqtorus2")

}elseif(Spawn == 27){#Rear Left Tri
   holoCreate(27, Body:toWorld(vec(-36.8,31.5,40)), vec(3.8,0.3,1.4), Body:toWorld(ang(151,-12,-36)),  BodyColour)
   holoModel(27, "prism")


}elseif(Spawn == 28){#Rear Right Tri
   holoCreate(28, Body:toWorld(vec(-36.8,-31.5,40)), vec(3.8,0.3,1.4), Body:toWorld(ang(151,12,36)),  BodyColour)
   holoModel(28, "prism")
  



}


holoParent(Spawn,Body)
holoParent(24,23)


}else{runOnTick(1)
if(tickClk()){
    
if(changed(Turn)){holoAng(23,Body:toWorld(ang(10,0,180-Turn*6)))}

if(changed(CamoNum)){Refresh = 0}
if(Refresh < 34){Refresh++
    

holoColor(Refresh, BodyColour)
holoColor(3, Brake)
holoColor(4, Brake)
IntColour = vec(135,135,135)
holoColor(22, IntColour)
holoColor(23, IntColour)
holoColor(24, IntColour)
Body:setColor(vec(135,135,135))
Exterior = Chip:isConstrainedTo("nocollide",Refresh)
Interior = Control:isConstrainedTo("nocollide",Refresh)
Exterior:setColor(BodyColour)
Interior:setColor(IntColour)
Dash:setColor(IntColour)
Body:setMaterial("models/weapons/v_stunbaton/w_shaft01a")
holoMaterial(Refresh, BodyMat)
holoMaterial(1, "phoenix_storms/middle")
holoMaterial(2, "phoenix_storms/middle")
holoMaterial(3, "phoenix_storms/middle")
holoMaterial(4, "phoenix_storms/middle")
holoMaterial(6, "phoenix_storms/glass")
holoMaterial(22, IntMat)
holoMaterial(23, IntMat)
holoMaterial(24, IntMat)
holoMaterial(29, "phoenix_storms/glass")
holoMaterial(30, "phoenix_storms/glass")
holoMaterial(31, "phoenix_storms/glass")
holoMaterial(32, "phoenix_storms/glass")

Exterior:setMaterial(BodyMat)
Interior:setMaterial(IntMat)


}

    if(Lights){holoColor(1, vec(230,250,255)),holoColor(2, vec(230,250,255)),holoDisableShading(1,1), holoDisableShading(2,1)}
    else{holoColor(1, vec(220,220,150)),holoColor(2, vec(220,220,150)), holoDisableShading(1,0), holoDisableShading(2,0)}
    if(changed(Brake)){holoColor(3, Brake),holoColor(4, Brake)}
    
            
}else{####end tick


        CamoNum += (Next-Prev)
        CamoNum = CamoNum%5
        CamoNum += (CamoNum<1)*5
        
        if(changed(CamoNum)){
                  
        IntColour = vec(135,135,135)
        IntMat = "Phoenix_storms/FuturisticTrackRamp_1-2"
        Dash:setMaterial("phoenix_storms/gear")

        
        
    if(CamoNum == 1){BodyColour = vec(170,250,190),
        BodyMat ="models/props_canal/canal_bridge_railing_01c"   #####Normal
        
        }
   

    elseif(CamoNum == 2){BodyColour = vec(90,120,60),
        BodyMat ="models/XQM/CellShadedCamo_diffuse"}##Jungle
    elseif(CamoNum == 3){BodyColour = vec(255,230,180),
        BodyMat ="models/XQM/CellShadedCamo_diffuse" }##Desert
    elseif(CamoNum == 4){BodyColour = vec(235,240,245),
        BodyMat ="phoenix_storms/fender_white"}     ##Snow
    elseif(CamoNum == 5){BodyColour = vec(175,175,180),
        BodyMat ="models/XQM/CellShadedCamo_diffuse" }##Urban
        }
         
 


}
}


