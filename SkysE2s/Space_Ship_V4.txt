@name Space Ship V4
@inputs 
@outputs 
@persist Counter Prop:entity B1:entity B2:entity
@trigger 

######################## Holo by Tunoo2 ######### Functions by Lorenz #########################

E=entity()
Seat=E:isWeldedTo()
Driver=Seat:driver()
O=owner()
Seat:propFreeze(1)

Seat:propNotSolid(0)
Seat:setAlpha(0)
Active = Seat:driver()
runOnTick(1)

Counter++
Speed=15




Driver =entity():isWeldedTo():driver()
Ang=(Driver:eyeAngles())
W=Driver:keyForward()
S=Driver:keyBack()
D=Driver:keyRight()
A=Driver:keyLeft()
Space=Driver:keyJump()
Shift=Driver:keySprint()
Mouse1=Driver:keyAttack1()
Mouse2=Driver:keyAttack2()
Alt=Driver:keyPressed("F")  
B=Driver:keyPressed("B")  
R=Driver:keyReload()
L1=Driver:keyPressed("1")
L2=Driver:keyPressed("2")
L3=Driver:keyPressed("3")
L4=Driver:keyPressed("4")
L5=Driver:keyPressed("5")
L6=Driver:keyPressed("6")
L7=Driver:keyPressed("7")
L8=Driver:keyPressed("8")
L9=Driver:keyPressed("9")
L0=O:keyPressed("0")
N=Driver:keyPressed("N")


Prop:setAng(Ang)

HASHAHA=holoEntity(200)
holoAng(200,Driver:eyeAngles())
if(Shift) {Add=3}else{Add=1}
if(W){Seat:setPos(Seat:pos()+HASHAHA:forward()*(Speed*Add))}
if(S){Seat:setPos(Seat:pos()+HASHAHA:forward()*-(Speed*Add))}
if(A){Seat:setPos(Seat:pos()+HASHAHA:right()*-(Speed*Add))}
if(D){Seat:setPos(Seat:pos()+HASHAHA:right()*(Speed*Add))}
if(Space){Seat:setPos(Seat:pos()+vec(0,0,Speed*Add))}
if(Alt){Seat:setPos(Seat:pos()+vec(0,0,-Speed*Add))}

if(Active){
    Driver:setMaterial("models/effects/comball_glow2")
}
else{
    owner():setMaterial("  ")
}
if(Driver:name()!=owner():name()) {
Seat:killPod()
}

##########Holo by Tunoo2#######################################################################################

if(first()){
#############Haupt Body######################
holoCreate(1)
holoModel(1,"models/xqm/jetbody1big.mdl")
holoScale(1,vec(1,1,1.1))
holoPos(1,Seat:pos()+vec(0,0,0))
holoAng(1,ang(0,0,0))
#holoColor(1,vec(0,0,0))
holoMaterial(1,"sprops/textures/sprops_rubber2")
###########Neben Bodys Verbindung###################
holoCreate(2)
holoModel(2,"cube")
holoScale(2,vec(1,7,3))
holoPos(2,Seat:pos()+vec(35,60,-5))
holoAng(2,ang(90,0,0))
#holoColor(2,vec(0,0,0))
holoMaterial(2,"sprops/textures/sprops_rubber2")

holoCreate(3)
holoModel(3,"cube")
holoScale(3,vec(1,7,3))
holoPos(3,Seat:pos()+vec(35,-60,-5))
holoAng(3,ang(90,0,0))
#holoColor(3,vec(0,0,0))
holoMaterial(3,"sprops/textures/sprops_rubber2")
##############Neben Bodys#################
###Rechts
holoCreate(4)
holoModel(4,"models/xqm/jetbody1big.mdl")
holoScale(4,vec(0.6,0.6,0.6))
holoPos(4,Seat:pos()+vec(5,110,0))
holoAng(4,ang(0,0,0))
#holoColor(4,vec(0,0,0))
holoMaterial(4,"sprops/textures/sprops_rubber2")
###Links
holoCreate(5)
holoModel(5,"models/xqm/jetbody1big.mdl")
holoScale(5,vec(0.6,0.6,0.6))
holoPos(5,Seat:pos()+vec(-5,-110,0))
holoAng(5,ang(0,0,0))
#holoColor(5,vec(0,0,0))
holoMaterial(5,"sprops/textures/sprops_rubber2")

###########Wings Verbindung##########
###Rechts
holoCreate(6)
holoModel(6,"cube")
holoScale(6,vec(0.5,0.5,5.5))
holoPos(6,Seat:pos()+vec(-110,110,30))
holoAng(6,ang(-45,0,0))
#holoColor(6,vec(0,0,0))
holoMaterial(6,"sprops/textures/sprops_rubber2")
##Links
holoCreate(7)
holoModel(7,"cube")
holoScale(7,vec(0.5,0.5,5.5))
holoPos(7,Seat:pos()+vec(-110,-110,30))
holoAng(7,ang(-45,0,0))
#holoColor(7,vec(0,0,0))
holoMaterial(7,"sprops/textures/sprops_rubber2")
##########Wing Of 2Body#######################
holoCreate(8)
holoModel(8,"cube")
holoScale(8,vec(3,20,0.4))
holoPos(8,Seat:pos()+vec(-137,0,53))
holoAng(8,ang(0,0,0))
#holoColor(8,vec(0,0,0))
holoMaterial(8,"sprops/textures/sprops_rubber2")

################################Tonou Gun######################################
###Rechts
################haupt rohr#############

holoCreate(9)
holoModel(9,"hq_cylinder")
holoScale(9,vec(0.7,0.7,3))
holoPos(9,Seat:pos()+vec(47,60,-30))
holoAng(9,ang(90,0,0))
#holoColor(9,vec(255,240,0))
holoMaterial(9,"sprops/textures/sprops_rubber2")

#########Combine ball############################

#holoCreate(10)
holoModel(10,"models/items/combine_rifle_ammo01.mdl")
holoScale(10,vec(2.3,2.3,0.6))
holoPos(10,Seat:pos()+vec(43.3,60,-30))
holoAng(10,ang(90,0,0))
#holoColor(10,vec(255,240,0))
holoMaterial(10,"")

#######haupt ring##############################

holoCreate(11)
holoModel(11,"hq_tube_thin")
holoScale(11,vec(2.3,2.3,0.6))
holoPos(11,Seat:pos()+vec(42.5,60,-30))
holoAng(11,ang(90,0,0))
#holoColor(11,vec(255,240,0))
holoMaterial(11,"sprops/textures/sprops_rubber2")

#######haup ring speichen#################################
#vertikal
holoCreate(12)
holoModel(12,"cube")
holoScale(12,vec(0.1,2.3,0.1))
holoPos(12,Seat:pos()+vec(42.5,60,-30))
holoAng(12,ang(90,0,0))
#holoColor(12,vec(255,240,0))
holoMaterial(12,"sprops/textures/sprops_rubber2")
#senkrecht
holoCreate(13)
holoModel(13,"cube")
holoScale(13,vec(0.1,0.1,2.3))
holoPos(13,Seat:pos()+vec(42.5,60,-30))
holoAng(13,ang(0,0,0))
#holoColor(13,vec(255,240,0))
holoMaterial(13,"sprops/textures/sprops_rubber2")

####################Gun Pipes########################################
#oben
holoCreate(14)
holoModel(14,"hq_tube_thin")
holoScale(14,vec(0.2,0.2,2))
holoPos(14,Seat:pos()+vec(77,60,-32.5))
holoAng(14,ang(90,0,0))
#holoColor(14,vec(255,240,0))
holoMaterial(14,"sprops/textures/sprops_rubber2")
#unten
holoCreate(15)
holoModel(15,"hq_tube_thin")
holoScale(15,vec(0.2,0.2,2))
holoPos(15,Seat:pos()+vec(77,60,-27.5))
holoAng(15,ang(90,0,0))
#holoColor(15,vec(255,240,0))
holoMaterial(15,"sprops/textures/sprops_rubber2")
#rechts
holoCreate(16)
holoModel(16,"hq_tube_thin")
holoScale(16,vec(0.2,0.2,2))
holoPos(16,Seat:pos()+vec(77,62.5,-30))
holoAng(16,ang(90,0,0))
#holoColor(16,vec(255,240,0))
holoMaterial(16,"sprops/textures/sprops_rubber2")
#links
holoCreate(17)
holoModel(17,"hq_tube_thin")
holoScale(17,vec(0.2,0.2,2))
holoPos(17,Seat:pos()+vec(77,57.5,-30))
holoAng(17,ang(90,0,0))
#holoColor(17,vec(255,240,0))
holoMaterial(17,"sprops/textures/sprops_rubber2")

####################Gun Pipes Ring########
#Hinten
holoCreate(18)
holoModel(18,"hq_cylinder")
holoScale(18,vec(0.7,0.7,0.1))
holoPos(18,Seat:pos()+vec(73,60,-30))
holoAng(18,ang(90,0,0))
#holoColor(18,vec(255,240,0))
holoMaterial(18,"sprops/textures/sprops_rubber2")
#Vorne
holoCreate(19)
holoModel(19,"hq_cylinder")
holoScale(19,vec(0.7,0.7,0.1))
holoPos(19,Seat:pos()+vec(82,60,-30))
holoAng(19,ang(90,0,0))
#holoColor(19,vec(255,240,0))
holoMaterial(19,"sprops/textures/sprops_rubber2")


#########################Tonou Gun##################################
###Links
################haupt rohr#############

holoCreate(20)
holoModel(20,"hq_cylinder")
holoScale(20,vec(0.7,0.7,3))
holoPos(20,Seat:pos()+vec(47,-60,-30))
holoAng(20,ang(90,0,0))
#holoColor(20,vec(255,240,0))
holoMaterial(20,"sprops/textures/sprops_rubber2")

#########Combine ball############################

#holoCreate(21)
holoModel(21,"models/items/combine_rifle_ammo01.mdl")
holoScale(21,vec(2.3,2.3,0.6))
holoPos(21,Seat:pos()+vec(43.3,-60,-30))
holoAng(21,ang(90,0,0))
#holoColor(21,vec(255,240,0))
holoMaterial(21,"")

#######haupt ring##############################

holoCreate(22)
holoModel(22,"hq_tube_thin")
holoScale(22,vec(2.3,2.3,0.6))
holoPos(22,Seat:pos()+vec(42.5,-60,-30))
holoAng(22,ang(90,0,0))
#holoColor(22,vec(255,240,0))
holoMaterial(22,"sprops/textures/sprops_rubber2")

#######haup ring speichen#################################
#vertikal
holoCreate(23)
holoModel(23,"cube")
holoScale(23,vec(0.1,2.3,0.1))
holoPos(23,Seat:pos()+vec(42.5,-60,-30))
holoAng(23,ang(90,0,0))
#holoColor(23,vec(255,240,0))
holoMaterial(23,"sprops/textures/sprops_rubber2")
#senkrecht
holoCreate(24)
holoModel(24,"cube")
holoScale(24,vec(0.1,0.1,2.3))
holoPos(24,Seat:pos()+vec(42.5,-60,-30))
holoAng(24,ang(0,0,0))
#holoColor(24,vec(255,240,0))
holoMaterial(24,"sprops/textures/sprops_rubber2")

####################Gun Pipes########################################
#oben
holoCreate(25)
holoModel(25,"hq_tube_thin")
holoScale(25,vec(0.2,0.2,2))
holoPos(25,Seat:pos()+vec(77,-60,-32.5))
holoAng(25,ang(90,0,0))
#holoColor(25,vec(255,240,0))
holoMaterial(25,"sprops/textures/sprops_rubber2")
#unten
holoCreate(26)
holoModel(26,"hq_tube_thin")
holoScale(26,vec(0.2,0.2,2))
holoPos(26,Seat:pos()+vec(77,-60,-27.5))
holoAng(26,ang(90,0,0))
#holoColor(26,vec(255,240,0))
holoMaterial(26,"sprops/textures/sprops_rubber2")
#rechts
holoCreate(27)
holoModel(27,"hq_tube_thin")
holoScale(27,vec(0.2,0.2,2))
holoPos(27,Seat:pos()+vec(77,-62.5,-30))
holoAng(27,ang(90,0,0))
#holoColor(27,vec(255,240,0))
holoMaterial(27,"sprops/textures/sprops_rubber2")
#links
holoCreate(28)
holoModel(28,"hq_tube_thin")
holoScale(28,vec(0.2,0.2,2))
holoPos(28,Seat:pos()+vec(77,-57.5,-30))
holoAng(28,ang(90,0,0))
#holoColor(28,vec(255,240,0))
holoMaterial(28,"sprops/textures/sprops_rubber2")

####################Gun Pipes Ring########
#Hinten
holoCreate(29)
holoModel(29,"hq_cylinder")
holoScale(29,vec(0.7,0.7,0.1))
holoPos(29,Seat:pos()+vec(73,-60,-30))
holoAng(29,ang(90,0,0))
#holoColor(29,vec(255,240,0))
holoMaterial(29,"sprops/textures/sprops_rubber2")
#Vorne
holoCreate(30)
holoModel(30,"hq_cylinder")
holoScale(30,vec(0.7,0.7,0.1))
holoPos(30,Seat:pos()+vec(82,-60,-30))
holoAng(30,ang(90,0,0))
#holoColor(30,vec(255,240,0))
holoMaterial(30,"sprops/textures/sprops_rubber2")
########################################################

#############Tunou Gun Verbingung#######################
###Rechts Zylinder
holoCreate(31)
holoModel(31,"hq_cylinder")
holoScale(31,vec(1,1,1))
holoPos(31,Seat:pos()+vec(31,60,-30))
holoAng(31,ang(90,0,0))
#holoColor(31,vec(255,240,0))
holoMaterial(31,"sprops/textures/sprops_rubber2")
###Links Zylinder
holoCreate(32)
holoModel(32,"hq_cylinder")
holoScale(32,vec(1,1,1))
holoPos(32,Seat:pos()+vec(31,-60,-30))
holoAng(32,ang(90,0,0))
#holoColor(32,vec(255,240,0))
holoMaterial(32,"sprops/textures/sprops_rubber2")
###########Tunoeu Gun Zylinder Verbindung ####
###Rechts Zylinder
holoCreate(31)
holoModel(31,"cube")
holoScale(31,vec(0.4,0.6,2))
holoPos(31,Seat:pos()+vec(32,60,-20))
holoAng(31,ang(0,0,0))
#holoColor(31,vec(255,240,0))
holoMaterial(31,"sprops/textures/sprops_rubber2")
###Links Zylinder
holoCreate(32)
holoModel(32,"cube")
holoScale(32,vec(0.4,0.6,2))
holoPos(32,Seat:pos()+vec(32,-60,-20))
holoAng(32,ang(0,0,0))
#holoColor(32,vec(255,240,0))
holoMaterial(32,"sprops/textures/sprops_rubber2")

holoCreate(33)
holoModel(33,"models/props_phx/mk-82.mdl")
holoScale(33,vec(0.5))
holoPos(33,Seat:pos()+vec(60,-80,-20))
holoAng(33,ang(0,0,0))
#holoColor(33,vec(255,240,0))
holoMaterial(33,"")

holoCreate(34)
holoModel(34,"models/props_phx/mk-82.mdl")
holoScale(34,vec(0.5))
holoPos(34,Seat:pos()+vec(60,80,-20))
holoAng(34,ang(0,0,0))
#holoColor(34,vec(255,240,0))
holoMaterial(34,"")

holoCreate(35)
holoModel(35,"models/props_phx/mk-82.mdl")
holoScale(35,vec(0.5))
holoPos(35,Seat:pos()+vec(60,40,-20))
holoAng(35,ang(0,0,0))
#holoColor(35,vec(255,240,0))
holoMaterial(35,"")

holoCreate(36)
holoModel(36,"models/props_phx/mk-82.mdl")
holoScale(36,vec(0.5))
holoPos(36,Seat:pos()+vec(60,-40,-20))
holoAng(36,ang(0,0,0))
#holoColor(36,vec(255,240,0))
holoMaterial(36,"")

holoCreate(37)
holoModel(37,"models/props_phx/torpedo.mdl")
holoScale(37,vec(1))
holoPos(37,Seat:pos()+vec(30,0,-60))
holoAng(37,ang(0,0,0))
#holoColor(37,vec(255,240,0))
holoMaterial(37,"")

 holoCreate(200,entity():pos(),vec(5))
 holoAlpha(200,0)
}
holoParent(2,1)
holoParent(3,1)
holoParent(4,1)
holoParent(5,1)
holoParent(6,1)
holoParent(7,1)
holoParent(8,1)
holoParent(9,1)
holoParent(10,1)
holoParent(11,1)
holoParent(12,1)
holoParent(13,1)
holoParent(14,1)
holoParent(15,1)
holoParent(16,1)
holoParent(17,1)
holoParent(18,1)
holoParent(19,1)
holoParent(20,1)
holoParent(21,1)
holoParent(22,1)
holoParent(23,1)
holoParent(24,1)
holoParent(25,1)
holoParent(26,1)
holoParent(27,1)
holoParent(28,1)
holoParent(29,1)
holoParent(30,1)
holoParent(31,1)
holoParent(32,1)
holoParent(33,1)
holoParent(34,1)
holoParent(35,1)
holoParent(36,1)
holoParent(37,1)
holoParent(1,Seat)
entity():parentTo(Seat)

holoAng(1,ang(0,0,0+Counter*0)+Driver:eyeAngles())

if(changed(L2)&L2){
Missle = propSpawn("models/props_phx/torpedo.mdl",(Driver:pos() + vec(30,0,-60)) + Driver:eye() * 25,Driver:eyeAngles(),0) 
}


if(changed(L1)&L1){
 L2Qe = propSpawn("models/props_phx/mk-82.mdl",(Driver:toWorld(vec(60,80,-20))) + Driver:eye() * 25,Driver:eyeAngles(),0)   
}
L2Qe:setMass(1000)
L2Qe:applyForce(Driver:eye()*100000000)
L2Qe:setTrails(50,0,1, "trails/laser", vec(random(255),random(255),random(255)),255)
L2Qe:propGravity(0)
L2Qe:setMaterial("")
L2Qe:setColor(vec(1,1,1))

if(changed(L1)&L1){
 L22b = propSpawn("models/props_phx/mk-82.mdl",(Driver:pos() + vec(60,-80,-20)) + Driver:eye() * 25,Driver:eyeAngles(),0)   
}
L22b:setMass(1000)
L22b:applyForce(Driver:eye()*100000000)
L22b:setTrails(50,0,1, "trails/laser", vec(random(255),random(255),random(255)),255)
L22b:propGravity(0)
L22b:setMaterial("")
L22b:setColor(vec(1,1,1))

if(changed(L1)&L1){
 L2Q1 = propSpawn("models/props_phx/mk-82.mdl",(Driver:pos() + vec(60,40,-20)) + Driver:eye() * 25,Driver:eyeAngles(),0)   
}
L2Q1:setMaterial("")
L2Q1:setColor(vec(1,1,1))
L2Q1:setMass(1000)
L2Q1:applyForce(Driver:eye()*100000000)
L2Q1:setTrails(50,0,1, "trails/laser", vec(random(255),random(255),random(255)),255)
L2Q1:propGravity(0)


if(changed(L1)&L1){
 L24 = propSpawn("models/props_phx/mk-82.mdl",(Driver:pos() + vec(60,-40,-20)) + Driver:eye() * 25,Driver:eyeAngles(),0)   
}
L24:setMaterial("")
L24:setColor(vec(1,1,1))
L24:setMass(1000)
L24:applyForce(Driver:eye()*100000000)
L24:setTrails(50,0,1, "trails/laser", vec(random(255),random(255),random(255)),255)
L24:propGravity(0)



timer("solide",150)
timer("reload",250)
if(Mouse2&clk("reload")){
 B1=propSpawn("models/props_phx/ww2bomb.mdl",(holoEntity(1):toWorld(vec(31,-60,-30))),0)
   B1:propGravity(0)
B1:setTrails(10,0,10,"trails/smoke",vec(50,50,50),255)
B1:applyForce(holoEntity(1):forward()*B1:mass()*5000)
B1:setAng(Driver:eyeAngles())
}
if(Mouse1&clk("reload")){
 B2=propSpawn("models/props_phx/ww2bomb.mdl",(holoEntity(1):toWorld(vec(31,60,-30))),0)
   B2:propGravity(0)
B2:setTrails(10,0,10,"trails/smoke",vec(50,50,50),255)
B2:applyForce(holoEntity(1):forward()*B2:mass()*5000)
B2:setAng(Driver:eyeAngles())



}

if(R){
holoColor(0, vec(0))
holoAlpha(0,0)
holoMaterial(1,"models/shadertest/predator")
holoMaterial(2,"models/shadertest/predator")
holoMaterial(3,"models/shadertest/predator")   
holoMaterial(4,"models/shadertest/predator")  
holoMaterial(5,"models/shadertest/predator")   
holoMaterial(6,"models/shadertest/predator")   
holoMaterial(7,"models/shadertest/predator")   
holoMaterial(8,"models/shadertest/predator")   
holoMaterial(9,"models/shadertest/predator")   
holoMaterial(10,"models/shadertest/predator")    
holoMaterial(11,"models/shadertest/predator")   
holoMaterial(12,"models/shadertest/predator")   
holoMaterial(13,"models/shadertest/predator")   
holoMaterial(14,"models/shadertest/predator")   
holoMaterial(15,"models/shadertest/predator")
holoMaterial(16,"models/shadertest/predator")  
holoMaterial(17,"models/shadertest/predator")  
holoMaterial(18,"models/shadertest/predator")  
holoMaterial(19,"models/shadertest/predator")  
holoMaterial(20,"models/shadertest/predator")  
holoMaterial(21,"models/shadertest/predator")  
holoMaterial(22,"models/shadertest/predator")  
holoMaterial(23,"models/shadertest/predator")  
holoMaterial(24,"models/shadertest/predator")  
holoMaterial(25,"models/shadertest/predator")  
holoMaterial(26,"models/shadertest/predator")  
holoMaterial(27,"models/shadertest/predator")  
holoMaterial(28,"models/shadertest/predator")  
holoMaterial(29,"models/shadertest/predator")  
holoMaterial(30,"models/shadertest/predator")  
holoMaterial(31,"models/shadertest/predator")  
holoMaterial(32,"models/shadertest/predator")  
holoMaterial(33,"models/shadertest/predator")  
holoMaterial(34,"models/shadertest/predator")  
holoMaterial(35,"models/shadertest/predator")  
holoMaterial(36,"models/shadertest/predator")  
holoMaterial(37,"models/shadertest/predator")  




holoEntity(10):setTrails(0,0,0,"trails/smoke",vec(0,255,255),255)
E:setMaterial("models/shadertest/predator")

Driver:setMaterial("effects/ar2_altfire1b")
}
else{
holoColor(0, vec(255,0,255))
holoAlpha(0,255)
holoMaterial(0,"sprops/textures/sprops_rubber2")
holoMaterial(1,"sprops/textures/sprops_rubber2")
holoMaterial(2,"sprops/textures/sprops_rubber2")
holoMaterial(3,"sprops/textures/sprops_rubber2")
holoMaterial(4,"sprops/textures/sprops_rubber2")
holoMaterial(5,"sprops/textures/sprops_rubber2")
holoMaterial(6,"sprops/textures/sprops_rubber2")
holoMaterial(7,"sprops/textures/sprops_rubber2")
holoMaterial(8,"sprops/textures/sprops_rubber2")
holoMaterial(9,"sprops/textures/sprops_rubber2")
holoMaterial(10,"sprops/textures/sprops_rubber2")
holoMaterial(11,"sprops/textures/sprops_rubber2")
holoMaterial(12,"sprops/textures/sprops_rubber2")
holoMaterial(13,"sprops/textures/sprops_rubber2")
holoMaterial(14,"sprops/textures/sprops_rubber2")
holoMaterial(15,"sprops/textures/sprops_rubber2")
holoMaterial(16,"sprops/textures/sprops_rubber2")
holoMaterial(17,"sprops/textures/sprops_rubber2")
holoMaterial(18,"sprops/textures/sprops_rubber2")
holoMaterial(19,"sprops/textures/sprops_rubber2")
holoMaterial(20,"sprops/textures/sprops_rubber2")
holoMaterial(21,"sprops/textures/sprops_rubber2")
holoMaterial(22,"sprops/textures/sprops_rubber2")
holoMaterial(23,"sprops/textures/sprops_rubber2")
holoMaterial(24,"sprops/textures/sprops_rubber2")
holoMaterial(25,"sprops/textures/sprops_rubber2")
holoMaterial(26,"sprops/textures/sprops_rubber2")
holoMaterial(27,"sprops/textures/sprops_rubber2")
holoMaterial(28,"sprops/textures/sprops_rubber2")
holoMaterial(29,"sprops/textures/sprops_rubber2")
holoMaterial(30,"sprops/textures/sprops_rubber2")
holoMaterial(31,"sprops/textures/sprops_rubber2")
holoMaterial(32,"sprops/textures/sprops_rubber2")
holoMaterial(33,"")
holoMaterial(34,"")
holoMaterial(35,"")
holoMaterial(36,"")
holoMaterial(37,"")
}




