@name Turret by E2Caster
@inputs 
@outputs 
@persist Counter A:array 
@trigger 

runOnTick(1)
if(first()){
 holoCreate(20)
holoPos(20,entity():pos()+vec(1,1,-10))
   holoModel(20,"hq_torus_thin")
holoColor(20,vec(255,0,0))
holoMaterial(20,"models/vortigaunt/pupil")
holoScale(20,vec(0.05,0.05,0.05))
}

Counter++
E=entity()
findIncludeClass("player")
AA=findInSphere(E:pos(),50)
O=findClosest(E:pos())
if(AA>0&O:weapon():type()=="weapon_crowbar"){
 holoAng(2,O:eyeAngles()+ang(90,0,0))
 holoAng(1,ang(90,0,0)+O:eyeAngles())
  holoAng(2,ang(0,0,0+Counter*8.5)+O:eyeAngles())
holoAng(15,O:eyeAngles()+ang(90,0,0))
holoAng(20,O:eyeAngles()+ang(90,0,0))
holoPos(20,O:shootPos()+O:eye()*20)
}

if(changed(O&O:weapon():type()=="weapon_crowbar")&O&O:weapon():type()=="weapon_crowbar"){
 soundPlay(1,999,"radio/letsgo.wav")   
}
if(changed(O&O:weapon():type()=="weapon_crowbar")&!O){
    soundPlay(1,999,"radio/roger.wav")
}
if(!O){
     holoAng(1,ang(0,0,90))
   propDeleteAll()
holoPos(20,entity():pos()+vec(1,1,-10))
}
if(!O&O:weapon():type()=="weapon_crowbar"){
 holoPos(20,entity():pos()+vec(1,1,-10))   
}

timer("shot",250)
if(O:keyAttack2()&O:weapon():type()=="weapon_crowbar"&clk("shot")){
   
 P=propSpawn("models/props_phx/ww2bomb.mdl",holoEntity(6):pos()+vec(1,1,1),0)  
P:setAlpha(0)
P:setMass(200)
P:propGravity(0) 
P:setAng(O:eyeAngles())
P:applyForce(holoEntity(1):up()*999999999999999999999)
P:setTrails(10,0,2,"trails/smoke",vec(25,25,25),255)
}
if(P){
 soundPlay(1,999,"weapons/deagle/deagle-1.wav")   
}



E:setAng(ang())
if(first()){

    holoCreate(1)
    holoPos(1,E:pos()+vec(1,1,50))
    holoModel(1,"hqcylinder")
    holoColor(1,vec(100,100,100))
    holoAng(1,ang(0,0,0))
  
holoCreate(2)
holoPos(2,E:pos()+vec(1,1,56.1))
holoModel(2,"models/XQM/panel360.mdl")
holoScale(2,vec(0.2,0.2,0.2))


###################3
holoCreate(3)
holoPos(3,E:toWorld(vec(5,4.5,56)))
holoModel(3,"hqcylinder")
holoScale(3,vec(0.2,0.2,2.5))
holoAng(3,ang(90,0,0))
holoColor(3,vec(255,0,0))

holoCreate(4)
holoPos(4,E:toWorld(vec(5,-2.5,56)))
holoModel(4,"hqcylinder")
holoScale(4,vec(0.2,0.2,2.5))
holoAng(4,ang(90,0,0))
holoColor(4,vec(255,0,0))

holoCreate(5)
holoPos(5,E:toWorld(vec(5,1,53)))
holoModel(5,"hqcylinder")
holoScale(5,vec(0.2,0.2,2.5))
holoAng(5,ang(90,0,0))
holoColor(5,vec(255,0,0))

holoCreate(6)
holoPos(6,E:toWorld(vec(5,1,59.5)))
holoModel(6,"hqcylinder")
holoScale(6,vec(0.2,0.2,2.5))
holoAng(6,ang(90,0,0))
holoColor(6,vec(255,0,0))
##################




holoCreate(7)
holoPos(7,E:toWorld(vec(9,1,56.1)))
holoModel(7,"models/XQM/panel360.mdl")
holoScale(7,vec(0.2,0.2,0.2))


holoCreate(8)
holoPos(8,E:toWorld(vec(17,1,56.1)))
holoModel(8,"models/XQM/panel360.mdl")
holoScale(8,vec(0.2,0.2,0.2))


###################

holoCreate(9)
holoPos(9,E:pos()+vec(-8,-2,19))
holoModel(9,"hqcylinder")
holoColor(9,vec(75,75,75))
holoAng(9,ang(15,0,0))
holoScale(9,vec(0.2,0.2,5.8))

holoCreate(10)
holoPos(10,E:pos()+vec(8,0,19))
holoModel(10,"hqcylinder")
holoColor(10,vec(75,75,75))
holoAng(10,ang(-15,0,0))
holoScale(10,vec(0.2,0.2,5.8))

holoCreate(11)
holoPos(11,E:pos()+vec(0,-1,19))
holoModel(11,"hqcylinder")
holoColor(11,vec(75,75,75))
holoAng(11,ang(-90,7.5,0))
holoScale(11,vec(0.2,0.2,1.3))

holoCreate(12)
holoPos(12,E:pos()+vec(0,-1,19))
holoModel(12,"hqcylinder")
holoColor(12,vec(75,75,75))
holoAng(12,ang(-90,7.5,0))
holoScale(12,vec(0.2,0.2,1.3))


holoCreate(13)
holoPos(13,E:pos()+vec(0,10,15))
holoModel(13,"hqcylinder")
holoColor(13,vec(75,75,75))
holoAng(13,ang(0,0,15))
holoScale(13,vec(0.2,0.2,5.8))

holoCreate(14)
holoPos(14,E:pos()+vec(-5,4,15))
holoModel(14,"hqcylinder")
holoColor(14,vec(1,1,1))
holoAng(14,ang(-94,54,0))
holoScale(14,vec(0.1,0.1,1.3))

holoCreate(15)
holoPos(15,E:toWorld(vec(1,1,57)))
holoModel(15,"hq_torus_thin")
holoColor(15,vec(50,0,255))
holoMaterial(15,"models/vortigaunt/pupil")

holoScale(15,vec(0.2,0.2,0.2))

}
holoParent(15,1)
holoParent(9,E)
holoParent(10,E)
holoParent(1,E)
holoParent(8,2)
holoParent(7,2)
holoParent(6,2)
holoParent(5,2)
holoParent(4,2)
holoParent(3,2)
holoParent(11,E)
holoParent(12,E)
holoParent(13,E)
holoParent(14,E)
holoParent(15,E)
holoParent(16,E)
holoParent(17,E)
holoParent(2,1) 
  

E:setAlpha(0)
