@name HoverAim
@inputs 
@outputs 
@persist Ent:entity Vec:vector Ang:angle
@trigger 

findByModel("models/hunter/blocks/cube05x05x05.mdl")
Ent=findResult(1)

runOnTick(1)

Vec = (entity():pos()-Ent:pos()+vec(0,0,50))*80

Ent:applyForce(Vec+$Vec*5)


######################################


Pitch = Ent:elevation(owner():aimPos())
Yaw = Ent:bearing(owner():aimPos())
Roll = Ent:angles():roll()

Ang = (-ang(Pitch,Yaw,Roll))*80

Ent:applyAngForce(Ang+$Ang*5)
