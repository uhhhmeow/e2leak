@name Kampf-Roboter
@inputs Egp:wirelink
@persist [E Seat P1P2 Sh Mi M2 MX M1 MM]:entity
@persist Acc WVel Counter AEE [Ang Posi M Props]:array 
@model models/hunter/blocks/cube1x1x1.mdl
Counter++

if(first()){
runOnTick(1) runOnChat(1) runOnLast(1)
E=entity()
E:setAng(ang())   

}
if(!Seat) {
findByClass("prop_vehicle_*")
findSortByDistance(E:pos())
if(find():owner() == owner()&!Seat)
{
Seat=find()
Seat:setPos(E:toWorld(vec(0,0,10)))
Seat:setAng(E:toWorld(ang(0,-90,0)))
Seat:propFreeze(1)
timer("P",1000)
}
}

if(clk("P")) {
Seat:parentTo(E)
printColor(vec(255,0,0),"PARENTED")
}
########seat###########
Driver=Seat:driver()
########
W=Driver:keyForward()
S=Driver:keyBack()
Space=Driver:keyJump()
Shift=Driver:keySprint()
A=Driver:keyLeft()
D=Driver:keyRight()
An=Driver:eyeAngles()

E:applyForce(-E:vel()*E:mass()*0.3)
E:applyAngForce(-E:angVel()*E:mass()*1)
E:applyAngForce((ang(0,E:angles():yaw(),0)-E:angles())*E:mass()*5)
E:applyForce(E:forward()*E:mass()*(W-S)*405)
E:applyAngForce(ang(0,1,0)*E:mass()*(A-D)*105)
E:applyForce(E:up()*E:mass()*150*(Space))
E:applyForce(E:up()*E:mass()*-150*(Shift))

#########
if(first()|duped()|dupefinished()){
    M=array(
    "models/hunter/tubes/tube2x2x025.mdl","models/hunter/tubes/circle2x2.mdl","models/hunter/tubes/tube2x2x025.mdl"
    ,"models/hunter/tubes/tube2x2x025.mdl","models/hunter/tubes/tube1x1x1c.mdl","models/hunter/tubes/tube1x1x1c.mdl"
     ,"models/hunter/misc/shell2x2c.mdl","models/Mechanics/gears2/gear_48t1.mdl","models/hunter/tubes/tube1x1x1.mdl"
     ,"models/hunter/tubes/tube1x1x1.mdl","models/props_phx/construct/metal_angle180.mdl","models/mechanics/wheels/wheel_smooth2.mdl"
     ,"models/mechanics/wheels/wheel_smooth2.mdl","models/mechanics/wheels/wheel_smooth2.mdl","models/mechanics/wheels/wheel_smooth2.mdl"
     ,"","",""
     ,"","",""
    )
    Posi=array(
    vec(0,0,-20),vec(0,0,-25),vec(0,0,-5)
    ,vec(0,0,10),vec(-20,50,10),vec(-20,-50,10)
    ,vec(-10,0,10),vec(0,0,-20),vec(-50,-30,0)
      ,vec(-50,30,0),vec(0,0,15),vec(-50,-30,50)
      ,vec(-50,30,50),vec(-50,30,5),vec(-50,-30,5)
      ,vec(0,0,0),vec(0,0,0),vec(0,0,0)
      ,vec(0,0,0),vec(0,0,0),vec(0,0,0)
      ,vec(0,0,0),vec(0,0,0),vec(0,0,0)
    )
    12
    13
    Ang=array(
    ang(0,0,0),ang(0,0,0),ang(0,0,0)
 ,ang(0,0,0),ang(90,0,0),ang(90,0,0)
 ,ang(0,0,0),ang(0,0,0),ang(0,0,0)
 ,ang(0,0,0),ang(0,180,0),ang(0,0,0)
 ,ang(0,0,0),ang(0,0,0),ang(0,0,0)
 ,ang(0,0,0),ang(0,0,0),ang(0,0,0)
 ,ang(0,0,0),ang(0,0,0),ang(0,0,0)
 ,ang(0,0,0),ang(0,0,0),ang(0,0,0)
    )
}
timer("spawn",2000)
if(changed(E)&E|clk("spawn")&Props:count()!=M:count()){
for(P=1,M:count()){
    if(!Props[P,entity]){
    Pos=E:toWorld(vec(0,0,0))
    Props[P,entity]=propSpawn(M[P,string],Pos+Posi[P,vector],Ang[P,angle],1)   
    Props[P,entity]:parentTo(E)
    }
    Props[P,entity]:setMaterial("phoenix_storms/gear")
    Props[P,entity]:setColor(vec(75,75,75))
}
}

E:propFreeze(0)
if(last()){propDeleteAll()  E:setAng(ang())}
#######shield######
timer("prop",1500)
if(clk("prop")){
if(!Sh){
Sh=propSpawn("models/hunter/misc/sphere175x175.mdl",(E:toWorld(vec(0,0,150))),ang(),1)   
}
Sh:setAlpha(100)
Sh:setMaterial("phoenix_storms/gear")
Sh:setColor(vec(75,75,75))
}
if(!Driver){
Sh:setPos(E:pos()+vec(0,0,550))    
}else{
Sh:setPos(E:pos()+vec(0,0,20))
}

Sh:setAng(ang(Counter*2.5))
###################
rangerFilter(Sh)
rangerFilter(Driver)
rangerFilter(Seat)
rangerFilter(Props[8,entity])
rangerFilter(Props[2,entity])
    rangerFilter(E)
Ran=rangerOffset(100,E:pos(),E:up()*-100)


if(Ran:hitWorld()&!Space){
    E:setPos(Ran:pos()+vec(0,0,40))
}
if(!Driver){
 E:setPos(Ran:pos()+vec(0,0,40))   
}
E:setMaterial("phoenix_storms/gear")
E:setColor(vec(75,75,75))
if(first()){
 E:propFreeze(1)   
}

if(first()){
    
 holoCreate(1,E:toWorld(vec(0,0,20)),vec(6,6,6),ang(),vec(75,0,75),"icosphere3")   

 holoCreate(2,E:toWorld(vec(-50,30,20)),vec(1,1,1),ang(),vec(25,150,25),"hqsphere")   
 holoCreate(3,E:toWorld(vec(-50,-30,20)),vec(1,1,1),ang(),vec(25,150,25),"hqsphere")   
 holoCreate(4,holoEntity(3):toWorld(vec(0,0,0)),vec(0.9,0.9,0.9),ang(),vec(25,150,25),"models/mechanics/wheels/wheel_smooth2.mdl")   
 holoCreate(5,holoEntity(2):toWorld(vec(0,0,0)),vec(0.9,0.9,0.9),ang(),vec(25,150,25),"models/mechanics/wheels/wheel_smooth2.mdl")   

 holoCreate(6,E:toWorld(vec(10,55,10)),vec(0.5,0.5,0.5),ang(0,0,0),vec(75,75,75),"models/props_phx/box_torpedo.mdl")   
 holoCreate(7,E:toWorld(vec(10,-55,10)),vec(0.5,0.5,0.5),ang(0,0,0),vec(75,75,75),"models/props_phx/box_torpedo.mdl")  
holoMaterial(6,"phoenix_storms/gear")
holoMaterial(7,"phoenix_storms/gear")
}
holoAng(6,E:toWorld(ang()))
holoAng(7,E:toWorld(ang()))
Mouse1=Driver:keyAttack1()
Mouse2=Driver:keyAttack2()
timer("relaod2",1000)
if(Mouse2&clk("relaod2")){
      soundPlay(1,999,"weapons/flashbang/flashbang_explode2.wav")
 P1=propSpawn("models/props_phx/torpedo.mdl",(holoEntity(7):toWorld(vec(100,0,0))),0)
P1:propGravity(0)  
P1:setMass(50000)
P1:setTrails(10,30,1,"trails/smoke",vec(75,75,75),255)
}

RP=rangerOffset(50,P1:pos(),P1:forward()*50)

if(RP:hitWorld()){
 P1:propBreak()   
}
P1:applyForce(P1:forward()*P1:mass()*5000)
timer("reload",450)
if(Mouse1&clk("reload")){
    soundPlay(1,999,"weapons/det_revolver/revolver-fire.wav")
 P2=propSpawn("models/props_phx/amraam.mdl",(holoEntity(6):toWorld(vec(100,0,0))),0)
P2:propGravity(0)  
P2:setMass(50000)
P2:setTrails(10,30,1,"trails/smoke",vec(75,75,75),255)
}
P2:applyForce(P2:forward()*P2:mass()*5000)
AngYaw=round(Driver:aimPos()-P2:pos()):toAngle()
RP=rangerOffset(50,P2:pos(),P2:forward()*50)

if(RP:hitWorld()){
 P2:propBreak()   
}
holoParent(8,6) holoParent(9,6)
DA=Driver:eyeAngles()
holoAng(1,ang(Counter*1))
holoAng(2,ang(0,0,0+Counter*3.5)+E:toWorld(ang(90,0,0)))
holoAng(3,ang(0,0,0+Counter*-3.5)+E:toWorld(ang(90,0,0)))
holoPos(7,E:toWorld(vec(10,-55,10)))
holoPos(6,E:toWorld(vec(10,55,10)))
holoParent(7,E)
holoParent(6,E)
holoParent(5,2)
holoParent(4,3)
holoParent(3,E)
holoParent(2,E)
holoParent(1,E)
if(Driver){
Driver:setAlpha(0)    
}
if(!Driver){
Driver:setAlpha(255)    
}
##########egp################
timer("egp",350)
if(changed(E)&E|clk("egp")){
    for(P=1,players():count()){
        Ply=players()[P,entity]
 Egp:egp3DTracker(P+100,Ply:attachmentPos("chest"))   
Egp:egpBoxOutline(P+15,vec2(),vec2(15,25))
  Dist=round(entity():pos():distance(Ply:pos()))  
Egp:egpText(P+10,P+" "+Ply:name()+" Distance from Mech ("+Dist+")",vec2())
Egp:egpParent(P+10,P+100)
Egp:egpParent(P+15,P+100)
if(Ply:isAlive()){Egp:egpColor(P+10,vec(0,0,255))}else{Egp:egpColor(P+10,vec(255,0,0))}

}
}


if(first()){holoCreate(123,Seat:toWorld(vec(0,0,30)),vec(0.1,0.1,0.1),ang(0,0,90),vec(100,0,100),"hq_torus_thin")}

holoPos(123,Driver:shootPos()+Driver:eye()*25)
holoAng(123,Driver:eyeAngles()+ang(90,0,0))
holoParent(123,E)
############mingun/turret@###############
R=Driver:keyReload()
if(first()|duped()|dupefinished()){
 holoCreate(10,E:toWorld(vec(40,20,0)),vec(1,1,2),ang(),vec(75,75,75),"hqcylinder")   
 holoCreate(11,holoEntity(10):pos()+vec(12,0,0),vec(0.2,0.2,0.2),ang(),vec(75,75,75),"models/XQM/panel360.mdl")   
 holoCreate(12,holoEntity(11):pos()+vec(3,0,10),vec(0.2,0.2,2.4),ang(0,0,0),vec(75,75,75),"hqtube")   
 holoCreate(13,holoEntity(11):pos()+vec(-3,0,10),vec(0.2,0.2,2.4),ang(0,0,0),vec(75,75,75),"hqtube")   
 holoCreate(14,holoEntity(11):pos()+vec(0,3,10),vec(0.2,0.2,2.4),ang(0,0,0),vec(75,75,75),"hqtube")   
 holoCreate(15,holoEntity(11):pos()+vec(0,-3,10),vec(0.2,0.2,2.4),ang(0,0,0),vec(75,75,75),"hqtube")   
 holoCreate(16,holoEntity(11):pos()+vec(0,0,10),vec(0.2,0.2,0.2),ang(90,0,0),vec(75,75,75),"models/XQM/panel360.mdl")   
 holoCreate(17,holoEntity(11):pos()+vec(0,0,20),vec(0.2,0.2,0.2),ang(90,0,0),vec(75,75,75),"models/XQM/panel360.mdl")   


 holoCreate(20,E:toWorld(vec(40,-20,0)),vec(1,1,2),ang(),vec(75,75,75),"hqcylinder")   
 holoCreate(21,holoEntity(20):pos()+vec(12,0,0),vec(0.2,0.2,0.2),ang(),vec(75,75,75),"models/XQM/panel360.mdl")   
 holoCreate(22,holoEntity(21):pos()+vec(3,0,10),vec(0.2,0.2,2.4),ang(0,0,0),vec(75,75,75),"hqtube")   
 holoCreate(23,holoEntity(21):pos()+vec(-3,0,10),vec(0.2,0.2,2.4),ang(0,0,0),vec(75,75,75),"hqtube")   
 holoCreate(24,holoEntity(21):pos()+vec(0,3,10),vec(0.2,0.2,2.4),ang(0,0,0),vec(75,75,75),"hqtube")   
 holoCreate(25,holoEntity(21):pos()+vec(0,-3,10),vec(0.2,0.2,2.4),ang(0,0,0),vec(75,75,75),"hqtube")   
 holoCreate(26,holoEntity(21):pos()+vec(0,0,10),vec(0.2,0.2,0.2),ang(90,0,0),vec(75,75,75),"models/XQM/panel360.mdl")   
 holoCreate(27,holoEntity(21):pos()+vec(0,0,20),vec(0.2,0.2,0.2),ang(90,0,0),vec(75,75,75),"models/XQM/panel360.mdl")    
}
holoAng(21,holoEntity(20):toWorld(ang(90,90,0+Counter*-4.5)))
holoAng(20,E:toWorld(ang(0,90,90)))
holoParent(28,E)
holoParent(27,21)
holoParent(26,21)
holoParent(25,21)
holoParent(24,21)
holoParent(23,21)
holoParent(22,21)
holoParent(21,20)
holoParent(20,E)

holoAng(11,holoEntity(10):toWorld(ang(90,90,0+Counter*4.5)))
holoAng(10,E:toWorld(ang(0,90,90)))
holoParent(18,E)
holoParent(17,11)
holoParent(16,11)
holoParent(15,11)
holoParent(14,11)
holoParent(13,11)
holoParent(12,11)
holoParent(11,10)
holoParent(10,E)
timer("rr",500)
if(R&clk("rr")){
    soundPlay(1,999,"weapons/airboat/airboat_gun_energy1.wav")
    Mi=propSpawn("models/props_junk/garbage_glassbottle003a.mdl",(E:toWorld(vec(40,20,0))),0)
    Mi:propGravity(0)
    Mi:setTrails(20,0,0.2,"trails/smoke",vec(75,75,75),255)
    Mi:setAng(E:toWorld(ang()))
    Mi:applyForce(E:forward()*Mi:mass()*5000)
}
if(R&clk("rr")){
    M2=propSpawn("models/props_junk/garbage_glassbottle003a.mdl",(E:toWorld(vec(40,-20,0))),0)
    M2:propGravity(0)
    M2:setTrails(20,0,0.2,"trails/smoke",vec(75,75,75),255)
    M2:setAng(E:toWorld(ang()))
    M2:applyForce(E:forward()*M2:mass()*5000)
}
M2:setMass(150)
Mi:setMass(150)
#########################33
A0=owner():keyPressed("0")
if(A0){
 Seat:killPod()   
}
#######################

####more guns xD#######
if(first()){
    holoCreate(30,E:toWorld(vec(0,30,50)),vec(0.1,0.1,0.1),ang(),vec(75,75,75),"models/props_phx/box_torpedo.mdl")
       holoCreate(31,E:toWorld(vec(0,35,50)),vec(0.1,0.1,0.1),ang(),vec(75,75,75),"models/props_phx/box_torpedo.mdl")
       holoCreate(32,E:toWorld(vec(0,35,55)),vec(0.1,0.1,0.1),ang(),vec(75,75,75),"models/props_phx/box_torpedo.mdl")
       holoCreate(33,E:toWorld(vec(0,30,55)),vec(0.1,0.1,0.1),ang(),vec(75,75,75),"models/props_phx/box_torpedo.mdl")
    
      holoCreate(34,E:toWorld(vec(0,-30,50)),vec(0.1,0.1,0.1),ang(),vec(75,75,75),"models/props_phx/box_torpedo.mdl")
       holoCreate(35,E:toWorld(vec(0,-35,50)),vec(0.1,0.1,0.1),ang(),vec(75,75,75),"models/props_phx/box_torpedo.mdl")
       holoCreate(36,E:toWorld(vec(0,-35,55)),vec(0.1,0.1,0.1),ang(),vec(75,75,75),"models/props_phx/box_torpedo.mdl")
       holoCreate(37,E:toWorld(vec(0,-30,55)),vec(0.1,0.1,0.1),ang(),vec(75,75,75),"models/props_phx/box_torpedo.mdl")
}
holoParent(30,E)
holoParent(31,E)
holoParent(32,E)
holoParent(33,E)
holoParent(34,E)
holoParent(35,E)
holoParent(36,E)
holoParent(37,E)
########rockets#######

F=Driver:keyPressed("F")
if(changed(F)&F){
 MM=propSpawn("models/props_phx/ww2bomb.mdl",holoEntity(33):toWorld(vec(0,0,0)),0) 
MX=propSpawn("models/props_phx/ww2bomb.mdl",holoEntity(35):toWorld(vec(0,0,0)),0) 

MX:setTrails(15,0,1,"trails/smoke",vec(25,25,25),255)
MM:setTrails(15,0,1,"trails/smoke",vec(25,25,25),255)

MM:propGravity(0)
MX:propGravity(0)
}
MX:applyForce(MX:forward()*MX:mass()*5000)
MM:applyForce(MM:forward()*MM:mass()*5000)
ANG=Driver:eyeAngles()
MX:setAng(ANG)
MM:setAng(ANG)
######################
######lock#########
if(first()){AEE=0}
Last=lastSaid():explode(" ")
if(chatClk(owner())&Last:string(1)=="/lock"){
    AEE=1
     hideChat(1)
    Seat:lockPod(AEE)
 print("You have locked your Mech")
}

if(chatClk(owner())&Last:string(1)=="/unlock"){
    AEE=0
    hideChat(1)
  Seat:lockPod(AEE)
 print("You have unlocked your mech") 
}   
######################
#####eject#####
if(chatClk(owner())&Last:string(1)=="/eject"){
    Seat:ejectPod()
}
##################
