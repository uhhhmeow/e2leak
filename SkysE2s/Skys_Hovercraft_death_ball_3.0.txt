@name Skys Hovercraft death ball
@inputs Pod:wirelink Cam:wirelink
@persist  T C C2 C3 C4 T2 T3 T4 T5 T6 T7 T8 T9 T10 R1 S GunSec Flir CamTog Prop:entity Props:array Ranger:ranger Craft:entity CamPos:vector Bullet:entity  Cam:wirelink Pod:wirelink [E P Dri]:entity PWrk VSV Ign VelLL Mul Ntrpl HoverHeight FlyAng SpeedMultiplier YawSpeed
@model models/hunter/misc/sphere2x2.mdl

#Camera Position Toggle

if(changed(Pod["Mouse2",number])&Pod["Mouse2",number]){CamTog = CamTog + 1}

if(CamTog >= 2){CamTog = 0}

if(CamTog == 0){CamPos = E:pos()+E:forward()*-1+E:up()*1}

if(CamTog == 1){CamPos = E:pos()+E:forward()*10+E:up()*50}

    

#Camera

Cam["Position",vector] = CamPos

Cam["Activated",number] = Pod["Active",number]

Cam["Angle",angle] = Pod["Entity",entity]:driver():eyeAngles()

    

#Camera FLIR Vision

if(changed(Pod["R",number])&Pod["R",number]){Flir = Flir + 1}

if(Flir >= 2){Flir = 0}

Cam["FLIR",number] = Flir

#Turret Timer

GunSec++



#Turret Fire

if(Pod["Mouse1",number] & changed(int(GunSec/5))){

    Ranger = rangerOffset(CamPos,CamPos+((vec(25000,0,0)):rotate(Pod["Entity",entity]:driver():eyeAngles())))

    Bullet:propBreak()

    Bullet = propSpawn("models/props_phx/mk-82.mdl",Ranger:pos()+vec(0,0,50),ang(90,0,0),1)

    Bullet:setAlpha(0)

}

Bullet:propBreak()
     entity():setColor(random(255),random(255),random(255))



    if(changed(P:keyAttack2())& P:keyAttack2() ){Prop = propSpawn("models/props_junk/watermelon01.mdl", P:pos()+(vec(0,0,60)), P:eyeAngles(), 0)
        , P:soundPlay(1,0, "wilhelm.wav")
        Prop:setTrails(50,1,1, "trails/lol", vec(100,0,100),250)
        Prop:setAlpha(255)
        Props:pushEntity(Prop)
}



interval(1)

 #P9
 T9=T9*1+0.07 
 X9=cos(T9)
 Y9=sin(T9)

 #Neptune
 holoCreate(11)
 holoScale(11, vec(1.1,1.1,1.1))
 holoPos(11, entity():toWorld(vec(X9*125,Y9*150,0)))
 holoParent(11, entity())
 holoColor(11, vec(0,0,25))
 holoModel(11, "hqsphere")

if(duped() | dupefinished())
{
    reset()
}

if(first())
{
    Ntrpl = 90/(500/33) #If your hovercraft is spazzing out, try lowing that 90 in the interval (Line 4) and this line here. Warning: Decreasing the interval will increase the ops usage.
    
    E = entity()
    P = entity():isConstrainedTo()
    
    E:setMass(5000)
    E:propFreeze(0)
     entity():setColor(random(255),random(255),random(255))
     E:setTrails(50,1,1, "trails/lol", vec(100,0,100),250)
    soundPlay("idl",0,"vehicles/diesel_loop2.wav")
    soundPlay("thr",0,"ambient/machines/turbine_loop_2.wav")
    soundPitch("idl",0)
    soundPitch("thr",0)
     E:setTrails(50,1,1, "trails/lol", vec(100,0,100),250)
    rangerHitWater(1)
    rangerPersist(1)

 interval(1)
interval(1)
holoCreate(1)
 holoPos(1,holoEntity(1):pos()-holoEntity(1):up()*200)
  holoScaleUnits(1,vec(40.100,30,40))
holoPos(1,(E:massCenter()+E:right()*-45)-E:up()*30-P:up()*10)
 holoMaterial(1,"phoenis_storms/wire/pck_green")

 E = entity()



    #Settings:
    HoverHeight = 60 #Self-explanitory
    FlyAng = -25 #Angle at which you want to fly towards when you hold Space
    SpeedMultiplier = 1 #Mess with this number if you wanna fly fast
    YawSpeed = 8 #Speed at which the hovercraft turns left and right
}

if(PWrk)
{
    Dri = P:driver()
    Act = P:driver():isValid()
    
    if(Act)
    {
        W = Dri:keyForward()
        A = Dri:keyLeft()
        S = Dri:keyBack()
        D = Dri:keyRight()
        Shi = Dri:keySprint()
        Spc = Dri:keyJump()
        R = Dri:keyReload()
        M1 = Dri:keyAttack1()
        M2 = Dri:keyAttack2()
        
        if(changed(R) & R)
        {
            Ign = !Ign
            soundStop("fail")
            soundPitch("thr",0)
            soundPlay("ignv",Ign ? 0.874 : 1.13, Ign ? "hl1/fvox/activated.wav" : "hl1/fvox/deactivated.wav")
        }
    }
    
    Mul += Ign ? 0.0125 : -0.0125
    Mul = clamp(Mul,0,1)
    if(changed(Mul))
    {
        soundPitch("idl",Mul*65)
    }
	
    if(changed(P))
    {
        PWrk = P:isValid()
    }
    
    VelLL = E:vel():length()
    DV = $VelLL
}else
{
    findByClass("prop_vehicle_prisoner_pod")
    findSortByDistance(E:pos())
    if(find():model() == "models/nova/jeep_seat.mdl" & find():owner() == owner())
    {
        P = find()
        P:setPos(E:toWorld(vec(0,5.5,17.25))) #Mess with that vector to change the offset position of the chair
        P:setAng(E:toWorld(ang(0,180,0))) #If you DO want your chair to face backwards or something, go ahead and mess with this
        
        timer("parent",20)
    }
    if(clk("parent"))
    {
        stoptimer("parent")
        
        P:parentTo(E)
        
        rangerFilter(array(E,P))
        
        PWrk = 1
    }
}

if(Ign)
{
    VelL = E:velL()
    VSV = clamp(-E:velL():y()/7.5,-1500,1500)
    
    if(changed(VSV))
    {
        soundPitch("thr",abs(VSV))
    }
    
    GR = rangerOffset(HoverHeight, E:pos() + (E:right() * clamp(VSV,0,1500)), vec(0,0,-1))
    FR = rangerOffset(25 + clamp(VSV,0,1500), mix(E:pos(), GR:pos(), 0.5), E:right())
    
    applyForce(((vec(0,0,(HoverHeight - GR:distance()) * GR:hit()))*0.75 + (E:right() * (W-S) * SpeedMultiplier * (10 * 1 + Spc*3 +  Shi*4 + FR:hit() * 3)) - E:toWorldAxis(VelL * vec(0.025,0.006125,0.045))) * E:mass() * Ntrpl)
    applyAngForce(((ang(E:angVel():yaw() / 6 - (2 * !GR:hit()), (A-D) * YawSpeed, ((Spc * FlyAng * (W-S)) + clamp((E:angles():roll() - 35) * FR:hit() * W,-90,0))  * (W|S))  - E:angles()*ang(1,0,1))*5 - E:angVel()/ang(2.5,2.5,1.5)) * E:mass() * Ntrpl)
    
    if(DV < -200) #Mess with the number if you wanna change the resistence to crashing
    {
        Ign = 0
        soundStop("ignv")
        soundPitch("thr",0)
        soundPlay("crash",0.8,"vehicles/v8/vehicle_impact_heavy"+randint(1,4):toString()+".wav")
        soundPlay("fail",4.176,"hl1/fvox/hev_critical_fail.wav")
    }
}
