@name Spoofzor's toLocal() Tutorial
@inputs Screen:wirelink
@persist [S Ply]:entity Smart1
#The function toLocal() is really easy to use, but tricky to understand.
#Once you understand it however, it's amazing.

interval(75)
#Personally I like to use interval(75) for touch screens.
#It offers a nice balance between reducing ops, and good response.

if(changed(->Screen) & ->Screen){
    S = Screen:entity()
    Screen[2041]=1
}
#The symbols -> are a way of detecting if the Input variable is wired.
#So if there is a change in the variable being wired and its wired...
#We assigned variable S, an entity, to be the screen prop itself.
#Then as a nice precaution we clear the screen before we use it.

#For this next section variable Ply will represent the player who is currently using the screen.
#So first, we need to get a player if there isnt one:
if(!Ply){
    findInCone(S:pos(),S:forward(),150,50)
    #findInCone finds the number of entities in a cone shape
    #The arguments above are:
    #S:pos() - where to start the cone
    #S:forward() - the direction to create the cone
    #150 - The length of the cone
    #50 - The angle of the cone's sides
    findClipToClass("player")
    #Here we just make sure that the only entities the cone finds are players
    Plys = findToArray()
    #Here we assign local variable Plys the array of all the players it finds so we can use them.
    
    for(P=1,Plys:count()){
        #Just your standard for loop.
        #You have a variable, in this case P, that starts at 1.
        #For every execution of this loop that variable will increase by 1
        #The loop will run until variable P is equal to the second value
        #In this case Plys:count() - which is the number of players in the array Plys
        if(Plys[P,entity]:aimEntity()==S){
            #If the current player the loop is checking is aiming at the screen...
            Ply = Plys[P,entity]
            #We assigned Ply (our player variable) to the player aiming at the screen
            Smart1 = 0
            #I like to use a 'Smart' variable that helps keep the ops low by reducing how much is written to the screen.
            #and how often.
            break
            #This is just exits the for loop
            #We are done, there is no need to continue running the for loop
        }
    }    
}elseif(Ply){
    #Now that we have our player, we can begin the touch screen portion.
    if(!Smart1){
        Screen:writeString("Testing!",0,0,999,0)
        #Just a simple string for now.
        
        Smart1 = 1
        #Now that we have written to the screen we can set Smart1 to 1, so it will only write once.
    }
    if(Ply:aimEntity()==S){
        #If our Player IS looking at the screen we can begin our touch screen work.
        
        if(changed(Ply:keyUse()) & Ply:keyUse()){
            #Once again we are using changed()
            #If our Player hits their Use key (e), it will trigger these events:
            
            AimPos = S:toLocal(Ply:aimPos())
            #This will return a vector relative to the screen entity, rather than the world.
            #Each value in this vector (x,y,z) is important.
            #First we need a way to get those values to start making our touch portion.
            X = round(AimPos:x()*100)/100
            Y = round(AimPos:y()*100)/100
            Z = round(AimPos:z()*100)/100
            #We have each value, multiplied then rounded then divided so we can chop off the decimals
            #Now that we have each value assigned to its own variable, we can use those values
            #But first we need to figure out what they are so we print them to our chat:
            print(X,Y,Z)

            #Once you have all three variables you can begin making your if-statements
            #Just like a Graphic's Tablet:
            if(inrange(vec(X,Y,Z),S:toLocal(vec()),S:toLocal(vec(25,25,3)))){
                #Do stuff
            }
        }
        
    
    }elseif(Ply:aimEntity()!=S){
        Ply = noentity()
        Screen[2041]=1
        #If our Player isn't looking at the screen anymore then we don't need to track them.
        #And we can get back to searching for another Player who is.
        #Also, we clear the screen just for the effect and the sake of this tutorial.
    }
    
}
