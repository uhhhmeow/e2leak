@name Hover
@persist Mass Height HoverForceMult Tilt HeightShutoff MomentumStopMult HoverForceMult2
@persist RotationStopMult
@persist Craft:entity Ang:angle Corner:array
@persist [Forward V1 V2 V3 V4]:vector
@model models/hunter/plates/plate1x2.mdl



if(first()|dupefinished()){
    
    runOnTick(1)
        
    Height            = 50   
    HoverForceMult    = 2    
    HoverForceMult2   = 1.6  
    HeightShutoff     = 0    
    MomentumStopMult  = 8   
    RotationStopMult  = 0.9
    
       
    Craft=entity()
    Craft:setMass(3000)
   
    Mass=0
    Props=Craft:getConstraints()
    for(A=1, Props:count()) {Props[A,entity]:setMass(2) Mass=Mass+2}
    Mass=Mass+Craft:mass()
    Tilt=0
    rangerPersist(1)
    rangerHitWater(1) 
    rangerFilter(Props)
    rangerFilter(Craft)
    rangerFilter(players())
    findExcludePlayer(owner())
    
        
    C1=Craft:toWorld(vec( 24,-47, 0))
    C2=Craft:toWorld(vec(-24,-47, 0))
    C3=Craft:toWorld(vec( 24, 47, 0))
    C4=Craft:toWorld(vec(-24, 47, 0))
    
   
    timer("RangerUpdate",30000)
}

C1=Craft:toWorld(vec( 24,-47, 0))
C2=Craft:toWorld(vec(-24,-47, 0))
C3=Craft:toWorld(vec( 24, 47, 0))
C4=Craft:toWorld(vec(-24, 47, 0))
Forward  = Craft:right()
CraftPos = Craft:pos()
CraftAng = Craft:angles()

if(inrange(CraftAng,ang(-90,-180,-90),ang(90,180,90))){
        #Ranger bundle
    R1=rangerOffset(20000,C1,vec(0,0,-1))
    D1=R1:distance()
    R2=rangerOffset(20000,C2,vec(0,0,-1))
    D2=R2:distance()
    R3=rangerOffset(20000,C3,vec(0,0,-1))
    D3=R3:distance() 
    R4=rangerOffset(20000,C4,vec(0,0,-1))
    D4=R4:distance() 
    AverageHeight=(D1+D2+D3+D4)/4
    
        ###Hover Code
    if(D1<Height){
        V1=(vec(0,0,Height-D1+Tilt))*Mass/HoverForceMult2
        Craft:applyOffsetForce(V1+$V1*HoverForceMult,C1)
    }
    if(D2<Height){
        V2=(vec(0,0,Height-D2-Tilt))*Mass/HoverForceMult2
        Craft:applyOffsetForce(V2+$V2*HoverForceMult,C2)
    }
    if(D3<Height){
        V3=(vec(0,0,Height-D3+Tilt))*Mass/HoverForceMult2
        Craft:applyOffsetForce(V3+$V3*HoverForceMult,C3)
    }
    if(D4<Height){
        V4=(vec(0,0,Height-D4-Tilt))*Mass/HoverForceMult2
        Craft:applyOffsetForce(V4+$V4*HoverForceMult,C4)
    }
    if(Craft:isUnderWater()){
        V100 = vec(0,0,40) * Mass
        Craft:applyForce(V100)
    }
    
        ##Slowdown code
    if(AverageHeight<Height+HeightShutoff){
        V5 = (( CraftPos - Craft:vel()/2 ) - CraftPos) * Mass/MomentumStopMult
        Craft:applyForce(V5)
    }
    
        ##Stop Spin code
    A1 = (( CraftAng - Craft:angVel()/RotationStopMult) - CraftAng) * Mass
    Craft:applyAngForce(A1)

  
    
}
