@name stargate by sky
@inputs 
@outputs 
@persist [Models Positions Angles Props]:array FFA
@trigger 
@model models/hunter/plates/plate1x1.mdl
E=entity()
###########props/base############
14
if(first()){
runOnTick(1)
runOnLast(1)
runOnChat(1)
  
 Models=array(
"models/props_phx/construct/metal_angle360.mdl","models/hunter/blocks/cube075x1x025.mdl","models/hunter/blocks/cube075x1x025.mdl",
"models/hunter/blocks/cube075x1x025.mdl","models/hunter/blocks/cube075x1x025.mdl","models/hunter/blocks/cube075x1x025.mdl",
"models/hunter/blocks/cube075x1x025.mdl","models/hunter/blocks/cube075x1x025.mdl","models/hunter/blocks/cube075x1x025.mdl"
,"models/hunter/blocks/cube075x1x025.mdl","models/hunter/blocks/cube075x1x025.mdl","models/hunter/blocks/cube075x1x025.mdl"
,"models/hunter/blocks/cube075x1x025.mdl","models/hunter/blocks/cube075x1x025.mdl","models/hunter/blocks/cube075x075x025.mdl"
,"","",""
,"","",""

)   
    Positions=array(
    vec(100,0,0),vec(100,0,5),vec(100,40,15)
     ,vec(100,-40,15),vec(100,-70,45),vec(100,70,45)
     ,vec(100,-87,85),vec(100,87,85),vec(100,83,125)
    ,vec(100,-83,125),vec(100,-60,160),vec(100,60,160)
    ,vec(100,25,182),vec(100,-25,182),vec(100,0,187.5)
      ,vec(0,0,0),vec(0,0,0),vec(0,0,0)
      ,vec(0,0,0),vec(0,0,0),vec(0,0,0)
    )
 Angles=array(
ang(0,0,0),ang(0,0,0),ang(0,0,35),
ang(0,0,-35),ang(0,0,-55),ang(0,0,55),
ang(0,0,-80),ang(0,0,80),ang(0,0,110),
ang(0,0,-110),ang(0,0,45),ang(0,0,-45),
ang(0,0,-20),ang(0,0,20),ang(0,0,0),
ang(0,0,0),ang(0,0,0),ang(0,0,0),
ang(0,0,0),ang(0,0,0),ang(0,0,0)

)   
    
}
for(P=1,Models:count()){
 if(!Props[P,entity]){
      POS=E:pos()
Props[P,entity]=propSpawn(Models[P,string],POS+Positions[P,vector],Angles[P,angle],1) 

break
  
} 
Props[P,entity]:parentTo(E)

Props[P,entity]:setMaterial("phoenix_storms/gear")
Props[P,entity]:setColor(100,100,100)
   
}
if(last()){
 propDeleteAll()   
}
#####################


#########holos#####################
holoScale(1,vec(0,1,1))
holoScale(2,vec(0,1,1))
holoScale(3,vec(0,1,1))
if(first()){
    FFA=0
holoCreate(5,Props[1,entity]:pos()+vec(0,0,98),vec(1,15,15),ang(),vec(255,0,0),"hqsphere")
holoAlpha(5,0)
}
timer("123",3000)
if(clk("123")){
holoCreate(6,Props[2,entity]:pos()+vec(0,0,0),vec(2,2,2),ang(),vec(255,0,0),"hqsphere")
holoCreate(7,Props[2,entity]:pos()+vec(0,0,0),vec(2,2,2),ang(),vec(255,0,0),"hqsphere")
holoCreate(8,Props[3,entity]:pos()+vec(0,0,0),vec(2,2,2),ang(),vec(255,0,0),"hqsphere")
holoCreate(9,Props[4,entity]:pos()+vec(0,0,0),vec(2,2,2),ang(),vec(255,0,0),"hqsphere")
holoCreate(10,Props[5,entity]:pos()+vec(0,0,0),vec(2,2,2),ang(),vec(255,0,0),"hqsphere")
holoCreate(11,Props[6,entity]:pos()+vec(0,0,0),vec(2,2,2),ang(),vec(255,0,0),"hqsphere")
holoCreate(12,Props[7,entity]:pos()+vec(0,0,0),vec(2,2,2),ang(),vec(255,0,0),"hqsphere")
holoCreate(13,Props[8,entity]:pos()+vec(0,0,0),vec(2,2,2),ang(),vec(255,0,0),"hqsphere")
holoCreate(14,Props[9,entity]:pos()+vec(0,0,0),vec(2,2,2),ang(),vec(255,0,0),"hqsphere")
holoCreate(15,Props[10,entity]:pos()+vec(0,0,0),vec(2,2,2),ang(),vec(255,0,0),"hqsphere")
holoCreate(16,Props[11,entity]:pos()+vec(0,0,0),vec(2,2,2),ang(),vec(255,0,0),"hqsphere")
holoCreate(17,Props[12,entity]:pos()+vec(0,0,0),vec(2,2,2),ang(),vec(255,0,0),"hqsphere")
holoCreate(18,Props[13,entity]:pos()+vec(0,0,0),vec(2,2,2),ang(),vec(255,0,0),"hqsphere")
holoCreate(19,Props[14,entity]:pos()+vec(0,0,0),vec(2,2,2),ang(),vec(255,0,0),"hqsphere")

}
holoParent(7,E)
holoParent(8,E)
holoParent(9,E)
holoParent(10,E)
holoParent(11,E)
holoParent(12,E)
holoParent(13,E)
holoParent(14,E)
holoParent(15,E)
holoParent(16,E)
holoParent(17,E)
holoParent(18,E)
holoParent(19,E)
######################
########on off command############
Last=owner():lastSaid():explode(" ")
if(chatClk(owner())&Last:string(1)=="/off"){
      hideChat(1)
 holoAlpha(5,0)   
soundPlay(1,999,"hl1/fvox/deactivated.wav")
FFA=0
}
if(chatClk(owner())&Last:string(1)=="/on"){
    hideChat(1)
 holoAlpha(5,255)   
FFA=80
}

if(chatClk(owner())&Last:string(1)=="/on"){
    hideChat(1)
  soundPlay(1,999,"hell_hole/open_effect.wav")   
 soundPlay(1,999,"hl1/fvox/activated.wav")   
}
if(FFA){
FFA=80
}


###########################

 ########teleporting###########
holoMaterial(5,"models/props_combine/tprings_globe")
holoParent(5,E)
findIncludeClass("weapons_*")
findIncludeClass("item_*")
findIncludeClass("pod_*")
findIncludeClass("vehicle_*")
findIncludeClass("ragdoll")
findIncludeClass("emitter_*")
findIncludeClass("expression")
findIncludeClass("playx")
findIncludeClass("camera")
findIncludeClass("prop_*")
findIncludeClass("npc_*")
findIncludeClass("npc_grenade_frag")
findIncludeClass("prop_combine_ball")
findIncludeClass("rpg_missile")
findIncludeClass("crossbow_bolt")
findIncludeClass("player")
findIncludeClass("*")
findIncludeClass("prop")
FC=findClosest(holoEntity(5):pos())
 Find=findInSphere(holoEntity(5):pos(),FFA)
if(Find){
    if(FC==owner()){
     FC:plySetHealth(999999999)   
    FC:plySetArmor(9999999999)
    }
      FC:plySetArmor(1000)
    FC:plySetHealth(300)
    FC:setPos(vec(0,0,4500))
      FC:plySetPos(vec(0,0,4500))
}
#######################

E:propFreeze(1)
E:setAlpha(0)



