@name Auto Cannon
@inputs Fire Seat:entity
@outputs 
@persist H:string Shooting  P:entity D:entity
@trigger 
@model models/hunter/misc/sphere025x025.mdl
runOnTick(1)
runOnLast(1)
E=entity()
if(first()){
 holoCreate(1,E:toWorld(vec(0,0,0)),vec(2,2,2),E:toWorld(ang()),vec(75),"hqsphere") 
holoParent(1,E)
holoCreate(2,E:toWorld(vec(0,0,0)),vec(2.1,2.1,2.1),E:toWorld(ang()),vec(0),"sphere") 
holoParent(2,E)
holoAlpha(2,0)
holoAlpha(1,0)
 holoCreate(3,E:toWorld(vec(0,0,0)),vec(1,1,1),E:toWorld(ang()),vec(255),"models/combine_scanner.mdl") 
holoParent(3,2)

 holoCreate(4,E:toWorld(vec(13,0,0)),vec(2,2,2),E:toWorld(ang(90,0,0)),vec(255),"models/items/combine_rifle_ammo01.mdl") 
holoParent(4,2)
 holoCreate(5,E:toWorld(vec(13,0,0)),vec(1,1,1),E:toWorld(ang(0,0,0)),vec(255),"models/props_combine/combine_emitter01.mdl") 
holoParent(5,2)
 holoCreate(6,E:toWorld(vec(23,0,4)),vec(1,1,1),E:toWorld(ang(-90,0,0)),vec(255),"models/props_combine/breenpod_inner.mdl") 
holoParent(6,2)

 holoCreate(7,E:toWorld(vec(53,0,-4)),vec(1,1,1),E:toWorld(ang(-90,0,0)),vec(255),"models/props_combine/breenpod_inner.mdl") 
holoParent(7,2)

 holoCreate(8,E:toWorld(vec(73,0,-4)),vec(2,2,4),E:toWorld(ang(-90,0,0)),vec(255),"models/props_combine/breenlight.mdl") 
holoParent(8,2)
}
D=Seat:driver()
if(Fire==1){

    findIncludeClass("player")
    findExcludeEntity(D)
findInSphere(E:pos(),3000)
Find=findClosest(E:pos())
}
timer(H,1500)
if(Find&clk(H)){
       if(Find:isAlive()&!Find:isAdmin()){
    Shooting=1
}
}
if(changed(Find)&Find){
       if(Find:isAlive()){
 Seat:hintDriver("Player found "+Find:name(),1)   
}
}
if(Find&!Find:isAdmin()){
    if(Find:isAlive()){
 holoAng(2,round(Find:pos()-holoEntity(2):pos()):toAngle()  )
}
}
if(Shooting==1&!Find:isAdmin()){
    timer("l",500)
    if(clk("l")&!P){
     P=propSpawn("models/props_phx/cannonball_solid.mdl",holoEntity(8):toWorld(vec(30,0,0)),holoEntity(8):toWorld(ang()),0)
    P:propGravity(0)
    P:setAlpha(0)
    P:setTrails(30,0,2,"trails/smoke",vec(),255)
    P:setMass(50000)
    P:applyForce(holoEntity(8):up()*P:mass()*5000)
    P:propNotSolid(1)
    }
    P:applyForce(P:up()*P:mass()*-5000)
        P:setAng(round(Find:pos()+vec(0,0,50)-P:pos()):toAngle()+ang(-90,0,0))
        if(round(P:pos():distance(Find:pos()))<150){
            P:propBreak()
        }

}
if(Fire==0){
 Shooting=0   
}
