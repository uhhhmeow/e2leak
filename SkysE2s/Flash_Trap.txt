@name Flash Trap
@persist Except:array RD:ranger Tripped Dist Trace Target:entity Add:entity
@trigger all
Hit=RD:hit()
interval(10)
runOnChat(1)
rangerDefaultZero(1) 
rangerIgnoreWorld(1)
rangerFilter(Except)
RD=ranger(10000)
holoPos(1,Target:shootPos())
holoParentAttachment(1,Target,"eyes")
if(first()){
    Except:pushEntity(owner())
    holoDeleteAll()
    rangerFilter(Except)
    holoCreate(1,vec(0,0,0),vec(-4,-4,-11),ang(0,0,0),vec(255,255,255))
    holoModel(1,"hqicosphere")
    holoAlpha(1,0)
}
if(Hit&!Trace){Tripped=1 Target=RD:entity()}
if(Tripped){
    holoAlpha(1,255)
    holoPos(1,Target:pos())
    timer("End",15000)
    hint("Alarm tripped by "+Target:name(),10)
    holoEntity(1):soundPlay(1,16000,"ambient/alarms/apc_alarm_loop1.wav")
    Trace=1
    Tripped=0
}
if(clk("End")){
    holoAlpha(1,0)
    soundStop(1)
    Trace=0
}

if(chatClk(owner())){
    PARSE=owner():lastSaid()
    Lastsaid=PARSE:explode(" ")
    if(Lastsaid[1,string]=="-friends"){
        hideChat(1)
        Find=1
        for(A=0,Except:count()){
            print(Except[A,entity]:name())
            if(A==0){print("Your friends are:")}
            if(A==Except:count()){Find=0 A=0}
            
        }
    }
    if(Lastsaid[1,string]=="-add"){
        hideChat(1)
        Add=findPlayerByName(Lastsaid[2,string])
        Except:pushEntity(Add)
        hint("Added "+Add:name()+" to friends list!",5)
    }
    if(Lastsaid[1,string]=="-remove"){
        hideChat(1)
        Add=findPlayerByName(Lastsaid[2,string])
        for(A=1,Except:count()){
            if(Except[A,entity]:name()==Add:name()){
                Except:remove(A)
                hint("Removed "+Add:name()+" from friends",7)
            }
        }       
    }
    if(Lastsaid[1,string]=="-clear"){
        hideChat(1)
        Except=array()
        hint("All friends removed!",7)
    }
}
