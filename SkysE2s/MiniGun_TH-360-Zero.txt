@name MiniGun TH-360-Zero
@inputs Fire
@outputs 
@persist Roll Moveup Minigun ShoT
@trigger all
@model models/hunter/misc/sphere175x175.mdl
runOnTick(1)
E=entity()
if(first()){
function void shot(){
 Roll=Roll+0.4
Moveup=Moveup*1+Roll
if(Roll>50){Roll=50 }
if(Roll>40){MiniGun=1}
}
function void down (){
 Roll=Roll+-0.4
if(Roll<40){MiniGun=0}
if(Roll<0){Roll=0}
if(Roll>0){Moveup=Moveup*1+Roll}   
}
}
if(!holoEntity(1)){
    holoCreate(1,E:toWorld(vec(30,0,0)),vec(6,6,1.5),E:toWorld(ang(90,0,0)),vec(50),"hqcylinder")
    holoParent(1,E)
        holoCreate(2,E:toWorld(vec(40,0,0)),vec(5,5,0.5),E:toWorld(ang(90,0,0)),vec(100),"hqcylinder")
    holoParent(2,E)
        holoCreate(3,E:toWorld(vec(50,0,0)),vec(5,5,1),E:toWorld(ang(90,0,0)),vec(60),"hqtube")
    holoParent(3,E)
            holoCreate(4,E:toWorld(vec(45,0,0)),vec(3,3,1.5),E:toWorld(ang(90,0,0)),vec(200),"cylinder")
    holoParent(4,E)
    
    
         holoCreate(5,E:toWorld(vec(110,15,0)),vec(1.3,1.3,10),E:toWorld(ang(90,0,0)),vec(80),"hqtube")
    holoParent(5,4)
          holoCreate(6,E:toWorld(vec(110,-15,0)),vec(1.3,1.3,10),E:toWorld(ang(90,0,0)),vec(80),"hqtube")
    holoParent(6,4)
          holoCreate(7,E:toWorld(vec(110,0,15)),vec(1.3,1.3,10),E:toWorld(ang(90,0,0)),vec(80),"hqtube")
    holoParent(7,4)
          holoCreate(8,E:toWorld(vec(110,0,-15)),vec(1.3,1.3,10),E:toWorld(ang(90,0,0)),vec(80),"hqtube")
    holoParent(8,4)
    
              holoCreate(9,E:toWorld(vec(110,0,-0)),vec(4.5,4.5,0.4),E:toWorld(ang(90,0,0)),vec(255,0,0),"hqcylinder")
    holoParent(9,4)
                  holoCreate(10,E:toWorld(vec(60,0,-0)),vec(4.5,4.5,0.4),E:toWorld(ang(90,0,0)),vec(255,0,0),"hqcylinder")
    holoParent(10,4)
                      holoCreate(11,E:toWorld(vec(150,0,-0)),vec(4.5,4.5,0.4),E:toWorld(ang(90,0,0)),vec(255,0,0),"hqcylinder")
    holoParent(11,4)
}
if(changed(Fire==1)&Fire==1){soundPlay(1,999,"weapons/airboat/airboat_gun_loop2.wav")}
if(Fire==1){ shot()}
if(Fire==0){down()}

holoCreate(12)
Ran=rangerOffset(999999,holoEntity(4):pos(),holoEntity(4):up()*999999999)
holoPos(12,Ran:pos())
ShoT++
if(MiniGun==1&changed(int(ShoT/26))){
    P=propSpawn("models/props_phx/ww2bomb.mdl",holoEntity(randint(5,8)):pos(),1)
    P:setAlpha(0)
    P:propNotSolid(1)
    P:setTrails(30,0,5,"trails/smoke",vec(255,0,0),255)
}
Dist=round(holoEntity(11):pos():distance(holoEntity(1):pos()))
RDT=randint(Dist/2)
if(changed(P)&P){
P:setPos(holoEntity(12):toWorld(vec(-30,RDT*2+-50,RDT*2+-50)))
P:propBreak()
}
holoAng(4,holoEntity(1):toWorld(ang(0,Moveup/2,0)))
soundPitch(1,Roll*5)
holoColor(12,vec(255,0,0))
