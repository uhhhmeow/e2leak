@name Jig's E2 Finder
@inputs E:wirelink
@persist Players:array I
@model models/props_phx/games/chess/black_dama.mdl
runOnTick(1)
if(owner():lastSaid() == ("lockchip")){
    hideChat(1)
entity():propNotSolid(1)
}
if(owner():lastSaid() == ("unlockchip")){
    hideChat(1)
entity():propNotSolid(0)
}
if (first()|duped()){
    entity():setColor(vec(0,0,255))
    E:egpClear()
    
    findByClass("gmod_wire_expression2")
    findSortByDistance(owner():pos())
    Players = findToArray()
    
    for (Tracker=1,Players:count()){
        Player = Players[Tracker,entity]
        
        Rect = Tracker+Players:count()
        Name = Tracker+Players:count()*2
        
        E:egp3DTracker(Tracker,Player:pos())
        
        
        E:egpBoxOutline(Rect, vec2(0,-Player:height()/2), vec2(0,0))
        E:egpColor(Rect,vec(0,0,255))
        
        E:egpText(Name,Player:name(),vec2(-10,-Player:height()))
        E:egpSize(Name,15)
        
        E:egpParent(Name,Tracker)
        E:egpParent(Rect,Tracker)
    }
    
    timer("reset",10000)
}
for (Tracker=1,Players:count()){
    Player = Players[Tracker,entity]
    X = Tracker+Players:count()
    
    Dist = Player:pos():distance(owner():pos())
    
    E:egpSize(X, vec2(40,50))
    E:egpPos(Tracker,Player:pos())
    
}
if (clk("reset")){
    reset()
}
Bind_E2_List="F"
 E2_Spawn_Alert = 1 #Message when a new E2 is spawned.
findByClass("gmod_wire_expression2")
R=findToArray()
Total=R:count()
if(changed(owner():keyPressed(Bind_E2_List))&owner():keyPressed(Bind_E2_List))
{
    Count=1
}
while(perf()&Count)
{
    I+=1
    if(R:count()>=1)
    {
        printColor(vec(255,255,0),"'"+R[I,entity]:getName()+"'",vec(255,255,255)," owned by ",vec(255,0,0),R[I,entity]:owner(),vec(255,255,255),". CPU usage: ",vec(255,0,0),""+round(R[I,entity]:cpuUsage()*1000000))
    }
    if(I>=R:count())
    {
        printColor(vec(0,255,0),"Total of "+R:count()+" E2s.")
        Count=0
        I=0
    }
}
Delta=$Total
if(changed(Delta==1)&Delta)
{
    timer("namegrab",300)
}
if(clk("namegrab")&E2_Spawn_Alert)
{
    printColor(vec(0,255,0),"New E2 spawned.")
    printColor(vec(255,255,0),"'"+R[1,entity]:getName()+"'",vec(255,255,255)," owned by ",vec(255,0,0),R[1,entity]:owner():name())
}
