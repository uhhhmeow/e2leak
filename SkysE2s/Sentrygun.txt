@name Sentrygun
@persist [Chip Nearest]:entity Timer H Mode
@persist RadiusKill RadiusSentry Speed DelaySentry
Timer+=1
T=Timer*3
T2=Timer*2
if(first()) {
H=1
RadiusKill=500  #radius in dem spieler anvisiert und gefeuert wird
RadiusSentry=200 #radius in dem spieler erkannt werden
Speed=30  #Raketen-Tempo
DelaySentry=100  #sollte nicht zu niedrig sein
DelayKill=300  #delay zwischen dem feuer im kill-mode
Mode=0 #startmode 0 fur sentry 1 fur kill-mode

runOnTick(1) runOnChat(1) runOnLast(1)
Chip=entity()
entity():setAlpha(0)
entity():propNotSolid(0)
holoCreate(25,vec(),vec(0.5),ang(),vec(255,0,0),"hpsphere")
holoEntity(25):setTrails(4,1,3,"trails/laser",vec(255,0,0),255)
################haupt rohr
holoCreate(1,entity():pos()+vec(0,0,40),vec(0.7,0.7,3),ang(90,0,0),vec(255,255,255),"hq_cylinder")
holoMaterial(1,"sprops/textures/sprops_rubber2")

#########Combine ball############################
holoCreate(2,entity():pos()+vec(1.5,0,40),vec(2.3,2.3,0.9),ang(90,0,0),vec(255,255,255),"models/items/combine_rifle_ammo01.mdl")

#######haupt ring##############################
holoCreate(3,entity():pos()+vec(2.5,0,40),vec(2.3,2.3,0.6),ang(90,0,0),vec(255,255,255),"hq_tube_thin")
holoMaterial(3,"sprops/textures/sprops_rubber2")
#######haup ring speichen#################################
#vertikal
holoCreate(4,entity():pos()+vec(2.5,0,40),vec(0.1,2.3,0.1),ang(90,0,0),vec(255,255,255),"cube")
holoMaterial(4,"sprops/textures/sprops_rubber2")
#senkrecht
holoCreate(5,entity():pos()+vec(2.5,0,40),vec(0.1,0.1,2.3),ang(0,0,0),vec(255,255,255),"cube")
holoMaterial(5,"sprops/textures/sprops_rubber2")

####################Gun Pipes########################################
holoCreate(20,entity():pos()+vec(27,0,40),vec(0.2),ang(),vec(255,255,255),"")
#oben
holoCreate(6,entity():pos()+vec(27,0,42.5),vec(0.2,0.2,2),ang(90,0,0),vec(255,255,255),"hq_tube_thin")
holoMaterial(6,"sprops/textures/sprops_rubber2")
#unten
holoCreate(7,entity():pos()+vec(27,0,37.5),vec(0.2,0.2,2),ang(90,0,0),vec(255,255,255),"hq_tube_thin")
holoMaterial(7,"sprops/textures/sprops_rubber2")
#rechts
holoCreate(8,entity():pos()+vec(27,2.5,40),vec(0.2,0.2,2),ang(90,0,0),vec(255,255,255),"hq_tube_thin")
holoMaterial(8,"sprops/textures/sprops_rubber2")
#links
holoCreate(9,entity():pos()+vec(27,-2.5,40),vec(0.2,0.2,2),ang(90,0,0),vec(255,255,255),"hq_tube_thin")
holoMaterial(9,"sprops/textures/sprops_rubber2")
####################Gun Pipes Ring########
#Hinten
holoCreate(10,entity():pos()+vec(23,0,40),vec(0.7,0.7,0.1),ang(90,0,0),vec(255,255,255),"hq_cylinder")
holoMaterial(10,"sprops/textures/sprops_rubber2")
#Vorne
holoCreate(11,entity():pos()+vec(33,0,40),vec(0.7,0.7,0.1),ang(90,0,0),vec(255,255,255),"hq_cylinder")
holoMaterial(11,"sprops/textures/sprops_rubber2")

##############################Ziel Vorrichting####################
#####Ring
holoCreate(12,entity():pos()+vec(2.5,0,56.5),vec(0.3,0.3,0.070),ang(90,0,0),vec(255,255,255),"hq_tube_thin")
holoMaterial(12,"sprops/textures/sprops_rubber2")
####Stab Senkrecht
holoCreate(13,entity():pos()+vec(2.5,0,55.1),vec(0.02,0.02,0.2),ang(0,0,0),vec(255,255,255),"cube")
holoMaterial(13,"sprops/textures/sprops_rubber2")
#####Vertikal
holoCreate(14,entity():pos()+vec(2.5,0,56.2),vec(0.02,0.02,0.3),ang(90,90,0),vec(255,255,255),"cube")
holoMaterial(14,"sprops/textures/sprops_rubber2")

###############Stand Base#########################################
holoCreate(15,entity():pos()+vec(2.5,0,24),vec(0.5,0.5,0.5),ang(0,0,0),vec(255,255,255),"hq_cylinder")
holoMaterial(15,"sprops/textures/sprops_rubber2")

###Base feet###
#Vorne
holoCreate(16,entity():pos()+vec(11.8,0,12.1),vec(0.2,0.2,2.5),ang(-30,0,0),vec(255,255,255),"cube")
holoMaterial(16,"sprops/textures/sprops_rubber2")
#Hinten rechts
holoCreate(17,entity():pos()+vec(-5,-7,13),vec(0.2,0.2,2.5),ang(35,45,0),vec(255,255,255),"cube")
holoMaterial(17,"sprops/textures/sprops_rubber2")
#Hinten Links
holoCreate(18,entity():pos()+vec(-5,7,13),vec(0.2,0.2,2.5),ang(35,-45,0),vec(255,255,255),"cube")
holoMaterial(18,"sprops/textures/sprops_rubber2")

############Monition Base#########################
holoCreate(19,entity():pos()+vec(-7,5.9,33),vec(0.5,1,1.5),ang(0,0,0),vec(255,255,255),"hq_rcube")
holoMaterial(19,"sprops/textures/sprops_rubber2")


holoParent(6,20)  holoParent(7,20)  holoParent(8,20)  holoParent(9,20) holoParent(10,20) holoParent(11,20)  
    for(I=1,19) {
        if(I==2|I==19) {
        holoParent(I,1)
        }
    
        if(I==4|I==5|I==13|I==14|I==12) {
        holoParent(I,3)
        }
    }

}
findInSphere(Chip:pos(),1000)
findIncludeClass("player")
Nearest=findClosest(Chip:pos())
holoPos(20,holoEntity(1):toWorld(vec(0,0,29.5)))

Last=owner():lastSaid()
if(chatClk(owner())&Last=="/change"|first()) {
hideChat(1)
Mode=!Mode
findClearWhiteList()
findClearBlackList()
if(Mode) {
printColor(vec(0,255,0),"Sentry-Mode")
holoEntity(25):setTrails(4,1,10,"trails/smoke",vec(0,255,0),255)
holoColor(25,vec(0,255,0))
hint("NACHGELADEN!!",50)
}else {
printColor(vec(255,0,0),"Kill-Mode")
holoEntity(25):setTrails(4,1,10,"trails/smoke",vec(255,0,0),255)
holoColor(25,vec(255,0,0))
hint("NACHGELADEN!!",50)
}
}

if(Mode) {
findInSphere(Chip:pos(),1000)
findIncludeClass("player")
Nearest=findClosest(Chip:pos())
holoAng(3,ang(90,0,0)+Nearest:eyeAngles():setPitch(0):setRoll(0))
holoAng(1,ang(90,0,0)+Nearest:eyeAngles())
holoAng(20,ang(0,0,T2*2)+Nearest:eyeAngles())
}elseif(!Mode) {
findInSphere(Chip:pos(),1000)
findIncludeClass("player")
findExcludePlayer("Linus [Ger]")
Nearest=findClosest(Chip:pos())
holoAng(3,ang(90,0,0)+(Nearest:attachmentPos("chest")-holoEntity(3):pos()):toAngle())
holoAng(1,ang(90,0,0)+(Nearest:attachmentPos("chest")-holoEntity(1):pos()):toAngle())
holoAng(20,ang(0,0,T2*2)+(Nearest:attachmentPos("chest")-holoEntity(20):pos()):toAngle())
}
##############################################################################################################
##############################################################################################################
##############################################################################################################
if(Mode) {
    timer("Fire",DelaySentry)
    holoPos(25,entity():pos()+vec(sin(Timer)*RadiusSentry,cos(Timer)*RadiusSentry,10)) 
        if((clk("Fire")&Nearest:keyAttack2()&Nearest:pos():distance(Chip:pos())<200&H<=40)|(changed(Nearest:keyAttack1())&Nearest:keyAttack1()&Nearest:pos():distance(Chip:pos())<200&H<=40)) {
        holoCreate(30+H,holoEntity(20):toWorld(vec(22,0,0)),vec(0.3),holoEntity(20):angles(),vec(255),"models/props_phx/mk-82.mdl")
        H++
        }
            for(I=1,H) {
            holoPos(30+I,holoEntity(30+I):pos()+holoEntity(30+I):forward()*Speed)
            RanD=rangerOffset(30,holoEntity(30+I):pos(),holoEntity(30+I):forward())
                if(RanD:hit()) {
                Spawn=propSpawn("models/props_phx/mk-82.mdl",holoEntity(30+I):pos(),ang(),1)
                Spawn:propBreak()
                holoDelete(30+I)
                }
            }
}elseif (!Mode) {
    holoPos(25,entity():pos()+vec(sin(Timer)*RadiusKill,cos(Timer)*RadiusKill,10)) 
    timer("Fire",1000)
        if(clk("Fire")&Nearest:pos():distance(Chip:pos())<RadiusKill&Nearest:isAlive()&H<=40) {
        holoCreate(30+H,holoEntity(20):toWorld(vec(22,0,0)),vec(0.3),holoEntity(20):angles(),vec(255),"models/props_phx/mk-82.mdl")
        H++
        }
            for(I=1,H) {
            holoPos(30+I,holoEntity(30+I):pos()+holoEntity(30+I):forward()*Speed)
            RanD=rangerOffset(30,holoEntity(30+I):pos(),holoEntity(30+I):forward())
                if(RanD:hit()) {
                Spawn=propSpawn("models/props_phx/mk-82.mdl",holoEntity(30+I):pos(),ang(),1)
                Spawn:propBreak()
                holoDelete(30+I)
                }
            }
}

if(H==40) {
reset()
}
