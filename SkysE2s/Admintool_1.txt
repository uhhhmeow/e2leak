@name Admintool 1
@inputs Egp:wirelink
@outputs 
@persist H
@trigger 
interval(100)
E=entity()



if(first()){Egp:egpClear()}
findExcludePlayerProps("SKY")
findExcludePlayer("SKY")
findIncludeClass("wire_expression2")
findInSphere(E:pos(),100000)


AR = findToArray()
for(I=1,AR:count()){
    
    
    Target = AR[I,entity]
    Count=AR:count()
    E2=round(Target:cpuUsage()*1000000)
    Name=Target:owner():name()
    if(first()){hint("E2 Scanner by Sadira",5)}




#Egp:egpText(I,Name+"'s E2:   " + E2 +"us cpu time",vec2(20,I*20))
Egp:egpText(I,"( "+Target:getName() +" ), by: "+Name,vec2(20,I*20))
Egp:egpSize(I,14)
Egp:egpText(50+I," cpu "+E2,vec2(445,I*20))
Egp:egpSize(50+I,14)

Egp:egpText(200,"E2 Scanner",vec2(200,5))
Egp:egpSize(200,14)

H=H-I
if(H<=1){H=360}
Rgb=hsv2rgb(H,1,1)
Egp:egpColor(I,Rgb)
Egp:egpColor(50+I,Rgb)
#Egp:egpText(100,Target:getName() ,vec2(250,50))
if(changed(AR:count())){Egp:egpClear()}
}

