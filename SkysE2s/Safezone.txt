@name Safezone
@inputs 
@outputs 
@persist T2:array T Pos1:array Pos2:array T3
@trigger 
runOnTick(2)
Radius = 4000
T3++

if (first()|duped()) {
    holoCreate(1)
    holoAlpha(1,0)
    holoCreate(2)
    holoAlpha(2,0)
    holoCreate(3)
    holoAlpha(3,0)
    holoEntity(1):setTrails(300,300,4,"trails/laser",vec(255,255,255),255)
    holoEntity(2):setTrails(300,300,4,"trails/laser",vec(255,255,255),255)
    holoEntity(3):setTrails(300,300,4,"trails/laser",vec(255,255,255),255)
}

holoPos(1,entity():pos()+vec(sin(T3*2)*Radius,cos(T3*2)*Radius,0))
holoPos(2,entity():pos()+vec(0,sin(T3*2)*-Radius,cos(T3*2)*-Radius))
holoPos(3,entity():pos()+vec(sin(T3*2)*Radius,0,cos(T3*2)*Radius))

P = players()

if (T == 1) 
{
    for (I=1,P:count())
    {
        Pos1[I,vector] = P[I,entity]:pos()    
    }
    T = 0
    
} else {
    for (I=1,P:count())
    {
        Pos2[I,vector] = P[I,entity]:pos()    
    }
    T = 1
}

for (I=1,P:count())
{
    if (Pos1[I,vector]:distance(entity():pos()) < Radius & Pos2[I,vector]:distance(entity():pos()) < Radius) {
        if (!P[I,entity]:plyInGod()) {
            P[I,entity]:plyGod(1)    
        }
    } elseif (Pos1[I,vector]:distance(entity():pos()) < Radius & Pos2[I,vector]:distance(entity():pos()) > Radius) {
        if (P[I,entity]:plyInGod()) {
            P[I,entity]:plyGod(0)    
        }  
    } elseif (Pos1[I,vector]:distance(entity():pos()) > Radius & Pos2[I,vector]:distance(entity():pos()) < Radius) {
        if (P[I,entity]:plyInGod()) {
            P[I,entity]:plyGod(0)    
        }  
    }
}
