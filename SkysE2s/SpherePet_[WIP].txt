@name SpherePet [WIP]
@inputs 
@outputs Health
@persist Hunger Sounds:array DebugMode CurLoc:vector BefLoc:vector Age Penissize SG Name:string 
@persist Status:string FES FEA FEB I Health MaleNames:array FemaleNames:array
@trigger 
timer("hunger", 10000)
timer("use", 500)
timer("action", 1000)
timer("checkloc", 10000)
timer("forcemove", 9000)
timer("age", 60000)
timer("poo", 10000)
#runOnDamage(1)

Use = owner():keyUse()

if (first()) {
    ######
    #Names
    ######
    MaleNames=array("Robert", "John", "James", "Paul", "Stephen", "Ryan", "Trevor", "Jerry", "Joe", "Robert")
    FemaleNames=array("Julia", "Jennifer", "Jane", "Catherine", "Olivia", "Katie", "Amiee", "Leah", "Fiona")
    DebugMode = 0
    SG = 1
    I = 3
    
    BefLoc = entity():pos()
        
    Sounds:setString(1,"weapons/bugbait/bugbait_squeeze1.wav")
    Sounds:setString(2,"weapons/bugbait/bugbait_squeeze2.wav")
    Sounds:setString(3,"weapons/bugbait/bugbait_squeeze3.wav")
    
    LS = owner():lastSaid():explode(" ")
    holoCreate(1)
    holoCreate(2)
    holoModel(1, "hqicosphere2")
    holoModel(2, "hqicosphere2")
    holoScale(2, vec(-SG - 0.2,-SG - 0.2,-SG - 0.2))
    holoScale(1, vec(SG,SG,SG))
    holoColor(2, vec(0,0,0))
    holoParent(1, entity())
    holoParent(2, entity())
    
    Gender = randint(1,2)
    
    if (Gender == 1) {
        Name = MaleNames[randint(1,MaleNames:count()), string]
        holoColor(1, vec(155,155,255))
    } else {
        Name = FemaleNames[randint(1,FemaleNames:count()), string]
        holoColor(1, vec(255,155,155))
    }
    
    Health = 100
    
}

if ((owner():pos():distance(entity():pos())<=25)) {
    KeyUse = 1
} else {
    KeyUse = 0
}

if (clk("hunger")) {
    Hunger += 1
    
    if (Hunger == 25) {
        holoColor(2, vec(50,0,0))
        Status = "Peckish"
        hint("Your pet is Peckish and needs a little food.",10)
    } elseif (Hunger < 25) {
        holoColor(2, vec(0,0,0))
        Status = "Happy"
    }

    if (Hunger == 50) {
        holoColor(2, vec(100,0,0))
        Status = "Hungry"
        hint("Your pet is Hungry and needs food.",10)
    }
    
    if (Hunger == 75) {
        holoColor(2, vec(150,0,0))
        Status = "Starving"
        hint("Your pet is Starving and needs alot of food!!!",10)
    }
    
    if (Hunger == 100) {
        hint("Your killed your pet you sick fucker, your going to hell.",7)
        selfDestruct()
    }
    
    setName("Name: " + Name + ", Status: " + Status + ", Age: " + Age + ", Penis Size: " + Penissize + " inch")    
}

if (clk("use")) {
    if (owner():keyUse()) {
        if (owner():aimEntity() == entity()) {
            if (Hunger >= 25 & Hunger <= 50) {
                Hunger = 0
                hint("You fed your pet some food.",7)
                FES++
            }
            
            if (Hunger >= 50 & Hunger <= 75) {
                Hunger = 0
                hint("You fed your pet food.",7)
                FEA++
            }
            
            if (Hunger >= 75 & Hunger <= 100) {
                Hunger = 0
                hint("You fed your pet alot of food.",7)
                FEB++
            } else {
                hint("Your pet is not hungry.",7)
            }
        }
    }
}
if (clk("action")) {
    True = randint(0,1)
    if (True == 1) {
        True = randint(1,4)
        
        # Make a movement
        if (True == 1) {
            
            if (DebugMode == 1) {
                print("Moved.")
            }
            entity():applyForce(vec(randint(-100,100),randint(-100,100), 175))
        }
        
        # Make a sound
        if (True == 2) {
            
            if (DebugMode == 1) {
                print("Made a noise.")
            }
            entity():soundPlay(1,200, Sounds[randint(1,3),string])
        }
        
        # Make a sound and movement
        if (True == 3) {
            if (DebugMode == 1) {
                print("Moved and made a noise.")
            }
            entity():soundPlay(1,200, Sounds[randint(1,3),string])
            entity():applyForce(vec(randint(-100,100),randint(-100,100), 175))
        }
        
        # Leap of faith
        if (True == 4) {
            LOF = randint(0,1)
            
            if (LOF == 1) {
                entity():applyForce(vec(randint(-255,255),randint(-255,255), 175))
            }
            
            if (DebugMode == 1) {
                    print("Leap of faith.")
            }

        }
    }
}

if (clk("checkloc")) {
    
    CurLoc = entity():pos()
    
    if (CurLoc == BefLoc) {
        hint("Your pet has died from being frozen, or stuck.",7)
        
        if (Age < 1) {
            hint("Your pet did not even live up to becoming 1.",7)
        } else {
            hint("Your pet died at the age of " + Age + ".",7)
        }
        selfDestruct()
    } else {
        BefLoc = entity():pos()
    }
}

if (clk("age")) {
    True = randint(Age,100)
    
    if (True == Age) {
        hint("Your pet passed away. It lived " + Age + " years.",7)
        selfDestruct()
    } else {
        Age++
        hint("Congratulations! Your pet turned " + Age + ".", 7)
        
        if (Age < 20) {
            SG = 0.05 * Age + 1
            
            holoScale(2, vec(-SG - 0.2,-SG - 0.2,-SG - 0.2))
            holoScale(1, vec(SG,SG,SG))
        } elseif (Age == 20) {
            hint("Congratulations! Your pet matured. It is now 20.",7)
        }
    }
}

if (clk("forcemove")) {
    entity():applyForce(vec(randint(-100,100),randint(-100,100), 175))
}
    
if (clk("poo") & owner():aimEntity() != entity()) {
    if (FEB > 0 & entity():vel() == vec(0,0,0)) {
        holoCreate(I)
        holoScale(I, vec(0.75,0.75,0.75))
        holoModel(I, "icosphere2")
        holoColor(I, vec(160,82,45))
        I++
    } elseif (FEA > 0 & entity():vel() == vec(0,0,0)) {
        holoCreate(I)
        holoScale(I, vec(0.50,0.50,0.50))
        holoModel(I, "icosphere2")
        holoColor(I, vec(160,82,45))
        I++
    } elseif (FES > 0 & entity():vel() == vec(0,0,0)) {
        holoCreate(I)
        holoScale(I, vec(0.30,0.30,0.30))
        holoModel(I, "icosphere2")
        holoColor(I, vec(160,82,45))
        I++
    }
}

#[if (dmgClk()) {
    EDMG = entity():dmginfo()
    
    Health -= EDMG:getDamage()
    
    if (Health <= 0) {
        hint("Your pet has been slaughtered!",7)
        selfDestruct()
    }
}
]#
