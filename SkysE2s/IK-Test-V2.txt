@name IK-Test-V2
@inputs Ent1:entity Ent2:entity
@outputs 
@persist 
@trigger 
runOnTick(1)
E = entity()
if(first()){
          function vector entity:towv(VectorW:vector){
return This:toWorld(VectorW)
}          
  function angle entity:towa(AngleW:angle){
return This:toWorld(AngleW)
}   
    function entity holop(HT,Vectos:vector){
 holoPos(HT,Vectos)   
}
     function entity holoa(Ho,Angl:angle){
 holoAng(Ho,Angl)   
}

function number co(A, B, C){
return acos((A^2 + B^2 - C^2) / (2*A*B))
}
function ik(L1,L2,Hip:vector,End:vector,I1,I2,Bs:entity){
local Rotation = ang(0, 0, 0)
local Axis1 = End-Hip
local Axis = Bs:toLocalAxis(Axis1):rotate(Rotation)
local Angle=Axis:toAngle():setRoll(-bearing(Hip,Bs:angles(),End))
local Dist=min(Axis:length(),L1+L2)
local Quat=quat(Angle)*qRotation(vec(0,90+co(Dist,L1,L2),0))
holoa(I1,Bs:towa(Quat:toAngle()))
holoa(I2,holoEntity(I1):towa(ang(co(L2, L1, Dist)+180,0,0)))
}

            function entity c(Index:number,Posi:vector,Scale:vector,Angle:angle,Colo:vector,Model:string,Parent:entity,Alpha:number){
 holoCreate(Index) holoPos(Index,Posi) holoScale(Index,Scale) holoAng(Index,Angle) holoColor(Index,Colo) holoModel(Index,Model) 
holoParent(Index,Parent) holoAlpha(Index,Alpha)
return holoEntity(Index)  
}

c(1,E:towv(vec(0,20,0)),vec(1,1,1),ang(),vec(255),"",E,255)   
c(2,E:towv(vec(0,20,20)),vec(1,1,1),ang(),vec(255),"",holoEntity(1),255)   
c(3,E:towv(vec(0,20,40)),vec(1,1,1),ang(),vec(255),"",holoEntity(2),255)  

c(4,E:towv(vec(0,-20,0)),vec(1,1,1),ang(),vec(255),"",E,255)   
c(5,E:towv(vec(0,-20,20)),vec(1,1,1),ang(),vec(255),"",holoEntity(4),255)   
c(6,E:towv(vec(0,-20,40)),vec(1,1,1),ang(),vec(255),"",holoEntity(5),255)    
}
ik(20,20,holoEntity(1):pos(),Ent1:pos(),1,2,E)
ik(20,20,holoEntity(2):pos(),Ent2:pos(),4,5,E)
