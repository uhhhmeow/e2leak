@name Warrior mech
@inputs Seat:entity
@outputs 
@persist  Dist Hover XY Space:number JumpAnim EAS Jumpp HH Shottu MORE MovingP P:entity HoloP C Roll Moving Shooting Holo Ammo Explo
@trigger 
@persist PA:entity
@model models/hunter/blocks/cube05x05x025.mdl
Driver=Seat:driver()
C++
E=entity()
runOnTick(1)
runOnChat(1)
runOnLast(1)
runOnKeys(Driver,1)
rangerFilter(E)
Ran=rangerOffset(50,E:pos(),E:up()*-50)
if(!Space&Ran:hit()&Driver){
Distance = 45
}else{
Distance = 40
}

E:setAlpha(0)

E:applyForce(((Ran:pos()+vec(0,0,Distance)-E:massCenter())*6-E:vel())*E:mass())

E:applyAngForce(-E:angVel()*E:mass()*1)
E:applyAngForce((ang(0,E:angles():yaw(),0)-E:angles())*E:mass()*5)
if(first()){
    holoCreate(1,E:toWorld(vec(0,15,0)),vec(1,1,1),E:toWorld(ang()),vec(75,75,75),"hqsphere")
    holoParent(1,E)
        holoCreate(2,E:toWorld(vec(0,-15,0)),vec(1,1,1),E:toWorld(ang()),vec(75,75,75),"hqsphere")
    holoParent(2,E)
    
       holoCreate(3,E:toWorld(vec(-20,15,-10)),vec(1,1,1),E:toWorld(ang()),vec(75,75,75),"hqsphere")
    holoParent(3,1)
        holoCreate(4,E:toWorld(vec(-20,-15,-10)),vec(1,1,1),E:toWorld(ang()),vec(75,75,75),"hqsphere")
    holoParent(4,2)
    
           holoCreate(5,E:toWorld(vec(0,15,-30)),vec(1,1,1),E:toWorld(ang()),vec(75,75,75),"hqsphere")
    holoParent(5,3)
        holoCreate(6,E:toWorld(vec(0,-15,-30)),vec(1,1,1),E:toWorld(ang()),vec(75,75,75),"hqsphere")
    holoParent(6,4)
    
    
        
       holoCreate(7,E:toWorld(vec(-10,15,-5)),vec(1,1,1),E:toWorld(ang(65,0,0)),vec(75,75,75),"cylinder")
    holoParent(7,1)
        holoCreate(8,E:toWorld(vec(-10,-15,-5)),vec(1,1,1),E:toWorld(ang(65,0,0)),vec(75,75,75),"cylinder")
    holoParent(8,2)
    
           holoCreate(9,E:toWorld(vec(-10,15,-20)),vec(1,1,1.5),E:toWorld(ang(-45,0,0)),vec(75,75,75),"cylinder")
    holoParent(9,3)
        holoCreate(10,E:toWorld(vec(-10,-15,-20)),vec(1,1,1.5),E:toWorld(ang(-45,0,0)),vec(75,75,75),"cylinder")
    holoParent(10,4)
    
            holoCreate(11,E:toWorld(vec(0,0,20)),vec(2,2,3),E:toWorld(ang(0,0,180)),vec(75,75,75),"hq_hdome")
    holoParent(11,E)
            holoCreate(12,E:toWorld(vec(0,0,23)),vec(4,4,0.5),E:toWorld(ang(0,0,180)),vec(1,1,1),"hq_hdome")
    holoParent(12,E)
        holoCreate(13,E:toWorld(vec(0,0,23)),vec(4,4,9),E:toWorld(ang(0,0,0)),vec(75,75,75),"hq_hdome")
    holoParent(13,E)
    
            holoCreate(14,E:toWorld(vec(0,0,23)),vec(4,4,1),E:toWorld(ang(0,0,0)),vec(75,75,75),"hq_hdome")
    holoParent(14,E)
    ###pulse cannon :P#####
       holoCreate(15,E:toWorld(vec(0,30,23)),vec(1,1,2),E:toWorld(ang(90,0,0)),vec(75,75,75),"hq_cylinder")
    holoParent(15,E)
         holoCreate(16,E:toWorld(vec(15,30,23)),vec(1.5,1.5,0.01),E:toWorld(ang(90,0,0)),vec(0,0,0),"cylinder")
    holoParent(16,15)
        holoCreate(17,E:toWorld(vec(18,30,23)),vec(1.3,1.3,0.01),E:toWorld(ang(90,0,0)),vec(100,0,100),"cylinder")
    holoParent(17,15)
           holoCreate(18,E:toWorld(vec(20,30,23)),vec(1.1,1.1,0.01),E:toWorld(ang(90,0,0)),vec(25,50,25),"cylinder")
    holoParent(18,15)
    
              holoCreate(19,E:toWorld(vec(10,30,23)),vec(0.3,0.3,2),E:toWorld(ang(90,0,0)),vec(1,0,0),"hqtube")
    holoParent(19,15)
    
            holoCreate(20,E:toWorld(vec(22,30,23)),vec(0.1,0.1,0.1),E:toWorld(ang(90,0,0)),vec(255,0,0),"hqcylinder")
    holoParent(20,15)
    
        holoCreate(21,E:toWorld(vec(20,20,35)),vec(0.1,0.1,1.5),E:toWorld(ang(130,45,0)),vec(255,0,0),"hqcylinder")
    holoParent(21,15)
            holoCreate(22,E:toWorld(vec(20,40,35)),vec(0.1,0.1,1.5),E:toWorld(ang(130,-45,0)),vec(255,0,0),"hqcylinder")
    holoParent(22,15)
          holoCreate(23,E:toWorld(vec(20,40,15)),vec(0.1,0.1,1.5),E:toWorld(ang(-130,-45,0)),vec(255,0,0),"hqcylinder")
    holoParent(23,15)
          holoCreate(24,E:toWorld(vec(20,20,15)),vec(0.1,0.1,1.5),E:toWorld(ang(-130,45,0)),vec(255,0,0),"hqcylinder")
    holoParent(24,15)
    
         holoCreate(25,E:toWorld(vec(-15,30,23)),vec(2,2,1),E:toWorld(ang(90,0,0)),vec(255,0,0),"hq_torus")
    holoParent(25,15)
    
    ##pusle###
      holoCreate(26,E:toWorld(vec(27,30,23)),vec(-0.1,-0.1,-0.1),E:toWorld(ang(0,0,0)),vec(0,0,0),"icosphere3")
    holoParent(26,15)
    ####
    ####
    ###holo minigun 
    holoCreate(27,E:toWorld(vec(0,-33,23)),vec(1,1,2),E:toWorld(ang(90,0,0)),vec(75,75,75),"hq_cylinder")
    holoParent(27,E)
       holoCreate(28,E:toWorld(vec(13,-33,23)),vec(1,1,0.5),E:toWorld(ang(90,0,0)),vec(75,75,75),"cylinder")
    holoParent(28,27)
    
      holoCreate(29,E:toWorld(vec(23,-36,23)),vec(0.3,0.3,5),E:toWorld(ang(90,0,0)),vec(75,75,75),"hqtube")
    holoParent(29,28)
        holoCreate(30,E:toWorld(vec(23,-30,23)),vec(0.3,0.3,5),E:toWorld(ang(90,0,0)),vec(75,75,75),"hqtube")
    holoParent(30,28)
        holoCreate(31,E:toWorld(vec(23,-33,26)),vec(0.3,0.3,5),E:toWorld(ang(90,0,0)),vec(75,75,75),"hqtube")
    holoParent(31,28)
        holoCreate(32,E:toWorld(vec(23,-33,20)),vec(0.3,0.3,5),E:toWorld(ang(90,0,0)),vec(75,75,75),"hqtube")
    holoParent(32,28)
            holoCreate(33,E:toWorld(vec(25,-33,23)),vec(1,1,0.1),E:toWorld(ang(90,0,0)),vec(75,75,75),"cylinder")
    holoParent(33,28)
           holoCreate(34,E:toWorld(vec(35,-33,23)),vec(1,1,0.1),E:toWorld(ang(90,0,0)),vec(75,75,75),"cylinder")
    holoParent(34,28)
          holoCreate(35,E:toWorld(vec(45,-33,23)),vec(1,1,0.1),E:toWorld(ang(90,0,0)),vec(75,75,75),"cylinder")
    holoParent(35,28)
}
if(Driver){
holoAng(15,round(Driver:aimPos()-holoEntity(15):pos()):toAngle()+ang(90,0,0))
holoAng(27,round(Driver:aimPos()-holoEntity(15):pos()):toAngle()+ang(90,0,0))
}else{
holoAng(15,E:toWorld(ang(90,0,0)))
holoAng(27,E:toWorld(ang(90,0,0)))
}


##################minigun shooting#####
R=Driver:keyReload()
if(changed(R)&R){Ammo+=1}
if(changed(Ammo>=2)&Ammo>=2){Ammo=0 }
if(changed(Ammo==1)&Ammo==1){Explo=1 Seat:printDriver("Bomb cannon Mode = 1")}
if(changed(Ammo==0)&Ammo==0){Explo=0 Seat:printDriver("Holo cannon Mode = 1")} 
M1=Driver:keyAttack2()
if(changed(M1)&M1){soundPlay(5,999,"weapons/airboat/airboat_gun_loop2.wav")}
if(M1){
    Roll=Roll+0.1
    Moving=Moving*1+Roll
    if(Roll>60){Roll=60 }
     if(Roll>40){Shooting=1 }
    soundPitch(5,Roll*3)
}
if(!M1){
    Shooting=0
      Roll=Roll+-0.1
    Moving=Moving*1+Roll
    if(Roll<0){Roll=0} 
    if(Roll==0){soundStop(5,1) Roll=0}
     soundPitch(5,Roll*3)
}

 holoAng(28,holoEntity(27):toWorld(ang(0,Moving,0)))
if(Driver){
timer("holoC",300)
if(Shooting==1&clk("holoC")){
    Holo++
    holoCreate(50+Holo,holoEntity(30):toWorld(vec(0,0,40)),vec(0.5,0.5,0.5),holoEntity(30):toWorld(ang(-90,0,0)),vec(255,0,0),"hqsphere")
soundPlay(6,0.5,"weapons/357/357_fire2.wav")
}
timer("antilag",150)

if(Shooting==1&clk("antilag")&Ammo==0){
    for(P=1,Holo){
          RAXA=rangerOffset(0,holoEntity(50+P):pos(),holoEntity(50+P):pos())
    if(!RAXA:hit()){
        holoPos(50+P,holoEntity(50+P):pos()+holoEntity(50+P):forward()*350)   
    }   
    if(RAXA:hit()){holoDelete(50+P)}
    }
  
}
}
if(Explo==1){
    if(Shooting==1&clk("antilag")&Ammo==1){
    for(P=1,Holo){
          RAXA=rangerOffset(0,holoEntity(50+P):pos(),holoEntity(50+P):pos())
    if(!RAXA:hit()){
        holoPos(50+P,holoEntity(50+P):pos()+holoEntity(50+P):forward()*350)   
    }   
    if(RAXA:hit()){ PA=propSpawn("models/props_phx/ww2bomb.mdl",holoEntity(50+P):pos(),1) PA:propBreak()}
    if(changed(PA)&PA){holoDelete(50+P)}
    }
  
}
}
timer("ops",150)
if(Shooting==0&clk("ops")){
      for(P=1,Holo){
             RAXA=rangerOffset(0,holoEntity(50+P):pos(),holoEntity(50+P):pos())
    if(!RAXA:hit()){
        holoPos(50+P,holoEntity(50+P):pos()+holoEntity(50+P):forward()*350)   
    }   
    if(RAXA:hit()){holoDelete(50+P)}
    }
     
}

############################
####pulse shooting####
if(Driver){
if(Driver:keyAttack1()){
 Shottu=1   
}
if(holoEntity(15)){
RAX=rangerOffset(0,holoEntity(26):pos(),holoEntity(26):pos())}
if(changed(Driver:keyAttack1())&Driver:keyAttack1()) {holoEntity(26):setTrails(30,0,5,"trails/electric",vec(255,255,255),255)  soundPlay(3,200,"ambient/energy/force_field_loop1.wav")}
if(Shottu==1){
MORE=MORE+0.005
holoScale(26,vec(MORE,MORE,MORE))
if(MORE>1){MORE=1 MovingP=1}
}
if(changed(MORE==1)&MORE==1){MovingP=1 soundPlay(4,2,"ambient/energy/whiteflash.wav") soundStop(3,1)}

if(MovingP==1){
     HoloP++
    holoUnparent(26)
    if(!RAX:hit()){
    holoPos(26,holoEntity(26):pos()+holoEntity(26):forward()*50)
}
if(changed(RAX:hit())&RAX:hit()){
P=propSpawn("models/props_phx/cannonball.mdl",holoEntity(26):pos(),1)
P:propBreak()  
}
}

if(changed(P)&P){
    HoloP=0
    MORE=0.01
    Shottu=0
    MovingP=0
 holoDelete(26)
}
if(!holoEntity(26)){      holoCreate(26,holoEntity(15):toWorld(vec(0,0,27)),vec(-0.3,-0.3,-0.3),holoEntity(15):toWorld(ang(-90,0,0)),vec(0,0,0),"icosphere3")
}
    holoParent(26,15)}
holoAlpha(13,10)
#######################
if(Driver){
W=Driver:keyForward()
A=Driver:keyLeft()
S=Driver:keyBack()
D=Driver:keyRight()
Space=Driver:keyJump()
E:applyForce(E:forward()*E:mass()*150*(W-S))
E:applyAngForce(ang(0,1,0)*E:mass()*150*(A-D))

if(W){
XY=XY*1+5
X=cos(XY)
Y=sin(XY)

holoAng(1,E:toWorld(ang(X*25,0,0)))
    holoAng(2,E:toWorld(ang(X*-25,0,0)))
    
    holoAng(3,E:toWorld(ang(Y*-25,0,0)))
    holoAng(4,E:toWorld(ang(Y*25,0,0)))
}
if(S){
XY=XY*1+-5
X=cos(XY)
Y=sin(XY)

holoAng(1,E:toWorld(ang(X*25,0,0)))
    holoAng(2,E:toWorld(ang(X*-25,0,0)))
    
    holoAng(3,E:toWorld(ang(Y*-25,0,0)))
    holoAng(4,E:toWorld(ang(Y*25,0,0)))
}
}
if(!Ran:hit()){
E:applyForce(E:up()*E:mass()*-150)    
}
Seat:setAlpha(10)
Seat:setPos(holoEntity(11):toWorld(vec(0,0,0)))
Seat:setAng(holoEntity(11):toWorld(ang(0,90,180)))
if(changed(Space)&Space&Ran:hit()){JumpAnim=1  E:setAng(E:toWorld(ang(20,0,0))) soundPlay(1,99,"ambient/machines/thumper_hit.wav")}
if(JumpAnim==1){
    E:applyForce(E:up()*E:mass()-40)
    
    XY=XY*1+-3
X=cos(XY)
Y=sin(XY)

holoAng(1,E:toWorld(ang(X*15,0,0)))
    holoAng(2,E:toWorld(ang(X*15,0,0)))
    
    holoAng(3,E:toWorld(ang(Y*15,0,0)))
    holoAng(4,E:toWorld(ang(Y*15,0,0)))
    E:applyForce(E:up()*E:mass()*3000)
     E:applyForce(E:forward()*E:mass()*3000)
    EAS++
}
if(EAS>20){
 JumpAnim=0
EAS=0   
}
if(changed(EAS>19)&EAS>=19){
 Jumpp=1
}
if(Jumpp==1){
    HH++
       E:applyForce(E:up()*E:mass()*-3000)
     E:applyForce(E:forward()*E:mass()*3000)
    if(HH>15){ 
        soundStop(1,1)
     Jumpp=0
    HH=0
    }
}
if(changed(Ran:hit())&Ran:hit()&changed(Space)&Space&JumpAnim==0){
    E:setPos(E:toWorld(vec(0,0,15)))
}
RAX=rangerOffset(5,holoEntity(5):pos(),holoEntity(5):up()*-5)
if(changed(RAX:hit())&RAX:hit()){soundPlay(2,0.5,"ambient/machines/spinup.wav") }
RAX2=rangerOffset(6,holoEntity(6):pos(),holoEntity(5):up()*-5)
if(changed(RAX2:hit())&RAX2:hit()){soundPlay(3,0.5,"ambient/machines/spinup.wav") }
if(Driver:name()!=owner():name()){Seat:killPod()}
