@name Team Fortress 0.1 By MZombie
@inputs EGP:wirelink
@persist [Red Blue]:array [RedFlag BlueFlag]:entity [ RedOnUse BlueOnUse RedPoints BluePoints]:number
runOnTick(1)
runOnChat(1)

O = owner()
E = entity()
Ls = O:lastSaid()

Last = Ls:explode(" ")
if(first()){

    EGP:egpClear()
}

if(chatClk(O)&Last:string(1)=="!addred"){Temp = findPlayerByName(Last:string(2)) if(Temp:isPlayer()){Red:pushEntity(Temp)}}
if(chatClk(O)&Last:string(1)=="!addblue"){Temp = findPlayerByName(Last:string(2)) if(Temp:isPlayer()){Blue:pushEntity(Temp)}}

if(chatClk(O)&Ls=="!redpos"){
    hideChat(1)
    printColor(vec(255,0,0),"Red Flag Has Spawned")
    
    holoCreate(1,O:pos()+vec(0,0,50),vec(0.5,0.5,8),ang(0,0,0),vec(75),"hq_cylinder")
    holoCreate(2,O:pos(),vec(0.5,2,2),ang(90,0,0),vec(255,0,0),"hq_sphere")
    holoCreate(3,O:pos()+vec(0,13,100),vec(0.5,2,3),ang(0,0,90),vec(255,0,0),"hq_cube")
    holoParent(3,1)
    RedFlag = holoEntity(1)
}

if(chatClk(O)&Ls=="!bluepos"){
    hideChat(1)
    printColor(vec(0,0,255),"Blue Flag Has Spawned")
    
    holoCreate(4,O:pos()+vec(0,0,50),vec(0.5,0.5,8),ang(0,0,0),vec(75),"hq_cylinder")
    holoCreate(5,O:pos(),vec(0.5,2,2),ang(90,0,0),vec(0,0,255),"hq_sphere")
    holoCreate(6,O:pos()+vec(0,13,100),vec(0.5,2,3),ang(0,0,90),vec(0,0,255),"hq_cube")
    holoParent(6,4)
    BlueFlag = holoEntity(4)
}


EGP:egpLine(1,vec2(250,0),vec2(250,510))
EGP:egpColor(1,vec(255,0,0))

EGP:egpText(2,"Red Team",vec2(85,40))
EGP:egpColor(2,vec(255,0,0))

EGP:egpText(3,"Blue Team",vec2(345,40))
EGP:egpColor(3,vec(0,0,255))

EGP:egpText(4,"Blue Points: "+BluePoints,vec2(275,480))
EGP:egpColor(4,vec(0,0,255))

EGP:egpText(5,"Red Points: "+RedPoints,vec2(10,480))
EGP:egpColor(5,vec(255,0,0))

if(changed(Red:count())){
for(R=1,Red:count()){
    EGP:egpText(R+20,Red[R,entity]:name(),vec2(10,75+R*50))   
}
}

if(changed(Blue:count())){
for(B=1,Blue:count()){
    EGP:egpText(B+30,Blue[B,entity]:name(),vec2(280,75+B*50))      
}
}

for(RF=1,Red:count()){
     if(Red[RF,entity]:pos():distance(BlueFlag:pos())<=100){holoPos(4,Red[RF,entity]:attachmentPos("chest")) RedOnUse = 1}
}
if(RedOnUse & BlueFlag:pos():distance(RedFlag:pos())<=100){
    RedOnUse = 0
    holoPos(4,holoEntity(5):pos()+vec(0,0,50))
    RedPoints = RedPoints + 1
}


for(BF=1,Blue:count()){
     if(Blue[BF,entity]:pos():distance(RedFlag:pos())<=100){holoPos(1,Blue[BF,entity]:attachmentPos("chest")) BlueOnUse = 1}
}
if(BlueOnUse & RedFlag:pos():distance(BlueFlag:pos())<=100){
    BlueOnUse = 0
    holoPos(1,holoEntity(2):pos()+vec(0,0,50))
    BluePoints = BluePoints + 1
}
