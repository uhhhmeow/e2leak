@name EGP Bombgame
@inputs EGP:wirelink User:entity
@persist TopLeft:array BottomRight:array Bomb:array Lines Nr

function void start(){
    EGP:egpClear()
    
    Lines=8
    
    
    
    for(I=0,Lines)
    {
    Bomb[I,number]=randint(Lines*Lines)
    }
    
    for(I=0,Lines*Lines)
    {
    EGP:egpBox(I+1, vec2((I%Lines)/Lines*512+(512/(Lines*2)),((I-I%Lines))/(Lines*Lines)*512+(512/(Lines*2))), vec2(1/(Lines+1)*512,1/(Lines+1)*512))
    EGP:egpColor(I+1,vec(150,150,150))
    TopLeft[I+1,vector2]=EGP:egpPos(I+1) - EGP:egpSize(I+1)/2
    BottomRight[I+1,vector2] = EGP:egpPos(I+1) + EGP:egpSize(I+1)/2
    I++  
    }

    EGP:egpCircleOutline(200, vec2(0,0), vec2(10,10))
    EGP:egpColor(200, vec(255,0,0))
    EGP:egpParentToCursor(200)
}

if(first())
{
start()
}

if(~User & User)
{

    local Cursor = EGP:egpCursor(User)

    for(I=0,Lines*Lines)
    {
    if(inrange(Cursor, TopLeft[I+1,vector2], BottomRight[I+1,vector2]))
    {
                Nr=0
        for(H=0,Bomb:count())
        {
        
        if(I+1==Bomb[H,number])
        {
        EGP:egpColor(I+1, vec(255,0,0))
        exit()    
        }
        
        else
        {
        
        EGP:egpColor(I+1, vec(0,255,0))
        
        if((Bomb[H,number]-1)%Lines==0)
        {
        for(J=0,1)
        {
        for(K=0,2)
        {
        if((I%Lines)+J + (((I-I%Lines))/Lines-1+K)*Lines==Bomb[H,number]){Nr++}    
        } 
        }
        }
            
        elseif(Bomb[H,number]%Lines==0)
        {
        for(J=1,2)
        {
        for(K=0,2)
        {
        if((I%Lines)+J + (((I-I%Lines))/Lines-1+K)*Lines==Bomb[H,number]){Nr++}    
        } 
        }
        }
        
        else
        {
        for(J=0,2)
        {
        for(K=0,2)
        {
        if((I%Lines)+J + (((I-I%Lines))/Lines-1+K)*Lines==Bomb[H,number]){Nr++}    
        } 
        }
        }
        
        EGP:egpText(I+101,""+Nr,TopLeft[I+1,vector2]+vec2(1/(Lines+1)*512/2,1/(Lines+1)*512/2))
        }
        }
    }
    }
}
