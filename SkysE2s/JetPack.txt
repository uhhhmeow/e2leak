@name JetPack
@inputs 
@outputs 
@persist B1 FlyUp
@trigger 
E = entity():isWeldedTo()
Driver = E:driver()
if(E){
W = Driver:keyForward()
A = Driver:keyLeft()
S = Driver:keyBack()
D = Driver:keyRight()
Space = Driver:keyJump()
Shift = Driver:keySprint()
Alt = Driver:keyWalk()
Mouse1 = Driver:keyAttack1()
Mouse2 = Driver:keyAttack2()
}
runOnTick(1)

if(first()|duped()|dupefinished()){
function vector entity:applyf(Vector1:vector){
    This:applyForce(Vector1)
}    
function angle entity:applya(Angle1:angle){
This:applyAngForce(Angle1)
}
function vector entity:applyt(Angle2:vector){
This:applyTorque(Angle2)
}
    
        function entity c(Index:number,Posi:vector,Scale:vector,Angle:angle,Colo:vector,Model:string,Parent:entity,Alpha:number){
 holoCreate(Index) holoPos(Index,Posi) holoScale(Index,Scale) holoAng(Index,Angle) holoColor(Index,Colo) holoModel(Index,Model) 
holoParent(Index,Parent) holoAlpha(Index,Alpha)
return holoEntity(Index)  
}
function fly(Entity:entity,Pitch:number,Yaw:number,Roll:number,Power:number,UP:number){
local Ang = quat(ang((W-S)*Pitch,Entity:angles():yaw()+(A-D)*Yaw,(D-A)*Roll)) / quat(Entity)
local Ang2 = Entity:toLocal(rotationVector(Ang)+Entity:pos())
Entity:applyt((Power *Ang2-Entity:angVelVector())*Entity:inertia()*20)
local PF = E:angles():pitch()
local RF = E:angles():roll()

Entity:applyf((E:right()*E:mass()*PF+E:forward()*E:mass()*RF+E:up()*E:mass()*UP*(Space-Shift))-Entity:vel()*E:mass()*0.03)
}
c(1,E:toWorld(vec(-10,0,50)),vec(1,3,1),E:toWorld(ang()),vec(50),"hq_rcube",E,255)
c(2,E:toWorld(vec(-10,0,35)),vec(0.7,1.8,2),E:toWorld(ang()),vec(20),"hq_rcube",E,255)

c(3,E:toWorld(vec(-10,13,35)),vec(0.7,0.7,2),E:toWorld(ang()),vec(90),"hqcylinder",E,255)
c(4,E:toWorld(vec(-10,-13,35)),vec(0.7,0.7,2),E:toWorld(ang()),vec(90),"hqcylinder",E,255)

c(5,E:toWorld(vec(-10,13,25)),vec(0.8,0.8,0.7),E:toWorld(ang()),vec(0),"hqcone",E,255)
c(6,E:toWorld(vec(-10,-13,25)),vec(0.8,0.8,0.7),E:toWorld(ang()),vec(0),"hqcone",E,255)

c(7,E:toWorld(vec(-10,0,60)),vec(0.7,1.3,1),E:toWorld(ang()),vec(50),"hq_rcube",E,255)

c(8,E:toWorld(vec(-10,3,40)),vec(0.8,0.8,1.7),E:toWorld(ang()),vec(90),"hqcylinder",E,255)
c(9,E:toWorld(vec(-10,-3,40)),vec(0.8,0.8,1.7),E:toWorld(ang()),vec(90),"hqcylinder",E,255)

c(10,E:toWorld(vec(-10,0,25)),vec(0.3,1.4,2),E:toWorld(ang()),vec(20),"hq_rcube",E,255)
c(11,E:toWorld(vec(-13.1,0,30)),vec(0.3,1,0.3),E:toWorld(ang()),vec(40),"hq_rcube",E,255)

}
if(changed(E)){
    E:propFreeze(1)
    E:setAng(ang())
}
if(W|S){
    if(B1 < 25){
B1 = B1 * 1 + 1
}
}

if(!W&!S){
if(B1 > 1){
B1 = B1 * 1 - 1
}
}
if(Space|Shift){
    if(FlyUp < 40){
    FlyUp = FlyUp * 1 + 1
}
}
if(!Space&!Shift){
    if(FlyUp > 2){
 FlyUp = FlyUp * 1 - 1   
}
}

if(Driver){
 P  =  8   
}else{
P = 100
}
fly(E,B1,10,10,P,FlyUp)

if(changed(Driver)&Driver){
holoEntity(5):setTrails(10,0,2,"trails/smoke",vec(0),255)
holoEntity(6):setTrails(10,0,2,"trails/smoke",vec(0),255)
}
if(changed(!Driver)&!Driver){
holoEntity(5):removeTrails()
holoEntity(6):removeTrails()
}
