@name 
@inputs 
@outputs Bx:vector
@persist [O E]:entity Idx Cid:array
@trigger 
@model 

if( dupefinished() ){ reset() }

if( first() )
{
    #runOnTick(1)
    #runOnChat(1)
    runOnLast(1)
    
    O = owner()
    E = entity():isWeldedTo()
    
    function number getID( N )
    {
        Idx += N
        return Idx
    }
    function number clipIt( N:number, [V1 V2]:vector )
    {
        Cid[N,number] = Cid[N,number] + 1
        holoClipEnabled( N, Cid[N,number], 1 )
        holoClip( N, Cid[N,number], V1, V2, 0 )
        return N
    }
    #clipIt( getID(0), vec(), vec(0,0,1) )
    #function number 
    
    Bx = E:aabbSize()
    
    #holoCreate( getID(1), E:toWorld(vec(-Bx[1]/2,0,Bx[3]/2)+vec(0,0,3):rotate(ang(22.5,0,0))), vec(12,12,12) / 12, E:toWorld(ang(67.5,0,0)), vec(255) )
    #holoModel( getID(0), "models/sprops/misc/fittings/corner_45_3.mdl" )
    #holoParent( getID(0), E )
    
    Scale = Bx[2]/9
    
    holoCreate( getID(1), E:toWorld(Scale*(vec(0,0,3.75)+vec(9.75,0,-4.5):rotate(ang(22.5,0,0)))), vec(0.2) / 12, E:toWorld(ang(22.5,0,0)), vec(255) )
    holoParent( getID(0), E )
    
    holoCreate( getID(1), E:toWorld(Scale*(vec(0,0,3.75)+vec(-9.75,0,-4.5):rotate(ang(-22.5,0,0)))), vec(0.2) / 12, E:toWorld(ang(-22.5,0,0)), vec(255) )
    holoParent( getID(0), E )
}

if( last() )
{
    holoDeleteAll()
}
