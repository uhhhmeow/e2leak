@name 3D Holo SProps Thing
@inputs 
@outputs Info:table Len Length String:string Width Min Pos:vector Angle:angle Pitch Distance Len_Abs Below
@persist [H B O E]:entity Idx [Undo Cid]:array Snap Side
@model 

if( dupefinished() ){ reset() }

if( first() )
{
    runOnTick(1)
    runOnChat(1)
    runOnLast(1)
    propSpawnEffect(0)
    
    O = findPlayerByName("Erza")
    E = entity():isWeldedTo() ?: entity()
    
    function number getID( N )
    {
        Idx+=N
        return Idx
    }
    function number clipIt( N:number, [V1 V2]:vector )
    {
        Cid[N,number] = Cid[N,number] + 1
        holoClipEnabled( N, Cid[N,number], 1 )
        holoClip( N, Cid[N,number], V1, V2, 0 )
    }
    #clipIt( getID(0), vec(), vec(0,0,1) )
    function setLen(S:string)
    {
        Length = S:toNumber()
    }
    function setWidth(S:string)
    {
        Width = S:toNumber()
    }
    function string entity:getFolder()
    {
        local Array = This:model():explode("/")
        return Array[4,string]
    }
    function string belowMin( N )
    {
        return select( N, "size_1",
                        "size_1_5",
                        "size_2",
                        "size_2_5",
                        "size_3",
                        "size_3_5",
                        "size_4",
                        "size_4_5",
                        "size_5",
                        "size_54",
                        "size_60",
                        "size_6",
                        "size_7",
                        "size_8",
                        "size_9",
                        "size_10",
                        "size_66",
                        "size_72",
                        "size_78",
                        "size_84",
                        "size_90"
                        )
    }
    function number getLen( N )
    {
        return select( N, 3, 6, 12, 18, 24, 30, 36, 42, 48, 54, 60, 66, 72, 78, 84, 90, 96, 108, 120, 132, 144, 192, 240, 288, 336, 384, 432, 480 )
    }
    function vector linePlane( Ent:entity, Ply:entity, A:number )
    {
        Normal = A!=2 ? Ent:up() : Ent:right()
        return Ent:toLocal( Ply:shootPos() + ( Normal:dot( Ent:pos() - Ply:shootPos() ) / Normal:dot( Ply:eye() ) ) * Ply:eye() )
    }
    function vector getPos( [N1 N2]:number, A:angle, N3 )
    {
        local V = N3 ? vec(N1/2,0,0) + vec(getLen(N2)/2,0,0):rotate(ang(A[3],0,0)) : vec(N1/2,0,0) + vec(getLen(N2)/2,0,0):rotate(A)
        return B:toWorld(V)
    }
    
    #Info = table()
    #Info["size_1_5 length",array] = array( 3, 6, 12, 18, 24, 30, 36, 42, 48, 54, 60, 66, 72, 78, 84, 90, 96, 108, 120, 132, 144, 192, 240, 288, 336, 384, 432, 480 )
    
    String = "models/sprops/rectangles/"+E:getFolder()+"/rect_"+Width+"x"+getLen(Len)+"x3.mdl"
    
    H = holoCreate( getID(1), E:toWorld(vec(0,0,0)), vec(12,12,12) / 12, E:toWorld(ang(0,0,0)), vec()+255 )
    holoModel( getID(0), String )
    holoAlpha( getID(0), 150 )
    holoParent( getID(0), E )
    
    holoCreate( getID(1), E:toWorld(vec(0,0,0)), vec(1) / 12, E:toWorld(ang(0,0,0)), vec(0,255,0) )
    holoModel( getID(0), "sphere" )
    holoAlpha( getID(0), 150 )
    holoMaterial( getID(0), "models/wireframe" )
    holoParent( getID(0), E )
    
    B = E
    
    setLen( B:model():explode("/")[5,string]:explode("x")[2,string] )
    setWidth( B:model():explode("/")[5,string]:explode("x")[1,string]:explode("_")[2,string] )
    
    Min = floor( Width / 6 ) + 1
    Len = floor( Length / 6 ) + 1
    
    Snap = 11.25
    
    Side = 1
}

Pos = linePlane( B, O, 2 )

Distance = vec(Side*Length/2,0,0):distance(Pos)
Len = max( Min, floor( Distance / 6 ) + 1 )
Len_Abs = floor(Distance/6)+1
Below = Len_Abs < Min

Pitch = acos( vec( 1, 0, 0 ):dot( ( Pos - vec( Side*Length / 2, 0, 0 ) ):normalized() ) )
Angle = ang( Pitch * sign( -Pos[3] ), 0, 0 )
if( Below )
{
    Angle = ang(0,90,0) + shiftL(Angle)
}

if( O:keySprint() )
{
    Angle = floor( Angle / Snap ) * Snap
}

if( changed(Angle) | changed(String) )
{
    print( _HUD_PRINTCENTER, "Pitch: " + ( Below ? Angle[3] : Angle[1] ) + "\nModel: " + String )
}

holoPos( 2, B:toWorld(Pos) )

holoPos( 1, getPos(Side*Length,Len_Abs,Angle,Below) )
holoAng( 1, B:toWorld( Angle ) )

Spawn = O:keyUse() & O:keyAttack1() > 0

if( changed(Len_Abs) )
{
    #String = "models/sprops/rectangles/"+B:getFolder()+"/rect_"+Width+"x"+getLen(Len)+"x3.mdl"
    String = "models/sprops/rectangles/"+( Below ? belowMin(Len_Abs) : B:getFolder() )+"/rect_"+( Below ? getLen(Len_Abs) : Width )+"x"+( Below ? Width : getLen(Len_Abs) )+"x3.mdl"
    holoModel( 1, String )
}

if( changed(Spawn) & Spawn )
{
    B = propSpawn( H:model(), getPos(Side*Length,Len_Abs,Angle,Below), B:toWorld(Angle), 1 )
    
    setLen( B:model():explode("/")[5,string]:explode("x")[2,string] )
    setWidth( B:model():explode("/")[5,string]:explode("x")[1,string]:explode("_")[2,string] )
    
    Undo:pushEntity( B )
}

Swap = O:keyReload()

if( changed(Swap) & Swap )
{
    Side = -Side
}

if( chatClk(O) )
{
    local Ls = O:lastSaid():lower()
    local Lsx = Ls:explode(" ")
    
    switch( Lsx[1,string] )
    {
        default,
        break
        
        case "set",
            B = O:aimEntity()
            hideChat(1)
        break
        
        case "done",
            hideChat(1)
            selfDestruct()
        break
        
        case "snap",
            Snap = Lsx[2,string]:toNumber()
            hideChat(1)
        break
    }
}

if( last() )
{
    holoDeleteAll()
}
