@name E2 Tool Localizer Ang-Pos
@inputs 
@outputs 
@persist [O E]:entity
@model 

if( duped() ){ reset() }

if(first())
{
    runOnChat(1)
    E = entity():isWeldedTo() ?: entity()
    O = owner()
}

if( chatClk(O) )
{
    Lsx = O:lastSaid():lower():explode(" ")
    Cmd = Lsx[1,string]
    if( Cmd=="!print" )
    {
        Ae = O:aimEntity()
        Pos = toString( round( E:toLocal(Ae:pos()) ) ):replace("[","("):replace("]",")")
        Ang = toString( round( E:toLocal(Ae:angles()) ) ):replace("[","("):replace("]",")")
        print("holoPos( #, E:toWorld(vec"+Pos+"))")
        print("holoAng( #, E:toWorld(vec"+Ang+"))")
    }
}
