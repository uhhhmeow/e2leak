@name Globe Tutorial
@persist Timer
interval(10)
Me=owner()
Chip=entity() ##Ok. Here is the Holo (parent). You can change it to follow you or, Follow the E2. Like (holoPos(1,Chip:pos()+vec(0,0,25)) or holoPos(1,Me:pos()+vec(0,0,25))##
Timer=Timer+1*1 ##Timer##
T=Timer ##Timer##

holoCreate(1) ## Create The Holo ##
holoModel(1,"sphere") ## Model ##
holoPos(1,Chip:pos()+vec(0,0,80)) ##This is the Holo position.##
holoAng(1,ang(T,T,T)) ## The Hologram Angles. Note : the T is the timer, you can remove that if
holoColor(1,vec(155,0,0)) ## Color ##
holoScale(1, vec(1,1,1)) ## Scale ##

## like this >D ###

holoCreate(2)
holoModel(2,"cube") ## Model ##
holoPos(2,Chip:pos()+vec(0,0,80)) ##This is the Holo position.##
holoAng(2,ang(T,T,T)) ## The Hologram Angles. Note : the T is the timer, you can remove that if
holoColor(2,vec(155,0,0)) ## Color ##
holoScale(2, vec(1,1,1)) ## Scale ##
