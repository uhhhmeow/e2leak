@persist T_HEAP:table

if( first() || dupefinished() ) {
    function string new() { local T_NULL = table( ), local S_UID = T_NULL:id( ):sub(10):toNumber(16):toString(), T_HEAP[ S_UID, table ] = T_NULL, return S_UID }
    function string new( T_TABLE:table ) { local S_UID = T_TABLE:id( ):sub(10):toNumber(16):toString(), T_HEAP[ S_UID, table ] = T_TABLE, return S_UID }
    function string table:new() { local S_UID = This:id( ):sub(10):toNumber(16):toString(), T_HEAP[ S_UID, table ] = This, return S_UID }
    function table string:get() { return T_HEAP[This, table] }
    function table get( S_STRING:string ) { return T_HEAP[S_STRING, table] }
    function string:clear() { This:get():clear() }
    function clear( S_STRING:string ) { S_STRING:get():clear() }
    function string:delete() { T_HEAP:remove( This ) }
    function delete( S_STRING:string ) { T_HEAP:remove( S_STRING ) }
}
