#[
    E2 Function Extension
    #include after directives
    Makes your life easier by making E2 more function-based
    
    Includes:
    
    // Output Functions
        echoSetName(string Name) - Sets echo name.
        echoSetColor(vector Color) - Sets echo name color.
        echo(array Args) - Prints colors with name.
    
    // setTimer Functions
        string ID = setInterval(string Function, number Interval) - Calls Function every Interval milliseconds.
            stopInterval(ID) - Kills an interval.
        string ID = setTimeout(string Function, number Interval) - Calls Function in Interval milliseconds.
            stopTimeout(ID) - Kills a timeout.
    
    // Globals
        // Color Vectors
            Red
            Green
            Blue
            Cyan
            Yellow
        // Entities
            Owner
            Entity
    
    // Chat Commands
        registerChatCommand(string Command,string Function,string Syntax,string Description) - Registers a chat command.
        deleteChatCommand(string Command) - Deletes a chat command.
        // Defaults
            .help - Lists commands, syntax, and descriptions
            .rawhelp - Lists command call stacks
        
    // Internal:
        table Ext_Func - Stores everything internally.
        timer 'Ticker' - Internal timer that checks against setTimer functions.
]#

@persist Ext_Func:table
@persist [Red Green Blue Cyan Yellow White]:vector 
@persist [Owner Entity]:entity

if(first()) {
    Ext_Func["Ticker",table]=table()
    Ext_Func["TimerStack",number]=1
    Ext_Func["TickerSpeed",number]=100
    Ext_Func["Chat",table]=table()
    
    Owner=owner()
    Entity=entity()
    
    Red=vec(255,0,0)
    Green=vec(0,255,0)
    Blue=vec(0,0,255)
    Cyan=vec(0,255,255)
    Yellow=vec(255,255,0)
    White=vec(255)
    
    function void echoSetName(Name:string) {
        Ext_Func["E2Name",string]=Name
    }
    
    function void echoSetColor(Color:vector) {
        Ext_Func["E2Color",vector]=Color
    }
    
    function void echo(Args:array) {
        Args:unshiftVector(vec(255))
        Args:unshiftString("["+Ext_Func["E2Name",string]+"] ")
        Args:unshiftVector(Ext_Func["E2Color",vector])
        printColor(Args)
    }
    
    function string setInterval(Function:string,Interval:number) {
        local ID=Ext_Func["TimerStack",number]+1
        Ext_Func["TimerStack",number]=ID
        Ext_Func["Ticker",table][ID,table]=table("Func"=Function,"ID"=ID,"Loop"=1,"Interval"=Interval)
        timer("EXT_"+ID,Interval)
        return toString(ID)
    }
    
    function string setTimeout(Function:string,Interval:number) {
        local ID=Ext_Func["TimerStack",number]+1
        Ext_Func["TimerStack",number]=ID
        Ext_Func["Ticker",table][ID,table]=table("Func"=Function,"ID"=ID,"Loop"=0,"Interval"=Interval)
        timer("EXT_"+ID,Interval)
        return toString(ID)
    }
    
    function void registerChatCommand(Name:string,Function:string,Syntax:string,Description:string) {
        Ext_Func["Chat",table][Name,table]=table("Name"=Name,"Func"=Function,"Syntax"=Syntax,"Desc"=Description,"Valid"=1)
    }
    
    function void deleteChatCommand(Name:string) {
        Ext_Func["Chat",table]:removeTable(Name)
    }
    
    function void cmd_Help(Args:array) {
        
    }
    
    function void cmd_RawHelp(Args:array) {
        
    }
    
    registerChatCommand("help","cmd_Help","help","Dumps data about commands.")
    registerChatCommand("rawhelp","cmd_RawHelp","rawhelp","Dumps command function calls.")
}

local Ext_ClickName=clkName()
if(Ext_ClickName:sub(1,4)=="EXT_") {
    local ID=Ext_ClickName:sub(5):toNumber()
    local Table=Ext_Func["Ticker",table][ID,table]
    Table["Func",string]()
    if(Table["Loop",number]) {timer(Ext_ClickName,Table["Interval",number])}
}

if (chatClk(Owner) & Owner:lastSaid():sub(1,1)==".") {
    hideChat(1)
    local Args=Owner:lastSaid():sub(2):explode(" ")
    local Command=Args:shiftString()
    local Table=Ext_Func["Chat",table][Command,table]
    if(Table["Valid",number]) {
        Table["Func",string](Args)
    } else {
        echo(array("No such command: ",Red,"'"+Command+"'",White,".  Use ",Green,"'.help' ",White," for help."))
    }
}
