function string checkAddress(Str:string, Gate:entity) {
    A = Gate:stargateAddressList()
    for(I = 1, A:count()) {
        S = A[I, string]:explode(" ")[1, string]:lower()
        Str = Str:lower()
        
        if(S:find(Str)) { return S }
    }
    
    return ""
}
function table getRingAddressList() {
    findInSphere(owner():pos(), 10000000000000000000000000000)
    A = findToArray()
    Table = table()
    for(I = 1, A:count()) {
        if(A[I,entity]:type():find("ring_base")) {
            Table:pushString(A[I, entity]:stargateRingAddress())
        }
    }
    
    return Table
}
function string checkRingAddress(Str:string) {
    T = getRingAddressList()
    for(I = 1, T:count()) {
        S = T[I, string]:lower()
        Str = Str:lower()
        
        if(S:find(Str)) { return S }
    }
    
    return ""
}
function entity getGateFromAddress(Str:string) {
    findInSphere(owner():pos(), 10000000000000000000000000000)
    A = findToArray()
    Table = table()
    for(I = 1, A:count()) {
        if(A[I,entity]:type():find("stargate")) {
            S = A[I, entity]:stargateAddress():lower()
            Str = Str:lower()
            
            if(S:find(Str)) {
                return A[I, entity]
            }
        }
    }
}
function entity getGateFromOwner(Str:string) {
    findInSphere(owner():pos(), 10000000000000000000000000000)
    A = findToArray()
    Table = table()
    for(I = 1, A:count()) {
        if(A[I,entity]:type():find("stargate")) {
            S = A[I, entity]:owner():name():lower()
            Str = Str:lower()
            
            if(S:find(Str)) {
                return A[I, entity]
            }
        }
    }
}
