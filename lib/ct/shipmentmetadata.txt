@persist ShipmentMetaData:table

if (first()) {
    ShipmentMetaData=table(
        # GD shipments
        "fas2_ragingbull"=table(
            "Type"="fas2_ragingbull",
            "Name"="Raging Bull",
            "Model"="models/weapons/w_357.mdl",
            "Price"=3000/5,
            "SpawnStock"=5
        ),
        "fas2_p226"=table(
            "Type"="fas2_p226",
            "Name"="P226",
            "Model"="models/weapons/w_pist_p228.mdl",
            "Price"=2500/5,
            "SpawnStock"=5
        ),
        "fas2_ots33"=table(
            "Type"="fas2_ots33",
            "Name"="OTs-33",
            "Model"="models/weapons/world/pistols/ots33.mdl",
            "Price"=2500/5,
            "SpawnStock"=5
        ),
        "fas2_m1911"=table(
            "Type"="fas2_m1911",
            "Name"="M1911",
            "Model"="models/weapons/w_pistol.mdl",
            "Price"=2000/5,
            "SpawnStock"=5
        ),
        "fas2_deagle"=table(
            "Type"="fas2_deagle",
            "Name"="Desert Eagle",
            "Model"="models/weapons/w_pist_deagle.mdl",
            "Price"=3000/5,
            "SpawnStock"=5
        ),
        "fas2_glock20"=table(
            "Type"="fas2_glock20",
            "Name"="Glock 20",
            "Model"="models/weapons/w_pist_glock18.mdl",
            "Price"=1500/5,
            "SpawnStock"=5
        ),
        "fas2_m3s90"=table(
            "Type"="fas2_m3s90",
            "Name"="M3 Super 90",
            "Model"="models/weapons/w_shot_m3super90.mdl",
            "Price"=4500/5,
            "SpawnStock"=5
        ),
        
        # BMD Shipments
        "fas2_m24"=table(
            "Type"="fas2_m24",
            "Name"="M24",
            "Model"="models/weapons/w_snip_scout.mdl",
            "Price"=9000/5,
            "SpawnStock"=5
        ),
        "fas2_uzi"=table(
            "Type"="fas2_uzi",
            "Name"="IMI Uzi",
            "Model"="models/weapons/w_smg_mac10.mdl",
            "Price"=2500/5,
            "SpawnStock"=5
        ),
        "fas2_ak47"=table(
            "Type"="fas2_ak47",
            "Name"="AK-47",
            "Model"="models/weapons/w_rif_ak47.mdl",
            "Price"=5000/5,
            "SpawnStock"=5
        ),
        "fas2_g3"=table(
            "Type"="fas2_g3",
            "Name"="G3A3",
            "Model"="models/weapons/w_rif_galil.mdl",
            "Price"=7500/5,
            "SpawnStock"=5
        ),
        "fas2_mp5k"=table(
            "Type"="fas2_mp5k",
            "Name"="MP5K",
            "Model"="models/weapons/w_smg_mp5.mdl",
            "Price"=7500/5,
            "SpawnStock"=5
        ),
        "fas2_sks"=table(
            "Type"="fas2_sks",
            "Name"="SKS",
            "Model"="models/weapons/w_rif_ak47.mdl",
            "Price"=8000/5,
            "SpawnStock"=5
        ),
        "fas2_m14"=table(
            "Type"="fas2_m14",
            "Name"="M14",
            "Model"="models/weapons/w_snip_sg550.mdl",
            "Price"=10000/5,
            "SpawnStock"=5
        ),
        "lockpick"=table(
            "Type"="lockpick",
            "Name"="Lock Pick",
            "Model"="models/weapons/w_crowbar.mdl",
            "Price"=3000/10,
            "SpawnStock"=10
        ),
        
        # stupid
        "weapon_slam"=table(
            "Type"="weapon_slam",
            "Name"="SLAM",
            "Model"="models/weapons/w_slam.mdl",
            "Price"=100000,
            "SpawnStock"=1
        )
    )
}
    
