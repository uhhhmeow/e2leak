@name holo_minigun
@inputs 
@outputs F 
@persist Obrot A:angle T [E E2]:entity
@persist [Ranger Ranger2]:ranger 
#@model models/hunter/plates/plate4x4.mdl




####################################
#-Holographic Minigun-             #
#By Pajro                          #
#Quaternion applyTorque by Fizyk   #
#Laser Pointer thingy by Grinchfox #
####################################
# To make minigun shoot, create a turret, wire it to the E2 "F"
# and hover it above the chip.

#if(duped()) { selfDestructAll() }


runOnTick(1)

Ranger = rangerOffset(100,entity():pos(),vec(0,0,1))
if(Ranger:distance() < 100 
& !E2 
& Ranger:entity():owner() == owner()) { E2 = Ranger:entity() } 
if(E2) { E = E2 E2:setAlpha(0)}

Ranger2 = rangerOffset(999999,E:pos(),E:forward())


#E = entity()
A = holoEntity(666):angles()



TarQ = quat(A) #Calculate quaternion for target orientation
CurQ = quat(E) #Calculate current orientation quaternion

#TarQ/CurQ is a quaternion representing the rotation that will rotate the object from current orientation to the target one.
Q = TarQ/CurQ

#applyTorque() works on intrinsic vectors! Get the rotation vector and transform it.
V = E:toLocal(rotationVector(Q)+E:pos())
#Alternatively, can use "V = transpose(matrix(E))*rotationVector(Q)"

#Apply torque. angVelVector() works like a delta term.
#Factors 150 and 12 can be adjusted to achieve best effect.
E:applyTorque((150*V - 12*E:angVelVector())*E:inertia())  
if(perf()){
E:applyForce(((holoEntity(666):pos()+vec(0,0,-2)-E:pos())*25-E:vel()/2)*E:mass())
}
if(owner():keyUse() & E & holoEntity(1):getAlpha()==255) { Obrot += 100 F=1}else{Obrot += 1 F=0}


if(first()) { 
    holoCreate(1,entity():pos(),vec(1,1,1),ang(),vec(255,255,255))
    holoModel(1, "hqcylinder")

holoAng(1,owner():attachmentAng("anim_attachment_RH"))
holoPos(1,owner():attachmentPos("anim_attachment_RH"))
holoParentAttachment(1,owner(),"anim_attachment_RH")
holoScale(1,vec(0.7,0.7,1)) 
holoPos(1,holoEntity(1):toWorld(vec(17,-4.3,5.3)))
holoAng(1,holoEntity(1):toWorld(ang(90,-10,0)))
holoMaterial(1, "models/props_canal/metalwall005b" )
#holoAlpha(1, 255)

holoCreate(2)
holoScale(2, vec(0.6,0.6,0.5))
holoModel(2, "cylinder")
holoPos(2, holoEntity(1):toWorld(vec(0,0,4)))
holoAng(2, holoEntity(1):toWorld(ang(0,0,0)))
holoParent(2, holoEntity(1))
holoMaterial(2, "models/weapons/v_stunbaton/w_shaft01a" )

holoCreate(3)
holoScale(3, vec(0.7,0.7,0.2))
holoModel(3, "hqcylinder")
holoPos(3, holoEntity(1):toWorld(vec(0,0,8)))
holoAng(3, holoEntity(1):toWorld(ang(0,0,0)))
holoParent(3, holoEntity(1))
holoMaterial(3, "models/props_canal/metalwall005b" )

#BAZA RUREK
holoCreate(4)
holoScale(4, vec(0.65,0.65,0.2))
holoModel(4, "cylinder")
holoPos(4, holoEntity(1):toWorld(vec(0,0,10)))
holoAng(4, holoEntity(1):toWorld(ang(0,0,0)))
holoParent(4, holoEntity(1))
holoMaterial(4, "models/props_pipes/pipeset_metal02" )

#RURKI
holoCreate(5)
holoScale(5, vec(0.15,0.15,2))
holoModel(5, "cylinder")
holoPos(5, holoEntity(4):toWorld(vec(2.6,0,9)))
holoAng(5, holoEntity(4):toWorld(ang(0,0,0)))
holoParent(5, holoEntity(4))
holoMaterial(5, "models/props_pipes/pipeset_metal02" )

holoCreate(6)
holoScale(6, vec(0.15,0.15,2))
holoModel(6, "cylinder")
holoPos(6, holoEntity(4):toWorld(vec(-2.6,0,9)))
holoAng(6, holoEntity(4):toWorld(ang(0,0,0)))
holoParent(6, holoEntity(4))
holoMaterial(6, "models/props_pipes/pipeset_metal02" )

holoCreate(7)
holoScale(7, vec(0.15,0.15,2))
holoModel(7, "cylinder")
holoPos(7, holoEntity(4):toWorld(vec(0,-2.6,9)))
holoAng(7, holoEntity(4):toWorld(ang(0,0,0)))
holoParent(7, holoEntity(4))
holoMaterial(7, "models/props_pipes/pipeset_metal02" )

holoCreate(8)
holoScale(8, vec(0.15,0.15,2))
holoModel(8, "cylinder")
holoPos(8, holoEntity(4):toWorld(vec(0,2.6,9)))
holoAng(8, holoEntity(4):toWorld(ang(0,0,0)))
holoParent(8, holoEntity(4))
holoMaterial(8, "models/props_pipes/pipeset_metal02" )

#rurki 2
holoCreate(9)
holoScale(9, vec(0.15,0.15,2))
holoModel(9, "cylinder")
holoPos(9, holoEntity(4):toWorld(vec(1.9,1.9,9)))
holoAng(9, holoEntity(4):toWorld(ang(0,0,0)))
holoParent(9, holoEntity(4))
holoMaterial(9, "models/props_pipes/pipeset_metal02" )

holoCreate(10)
holoScale(10, vec(0.15,0.15,2))
holoModel(10, "cylinder")
holoPos(10, holoEntity(4):toWorld(vec(-1.9,-1.9,9)))
holoAng(10, holoEntity(4):toWorld(ang(0,0,0)))
holoParent(10, holoEntity(4))
holoMaterial(10, "models/props_pipes/pipeset_metal02" )

holoCreate(11)
holoScale(11, vec(0.15,0.15,2))
holoModel(11, "cylinder")
holoPos(11, holoEntity(4):toWorld(vec(-1.9,1.9,9)))
holoAng(11, holoEntity(4):toWorld(ang(0,0,0)))
holoParent(11, holoEntity(4))
holoMaterial(11, "models/props_pipes/pipeset_metal02" )

holoCreate(12)
holoScale(12, vec(0.15,0.15,2))
holoModel(12, "cylinder")
holoPos(12, holoEntity(4):toWorld(vec(1.9,-1.9,9)))
holoAng(12, holoEntity(4):toWorld(ang(0,0,0)))
holoParent(12, holoEntity(4))
holoMaterial(12, "models/props_pipes/pipeset_metal02" )

#dziurki
holoCreate(512)
holoScale(512, vec(0.12,0.12,2.032))
holoModel(512, "cylinder")
holoPos(512, holoEntity(4):toWorld(vec(2.6,0,8.88)))
holoAng(512, holoEntity(4):toWorld(ang(0,0,0)))
holoParent(512, holoEntity(4))
holoMaterial(512, "debug/debugdrawflat" )
holoColor(512, vec(0,0,0))

holoCreate(612)
holoScale(612, vec(0.12,0.12,2.032))
holoModel(612, "cylinder")
holoPos(612, holoEntity(4):toWorld(vec(-2.6,0,8.88)))
holoAng(612, holoEntity(4):toWorld(ang(0,0,0)))
holoParent(612, holoEntity(4))
holoMaterial(612, "debug/debugdrawflat" )
holoColor(612, vec(0,0,0))

holoCreate(712)
holoScale(712, vec(0.12,0.12,2.032))
holoModel(712, "cylinder")
holoPos(712, holoEntity(4):toWorld(vec(0,2.6,8.88)))
holoAng(712, holoEntity(4):toWorld(ang(0,0,0)))
holoParent(712, holoEntity(4))
holoMaterial(712, "debug/debugdrawflat" )
holoColor(712, vec(0,0,0))

holoCreate(812)
holoScale(812, vec(0.12,0.12,2.032))
holoModel(812, "cylinder")
holoPos(812, holoEntity(4):toWorld(vec(0,-2.6,8.88)))
holoAng(812, holoEntity(4):toWorld(ang(0,0,0)))
holoParent(812, holoEntity(4))
holoMaterial(812, "debug/debugdrawflat" )
holoColor(812, vec(0,0,0))


#DZIURKI 2
holoCreate(912)
holoScale(912, vec(0.12,0.12,2.032))
holoModel(912, "cylinder")
holoPos(912, holoEntity(4):toWorld(vec(1.9,1.9,8.88)))
holoAng(912, holoEntity(4):toWorld(ang(0,0,0)))
holoParent(912, holoEntity(4))
holoMaterial(912, "debug/debugdrawflat" )
holoColor(912, vec(0,0,0))

holoCreate(1012)
holoScale(1012, vec(0.12,0.12,2.032))
holoModel(1012, "cylinder")
holoPos(1012, holoEntity(4):toWorld(vec(-1.9,-1.9,8.88)))
holoAng(1012, holoEntity(4):toWorld(ang(0,0,0)))
holoParent(1012, holoEntity(4))
holoMaterial(1012, "debug/debugdrawflat" )
holoColor(1012, vec(0,0,0))

holoCreate(1112)
holoScale(1112, vec(0.12,0.12,2.032))
holoModel(1112, "cylinder")
holoPos(1112, holoEntity(4):toWorld(vec(-1.9,1.9,8.88)))
holoAng(1112, holoEntity(4):toWorld(ang(0,0,0)))
holoParent(1112, holoEntity(4))
holoMaterial(1112, "debug/debugdrawflat" )
holoColor(1112, vec(0,0,0))

holoCreate(1212)
holoScale(1212, vec(0.12,0.12,2.032))
holoModel(1212, "cylinder")
holoPos(1212, holoEntity(4):toWorld(vec(1.9,-1.9,8.88)))
holoAng(1212, holoEntity(4):toWorld(ang(0,0,0)))
holoParent(1212, holoEntity(4))
holoMaterial(1212, "debug/debugdrawflat" )
holoColor(1212, vec(0,0,0))

#endowy shit
holoCreate(13)
holoScale(13, vec(0.65,0.65,0.2))
holoModel(13, "cylinder")
holoPos(13, holoEntity(1):toWorld(vec(0,0,29.5)))
holoAng(13, holoEntity(1):toWorld(ang(0,0,0)))
holoParent(13, holoEntity(4))
holoMaterial(13, "models/props_pipes/pipeset_metal02" )

holoCreate(14)
holoScale(14, vec(0.28,0.28,0.23))
holoModel(14, "hqcylinder2")
holoPos(14, holoEntity(1):toWorld(vec(0,0,29.5)))
holoAng(14, holoEntity(1):toWorld(ang(0,0,0)))
holoParent(14, holoEntity(1))
holoMaterial(14, "models/props_canal/metalwall005b" )

holoCreate(15)
holoScale(15, vec(0.15,0.15,2))
holoModel(15, "hqcylinder2")
holoPos(15, holoEntity(4):toWorld(vec(0,0,9)))
holoAng(15, holoEntity(4):toWorld(ang(0,0,0)))
holoParent(15, holoEntity(4))
holoMaterial(15, "models/props_canal/metalwall005b" )

holoCreate(666)
holoScale(666, vec(0.3,0.3,0.2))
#holoModel(666, "cylinder")
holoPos(666, holoEntity(4):toWorld(vec(-2.6,0,15)))
holoAng(666, holoEntity(1):toWorld(ang(-90,0,0)))
holoParent(666, holoEntity(1))
holoMaterial(666, "models/props_pipes/pipeset_metal02" )
holoAlpha(666, 0)

#magazynek
holoCreate(1636)
holoScale(1636, vec(0.5,0.7,0.3))
holoPos(1636, holoEntity(4):toWorld(vec(0,-6,-8)))
holoAng(1636, holoEntity(1):toWorld(ang(-90,0,0)))
holoParent(1636, holoEntity(1))
holoMaterial(1636, "models/props_pipes/pipeset_metal02" )

}
holoParent(4, holoEntity(1))
holoAng(4, holoEntity(1):toWorld(ang(0,Obrot,0)))










if(first())
{
holoCreate(1337,vec(0,0,0),vec(0.01,1.5,1.5))
holoColor(1337,vec(255,48,0))
holoMaterial(1337 ,"models/roller/rollermine_glow")}

holoPos(1337, Ranger2:position())
holoAng(1337,Ranger2:hitNormal():toAngle())  

if(owner():weapon():type() == "weapon_physgun" & holoEntity(1):getAlpha()==0) { 
for(I=1,12){holoAlpha(I, 255)}
holoAlpha(512, 255)
holoAlpha(612, 255)
holoAlpha(712, 255)
holoAlpha(812, 255)
holoAlpha(912, 255)
holoAlpha(1012, 255)
holoAlpha(1112, 255)
holoAlpha(1212, 255)
holoAlpha(13, 255)
holoAlpha(14, 255)
holoAlpha(15, 255)
holoAlpha(666, 0)
holoAlpha(1636, 255)
holoAlpha(1337, 255)
holoEntity(1337):setTrails(2,0,0.1,"engine/additivevertexcolorvertexalpha",vec(255,128,0),1)
}
if(owner():weapon():type() != "weapon_physgun" & holoEntity(1):getAlpha()==255) {
for(I=1,12){holoAlpha(I, 0)}
holoAlpha(512, 0)
holoAlpha(612, 0)
holoAlpha(712, 0)
holoAlpha(812, 0)
holoAlpha(912, 0)
holoAlpha(1012, 0)
holoAlpha(1112, 0)
holoAlpha(1212, 0)
holoAlpha(13, 0)
holoAlpha(14, 0)
holoAlpha(15, 0)
holoAlpha(666, 0)
holoAlpha(1636, 0)
holoAlpha(1337, 0)
holoEntity(1337):removeTrails()
}
 

