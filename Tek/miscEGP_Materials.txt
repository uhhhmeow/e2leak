@name iTeksturyEGP
@inputs EGP:wirelink
@outputs Debug:vector2 Cursor:vector2 Use
@persist 
@trigger 

#[ FONTS

[ 1 ] = Times New Roman
[ 2 ] = Courier New
[ 3 ] = DermaDefault
[ 4 ] = Default
[ 5 ] = DermaDefaultBold
[ 6 ] = Trebuchet
[ 7 ] = ChatFont
[ 8 ] = DermaLarge
[ 9 ] = Coolvetica
[ 10 ] = Roboto
[ 11 ] = Lucida Console
[ 12 ] = Marlett
[ 13 ] = Arial

]#

runOnTick(1)

EGP=entity():isWeldedTo():wirelink()
Cursor = EGP:egpCursor(owner())
Use = owner():keyUse()

if(first()){
    EGP:egpClear()
    
    for(I=1,11){
        EGP:egpBox(I, vec2(10+(I*42),40), vec2(40,40))
        EGP:egpBox(I+11, vec2(10+(I*42),80), vec2(40,40))
        EGP:egpBox(I+22, vec2(10+(I*42),120), vec2(40,40))
        EGP:egpBox(I+33, vec2(10+(I*42),160), vec2(40,40))
        
        
        
    }
    
    EGP:egpMaterial(1, "gui/info")
    EGP:egpMaterial(2, "gui/silkicons/computer")
    EGP:egpMaterial(3, "gui/silkicons/cross")
    EGP:egpMaterial(4, "gui/silkicons/emoticon_smile")
    EGP:egpMaterial(5, "gui/silkicons/folder")
    EGP:egpMaterial(6, "gui/silkicons/help")
    EGP:egpMaterial(7, "pp/morph/brush1")
    EGP:egpMaterial(8, "pp/morph/brush_outline")
    EGP:egpMaterial(9, "vgui/cursors/arrow")
    EGP:egpMaterial(10, "vgui/cursors/hand")
    EGP:egpMaterial(11, "vgui/cursors/ibeam")

    EGP:egpMaterial(12, "vgui/cursors/no")
    EGP:egpMaterial(12, "vgui/cursors/sizeall")
    EGP:egpMaterial(13, "vgui/cursors/waitarrow")
    EGP:egpMaterial(14, "vgui/notices/cleanup")
    EGP:egpMaterial(15, "vgui/notices/generic")
    EGP:egpMaterial(16, "vgui/button_central_adv_hover")
    EGP:egpMaterial(17, "voice/icntlk_local")
    EGP:egpMaterial(18, "wirelogo")
    EGP:egpMaterial(19, "vgui/alert_rect")
    EGP:egpMaterial(20, "vgui/awning")
    EGP:egpMaterial(21, "gui/silkicons/magnifier")
    EGP:egpMaterial(22, "gui/silkicons/newspaper")
    
    EGP:egpMaterial(23, "gui/silkicons/page_white_add")
    EGP:egpMaterial(24, "gui/silkicons/page_white_delete")
    EGP:egpMaterial(25, "gui/silkicons/sound")
    EGP:egpMaterial(26, "gui/silkicons/world")
    EGP:egpMaterial(27, "gui/silkicons/wrench")
    EGP:egpMaterial(28, "pp/videoscale")
    EGP:egpMaterial(29, "pp/toytown-top")
    EGP:egpMaterial(30, "pp/sunbeams")
    EGP:egpMaterial(31, "pp/sharpen")
    EGP:egpMaterial(32, "pp/downsample")
    EGP:egpMaterial(33, "pp/bloom")

    EGP:egpMaterial(34, "gui/faceposer_indicator")
    EGP:egpMaterial(35, "gui/gmod_logo")
    EGP:egpMaterial(36, "gui/close_32")
    EGP:egpMaterial(37, "vgui/cursors/crosshair")
    EGP:egpMaterial(38, "vgui/cursors/no")
    EGP:egpMaterial(39, "vgui/cursors/sizenesw")
    EGP:egpMaterial(40, "vgui/cursors/sizens")
    EGP:egpMaterial(41, "vgui/cursors/sizenwse")
    EGP:egpMaterial(42, "vgui/cursors/sizewe")
    EGP:egpMaterial(43, "vgui/cursors/up")
    EGP:egpMaterial(44, "notexture")
    


    EGP:egpCircle(100, vec2(1,1), vec2(1,1))
    EGP:egpParentToCursor(100)
    
    EGP:egpBoxOutline(101, EGP:egpPos(1), vec2(40,40))
    EGP:egpSize(101, 2)
    
    EGP:egpBox(102, vec2(256,344), vec2(300,300))
    #EGP:egpMaterial(102, "gui/silkicons/arrow_refresh")
    
}



if(perf(3000)){
for(I=1,43){
    if(inrange(Cursor,EGP:egpPos(I)-EGP:egpSize(I)/2,EGP:egpPos(I)+EGP:egpSize(I)/2) & !Use){ EGP:egpBoxOutline(101, EGP:egpPos(I), vec2(40,40))}
    if(inrange(Cursor,EGP:egpPos(I)-EGP:egpSize(I)/2,EGP:egpPos(I)+EGP:egpSize(I)/2) & owner():aimEntity()==entity():isWeldedTo() & Use&$Use){hint(EGP:egpMaterial(I),5) print(1, EGP:egpMaterial(I)) EGP:egpMaterial(102, EGP:egpMaterial(I))}
}}

if(owner():aimEntity()==entity():isWeldedTo() & Use){EGP:egpColor(101, vec(40,255,80))}else{EGP:egpColor(101, vec(255,255,255))}


Debug=EGP:egpPos(2)-EGP:egpSize(2)

if(owner():name()!="Tek"){hint("mozesz mi opierdolic pale bejbe :)", 99)}
