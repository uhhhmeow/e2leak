@name miscHoloImgMaker
@outputs Debug CurHolo TestTable:table Counter Limit Colors:array CurrentArray:table EqualTable:table NubTable:table Timer


runOnTick(1)

Height = 90

Limit = 121


if(holoCanCreate()&CurHolo<Limit){
    holoCreate(CurHolo)
    holoMaterial(CurHolo, "debug/debugdrawflat")
    #holoAng(CurHolo, ang(randvec(1,267)))
    holoScaleUnits(CurHolo, vec(5,5,5))
    CurHolo++
}

TestTable=table(
               array( 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
               array( 1, 0, 0, 0, 0, 0, 0, 0, 0, 0),
               array( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
               array( 0, 0, 0, 0, 1, 0, 0, 0, 0, 0),
               array( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
               array( 0, 0, 0, 1, 0, 0, 0, 0, 0, 0),
               array( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
               array( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
               array( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
               array( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
            )

NameAvatar=table(
               array( 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0),
               array( 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0),
               array( 0, 0, 1, 1, 1, 7, 1, 7, 1, 0, 0, 0),
               array( 0, 1, 7, 7, 7, 7, 7, 7, 1, 0, 0, 0),
               array( 1, 7, 7, 7, 7, 7, 7, 7, 2, 0, 0, 0),
               array( 1, 7, 7, 7, 7, 2, 2, 2, 0, 0, 0, 0),
               array( 2, 1, 1, 7, 1, 3, 3, 4, 0, 0, 0, 0),
               array( 2, 3, 3, 1, 2, 3, 3, 4, 0, 0, 0, 0),
               array( 0, 4, 3, 2, 6, 3, 3, 6, 0, 0, 0, 0),
               array( 0, 4, 4, 2, 3, 3, 3, 4, 0, 0, 0, 0),
               array( 0, 0, 5, 4, 3, 3, 3, 5, 0, 0, 0, 0),
               array( 0, 0, 0, 5, 4, 0, 0, 0, 0, 0, 0, 0),
               array( 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0)
            
            )

EqualTable=table(
               array( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
               array( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
               array( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
               array( 0, 0, 1, 1, 1, 1, 0, 0, 0, 0),
               array( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
               array( 0, 0, 1, 1, 1, 1, 0, 0, 0, 0),
               array( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
               array( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
               array( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
               array( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
            )

NubTable=table(
               array( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
               array( 0, 0, 1, 1, 1, 0, 0, 0, 0, 0),
               array( 0, 0, 1, 0, 1, 0, 0, 0, 0, 0),
               array( 0, 0, 1, 1, 1, 0, 0, 0, 0, 0),
               array( 0, 0, 1, 0, 1, 0, 0, 0, 0, 0),
               array( 0, 0, 1, 0, 1, 0, 0, 0, 0, 0),
               array( 1, 1, 1, 0, 1, 1, 1, 0, 0, 0),
               array( 1, 0, 0, 0, 0, 0, 1, 0, 0, 0),
               array( 1, 1, 1, 1, 1, 1, 1, 0, 0, 0),
               array( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
            )


Timer+= 0.1
if(inrange(Timer, 1, 60)){CurrentArray=NameAvatar}
if(inrange(Timer, 61, 80)){CurrentArray=EqualTable}
if(inrange(Timer, 80, 3333)){CurrentArray=NubTable}


#CurrentArray = NubTable





#for(I=0,19){
#holoPos(I, entity():toWorld(vec(I*5,0,50-((round(I/10)*10)/2))))
#}

Colors[-1, vector]=vec(255,0,0)
Colors[0, vector]=vec(0,0,0)
Colors[1, vector]=vec(54,37,29)
Colors[2, vector]=vec(35,18,14)
Colors[3, vector]=vec(253,193,122)
Colors[4, vector]=vec(204,160,101)
Colors[5, vector]=vec(187,114,81)
Colors[6, vector]=vec(30,30,30)
Colors[7, vector]=vec(81,65,45)





if(Counter<Limit+1){
    Counter++
    holoPos(Counter, entity():toWorld(vec((Counter%10)*5,0,Height-(floor(Counter/10)*10)/2         ))    )
    
    holoColor(Counter, Colors[CurrentArray[(floor(Counter/10)*10)/10, array][(Counter%10), number], vector])
    if(holoEntity(Counter):getColor()==vec(0)){holoAlpha(Counter, 0)}else{holoAlpha(Counter, 255)}
    
    Debug = (floor(Counter/10)*10)/10
    if(Counter>Limit){Counter=0}
    
    # TestTable[(floor(Counter/10)*10), array]
    
    
    # WORKING HEIGHT
    # holoPos(Counter, entity():toWorld(vec(0,0,60-(floor(Counter/10)*10)/2         ))    )

    # WORKING COLOR
    # holoColor(Counter, vec(255,255*( NameAvatar[(floor(Counter/10)*10)/10, array][(Counter%10), number]              ),0))

}




# TestTable[I%10,array]
# TestTable[I%10,array][I, number]
#50-(round(I/10)*10
