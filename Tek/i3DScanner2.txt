@name i3DScanner
@model models/mechanics/wheels/wheel_smooth_18r.mdl
@outputs Debug Object:entity R:ranger R2:ranger Mode Master:entity Use Reload SpawnPos:vector ResetTimer StartTimer Lag ScanTimer ScanMode
@outputs GoUp SpawnLag PropArray:array Prop:entity X Y TimerTimer CurrentSpawnPos:vector [OldSpawn NewSpawn]:entity Model1:string Model2:string Intersection
@outputs AngMod:angle MRNorm:vector Angiel:vector
# THIS PROJECT IS NOT SERIOUS, I'M MAKING IT ON A WHIM ON A SHITTY SERVER WITHOUT CONCEPT OR DOCUMENTATION... BRING IT ON

runOnTick(1)

setName("Prop Count: "+PropArray:count():toString())

#Debug=owner():aimEntity():boxSize()
Master=owner()
Use=Master:keyUse()
Reload=Master:keyReload()
Precision=3.5   #(DEF=7, 3.5 AWESOME))LESS IS MORE PRECISION(5 SEEMS TO BE "PERFECT SIMETRIC VALUE ON BARREL", AND 7 ON BOXES)
Intersection=0  # 1 = intersection prop is spawned between main spawns 0 = not
Model1 = "models/hunter/plates/plate.mdl"    # MAIN SPAWN MODEL
Model2 = "models/hunter/plates/plate.mdl" # INTERSECTION SPAWN MODEL
AngMod = ang(0,0,0)

# models/hunter/plates/plate.mdl            # LITTLE CUBE (AND MOD: )
# models/hunter/plates/plate025.mdl         # 1/4 OF AP PLATE (ANG MOD: )
# models/hunter/plates/plate025x025.mdl     # SMALL PLATE (ANG MOD: 90,0,0)
# 

if(first()){
    for(I=1,6){
        holoCreate(I)
    }
    
    Mode=0
    ScanMode=1  
    holoModel(1, "hq_cylinder")
    holoModel(2, "hq_cylinder")
    holoScaleUnits(2, vec(15,15,2))
    holoAlpha(3, 0)
    holoMaterial(3, "models/wireframe")
    holoMaterial(4, "models/wireframe")
    holoScaleUnits(5, vec(2,2,2))
    holoModel(4, "hq_sphere")
    #holoScaleUnits(4, vec(3,3,3))
    holoPos(6, holoEntity(4):pos())
    #holoParent(6, holoEntity(4))
    holoScaleUnits(6, vec(5,5,5))
    holoColor(6, vec(255,0,0))
    holoAlpha(3, 0)
    holoAlpha(4, 0)
    holoAlpha(5, 0)
    holoAlpha(6, 0)
    
    #holo
    
}

R=rangerOffset(200, entity():pos(), vec(0,0,1))
#LASER HIGHLIGHTING
if(R:entity()){holoColor(1, vec(0,255,0))}else{holoColor(1, vec(255,0,0))}

Object=R:entity()
holoPos(1, (R:position()+entity():pos())/2)
holoScaleUnits(1, vec(1,1,R:distance()))

# DELETING PROPS
if(Mode==0&PropArray[1, entity]){for(I=1,PropArray:count()){
    PropArray[I, entity]:propDelete()}
    PropArray:clear()
    
    }

if(Mode==0){
    holoPos(2, entity():pos())
    holoScaleUnits(2, vec(15,15,2))
    holoColor(2, vec(200,0,0))
    holoModel(2, "hq_cylinder")
    holoAlpha(3, 0)
    
    # RESETING 
    ScanTimer=0
    SpawnLag=0
    GoUp=0
    holoAlpha(6, 0)
    holoAlpha(4, 0)
    holoAlpha(5, 0)
}

#MODE CHANGE TO 1 - SETTING POSITION
if(Mode!=2&Mode!=3&Mode!=4&Master:aimEntity()==entity()&Use&$Use){holoAlpha(3, 0) Mode=1 holoColor(2, vec(200,200,0)) holoModel(2, "hq_tube") holoScaleUnits(2, vec(20,20,1))}

# HOLOGRAM FOLLOWING
if(Mode==1){holoPos(2, Master:aimPos())}

# MODE CHANGE TO 2 - POSITION SET
if(Mode==1&!Master:aimEntity()&Use&$Use){SpawnPos=Master:aimPos()  holoColor(2, vec(0,200,0)) holoModel(2, "hq_tube_thin") holoScaleUnits(2, vec(80,80,1)) Mode=2 Lag=-10}

# CHANGING BACK TO MODE 0 - USE KEY HOLD ON ENTITY
if(Master:aimEntity()==entity()&Use){ResetTimer++}else{ResetTimer=0}
if(ResetTimer>10){Mode=0}

# SETTING HOLO 3(WIREFRAME PREVIEW)
if(Mode>=2&Object){
    holoPos(3, holoEntity(2):pos()+vec(0,0,60+Object:boxSize():z()/5)) 
    holoColor(3, vec(255,255,255))
    holoModel(3, Object:model())
    holoScaleUnits(3, Object:boxSize())
    holoAng(3, Object:angles())
    }else{
    holoColor(3, vec(255,0,0))
    
    }
    

# MODE 2 RING COLOR CHANGE
if(Mode==2&holoEntity(2):pos():distance(owner():aimPos())<40&!Use){holoColor(2, vec(30,60,155))}elseif(Mode==2){holoColor(2, vec(10,30,60))}
if(Object & Lag>1 & Mode==2&holoEntity(2):pos():distance(owner():aimPos())<40&Use&$Use){holoColor(2, vec(60,80,255)) Mode=3}
Lag++

#############################
#MODE 3 - SCANNING


if(Mode==3 & Object){
    if(propCanCreate()){if(SpawnLag==0){ScanTimer+=1.4}elseif(Mode==3){ScanTimer=ScanTimer}
    
    holoAlpha(6, 255)
    
    
    
    holoPos(5, (holoEntity(4):pos()+R2:position())/2)
    holoScaleUnits(5, vec(R2:distance(),1,1))
    
    R2=rangerOffset(100000000, holoEntity(6):pos(), holoEntity(6):forward())
    
    if(R2:entity()==Object){holoAlpha(5, 255)}else{holoAlpha(5, 0)}
    
    if(R2:entity()==Object&R2:hit()&SpawnLag==0&CurrentSpawnPos:distance(R2:position())>Precision){
        
            # AIMNORMAL TEST
            MRNorm = R2:hitNormal()
            Angiel = MRNorm:cross(R2:entity():up()):normalized():cross(MRNorm)
            
        
        
        #Prop=propSpawn(Model1,holoEntity(3):toWorld(Object:toLocal(R2:position())),R2:hitNormal():toAngle()+AngMod,1)  #NORMAL HIT NORMAL
        Prop=propSpawn(Model1,holoEntity(3):toWorld(Object:toLocal(R2:position())),Angiel:toAngle(),1)  # NORMALIZED HIT NORMAL
        Prop:setColor(vec(80,100,255))
        #Prop:setAlpha(0)
        PropArray:pushEntity(Prop)
        SpawnLag=10
        CurrentSpawnPos=R2:position()
        holoAlpha(3, 0)
        # INTERSECTION SPAWN
        if(Intersection){
        OldSpawn = PropArray[PropArray:count()-2, entity]
        NewSpawn = PropArray[PropArray:count(), entity]
        Prop=propSpawn(Model2,(NewSpawn:pos()+OldSpawn:pos())/2,(OldSpawn:pos()-NewSpawn:pos()):toAngle()+ang(0,0,0),1)
        Prop:setColor(vec(100,140,255))
        PropArray:pushEntity(Prop)
        #(R:position()+entity():pos())/2
        #(OldSpawn:pos()-NewSpawn:pos())/2
}
        
        
        
}
    
    SpawnLag--
    if(SpawnLag<=0){SpawnLag=0}
    #holoPos(4, Object:pos()+((holoEntity(4):forward()*5)*Object:boxSize():x())-vec(0,0,(Object:boxSize():z()/1.5)+GoUp))
    #lets try to change spawn type to toWorld to object so i can rotate this shit
    #holoPos(4, Object:toWorld(holoEntity(4):forward()*Object:boxSize():x())-vec(0,0,(Object:boxSize():z()/1.5)+GoUp))
    # MKAY NOW THAT Z DIMENSION...
    holoAng(5, ang(0,holoEntity(5):bearing(Object:pos()),0))
    holoPos(4, Object:toWorld((holoEntity(4):forward()*2)*Object:boxSize():x())-Object:up()*((Object:boxSize():z()/1.8)+GoUp))
    holoAng(4, ang(0,ScanTimer+45,0))
    holoAng(5, holoEntity(6):angles())
    holoPos(6, holoEntity(4):pos())
    #holoScaleUnits(6, )
    #holoAng(6, Object:toWorld(ang(0,(holoEntity(6):pos()-Object:pos()):toAngle():yaw(),0)))
    holoAng(6, Object:toWorld(ang(0,ScanTimer-135,0)))
    if(ScanTimer>=360){GoUp-=3.2 ScanTimer=0}
    holoAlpha(3, 0)
}
}






    #propSpawn("models/hunter/blocks/cube05x1x025.mdl",holoEntity(7):pos(),holoEntity(7):angles(),1)
    
    






#holoPos(4, Object:toWorld(holoEntity(4):forward()*Object:boxSize():x()))
#holoAng(4, ang(0,0,0))



if(!Object&Mode>=2){holoAlpha(3, 0)}elseif(Mode==2){holoAlpha(3, 255)}

