@name iCinema
@inputs 
@outputs Camera:entity E:entity Eang:angle Epos:vector Array:array Distance LastSaid R:ranger
@persist 
@model models/hunter/plates/plate8x8.mdl

runOnTick(1)

if(first()){
    hint("!camera <distance>",10)
    
entity():setColor(vec(30,30,30))
entity():setMaterial("debug/debugdrawflat")
holoCreate(1)
holoScale(1, vec(-3,-3,-3))
holoModel(1, "hq_sphere")
holoMaterial(1, "debug/debugdrawflat")
holoAlpha(1, 200)
# IMPORTANT NOTE ! DUE TO PLAYX PROJECTING NOT IN STRAIGHT LINE, PLATE IS CAPABLE
# OF PROJECTING UP TO DISTANCE 4000
}
runOnChat(1)
findByClass("gmod_playx")
Camera = findResult(1)
E = entity()
Eang = E:angles()
Epos = E:pos()
E:applyForce(((Camera:toWorld(vec(Distance,0,0))-E:pos())*25-E:vel()/2)*E:mass())
#E:applyAngForce(((Eang+$Eang*40)-Camera:angles())*E:mass())

 # QUATERNION
    
    #Pos = Cel:toWorld(Cel:boxCenter())   #aim for the center 

    Eye = Camera:angles()+ang(90,0,0)
    #Cel = owner()

    #calculate the quaternions
    A = Eye
    TarQ = quat(A)
    CurQ = quat(E)
    
    Q = TarQ/CurQ
    V = transpose(matrix(E))*rotationVector(Q)
    
    #Aim
    E:applyTorque((100*V - 10*E:angVelVector())*E:inertia())

Array = owner():lastSaid():explode(" ")
if(Array[1, string]=="!camera"){hideChat(1) Distance=Array[2,string]:toNumber()}


if(first()){
    holoPos(1, Camera:toWorld(vec(0,0,0)))
    holoAng(1, Camera:toWorld(ang(0,0,0)))
    holoParent(1, Camera)
}
