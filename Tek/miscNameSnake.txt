@name miscNameSnake
@persist E:entity

interval(10)

if(first()){
    E=entity()
    
    holoCreate(1,E:pos(),vec(0.3))
    holoParent(1,E)
    
    for(I=2,30){
        
        holoCreate(I,E:toWorld(vec(0,0,(I-1.5)*10)))
        holoScaleUnits(I,vec(1,1,5))
        
    }
    
}


for(I=2,30){
    
    holoPos(I,holoEntity(I-1):pos()+
    (holoEntity(I):toWorld(vec(0,0,-2.5))-holoEntity(I-1):toWorld(vec(0,0,-2.5))):normalized()*5.5)
    
    
    holoAng(I,(holoEntity(I):toWorld(vec(0,0,2.5))-holoEntity(I-1):toWorld(vec(0,0,-2.5))):toAngle()+ang(90,0,0))
    
}

