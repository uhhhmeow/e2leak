@name iColor2
@model models/hunter/plates/plate025x05.mdl
@outputs Fire Debug Master:entity Use Reload X Y Z HudCursor:vector HudCursorLocal:vector CoreHolo:entity CoreHull:entity Ons OnsZ Attack1 PlayerColor:vector
@outputs SliderLocalY HSVMode Lag HSVTimer MatListMode HSVPhys HSVPhysTimer

runOnTick(1)

Master=owner()

if(first()){
    
    for(I=1,60){
        holoCreate(I)
        holoPos(I, entity():boxCenterW()+vec(0,0,80)) # delete later
        holoColor(I, randvec(40,255))
        holoDisableShading(I, 1)
    }
    
    # CORE HOLOGRAM FOR POINT-LINE INTERSECTION
    holoPos(1, entity():boxCenterW()+entity():up()*1.5)
    holoAng(1, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(1, vec(1))
    holoParent(1, entity())
    holoAlpha(1, 0)
    CoreHolo=holoEntity(1)
    
    # HULL + CORE HULL
    holoPos(2, entity():boxCenterW())
    holoAng(2, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(2, entity():boxSize()-vec(0,0,1))
    holoMaterial(2, "phoenix_storms/black_chrome")
    #holoModel(2, "")
    holoColor(2, vec(255,255,255))
    holoParent(2, entity())
    
    CoreHull = holoEntity(2)
    
    # SCREEN
    holoPos(3, CoreHull:toWorld(vec(0,0,0.2)))
    holoAng(3, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(3, entity():boxSize()-vec(0.5,0.5,1))
    #holoMaterial(3, "effects/flashlight/gradient")
    #holoModel(3, "")
    holoColor(3, vec(180))
    holoParent(3, entity())
    
    # BUTTON 1 OUTLINE - PAINT PLAYER
    holoPos(4, CoreHull:toWorld(vec(-2.8,-8.7,1)))
    holoAng(4, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(4, vec(6,6,1))
    #holoMaterial(4, "bull/various/subwoofer")
    #holoModel(4, "")
    holoColor(4, vec(0))
    holoParent(4, entity())
    
    # BUTTON 1 SECONDARY OUTLINE - PAINT PLAYER
    holoPos(5, CoreHull:toWorld(vec(-2.8,-8.7,1.1)))
    holoAng(5, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(5, vec(5.5,5.5,1))
    #holoMaterial(5, "vgui/hand")
    #holoModel(5, "")
    holoColor(5, vec(255))
    holoParent(5, entity())
    
    # BUTTON 1 FILL - PAINT PLAYER
    holoPos(6, CoreHull:toWorld(vec(-2.8,-8.7,1.2)))
    holoAng(6, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(6, vec(5.3,5.3,1))
    holoMaterial(6, "vgui/hand")
    #holoModel(6, "")
    holoColor(6, vec(255))
    holoParent(6, entity())
    
    # BUTTON 1 SLIDER - OUTLINE
    holoPos(7, CoreHull:toWorld(vec(-2.8,2,1)))
    holoAng(7, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(7, vec(6,15,1))
    #holoMaterial(7, "bull/various/subwoofer")
    #holoModel(7, "")
    holoColor(7, vec(0))
    holoParent(7, entity())
    
    # BUTTON 1 SLIDER - INLINE
    holoPos(8, CoreHull:toWorld(vec(-2.8,2,1.1)))
    holoAng(8, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(8, vec(5.5,14.5,1))
    #holoMaterial(8, "bull/various/subwoofer")
    #holoModel(8, "")
    holoColor(8, vec(255))
    holoParent(8, entity())
    
    # BUTTON 1 SLIDER - HANDLE
    holoPos(9, CoreHull:toWorld(vec(-2.8,8.8,1.3)))
    holoAng(9, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(9, vec(5.6,0.3,1))
    #holoMaterial(9, "bull/various/subwoofer")
    #holoModel(9, "")
    holoColor(9, vec(0))
    holoParent(9, entity())
    
    # BUTTON 1 SLIDER - BLACK AREA
    holoPos(10, CoreHull:toWorld(vec(-2.8,-4.8,1.2)))
    holoAng(10, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(10, vec(5.5,1,1))
    #holoMaterial(10, "bull/various/subwoofer")
    #holoModel(10, "")
    holoColor(10, vec(40))
    holoParent(10, entity())
    
    # BUTTON 1 SLIDER - WHITE AREA
    holoPos(11, CoreHull:toWorld(vec(-2.8,8.8,1.2)))
    holoAng(11, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(11, vec(5.5,1,1))
    #holoMaterial(11, "bull/various/subwoofer")
    #holoModel(11, "")
    holoColor(11, vec(100))
    holoParent(11, entity())
    
    # BUTTON 2 HSVRGB LOOP - OUTLINE
    holoPos(12, CoreHull:toWorld(vec(-2.8,10.7,1)))
    holoAng(12, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(12, vec(6,2,1))
    #holoMaterial(12, "bull/various/subwoofer")
    #holoModel(12, "")
    holoColor(12, vec(0))
    holoParent(12, entity())
    
    # BUTTON 2 HSVRGB LOOP - INLINE
    holoPos(13, CoreHull:toWorld(vec(-2.8,10.7,1.1)))
    holoAng(13, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(13, vec(5.5,1.5,1))
    #holoMaterial(13, "bull/various/subwoofer")
    #holoModel(13, "")
    holoColor(13, vec(255))
    holoParent(13, entity())
    
    # SHOW MATS OUTLINE
    holoPos(14, CoreHull:toWorld(vec(0,13.3,1)))
    holoAng(14, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(14, vec(10,2,1))
    holoMaterial(14, "phoenix_storms/black_chrome")
    #holoModel(14, "")
    holoColor(14, vec(255))
    holoParent(14, entity())
    
    # SHOW MATS FILL
    holoPos(15, CoreHull:toWorld(vec(0,13.3,1.1)))
    holoAng(15, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(15, vec(9.5,1.5,1))
    #holoMaterial(15, "phoenix_storms/black_chrome")
    #holoModel(15, "")
    holoColor(15, vec(255))
    holoParent(15, entity())
    
    # BUTTON 3 OUTLINE - COLOR PHYSGUN
    holoPos(16, CoreHull:toWorld(vec(3,-8.7,1)))
    holoAng(16, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(16, vec(5,5,1))
    #holoMaterial(16, "bull/various/subwoofer")
    #holoModel(16, "")
    holoColor(16, vec(0))
    holoParent(16, entity())
    
    # BUTTON 3 INLINE - COLOR PHYSGUN
    holoPos(17, CoreHull:toWorld(vec(3,-8.7,1.1)))
    holoAng(17, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(17, vec(4.8,4.8,1))
    #holoMaterial(17, "jaanus/wiretool/rangerha")
    #holoModel(17, "")
    holoColor(17, vec(100))
    holoParent(17, entity())
    
    # BUTTON 3 INLINE - COLOR PHYSGUN
    holoPos(18, CoreHull:toWorld(vec(3,-8.7,1.2)))
    holoAng(18, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(18, vec(4.6,4.6,1))
    holoMaterial(18, "jaanus/wiretool/rangerha")
    #holoModel(18, "")
    holoColor(18, vec(255))
    holoParent(18, entity())
    
    # BUTTON 3 SIDE OUTLINE - COLOR PHYSGUN
    holoPos(19, CoreHull:toWorld(vec(3,-5.7,1)))
    holoAng(19, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(19, vec(4,1,1))
    #holoMaterial(19, "bull/various/subwoofer")
    #holoModel(19, "")
    holoColor(19, vec(0))
    holoParent(19, entity())
    
    # BUTTON 3 SIDE OUTLINE - COLOR PHYSGUN
    holoPos(20, CoreHull:toWorld(vec(3,-5.75,1.1)))
    holoAng(20, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(20, vec(3.8,0.9,1))
    #holoMaterial(20, "bull/various/subwoofer")
    #holoModel(20, "")
    holoColor(20, vec(255))
    holoParent(20, entity())
    
    # PHYSGUN PARENTED HOLOGRAM
    #holoPos(21, owner():pos()+vec(0,0,60))
    #holoMaterial(21, "sprites/physg_glow2")
    holoColor(21, vec(255,0,0))
    
    # BUTTON 4 OUTLINE - SHADOW MANIPULATION
    holoPos(22, CoreHull:toWorld(vec(3,-2,1)))
    holoAng(22, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(22, vec(5,5,1))
    #holoMaterial(22, "bull/various/subwoofer")
    #holoModel(22, "")
    holoColor(22, vec(0))
    holoParent(22, entity())
    
    entity():setAlpha(0)
    SliderLocalY=10
    PlayerColor=vec(255)
    
}

# GLOBALS
Use = Master:keyUse()
Reload = Master:keyReload()
Attack1 = Master:keyAttack1()


Holo = holoEntity(1)

Origin = Master:shootPos()
OriginDirVec = Master:eye()

#How does line-plane intersection work?
PlanePoint = Holo:pos() #Get a point in the plane
Normal = Holo:up() #Get the normal (a vector perpendicular to the surface) of the plane
LinePoint1 = Origin #Get a point on the line
LinePoint2 = Origin+OriginDirVec #Get a point on the line "after" point 1#
X = (Normal:dot(PlanePoint-LinePoint1))/(Normal:dot(LinePoint2-LinePoint1)) #Not really sure how, but it returns how many times the distance from point 1 to point 2 you need to go from point 1 to reach the intersection
HudCursor = LinePoint1+X*(LinePoint2-LinePoint1) #Get the intersections position using f(X) = LinePoint1+X*(LinePoint2-LinePoint1)
HudCursorLocal=holoEntity(1):toLocal(HudCursor)
# HUD CURSOR END//

#holoPos(30, HudCursor)

X=HudCursorLocal:x()
Y=HudCursorLocal:y()
OnsZ=entity():toLocal(Master:aimPos()):z()
if(Master:aimEntity()==entity() & OnsZ>1 & !Attack1){Ons=1}else{Ons=0}



X = HudCursorLocal:x()
Y = HudCursorLocal:y()
Z = HudCursorLocal:z()

# COLORING PLAYER
owner():setColor(PlayerColor)


# HIGHLIGHTING AND CONTROLL
# BUTTON 1 - COLOR PLAYER
if(!Use&Ons&Master & inrange(HudCursorLocal, vec(-5.5,-11,-10), vec(0,-6,10))) {holoColor(5, vec(200,200,60))}else{holoColor(5, vec(100))}
if(Use&Ons&Master & inrange(HudCursorLocal, vec(-5.5,-11,-10), vec(0,-6,10))) {holoColor(5, vec(60,200,60)) PlayerColor=holoEntity(8):getColor()}


# SLIDER HANDLE MOVING
if(Use&Ons&Master & inrange(HudCursorLocal, vec(-5.5,-5,-10), vec(0,9,10))) {
    
    SliderLocalY=entity():toLocal(HudCursor):y()
    #+4.1
    
    holoPos(9, CoreHull:toWorld(vec(-2.8,SliderLocalY,1.3)))}
    
    if(SliderLocalY>-4.4 & SliderLocalY<8.3){holoColor(8, hsv2rgb(vec(clamp((SliderLocalY+4.1)*30,0,360),1,1)))}
    if(SliderLocalY<-4.4){holoColor(8, vec(20))}
    if(SliderLocalY>8.3){holoColor(8, vec(200))}
    

# LAG
Lag--
if(Lag<=0){Lag=0}

# HSV MODE HOVERING AND ACTIVATING
if(HSVMode==0&!Use&Ons&Master & inrange(HudCursorLocal, vec(-5.5,9.9,-10), vec(0,11.5,10))) {holoColor(13, vec(200,200,60))}elseif(HSVMode==0){holoColor(13, vec(100))}
if(!Lag&HSVMode==0&Use&$Use&Ons&Master & inrange(HudCursorLocal, vec(-5.5,9.9,-10), vec(0,11.5,10))) {HSVMode=1 Lag=10 holoColor(13, vec(60,200,60))}
if(!Lag&HSVMode==1&Use&$Use&Ons&Master & inrange(HudCursorLocal, vec(-5.5,9.9,-10), vec(0,11.5,10))) {HSVMode=0 Lag=10 PlayerColor=holoEntity(8):getColor()}
if(HSVMode){
    HSVTimer++
    if(HSVTimer>=360){HSVTimer=0}
    
    PlayerColor=hsv2rgb(HSVTimer,1,1)
    holoColor(8, hsv2rgb(HSVTimer,1,1))
    
    
}else{HSVTimer=0}
    
# MATERIALS DISPLAY MatListMode
if(!MatListMode&!Use&!Ons&Master & inrange(HudCursorLocal, vec(-4.7,12.6,-10), vec(4.7,14.1,10))) {holoColor(15, vec(200,200,60))}elseif(MatListMode==0){holoColor(15, vec(100))}
if(!MatListMode&!Lag&Use&$Use&!Ons&Master & inrange(HudCursorLocal, vec(-4.7,12.6,-10), vec(4.7,14.1,10))) {
    MatListMode=1 
    holoColor(15, vec(60,200,60))
    
    
    
    Lag=10
    }
    if(MatListMode==1&!Lag&Use&$Use&!Ons&Master & inrange(HudCursorLocal, vec(-4.7,12.6,-10), vec(4.7,14.1,10))) {MatListMode=0 Lag=10}

# PHYSGUN PERSONAL HOLO PROJECTION
#holoPos(21,owner():attachmentPos("eyes")+(owner():aimPos()-owner():attachmentPos("eyes")):normalized()*50)
    
# HSVPHYSGUNMODE
# HSVPhys
if(HSVPhys==0&!Use&Ons&Master & inrange(HudCursorLocal, vec(0.6,-10.8,-10), vec(5.4,-6.3,10))) {holoColor(20, vec(200,200,60)) }elseif(HSVPhys==0){holoColor(20, vec(100))}
if(HSVPhys==0&!Lag&Use&$Use&Ons&Master & inrange(HudCursorLocal, vec(0.6,-10.8,-10), vec(5.4,-6.3,10))) {HSVPhys=1 holoColor(20, vec(60,200,60)) Lag=10}
if(HSVPhys==1&!Lag&Use&$Use&Ons&Master & inrange(HudCursorLocal, vec(0.6,-10.8,-10), vec(5.4,-6.3,10))) {HSVPhys=0 Lag=10}
    
if(HSVPhys==1){
    HSVPhysTimer+=3
    if(HSVPhysTimer>360){HSVPhysTimer=0}
    
    
    holoMaterial(21, "sprites/physg_glow2")
    holoColor(21, hsv2rgb(HSVPhysTimer,1,1))
    holoPos(21,owner():attachmentPos("eyes")+(owner():aimPos()-owner():attachmentPos("eyes")):normalized()*50)
    }



print(4, HudCursorLocal:toString())
#print(4, ((SliderLocalY+4.1)*30):toString())
#print(4, ops():toString())
#print(4, entity():boxSize():toString())
#if()
# decals/rendershadow

