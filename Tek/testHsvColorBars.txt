@name testHsvColorBars
@inputs 
@outputs 
@persist 
@trigger 

for(I=1,36){
    holoCreate(I)
    holoMaterial(I, "effects/flashlight/gradient")
    holoScaleUnits(I, vec(1.5,1,38.3))
    holoPos(I, entity():toWorld(vec(I*1.32,0,80)))
    holoColor(I, hsv2rgb(vec(I*10,1,1)))
    
    holoClipEnabled(I,1,1)
    holoClip(I,1,vec(-0.7,0,0),vec(1,0,0),0)
    holoClipEnabled(I,2,1)
    holoClip(I,2,vec(0.7,0,0),vec(-1,0,0),0)
    holoClipEnabled(I,3,1)
    holoClip(I,3,vec(0,0,18),vec(0,0,-1),0)
    holoClipEnabled(I,4,1)
    holoClip(I,4,vec(0,0,-18),vec(0,0,1),0)
    
}


    #holoCreate(37)
    #holoMaterial(I, "effects/flashlight/gradient")
    holoScaleUnits(37, vec(1,1,36))
    #holoPos(1, entity():toWorld(vec(I,0,80)))
    #holoColor(1, hsv2rgb(vec(I*10,1,1)))
    holoPos(37, entity():toWorld(vec(37,0,80)))
