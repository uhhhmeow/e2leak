@name iConnect_4_2.0
@outputs  [Player1 Player2 TempPlayer Master]:entity Tura
@persist  Debug Curholo Holoturn SpawnDone
@outputs  Przerwa Use On X Y Z RowSelect T
@persist Row1 Row2 Row3 Row4 Row5 Row6 Row7
@persist  ReloadTimer CenterCheck CheckRow Debug2
@persist WinColor:vector FR P:array
@persist [DefMat CheckMat]:string Victory VictoryT SoundT
@model models/hunter/plates/plate2x3.mdl 



#Debug = findResult(FR):name()

if(first()){
    #Player1=findPlayerByName("Tek")
    #Player2=findPlayerByName("Tek")
    
    #DefMat = "debug/env_cubemap_model"
    DefMat = "debug/debugdrawflat"
    CheckMat = "debug/debugdrawflat"
    
    WinColor = vec(40,255,40)
    
    Row1 = 0
    Row2 = 0
    Row3 = 0
    Row4 = 0
    Row5 = 0
    Row6 = 0
    Row7 = 0
    
    Tura=1
    Curholo=10
    Holoturn = 1
    holoCreate(999)
    holoCreate(998)
    holoModel(998, "prism")
    holoMaterial(998, "debug/debugdrawflat")
    holoScaleUnits(998, vec(10,4,10))
    holoMaterial(999, "debug/debugdrawflat")
    holoColor(999, vec(0,101,176))
    holoScaleUnits(999, entity():boxSize())
    holoPos(999, entity():toWorld(vec(0,0,0)))
    holoAng(999, entity():toWorld(ang(0,0,0)))
    holoParent(999, entity())
    entity():setAlpha(0)
    
    holoCreate(5)
    holoCreate(6)
    
    holoModel(5, "pyramid")
    holoModel(6, "pyramid")
    
    holoColor(5, vec(254,0,0))
    holoColor(6, vec(255,254,0))
    
    holoAng(5, ang(180,0,0))
    holoAng(6, ang(180,0,0))
    
    # VICTORY HOLOS
    holoCreate(2001)
    holoCreate(2002)
    holoCreate(2003)
    holoCreate(2004)
    
    holoParent(2001, entity())
    holoParent(2002, entity())
    holoParent(2003, entity())
    holoParent(2004, entity())    
    
    holoModel(2001, "hq_tube")
    holoModel(2002, "hq_tube")
    holoModel(2003, "hq_tube")
    holoModel(2004, "hq_tube")
    
    holoScaleUnits(2001, vec(13.2,13.2,3.9))
    holoScaleUnits(2002, vec(13.2,13.2,3.9))
    holoScaleUnits(2003, vec(13.2,13.2,3.9))
    holoScaleUnits(2004, vec(13.2,13.2,3.9))
    
    holoColor(2001, vec(60,255,60))
    holoColor(2002, vec(60,255,60))
    holoColor(2003, vec(60,255,60))
    holoColor(2004, vec(60,255,60))
    
    # CONNECT 4 STAND HOLOGRAM
    holoCreate(339)
    holoScaleUnits(339, vec(2,80,6))
    holoColor(339, vec(0,101,176))
    holoPos(339, entity():toWorld(vec(48,0,0)))
    holoAng(339, entity():toWorld(ang(0,0,0)))
    holoParent(339, entity())
    
    # CONNECT 4 PLAYER INDICATOR HOLOGRAM
    holoCreate(340)
    holoScaleUnits(340, vec(1,79,6.2))
    holoColor(340, vec(0,255,176))
    holoPos(340, entity():toWorld(vec(48,0,0)))
    holoAng(340, entity():toWorld(ang(0,0,0)))
    holoParent(340, entity())
    holoDisableShading(340, 1)
    
    #CONNECT 4 BORDER
    holoCreate(284)
    holoParent(284, entity())
    holoScaleUnits(284, entity():boxSize()+vec(1,1,-0.55))
    holoColor(284, vec(0,91,166))
    
    #CONNECT 4 TUBE POINTER
    holoCreate(937)
    holoParent(937, entity())
    holoModel(937, "hq_tube_thin")
    holoScaleUnits(937, vec(13.2,13.2,3.9))
    holoMaterial(937, "debug/debugdrawflat")
    holoColor(937, vec(200))
}

########## RUN ON TICK ACTIVATOR
if(!holoEntity(75)){interval(1) SpawnDone=0}else{SpawnDone=1}
if(SpawnDone & !Player1 | SpawnDone & !Player2){interval(1)}
if(SpawnDone & Player1 & Player2){interval(20)}


############################
########## TILES CREATION ##
############################

if(!holoEntity(15)&holoCanCreate() & Przerwa==5){
for(I=10,15){holoCreate(I)}
Przerwa=0}

if(!holoEntity(25)&holoEntity(15) & Przerwa==5 & holoCanCreate()){
for(I=20,25){holoCreate(I)}
Przerwa=0}

if(!holoEntity(35)&holoEntity(25) & Przerwa==5 & holoCanCreate()){
for(I=30,35){holoCreate(I)}
Przerwa=0}

if(!holoEntity(45)&holoEntity(35) & Przerwa==5 & holoCanCreate()){
for(I=40,45){holoCreate(I)}
Przerwa=0}

if(!holoEntity(55)&holoEntity(45) & Przerwa==5 & holoCanCreate()){
for(I=50,55){holoCreate(I)}
Przerwa=0}

if(!holoEntity(65)&holoEntity(55) & Przerwa==5 & holoCanCreate()){
for(I=60,65){holoCreate(I)}
Przerwa=0}

if(!holoEntity(75)&holoEntity(65) & Przerwa==5 & holoCanCreate()){
for(I=70,75){holoCreate(I)}
Przerwa=0}

#[

#CREATING FIRST ROW (10)
if(!holoEntity(15) & holoCanCreate()){
    holoCreate(Curholo) 
    Curholo++}
#CREATING SECOND ROW (20)
if(holoEntity(15) & !holoEntity(25) & holoCanCreate()){
    holoCreate(Curholo)

    Curholo++}
#CREATING THIRD ROW (30)
if(holoEntity(25) & !holoEntity(35) & holoCanCreate()){
    holoCreate(Curholo)

    Curholo++}
#CREATING FOURTH ROW (40)
if(holoEntity(35) & !holoEntity(45) & holoCanCreate()){
    holoCreate(Curholo) 
 
    Curholo++}
#CREATING FIFTH ROW (50)
if(holoEntity(45) & !holoEntity(55) & holoCanCreate()){
    holoCreate(Curholo) 
 
    Curholo++}
#CREATING SIXTH ROW (60)
if(holoEntity(55) & !holoEntity(65) & holoCanCreate()){
    holoCreate(Curholo) 

    Curholo++}
#CREATING SEVENTH ROW (70)
if(holoEntity(65) & !holoEntity(75) & holoCanCreate()){
    holoCreate(Curholo) 

    Curholo++}
    
    if(Curholo>15&Curholo<20){Curholo=20}
    if(Curholo>25&Curholo<30){Curholo=30}
    if(Curholo>35&Curholo<40){Curholo=40}
    if(Curholo>45&Curholo<50){Curholo=50}
    if(Curholo>55&Curholo<60){Curholo=60}
    if(Curholo>65&Curholo<70){Curholo=70}
    
    ]#
    
############################
##########   POSITIONING  ##
############################



if(SpawnDone&$SpawnDone){
    
#FIRST (10)
holoPos(10, entity():toWorld(vec(37,-45,0)))
holoPos(11, entity():toWorld(vec(22,-45,0)))
holoPos(12, entity():toWorld(vec(7,-45,0)))
holoPos(13, entity():toWorld(vec(-8,-45,0)))
holoPos(14, entity():toWorld(vec(-23,-45,0)))
holoPos(15, entity():toWorld(vec(-38,-45,0)))
     
#SECOND (20)
holoPos(20, entity():toWorld(vec(37,-30,0)))
holoPos(21, entity():toWorld(vec(22,-30,0)))
holoPos(22, entity():toWorld(vec(7,-30,0)))
holoPos(23, entity():toWorld(vec(-8,-30,0)))
holoPos(24, entity():toWorld(vec(-23,-30,0)))
holoPos(25, entity():toWorld(vec(-38,-30,0)))
     
#THIRD (30)
holoPos(30, entity():toWorld(vec(37,-15,0)))
holoPos(31, entity():toWorld(vec(22,-15,0)))
holoPos(32, entity():toWorld(vec(7,-15,0)))
holoPos(33, entity():toWorld(vec(-8,-15,0)))
holoPos(34, entity():toWorld(vec(-23,-15,0)))
holoPos(35, entity():toWorld(vec(-38,-15,0)))
    
#FOURTH (40)
holoPos(40, entity():toWorld(vec(37,0,0)))
holoPos(41, entity():toWorld(vec(22,0,0)))
holoPos(42, entity():toWorld(vec(7,0,0)))
holoPos(43, entity():toWorld(vec(-8,0,0)))
holoPos(44, entity():toWorld(vec(-23,0,0)))
holoPos(45, entity():toWorld(vec(-38,0,0)))

#FIFTH (50)
holoPos(50, entity():toWorld(vec(37,15,0)))
holoPos(51, entity():toWorld(vec(22,15,0)))
holoPos(52, entity():toWorld(vec(7,15,0)))
holoPos(53, entity():toWorld(vec(-8,15,0)))
holoPos(54, entity():toWorld(vec(-23,15,0)))
holoPos(55, entity():toWorld(vec(-38,15,0)))

#SIXTH (60)
holoPos(60, entity():toWorld(vec(37,30,0)))
holoPos(61, entity():toWorld(vec(22,30,0)))
holoPos(62, entity():toWorld(vec(7,30,0)))
holoPos(63, entity():toWorld(vec(-8,30,0)))
holoPos(64, entity():toWorld(vec(-23,30,0)))
holoPos(65, entity():toWorld(vec(-38,30,0)))

#SEVENTH (70)
holoPos(70, entity():toWorld(vec(37,45,0)))
holoPos(71, entity():toWorld(vec(22,45,0)))
holoPos(72, entity():toWorld(vec(7,45,0)))
holoPos(73, entity():toWorld(vec(-8,45,0)))
holoPos(74, entity():toWorld(vec(-23,45,0)))
holoPos(75, entity():toWorld(vec(-38,45,0)))




for(I=10,76){
    
    holoColor(I, vec(0,0,0))
    holoModel(I, "hq_cylinder")
    holoMaterial(I, DefMat)
    #holoMaterial(I, "debug/env_cubemap_model")
    holoAng(I, entity():toWorld(ang(0,0,0)))
    holoParent(I, entity())
    holoScaleUnits(I, vec(13,13,3.8))}
    

}

holoColor(27, vec(255,0,0))

#POINTER CONTROL #################

if(Tura==1){Master=Player1}
if(Tura==2){Master=Player2}
Przerwa++
if(Przerwa>=5){Przerwa=5}
if(Master:aimEntity()==entity()){On=1}else{On=0}
Use = Master:keyUse()
X = entity():toLocal(Master:aimPos()):x()
Y = entity():toLocal(Master:aimPos()):y()
Z = entity():toLocal(Master:aimPos()):z()

##################################

# ROW SELECTION #####
if(Y<-51.5){RowSelect=0}
if(Y>-51.5&Y<-38.5&On){RowSelect=1}
if(Y>-35.5&Y<-23.5&On){RowSelect=2}
if(Y>-23.5&Y<-8.5&On){RowSelect=3}
if(Y>-6.5&Y<6.5&On){RowSelect=4}
if(Y<23.5&Y>8.5&On){RowSelect=5}
if(Y<35.5&Y>23.5&On){RowSelect=6}
if(Y<51.5&Y>38.5&On){RowSelect=7}
if(Y>51.5){RowSelect=0}
if(!On){RowSelect=0}
#####################
# TURA BOUNDARIES

if(Tura>2){Tura=1}
if(Tura==1){Master=Player1}else{Master=Player2}

 if(Use&$Use&RowSelect>0 & holoEntity(RowSelect*10+5):getColor()==vec(0,0,0)){soundPlay(20,soundDuration("buttons/lightswitch2.wav"),"buttons/lightswitch2.wav")}
    

#######################################################
#PLACING TILES

#  ROW 1 ####################################
if(!Victory&Tura==1 & On & RowSelect==1 & Use & $Use & Przerwa==5 & Row1<6){
    Przerwa=0
    holoColor(10+Row1, vec(255,0,0))
    holoMaterial(10+Row1, CheckMat)
    Row1++
    Tura++}

if(!Victory&Tura==2 & On & RowSelect==1 & Use & $Use & Przerwa==5 & Row1<6){
    Przerwa=0
    holoColor(10+Row1, vec(255,255,0))
    holoMaterial(10+Row1, CheckMat)
    Row1++
    Tura++}

#  ROW 2 ####################################
if(!Victory&Tura==1 & On & RowSelect==2 & Use & $Use & Przerwa==5 & Row2<6){
    Przerwa=0
    holoColor(20+Row2, vec(255,0,0))
    holoMaterial(20+Row2, CheckMat)
    Row2++
    Tura++}

if(!Victory&Tura==2 & On & RowSelect==2 & Use & $Use & Przerwa==5 & Row2<6){
    Przerwa=0
    holoColor(20+Row2, vec(255,255,0))
    holoMaterial(20+Row2, CheckMat)
    Row2++
    Tura++}

#  ROW 3 ####################################
if(!Victory&Tura==1 & On & RowSelect==3 & Use & $Use & Przerwa==5 & Row3<6){
    Przerwa=0
    holoColor(30+Row3, vec(255,0,0))
    holoMaterial(30+Row3, CheckMat)
    Row3++
    Tura++}

if(!Victory&Tura==2 & On & RowSelect==3 & Use & $Use & Przerwa==5 & Row3<6){
    Przerwa=0
    holoColor(30+Row3, vec(255,255,0))
    holoMaterial(30+Row3, CheckMat)
    Row3++
    Tura++}
    
#  ROW 4 ####################################
if(!Victory&Tura==1 & On & RowSelect==4 & Use & $Use & Przerwa==5 & Row4<6){
    Przerwa=0
    holoColor(40+Row4, vec(255,0,0))
    holoMaterial(40+Row4, CheckMat)
    Row4++
    Tura++}

if(!Victory&Tura==2 & On & RowSelect==4 & Use & $Use & Przerwa==5 & Row4<6){
    Przerwa=0
    holoColor(40+Row4, vec(255,255,0))
    holoMaterial(40+Row4, CheckMat)
    Row4++
    Tura++}
    
#  ROW 5 ####################################
if(!Victory&Tura==1 & On & RowSelect==5 & Use & $Use & Przerwa==5 & Row5<6){
    Przerwa=0
    holoColor(50+Row5, vec(255,0,0))
    holoMaterial(50+Row5, CheckMat)
    Row5++
    Tura++}

if(!Victory&Tura==2 & On & RowSelect==5 & Use & $Use & Przerwa==5 & Row5<6){
    Przerwa=0
    holoColor(50+Row5, vec(255,255,0))
    holoMaterial(50+Row5, CheckMat)
    Row5++
    Tura++}
    
#  ROW 6 ####################################
if(!Victory&Tura==1 & On & RowSelect==6 & Use & $Use & Przerwa==5 & Row6<6){
    Przerwa=0
    holoColor(60+Row6, vec(255,0,0))
    holoMaterial(60+Row6, CheckMat)
    Row6++
    Tura++}

if(!Victory&Tura==2 & On & RowSelect==6 & Use & $Use & Przerwa==5 & Row6<6){
    Przerwa=0
    holoColor(60+Row6, vec(255,255,0))
    holoMaterial(60+Row6, CheckMat)
    Row6++
    Tura++}
    
#  ROW 7 ####################################
if(!Victory&Tura==1 & On & RowSelect==7 & Use & $Use & Przerwa==5 & Row7<6){
    Przerwa=0
    holoColor(70+Row7, vec(255,0,0))
    holoMaterial(70+Row7, CheckMat)
    Row7++
    Tura++}

if(!Victory&Tura==2 & On & RowSelect==7 & Use & $Use & Przerwa==5 & Row7<6){
    Przerwa=0
    holoColor(70+Row7, vec(255,255,0))
    holoMaterial(70+Row7, CheckMat)
    Row7++
    Tura++}
    
    
   
    
    
############################################################
#POINTER

T++
if(T>360){T=0}

if(RowSelect>0){holoPos(998, holoEntity(RowSelect*10+5):toWorld(vec(-17+sin(T*2),0,0))) holoAlpha(998, 255) holoAlpha(937, 255)}else{holoAlpha(998, 0) holoAlpha(937, 0)}
holoAng(998, entity():toWorld(ang(0,90,90)))
if(Tura==1){holoColor(998, vec(254,0,0)) holoColor(340, vec(254,0,0))}
if(Tura==2){holoColor(998, vec(255,254,0)) holoColor(340, vec(255,254,0))}

# TUBE POINTER
if(RowSelect==1){holoPos(937, holoEntity(RowSelect*10+5):toWorld(vec(75-Row1*15,0,0))) if(Row1>5){holoAlpha(937, 0)}}
if(RowSelect==2){holoPos(937, holoEntity(RowSelect*10+5):toWorld(vec(75-Row2*15,0,0))) if(Row2>5){holoAlpha(937, 0)}}
if(RowSelect==3){holoPos(937, holoEntity(RowSelect*10+5):toWorld(vec(75-Row3*15,0,0))) if(Row3>5){holoAlpha(937, 0)}}
if(RowSelect==4){holoPos(937, holoEntity(RowSelect*10+5):toWorld(vec(75-Row4*15,0,0))) if(Row4>5){holoAlpha(937, 0)}}
if(RowSelect==5){holoPos(937, holoEntity(RowSelect*10+5):toWorld(vec(75-Row5*15,0,0))) if(Row5>5){holoAlpha(937, 0)}}
if(RowSelect==6){holoPos(937, holoEntity(RowSelect*10+5):toWorld(vec(75-Row6*15,0,0))) if(Row6>5){holoAlpha(937, 0)}}
if(RowSelect==7){holoPos(937, holoEntity(RowSelect*10+5):toWorld(vec(75-Row7*15,0,0))) if(Row7>5){holoAlpha(937, 0)}}


############################################################
#RESET BY OWNER
if(owner():keyReload() & owner():aimEntity()==entity()){ReloadTimer++}else{ReloadTimer=0}
if(ReloadTimer>50){
    ReloadTimer=0
    Row1 = 0
    Row2 = 0
    Row3 = 0
    Row4 = 0
    Row5 = 0
    Row6 = 0
    Row7 = 0
    
    for(I=10,75){
        holoMaterial(I, DefMat)
        holoColor(I, vec(0,0,0))
        }
    Tura=1
    }

####################################################################
# VICTORY CHECKING

if(RowSelect==1){CheckRow=Row1}
if(RowSelect==2){CheckRow=Row2}
if(RowSelect==3){CheckRow=Row3}
if(RowSelect==4){CheckRow=Row4}
if(RowSelect==5){CheckRow=Row5}
if(RowSelect==6){CheckRow=Row6}
if(RowSelect==7){CheckRow=Row7}

CenterCheck = RowSelect*10+CheckRow-1

if(Use){
################################### HORIZONTAL

#HORIZONTAL [] [] [] [C]
# NOTE 2013 Update: added checking for h
if(holoEntity(CenterCheck):getColor()==holoEntity(CenterCheck-30):getColor() &
holoEntity(CenterCheck):getColor()==holoEntity(CenterCheck-20):getColor() &
holoEntity(CenterCheck):getColor()==holoEntity(CenterCheck-10):getColor()& 
holoEntity(CenterCheck):getColor()!=vec(0,0,0) &
holoEntity(CenterCheck-30)!=holoEntity(1))
{
   # holoColor(CenterCheck, WinColor)
  #  holoColor(CenterCheck-30, WinColor)
   # holoColor(CenterCheck-20, WinColor)
   # holoColor(CenterCheck-10, WinColor)
    
    holoPos(2001, holoEntity(CenterCheck):pos())
    holoPos(2002, holoEntity(CenterCheck-30):pos())
    holoPos(2003, holoEntity(CenterCheck-20):pos())
    holoPos(2004, holoEntity(CenterCheck-10):pos())
    
    Victory=1
    }

#HORIZONTAL [] [] [C] []

if(holoEntity(CenterCheck):getColor()==holoEntity(CenterCheck-20):getColor() &
holoEntity(CenterCheck):getColor()==holoEntity(CenterCheck-10):getColor() &
holoEntity(CenterCheck):getColor()==holoEntity(CenterCheck+10):getColor()& holoEntity(CenterCheck):getColor()!=vec(0,0,0))
{
    #holoColor(CenterCheck, WinColor)
    #holoColor(CenterCheck-20,WinColor)
    #holoColor(CenterCheck-10, WinColor)
    #holoColor(CenterCheck+10, WinColor)
    
    holoPos(2001, holoEntity(CenterCheck):pos())
    holoPos(2002, holoEntity(CenterCheck-20):pos())
    holoPos(2003, holoEntity(CenterCheck-10):pos())
    holoPos(2004, holoEntity(CenterCheck+10):pos())
    
    Victory=1
    }

#HORIZONTAL [] [C] [] []

if(holoEntity(CenterCheck):getColor()==holoEntity(CenterCheck-10):getColor() &
holoEntity(CenterCheck):getColor()==holoEntity(CenterCheck+10):getColor() &
holoEntity(CenterCheck):getColor()==holoEntity(CenterCheck+20):getColor()& holoEntity(CenterCheck):getColor()!=vec(0,0,0))
{
    #holoColor(CenterCheck, WinColor)
   # holoColor(CenterCheck-10, WinColor)
    #holoColor(CenterCheck+10, WinColor)
   # holoColor(CenterCheck+20, WinColor)
    
    holoPos(2001, holoEntity(CenterCheck):pos())
    holoPos(2002, holoEntity(CenterCheck-10):pos())
    holoPos(2003, holoEntity(CenterCheck+10):pos())
    holoPos(2004, holoEntity(CenterCheck+20):pos())
    
    Victory=1
    }

#HORIZONTAL [C] [] [] []

if(holoEntity(CenterCheck):getColor()==holoEntity(CenterCheck+10):getColor() &
holoEntity(CenterCheck):getColor()==holoEntity(CenterCheck+20):getColor() &
holoEntity(CenterCheck):getColor()==holoEntity(CenterCheck+30):getColor()& holoEntity(CenterCheck):getColor()!=vec(0,0,0))
{
    #holoColor(CenterCheck, WinColor)
    #holoColor(CenterCheck+10, WinColor)
    #holoColor(CenterCheck+20, WinColor)
    #holoColor(CenterCheck+30, WinColor)
    
    holoPos(2001, holoEntity(CenterCheck):pos())
    holoPos(2002, holoEntity(CenterCheck+10):pos())
    holoPos(2003, holoEntity(CenterCheck+20):pos())
    holoPos(2004, holoEntity(CenterCheck+30):pos())
    
    Victory=1
    }

################################### VERTICAL

#VERTICAL (up to bottom)

if(holoEntity(CenterCheck):getColor()==holoEntity(CenterCheck-1):getColor() &
holoEntity(CenterCheck):getColor()==holoEntity(CenterCheck-2):getColor() &
holoEntity(CenterCheck):getColor()==holoEntity(CenterCheck-3):getColor() & holoEntity(CenterCheck):getColor()!=vec(0,0,0))
{
    #holoColor(CenterCheck, WinColor)
    #holoColor(CenterCheck-1, WinColor)
    #holoColor(CenterCheck-2, WinColor)
    #holoColor(CenterCheck-3, WinColor)
    
    holoPos(2001, holoEntity(CenterCheck):pos())
    holoPos(2002, holoEntity(CenterCheck-1):pos())
    holoPos(2003, holoEntity(CenterCheck-2):pos())
    holoPos(2004, holoEntity(CenterCheck-3):pos())
    
    Victory=1
    }

################################### DIAGONAL

#DIAGONAL ,[C][][][]^

if(holoEntity(CenterCheck):getColor()==holoEntity(CenterCheck+11):getColor() &
holoEntity(CenterCheck):getColor()==holoEntity(CenterCheck+22):getColor() &
holoEntity(CenterCheck):getColor()==holoEntity(CenterCheck+33):getColor() & holoEntity(CenterCheck):getColor()!=vec(0,0,0))
{
    #holoColor(CenterCheck, WinColor)
    #holoColor(CenterCheck+11, WinColor)
    #holoColor(CenterCheck+22, WinColor)
    #holoColor(CenterCheck+33, WinColor)
    
    holoPos(2001, holoEntity(CenterCheck):pos())
    holoPos(2002, holoEntity(CenterCheck+11):pos())
    holoPos(2003, holoEntity(CenterCheck+22):pos())
    holoPos(2004, holoEntity(CenterCheck+33):pos())
    
    Victory=1
    }

#DIAGONAL ,[][C][][]^

if(holoEntity(CenterCheck):getColor()==holoEntity(CenterCheck-11):getColor() &
holoEntity(CenterCheck):getColor()==holoEntity(CenterCheck+11):getColor() &
holoEntity(CenterCheck):getColor()==holoEntity(CenterCheck+22):getColor() & holoEntity(CenterCheck):getColor()!=vec(0,0,0))
{
    #holoColor(CenterCheck, WinColor)
    #holoColor(CenterCheck-11, WinColor)
    #holoColor(CenterCheck+11, WinColor)
    #holoColor(CenterCheck+22, WinColor)
    
    holoPos(2001, holoEntity(CenterCheck):pos())
    holoPos(2002, holoEntity(CenterCheck-11):pos())
    holoPos(2003, holoEntity(CenterCheck+11):pos())
    holoPos(2004, holoEntity(CenterCheck+22):pos())
    
    Victory=1
    }

#DIAGONAL ,[][][C][]^

if(holoEntity(CenterCheck):getColor()==holoEntity(CenterCheck-22):getColor() &
holoEntity(CenterCheck):getColor()==holoEntity(CenterCheck-11):getColor() &
holoEntity(CenterCheck):getColor()==holoEntity(CenterCheck+11):getColor() & holoEntity(CenterCheck):getColor()!=vec(0,0,0))
{
    #holoColor(CenterCheck, WinColor)
    #holoColor(CenterCheck-22, WinColor)
    #holoColor(CenterCheck-11, WinColor)
    #holoColor(CenterCheck+11, WinColor)
    
    holoPos(2001, holoEntity(CenterCheck):pos())
    holoPos(2002, holoEntity(CenterCheck-22):pos())
    holoPos(2003, holoEntity(CenterCheck-11):pos())
    holoPos(2004, holoEntity(CenterCheck+11):pos())
    
    Victory=1
    }

#DIAGONAL ,[][][][C]^

if(holoEntity(CenterCheck):getColor()==holoEntity(CenterCheck-11):getColor() &
holoEntity(CenterCheck):getColor()==holoEntity(CenterCheck-22):getColor() &
holoEntity(CenterCheck):getColor()==holoEntity(CenterCheck-33):getColor() & holoEntity(CenterCheck):getColor()!=vec(0,0,0))
{
    #holoColor(CenterCheck, WinColor)
    #holoColor(CenterCheck-11, WinColor)
    #holoColor(CenterCheck-22, WinColor)
    #holoColor(CenterCheck-33, WinColor)
    
    holoPos(2001, holoEntity(CenterCheck):pos())
    holoPos(2002, holoEntity(CenterCheck-11):pos())
    holoPos(2003, holoEntity(CenterCheck-22):pos())
    holoPos(2004, holoEntity(CenterCheck-33):pos())
    
    Victory=1
    }



############

#DIAGONAL ^[][][][C],

if(holoEntity(CenterCheck):getColor()==holoEntity(CenterCheck-9):getColor() &
holoEntity(CenterCheck):getColor()==holoEntity(CenterCheck-18):getColor() &
holoEntity(CenterCheck):getColor()==holoEntity(CenterCheck-27):getColor() & holoEntity(CenterCheck):getColor()!=vec(0,0,0))
{
    #holoColor(CenterCheck, WinColor)
    #holoColor(CenterCheck-9, WinColor)
    #holoColor(CenterCheck-18, WinColor)
    #holoColor(CenterCheck-27, WinColor)
    
    holoPos(2001, holoEntity(CenterCheck):pos())
    holoPos(2002, holoEntity(CenterCheck-9):pos())
    holoPos(2003, holoEntity(CenterCheck-18):pos())
    holoPos(2004, holoEntity(CenterCheck-27):pos())
    
    Victory=1
    }

#DIAGONAL ^[][][C][],

if(holoEntity(CenterCheck):getColor()==holoEntity(CenterCheck+9):getColor() &
holoEntity(CenterCheck):getColor()==holoEntity(CenterCheck-9):getColor() &
holoEntity(CenterCheck):getColor()==holoEntity(CenterCheck-18):getColor() & holoEntity(CenterCheck):getColor()!=vec(0,0,0))
{
    #holoColor(CenterCheck, WinColor)
    #holoColor(CenterCheck+9, WinColor)
    #holoColor(CenterCheck-9, WinColor)
    #holoColor(CenterCheck-18, WinColor)
    
    holoPos(2001, holoEntity(CenterCheck):pos())
    holoPos(2002, holoEntity(CenterCheck+9):pos())
    holoPos(2003, holoEntity(CenterCheck-9):pos())
    holoPos(2004, holoEntity(CenterCheck-18):pos())
    
    Victory=1
    }

#DIAGONAL ^[][C][][],

if(holoEntity(CenterCheck):getColor()==holoEntity(CenterCheck-9):getColor() &
holoEntity(CenterCheck):getColor()==holoEntity(CenterCheck+9):getColor() &
holoEntity(CenterCheck):getColor()==holoEntity(CenterCheck+18):getColor() & holoEntity(CenterCheck):getColor()!=vec(0,0,0))
{
    #holoColor(CenterCheck, WinColor)
    #holoColor(CenterCheck-9, WinColor)
    #holoColor(CenterCheck+9, WinColor)
    #holoColor(CenterCheck+18, WinColor)
    
    holoPos(2001, holoEntity(CenterCheck):pos())
    holoPos(2002, holoEntity(CenterCheck-9):pos())
    holoPos(2003, holoEntity(CenterCheck+9):pos())
    holoPos(2004, holoEntity(CenterCheck+18):pos())
    
    Victory=1
    }

#DIAGONAL ^[C][][][],

if(holoEntity(CenterCheck):getColor()==holoEntity(CenterCheck+9):getColor() &
holoEntity(CenterCheck):getColor()==holoEntity(CenterCheck+18):getColor() &
holoEntity(CenterCheck):getColor()==holoEntity(CenterCheck+27):getColor() & holoEntity(CenterCheck):getColor()!=vec(0,0,0))
{
    #holoColor(CenterCheck, WinColor)
    #holoColor(CenterCheck+9, WinColor)
    #holoColor(CenterCheck+18, WinColor)
    #holoColor(CenterCheck+27, WinColor)
    
    holoPos(2001, holoEntity(CenterCheck):pos())
    holoPos(2002, holoEntity(CenterCheck+9):pos())
    holoPos(2003, holoEntity(CenterCheck+18):pos())
    holoPos(2004, holoEntity(CenterCheck+27):pos())
    
    
    Victory=1
    }

}

#PLAYER CHOOSING

findByClass("Player")
findInSphere(entity():pos(), 400)
P=findToArray()

if(Przerwa==5 & !findResult(FR):keyAttack1() & findResult(FR):aimEntity()==entity() & findResult(FR):keyUse() & !Player1 & findResult(FR):pos():distance(entity():pos())<350){Player1 = findResult(FR) holoEntity(5):soundPlay(20,soundDuration("/buttons/button24.wav"),"/buttons/button24.wav") Przerwa=4}elseif(!Player1){FR++}
if(Przerwa==5 & findResult(FR):aimEntity()==entity() & findResult(FR)!=Player1 & findResult(FR):keyUse() &!Player2 & findResult(FR):pos():distance(entity():pos())<350){Player2 = findResult(FR) holoEntity(5):soundPlay(20,soundDuration("/buttons/button24.wav"),"/buttons/button24.wav") Przerwa=4}elseif(!Player2){FR++}




if(FR>P:count()+1){FR=1}

Debug2 = P:count()

# PLAYER INDICATOR

if(Player1){holoPos(5,Player1:pos() + vec(0,0,120+sin(T*2))) holoAlpha(5, 255)}else{holoAlpha(5, 0)}
if(Player2){holoPos(6,Player2:pos() + vec(0,0,120+sin(T*2))) holoAlpha(6, 255)}else{holoAlpha(6, 0)}

if(Player1 & Player1:pos():distance(entity():pos())>350){Player1=noentity() holoEntity(5):soundPlay(20,soundDuration("/buttons/button1.wav"),"/buttons/button1.wav")}
if(Player2 & Player2:pos():distance(entity():pos())>350){Player2=noentity() holoEntity(5):soundPlay(20,soundDuration("/buttons/button1.wav"),"/buttons/button1.wav")}

# DRAW
if(
Row1==6&
Row2==6&
Row3==6&
Row4==6&
Row5==6&
Row6==6&
Row7==6&
Victory==0
){
    Victory=1 
    holoPos(2001, entity():pos()+vec(0,0,5000))
    holoPos(2002, entity():pos()+vec(0,0,5000))
    holoPos(2003, entity():pos()+vec(0,0,5000))
    holoPos(2004, entity():pos()+vec(0,0,5000))
    
    
    
    }

# RESET BY VICTORY 
if(Victory){
    VictoryT++
    holoAlpha(2001, 255)
    holoAlpha(2002, 255)
    holoAlpha(2003, 255)
    holoAlpha(2004, 255)
    holoColor(340, vec(0,255,0))
    }else{
    holoAlpha(2001, 0)
    holoAlpha(2002, 0)
    holoAlpha(2003, 0)
    holoAlpha(2004, 0)
    VictoryT=0}

if(VictoryT>150){
    Row1 = 0
    Row2 = 0
    Row3 = 0
    Row4 = 0
    Row5 = 0
    Row6 = 0
    Row7 = 0
    
    for(I=10,75){
        holoMaterial(I, DefMat)
        holoColor(I, vec(0,0,0))
        }
    Tura=1
    Victory=0
    SoundT=0
    
    TempPlayer=Player1
    
    Player1=Player2
    Player2=TempPlayer
    
    }
#Player1=Player2=owner()

# VICTORY SOUND
if(Victory==1&SoundT<1){
    entity():soundPlay(643,0.5, "friends/message.wav")
    SoundT++
    soundPitch(643, 200)
    }
    
# BOTTOM PLAYER INDICATOR COLORING
#if(!Player1&!Player2){holoColor(340, vec(60,60,60))}
if(Tura==1&!Player1){holoColor(340, vec(60,60,60))}
if(Tura==2&!Player2){holoColor(340, vec(60,60,60))}
#Debug = 



#
if(Player1==Player2){Player2=noentity()}
#if(!Player1){Player1=findPlayerByName("Tek") Player2=Player1}


# SLAY TOURNAMENT CODING
# QUOTE THIS PART IF YOU ARE NOT AN ADMIN ON THE SERVER

#if(Vi)

