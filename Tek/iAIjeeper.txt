@name iAIjeeper
@model models/buggy.mdl
@outputs Debug Jeep:entity [R1 R2 R3 R4]:ranger Force D1 D2 D3 D4 Bearing VelX VelY

runOnTick(1)


if(first()){
    
    for(I=1,60){
        holoCreate(I)
        holoPos(I, entity():boxCenterW()+vec(0,0,80)) # delete later
        holoColor(I, randvec(40,255))
        #holoDisableShading(I, 1)
    }
    
    Jeep = entity()
    Force =640
    
    #REAR-RIGHT WHEEL AXIS
    holoPos(1, Jeep:toWorld(vec(50,-93,28)))
    holoAng(1, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(1, vec(10,10,10))
    #holoMaterial(1, "effects/flashlight/gradient")
    #holoModel(1, "")
    holoColor(1, vec(255,255,255))
    holoParent(1, entity())
    
    #REAR-RIGHT WHEEL AXIS
    holoPos(2, Jeep:toWorld(vec(-50,-93,28)))
    holoAng(2, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(2, vec(10,10,10))
    #holoMaterial(2, "effects/flashlight/gradient")
    #holoModel(2, "")
    holoColor(2, vec(255,255,255))
    holoParent(2, entity())
    
    #FRONT-RIGHT WHEEL AXIS
    holoPos(3, Jeep:toWorld(vec(50,43,28)))
    holoAng(3, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(3, vec(10,10,10))
    #holoMaterial(3, "effects/flashlight/gradient")
    #holoModel(3, "")
    holoColor(3, vec(255,255,255))
    holoParent(3, entity())
    
    #FRONT-LEFT WHEEL AXIS
    holoPos(4, Jeep:toWorld(vec(-50,43,28)))
    holoAng(4, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(4, vec(10,10,10))
    #holoMaterial(4, "effects/flashlight/gradient")
    #holoModel(4, "")
    holoColor(4, vec(255,255,255))
    holoParent(4, entity())
    
}

R1 = rangerOffset(30, holoEntity(1):pos(), -entity():up())
rangerFilter(entity())
R2 = rangerOffset(30, holoEntity(2):pos(), -entity():up())
rangerFilter(entity())
R3 = rangerOffset(30, holoEntity(3):pos(), -entity():up())
rangerFilter(entity())
R4 = rangerOffset(30, holoEntity(4):pos(), -entity():up())
rangerFilter(entity())

D1 = R1:distance()
D2 = R2:distance()
D3 = R3:distance()
D4 = R4:distance()

if(R1:hit()&R1:distance()<25){entity():applyOffsetForce(vec(0,0,Force+(D1+$D1*2)*10),(R1:position()+vec(0,0,-20)))}
if(R2:hit()&R2:distance()<25){entity():applyOffsetForce(vec(0,0,Force+(D2+$D2*2)*10),(R2:position()+vec(0,0,-20)))}
if(R3:hit()&R3:distance()<25){entity():applyOffsetForce(vec(0,0,Force+(D3+$D3*2)*10),(R3:position()+vec(0,0,-20)))}
if(R4:hit()&R4:distance()<25){entity():applyOffsetForce(vec(0,0,Force+(D4+$D4*2)*10),(R4:position()+vec(0,0,-20)))}

holoPos(5, R1:position())
