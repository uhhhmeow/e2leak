@name testMaterialTesting
@inputs Wirelink:wirelink
@outputs Debug T When Material:string
@persist 
@trigger 

runOnTick(1)
runOnChat(1)
T++

if(first()){
    holoCreate(1)
    holoCreate(2)
    holoCreate(3)
    holoCreate(4):setTrails(15,30,1,"trails/laser",vec(255,255,255),255)
    holoCreate(5):setTrails(15,30,1,"trails/laser",vec(255,255,255),255)
    


    holoPos(1, entity():toWorld(vec(50,0,20)))
    holoPos(2, entity():toWorld(vec(0,0,20)))
    holoPos(3, entity():toWorld(vec(-50,0,20)))
    #holoPos(1, entity():toWorld(vec(0,0,0)))
    holoScale(4, vec(0.1,0.1,0.1))
    holoColor(4, vec(255,0,255))
    holoScale(5, vec(0.1,0.1,0.1))
    holoColor(5, vec(0,255,255))
}

    holoPos(4, entity():toWorld(vec(sin(T*2)*16,0,50+cos(T*2)*16)))
    holoPos(5, entity():toWorld(vec(sin(T*2)*16,0,65+cos(T*2)*16)))

When = owner():lastSaidWhen()


if(owner():lastSaid() & changed(When)){
    Material=owner():lastSaid()
    
    holoColor(1, vec(255))
    holoColor(2, vec(255))
    holoColor(3, vec(255))
    holoColor(4, vec(255))
    #holoColor(5, vec(255))
    
    holoMaterial(1, Material)
    holoMaterial(2, Material)
    holoMaterial(3, Material)
    holoMaterial(4, Material)
    holoEntity(5):setTrails(15,30,1,Material,vec(255,255,255),255)
    }
    
    if(owner():keyReload()){       
    holoColor(1, vec(255,0,0))
    holoColor(2, vec(255,0,0))
    holoColor(3, vec(255,0,0))
    holoColor(4, vec(255,0,0))
    holoEntity(5):setTrails(15,30,1,Material,vec(255,0,0),255)
    }
    
    if(owner():keyUse()){       
    holoColor(1, vec(255))
    holoColor(2, vec(255))
    holoColor(3, vec(255))
    holoColor(4, vec(255))
    holoEntity(5):setTrails(15,30,1,Material,vec(255,255,255),255)
    }


Wirelink:egpBox(1, vec2(256,256), vec2(512,512))
Wirelink:egpMaterial(1, Material)
