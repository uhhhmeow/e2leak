@name radar
@inputs 
@outputs FoundResult:entity R:array Debug Range
@persist 
@model models/hunter/tubes/circle2x2.mdl

runOnTick(1)

Range = 1000

findByClass("player")
R=findToArray()


if(first()){
    for(I=0,21){
        holoCreate(I)
    }
    
    entity():setAlpha(0)
    holoAlpha(0, 0)
    holoAlpha(5, 0)
    holoAlpha(4, 0)
    
    
    #EKRAN - 1
    holoPos(1, entity():toWorld(vec(0,0,0)))
    holoMaterial(1, "vgui/screens/radar")
    holoScaleUnits(1, entity():boxSize())
    holoModel(1, "hq_cylinder")
    holoParent(1, entity())
    
    #KADLUB - 2
    holoPos(2, entity():toWorld(vec(0,0,-0.2)))
    holoMaterial(2, "phoenix_storms/black_chrome")
    holoScaleUnits(2, entity():boxSize()+vec(0.5,0.5,0))
    holoModel(2, "hq_cylinder")
    holoParent(2, entity())
    
    #OTOCZKA KADLUBA - 3
    holoPos(3, entity():toWorld(vec(0,0,0.5)))
    holoMaterial(3, "phoenix_storms/black_chrome")
    holoScaleUnits(3, entity():boxSize()+vec(0.5,0.5,0))
    holoModel(3, "hq_tube_thin")
    holoParent(3, entity())
    
    #HOLOGRAM - 4 not used
    
    #HOLOGRAM 5 - GUIDER
    
    #HOLOGRAM GRACZA 1#
    holoPos(6, entity():toWorld(vec(0,0,0.5)))
   # holoMaterial(6, "models/roller/rollermine_glow")
    holoModel(6, "hq_cylinder")
    holoParent(6, entity())
    holoScaleUnits(6, vec(3,3,4))
    
    #HOLOGRAM GRACZA 2#
    holoPos(7, entity():toWorld(vec(0,0,0.5)))
   # holoMaterial(7, "models/roller/rollermine_glow")
    holoModel(7, "hq_cylinder")
    holoParent(7, entity())
    holoScaleUnits(7, vec(3,3,4))
    
    #HOLOGRAM GRACZA 2#
    holoPos(8, entity():toWorld(vec(0,0,0.5)))
   # holoMaterial(7, "models/roller/rollermine_glow")
    holoModel(8, "hq_cylinder")
    holoParent(8, entity())
    holoScaleUnits(8, vec(3,3,4))
    
    #HOLOGRAM GRACZA 2#
    holoPos(9, entity():toWorld(vec(0,0,0.5)))
   # holoMaterial(9, "models/roller/rollermine_glow")
    holoModel(9, "hq_cylinder")
    holoParent(9, entity())
    holoScaleUnits(9, vec(3,3,4))
    
    #HOLOGRAM GRACZA 2#
    holoPos(10, entity():toWorld(vec(0,0,0.5)))
   # holoMaterial(10, "models/roller/rollermine_glow")
    holoModel(10, "hq_cylinder")
    holoParent(10, entity())
    holoScaleUnits(10, vec(3,3,4))

    #HOLOGRAM GRACZA 2#
    holoPos(11, entity():toWorld(vec(0,0,0.5)))
   # holoMaterial(10, "models/roller/rollermine_glow")
    holoModel(11, "hq_cylinder")
    holoParent(11, entity())
    holoScaleUnits(11, vec(3,3,4))

    #HOLOGRAM GRACZA 2#
    holoPos(12, entity():toWorld(vec(0,0,0.5)))
   # holoMaterial(11, "models/roller/rollermine_glow")
    holoModel(12, "hq_cylinder")
    holoParent(12, entity())
    holoScaleUnits(12, vec(3,3,4))
    
    #HOLOGRAM GRACZA 2#
    holoPos(13, entity():toWorld(vec(0,0,0.5)))
   # holoMaterial(10, "models/roller/rollermine_glow")
    holoModel(13, "hq_cylinder")
    holoParent(13, entity())
    holoScaleUnits(13, vec(3,3,4))
    
    #HOLOGRAM GRACZA 2#
    holoPos(14, entity():toWorld(vec(0,0,0.5)))
   # holoMaterial(10, "models/roller/rollermine_glow")
    holoModel(14, "hq_cylinder")
    holoParent(14, entity())
    holoScaleUnits(14, vec(3,3,4))
    
    #HOLOGRAM GRACZA 2#
    holoPos(15, entity():toWorld(vec(0,0,0.5)))
   # holoMaterial(10, "models/roller/rollermine_glow")
    holoModel(15, "hq_cylinder")
    holoParent(15, entity())
    holoScaleUnits(15, vec(3,3,4))
    
    #HOLOGRAM GRACZA 2#
    holoPos(16, entity():toWorld(vec(0,0,0.5)))
   # holoMaterial(10, "models/roller/rollermine_glow")
    holoModel(16, "hq_cylinder")
    holoParent(16, entity())
    holoScaleUnits(16, vec(3,3,4))
    
    #HOLOGRAM GRACZA 2#
    holoPos(17, entity():toWorld(vec(0,0,0.5)))
   # holoMaterial(10, "models/roller/rollermine_glow")
    holoModel(17, "hq_cylinder")
    holoParent(17, entity())
    holoScaleUnits(17, vec(3,3,4))
    
    #HOLOGRAM GRACZA 2#
    holoPos(18, entity():toWorld(vec(0,0,0.5)))
   # holoMaterial(10, "models/roller/rollermine_glow")
    holoModel(18, "hq_cylinder")
    holoParent(18, entity())
    holoScaleUnits(18, vec(3,3,4))
     
    #HOLOGRAM GRACZA 2#
    holoPos(19, entity():toWorld(vec(0,0,0.5)))
   # holoMaterial(10, "models/roller/rollermine_glow")
    holoModel(19, "hq_cylinder")
    holoParent(19, entity())
    holoScaleUnits(19, vec(3,3,4))
    
    #HOLOGRAM GRACZA 2#
    holoPos(20, entity():toWorld(vec(0,0,0.5)))
   # holoMaterial(10, "models/roller/rollermine_glow")
    holoModel(20, "hq_cylinder")
    holoParent(20, entity())
    holoScaleUnits(20, vec(3,3,4))

     #HOLOGRAM GRACZA 2#
    holoPos(21, entity():toWorld(vec(0,0,0.5)))
   # holoMaterial(10, "models/roller/rollermine_glow")
    holoModel(21, "hq_cylinder")
    holoParent(21, entity())
    holoScaleUnits(21, vec(3,3,4))
}

# HOLO GUIDING

holoAng(0, ang(0,entity():angles():yaw(),0))
holoPos(0, entity():pos())

holoAng(5, ang(entity():angles():pitch(),holoEntity(0):angles():yaw(),entity():angles():roll()))
holoPos(5, entity():pos())

# HOLO COLORING

for(I=1,16){holoColor(I+5, teamColor(findResult(I):team()))}

#POS HOLOGRAMOW
holoPos(6, holoEntity(5):toWorld(holoEntity(0):toLocal(vec(findResult(1):pos():x(),findResult(1):pos():y(),entity():pos():z()+5))/Range) )
holoPos(7, holoEntity(5):toWorld(holoEntity(0):toLocal(vec(findResult(2):pos():x(),findResult(2):pos():y(),entity():pos():z()+5))/Range) )
holoPos(8, holoEntity(5):toWorld(holoEntity(0):toLocal(vec(findResult(3):pos():x(),findResult(3):pos():y(),entity():pos():z()+5))/Range) )
holoPos(9, holoEntity(5):toWorld(holoEntity(0):toLocal(vec(findResult(4):pos():x(),findResult(4):pos():y(),entity():pos():z()+5))/Range) )
holoPos(10, holoEntity(5):toWorld(holoEntity(0):toLocal(vec(findResult(5):pos():x(),findResult(5):pos():y(),entity():pos():z()+5))/Range) )
holoPos(11, holoEntity(5):toWorld(holoEntity(0):toLocal(vec(findResult(6):pos():x(),findResult(6):pos():y(),entity():pos():z()+5))/Range) )
holoPos(12, holoEntity(5):toWorld(holoEntity(0):toLocal(vec(findResult(7):pos():x(),findResult(7):pos():y(),entity():pos():z()+5))/Range) )
holoPos(13, holoEntity(5):toWorld(holoEntity(0):toLocal(vec(findResult(8):pos():x(),findResult(8):pos():y(),entity():pos():z()+5))/Range) )
holoPos(14, holoEntity(5):toWorld(holoEntity(0):toLocal(vec(findResult(9):pos():x(),findResult(9):pos():y(),entity():pos():z()+5))/Range) )
holoPos(15, holoEntity(5):toWorld(holoEntity(0):toLocal(vec(findResult(10):pos():x(),findResult(10):pos():y(),entity():pos():z()+5))/Range) )
holoPos(16, holoEntity(5):toWorld(holoEntity(0):toLocal(vec(findResult(11):pos():x(),findResult(11):pos():y(),entity():pos():z()+5))/Range) )
holoPos(17, holoEntity(5):toWorld(holoEntity(0):toLocal(vec(findResult(12):pos():x(),findResult(12):pos():y(),entity():pos():z()+5))/Range) )
holoPos(18, holoEntity(5):toWorld(holoEntity(0):toLocal(vec(findResult(13):pos():x(),findResult(13):pos():y(),entity():pos():z()+5))/Range) )
holoPos(19, holoEntity(5):toWorld(holoEntity(0):toLocal(vec(findResult(14):pos():x(),findResult(14):pos():y(),entity():pos():z()+5))/Range) )
holoPos(20, holoEntity(5):toWorld(holoEntity(0):toLocal(vec(findResult(15):pos():x(),findResult(15):pos():y(),entity():pos():z()+5))/Range) )
holoPos(21, holoEntity(5):toWorld(holoEntity(0):toLocal(vec(findResult(16):pos():x(),findResult(16):pos():y(),entity():pos():z()+5))/Range) )




#ALPHA HOLOGRAMOW
for(I=6,21){
    if(holoEntity(I):pos():distance(entity():pos())>44 | findResult(I-5):health()<=0){holoEntity(I):setAlpha(0)}else{holoEntity(I):setAlpha(255)}
    
}

if(holoEntity(6):pos():distance(entity():pos())>44){holoAlpha(6, 0)}else{holoAlpha(6, 255)}

holoEntity(0):pos():distance(findResult(1):pos())

Debug = holoEntity(6):pos():distance(entity():pos())

#holoEntity(0):toLocal(vec(findResult(1):pos():x(),findResult(1):pos():y(),findResult(1):pos():z())/-60)
