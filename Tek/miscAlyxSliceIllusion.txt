@name miscAlyxSliceIllusion - idea of The Dance. (dot intended)
@inputs 
@outputs Current Rdy Mats:array MatIdx CurrentAnimation
@persist 
@trigger 

runOnTick(1)

if(first()){
    
 Current=1   
 MatIdx=2
    
}

Mats[1,string]=""
Mats[2,string]="debug/env_cubemap_model_translucent"
Mats[3,string]="shadertest/cubemapdemo"
Mats[4, string]="phoenix_storms/white_fps"
Mats[5, string]="vgui/screens/radar"
Mats[6, string]="models/wireframe"

if(holoCanCreate() & !holoEntity(180) & !Rdy){
    
    holoCreate(Current)
    #holoScale(Current,vec(1))
    #holoColor(Current, hsv2rgb(vec(Current*2,1,1)))
    #holoMaterial(Current, "debug/env_cubemap_model_translucent")
    
    holoModel(Current, "models/Combine_Super_Soldier.mdl")
    #holoAlpha(Current,random(100,255))
    
    holoClipEnabled(Current,1,1)
    holoClipEnabled(Current,2,1)
    
    holoClip(Current,1,entity():pos()+vec(0,0,(Current+1)/2.3),-entity():up(),1)
    holoClip(Current,2,entity():pos()+vec(0,0,(Current)/2.3),entity():up(),1)
    
    #holoAnim(Current,42)
    
    Current++
    
    CurrentAnimation=1
    
}



if(holoEntity(180) & !Rdy){
    
    Current=1
    Rdy=1
    
    
    }

if(Rdy){
    if(MatIdx>Mats:count()){MatIdx=1}
    #holoMaterial(Current, Mats[MatIdx,string])
    holoAnim(Current,CurrentAnimation)
    Current+=1
    
    #36 - wobble... kinda weird
    #100 - stands to side, wobbles
    
    if(Current>180){Current=0 MatIdx++ CurrentAnimation++}
    
    }

