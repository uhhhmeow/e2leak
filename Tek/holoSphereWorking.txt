@name holoSphereWorking
@inputs Button
@outputs X Y Z FadeRate HoloON
@persist I Radius

interval(1)

if(first()){holoCreate(1):setTrails(5,5,15,"trails/laser",vec(255),255)}
Radius=200

if(Button){
FadeRate=1
I=I+4
HoloON=1

X=(cos(I)*sin(I/50))*Radius
Y=(sin(I)*sin(I/50))*Radius
Z=cos(I/50)*Radius

if(Z==-Radius){
stoptimer("interval")
}
}

if(!Button){
FadeRate=10000
X=0
Y=0
Z=0
I=0
HoloON=0
}
holoPos(1, entity():toWorld(vec(X,Y,Z)))
