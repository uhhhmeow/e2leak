@name miscIllusion1
@outputs Use Start Timer Alpha

runOnTick(1)

if(first()){
    
    for(I=1,32){
        holoCreate(I)
        holoPos(I, entity():boxCenterW()+vec(0,0,120)) # delete later
        holoColor(I, randvec(40,255))
        #holoDisableShading(I, 1)
    }
    
    holoPos(1, entity():toWorld(vec(0,0,60)))
    holoColor(1, vec(255))
    holoParent(1, entity())
    holoScaleUnits(1, vec(57,1,25))
    
    for(I=2,30){
        holoScaleUnits(I,vec(1,1,25))
        holoPos(I, entity():toWorld(vec((I*2)-32,-0.2,60)))
        holoColor(I, vec(0))
        
    }
    
    holoColor(32, vec(255,255,0))
    holoScaleUnits(32, vec(4,1,2))
    
    holoColor(31, vec(0,0,200))
    holoScaleUnits(31, vec(4,1,2))
    
    
    Alpha=255
    
    
    
    
    
    
    
}
holoPos(31, entity():toWorld(vec(-30+Timer,-0.3,55)))
holoPos(32, entity():toWorld(vec(-30+Timer,-0.3,65)))


Use=owner():keyUse()

if(Use&$Use){Start=1}
if(Start){Timer+=0.05}else{Timer=0}

if(Timer>25&Timer<26){
for(I=2,30){
    holoAlpha(I, 0)
}
}

if(Timer>30&Timer<31){
for(I=2,30){
    holoAlpha(I, 255)
}
}

if(Timer>35&Timer<36){
for(I=2,30){
    holoAlpha(I, 0)
}
}

if(Timer>45&Timer<46){
for(I=2,30){
    holoAlpha(I, 255)
}
}

if(Timer>50){Start=0 Timer=0}
#print(4, Timer:toString())
