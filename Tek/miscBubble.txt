@name miscBubble
@model models/hunter/plates/plate025x025.mdl 
@outputs Master:entity Use Ons HudCursorLocal:vector Mode SubMode Props:array Lag OriginPos:vector Vec:vector CurField Sizes:array GravityFix Gravity Nearest:vector NearestLocal:vector Spawned


# SETTINGS

# FIXES TOO LOW GRAVITY ON SOME SERVERS
GravityFix = 0

if(Spawned<80){Spawned++}

Master=owner()

if(first()){
    if(GravityFix==1){Gravity=9.0131}else{Gravity=4.5065}
    CurField=37
    
    runOnTick(1)
    entity():setAlpha(0)
    
    for(I=1,54){
        holoCreate(I)
        holoPos(I, entity():boxCenterW()+vec(0,0,80)) # delete later
        holoColor(I, randvec(40,255))
        holoDisableShading(I, 1)
    }
    
    
    
    # CORE HOLOGRAM FOR POINT-LINE INTERSECTION
    holoPos(1, entity():boxCenterW()+entity():up()*1.5)
    holoAng(1, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(1, vec(1))
    holoParent(1, entity())
    holoAlpha(1, 0)
    
    CoreHolo=holoEntity(1)
    
    # HULL + CORE HULL
    holoPos(2, entity():boxCenterW())
    holoAng(2, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(2, entity():boxSize()-vec(0,0,1))
    holoMaterial(2, "phoenix_storms/black_chrome")
    #holoModel(2, "")
    holoColor(2, vec(255,255,255))
    holoParent(2, entity())
    
    CoreHull = holoEntity(2)
    
    # SCREEN
    holoPos(3, CoreHull:toWorld(vec(0,0,0.2)))
    holoAng(3, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(3, entity():boxSize()-vec(0.5,0.5,1))
    #holoMaterial(3, "effects/flashlight/gradient")
    #holoModel(3, "")
    holoColor(3, vec(180))
    holoParent(3, entity())

    # BUTTON 1 OUTLINE - NO GRAV
    holoPos(4, CoreHull:toWorld(vec(2.9,-2.9,1)))
    holoAng(4, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(4, vec(5.5,5.5,1))
    #holoMaterial(4, "bull/various/subwoofer")
    #holoModel(4, "")
    holoColor(4, vec(0))
    holoParent(4, entity())
    
    # BUTTON 1 INLINE - NO GRAV
    holoPos(5, CoreHull:toWorld(vec(2.9,-2.9,1.1)))
    holoAng(5, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(5, vec(5.25,5.25,1))
    #holoMaterial(5, "effects/flashlight/gradient")
    #holoModel(5, "")
    holoColor(5, vec(255,255,255))
    holoParent(5, entity())
    
    # BUTTON 1 ICON - BOX OUTLINE
    holoPos(6, CoreHull:toWorld(vec(2.9,-3.3,1.15)))
    holoAng(6, entity():toWorld(ang(0,35,0)))
    holoScaleUnits(6, vec(2.25,2.25,1))
    #holoMaterial(6, "effects/flashlight/gradient")
    #holoModel(6, "")
    holoColor(6, vec(0))
    holoParent(6, entity())
    
    # BUTTON 1 ICON - ARROW TIP OUTLINE
    holoPos(7, CoreHull:toWorld(vec(2.9,-2.7,1.15)))
    holoAng(7, entity():toWorld(ang(0,0,90)))
    holoScaleUnits(7, vec(2,1.1,1.25))
    #holoMaterial(7, "effects/flashlight/gradient")
    holoModel(7, "prism")
    holoColor(7, vec(0))
    holoParent(7, entity())
    
    # BUTTON 1 ICON - ARROW TAIL OUTLINE
    holoPos(8, CoreHull:toWorld(vec(2.9,-1.7,1.15)))
    holoAng(8, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(8, vec(1,1,1.1))
    #holoMaterial(8, "effects/flashlight/gradient")
    #holoModel(8, "prism")
    holoColor(8, vec(0))
    holoParent(8, entity())
    
    # BUTTON 1 ICON - BOX INLINE
    holoPos(9, CoreHull:toWorld(vec(2.9,-3.3,1.15)))
    holoAng(9, entity():toWorld(ang(0,35,0)))
    holoScaleUnits(9, vec(2.12,2.12,1.05))
    #holoMaterial(9, "effects/flashlight/gradient")
    #holoModel(9, "")
    holoColor(9, vec(255))
    holoParent(9, entity())
    
    # BUTTON 1 ICON - ARROW TIP INLINE
    holoPos(10, CoreHull:toWorld(vec(2.9,-2.67,1.15)))
    holoAng(10, entity():toWorld(ang(0,0,90)))
    holoScaleUnits(10, vec(1.7,1.2,1.05))
    #holoMaterial(10, "effects/flashlight/gradient")
    holoModel(10, "prism")
    holoColor(10, vec(255))
    holoParent(10, entity())
    
    # BUTTON 1 ICON - ARROW TAIL OUTLINE
    holoPos(11, CoreHull:toWorld(vec(2.9,-1.72,1.15)))
    holoAng(11, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(11, vec(0.85,0.9,1.2))
    #holoMaterial(11, "effects/flashlight/gradient")
    #holoModel(11, "prism")
    holoColor(11, vec(255))
    holoParent(11, entity())
    
    # BUTTON 2 OUTLINE - REPEL
    holoPos(12, CoreHull:toWorld(vec(-2.9,-2.9,1)))
    holoAng(12, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(12, vec(5.5,5.5,1))
    #holoMaterial(12, "bull/various/subwoofer")
    #holoModel(12, "")
    holoColor(12, vec(0))
    holoParent(12, entity())
    
    # BUTTON 2 INLINE - REPEL
    holoPos(13, CoreHull:toWorld(vec(-2.9,-2.9,1.1)))
    holoAng(13, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(13, vec(5.25,5.25,1))
    #holoMaterial(13, "effects/flashlight/gradient")
    #holoModel(13, "")
    holoColor(13, vec(255,255,255))
    holoParent(13, entity())
    
    # BUTTON 2 ICON - PRISM R OUTLINE 1
    holoPos(14, CoreHull:toWorld(vec(-3.9,-2.9,1.1)))
    holoAng(14, entity():toWorld(ang(0,-90,90)))
    holoScaleUnits(14, vec(3,1.1,1.8))
    #holoMaterial(14, "effects/flashlight/gradient")
    holoModel(14, "prism")
    holoColor(14, vec(0))
    holoParent(14, entity())
    
    # BUTTON 2 ICON - PRISM L OUTLINE 1
    holoPos(15, CoreHull:toWorld(vec(-1.9,-2.9,1.1)))
    holoAng(15, entity():toWorld(ang(0,90,90)))
    holoScaleUnits(15, vec(3,1.1,1.8))
    #holoMaterial(15, "effects/flashlight/gradient")
    holoModel(15, "prism")
    holoColor(15, vec(0))
    holoParent(15, entity())
    
    # BUTTON 2 ICON - PRISM R OUTLINE 1
    holoPos(16, CoreHull:toWorld(vec(-3.9,-2.9,1.1)))
    holoAng(16, entity():toWorld(ang(0,-90,90)))
    holoScaleUnits(16, vec(2.6,1.15,1.6))
    #holoMaterial(16, "effects/flashlight/gradient")
    holoModel(16, "prism")
    holoColor(16, vec(255))
    holoParent(16, entity())
    
    # BUTTON 2 ICON - PRISM R OUTLINE 1
    holoPos(17, CoreHull:toWorld(vec(-1.9,-2.9,1.1)))
    holoAng(17, entity():toWorld(ang(0,90,90)))
    holoScaleUnits(17, vec(2.6,1.15,1.6))
    #holoMaterial(17, "effects/flashlight/gradient")
    holoModel(17, "prism")
    holoColor(17, vec(255))
    holoParent(17, entity())
    
    # BUTTON 2 ICON - ORB OUTLINE
    holoPos(18, CoreHull:toWorld(vec(-2.9,-2.9,1.1)))
    holoAng(18, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(18, vec(1.2,1.2,1.2))
    #holoMaterial(18, "effects/flashlight/gradient")
    holoModel(18, "hq_cylinder")
    holoColor(18, vec(0))
    holoParent(18, entity())
    
    # BUTTON 2 ICON - ORB OUTLINE
    holoPos(19, CoreHull:toWorld(vec(-2.9,-2.9,1.1)))
    holoAng(19, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(19, vec(1.05,1.05,1.25))
    #holoMaterial(19, "effects/flashlight/gradient")
    holoModel(19, "hq_cylinder")
    holoColor(19, vec(255))
    holoParent(19, entity())
    
    # BUTTON 3 OUTLINE - ATTRACT
    holoPos(20, CoreHull:toWorld(vec(2.9,2.9,1)))
    holoAng(20, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(20, vec(5.5,5.5,1))
    #holoMaterial(20, "bull/various/subwoofer")
    #holoModel(20, "")
    holoColor(20, vec(0))
    holoParent(20, entity())
    
    # BUTTON 3 INLINE - ATTRACT
    holoPos(21, CoreHull:toWorld(vec(2.9,2.9,1.1)))
    holoAng(21, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(21, vec(5.25,5.25,1))
    #holoMaterial(21, "effects/flashlight/gradient")
    #holoModel(21, "")
    holoColor(21, vec(255,255,255))
    holoParent(21, entity())
    
    # BUTTON 3 ICON - PRISM R OUTLINE 1
    holoPos(22, CoreHull:toWorld(vec(3.9,2.9,1.1)))
    holoAng(22, entity():toWorld(ang(0,-90,90)))
    holoScaleUnits(22, vec(3,1.1,1.8))
    #holoMaterial(22, "effects/flashlight/gradient")
    holoModel(22, "prism")
    holoColor(22, vec(0))
    holoParent(22, entity())
    
    # BUTTON 3 ICON - PRISM L OUTLINE 1
    holoPos(23, CoreHull:toWorld(vec(1.9,2.9,1.1)))
    holoAng(23, entity():toWorld(ang(0,90,90)))
    holoScaleUnits(23, vec(3,1.1,1.8))
    #holoMaterial(23, "effects/flashlight/gradient")
    holoModel(23, "prism")
    holoColor(23, vec(0))
    holoParent(23, entity())
    
    # BUTTON 3 ICON - PRISM R OUTLINE 1
    holoPos(24, CoreHull:toWorld(vec(3.9,2.9,1.1)))
    holoAng(24, entity():toWorld(ang(0,-90,90)))
    holoScaleUnits(24, vec(2.6,1.15,1.6))
    #holoMaterial(24, "effects/flashlight/gradient")
    holoModel(24, "prism")
    holoColor(24, vec(255))
    holoParent(24, entity())
    
    # BUTTON 3 ICON - PRISM R OUTLINE 1
    holoPos(25, CoreHull:toWorld(vec(1.9,2.9,1.1)))
    holoAng(25, entity():toWorld(ang(0,90,90)))
    holoScaleUnits(25, vec(2.6,1.15,1.6))
    #holoMaterial(25, "effects/flashlight/gradient")
    holoModel(25, "prism")
    holoColor(25, vec(255))
    holoParent(25, entity())
    
    # BUTTON 3 ICON - ORB OUTLINE
    holoPos(26, CoreHull:toWorld(vec(2.9,2.9,1.1)))
    holoAng(26, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(26, vec(1.2,1.2,1.2))
    #holoMaterial(26, "effects/flashlight/gradient")
    holoModel(26, "hq_cylinder")
    holoColor(26, vec(0))
    holoParent(26, entity())
    
    # BUTTON 3 ICON - ORB OUTLINE
    holoPos(27, CoreHull:toWorld(vec(2.9,2.9,1.1)))
    holoAng(27, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(27, vec(1.05,1.05,1.25))
    #holoMaterial(27, "effects/flashlight/gradient")
    holoModel(27, "hq_cylinder")
    holoColor(27, vec(255))
    holoParent(27, entity())
    
    # BUTTON 4 OUTLINE - DELETE
    holoPos(28, CoreHull:toWorld(vec(-2.9,2.9,1)))
    holoAng(28, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(28, vec(5.5,5.5,1))
    #holoMaterial(28, "bull/various/subwoofer")
    #holoModel(28, "")
    holoColor(28, vec(0))
    holoParent(28, entity())
    
    # BUTTON 4 INLINE - DELETE
    holoPos(29, CoreHull:toWorld(vec(-2.9,2.9,1.1)))
    holoAng(29, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(29, vec(5.25,5.25,1))
    #holoMaterial(29, "effects/flashlight/gradient")
    #holoModel(29, "")
    holoColor(29, vec(255,255,255))
    holoParent(29, entity())
    
    # BUTTON 4 ICON - BOX 1 OUTLINE
    holoPos(30, CoreHull:toWorld(vec(-2.9,2.9,1)))
    holoAng(30, entity():toWorld(ang(0,45,0)))
    holoScaleUnits(30, vec(1.0,3.5,1.3))
    #holoMaterial(30, "bull/various/subwoofer")
    #holoModel(30, "")
    holoColor(30, vec(0))
    holoParent(30, entity())
    
    # BUTTON 4 ICON - BOX 2 OUTLINE
    holoPos(31, CoreHull:toWorld(vec(-2.9,2.9,1)))
    holoAng(31, entity():toWorld(ang(0,-45,0)))
    holoScaleUnits(31, vec(1.0,3.5,1.3))
    #holoMaterial(31, "bull/various/subwoofer")
    #holoModel(31, "")
    holoColor(31, vec(0))
    holoParent(31, entity())
    
    # BUTTON 4 ICON - BOX 1 OUTLINE
    holoPos(32, CoreHull:toWorld(vec(-2.9,2.9,1)))
    holoAng(32, entity():toWorld(ang(0,45,0)))
    holoScaleUnits(32, vec(0.8,3.3,1.35))
    #holoMaterial(32, "bull/various/subwoofer")   
    #holoModel(32, "")
    holoColor(32, vec(255))
    holoParent(32, entity())
    
    # BUTTON 4 ICON - BOX 2 OUTLINE
    holoPos(33, CoreHull:toWorld(vec(-2.9,2.9,1)))
    holoAng(33, entity():toWorld(ang(0,-45,0)))
    holoScaleUnits(33, vec(0.8,3.3,1.35))
    #holoMaterial(33, "bull/various/subwoofer")
    #holoModel(33, "")
    holoColor(33, vec(255))
    holoParent(33, entity())
    
    ###############################
    ######### HOLOGRAM 34 IS SPAWNER
    holoAng(34, ang(0,0,0))
    holoModel(34, "hq_sphere")
    holoScaleUnits(34,vec(4,4,1))
    holoColor(34, vec(255))
    
    # HOLOGRAM 35 - DISTANCE LINE
    
    # HOLOGRAM 36 - DISTANCE CYLINDER
    holoAng(36, ang(0,0,0))
    holoModel(36, "hq_tube_thin")
    #holoScaleUnits(36,vec(4,4,1))
    holoColor(36, vec(255))
    
    # HOLOGRAMS 37 UP TO 42 ARE FIELD HOLOGRAMS
    for(I=37,42){
        holoMaterial(I, "models/wireframe")
        holoModel(I, "hq_sphere")
        holoAng(I, ang(0,0,0))
        holoAlpha(I, 0)
        holoColor(I, vec(0))
        
        holoMaterial(I+6, "debug/debugdrawflat")
        holoModel(I+6, "hq_sphere")
        holoAng(I+6, ang(0,0,0))
        holoAlpha(I+6, 0)
        holoColor(I+6, vec(0))
        
        holoMaterial(I+12, "debug/debugdrawflat")
        holoModel(I+12, "hq_sphere")
        holoAng(I+12, ang(0,0,0))
        holoAlpha(I+12, 0)
        holoColor(I+12, vec(0))
        holoScaleUnits(I+12, vec(3))
        }
    #[ HOLOGRAMS 43 UP TO 48 ARE FIELD BACKGROUNDS
    for(I=43,48){
        holoMaterial(I, "debug/debugdrawflat")
        holoModel(I, "hq_sphere")
        holoAng(I, ang(0,0,0))
        holoAlpha(I, 0)
        holoColor(I, vec(0))
        }
    # HOLOGRAMS 49 UP TO 54 ARE FIELD CENTERS
    for(I=49,54){
        holoMaterial(I, "debug/debugdrawflat")
        holoModel(I, "hq_sphere")
        holoAng(I, ang(0,0,0))
        holoAlpha(I, 0)
        holoColor(I, vec(0))
        holoScaleUnits(I, vec(3))
        
        }]#
        
        
    holoAlpha(34, 0)
    holoAlpha(35, 0)
    holoAlpha(36, 0)
    
}

# PLANE-LINE INTERSECTION
Holo = holoEntity(34)

Origin = owner():shootPos()
OriginDirVec = owner():eye()

#How does line-plane intersection work?
PlanePoint = Holo:pos() #Get a point in the plane
Normal = Holo:up() #Get the normal (a vector perpendicular to the surface) of the plane
LinePoint1 = Origin #Get a point on the line
LinePoint2 = Origin+OriginDirVec #Get a point on the line "after" point 1#
X = (Normal:dot(PlanePoint-LinePoint1))/(Normal:dot(LinePoint2-LinePoint1)) #Not really sure how, but it returns how many times the distance from point 1 to point 2 you need to go from point 1 to reach the intersection
Vec = LinePoint1+X*(LinePoint2-LinePoint1) #Get the intersections position using f(X) = LinePoint1+X*(LinePoint2-LinePoint1)

#holoPos(34, Vec)


#BUTTONS HIGHLIGTHING
#####################

if(Master:aimEntity()==entity()){Ons=1}else{Ons=0}
HudCursorLocal=holoEntity(1):toLocal(Master:aimPos())
Use = Master:keyUse()

if(Mode==0){
# 1
if(Ons&Master&inrange(HudCursorLocal, vec(0.2,-5.5,-10), vec(5.5,-0.2,10))){holoColor(5, vec(60,255,60))    if(Use&$Use){Mode=1 Lag=0 holoAlpha(34, 255) soundPlay(3,soundDuration("buttons/lightswitch2.wav"),"buttons/lightswitch2.wav")}}else{holoColor(5, vec(255))}
# 2
if(Ons&Master&inrange(HudCursorLocal, vec(-5.5,-5.5,-10), vec(0.2,-0.2,10))){holoColor(13, vec(60,255,60))  if(Use&$Use){Mode=2 Lag=0 holoAlpha(34, 255) soundPlay(3,soundDuration("buttons/lightswitch2.wav"),"buttons/lightswitch2.wav")}}else{holoColor(13, vec(255))}
# 4
if(Ons&Master&inrange(HudCursorLocal, vec(-5.5,0.2,-10), vec(0.2,5.5,10))){holoColor(29, vec(60,255,60))    if(Use&$Use){Mode=3 Lag=0 holoAlpha(34, 255) soundPlay(3,soundDuration("buttons/lightswitch2.wav"),"buttons/lightswitch2.wav")}}else{holoColor(29, vec(255))}
# 3
if(Ons&Master&inrange(HudCursorLocal, vec(-0.2,0.2,-10), vec(5.5,5.5,10))){holoColor(21, vec(60,255,60))    if(Use&$Use){Mode=4 Lag=0 holoAlpha(34, 255) soundPlay(3,soundDuration("buttons/lightswitch2.wav"),"buttons/lightswitch2.wav")}}else{holoColor(21, vec(255))}
}

# ENTITY DISCOVERY
##################

timer("search",500)
if(clk("search")){
findByClass("prop_*")
findIncludePlayerProps(owner())
Props = findToArray()
}


if(Mode==1){
    
    if(SubMode==0){holoPos(34, Master:aimPos())}
    
    if(Use&$Use&Lag==10&SubMode==0){SubMode=1 Lag=0 OriginPos=Master:aimPos() }
    if(SubMode==1){
        
        if(OriginPos:distance(Vec)<300){
            holoColor(34, vec(255))
            holoColor(35, vec(255))
            holoColor(36, vec(255))
            holoScaleUnits(35, vec(OriginPos:distance(Vec),1,1))
            holoScaleUnits(36, vec(OriginPos:distance(Vec)*2,OriginPos:distance(Vec)*2,1))
            holoAng(35, (holoEntity(34):pos()-Vec):toAngle())
            holoPos(36, OriginPos)
            holoPos(35, OriginPos+holoEntity(35):forward()*-( OriginPos:distance(Vec)/2))
            holoAlpha(35, 255)
            holoAlpha(36, 255)
            
        
            # SPAWNING FIELD
            if(Use&$Use&Lag==10){
                
                holoPos(CurField, OriginPos)
                holoScaleUnits(CurField, -vec(OriginPos:distance(Vec)*2))
                holoColor(CurField, vec(0,255,0))
                holoAlpha(CurField, 255)
                
                holoPos(CurField+6, OriginPos)
                holoScaleUnits(CurField+6, vec(OriginPos:distance(Vec)*2))
                holoAlpha(CurField+6, 10)
                
                holoPos(CurField+12, OriginPos)
                holoScaleUnits(CurField+12, vec(3))
                holoColor(CurField+12, vec(0,255,0))
                holoAlpha(CurField+12, 255)
                
                Sizes[CurField+6,number]=vec(OriginPos:distance(Vec)):x()
                CurField++
                Lag=0
                SubMode=0
                OriginPos=vec(0)
                Mode=0
                holoAlpha(34, 0)
                holoAlpha(35, 0)
                holoAlpha(36, 0)
                
                
            }
            }else{
            holoColor(34, vec(255,60,60))
            holoColor(35, vec(255,60,60))
            holoColor(36, vec(255,60,60))   
        } 
        }   
}

###########################################################################################
######### REPEL

if(Mode==2){
    
    if(SubMode==0){holoPos(34, Master:aimPos())}
    
    if(Use&$Use&Lag==10&SubMode==0){SubMode=1 Lag=0 OriginPos=Master:aimPos() }
    if(SubMode==1){
        
        if(OriginPos:distance(Vec)<300){
            holoColor(34, vec(255))
            holoColor(35, vec(255))
            holoColor(36, vec(255))
            holoScaleUnits(35, vec(OriginPos:distance(Vec),1,1))
            holoScaleUnits(36, vec(OriginPos:distance(Vec)*2,OriginPos:distance(Vec)*2,1))
            holoAng(35, (holoEntity(34):pos()-Vec):toAngle())
            holoPos(36, OriginPos)
            holoPos(35, OriginPos+holoEntity(35):forward()*-( OriginPos:distance(Vec)/2))
            holoAlpha(35, 255)
            holoAlpha(36, 255)
            
        
            # SPAWNING FIELD
            if(Use&$Use&Lag==10){
                
                holoPos(CurField, OriginPos)
                holoScaleUnits(CurField, -vec(OriginPos:distance(Vec)*2))
                holoColor(CurField, vec(255,0,0))
                holoAlpha(CurField, 255)
                
                holoPos(CurField+6, OriginPos)
                holoScaleUnits(CurField+6, vec(OriginPos:distance(Vec)*2))
                holoAlpha(CurField+6, 10)
                
                holoPos(CurField+12, OriginPos)
                holoScaleUnits(CurField+12, vec(3))
                holoColor(CurField+12, vec(255,0,0))
                holoAlpha(CurField+12, 255)
                
                Sizes[CurField+6,number]=vec(OriginPos:distance(Vec)):x()
                CurField++
                Lag=0
                SubMode=0
                OriginPos=vec(0)
                Mode=0
                holoAlpha(34, 0)
                holoAlpha(35, 0)
                holoAlpha(36, 0)
                
                
            }
            }else{
            holoColor(34, vec(255,60,60))
            holoColor(35, vec(255,60,60))
            holoColor(36, vec(255,60,60))   
        } 
        }   
}

###########################################################################################
######### ATTRACT

if(Mode==4){
    
    if(SubMode==0){holoPos(34, Master:aimPos())}
    
    if(Use&$Use&Lag==10&SubMode==0){SubMode=1 Lag=0 OriginPos=Master:aimPos() }
    if(SubMode==1){
        
        if(OriginPos:distance(Vec)<300){
            holoColor(34, vec(255))
            holoColor(35, vec(255))
            holoColor(36, vec(255))
            holoScaleUnits(35, vec(OriginPos:distance(Vec),1,1))
            holoScaleUnits(36, vec(OriginPos:distance(Vec)*2,OriginPos:distance(Vec)*2,1))
            holoAng(35, (holoEntity(34):pos()-Vec):toAngle())
            holoPos(36, OriginPos)
            holoPos(35, OriginPos+holoEntity(35):forward()*-( OriginPos:distance(Vec)/2))
            holoAlpha(35, 255)
            holoAlpha(36, 255)
            
        
            # SPAWNING FIELD
            if(Use&$Use&Lag==10){
                
                holoPos(CurField, OriginPos)
                holoScaleUnits(CurField, -vec(OriginPos:distance(Vec)*2))
                holoColor(CurField, vec(0,0,255))
                holoAlpha(CurField, 255)
                
                holoPos(CurField+6, OriginPos)
                holoScaleUnits(CurField+6, vec(OriginPos:distance(Vec)*2))
                holoAlpha(CurField+6, 10)
                
                holoPos(CurField+12, OriginPos)
                holoScaleUnits(CurField+12, vec(3))
                holoColor(CurField+12, vec(0,0,255))
                holoAlpha(CurField+12, 255)
                
                Sizes[CurField+6,number]=vec(OriginPos:distance(Vec)):x()
                CurField++
                Lag=0
                SubMode=0
                OriginPos=vec(0)
                Mode=0
                holoAlpha(34, 0)
                holoAlpha(35, 0)
                holoAlpha(36, 0)
                
                
            }
            }else{
            holoColor(34, vec(255,60,60))
            holoColor(35, vec(255,60,60))
            holoColor(36, vec(255,60,60))   
        } 
        }   
}

#print(4, (OriginPos:distance(Master:aimPos())):toString())

if(Mode==3){
    
    
    # HOLOGRAMS 37 UP TO 42 ARE FIELD HOLOGRAMS
    for(I=37,42){
        holoMaterial(I, "models/wireframe")
        holoModel(I, "hq_sphere")
        holoAng(I, ang(0,0,0))
        holoAlpha(I, 0)
        holoColor(I, vec(0))
        }
    # HOLOGRAMS 43 UP TO 48 ARE FIELD BACKGROUNDS
    for(I=43,48){
        holoMaterial(I, "debug/debugdrawflat")
        holoModel(I, "hq_sphere")
        holoAng(I, ang(0,0,0))
        holoAlpha(I, 0)
        holoColor(I, vec(0))
        holoParent(I, holoEntity(I-6))
        }
    # HOLOGRAMS 49 UP TO 54 ARE FIELD CENTERS
    for(I=49,54){
        holoMaterial(I, "debug/debugdrawflat")
        holoModel(I, "hq_sphere")
        holoAng(I, ang(0,0,0))
        holoAlpha(I, 0)
        holoColor(I, vec(0))
        holoScaleUnits(I, vec(3))
        holoParent(I, holoEntity(I-6))
        }
        
        
    holoAlpha(34, 0)
    holoAlpha(35, 0)
    holoAlpha(36, 0)
    
    
    
    
    Lag=0
    CurField=37
    Mode=0
    
    
    
    
    
    
}


Lag+=0.5
if(Lag>=10){Lag=10}
if(CurField>39){CurField=37}


#############################################################################
#############################################################################
# FIELD CODE

for(I=1,Props:count()){
    
        # FIELD 1
        if(holoEntity(37):pos():distance(Props[I, entity]:nearestPoint(holoEntity(43):pos()))<Sizes[43,number]){
            
            # NO GRAVITY
            if(holoEntity(37):getColor()==vec(0,255,0)){
                Props[I, entity]:applyForce(vec(0,0,Gravity*Props[I, entity]:mass()))
            }
            
            # REPEL
            elseif(holoEntity(37):getColor()==vec(255,0,0)){
                Nearest = Props[I, entity]:nearestPoint(holoEntity(37):pos())
                NearestLocal= holoEntity(37):toLocal(Nearest)
                Props[I,entity]:applyOffsetForce(NearestLocal*(Props[I, entity]:mass()/4), (holoEntity(37):pos()*1))
                
            }
            elseif(holoEntity(37):getColor()==vec(0,0,255)){
                Nearest = Props[I, entity]:nearestPoint(holoEntity(37):pos())
                NearestLocal= holoEntity(37):toLocal(Nearest)
                Props[I,entity]:applyOffsetForce(-(NearestLocal*(Props[I, entity]:mass()/6)), (holoEntity(37):pos()*1))
            }    
    # END OF FIELD 1       
    }


    # FIELD 2
        if(holoEntity(38):pos():distance(Props[I, entity]:nearestPoint(holoEntity(44):pos()))<Sizes[44,number]){
            
            # NO GRAVITY
            if(holoEntity(38):getColor()==vec(0,255,0)){
                Props[I, entity]:applyForce(vec(0,0,Gravity*Props[I, entity]:mass()))
            }
            
            # REPEL
            elseif(holoEntity(38):getColor()==vec(255,0,0)){
                Nearest = Props[I, entity]:nearestPoint(holoEntity(38):pos())
                NearestLocal= holoEntity(38):toLocal(Nearest)
                Props[I,entity]:applyOffsetForce(NearestLocal*(Props[I, entity]:mass()/4), (holoEntity(38):pos()*1))
                
            }
            elseif(holoEntity(38):getColor()==vec(0,0,255)){
                Nearest = Props[I, entity]:nearestPoint(holoEntity(38):pos())
                NearestLocal= holoEntity(38):toLocal(Nearest)
                Props[I,entity]:applyOffsetForce(-(NearestLocal*(Props[I, entity]:mass()/6)), (holoEntity(38):pos()*1))
            }    
    # END OF FIELD 2       
    }

    # FIELD 3
        if(holoEntity(39):pos():distance(Props[I, entity]:nearestPoint(holoEntity(45):pos()))<Sizes[45,number]){
            
            # NO GRAVITY
            if(holoEntity(39):getColor()==vec(0,255,0)){
                Props[I, entity]:applyForce(vec(0,0,Gravity*Props[I, entity]:mass()))
            }
            
            # REPEL
            elseif(holoEntity(39):getColor()==vec(255,0,0)){
                Nearest = Props[I, entity]:nearestPoint(holoEntity(39):pos())
                NearestLocal= holoEntity(39):toLocal(Nearest)
                Props[I,entity]:applyOffsetForce(NearestLocal*(Props[I, entity]:mass()/4), (holoEntity(39):pos()*1))
                
            }
            elseif(holoEntity(39):getColor()==vec(0,0,255)){
                Nearest = Props[I, entity]:nearestPoint(holoEntity(39):pos())
                NearestLocal= holoEntity(39):toLocal(Nearest)
                Props[I,entity]:applyOffsetForce(-(NearestLocal*(Props[I, entity]:mass()/6)), (holoEntity(39):pos()*1))
            }    
    # END OF FIELD 3       
    }

# END OF LOOP
}

#holoAng(37, ang(0,curtime()*4,0))
#holoAng(38, ang(0,curtime()*4,0))
##holoAng(39, ang(0,curtime()*4,0))

#print(4, NearestLocal:toString())


