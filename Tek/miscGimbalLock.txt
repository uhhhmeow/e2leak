@name miscGimbalLock
@inputs 
@outputs Turret:entity DirectionToTarget:vector Force:angle Angles:angle Pang:angle
@persist 
@trigger 

runOnTick(1)

if(first()){

entity():isWeldedTo():propGravity(0)
entity():propGravity(0)
entity():isWeldedTo():setMass(1000)

holoCreate(1)
holoScaleUnits(1, vec(3))
holoModel(1, "hq_sphere")
}
Plate=entity():isWeldedTo()
Angles = (Plate:pos()-owner():aimPos()):toAngle()

if(owner():keyUse()){
    
# WORKING CODE FOR NORMAL OBJECTS
# Force = Plate:toLocal(Angles)
# Plate:applyAngForce( Force * somenumber)

# CODE FOR SEATS + 45 UNITS IN YAW TO ROTATE
Plate:applyAngForce(ang(Plate:angVel():roll(),-Plate:angVel():yaw(),-Plate:angVel():pitch())*Plate:mass()) 
}else{




# TRABANT WORKING SOLUTION FOR SEAT VEL NULLYFING
Plate:applyAngForce(ang(Plate:angVel():roll(),-Plate:angVel():yaw(),-Plate:angVel():pitch())*Plate:mass()) 













}

