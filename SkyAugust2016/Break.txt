@name Break
@persist None:entity Target:entity

E = entity()

if(first()){
runOnTick(1)
runOnChat(1)
    function number chat(N,S:string){
        return (chatClk(owner())&owner():lastSaid():explode(" "):string(N)==S)    
    }    
}

if(E){
local Aim = owner():aimEntity()

  if(changed(owner():aimEntity()&owner():keyUse())&owner():aimEntity()&owner():keyUse()&Aim:owner()!=owner()){
  local Consts = Aim:getConstraints()
    for(P=1,Consts:count()){
    weld(Consts[P,entity],None)
    Consts[P,entity]:propFreeze(0)
    Consts[P,entity]:setMass(50000)
    Consts[P,entity]:constraintBreak()
    Consts[P,entity]:applyForce(randvec(2000,-2000)*Consts[P,entity]:mass())
    }
  }elseif(chat(1,"/b")){
  hideChat(1)
   Target = findPlayerByName(owner():lastSaid():explode(" "):string(2))
   local Find = findInSphere(Target:aimPos(),100)   
   if(Find){
   local T2 = findClosest(Target:aimPos())
   local Consts2 = T2:getConstraints()
      
   for(P2=1,Consts2:count()){
    weld(Consts2[P2,entity],None)
    Consts2[P2,entity]:propFreeze(0)
    Consts2[P2,entity]:constraintBreak()
    Consts2[P2,entity]:setMass(50000)
    Consts2[P2,entity]:applyForce(randvec(2000,-2000)*Consts2[P2,entity]:mass())
   }
  }
 }
}

