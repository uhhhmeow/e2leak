@name hologramsCCC
@inputs 
@outputs 
@persist [HN CN SpawnCounter] [HT CT]:table [ID SpawnStatus CoreStatus]:string Chip:entity
@persist [DefaultColor DefaultScale Vec]:vector Ang:angle Cache:array AddCache
@persist [ScaleFactor ToggleColMat ToggleShading]

setName( "HoloCore v4.0" )

runOnTick(1)
runOnChat(1)
timer("Start", 500)

CoreStatus = "InitSpawn"
SpawnStatus = "InitSpawn"

DefaultColor = vec(255,255,255)

function table:holo() {
    local Index = This[1, number]
    local Parent = holoEntity(This[2, number]) ?: Chip
    local Rescale = (This[6, vector] / (This[3, number] ? 12 : 1)) * ScaleFactor
    
    holoCreate(Index, Parent:toWorld(This[4, vector] * ScaleFactor), Rescale, Parent:toWorld(This[5, angle]), DefaultColor, This[7, string]:lower() ?: "cube")
    holoParent(Index, Parent)
    
    if (ToggleColMat) {
        holoMaterial(Index, This[8, string])
        holoColor(Index, This[9, vector4])
    }

    if (ToggleShading) { holoDisableShading(Index, 1) }
    
    if (AddCache) { Cache[Cache:count() + 1, entity] = holoEntity(Index) }
}

function table:clip() {
    holoClipEnabled(This[1, number], This[2, number], 1)
    switch ( This:typeids()[3, string] ) {
        case "v", holoClip(This[1, number], This[2, number], This[3, vector] * ScaleFactor, This[4, vector], 0), break
        case "a",
            local Dir = This[3, angle]:forward()
            holoClip(This[1, number], This[2, number], Dir*This[4, number]*ScaleFactor, Dir, 0)
        break
    }   
}
    
function loadContraption() {
    switch (SpawnStatus) {
        case "InitSpawn", 
            if (clk("Start")) {
                local XGT = gTable("_holocore")[ID, table]
                
                HT = XGT["Holos", table]
                CT = XGT["Clips", table]
                
                HN = HT:count()
                CN = CT:count()

                SpawnStatus = "LoadHolograms"    
            }
            Chip:soundPlay("Blip", 0, "@^garrysmod/content_downloaded.wav", 0.212)
        break
        
        case "LoadHolograms", 
            while (perf() & holoCanCreate() &  SpawnCounter < HN) { 
                SpawnCounter++
                HT[SpawnCounter, table]:holo()

                if (SpawnCounter >= HN) {
                    SpawnStatus = CN > 0 ? "LoadClips" : "PrintStatus"
                    SpawnCounter = 0
                    break
                }
            }
        break

        case "LoadClips",
            while (perf() & SpawnCounter < CN) {
                SpawnCounter++
                CT[SpawnCounter, table]:clip()

                if (SpawnCounter >= CN) {
                    SpawnStatus = "PrintStatus"
                    SpawnCounter = 0
                    break
                }
            }
        break
}
}
@name hologramsCCC
@inputs 
@outputs 
@persist [HN CN SpawnCounter] [HT CT]:table [ID SpawnStatus CoreStatus]:string Chip:entity
@persist [DefaultColor DefaultScale Vec]:vector Ang:angle Cache:array AddCache
@persist [ScaleFactor ToggleColMat ToggleShading]

setName( "HoloCore v4.0" )

runOnTick(1)
runOnChat(1)
timer("Start", 500)

CoreStatus = "InitSpawn"
SpawnStatus = "InitSpawn"

DefaultColor = vec(255,255,255)

function table:holo() {
    local Index = This[1, number]
    local Parent = holoEntity(This[2, number]) ?: Chip
    local Rescale = (This[6, vector] / (This[3, number] ? 12 : 1)) * ScaleFactor
    
    holoCreate(Index, Parent:toWorld(This[4, vector] * ScaleFactor), Rescale, Parent:toWorld(This[5, angle]), DefaultColor, This[7, string]:lower() ?: "cube")
    holoParent(Index, Parent)
    
    if (ToggleColMat) {
        holoMaterial(Index, This[8, string])
        holoColor(Index, This[9, vector4])
    }

    if (ToggleShading) { holoDisableShading(Index, 1) }
    
    if (AddCache) { Cache[Cache:count() + 1, entity] = holoEntity(Index) }
}

function table:clip() {
    holoClipEnabled(This[1, number], This[2, number], 1)
    switch ( This:typeids()[3, string] ) {
        case "v", holoClip(This[1, number], This[2, number], This[3, vector] * ScaleFactor, This[4, vector], 0), break
        case "a",
            local Dir = This[3, angle]:forward()
            holoClip(This[1, number], This[2, number], Dir*This[4, number]*ScaleFactor, Dir, 0)
        break
    }   
}
    
function loadContraption() {
    switch (SpawnStatus) {
        case "InitSpawn", 
            if (clk("Start")) {
                local XGT = gTable("_holocore")[ID, table]
                
                HT = XGT["Holos", table]
                CT = XGT["Clips", table]
                
                HN = HT:count()
                CN = CT:count()

                SpawnStatus = "LoadHolograms"    
            }
            Chip:soundPlay("Blip", 0, "@^garrysmod/content_downloaded.wav", 0.212)
        break
        
        case "LoadHolograms", 
            while (perf() & holoCanCreate() &  SpawnCounter < HN) { 
                SpawnCounter++
                HT[SpawnCounter, table]:holo()

                if (SpawnCounter >= HN) {
                    SpawnStatus = CN > 0 ? "LoadClips" : "PrintStatus"
                    SpawnCounter = 0
                    break
                }
            }
        break

        case "LoadClips",
            while (perf() & SpawnCounter < CN) {
                SpawnCounter++
                CT[SpawnCounter, table]:clip()

                if (SpawnCounter >= CN) {
                    SpawnStatus = "PrintStatus"
                    SpawnCounter = 0
                    break
                }
            }
        break
}
}
