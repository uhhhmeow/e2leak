@name Lazer cannon TH-360-Zero
@inputs Fire
@outputs 
@persist C Shot Ang HX Schuss HJN GH:entity 
@trigger 
@model models/hunter/misc/sphere175x175.mdl
interval(50)
E=entity()
C++
U=1823
Rauch=13812
if(first()){
 holoCreate(1,E:toWorld(vec(35,0,0)),vec(5,5,2),E:toWorld(ang(90,0,0)),vec(50),"hqcylinder")
holoParent(1,E)  
 holoCreate(2,E:toWorld(vec(50,0,0)),vec(6,6,1),E:toWorld(ang(90,0,0)),vec(150),"hqtube")
holoParent(2,E) 
 holoCreate(3,E:toWorld(vec(50,0,0)),vec(4,4,1),E:toWorld(ang(90,0,0)),vec(255,0,0),"cylinder")
holoParent(3,E) 
 holoCreate(4,E:toWorld(vec(70,0,0)),vec(4,4,1),E:toWorld(ang(90,0,0)),vec(255,0,0),"cylinder")
holoParent(4,E)
 holoCreate(5,E:toWorld(vec(90,0,0)),vec(4,4,1),E:toWorld(ang(90,0,0)),vec(255,0,0),"cylinder")
holoParent(5,E) 
 holoCreate(6,E:toWorld(vec(110,0,0)),vec(4,4,1),E:toWorld(ang(90,0,0)),vec(255,0,0),"cylinder")
holoParent(6,E)   
 holoCreate(7,E:toWorld(vec(85,0,0)),vec(2,2,5),E:toWorld(ang(90,0,0)),vec(0,0,0),"hqtube")
holoParent(7,E)   
 holoCreate(8,E:toWorld(vec(120,0,0)),vec(1,1,2),E:toWorld(ang(90,0,0)),vec(10),"hqtube")
holoParent(8,E) 
 holoCreate(9,E:toWorld(vec(130,0,0)),vec(2,2,1),E:toWorld(ang(90,0,0)),vec(0,0,255),"hqcylinder")
holoParent(9,E)  
 holoCreate(10,E:toWorld(vec(140,10,0)),vec(0.5,0.5,2),E:toWorld(ang(90,0,0)),vec(0,0,0),"hqcylinder")
holoParent(10,9)  
 holoCreate(11,E:toWorld(vec(140,-10,0)),vec(0.5,0.5,2),E:toWorld(ang(90,0,0)),vec(0,0,0),"hqcylinder")
holoParent(11,9)  
 holoCreate(12,E:toWorld(vec(140,0,10)),vec(0.5,0.5,2),E:toWorld(ang(90,0,0)),vec(0,0,0),"hqcylinder")
holoParent(12,9)  
 holoCreate(13,E:toWorld(vec(140,0,-10)),vec(0.5,0.5,2),E:toWorld(ang(90,0,0)),vec(0,0,0),"hqcylinder")
holoParent(13,9)  
}
holoAng(3,holoEntity(1):toWorld(ang(0,C*5,0)))
holoAng(4,holoEntity(1):toWorld(ang(0,C*-5,0)))
holoAng(5,holoEntity(1):toWorld(ang(0,C*5,0)))
holoAng(6,holoEntity(1):toWorld(ang(0,C*-5,0)))
holoCreate(U)
Ran=rangerOffset(99999999999,holoEntity(9):pos(),holoEntity(9):up()*99999999999999999) holoPos(U,Ran:pos())
if(changed(Fire==1)&Fire==1){
 E:soundPlay(1,9999,"ambient/energy/force_field_loop1.wav")   
}
if(Fire==1&Shot==0){
    Shot=1
}
if(Shot==1&Schuss==0){
 Ang=Ang+1
HX=HX*1+Ang 
if(changed(Ang>49)&Ang>49){
    GH=propSpawn("models/props_phx/cannonball_solid.mdl",holoEntity(11):pos(),1) GH:setAlpha(0) GH:propNotSolid(1)
    GH:setTrails(50,0,1,"trails/smoke",vec(0,255,255),255)
    holoCreate(Rauch,holoEntity(11):toWorld(vec(0,0,0)))
        holoEntity(Rauch):setTrails(50,0,2,"trails/smoke",vec(0,255,255),255)
     }
 if(Ang>50){Ang=50 Schuss=1 HJN=1 }
}
if(Schuss==1){
    Shot=2
}
if(Shot==2){
     Ang=Ang+-1
if(Ang>0){HX=HX*1+Ang Shot=0}
if(Ang<0){Ang=0 Shot=0 Schuss=0 HJN=0}
}
if(changed(HJN==1)&HJN==1){
Pos=1
}
if(Pos==1){
   holoPos(Rauch,holoEntity(U):toWorld(vec(-50,0,0)))     
}
if(HJN==1){
 
    timer("back",1500)
    GH:setPos(Ran:pos()) GH:propBreak()
}
if(clk("back")){
 holoDelete(Rauch)   
}
soundPitch(1,Ang*5)
if(changed(GH)&GH){
 E:soundPlay(2,2,"ambient/machines/floodgate_stop1.wav")   
}
holoAng(9,holoEntity(1):toWorld(ang(0,HX,0)))
holoColor(U,vec(255,0,0))
@name Lazer cannon TH-360-Zero
@inputs Fire
@outputs 
@persist C Shot Ang HX Schuss HJN GH:entity 
@trigger 
@model models/hunter/misc/sphere175x175.mdl
interval(50)
E=entity()
C++
U=1823
Rauch=13812
if(first()){
 holoCreate(1,E:toWorld(vec(35,0,0)),vec(5,5,2),E:toWorld(ang(90,0,0)),vec(50),"hqcylinder")
holoParent(1,E)  
 holoCreate(2,E:toWorld(vec(50,0,0)),vec(6,6,1),E:toWorld(ang(90,0,0)),vec(150),"hqtube")
holoParent(2,E) 
 holoCreate(3,E:toWorld(vec(50,0,0)),vec(4,4,1),E:toWorld(ang(90,0,0)),vec(255,0,0),"cylinder")
holoParent(3,E) 
 holoCreate(4,E:toWorld(vec(70,0,0)),vec(4,4,1),E:toWorld(ang(90,0,0)),vec(255,0,0),"cylinder")
holoParent(4,E)
 holoCreate(5,E:toWorld(vec(90,0,0)),vec(4,4,1),E:toWorld(ang(90,0,0)),vec(255,0,0),"cylinder")
holoParent(5,E) 
 holoCreate(6,E:toWorld(vec(110,0,0)),vec(4,4,1),E:toWorld(ang(90,0,0)),vec(255,0,0),"cylinder")
holoParent(6,E)   
 holoCreate(7,E:toWorld(vec(85,0,0)),vec(2,2,5),E:toWorld(ang(90,0,0)),vec(0,0,0),"hqtube")
holoParent(7,E)   
 holoCreate(8,E:toWorld(vec(120,0,0)),vec(1,1,2),E:toWorld(ang(90,0,0)),vec(10),"hqtube")
holoParent(8,E) 
 holoCreate(9,E:toWorld(vec(130,0,0)),vec(2,2,1),E:toWorld(ang(90,0,0)),vec(0,0,255),"hqcylinder")
holoParent(9,E)  
 holoCreate(10,E:toWorld(vec(140,10,0)),vec(0.5,0.5,2),E:toWorld(ang(90,0,0)),vec(0,0,0),"hqcylinder")
holoParent(10,9)  
 holoCreate(11,E:toWorld(vec(140,-10,0)),vec(0.5,0.5,2),E:toWorld(ang(90,0,0)),vec(0,0,0),"hqcylinder")
holoParent(11,9)  
 holoCreate(12,E:toWorld(vec(140,0,10)),vec(0.5,0.5,2),E:toWorld(ang(90,0,0)),vec(0,0,0),"hqcylinder")
holoParent(12,9)  
 holoCreate(13,E:toWorld(vec(140,0,-10)),vec(0.5,0.5,2),E:toWorld(ang(90,0,0)),vec(0,0,0),"hqcylinder")
holoParent(13,9)  
}
holoAng(3,holoEntity(1):toWorld(ang(0,C*5,0)))
holoAng(4,holoEntity(1):toWorld(ang(0,C*-5,0)))
holoAng(5,holoEntity(1):toWorld(ang(0,C*5,0)))
holoAng(6,holoEntity(1):toWorld(ang(0,C*-5,0)))
holoCreate(U)
Ran=rangerOffset(99999999999,holoEntity(9):pos(),holoEntity(9):up()*99999999999999999) holoPos(U,Ran:pos())
if(changed(Fire==1)&Fire==1){
 E:soundPlay(1,9999,"ambient/energy/force_field_loop1.wav")   
}
if(Fire==1&Shot==0){
    Shot=1
}
if(Shot==1&Schuss==0){
 Ang=Ang+1
HX=HX*1+Ang 
if(changed(Ang>49)&Ang>49){
    GH=propSpawn("models/props_phx/cannonball_solid.mdl",holoEntity(11):pos(),1) GH:setAlpha(0) GH:propNotSolid(1)
    GH:setTrails(50,0,1,"trails/smoke",vec(0,255,255),255)
    holoCreate(Rauch,holoEntity(11):toWorld(vec(0,0,0)))
        holoEntity(Rauch):setTrails(50,0,2,"trails/smoke",vec(0,255,255),255)
     }
 if(Ang>50){Ang=50 Schuss=1 HJN=1 }
}
if(Schuss==1){
    Shot=2
}
if(Shot==2){
     Ang=Ang+-1
if(Ang>0){HX=HX*1+Ang Shot=0}
if(Ang<0){Ang=0 Shot=0 Schuss=0 HJN=0}
}
if(changed(HJN==1)&HJN==1){
Pos=1
}
if(Pos==1){
   holoPos(Rauch,holoEntity(U):toWorld(vec(-50,0,0)))     
}
if(HJN==1){
 
    timer("back",1500)
    GH:setPos(Ran:pos()) GH:propBreak()
}
if(clk("back")){
 holoDelete(Rauch)   
}
soundPitch(1,Ang*5)
if(changed(GH)&GH){
 E:soundPlay(2,2,"ambient/machines/floodgate_stop1.wav")   
}
holoAng(9,holoEntity(1):toWorld(ang(0,HX,0)))
holoColor(U,vec(255,0,0))
