@name Ship v1
@inputs 
@outputs 
@persist Seat:entity Pos Move XY Speed C Moving Shot:entity CA
@trigger   
@model models/hunter/blocks/cube05x075x025.mdl
E=entity()
runOnTick(1)
runOnLast(1)
Driver=Seat:driver()
W=Driver:keyForward()
A=Driver:keyLeft()
S=Driver:keyBack()
D=Driver:keyRight()
Space=Driver:keyJump()
Shift=Driver:keySprint()
M1=Driver:keyAttack1()
R=Driver:keyReload()
M2=Driver:keyAttack2()



XY=XY*1+1
X=cos(XY)
Y=sin(XY)
if(first()){
holoCreate(1,E:toWorld(vec(0,0,0)),vec(2,3,5),E:toWorld(ang(90,90,0)),vec(20,20,20),"hqtube")
holoParent(1,E)
holoCreate(2,E:toWorld(vec(50,0,0)),vec(4,4,0.5),E:toWorld(ang(0,0,0)),vec(20,20,20),"hqtube")
holoParent(2,E)
holoCreate(3,E:toWorld(vec(-50,0,0)),vec(4,4,0.5),E:toWorld(ang(0,0,0)),vec(20,20,20),"hqtube")
holoParent(3,E)

holoCreate(4,E:toWorld(vec(50,0,0)),vec(0.5,4,0.5),E:toWorld(ang(90,0,0)),vec(20,20,20),"hqsphere")
holoParent(4,2)
holoCreate(5,E:toWorld(vec(-50,0,0)),vec(0.5,4,0.5),E:toWorld(ang(90,90,0)),vec(20,20,20),"hqsphere")
holoParent(5,3)


holoCreate(6,E:toWorld(vec(20,0,0)),vec(0.5,0.5,1.5),E:toWorld(ang(90,0,0)),vec(20,20,20),"hqcylinder")
holoParent(6,E)
holoCreate(7,E:toWorld(vec(-20,0,0)),vec(0.5,0.5,1.5),E:toWorld(ang(90,0,0)),vec(20,20,20),"hqcylinder")
holoParent(7,E)

holoCreate(8,E:toWorld(vec(-10,0,10)),vec(0.1,0.1,5),E:toWorld(ang(90,90,0)),vec(255,255,255),"")
holoParent(8,E)
holoCreate(9,E:toWorld(vec(10,0,10)),vec(0.1,0.1,5),E:toWorld(ang(90,90,0)),vec(255,255,255),"")
holoParent(9,E)

holoCreate(10,E:toWorld(vec(0,30,0)),vec(2,3,2),E:toWorld(ang(90,90,0)),vec(20,20,20),"hqsphere")
holoParent(10,E)

holoCreate(11,E:toWorld(vec(0,30,0)),vec(1,1,1),E:toWorld(ang(90,90,0)),vec(20,20,20),"hqsphere")
holoParent(11,E)
holoCreate(12,E:toWorld(vec(0,50,0)),vec(0.5,0.5,3),E:toWorld(ang(90,90,0)),vec(20,20,20),"hqtube")
holoParent(12,11)
holoCreate(13,E:toWorld(vec(0,-30,0)),vec(2,3,2),E:toWorld(ang(90,90,0)),vec(20,20,20),"hqsphere")
holoParent(13,E)
}
Aim=round(Driver:aimPos()-holoEntity(11):pos()):toAngle()
if(Driver){
holoAng(11,Aim+ang(90,0,0))
}
timer("reload",1000)
if(M1&clk("reload")){
    Shot=propSpawn("models/props_phx/ww2bomb.mdl",holoEntity(12):toWorld(vec(0,0,0)),0)
    Shot:setTrails(0,50,2,"trails/smoke",vec(0,255,0),255)
    Shot:applyForce(holoEntity(12):up()*Shot:mass()*500050)
    Shot:setAng(Aim+ang(0,0,0))
    Shot:propGravity(0)
    Shot:setMass(50)
}
holoAng(3,E:toWorld(ang(0,0,0)))
holoAng(2,E:toWorld(ang(0,0,0)))
if(Moving==1){
if(W){
holoAng(3,E:toWorld(ang(0,0,-50)))   
holoAng(2,E:toWorld(ang(0,0,-50)))  
}
if(S){
    holoAng(3,E:toWorld(ang(0,0,50)))   
holoAng(2,E:toWorld(ang(0,0,50)))  
}
if(A){
     holoAng(3,E:toWorld(ang(0,0,50)))   
holoAng(2,E:toWorld(ang(0,0,-50)))    
}
if(D){
    holoAng(3,E:toWorld(ang(0,0,-50)))   
holoAng(2,E:toWorld(ang(0,0,50)))     
}
}
C++
if(Driver){
 Active=1   
}else{
Active=0
Move=0
Speed=0
}

if(Active==1){  Speed=Speed+0.05
      Move=Move*1+Speed
    if(Speed>250){Speed=250
       
        }
 holoAng(5,holoEntity(3):toWorld(ang(0,Move,0))  ) 
 holoAng(4,holoEntity(2):toWorld(ang(0,-Move,0))  ) 
if(Speed==40){
  Moving=1  
}

}
if(!Driver){

    Speed=0
Moving=0
}
if(W){
    
}
if(!Seat){
    findByClass("prop_vehicle_*")
   findSortByDistance(E:pos())
if(find():owner()==owner()&!Seat){
Seat=find() 
   Pos=1
} 
}
if(Pos==1){
    Seat:setAng(E:toWorld(ang(0,0,0)))
   Seat:setPos(E:toWorld(vec(0,0,5)))
timer("parent",150) 
}
if(clk("parent")){
 Seat:parentTo(E)  
stoptimer("parent")
Pos=0 
}
Seat:setAlpha(0)
if(last()){

}


if(Driver&Moving==1){
 E:applyForce(E:right()*clamp(10,0,10)*E:mass()*-1*(W-S))  
E:applyForce(E:up()*clamp(10,0,10)*E:mass()*2*(Space-Shift))
E:applyAngForce(ang(0,1,0)*clamp(10,0,10)*E:mass()*1*(A-D))
}
if(R&Moving==1){
 E:applyForce(-E:vel()*E:mass()*0.3)
E:applyAngForce(-E:angVel()*E:mass()*0.3)
E:applyAngForce((ang(0,E:angles():yaw(),0)-E:angles())*E:mass()*5)   
}

F=Driver:keyPressed("F")
if(changed(F)&F){CA+=1}
if(CA>=2){CA=0}
if(CA==1){E:propGravity(0)}
if(CA==0){E:propGravity(1)}
if(!Driver){E:propGravity(1)}

@name Ship v1
@inputs 
@outputs 
@persist Seat:entity Pos Move XY Speed C Moving Shot:entity CA
@trigger   
@model models/hunter/blocks/cube05x075x025.mdl
E=entity()
runOnTick(1)
runOnLast(1)
Driver=Seat:driver()
W=Driver:keyForward()
A=Driver:keyLeft()
S=Driver:keyBack()
D=Driver:keyRight()
Space=Driver:keyJump()
Shift=Driver:keySprint()
M1=Driver:keyAttack1()
R=Driver:keyReload()
M2=Driver:keyAttack2()



XY=XY*1+1
X=cos(XY)
Y=sin(XY)
if(first()){
holoCreate(1,E:toWorld(vec(0,0,0)),vec(2,3,5),E:toWorld(ang(90,90,0)),vec(20,20,20),"hqtube")
holoParent(1,E)
holoCreate(2,E:toWorld(vec(50,0,0)),vec(4,4,0.5),E:toWorld(ang(0,0,0)),vec(20,20,20),"hqtube")
holoParent(2,E)
holoCreate(3,E:toWorld(vec(-50,0,0)),vec(4,4,0.5),E:toWorld(ang(0,0,0)),vec(20,20,20),"hqtube")
holoParent(3,E)

holoCreate(4,E:toWorld(vec(50,0,0)),vec(0.5,4,0.5),E:toWorld(ang(90,0,0)),vec(20,20,20),"hqsphere")
holoParent(4,2)
holoCreate(5,E:toWorld(vec(-50,0,0)),vec(0.5,4,0.5),E:toWorld(ang(90,90,0)),vec(20,20,20),"hqsphere")
holoParent(5,3)


holoCreate(6,E:toWorld(vec(20,0,0)),vec(0.5,0.5,1.5),E:toWorld(ang(90,0,0)),vec(20,20,20),"hqcylinder")
holoParent(6,E)
holoCreate(7,E:toWorld(vec(-20,0,0)),vec(0.5,0.5,1.5),E:toWorld(ang(90,0,0)),vec(20,20,20),"hqcylinder")
holoParent(7,E)

holoCreate(8,E:toWorld(vec(-10,0,10)),vec(0.1,0.1,5),E:toWorld(ang(90,90,0)),vec(255,255,255),"")
holoParent(8,E)
holoCreate(9,E:toWorld(vec(10,0,10)),vec(0.1,0.1,5),E:toWorld(ang(90,90,0)),vec(255,255,255),"")
holoParent(9,E)

holoCreate(10,E:toWorld(vec(0,30,0)),vec(2,3,2),E:toWorld(ang(90,90,0)),vec(20,20,20),"hqsphere")
holoParent(10,E)

holoCreate(11,E:toWorld(vec(0,30,0)),vec(1,1,1),E:toWorld(ang(90,90,0)),vec(20,20,20),"hqsphere")
holoParent(11,E)
holoCreate(12,E:toWorld(vec(0,50,0)),vec(0.5,0.5,3),E:toWorld(ang(90,90,0)),vec(20,20,20),"hqtube")
holoParent(12,11)
holoCreate(13,E:toWorld(vec(0,-30,0)),vec(2,3,2),E:toWorld(ang(90,90,0)),vec(20,20,20),"hqsphere")
holoParent(13,E)
}
Aim=round(Driver:aimPos()-holoEntity(11):pos()):toAngle()
if(Driver){
holoAng(11,Aim+ang(90,0,0))
}
timer("reload",1000)
if(M1&clk("reload")){
    Shot=propSpawn("models/props_phx/ww2bomb.mdl",holoEntity(12):toWorld(vec(0,0,0)),0)
    Shot:setTrails(0,50,2,"trails/smoke",vec(0,255,0),255)
    Shot:applyForce(holoEntity(12):up()*Shot:mass()*500050)
    Shot:setAng(Aim+ang(0,0,0))
    Shot:propGravity(0)
    Shot:setMass(50)
}
holoAng(3,E:toWorld(ang(0,0,0)))
holoAng(2,E:toWorld(ang(0,0,0)))
if(Moving==1){
if(W){
holoAng(3,E:toWorld(ang(0,0,-50)))   
holoAng(2,E:toWorld(ang(0,0,-50)))  
}
if(S){
    holoAng(3,E:toWorld(ang(0,0,50)))   
holoAng(2,E:toWorld(ang(0,0,50)))  
}
if(A){
     holoAng(3,E:toWorld(ang(0,0,50)))   
holoAng(2,E:toWorld(ang(0,0,-50)))    
}
if(D){
    holoAng(3,E:toWorld(ang(0,0,-50)))   
holoAng(2,E:toWorld(ang(0,0,50)))     
}
}
C++
if(Driver){
 Active=1   
}else{
Active=0
Move=0
Speed=0
}

if(Active==1){  Speed=Speed+0.05
      Move=Move*1+Speed
    if(Speed>250){Speed=250
       
        }
 holoAng(5,holoEntity(3):toWorld(ang(0,Move,0))  ) 
 holoAng(4,holoEntity(2):toWorld(ang(0,-Move,0))  ) 
if(Speed==40){
  Moving=1  
}

}
if(!Driver){

    Speed=0
Moving=0
}
if(W){
    
}
if(!Seat){
    findByClass("prop_vehicle_*")
   findSortByDistance(E:pos())
if(find():owner()==owner()&!Seat){
Seat=find() 
   Pos=1
} 
}
if(Pos==1){
    Seat:setAng(E:toWorld(ang(0,0,0)))
   Seat:setPos(E:toWorld(vec(0,0,5)))
timer("parent",150) 
}
if(clk("parent")){
 Seat:parentTo(E)  
stoptimer("parent")
Pos=0 
}
Seat:setAlpha(0)
if(last()){

}


if(Driver&Moving==1){
 E:applyForce(E:right()*clamp(10,0,10)*E:mass()*-1*(W-S))  
E:applyForce(E:up()*clamp(10,0,10)*E:mass()*2*(Space-Shift))
E:applyAngForce(ang(0,1,0)*clamp(10,0,10)*E:mass()*1*(A-D))
}
if(R&Moving==1){
 E:applyForce(-E:vel()*E:mass()*0.3)
E:applyAngForce(-E:angVel()*E:mass()*0.3)
E:applyAngForce((ang(0,E:angles():yaw(),0)-E:angles())*E:mass()*5)   
}

F=Driver:keyPressed("F")
if(changed(F)&F){CA+=1}
if(CA>=2){CA=0}
if(CA==1){E:propGravity(0)}
if(CA==0){E:propGravity(1)}
if(!Driver){E:propGravity(1)}

