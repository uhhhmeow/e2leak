@name MechAnimANDbezier
@inputs 
@outputs 
@persist T:table
@trigger 
@model models/hunter/blocks/cube025x025x025.mdl
interval(100)
E = entity()

if(first()){
function number c(I,V:vector,S:vector,A:angle,Col:vector,Model:string,Alpha:number){ holoCreate(I,V,S,A,Col,Model) holoAlpha(I,Alpha) }
function number c(I,V:vector,S:vector,A:angle,Col:vector,Model:string,Parent:entity,Alpha:number){ holoCreate(I,V,S,A,Col,Model) holoAlpha(I,Alpha) holoParent(I,Parent) }
function number c(I,V:vector,S:vector,A:angle,Col:vector,Model:string,Parent:number,Alpha:number){ holoCreate(I,V,S,A,Col,Model) holoAlpha(I,Alpha) holoParent(I,Parent) }
function number cosine (One, Two, Three){ return acos( (One^2 + Two^2 - Three^2) / ((Two*2)*One) ) }
c(10,E:toWorld(vec(0,0,0)),vec(1),ang(),vec(255),"cube",E,0)  

c(1,holoEntity(10):toWorld(vec(0,0,0)),vec(1),ang(),vec(255,0,0),"cube",10,255)  
c(2,holoEntity(1):toWorld(vec(0,0,50)),vec(1),ang(),vec(0,0,255),"cube",1,255)      
c(3,holoEntity(2):toWorld(vec(0,0,50)),vec(1),ang(),vec(0,255,0),"cube",2,255)   

 function table:anim(Speed,StartAnim,EndCounter,Sheight,EndVector:vector,Glength,MaxDist,Vel,Base:entity,FeetHolo){
 local T = This
if(T["Parts",string]!="Setupanim"&T["Parts",string]!="Doanim"){ T["Parts",string] = "Setupanim" }
 T["Count.data",number] = (T["Count.data",number] + 1) % EndCounter
 rangerFilter(Base)
# rangerFilter(PropLock["Locker",entity])
 local TempA = rangerOffset(Glength,EndVector+Base:vel():setZ(0)/Vel,Base:up()*-1)
 T["RangerPos",vector]=TempA:position()
   switch (T["Parts",string]){
    case "Setupanim",
    if(TempA:hit()){
     if(T["Count.data",number]==StartAnim){
      if(T["StartPos.data",vector]:distance(  T["RangerPos",vector] )>=MaxDist){ 
       T["StartPos.data",vector] = T["Bezier",vector]
       T["Distance.data",number] = (T["StartPos.data",vector]- T["RangerPos",vector]):length()
       T["Middle.data",vector] = Base:up()+mix( T["RangerPos",vector],T["StartPos.data",vector],0.99) + Base:up()*T["Distance.data",number] / Sheight
       T["Parts",string] = "Doanim"
      }  
     }  
    }
    break  
    case "Doanim",
    T["Move_Count.data",number] = min(T["Move_Count.data",number] + Speed,1)
    T["Bezier",vector] = Base:up()+bezier(T["StartPos.data",vector],T["Middle.data",vector], T["RangerPos",vector],T["Move_Count.data",number])
      if(T["Move_Count.data",number]>=1){
      holoEntity(FeetHolo):soundPlay(FeetHolo*randint(999),1.3,"npc/dog/dog_footstep_run"+randint(1,8)+".wav")
      T["Move_Count.data",number] = 0
      T["Parts",string] = "Setupanim"
      }
    break
   }
  holoAng(FeetHolo,slerp(quat(holoEntity(FeetHolo):angles()), quat(Base:angles()),1):toAngle())
 }
function number co(A, B, C){ return acos((A^2 + B^2 - C^2) / (2*A*B)) }

function nik(L1,L2,Hip:vector,End:vector,I1,I2,Bs:entity){
local Axis=Bs:toLocalAxis(End-Hip)
local Angle=Axis:toAngle():setRoll(-bearing(Hip,Bs:angles(),End))
local Dist=min(Axis:length(),L1+L2)
local Quat=quat(Angle)*qRotation(vec(0,0+90+co(Dist,L1,L2),0))
holoAng(I1,Bs:toWorld(Quat:toAngle())+ang(0,0,0))
holoAng(I2,holoEntity(I1):toWorld(ang(co(L2, L1, Dist)+180,0,0)))
}


function vector entity:inverse(A, B, C, D, End:vector){
    local Length_1 = abs(holoEntity(B):toLocal(holoEntity(C):pos()):z())
    local Length_2 = abs(holoEntity(C):toLocal(holoEntity(D):pos()):z())
    local Vector = (holoEntity(B):pos()-End)
    local AxisA = This:toLocalAxis(Vector)
    local AxisB = holoEntity(A):toLocalAxis(Vector)
    local Distance = min(Vector:length(),Length_1 + Length_2)
    local Hip_Acos = acos( (Distance^2 + Length_1^2 - Length_2^2) / ((Length_1*2)*Distance))
    local Knee_Acos = acos( (Length_1^2 + Length_2^2 - Distance^2) / ((Length_1*2)*Length_1))
    local Inverse = atan(AxisB[1],AxisB[3])
    holoAng(A,This:toWorld(ang(0,0,90+atan(AxisA[3],AxisA[2]))))
    holoAng(B,holoEntity(A):toWorld(ang(Hip_Acos-180+Inverse,0,0)))
    holoAng(C,holoEntity(B):toWorld(ang(180+Knee_Acos,0,0)))
    holoAng(D, ang(180 + (inrange(abs(Inverse), 160, 180) ? 180 : Inverse < 0 ? -Inverse : 180 + (180 - Inverse)), This:angles():yaw(), 0))

}


function vector entity:inverse(A,B,C,End:vector){
local D1 =  abs(holoEntity(A):toLocal(holoEntity(B):pos()):z())
local D2 =  abs(holoEntity(B):toLocal(holoEntity(C):pos()):z())
local Vec = (holoEntity(A):pos()-End)
local VecDist = min(This:toLocalAxis(Vec):length(),D1+D2)
local AxisA = This:toLocalAxis(Vec)
local Ang = AxisA:toAngle():setRoll(-bearing(holoEntity(A):pos(),This:angles(),End))
local HipAng = quat(Ang)*qRotation(vec(0,90+co(VecDist,D1,D2),0))

holoAng(A,This:toWorld(HipAng:toAngle()))
#holoAng(B,holoEntity(A):toWorld(ang(Knee+0,0,0)))
}
function animate(){
T:anim(0.20,1,5,4,E:toWorld(vec(0,0,0)),100,5,8,E,3)
#E:inverse(10,1,2,3,T["Bezier",vector])
E:inverse(1,2,3,T["Bezier",vector])
}

}
animate()

@name MechAnimANDbezier
@inputs 
@outputs 
@persist T:table
@trigger 
@model models/hunter/blocks/cube025x025x025.mdl
interval(100)
E = entity()

if(first()){
function number c(I,V:vector,S:vector,A:angle,Col:vector,Model:string,Alpha:number){ holoCreate(I,V,S,A,Col,Model) holoAlpha(I,Alpha) }
function number c(I,V:vector,S:vector,A:angle,Col:vector,Model:string,Parent:entity,Alpha:number){ holoCreate(I,V,S,A,Col,Model) holoAlpha(I,Alpha) holoParent(I,Parent) }
function number c(I,V:vector,S:vector,A:angle,Col:vector,Model:string,Parent:number,Alpha:number){ holoCreate(I,V,S,A,Col,Model) holoAlpha(I,Alpha) holoParent(I,Parent) }
function number cosine (One, Two, Three){ return acos( (One^2 + Two^2 - Three^2) / ((Two*2)*One) ) }
c(10,E:toWorld(vec(0,0,0)),vec(1),ang(),vec(255),"cube",E,0)  

c(1,holoEntity(10):toWorld(vec(0,0,0)),vec(1),ang(),vec(255,0,0),"cube",10,255)  
c(2,holoEntity(1):toWorld(vec(0,0,50)),vec(1),ang(),vec(0,0,255),"cube",1,255)      
c(3,holoEntity(2):toWorld(vec(0,0,50)),vec(1),ang(),vec(0,255,0),"cube",2,255)   


function vector bezierCurve(V1:vector,V2:vector,V3:vector,S){

local Fmix1 = mix(V3,V2,S)
local Fmix2 = mix(V2,V1,S)

local Mmix1 = mix(Fmix1,V2,S)
local Mmix2 = mix(V2,Fmix2,S)

local Emix1 = mix(Mmix1,V2,S)
local Emix2 = mix(Mmix2,V1,S)

return mix(Emix1,Emix2,S)
}

 function table:anim(Speed,StartAnim,EndCounter,Sheight,EndVector:vector,Glength,MaxDist,Vel,Base:entity,FeetHolo){
 local T = This
if(T["Parts",string]!="Setupanim"&T["Parts",string]!="Doanim"){ T["Parts",string] = "Setupanim" }
 T["Count.data",number] = (T["Count.data",number] + 1) % EndCounter
 rangerFilter(Base)
# rangerFilter(PropLock["Locker",entity])
 local TempA = rangerOffset(Glength,EndVector+Base:vel():setZ(0)/Vel,Base:up()*-1)
 T["RangerPos",vector]=TempA:position()
   switch (T["Parts",string]){
    case "Setupanim",
    if(TempA:hit()){
     if(T["Count.data",number]==StartAnim){
      if(T["StartPos.data",vector]:distance(  T["RangerPos",vector] )>=MaxDist){ 
       T["StartPos.data",vector] = T["Bezier",vector]
       T["Distance.data",number] = (T["StartPos.data",vector]- T["RangerPos",vector]):length()
       T["Middle.data",vector] = Base:up()+mix( T["RangerPos",vector],T["StartPos.data",vector],0.99) + Base:up()*T["Distance.data",number] / Sheight
       T["Parts",string] = "Doanim"
      }  
     }  
    }
    break  
    case "Doanim",
    T["Move_Count.data",number] = min(T["Move_Count.data",number] + Speed,1)
    T["Bezier",vector] = Base:up()+bezierCurve(T["StartPos.data",vector],T["Middle.data",vector], T["RangerPos",vector],T["Move_Count.data",number])
      if(T["Move_Count.data",number]>=1){
      holoEntity(FeetHolo):soundPlay(FeetHolo*randint(999),1.3,"npc/dog/dog_footstep_run"+randint(1,8)+".wav")
      T["Move_Count.data",number] = 0
      T["Parts",string] = "Setupanim"
      }
    break
   }
  holoAng(FeetHolo,slerp(quat(holoEntity(FeetHolo):angles()), quat(Base:angles()),1):toAngle())
 }
function number co(A, B, C){ return acos((A^2 + B^2 - C^2) / (2*A*B)) }

function nik(L1,L2,Hip:vector,End:vector,I1,I2,Bs:entity){
local Axis=Bs:toLocalAxis(End-Hip)
local Angle=Axis:toAngle():setRoll(-bearing(Hip,Bs:angles(),End))
local Dist=min(Axis:length(),L1+L2)
local Quat=quat(Angle)*qRotation(vec(0,0+90+co(Dist,L1,L2),0))
holoAng(I1,Bs:toWorld(Quat:toAngle())+ang(0,0,0))
holoAng(I2,holoEntity(I1):toWorld(ang(co(L2, L1, Dist)+180,0,0)))
}


function vector entity:inverse(A, B, C, D, End:vector){
    local Length_1 = abs(holoEntity(B):toLocal(holoEntity(C):pos()):z())
    local Length_2 = abs(holoEntity(C):toLocal(holoEntity(D):pos()):z())
    local Vector = (holoEntity(B):pos()-End)
    local AxisA = This:toLocalAxis(Vector)
    local AxisB = holoEntity(A):toLocalAxis(Vector)
    local Distance = min(Vector:length(),Length_1 + Length_2)
    local Hip_Acos = acos( (Distance^2 + Length_1^2 - Length_2^2) / ((Length_1*2)*Distance))
    local Knee_Acos = acos( (Length_1^2 + Length_2^2 - Distance^2) / ((Length_1*2)*Length_1))
    local Inverse = atan(AxisB[1],AxisB[3])
    holoAng(A,This:toWorld(ang(0,0,90+atan(AxisA[3],AxisA[2]))))
    holoAng(B,holoEntity(A):toWorld(ang(Hip_Acos-180+Inverse,0,0)))
    holoAng(C,holoEntity(B):toWorld(ang(180+Knee_Acos,0,0)))
    holoAng(D, ang(180 + (inrange(abs(Inverse), 160, 180) ? 180 : Inverse < 0 ? -Inverse : 180 + (180 - Inverse)), This:angles():yaw(), 0))

}


function vector entity:inverse(A,B,C,End:vector){
local D1 =  abs(holoEntity(A):toLocal(holoEntity(B):pos()):z())
local D2 =  abs(holoEntity(B):toLocal(holoEntity(C):pos()):z())
local Vec = (holoEntity(A):pos()-End)
local VecDist = min(This:toLocalAxis(Vec):length(),D1+D2)
local AxisA = This:toLocalAxis(Vec)
local Ang = AxisA:toAngle():setRoll(-bearing(holoEntity(A):pos(),This:angles(),End))
local HipAng = quat(Ang)*qRotation(vec(0,90+co(VecDist,D1,D2),0))

holoAng(A,This:toWorld(HipAng:toAngle()))
#holoAng(B,holoEntity(A):toWorld(ang(Knee+0,0,0)))
}
function animate(){
T:anim(0.20,1,5,4,E:toWorld(vec(0,0,0)),100,5,8,E,3)
E:inverse(10,1,2,3,T["Bezier",vector])
#E:inverse(1,2,3,T["Bezier",vector])
}

}
animate()

