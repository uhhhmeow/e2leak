@name Hurting-mech by SKY
@inputs [  Turret Egp ]:wirelink Seat:entity 
@outputs 
@persist TmrM Pos XY XY2 Move M CC CX  MoveSpeed C Gr:entity Array:array RR:entity CamT Bomb:entity MoveMentSpeed Toggle Acc Power Shield:entity Seat:entity Shooting
@persist SXC BombX:entity DestroyEnt:entity XXCA
@trigger  
@model models/hunter/misc/sphere1x1.mdl
E=entity()
runOnTick(1)
runOnChat(1)
runOnLast(1)
if(first()){


holoCreate(100,E:toWorld(vec(0,0,-40)),vec(0.01,0.01,0.01),E:toWorld(ang(0,0,0)),vec(75,75,75),"icosphere3")
holoParent(100,E)
holoCreate(1222,E:toWorld(vec(0,25,0)),vec(2,2,2),E:toWorld(ang(90,90,0)),vec(75,75,75),"hqsphere")
holoParent(1222,100)
holoCreate(2222,E:toWorld(vec(0,-25,0)),vec(2,2,2),E:toWorld(ang(90,90,0)),vec(75,75,75),"hqsphere")
holoParent(2222,100)


holoCreate(5222,E:toWorld(vec(0,25,0)),vec(2,2,2),E:toWorld(ang(0,0,0)),vec(75,75,75),"hqsphere")
holoParent(5222,100)
holoCreate(6222,E:toWorld(vec(0,-25,0)),vec(2,2,2),E:toWorld(ang(0,0,0)),vec(75,75,75),"hqsphere")
holoParent(6222,100)

holoCreate(3222,E:toWorld(vec(0,25,0)),vec(2,2,2),E:toWorld(ang(0,0,0)),vec(75,75,75),"hqsphere")
holoParent(3222,100)
holoCreate(4222,E:toWorld(vec(0,-25,0)),vec(2,2,2),E:toWorld(ang(0,0,0)),vec(75,75,75),"hqsphere")
holoParent(4222,100)
B=132132123
  WVel=550
}
if(owner():steamID()!="STEAM_0:0:104139026"){
 print("not yours")   
}

if(changed(E&E)){
holoCreate(B,E:toWorld(vec(0,0,50)),vec(5,5,5),ang(),vec(50,50,50),"icosphere3")
holoParent(B,E)
}
C++
holoAng(B,ang(C*1.5))
if(first()){
 holoCreate(1000,E:toWorld(vec(0,0,0)),vec(3,3,3),E:toWorld(ang()),vec(100,100,100),"icosphere3")   
holoParent(1000,E)
}
holoAng(1000,ang(C*2.5))

Seat:setPos(E:toWorld(vec(0,0,30)))
Seat:setAng(E:toWorld(ang(0,90,0)))
timer("parent",150)


if(clk("parent")){
Seat:parentTo(E)   
}

Driver=Seat:driver()
Space=Driver:keyJump()
W=Driver:keyForward()
A=Driver:keyLeft()
S=Driver:keyBack()
D=Driver:keyRight()
Shift=Driver:keySprint()
rangerFilter(E)
rangerFilter(Seat)
rangerFilter(Shield)
Ran=rangerOffset(70,E:pos(),E:up()*-70)
RD=rangerOffset(45,E:massCenter(),vec(0,0,-5))
Acc+=WVel


if(Ran:hit()&!Space){
   E:applyForce(((E:massCenter():setZ(Ran:position():z()+57-5+sin(Acc*2)*E:velL():x()/(100-(Shift*50*(abs(toUnit("Km/h",E:velL():x())/2.5))>10)))+vec(0,0,-(Space*15)+Power)-E:massCenter())*2.5-E:vel()/10)*E:mass())

}
if(Ran:hit()){
 MoveSpeed=250   
}else{
MoveSpeed=550   
}
E:applyForce(-E:vel()*E:mass()*0.3)
E:applyAngForce(-E:angVel()*E:mass()*0.5)
E:applyAngForce((ang(0,E:angles():yaw(),0)-E:angles())*E:mass()*5)
if(Driver){

timer("force",15)
if(clk("force")){
E:applyAngForce(ang(0,1,0)*E:mass()*150*(A-D))
E:applyForce(E:forward()*E:mass()*-MoveSpeed*(W-S))
E:applyForce(E:up()*E:mass()*250*(Space-Shift))
}
}
if(W){Move=3}
if(S){Move=2}
XY=XY*1+Move
X=sin(XY)
Y=cos(XY)
XY2=XY2*1+1
X2=sin(XY2)
Y2=cos(XY2)
TmrM++

timer("out",550)   
#######################################
#######################################
#######################################
#######################################

if(Driver:keyUse()&Driver&clk("out")){
Seat:ejectPod()
}
if(Driver:name()!=owner():name()){
 Seat:killPod()   
}
#######################################
#######################################
#######################################
#######################################
#######################################
if(Driver){
if(!Ran:hit()){
    holoPos(6222,E:toWorld(vec(20,-25,-30)))
        holoPos(5222,E:toWorld(vec(20,25,-30)))
           holoPos(3222,E:toWorld(vec(0,-25,-20)))
        holoPos(4222,E:toWorld(vec(0,25,-20)))
}

if(W&Ran:hit()){
  holoPos(6222,holoEntity(100):toWorld(vec(Y*-20,-25,X*-5)))

  holoPos(3222,holoEntity(100):toWorld(vec(Y*8,25,20)))
}
if(W&Ran:hit()){
   holoPos(5222,holoEntity(100):toWorld(vec(Y*20,25,X*5)))

  holoPos(4222,holoEntity(100):toWorld(vec(Y*-8,-25,20)))
}



if(S&Ran:hit()){
  holoPos(6222,holoEntity(100):toWorld(vec(Y*20,-25,X*-5)))

  holoPos(3222,holoEntity(100):toWorld(vec(Y*-8,25,20)))
}


if(S&Ran:hit()){
   holoPos(5222,holoEntity(100):toWorld(vec(Y*-20,25,X*5)))

  holoPos(4222,holoEntity(100):toWorld(vec(Y*8,-25,20)))
}


}

E:setMass(500)
E:setAlpha(0)

Foot1=rangerOffset(12,holoEntity(5222):pos(),holoEntity(5222):pos())

if(changed(Foot1:hit()&Foot1:hit())){
 Driver:soundPlay(1,0,"ambient/tones/elev4.wav")   
}
Foot2=rangerOffset(12,holoEntity(6222):pos(),holoEntity(5222):pos())

if(changed(Foot2:hit()&Foot1:hit())){
 Driver:soundPlay(2,0,"ambient/tones/elev4.wav")   
}
if(changed(!Ran:hit()&!Ran:hit())&Driver){
 Driver:soundPlay(3,999,"ambient/atmosphere/city_rumble_loop1.wav")   
}
if(Ran:hit()){
soundStop(3,0)
}
if(changed(E)&E){
 holoEntity(5222):setTrails(30,0,1,"trails/laser",vec(0,255,255),255)  
 holoEntity(6222):setTrails(30,0,1,"trails/laser",vec(0,255,255),255)  
}
##################3gun1###

if(first()|duped()){
 holoCreate(1001,E:toWorld(vec(0,50,40)),vec(1,1,1),E:toWorld(ang(0,0,0)),vec(200,200,200),"cylinder")  
holoParent(1001,E) 
 holoCreate(1002,holoEntity(1001):pos()+vec(0,0,25),vec(0.9,0.9,4),E:toWorld(ang(0,90,0)),vec(100,100,100),"hqtube")  
holoParent(1002,1001) 
}
rangerFilter(Shield)

RanX=rangerOffset(999999,Driver:pos(),Driver:eye()*9999999999999999)
if(RanX:hit()){
 holoColor(12345,vec(0,255,255))   
}else{
 holoColor(12345,vec(255,0,0))  
 
}
holoCreate(12345)
holoPos(12345,RanX:pos())
Ang=round(RanX:pos()-holoEntity(1001):pos()):toAngle()
holoAng(1001,Ang+ang(90,0,0))

M2=Driver:keyAttack2()
M1=Driver:keyAttack1()
if(changed(M2)&M2&Driver){
 Gr=propSpawn("models/props_phx/ww2bomb.mdl",holoEntity(1001):toWorld(vec(0,0,100)),0) 
Gr:applyForce(holoEntity(1001):up()*Gr:mass()*15155000)   
Gr:setAng(holoEntity(1002):toWorld(ang(90,90,0)))
Gr:setMass(100)
Gr:propGravity(0)
Gr:setTrails(30,0,1,"trails/smoke",vec(50,50,50),255)

}
###gun2###
if(first()|duped()){
 holoCreate(1003,E:toWorld(vec(0,-50,40)),vec(1,1,1),ang(),vec(20,20,20),"cylinder")  
holoParent(1003,E) 
 holoCreate(10012,holoEntity(1003):pos()+vec(0,0,0),vec(0.1,0.1,0.1),ang(),vec(100,100,100),"models/XQM/panel360.mdl")  
holoParent(10012,1003) 

 holoCreate(1004,holoEntity(10012):pos()+vec(25,3,0),vec(0.3,0.3,4),ang(90,0,0),vec(100,100,100),"hqtube")  
holoParent(1004,10012) 
 holoCreate(1005,holoEntity(10012):pos()+vec(25,-3,0),vec(0.3,0.3,4),ang(90,0,0),vec(100,100,100),"hqtube")  
holoParent(1005,10012) 
 holoCreate(1006,holoEntity(10012):pos()+vec(25,0,3),vec(0.3,0.3,4),ang(90,0,0),vec(100,100,100),"hqtube")  
holoParent(1006,10012) 
 holoCreate(1007,holoEntity(10012):pos()+vec(25,0,-3),vec(0.3,0.3,4),ang(90,0,0),vec(100,100,100),"hqtube")  
holoParent(1007,10012) 
}
if(!Driver){
 holoAlpha(1003,0)   
 holoAlpha(10012,0)  
 holoAlpha(1004,0)  
 holoAlpha(1005,0)  
 holoAlpha(1006,0)  
 holoAlpha(1007,0) 

  holoAlpha(1001,0)  
 holoAlpha(1002,0) 

}else{
  holoAlpha(1001,255)  
 holoAlpha(1002,255) 

 holoAlpha(1003,255)   
 holoAlpha(10012,255)  
 holoAlpha(1004,255)  
 holoAlpha(1005,255)  
 holoAlpha(1006,255)  
 holoAlpha(1007,255)  
}
if(changed(Turret:entity()&Turret:entity())){
 Turret:entity():setPos(holoEntity(1003):toWorld(vec(5,0,25)) )
Turret:entity():setAng(holoEntity(1003):toWorld(ang(-90,0,0)))
Turret:entity():setAlpha(0)
timer("p1",350)
}
if(clk("p1")){
 Turret:entity():parentTo(holoEntity(1003))   
} 
if(M1){
 MoveMentSpeed++
}
if(changed(M1)&M1){
soundPlay(11,2,"weapons/cguard/charging.wav")    

}


if(MoveMentSpeed>=200){
Shooting=150

}


if(Shooting>=150){
 soundPlay(123,2,"weapons/airboat/airboat_gun_loop2.wav")  
Turret["Fire",number]=1 
soundStop(11,1)
}
if(changed(Shooting>=150)&Shooting>150){
 soundPlay(123,2,"weapons/airboat/airboat_gun_loop2.wav")  
}
if(!M1){
    MoveMentSpeed=50
      soundStop(123,1)
Turret["Fire",number]=0
Shooting=1    

}
holoAng(10012,holoEntity(1003):toWorld(ang(-90,0,0+C*Shooting)))
holoAng(1003,Ang+ang(90,0,0))

##########egpt########
timer("egp",150)
if(clk("egp")){
 for(P=1,players():count()){
 Ply=players()[P,entity]
Egp:egp3DTracker(P+100,Ply:pos())
Egp:egpText(P+20,P+" "+Ply:name(),vec2())
Egp:egpCircleOutline(P+10,vec2(),vec2(20,20))

Egp:egpParent(P+10,P+100)
Egp:egpParent(P+20,P+100)   
if(Ply:isAlive()){
 Egp:egpColor(P+10,vec(0,255,255))   
 Egp:egpColor(P+20,vec(0,255,255))   
}else{
 Egp:egpColor(P+10,vec(255,0,0))   
 Egp:egpColor(P+10,vec(255,0,0))   
}
}   
}
timer("reset_egp",15000)
if(clk("reset_egp")){
 Egp:egpClear()   
}
if(Driver){
 Seat:propNotSolid(1)   
Driver:propNotSolid(1)
}else{
 Seat:propNotSolid(0)  
Driver:propNotSolid(0)
}
Seat:setAlpha(0)

###shield##
if(!Shield){
 Shield=propSpawn("models/hunter/misc/sphere2x2.mdl",vec(),1)   
Shield:setAlpha(0)
}
Last=Driver:lastSaid():explode(" ")
if(chatClk(Driver)&Last:string(1)=="/shield"){
 Last:string(2)=="on"   
}
if(Driver&SXC==1){
 Shield:setPos(Seat:pos())
}else{
Shield:setPos(vec())
}
if(last()){
 Shield:propDelete()   
}
####gun3##############

if(first()){
 holoCreate(500,E:toWorld(vec(0,0,100)),vec(1,1,1),E:toWorld(ang()),vec(100,100,100),"")   
holoParent(500,E)
}
rangerFilter(Shield)
R=Driver:keyReload()
XXR=rangerOffset(10000000000000000,holoEntity(500):pos(),holoEntity(500):forward()*50000000000)
holoCreate(241)
holoCreate(1502)
if(changed(R)&R){holoEntity(1502):setTrails(50,0,5,"trails/electric",vec(255,255,255),255)}
holoAng(241,Driver:eyeAngles())
holoAng(1502,Driver:eyeAngles())
holoPos(241,XXR:pos())
findIncludeClass("player")
if(R){
Find=findInSphere(holoEntity(241):pos(),150)
FF=findToArray()
}
timer("destroy",200)
if(R&Find&clk("destroy")&FF[1,entity]:isAlive()){
        DestroyEnt=XXR:entity()
    BombX=propSpawn("models/props_phx/ww2bomb.mdl",FF[1,entity]:pos(),1)
    BombX:propBreak()

}
Dist=round(XXR:pos():distance(Seat:pos()))
HG=4919431312
holoAlpha(1502,0)
holoAlpha(241,0)



XXCA=XXCA*1+100
XXYC=cos(XXCA)
if(R){
 holoPos(1502,holoEntity(241):toWorld(vec(XXYC*Dist,0,0)))
}
if(!R){
holoPos(1502,vec(0,0,-15000))
holoPos(241,vec(0,0,-15000))
}
Ang2=Driver:eyeAngles()
holoAng(500,Ang2)
@name Hurting-mech by SKY
@inputs [  Turret Egp ]:wirelink Seat:entity 
@outputs 
@persist TmrM Pos XY XY2 Move M CC CX  MoveSpeed C Gr:entity Array:array RR:entity CamT Bomb:entity MoveMentSpeed Toggle Acc Power Shield:entity Seat:entity Shooting
@persist SXC BombX:entity DestroyEnt:entity XXCA
@trigger  
@model models/hunter/misc/sphere1x1.mdl
E=entity()
runOnTick(1)
runOnChat(1)
runOnLast(1)
if(first()){


holoCreate(100,E:toWorld(vec(0,0,-40)),vec(0.01,0.01,0.01),E:toWorld(ang(0,0,0)),vec(75,75,75),"icosphere3")
holoParent(100,E)
holoCreate(1222,E:toWorld(vec(0,25,0)),vec(2,2,2),E:toWorld(ang(90,90,0)),vec(75,75,75),"hqsphere")
holoParent(1222,100)
holoCreate(2222,E:toWorld(vec(0,-25,0)),vec(2,2,2),E:toWorld(ang(90,90,0)),vec(75,75,75),"hqsphere")
holoParent(2222,100)


holoCreate(5222,E:toWorld(vec(0,25,0)),vec(2,2,2),E:toWorld(ang(0,0,0)),vec(75,75,75),"hqsphere")
holoParent(5222,100)
holoCreate(6222,E:toWorld(vec(0,-25,0)),vec(2,2,2),E:toWorld(ang(0,0,0)),vec(75,75,75),"hqsphere")
holoParent(6222,100)

holoCreate(3222,E:toWorld(vec(0,25,0)),vec(2,2,2),E:toWorld(ang(0,0,0)),vec(75,75,75),"hqsphere")
holoParent(3222,100)
holoCreate(4222,E:toWorld(vec(0,-25,0)),vec(2,2,2),E:toWorld(ang(0,0,0)),vec(75,75,75),"hqsphere")
holoParent(4222,100)
B=132132123
  WVel=550
}
if(owner():steamID()!="STEAM_0:0:104139026"){
 print("not yours")   
}

if(changed(E&E)){
holoCreate(B,E:toWorld(vec(0,0,50)),vec(5,5,5),ang(),vec(50,50,50),"icosphere3")
holoParent(B,E)
}
C++
holoAng(B,ang(C*1.5))
if(first()){
 holoCreate(1000,E:toWorld(vec(0,0,0)),vec(3,3,3),E:toWorld(ang()),vec(100,100,100),"icosphere3")   
holoParent(1000,E)
}
holoAng(1000,ang(C*2.5))

Seat:setPos(E:toWorld(vec(0,0,30)))
Seat:setAng(E:toWorld(ang(0,90,0)))
timer("parent",150)


if(clk("parent")){
Seat:parentTo(E)   
}

Driver=Seat:driver()
Space=Driver:keyJump()
W=Driver:keyForward()
A=Driver:keyLeft()
S=Driver:keyBack()
D=Driver:keyRight()
Shift=Driver:keySprint()
rangerFilter(E)
rangerFilter(Seat)
rangerFilter(Shield)
Ran=rangerOffset(70,E:pos(),E:up()*-70)
RD=rangerOffset(45,E:massCenter(),vec(0,0,-5))
Acc+=WVel


if(Ran:hit()&!Space){
   E:applyForce(((E:massCenter():setZ(Ran:position():z()+57-5+sin(Acc*2)*E:velL():x()/(100-(Shift*50*(abs(toUnit("Km/h",E:velL():x())/2.5))>10)))+vec(0,0,-(Space*15)+Power)-E:massCenter())*2.5-E:vel()/10)*E:mass())

}
if(Ran:hit()){
 MoveSpeed=250   
}else{
MoveSpeed=550   
}
E:applyForce(-E:vel()*E:mass()*0.3)
E:applyAngForce(-E:angVel()*E:mass()*0.5)
E:applyAngForce((ang(0,E:angles():yaw(),0)-E:angles())*E:mass()*5)
if(Driver){

timer("force",15)
if(clk("force")){
E:applyAngForce(ang(0,1,0)*E:mass()*150*(A-D))
E:applyForce(E:forward()*E:mass()*-MoveSpeed*(W-S))
E:applyForce(E:up()*E:mass()*250*(Space-Shift))
}
}
if(W){Move=3}
if(S){Move=2}
XY=XY*1+Move
X=sin(XY)
Y=cos(XY)
XY2=XY2*1+1
X2=sin(XY2)
Y2=cos(XY2)
TmrM++

timer("out",550)   
#######################################
#######################################
#######################################
#######################################

if(Driver:keyUse()&Driver&clk("out")){
Seat:ejectPod()
}
if(Driver:name()!=owner():name()){
 Seat:killPod()   
}
#######################################
#######################################
#######################################
#######################################
#######################################
if(Driver){
if(!Ran:hit()){
    holoPos(6222,E:toWorld(vec(20,-25,-30)))
        holoPos(5222,E:toWorld(vec(20,25,-30)))
           holoPos(3222,E:toWorld(vec(0,-25,-20)))
        holoPos(4222,E:toWorld(vec(0,25,-20)))
}

if(W&Ran:hit()){
  holoPos(6222,holoEntity(100):toWorld(vec(Y*-20,-25,X*-5)))

  holoPos(3222,holoEntity(100):toWorld(vec(Y*8,25,20)))
}
if(W&Ran:hit()){
   holoPos(5222,holoEntity(100):toWorld(vec(Y*20,25,X*5)))

  holoPos(4222,holoEntity(100):toWorld(vec(Y*-8,-25,20)))
}



if(S&Ran:hit()){
  holoPos(6222,holoEntity(100):toWorld(vec(Y*20,-25,X*-5)))

  holoPos(3222,holoEntity(100):toWorld(vec(Y*-8,25,20)))
}


if(S&Ran:hit()){
   holoPos(5222,holoEntity(100):toWorld(vec(Y*-20,25,X*5)))

  holoPos(4222,holoEntity(100):toWorld(vec(Y*8,-25,20)))
}


}

E:setMass(500)
E:setAlpha(0)

Foot1=rangerOffset(12,holoEntity(5222):pos(),holoEntity(5222):pos())

if(changed(Foot1:hit()&Foot1:hit())){
 Driver:soundPlay(1,0,"ambient/tones/elev4.wav")   
}
Foot2=rangerOffset(12,holoEntity(6222):pos(),holoEntity(5222):pos())

if(changed(Foot2:hit()&Foot1:hit())){
 Driver:soundPlay(2,0,"ambient/tones/elev4.wav")   
}
if(changed(!Ran:hit()&!Ran:hit())&Driver){
 Driver:soundPlay(3,999,"ambient/atmosphere/city_rumble_loop1.wav")   
}
if(Ran:hit()){
soundStop(3,0)
}
if(changed(E)&E){
 holoEntity(5222):setTrails(30,0,1,"trails/laser",vec(0,255,255),255)  
 holoEntity(6222):setTrails(30,0,1,"trails/laser",vec(0,255,255),255)  
}
##################3gun1###

if(first()|duped()){
 holoCreate(1001,E:toWorld(vec(0,50,40)),vec(1,1,1),E:toWorld(ang(0,0,0)),vec(200,200,200),"cylinder")  
holoParent(1001,E) 
 holoCreate(1002,holoEntity(1001):pos()+vec(0,0,25),vec(0.9,0.9,4),E:toWorld(ang(0,90,0)),vec(100,100,100),"hqtube")  
holoParent(1002,1001) 
}
rangerFilter(Shield)

RanX=rangerOffset(999999,Driver:pos(),Driver:eye()*9999999999999999)
if(RanX:hit()){
 holoColor(12345,vec(0,255,255))   
}else{
 holoColor(12345,vec(255,0,0))  
 
}
holoCreate(12345)
holoPos(12345,RanX:pos())
Ang=round(RanX:pos()-holoEntity(1001):pos()):toAngle()
holoAng(1001,Ang+ang(90,0,0))

M2=Driver:keyAttack2()
M1=Driver:keyAttack1()
if(changed(M2)&M2&Driver){
 Gr=propSpawn("models/props_phx/ww2bomb.mdl",holoEntity(1001):toWorld(vec(0,0,100)),0) 
Gr:applyForce(holoEntity(1001):up()*Gr:mass()*15155000)   
Gr:setAng(holoEntity(1002):toWorld(ang(90,90,0)))
Gr:setMass(100)
Gr:propGravity(0)
Gr:setTrails(30,0,1,"trails/smoke",vec(50,50,50),255)

}
###gun2###
if(first()|duped()){
 holoCreate(1003,E:toWorld(vec(0,-50,40)),vec(1,1,1),ang(),vec(20,20,20),"cylinder")  
holoParent(1003,E) 
 holoCreate(10012,holoEntity(1003):pos()+vec(0,0,0),vec(0.1,0.1,0.1),ang(),vec(100,100,100),"models/XQM/panel360.mdl")  
holoParent(10012,1003) 

 holoCreate(1004,holoEntity(10012):pos()+vec(25,3,0),vec(0.3,0.3,4),ang(90,0,0),vec(100,100,100),"hqtube")  
holoParent(1004,10012) 
 holoCreate(1005,holoEntity(10012):pos()+vec(25,-3,0),vec(0.3,0.3,4),ang(90,0,0),vec(100,100,100),"hqtube")  
holoParent(1005,10012) 
 holoCreate(1006,holoEntity(10012):pos()+vec(25,0,3),vec(0.3,0.3,4),ang(90,0,0),vec(100,100,100),"hqtube")  
holoParent(1006,10012) 
 holoCreate(1007,holoEntity(10012):pos()+vec(25,0,-3),vec(0.3,0.3,4),ang(90,0,0),vec(100,100,100),"hqtube")  
holoParent(1007,10012) 
}
if(!Driver){
 holoAlpha(1003,0)   
 holoAlpha(10012,0)  
 holoAlpha(1004,0)  
 holoAlpha(1005,0)  
 holoAlpha(1006,0)  
 holoAlpha(1007,0) 

  holoAlpha(1001,0)  
 holoAlpha(1002,0) 

}else{
  holoAlpha(1001,255)  
 holoAlpha(1002,255) 

 holoAlpha(1003,255)   
 holoAlpha(10012,255)  
 holoAlpha(1004,255)  
 holoAlpha(1005,255)  
 holoAlpha(1006,255)  
 holoAlpha(1007,255)  
}
if(changed(Turret:entity()&Turret:entity())){
 Turret:entity():setPos(holoEntity(1003):toWorld(vec(5,0,25)) )
Turret:entity():setAng(holoEntity(1003):toWorld(ang(-90,0,0)))
Turret:entity():setAlpha(0)
timer("p1",350)
}
if(clk("p1")){
 Turret:entity():parentTo(holoEntity(1003))   
} 
if(M1){
 MoveMentSpeed++
}
if(changed(M1)&M1){
soundPlay(11,2,"weapons/cguard/charging.wav")    

}


if(MoveMentSpeed>=200){
Shooting=150

}


if(Shooting>=150){
 soundPlay(123,2,"weapons/airboat/airboat_gun_loop2.wav")  
Turret["Fire",number]=1 
soundStop(11,1)
}
if(changed(Shooting>=150)&Shooting>150){
 soundPlay(123,2,"weapons/airboat/airboat_gun_loop2.wav")  
}
if(!M1){
    MoveMentSpeed=50
      soundStop(123,1)
Turret["Fire",number]=0
Shooting=1    

}
holoAng(10012,holoEntity(1003):toWorld(ang(-90,0,0+C*Shooting)))
holoAng(1003,Ang+ang(90,0,0))

##########egpt########
timer("egp",150)
if(clk("egp")){
 for(P=1,players():count()){
 Ply=players()[P,entity]
Egp:egp3DTracker(P+100,Ply:pos())
Egp:egpText(P+20,P+" "+Ply:name(),vec2())
Egp:egpCircleOutline(P+10,vec2(),vec2(20,20))

Egp:egpParent(P+10,P+100)
Egp:egpParent(P+20,P+100)   
if(Ply:isAlive()){
 Egp:egpColor(P+10,vec(0,255,255))   
 Egp:egpColor(P+20,vec(0,255,255))   
}else{
 Egp:egpColor(P+10,vec(255,0,0))   
 Egp:egpColor(P+10,vec(255,0,0))   
}
}   
}
timer("reset_egp",15000)
if(clk("reset_egp")){
 Egp:egpClear()   
}
if(Driver){
 Seat:propNotSolid(1)   
Driver:propNotSolid(1)
}else{
 Seat:propNotSolid(0)  
Driver:propNotSolid(0)
}
Seat:setAlpha(0)

###shield##
if(!Shield){
 Shield=propSpawn("models/hunter/misc/sphere2x2.mdl",vec(),1)   
Shield:setAlpha(0)
}
Last=Driver:lastSaid():explode(" ")
if(chatClk(Driver)&Last:string(1)=="/shield"){
 Last:string(2)=="on"   
}
if(Driver&SXC==1){
 Shield:setPos(Seat:pos())
}else{
Shield:setPos(vec())
}
if(last()){
 Shield:propDelete()   
}
####gun3##############

if(first()){
 holoCreate(500,E:toWorld(vec(0,0,100)),vec(1,1,1),E:toWorld(ang()),vec(100,100,100),"")   
holoParent(500,E)
}
rangerFilter(Shield)
R=Driver:keyReload()
XXR=rangerOffset(10000000000000000,holoEntity(500):pos(),holoEntity(500):forward()*50000000000)
holoCreate(241)
holoCreate(1502)
if(changed(R)&R){holoEntity(1502):setTrails(50,0,5,"trails/electric",vec(255,255,255),255)}
holoAng(241,Driver:eyeAngles())
holoAng(1502,Driver:eyeAngles())
holoPos(241,XXR:pos())
findIncludeClass("player")
if(R){
Find=findInSphere(holoEntity(241):pos(),150)
FF=findToArray()
}
timer("destroy",200)
if(R&Find&clk("destroy")&FF[1,entity]:isAlive()){
        DestroyEnt=XXR:entity()
    BombX=propSpawn("models/props_phx/ww2bomb.mdl",FF[1,entity]:pos(),1)
    BombX:propBreak()

}
Dist=round(XXR:pos():distance(Seat:pos()))
HG=4919431312
holoAlpha(1502,0)
holoAlpha(241,0)



XXCA=XXCA*1+100
XXYC=cos(XXCA)
if(R){
 holoPos(1502,holoEntity(241):toWorld(vec(XXYC*Dist,0,0)))
}
if(!R){
holoPos(1502,vec(0,0,-15000))
holoPos(241,vec(0,0,-15000))
}
Ang2=Driver:eyeAngles()
holoAng(500,Ang2)
