@name Holo-Tank
@inputs XWL:wirelink Cam:wirelink
@outputs View:vector Angle:angle
@persist [Pod E Turret]:entity
@outputs [BulletArray BulletGravArray ModeArray DamageArray2]:array
@persist Speed SavedTime Fixed
@persist ShootCount HoloID HoloIDLength Scale Radius DealDamage DealExplosionDamage DealRadius DealFalloff Health
@persist G V Mass IfDef
@persist  T C C2 C3 C4 T2 T3 T4 T5 T6 T7 T8 T9 T10 R1 S GunSec Flir CamTog Prop:entity Props:array Ranger:ranger Craft:entity CamPos:vector Bullete:entity  Cam:wirelink   PWrk VSV Ign VelLL Mul Ntrpl HoverHeight FlyAng SpeedMultiplier YawSpeed
@trigger 
@model

if(CamTog >= 2){CamTog = 0}
if(CamTog == 0){CamPos = E:pos()+E:forward()*-40+E:up()*30}
if(CamTog == 1){CamPos = E:pos()+E:forward()*0+E:up()*0}
Cam["Position",vector] = CamPos
Cam["Activated",number] = Pod["Active",number]
Cam["Angle",angle] = Pod["Entity",entity]:driver():eyeAngles()
if(changed(Pod["R",number])&Pod["R",number]){Flir = Flir + 1}
if(Flir >= 2){Flir = 0}
Cam["FLIR",number] = Flir
if(changed(Pod["Mouse2",number])&Pod["Mouse2",number]){CamTog = CamTog + 1}
#Camera Position Toggle

if(changed(Pod["Mouse2",number])&Pod["Mouse2",number]){CamTog = CamTog + 1}

if(CamTog >= 2){CamTog = 0}

if(CamTog == 0){CamPos = E:pos()+E:forward()*-1+E:up()*1}

if(CamTog == 1){CamPos = E:pos()+E:forward()*10+E:up()*50}

    

#Camera

Cam["Position",vector] = CamPos

Cam["Activated",number] = Pod["Active",number]

Cam["Angle",angle] = Pod["Entity",entity]:driver():eyeAngles()

    

#Camera FLIR Vision

if(changed(Pod["R",number])&Pod["R",number]){Flir = Flir + 1}

if(Flir >= 2){Flir = 0}

Cam["FLIR",number] = Flir

#Turret Timer

GunSec++



#Turret Fire

if(Pod["Mouse1",number] & changed(int(GunSec/5))){

    Ranger = rangerOffset(CamPos,CamPos+((vec(25000,0,0)):rotate(Pod["Entity",entity]:driver():eyeAngles())))

    Bullete:propBreak()

    Bullete = propSpawn("models/props_phx/mk-82.mdl",Ranger:pos()+vec(0,0,50),ang(90,0,0),1)

    Bullete:setAlpha(0)

}

Bullete:propBreak()

#First
if (duped() | inputClk()) {
    reset()
}

if (first()) {
    runOnTick(1)
    runOnChat(1)
    
    gSetStr("Damage","")
    signalSetGroup("HCS")
    runOnSignal("Damage",1,1)
    gShare(1)
    
    Pod = XWL["Entity",entity]
    
    Scale = 1
    Color = vec(255,255,255)
    
    G = 9.82*2
    Mass = 100
    
    ShootCount = 19
    Radius = 5
    DealDamage = 10
    DealExplosionDamage = 42
    DealRadius = 25
    DealFalloff = 0.1
    Health = 100
    
    
    holoCreate(1)
    holoScaleUnits(1,vec(1,1,1))  
    E = holoEntity(1)
    HoloID = E:id()
    HoloIDLength = HoloID:toString():length()
    
    holoCreate(2)
    holoModel(2,"hqcylinder")
    holoColor(2,Color)
    holoScaleUnits(2,vec(120.5,40,70.5))
    holoPos(2,E:toWorld(vec(0,0,0)))
    holoAng(2,E:toWorld(ang(0,0,90)))
    holoParent(2,E)
    
    holoCreate(3)
    holoModel(3,"hqcylinder")
    holoColor(3,vec(0,0,0))
    holoScaleUnits(3,vec(150,40,20.5))   
    holoPos(3,E:toWorld(vec(-2.5,5,-1.5)))
    holoAng(3,E:toWorld(ang(0,0,90)))
    holoParent(3,E)
    holoCreate(4)
    holoModel(4,"hqcylinder")
    holoColor(4,vec(0,0,0))
    holoScaleUnits(4,vec(150,40,20.5))   
    holoPos(4,E:toWorld(vec(-2.5,-5,-1.5)))
    holoAng(4,E:toWorld(ang(0,0,90)))
    holoParent(4,E)
    
    holoCreate(5)
    holoModel(5,"hqcylinder")
    holoColor(5,Color)
    holoScaleUnits(5,vec(16,3,3))   
    holoPos(5,E:toWorld(vec(-2.5,5,-0.5)))
    holoAng(5,E:toWorld(ang(0,0,90)))
    holoParent(5,E)
    holoCreate(6)
    holoModel(6,"hqcylinder")
    holoColor(6,Color)
    holoScaleUnits(6,vec(160,30,30))   
    holoPos(6,E:toWorld(vec(-2.5,-5,-1.5)))
    holoAng(6,E:toWorld(ang(0,0,90)))
    holoParent(6,E)
    
    holoCreate(7)
    holoModel(7,"hqcylinder")
    holoColor(7,Color)
    holoScaleUnits(7,vec(50,50,10))   
    holoPos(7,E:toWorld(vec(0,0,20)))
    holoAng(7,ang(0,0,0))
    holoParent(7,E)
    Turret = holoEntity(7)
    
    holoCreate(8)
    holoModel(8,"dome")
    holoColor(8,Color)
    holoScaleUnits(8,vec(100,70.5,30.5))   
    holoPos(8,Turret:toWorld(vec(0,0,0)))
    holoAng(8,ang(0,0,0))
    holoParent(8,Turret)
    
    holoCreate(9)
    holoModel(9,"cube")
    holoColor(9,Color)
    holoScaleUnits(9,vec(30,50,10))   
    holoPos(9,Turret:toWorld(vec(-4,0,10.5)))
    holoAng(9,ang(10,0,0))
    holoParent(9,Turret)
    
    holoCreate(10)
    holoModel(10,"dome")
    holoColor(10,Color)
    holoScaleUnits(10,vec(30,30,20))   
    holoPos(10,Turret:toWorld(vec(-1,0,10)))
    holoAng(10,ang(0,0,0))
    holoParent(10,Turret)
    
    holoCreate(11)
    holoModel(11,"prism")
    holoColor(11,Color)
    holoScaleUnits(11,vec(20,10,10))   
    holoPos(11,Turret:toWorld(vec(20,0,10.4)))
    holoAng(11,ang(25,0,0))
    holoParent(11,Turret)
     holoCreate(12)
    holoModel(12,"hqcylinder")
    holoColor(12,Color)
    holoScaleUnits(12,vec(10.5,10.5,20))   
    holoPos(12,Turret:toWorld(vec(49,0,5.5)))
    holoAng(12,ang(90,0,0))
    holoParent(12,Turret)
    
    holoCreate(13)
    holoModel(13,"hqcylinder")
    holoColor(13,Color)
    holoScaleUnits(13,vec(10,10,50))   
    holoPos(13,Turret:toWorld(vec(7,0,0.5)))
    holoAng(13,ang(90,0,0))
    holoParent(13,Turret)
    
    holoCreate(18)
    holoModel(18,"hqcylinder")
    holoMaterial(18,"models/effects/splode_sheet")
    holoAlpha(18,0)
    holoScaleUnits(18,vec(10,10,10000))
    holoPos(18,Turret:toWorld(vec(300,0,0.5)))
    holoAng(18,ang(90,0,0))
    holoParent(18,Turret)
    
    holoCreate(19)
    holoModel(19,"hqsphere")
    holoMaterial(19,"models/effects/splode_sheet")
    holoAlpha(19,0)
    holoScaleUnits(19,vec(1000,1000,1000))

}

#Stuff
Time = curtime()
DeltaTime = Time-SavedTime
DRIVER = Pod:driver()

if (changed(DRIVER) & DRIVER) {
    timer("physics",0)
    timer("bullets",0)
    Health = 100
} elseif (changed(DRIVER)) {
    Angle = entity():angles()
    Pos = entity():pos()
    holoPos(1,Pos)
    holoAng(1,Angle)
    #Pod:setView(0,E:pos(),E:angles())
}

if (DRIVER) {
    #Wirelinks
    W = XWL["W",number]
    S = XWL["S",number]
    A = XWL["A",number]
    D = XWL["D",number]
    Space = XWL["Space",number]
    Shift = XWL["Shift",number]
    Alt = XWL["Alt",number]
    F = XWL["Light",number]
    LMB = XWL["Mouse1",number]
    RMB = XWL["Mouse2",number]
    
    if (changed(F) & F) {
        Fixed = !Fixed
    }
    
    #Steer
    Pi = pi()
    Pos = E:pos()
    Forward = E:forward()
    Right = E:right()
    Up = E:up()
    
    Speed = ((2+Shift/3)/(2+Alt))/3
    Pos+= Forward*Speed*(W-S/2)
    Yaw = Speed/2*(A-D)
    
    #Collision detection for forward and backward, will enable when they can drive on slopes.
#    RD = rangerOffset(Speed+10,Pos,Forward)
#    if (RD:hit()) {
#        Pos = E:pos()
#    }
#    RD = rangerOffset(Speed+15,Pos,-Forward)
#    if (RD:hit()) {
#        Pos = E:pos()
#    }
   
 
    
   
    #Get Aim-stuff
    RD = rangerOffset(1337,Pos,E:forward())
    AimEntity = RD:entity()
    Owner = AimEntity:owner()
    AimPos = RD:position()
    
    #Shoot/Use
    if (changed(LMB)) {
        holoEntity(13):soundPlay(2,99,"synth/white_noise.wav")
        soundVolume(2,0.4) 
    }
    
    if (clk("laser") & LMB) {
        ID = HoloID:toString()
        
        DamageArray = array(1,Time,""+1,Pos,Turret:forward(),1000,ID,1)
    
        gSetStr("Damage",glonEncode(DamageArray))
        
        signalSetGroup("HCS")
        signalSend("Damage",1)
        
        holoAlpha(18,150)
        timer("laser",100)
    } elseif (!LMB) {
        soundStop(2)
        holoAlpha(18,0)
    }
    
    if (clk("Cannon") & RMB) {
        if (BulletArray:count() < 10) {
            if (ShootCount < 29) {
                ShootCount++
            } else {
                ShootCount = 20
            }
            if (ModeArray[BulletArray[ShootCount,number],number] != 1) {
                holoCreate(ShootCount)
                holoModel(ShootCount,"hqsphere")
                holoScaleUnits(ShootCount,vec(1,1,1))
                holoMaterial(ShootCount,"")
                holoAlpha(ShootCount,255)
                holoPos(ShootCount,Turret:toWorld(vec(10,0,0)))
                holoAng(ShootCount,Turret:angles())
                Count = BulletArray:count()+1
                BulletArray[Count,number]=ShootCount
                BulletGravArray[Count,number]=-pi()
                ModeArray[Count,number]=1
                
                holoEntity(13):soundPlay(1,0.1,"synth/brown_noise.wav") 
            }
            timer("Cannon",1000)
        }
    }
    
    #Set holoPos, holoAng and view
    holoPos(1,Pos)
    holoAng(1,E:toWorld(ang(0,Yaw,0)))
    
    Eye = DRIVER:eye()
    if (!Fixed) {
        View = E:pos()-Eye*25*(1-Space)
    } else {
        View = E:toWorld(-Eye*25*(1-Space))
    }
    ViewRD = rangerOffset(50,View+vec(0,0,50),vec(0,0,-1))
    if (ViewRD:hit()) {
        View = View:setZ(ViewRD:position():z()+5)
    }
    
    if (Space) {
        Angle = DRIVER:eyeAngles()
        View = View+Turret:up()*4*Space+Turret:forward()*3*Space
    } else {
        Angle = (Turret:pos()-View):toAngle()
    }
    #Pod:setView(1,View,Angle)
    
    if (Space) {
        holoAng(7,Angle)
    } else {
        holoAng(7,E:toWorld(ang(0,Angle:yaw()-E:angles():yaw(),0)))
    }
     #ifdef entity:hudRemoveElement(number)
        #Hud
        if (AimEntity:isPlayer()) {
            Driver:hudDrawText(1,AimEntity:name(),50,50,vec(100,200,50),255,1,pi())
        } elseif (AimEntity) {
            Driver:hudDrawText(1,"Owner: "+AimEntity:owner():name(),50,50,vec(100,200,50),255,1,pi())
        } else {
            Driver:hudRemoveElement(1)
        }
        IfDef = 1
        Driver:hudDrawRBox(2,1,40,85,20,3,vec(255,0,0),255)
        Driver:hudDrawRBox(3,1,40,85,(Health/100)*20,3,vec(0,255,0),255)
    #else
        if (IfDef & IfDef != 2) {
            Pod:printDriver("Cant use huds on this srv, so you wont see a healthbar.")
            IfDef = 2
        } elseif (IfDef != 2) {
            IfDef = 1
        }
    #endif
}



#########
#Bullets#
#########

if (clk("bullets")) {
    BulletTime = 0.05
    BulletSpeed = 1500*BulletTime
    for (I=1,BulletArray:count()){
        BulletMode = ModeArray[I,number]
        if (BulletMode == 1) {
            BulletGravity = BulletGravArray[I,number]+G*BulletTime
            BulletGravArray[I,number] = BulletGravity
            Bullet = BulletArray[I,number]
            BulletEnt = holoEntity(Bullet)
            Origin = BulletEnt:pos()
            DirVec = BulletEnt:toWorld(vec(BulletSpeed,0,0))-vec(0,0,BulletGravity)
            DirVec = DirVec-BulletEnt:pos()
            
            
            #Damage
            ID = HoloID:toString()+"-"+BulletEnt:id():toString()
            
            HitArray = glonDecode(gGetStr(ID+"Hit"))
            HitVec = HitArray[1,vector]
            
            RD = rangerOffset(BulletSpeed,Origin,DirVec)
            
            if (HitVec | RD:hit()) {
                if (HitVec) {
                    gSetStr(ID+"Hit","")
                } elseif (RD:hit()) {
                    DamageArray = array(2,Time,DealExplosionDamage,DealRadius,DealFalloff,RD:position(),ID)
                    gSetStr("Damage",glonEncode(DamageArray))
                    
                    HitVec = RD:position()
                    HitEnt = RD:entity()
                    if (HitEnt) {
                        HitEnt:applyOffsetForce(DirVec:normalized()*250,HitVec)
                    }
                }
                
                #Effect
                #FX("explosion",RD:position(),10)
                holoScaleUnits(Bullet,vec(DealRadius,DealRadius,DealRadius))
                holoMaterial(Bullet,"models/effects/splode_sheet")
                holoAlpha(Bullet,100)
                holoPos(Bullet,HitVec)
                
                #Bullet
                ModeArray[I,number]=Time
            } else {
                holoPos(Bullet,BulletEnt:toWorld(vec(BulletSpeed,0,0))-vec(0,0,BulletGravity))
                
                DamageArray = array(3,Time,""+DealDamage,""+DealExplosionDamage,DealRadius,DealFalloff,Origin,DirVec,BulletSpeed,ID,0)
                
                gSetStr("Damage",glonEncode(DamageArray))
                
                signalSetGroup("HCS")
                signalSend("Damage",1)
            }
        }
    }
    
    RemoveTime = ModeArray[1,number]
    if (RemoveTime != 1 & Time-5 > RemoveTime) {
        holoDelete(BulletArray[1,number])
        BulletArray:remove(1)
        ModeArray:remove(1)
    }
    timer("bullets",1000*BulletTime)
}




###############
#Damage-taking#
###############

if (signalClk("HCS","Damage",1)) { 
    DamageArray = glonDecode(gGetStr("Damage"))
    
    Mode = DamageArray[1,number]
    Time = DamageArray[2,number]
    
    if (Mode == 1) {
        # getting the variables I need to use #
        Damage = DamageArray[3,string]:toNumber()
        Origin = DamageArray[4,vector]
        OriginDirVec = DamageArray[5,vector]:normalized()
        Range = DamageArray[6,number]
        ID = DamageArray[7,string]
        Flag = DamageArray[8,number]
        
        Plane = E:pos() #Get the planes center
        P1 = Origin #Get a point on the line
        P2 = Origin+OriginDirVec #Get a point on the line "after" point 1
        Normal = -OriginDirVec #Get the normal (a vector pointin straight out of the surface) of the plane
        X = Normal:dot(Plane-P1)/Normal:dot(P2-P1) #Not really sure how, but it returns how many times the distance from point 1 to point 2 you need to go from point 1 to reach the intersection
        HitPos = P1+X*(P2-P1) #Get the intersections position
        
        if(HitPos & X > 0) {
            A = Plane:distance(HitPos)
            C = Radius
            if (A < C) {
                B = sqrt(C^2-A^2)
                HitPos = HitPos+Normal:normalized()*B
                
                Distance = HitPos:distance(Origin)
                if (Distance < Range) {
                    HitArray = glonDecode(gGetStr(ID+"Hit"))
                    
                    if (Flag | !HitArray[1,vector]) {
                        HitArray:pushVector(HitPos)
                        gSetStr(ID+"Hit",glonEncode(HitArray))
                        
                        Hit = 1
                    }
                }
            }
        }
        } elseif (Mode == 2) {
        ExplosionDamage = DamageArray[3,string]:toNumber()
        ExplosionRadius = DamageArray[4,number]
        Falloff = DamageArray[5,number]
        Origin = DamageArray[6,vector]
        ID = DamageArray[7,string]
        Flag = DamageArray[8,number]
        
        Target = E:pos()
        Distance = Origin:distance(Target)
        if (Distance <= ExplosionRadius) {
            Hit = 1
            
            Damage = ExplosionDamage/(Distance*Falloff+1)
            
            HitArray = glonDecode(gGetStr(ID+"Hit"))
            
            if (Flag | !HitArray[1,vector]) {
                HitArray:pushVector(Origin)
                gSetStr(ID+"Hit",glonEncode(HitArray))
            }
        }
    } elseif (Mode == 3) {
        Damage = DamageArray[3,string]:toNumber()
        ExplosionDamage = DamageArray[4,number]
        ExplosionRadius = DamageArray[5,number]
        Falloff = DamageArray[6,number]
        Origin = DamageArray[7,vector]
        OriginDirVec = DamageArray[8,vector]:normalized()
        Range = DamageArray[9,number]
        ID = DamageArray[10,string]
        Flag = DamageArray[11,number]
        
        Plane = E:pos() #Get the planes center
        P1 = Origin #Get a point on the line
        P2 = Origin+OriginDirVec #Get a point on the line "after" point 1
        Normal = -OriginDirVec #Get the normal (a vector pointin straight out of the surface) of the plane
        X = Normal:dot(Plane-P1)/Normal:dot(P2-P1) #Not really sure how, but it returns how many times the distance from point 1 to point 2 you need to go from point 1 to reach the intersection
        HitPos = P1+X*(P2-P1) #Get the intersections position
        
        if(HitPos & X > 0) {
            A = Plane:distance(HitPos)
            C = Radius/2
            if (A < C) {
                B = sqrt(C^2-A^2)
                HitPos = HitPos+Normal:normalized()*B
                
                Distance = HitPos:distance(Origin)
                if (Distance < Range) {
                    HitArray = glonDecode(gGetStr(ID+"Hit"))
                    
                    if (Flag | !HitArray[1,vector]) {
                        HitArray:pushVector(HitPos)
                        gSetStr(ID+"Hit",glonEncode(HitArray))
                        
                        Hit = 1
                        
                        DamageArray2 = array(2,curtime(),ExplosionDamage,ExplosionRadius,Falloff,HitPos,ID,0)
                        if (A <= ExplosionRadius) {
                            Hit = 1
                            
                            Damage+= ExplosionDamage/(Distance*Falloff+1)
                        }
                        timer("explosion",0)
                    }
                }
            }
        }
    }
} elseif (clk("explosion")) {
    gSetStr("Damage",glonEncode(DamageArray2))
    signalSetGroup("HCS")
    signalSend("Damage",1)
}

if (Hit) {
    Health-=Damage
}

if (Health <= 0) {
    #Boom-Code goes here.
    DamageArray = array(2,curtime(),100,100,Falloff,E:pos(),ID)
    
    signalSetGroup("HCS")
    signalSend("Damage",1)
    
    Pod:killPod()
    
    holoAlpha(18,0)
    holoPos(19,E:pos())
    holoAlpha(19,100)
    
    soundStop(1)
    soundStop(2)
    
    Health = 100
    
    timer("boom",5000)
}

if (clk("boom")) {
    holoAlpha(19,0)
}


SavedTime = curtime()
@name Holo-Tank
@inputs XWL:wirelink Cam:wirelink
@outputs View:vector Angle:angle
@persist [Pod E Turret]:entity
@outputs [BulletArray BulletGravArray ModeArray DamageArray2]:array
@persist Speed SavedTime Fixed
@persist ShootCount HoloID HoloIDLength Scale Radius DealDamage DealExplosionDamage DealRadius DealFalloff Health
@persist G V Mass IfDef
@persist  T C C2 C3 C4 T2 T3 T4 T5 T6 T7 T8 T9 T10 R1 S GunSec Flir CamTog Prop:entity Props:array Ranger:ranger Craft:entity CamPos:vector Bullete:entity  Cam:wirelink   PWrk VSV Ign VelLL Mul Ntrpl HoverHeight FlyAng SpeedMultiplier YawSpeed
@trigger 
@model

if(CamTog >= 2){CamTog = 0}
if(CamTog == 0){CamPos = E:pos()+E:forward()*-40+E:up()*30}
if(CamTog == 1){CamPos = E:pos()+E:forward()*0+E:up()*0}
Cam["Position",vector] = CamPos
Cam["Activated",number] = Pod["Active",number]
Cam["Angle",angle] = Pod["Entity",entity]:driver():eyeAngles()
if(changed(Pod["R",number])&Pod["R",number]){Flir = Flir + 1}
if(Flir >= 2){Flir = 0}
Cam["FLIR",number] = Flir
if(changed(Pod["Mouse2",number])&Pod["Mouse2",number]){CamTog = CamTog + 1}
#Camera Position Toggle

if(changed(Pod["Mouse2",number])&Pod["Mouse2",number]){CamTog = CamTog + 1}

if(CamTog >= 2){CamTog = 0}

if(CamTog == 0){CamPos = E:pos()+E:forward()*-1+E:up()*1}

if(CamTog == 1){CamPos = E:pos()+E:forward()*10+E:up()*50}

    

#Camera

Cam["Position",vector] = CamPos

Cam["Activated",number] = Pod["Active",number]

Cam["Angle",angle] = Pod["Entity",entity]:driver():eyeAngles()

    

#Camera FLIR Vision

if(changed(Pod["R",number])&Pod["R",number]){Flir = Flir + 1}

if(Flir >= 2){Flir = 0}

Cam["FLIR",number] = Flir

#Turret Timer

GunSec++



#Turret Fire

if(Pod["Mouse1",number] & changed(int(GunSec/5))){

    Ranger = rangerOffset(CamPos,CamPos+((vec(25000,0,0)):rotate(Pod["Entity",entity]:driver():eyeAngles())))

    Bullete:propBreak()

    Bullete = propSpawn("models/props_phx/mk-82.mdl",Ranger:pos()+vec(0,0,50),ang(90,0,0),1)

    Bullete:setAlpha(0)

}

Bullete:propBreak()

#First
if (duped() | inputClk()) {
    reset()
}

if (first()) {
    runOnTick(1)
    runOnChat(1)
    
    gSetStr("Damage","")
    signalSetGroup("HCS")
    runOnSignal("Damage",1,1)
    gShare(1)
    
    Pod = XWL["Entity",entity]
    
    Scale = 1
    Color = vec(255,255,255)
    
    G = 9.82*2
    Mass = 100
    
    ShootCount = 19
    Radius = 5
    DealDamage = 10
    DealExplosionDamage = 42
    DealRadius = 25
    DealFalloff = 0.1
    Health = 100
    
    
    holoCreate(1)
    holoScaleUnits(1,vec(1,1,1))  
    E = holoEntity(1)
    HoloID = E:id()
    HoloIDLength = HoloID:toString():length()
    
    holoCreate(2)
    holoModel(2,"hqcylinder")
    holoColor(2,Color)
    holoScaleUnits(2,vec(120.5,40,70.5))
    holoPos(2,E:toWorld(vec(0,0,0)))
    holoAng(2,E:toWorld(ang(0,0,90)))
    holoParent(2,E)
    
    holoCreate(3)
    holoModel(3,"hqcylinder")
    holoColor(3,vec(0,0,0))
    holoScaleUnits(3,vec(150,40,20.5))   
    holoPos(3,E:toWorld(vec(-2.5,5,-1.5)))
    holoAng(3,E:toWorld(ang(0,0,90)))
    holoParent(3,E)
    holoCreate(4)
    holoModel(4,"hqcylinder")
    holoColor(4,vec(0,0,0))
    holoScaleUnits(4,vec(150,40,20.5))   
    holoPos(4,E:toWorld(vec(-2.5,-5,-1.5)))
    holoAng(4,E:toWorld(ang(0,0,90)))
    holoParent(4,E)
    
    holoCreate(5)
    holoModel(5,"hqcylinder")
    holoColor(5,Color)
    holoScaleUnits(5,vec(16,3,3))   
    holoPos(5,E:toWorld(vec(-2.5,5,-0.5)))
    holoAng(5,E:toWorld(ang(0,0,90)))
    holoParent(5,E)
    holoCreate(6)
    holoModel(6,"hqcylinder")
    holoColor(6,Color)
    holoScaleUnits(6,vec(160,30,30))   
    holoPos(6,E:toWorld(vec(-2.5,-5,-1.5)))
    holoAng(6,E:toWorld(ang(0,0,90)))
    holoParent(6,E)
    
    holoCreate(7)
    holoModel(7,"hqcylinder")
    holoColor(7,Color)
    holoScaleUnits(7,vec(50,50,10))   
    holoPos(7,E:toWorld(vec(0,0,20)))
    holoAng(7,ang(0,0,0))
    holoParent(7,E)
    Turret = holoEntity(7)
    
    holoCreate(8)
    holoModel(8,"dome")
    holoColor(8,Color)
    holoScaleUnits(8,vec(100,70.5,30.5))   
    holoPos(8,Turret:toWorld(vec(0,0,0)))
    holoAng(8,ang(0,0,0))
    holoParent(8,Turret)
    
    holoCreate(9)
    holoModel(9,"cube")
    holoColor(9,Color)
    holoScaleUnits(9,vec(30,50,10))   
    holoPos(9,Turret:toWorld(vec(-4,0,10.5)))
    holoAng(9,ang(10,0,0))
    holoParent(9,Turret)
    
    holoCreate(10)
    holoModel(10,"dome")
    holoColor(10,Color)
    holoScaleUnits(10,vec(30,30,20))   
    holoPos(10,Turret:toWorld(vec(-1,0,10)))
    holoAng(10,ang(0,0,0))
    holoParent(10,Turret)
    
    holoCreate(11)
    holoModel(11,"prism")
    holoColor(11,Color)
    holoScaleUnits(11,vec(20,10,10))   
    holoPos(11,Turret:toWorld(vec(20,0,10.4)))
    holoAng(11,ang(25,0,0))
    holoParent(11,Turret)
     holoCreate(12)
    holoModel(12,"hqcylinder")
    holoColor(12,Color)
    holoScaleUnits(12,vec(10.5,10.5,20))   
    holoPos(12,Turret:toWorld(vec(49,0,5.5)))
    holoAng(12,ang(90,0,0))
    holoParent(12,Turret)
    
    holoCreate(13)
    holoModel(13,"hqcylinder")
    holoColor(13,Color)
    holoScaleUnits(13,vec(10,10,50))   
    holoPos(13,Turret:toWorld(vec(7,0,0.5)))
    holoAng(13,ang(90,0,0))
    holoParent(13,Turret)
    
    holoCreate(18)
    holoModel(18,"hqcylinder")
    holoMaterial(18,"models/effects/splode_sheet")
    holoAlpha(18,0)
    holoScaleUnits(18,vec(10,10,10000))
    holoPos(18,Turret:toWorld(vec(300,0,0.5)))
    holoAng(18,ang(90,0,0))
    holoParent(18,Turret)
    
    holoCreate(19)
    holoModel(19,"hqsphere")
    holoMaterial(19,"models/effects/splode_sheet")
    holoAlpha(19,0)
    holoScaleUnits(19,vec(1000,1000,1000))

}

#Stuff
Time = curtime()
DeltaTime = Time-SavedTime
DRIVER = Pod:driver()

if (changed(DRIVER) & DRIVER) {
    timer("physics",0)
    timer("bullets",0)
    Health = 100
} elseif (changed(DRIVER)) {
    Angle = entity():angles()
    Pos = entity():pos()
    holoPos(1,Pos)
    holoAng(1,Angle)
    #Pod:setView(0,E:pos(),E:angles())
}

if (DRIVER) {
    #Wirelinks
    W = XWL["W",number]
    S = XWL["S",number]
    A = XWL["A",number]
    D = XWL["D",number]
    Space = XWL["Space",number]
    Shift = XWL["Shift",number]
    Alt = XWL["Alt",number]
    F = XWL["Light",number]
    LMB = XWL["Mouse1",number]
    RMB = XWL["Mouse2",number]
    
    if (changed(F) & F) {
        Fixed = !Fixed
    }
    
    #Steer
    Pi = pi()
    Pos = E:pos()
    Forward = E:forward()
    Right = E:right()
    Up = E:up()
    
    Speed = ((2+Shift/3)/(2+Alt))/3
    Pos+= Forward*Speed*(W-S/2)
    Yaw = Speed/2*(A-D)
    
    #Collision detection for forward and backward, will enable when they can drive on slopes.
#    RD = rangerOffset(Speed+10,Pos,Forward)
#    if (RD:hit()) {
#        Pos = E:pos()
#    }
#    RD = rangerOffset(Speed+15,Pos,-Forward)
#    if (RD:hit()) {
#        Pos = E:pos()
#    }
   
 
    
   
    #Get Aim-stuff
    RD = rangerOffset(1337,Pos,E:forward())
    AimEntity = RD:entity()
    Owner = AimEntity:owner()
    AimPos = RD:position()
    
    #Shoot/Use
    if (changed(LMB)) {
        holoEntity(13):soundPlay(2,99,"synth/white_noise.wav")
        soundVolume(2,0.4) 
    }
    
    if (clk("laser") & LMB) {
        ID = HoloID:toString()
        
        DamageArray = array(1,Time,""+1,Pos,Turret:forward(),1000,ID,1)
    
        gSetStr("Damage",glonEncode(DamageArray))
        
        signalSetGroup("HCS")
        signalSend("Damage",1)
        
        holoAlpha(18,150)
        timer("laser",100)
    } elseif (!LMB) {
        soundStop(2)
        holoAlpha(18,0)
    }
    
    if (clk("Cannon") & RMB) {
        if (BulletArray:count() < 10) {
            if (ShootCount < 29) {
                ShootCount++
            } else {
                ShootCount = 20
            }
            if (ModeArray[BulletArray[ShootCount,number],number] != 1) {
                holoCreate(ShootCount)
                holoModel(ShootCount,"hqsphere")
                holoScaleUnits(ShootCount,vec(1,1,1))
                holoMaterial(ShootCount,"")
                holoAlpha(ShootCount,255)
                holoPos(ShootCount,Turret:toWorld(vec(10,0,0)))
                holoAng(ShootCount,Turret:angles())
                Count = BulletArray:count()+1
                BulletArray[Count,number]=ShootCount
                BulletGravArray[Count,number]=-pi()
                ModeArray[Count,number]=1
                
                holoEntity(13):soundPlay(1,0.1,"synth/brown_noise.wav") 
            }
            timer("Cannon",1000)
        }
    }
    
    #Set holoPos, holoAng and view
    holoPos(1,Pos)
    holoAng(1,E:toWorld(ang(0,Yaw,0)))
    
    Eye = DRIVER:eye()
    if (!Fixed) {
        View = E:pos()-Eye*25*(1-Space)
    } else {
        View = E:toWorld(-Eye*25*(1-Space))
    }
    ViewRD = rangerOffset(50,View+vec(0,0,50),vec(0,0,-1))
    if (ViewRD:hit()) {
        View = View:setZ(ViewRD:position():z()+5)
    }
    
    if (Space) {
        Angle = DRIVER:eyeAngles()
        View = View+Turret:up()*4*Space+Turret:forward()*3*Space
    } else {
        Angle = (Turret:pos()-View):toAngle()
    }
    #Pod:setView(1,View,Angle)
    
    if (Space) {
        holoAng(7,Angle)
    } else {
        holoAng(7,E:toWorld(ang(0,Angle:yaw()-E:angles():yaw(),0)))
    }
     #ifdef entity:hudRemoveElement(number)
        #Hud
        if (AimEntity:isPlayer()) {
            Driver:hudDrawText(1,AimEntity:name(),50,50,vec(100,200,50),255,1,pi())
        } elseif (AimEntity) {
            Driver:hudDrawText(1,"Owner: "+AimEntity:owner():name(),50,50,vec(100,200,50),255,1,pi())
        } else {
            Driver:hudRemoveElement(1)
        }
        IfDef = 1
        Driver:hudDrawRBox(2,1,40,85,20,3,vec(255,0,0),255)
        Driver:hudDrawRBox(3,1,40,85,(Health/100)*20,3,vec(0,255,0),255)
    #else
        if (IfDef & IfDef != 2) {
            Pod:printDriver("Cant use huds on this srv, so you wont see a healthbar.")
            IfDef = 2
        } elseif (IfDef != 2) {
            IfDef = 1
        }
    #endif
}



#########
#Bullets#
#########

if (clk("bullets")) {
    BulletTime = 0.05
    BulletSpeed = 1500*BulletTime
    for (I=1,BulletArray:count()){
        BulletMode = ModeArray[I,number]
        if (BulletMode == 1) {
            BulletGravity = BulletGravArray[I,number]+G*BulletTime
            BulletGravArray[I,number] = BulletGravity
            Bullet = BulletArray[I,number]
            BulletEnt = holoEntity(Bullet)
            Origin = BulletEnt:pos()
            DirVec = BulletEnt:toWorld(vec(BulletSpeed,0,0))-vec(0,0,BulletGravity)
            DirVec = DirVec-BulletEnt:pos()
            
            
            #Damage
            ID = HoloID:toString()+"-"+BulletEnt:id():toString()
            
            HitArray = glonDecode(gGetStr(ID+"Hit"))
            HitVec = HitArray[1,vector]
            
            RD = rangerOffset(BulletSpeed,Origin,DirVec)
            
            if (HitVec | RD:hit()) {
                if (HitVec) {
                    gSetStr(ID+"Hit","")
                } elseif (RD:hit()) {
                    DamageArray = array(2,Time,DealExplosionDamage,DealRadius,DealFalloff,RD:position(),ID)
                    gSetStr("Damage",glonEncode(DamageArray))
                    
                    HitVec = RD:position()
                    HitEnt = RD:entity()
                    if (HitEnt) {
                        HitEnt:applyOffsetForce(DirVec:normalized()*250,HitVec)
                    }
                }
                
                #Effect
                #FX("explosion",RD:position(),10)
                holoScaleUnits(Bullet,vec(DealRadius,DealRadius,DealRadius))
                holoMaterial(Bullet,"models/effects/splode_sheet")
                holoAlpha(Bullet,100)
                holoPos(Bullet,HitVec)
                
                #Bullet
                ModeArray[I,number]=Time
            } else {
                holoPos(Bullet,BulletEnt:toWorld(vec(BulletSpeed,0,0))-vec(0,0,BulletGravity))
                
                DamageArray = array(3,Time,""+DealDamage,""+DealExplosionDamage,DealRadius,DealFalloff,Origin,DirVec,BulletSpeed,ID,0)
                
                gSetStr("Damage",glonEncode(DamageArray))
                
                signalSetGroup("HCS")
                signalSend("Damage",1)
            }
        }
    }
    
    RemoveTime = ModeArray[1,number]
    if (RemoveTime != 1 & Time-5 > RemoveTime) {
        holoDelete(BulletArray[1,number])
        BulletArray:remove(1)
        ModeArray:remove(1)
    }
    timer("bullets",1000*BulletTime)
}




###############
#Damage-taking#
###############

if (signalClk("HCS","Damage",1)) { 
    DamageArray = glonDecode(gGetStr("Damage"))
    
    Mode = DamageArray[1,number]
    Time = DamageArray[2,number]
    
    if (Mode == 1) {
        # getting the variables I need to use #
        Damage = DamageArray[3,string]:toNumber()
        Origin = DamageArray[4,vector]
        OriginDirVec = DamageArray[5,vector]:normalized()
        Range = DamageArray[6,number]
        ID = DamageArray[7,string]
        Flag = DamageArray[8,number]
        
        Plane = E:pos() #Get the planes center
        P1 = Origin #Get a point on the line
        P2 = Origin+OriginDirVec #Get a point on the line "after" point 1
        Normal = -OriginDirVec #Get the normal (a vector pointin straight out of the surface) of the plane
        X = Normal:dot(Plane-P1)/Normal:dot(P2-P1) #Not really sure how, but it returns how many times the distance from point 1 to point 2 you need to go from point 1 to reach the intersection
        HitPos = P1+X*(P2-P1) #Get the intersections position
        
        if(HitPos & X > 0) {
            A = Plane:distance(HitPos)
            C = Radius
            if (A < C) {
                B = sqrt(C^2-A^2)
                HitPos = HitPos+Normal:normalized()*B
                
                Distance = HitPos:distance(Origin)
                if (Distance < Range) {
                    HitArray = glonDecode(gGetStr(ID+"Hit"))
                    
                    if (Flag | !HitArray[1,vector]) {
                        HitArray:pushVector(HitPos)
                        gSetStr(ID+"Hit",glonEncode(HitArray))
                        
                        Hit = 1
                    }
                }
            }
        }
        } elseif (Mode == 2) {
        ExplosionDamage = DamageArray[3,string]:toNumber()
        ExplosionRadius = DamageArray[4,number]
        Falloff = DamageArray[5,number]
        Origin = DamageArray[6,vector]
        ID = DamageArray[7,string]
        Flag = DamageArray[8,number]
        
        Target = E:pos()
        Distance = Origin:distance(Target)
        if (Distance <= ExplosionRadius) {
            Hit = 1
            
            Damage = ExplosionDamage/(Distance*Falloff+1)
            
            HitArray = glonDecode(gGetStr(ID+"Hit"))
            
            if (Flag | !HitArray[1,vector]) {
                HitArray:pushVector(Origin)
                gSetStr(ID+"Hit",glonEncode(HitArray))
            }
        }
    } elseif (Mode == 3) {
        Damage = DamageArray[3,string]:toNumber()
        ExplosionDamage = DamageArray[4,number]
        ExplosionRadius = DamageArray[5,number]
        Falloff = DamageArray[6,number]
        Origin = DamageArray[7,vector]
        OriginDirVec = DamageArray[8,vector]:normalized()
        Range = DamageArray[9,number]
        ID = DamageArray[10,string]
        Flag = DamageArray[11,number]
        
        Plane = E:pos() #Get the planes center
        P1 = Origin #Get a point on the line
        P2 = Origin+OriginDirVec #Get a point on the line "after" point 1
        Normal = -OriginDirVec #Get the normal (a vector pointin straight out of the surface) of the plane
        X = Normal:dot(Plane-P1)/Normal:dot(P2-P1) #Not really sure how, but it returns how many times the distance from point 1 to point 2 you need to go from point 1 to reach the intersection
        HitPos = P1+X*(P2-P1) #Get the intersections position
        
        if(HitPos & X > 0) {
            A = Plane:distance(HitPos)
            C = Radius/2
            if (A < C) {
                B = sqrt(C^2-A^2)
                HitPos = HitPos+Normal:normalized()*B
                
                Distance = HitPos:distance(Origin)
                if (Distance < Range) {
                    HitArray = glonDecode(gGetStr(ID+"Hit"))
                    
                    if (Flag | !HitArray[1,vector]) {
                        HitArray:pushVector(HitPos)
                        gSetStr(ID+"Hit",glonEncode(HitArray))
                        
                        Hit = 1
                        
                        DamageArray2 = array(2,curtime(),ExplosionDamage,ExplosionRadius,Falloff,HitPos,ID,0)
                        if (A <= ExplosionRadius) {
                            Hit = 1
                            
                            Damage+= ExplosionDamage/(Distance*Falloff+1)
                        }
                        timer("explosion",0)
                    }
                }
            }
        }
    }
} elseif (clk("explosion")) {
    gSetStr("Damage",glonEncode(DamageArray2))
    signalSetGroup("HCS")
    signalSend("Damage",1)
}

if (Hit) {
    Health-=Damage
}

if (Health <= 0) {
    #Boom-Code goes here.
    DamageArray = array(2,curtime(),100,100,Falloff,E:pos(),ID)
    
    signalSetGroup("HCS")
    signalSend("Damage",1)
    
    Pod:killPod()
    
    holoAlpha(18,0)
    holoPos(19,E:pos())
    holoAlpha(19,100)
    
    soundStop(1)
    soundStop(2)
    
    Health = 100
    
    timer("boom",5000)
}

if (clk("boom")) {
    holoAlpha(19,0)
}


SavedTime = curtime()
