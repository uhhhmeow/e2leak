@name KlyaniX_nanoworld_server
@outputs DestroyTree DestroyRock DestroyIron DestroyUran [Trees Rocks Irons Urans]:table
@persist [TreesModel RocksModel UransModel Players]:array
@persist [JunglePos RocksPos UransPos]:table RANDIT
@persist RD:ranger
@model models/props_lab/servers.mdl
if (first()) {
   #SOUNDS!111 
    if (map()=="gm_bigcity") {
        JunglePos:pushTable(
            table(
                "Min" = vec(-2745,-1081,-11135.96875),
                "Max" = vec(-1607,888,-11135.96875)	
            )
        )
        JunglePos:pushTable(
            table(
                "Min" = vec(-1607,-312,-11135.96875),
                "Max" = vec(-329,888,-11135.96875)	
            )
        )
        JunglePos:pushTable(
            table(
                "Min" = vec(-1087.7120361328,-831.94903564453,-11135.96875),
                "Max" = vec(959.12426757812,-3456.1784667969,-11135.96875)	
            )
        )
        JunglePos:pushTable(
            table(
                "Min" = vec(-9164.6640625,8907.2255859375,-11135.96875	),
                "Max" = vec(-12848.52734375,12846.658203125,-11135.96875)	
            )
        )
        RocksPos:pushTable(
            table(
                "Min" = vec(-7885.4692382812,2479.7670898438,-11135.96875),
                "Max" = vec(-10545.860351562,-177.48155212402,-11135.96875)	
            )
        )
        RocksPos:pushTable(
            table(
                "Min" = vec(2928.0161132812,-7313.1474609375,-11135.96875),
                "Max" = vec(1615.0637207031,-8304.52734375,-11135.96875)	
            )
        )
        RocksPos:pushTable(
            table(
                "Min" = vec(-12228.73046875,-10577.14453125,-11135.96875),
                "Max" = vec(-13204.17578125,-11467.62890625,-11135.96875)	
            )
        )
        UransPos:pushTable(
            table(
                "Min" = vec(9422.890625,9264.32421875,-11127.96875),
                "Max" = vec(8994.203125,9692.294921875,-11127.96875)	
            )
        )
        UransPos:pushTable(
            table(
                "Min" = vec(5612.798828125,-5137.1391601562,-11279.96875),
                "Max" = vec(5135.3559570312,-6128.8583984375,-11279.96875)	
            )
        )
        UransPos:pushTable(
            table(
                "Min" = vec(-8463.0908203125,-3855.4284667969,-11135.96875),
                "Max" = vec(-9712.904296875,-5488.5791015625,-11135.96875)	
            )
        )
        UransPos:pushTable(
            table(
                "Min" = vec(-11535.696289062,-4081.0344238281,-11135.96875),
                "Max" = vec(-12400.5546875,-3343.0803222656,-11135.96875)	
            )
        )
    } elseif (map()=="gm_construct") {
        UransPos:pushTable(
            table(
                "Min" = vec(491,-1899,-143),
                "Max" = vec(-1004,-1027,-143)	
            )
        )
        RocksPos:pushTable(
            table(
                "Min" = vec(491,-1899,-143),
                "Max" = vec(-1004,-1027,-143)	
            )
        )
        JunglePos:pushTable(
            table(
                "Min" = vec(491,-1899,-143),
                "Max" = vec(-1004,-1027,-143)			
            )
        )
    }
    
    TreesModel:pushString("models/props_foliage/tree_springers_01a-lod.mdl")
    TreesModel:pushString("models/props_foliage/tree_springers_01a.mdl")
    
    RocksModel:pushString("models/props_wasteland/rockcliff_cluster02a.mdl")
    RocksModel:pushString("models/props_wasteland/rockcliff_cluster01b.mdl")
    RocksModel:pushString("models/props_wasteland/rockcliff_cluster02c.mdl")
    RocksModel:pushString("models/props_wasteland/rockcliff_cluster03a.mdl")
    RocksModel:pushString("models/props_wasteland/rockcliff_cluster03c.mdl")
    RocksModel:pushString("models/props_wasteland/rockcliff_cluster02b.mdl")
    
    UransModel:pushString("models/props_wasteland/rockcliff05a.mdl")
    UransModel:pushString("models/props_wasteland/rockcliff05b.mdl")
    UransModel:pushString("models/props_wasteland/rockcliff05e.mdl")
    UransModel:pushString("models/props_wasteland/rockcliff05f.mdl")
}
interval(100)
###JUNGLE
if (Trees:count()<100&holoCanCreate()) {
    RANDIT=randint(1,100)
    #[if (RANDIT<20) {
        RANDIT=randint(1,3)
    } else {
        RANDIT=randint(4,JunglePos:count())
    }]#
    RANDIT=randint(1,JunglePos:count())
    rangerFilter(players())
    rangerHitEntities(0)
    RD = rangerOffset(300,randvec(JunglePos[RANDIT,table]["Min",vector],JunglePos[RANDIT,table]["Max",vector]+vec(0,0,150)),vec(0,0,-1))
    holoCreate(Trees:count(),RD:pos())
    holoAng(Trees:count(),ang(0,random(0,360),0))
    holoModel(Trees:count(),TreesModel[randint(1,TreesModel:count()),string])
    holoScale(Trees:count(),vec(0.05)*random(1,1.2))
    Trees:pushTable(table(
        "Entity" = holoEntity(Trees:count()),
        "Hp" = 5
        )
    )
}
###ROCKS
if (Rocks:count()<50&holoCanCreate()) {
    RANDIT=randint(1,RocksPos:count())
    rangerFilter(players())
    rangerHitEntities(0)
    RD = rangerOffset(300,randvec(RocksPos[RANDIT,table]["Min",vector],RocksPos[RANDIT,table]["Max",vector]+vec(0,0,150)),vec(0,0,-1))
    holoCreate(100+Rocks:count(),RD:pos())
    holoAng(100+Rocks:count(),ang(0,random(0,360),0))
    holoModel(100+Rocks:count(),RocksModel[randint(1,RocksModel:count()),string])
    holoSkin(100+Rocks:count(),1)
    holoScale(100+Rocks:count(),vec(0.05))
    Rocks:pushTable(table(
        "Entity" = holoEntity(100+Rocks:count()),
        "Hp" = 10
        )
    )
}
###IRON
if (Irons:count()<25&holoCanCreate()) {
    RANDIT=randint(1,RocksPos:count())
    rangerFilter(players())
    rangerHitEntities(0)
    RD = rangerOffset(300,randvec(RocksPos[RANDIT,table]["Min",vector],RocksPos[RANDIT,table]["Max",vector]+vec(0,0,150)),vec(0,0,-1))
    holoCreate(200+Irons:count(),RD:pos())
    holoAng(200+Irons:count(),ang(0,random(0,360),0))
    holoModel(200+Irons:count(),RocksModel[randint(1,RocksModel:count()),string])
    holoSkin(200+Irons:count(),1)
    holoColor(200+Irons:count(),vec(101,67,33))
    holoMaterial(200+Irons:count(),"models/debug/debugwhite")
    holoScale(200+Irons:count(),vec(0.05))
    Irons:pushTable(table(
        "Entity" = holoEntity(200+Irons:count()),
        "Hp" = 8
        )
    )
}
###URAN
if (Urans:count()<10&holoCanCreate()) {
    RANDIT=randint(1,100)
    if (RANDIT<20) {
        RANDIT=1
    } else {
        RANDIT=randint(2,JunglePos:count())
    }
    rangerFilter(players())
    rangerHitEntities(0)
    RANDIT=randint(1,UransPos:count())
    RD = rangerOffset(300,randvec(UransPos[RANDIT,table]["Min",vector],UransPos[RANDIT,table]["Max",vector]+vec(0,0,150)),vec(0,0,-1))
    holoCreate(300+Urans:count(),RD:pos())
    holoAng(300+Urans:count(),ang(0,random(0,360),0))
    holoModel(300+Urans:count(),UransModel[randint(1,UransModel:count()),string])
    holoSkin(300+Urans:count(),1)
    holoColor(300+Urans:count(),vec(0,255,0))
    holoMaterial(300+Urans:count(),"models/debug/debugwhite")
    holoScale(300+Urans:count(),vec(0.05))
    holoDisableShading(300+Urans:count(),1)
    Urans:pushTable(table(
        "Entity" = holoEntity(300+Urans:count()),
        "Hp" = 1
        )
    )
}
