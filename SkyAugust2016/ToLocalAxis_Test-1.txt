@name ToLocalAxis_Test-1
@inputs 
@outputs 
@persist 

interval(60)
E = entity()

if(first()){
function number c(I,V:vector,S:vector,A:angle,Col:vector,Model:string,Alpha:number){ holoCreate(I,V,S,A,Col,Model) holoAlpha(I,Alpha) }
function number c(I,V:vector,S:vector,A:angle,Col:vector,Model:string,Parent:entity,Alpha:number){ holoCreate(I,V,S,A,Col,Model) holoAlpha(I,Alpha) holoParent(I,Parent) }
function number c(I,V:vector,S:vector,A:angle,Col:vector,Model:string,Parent:number,Alpha:number){ holoCreate(I,V,S,A,Col,Model) holoAlpha(I,Alpha) holoParent(I,Parent) }
function number c(I,V:vector,S:vector,A:angle,Col:vector,Model:string,Parent:number,Alpha:number,Mat:string){ holoCreate(I,V,S,A,Col,Model) holoAlpha(I,Alpha) holoParent(I,Parent)     holoMaterial(I,Mat) }
function number c(I,V:vector,S:vector,A:angle,Col:vector,Model:string,Parent:entity,Alpha:number,Mat:string){ holoCreate(I,V,S,A,Col,Model) holoAlpha(I,Alpha) holoParent(I,Parent)     holoMaterial(I,Mat) }
function number c(I,V:vector,S:vector,A:angle,Col:vector,Model:string,Alpha:number,Mat:string){ holoCreate(I,V,S,A,Col,Model) holoAlpha(I,Alpha)    holoMaterial(I,Mat) }


c(1,E:toWorld(vec(0,0,0)),vec(1),E:angles(),vec(0,144,255),"cube",E,255)
c(2,E:toWorld(vec(20,0,0)),vec(1),E:angles(),vec(255,0,0),"cube",1,255)
}


local Axis = E:toLocalAxis(owner():aimPos()-holoEntity(1):pos())


holoAng(1,ang(0,E:angles():yaw()+atan(Axis[2],Axis[1]),atan(Axis[2],Axis[1])))
