@name Anim by dimeji
@persist E:entity Interval C Pold:vector P:vector
runOnTick(1)

if(first())
{
    #include "IKuswbyDimeji"
    
    E = entity()
    holoCreate(0)
 #   holoCreate(1)
    Interval = tickInterval()/2.5
    
#    holoColor(1, vec(255,0,0))
}



Vel = E:vel():setZ(0)
Frequency = 2+E:vel():setZ(0):length()*0.001
Stridelength = 0.2+clamp(E:vel():setZ(0):length()*0.005,0.1,0.625)


holoPos(0, P)
holoPos(1, E:toWorld(Pold))

if(C>1){
     C = C-1
} 
if(C>Stridelength)
{
    P = bezierAnim(E, E:toWorld(Pold), getTarget3(E, Vel, Frequency, Stridelength), (C-Stridelength)/(1-Stridelength))
}
elseif(C<=Stridelength)
{
    Pold = E:toLocal(P)
}
C += Frequency*Interval


