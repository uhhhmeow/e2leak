@name Ik3Seg_V3
@inputs End:entity
interval(60)
E = entity()

Space = 30


if(first()){
function number c(I,V:vector,S:vector,A:angle,Col:vector,Model:string,Alpha:number){ holoCreate(I,V,S,A,Col,Model) holoAlpha(I,Alpha) }
function number c(I,V:vector,S:vector,A:angle,Col:vector,Model:string,Parent:entity,Alpha:number){ holoCreate(I,V,S,A,Col,Model) holoAlpha(I,Alpha) holoParent(I,Parent) }
function number c(I,V:vector,S:vector,A:angle,Col:vector,Model:string,Parent:number,Alpha:number){ holoCreate(I,V,S,A,Col,Model) holoAlpha(I,Alpha) holoParent(I,Parent) }
function number c(I,V:vector,S:vector,A:angle,Col:vector,Model:string,Parent:number,Alpha:number,Mat:string){ holoCreate(I,V,S,A,Col,Model) holoAlpha(I,Alpha) holoParent(I,Parent)     holoMaterial(I,Mat) }
function number c(I,V:vector,S:vector,A:angle,Col:vector,Model:string,Parent:entity,Alpha:number,Mat:string){ holoCreate(I,V,S,A,Col,Model) holoAlpha(I,Alpha) holoParent(I,Parent)     holoMaterial(I,Mat) }
function number c(I,V:vector,S:vector,A:angle,Col:vector,Model:string,Alpha:number,Mat:string){ holoCreate(I,V,S,A,Col,Model) holoAlpha(I,Alpha)    holoMaterial(I,Mat) }



c(1,E:toWorld(vec(0,20,0)),vec(0.6),ang(),vec(0,255,0),"hq_sphere",E,255)
c(2,holoEntity(1):toWorld(vec(0,0,Space)),vec(0.6),ang(),vec(0,255,0),"hq_sphere",1,255)
c(3,holoEntity(2):toWorld(vec(0,0,Space)),vec(0.6),ang(),vec(0,255,0),"hq_sphere",2,255)
c(4,holoEntity(3):toWorld(vec(0,0,Space)),vec(0.6),ang(),vec(0,255,0),"hq_sphere",3,255)
c(5,holoEntity(1):toWorld(vec(0,0,Space/2)),vec(0.5,0.5,(Space/5)/2.5),ang(),vec(255,0,0),"hq_cylinder",1,255)
c(6,holoEntity(2):toWorld(vec(0,0,Space/2)),vec(0.5,0.5,(Space/5)/2.5),ang(),vec(255,0,0),"hq_cylinder",2,255)
c(7,holoEntity(3):toWorld(vec(0,0,Space/2)),vec(0.5,0.5,(Space/5)/2.5),ang(),vec(255,0,0),"hq_cylinder",3,255)

function number cosine(One, Two, Three){ return acos( (Three^2 + Two^2 - One^2) / (2*Two*Three) ) }
function number getL(P1:entity,P2:vector){ return abs(P1:toLocal(P2):z()) }
function ik(A,B,C,D,End:vector){
local L1 = getL(holoEntity(A),holoEntity(B):pos())
local L2 = getL(holoEntity(B),holoEntity(C):pos())
local L3 = getL(holoEntity(C),holoEntity(D):pos())
local Vec = E:toLocalAxis(End-holoEntity(A):pos())
local Calc = cosine(L1,L2,min(Vec:length()-L3,L1+L2))
local Pos = E:toLocal((End-holoEntity(A):pos()):toAngle())
holoAng(A,E:toWorld(ang(0,-90+Pos:yaw(),-90-Pos:pitch()+Calc)))
holoAng(B,holoEntity(A):toWorld(ang(0,0,-Calc)))
holoAng(C,holoEntity(B):toWorld(ang(0,0,-Calc)))     
}


}
ik(1,2,3,4,End:pos())
