@name quadrocopter by Sadira
@inputs Pod:wirelink
@outputs Rpm I Active Space CamPos:vector CamAng:angle CamPar:entity
@persist T I MaxSpeed Speed
@model models/hunter/blocks/cube1x1x05.mdl

interval(10)
E=entity()
E:setMass(500)
#E:propGravity(1)

#---------Pod Controller---------------
W = Pod["W",number]
A = Pod["A",number]
S = Pod["S",number]
D = Pod["D",number]
Space = Pod["Space",number]
Alt = Pod["Alt",number]
Shift = Pod["Shift",number]
PrevWeapon = Pod["PrevWeapon",number]
NextWeapon = Pod["NextWeapon",number]
R = Pod["R",number]
Driver =Pod["Entity",entity] 
Active=Pod["Active",number]
Mouse1=Pod["Mouse1",number]
Mouse2=Pod["Mouse2",number]
#---------------------------------------

if(dupefinished()){reset()}

if(first()|duped()){

E:setAlpha(0)



for(I=1,26){   
    holoCreate(I)
    holoShadow(I,3)
}
holoModel(7,"models/Combine_Helicopter/helicopter_bomb01.mdl")
holoScale(7,vec(1,1,1)/15)
holoAng(7,E:toWorld(ang(180,45,0)))
holoPos(7,E:toWorld(vec(0,0,5)))
holoParent(7,E)

holoModel(8,"models/props_wasteland/laundry_washer001a.mdl")
holoMaterial(8,"sprops/trans/wheels/wheel_d_rim2")
holoAng(8,E:toWorld(ang(0,0,0)))
holoPos(8,E:toWorld(vec(0,1,5.5)))
holoScale(8,vec(1,1,0.5)/4.3)
holoParent(8,E)


#staender
holoModel(1,"models/props_c17/playground_teetertoter_stan.mdl")
holoMaterial(1,"sprops/trans/wheels/wheel_d_rim2")
holoModel(2,"models/props_c17/playground_teetertoter_stan.mdl")
holoMaterial(2,"sprops/trans/wheels/wheel_d_rim2")
holoAng(1,E:toWorld(ang(180,0,0)))
holoAng(2,E:toWorld(ang(180,0,0)))
holoPos(1,E:toWorld(vec(0,10,5)))
holoPos(2,E:toWorld(vec(0,-10,5)))
holoScale(1,vec(1,1,1.2)/1.5)
holoScale(2,vec(1,1,1.2)/1.5)
holoParent(1,E)
holoParent(2,E)

#X chassis
holoModel(3,"models/sprops/misc/fittings/cred_12_6_tall.mdl")
holoMaterial(3,"sprops/trans/wheels/wheel_d_rim2")
holoModel(4,"models/sprops/misc/fittings/cred_12_6_tall.mdl")
holoMaterial(4,"sprops/trans/wheels/wheel_d_rim2")
holoAng(3,holoEntity(7):toWorld(ang(90,90,0)))
holoAng(4,holoEntity(7):toWorld(ang(90,-90,0)))
holoPos(3,holoEntity(7):toWorld(vec(0,18,-0)))
holoPos(4,holoEntity(7):toWorld(vec(0,-18,-0)))
holoParent(3,holoEntity(7))
holoParent(4,holoEntity(7))
holoScale(3,vec(0.7,1,3))
holoScale(4,vec(0.7,1,3))

holoModel(5,"models/sprops/misc/fittings/cred_12_6_tall.mdl")
holoMaterial(5,"sprops/trans/wheels/wheel_d_rim2")
holoModel(6,"models/sprops/misc/fittings/cred_12_6_tall.mdl")
holoMaterial(6,"sprops/trans/wheels/wheel_d_rim2")
holoAng(5,holoEntity(7):toWorld(ang(90,0,0)))
holoAng(6,holoEntity(7):toWorld(ang(-90,0,0)))
holoPos(5,holoEntity(7):toWorld(vec(18,0,-0)))
holoPos(6,holoEntity(7):toWorld(vec(-18,-0,-0)))
holoParent(5,holoEntity(7))
holoParent(6,holoEntity(7))
holoScale(5,vec(0.7,1,3))
holoScale(6,vec(0.7,1,3))

#motor halter
holoModel(9,"models/Combine_Helicopter/helicopter_bomb01.mdl")
holoMaterial(9,"phoenix_storms/gear")
holoModel(10,"models/Combine_Helicopter/helicopter_bomb01.mdl")
holoMaterial(10,"phoenix_storms/gear")
holoAng(9,holoEntity(7):toWorld(ang(0,0,0)))
holoAng(10,holoEntity(7):toWorld(ang(0,0,0)))
holoPos(9,holoEntity(7):toWorld(vec(36,0,-0)))
holoPos(10,holoEntity(7):toWorld(vec(-36,-0,-0)))
holoParent(9,holoEntity(7))
holoParent(10,holoEntity(7))
holoScale(9,vec(1.5,1,0.7)/5.4)
holoScale(10,vec(1.5,1,0.7)/5.4)
holoColor(9,vec(200,200,200))
holoColor(10,vec(200,200,200))

holoModel(11,"models/Combine_Helicopter/helicopter_bomb01.mdl")
holoMaterial(11,"phoenix_storms/gear")
holoModel(12,"models/Combine_Helicopter/helicopter_bomb01.mdl")
holoMaterial(12,"phoenix_storms/gear")
holoAng(11,holoEntity(7):toWorld(ang(0,90,0)))
holoAng(12,holoEntity(7):toWorld(ang(0,90,0)))
holoPos(11,holoEntity(7):toWorld(vec(0,36,-0)))
holoPos(12,holoEntity(7):toWorld(vec(-0,-36,-0)))
holoParent(11,holoEntity(7))
holoParent(12,holoEntity(7))
holoScale(11,vec(1.5,1,0.7)/5.4)
holoScale(12,vec(1.5,1,0.7)/5.4)
holoColor(11,vec(200,200,200))
holoColor(12,vec(200,200,200))


#motor models/engines/emotorsmall2.mdl
holoModel(13,"models/xqm/cylinderx1.mdl")
holoMaterial(13,"phoenix_storms/fender_chrome")
holoModel(14,"models/xqm/cylinderx1.mdl")
holoMaterial(14,"phoenix_storms/fender_chrome")
holoAng(13,holoEntity(9):toWorld(ang(-90,180,0)))
holoAng(14,holoEntity(10):toWorld(ang(-90,0,0)))
holoPos(13,holoEntity(9):toWorld(vec(0,0,-2)))
holoPos(14,holoEntity(10):toWorld(vec(-0,-0,-2)))
holoParent(13,holoEntity(7))
holoParent(14,holoEntity(7))
holoScale(13,vec(1,1,1)/3)
holoScale(14,vec(1,1,1)/3)
holoColor(13,vec(0,0,0))
holoColor(14,vec(0,0,0))

holoModel(15,"models/xqm/cylinderx1.mdl")
holoMaterial(15,"phoenix_storms/fender_chrome")
holoModel(16,"models/xqm/cylinderx1.mdl")
holoMaterial(16,"phoenix_storms/fender_chrome")
holoAng(15,holoEntity(11):toWorld(ang(-90,180,0)))
holoAng(16,holoEntity(12):toWorld(ang(-90,0,0)))
holoPos(15,holoEntity(11):toWorld(vec(0,0,-2)))
holoPos(16,holoEntity(12):toWorld(vec(-0,-0,-2)))
holoParent(15,holoEntity(7))
holoParent(16,holoEntity(7))
holoScale(15,vec(1,1,1)/3)
holoScale(16,vec(1,1,1)/3)
holoColor(15,vec(0,0,0))
holoColor(16,vec(0,0,0))

holoModel(17,"models/sprops/trans/air/prop_2b_s.mdl")
#holoMaterial(13,"sprops/trans/wheels/wheel_d_rim2")
holoModel(18,"models/sprops/trans/air/prop_2b_s.mdl")
#holoMaterial(14,"sprops/trans/wheels/wheel_d_rim2")
holoAng(17,holoEntity(9):toWorld(ang(-90,180,0)))
holoAng(18,holoEntity(10):toWorld(ang(-90,0,0)))
holoPos(17,holoEntity(9):toWorld(vec(0,0,-5)))
holoPos(18,holoEntity(10):toWorld(vec(-0,-0,-5)))
holoParent(17,holoEntity(6))
holoParent(18,holoEntity(6))
holoScale(17,vec(1,1,1)/1)
holoScale(18,vec(1,1,1)/1)
holoColor(17,vec(0,0,255))
holoColor(18,vec(255,0,0))

holoModel(19,"models/sprops/trans/air/prop_2b_s.mdl")
#holoMaterial(13,"sprops/trans/wheels/wheel_d_rim2")
holoModel(20,"models/sprops/trans/air/prop_2b_s.mdl")
#holoMaterial(14,"sprops/trans/wheels/wheel_d_rim2")
holoAng(19,holoEntity(11):toWorld(ang(90,180,0)))
holoAng(20,holoEntity(12):toWorld(ang(-90,0,0)))
holoPos(19,holoEntity(11):toWorld(vec(0,0,-5)))
holoPos(20,holoEntity(12):toWorld(vec(-0,-0,-5)))
holoParent(19,holoEntity(6))
holoParent(20,holoEntity(6))
holoScale(19,vec(1,1,1)/1)
holoScale(20,vec(1,1,1)/1)
holoColor(19,vec(0,0,255))
holoColor(20,vec(255,0,0))

#halbkreis deko models/sprops/misc/fittings/bend_long_90_6.mdl
holoModel(21,"models/sprops/misc/fittings/bend_long_90_6.mdl")
holoMaterial(21,"sprops/trans/wheels/wheel_d_rim2")
holoModel(22,"models/sprops/misc/fittings/bend_long_90_6.mdl")
holoMaterial(22,"sprops/trans/wheels/wheel_d_rim2")
holoAng(21,holoEntity(7):toWorld(ang(0,0,90)))
holoAng(22,holoEntity(7):toWorld(ang(-0,180,90)))
holoPos(21,holoEntity(7):toWorld(vec(-7,7,0)))
holoPos(22,holoEntity(7):toWorld(vec(7,-7,0)))

holoParent(21,holoEntity(6))
holoParent(22,holoEntity(6))
holoScale(21,vec(1,1.2,1)/1)
holoScale(22,vec(1,1.2,1)/1)
##holoColor(21,vec(100,100,100))
#holoColor(22,vec(100,100,100))

holoModel(23,"models/sprops/misc/fittings/bend_long_90_6.mdl")
holoMaterial(23,"sprops/trans/wheels/wheel_d_rim2")
holoModel(24,"models/sprops/misc/fittings/bend_long_90_6.mdl")
holoMaterial(24,"sprops/trans/wheels/wheel_d_rim2")
holoAng(23,holoEntity(7):toWorld(ang(0,90,90)))
holoAng(24,holoEntity(7):toWorld(ang(-0,-90,90)))
holoPos(23,holoEntity(7):toWorld(vec(-7,-7,0)))
holoPos(24,holoEntity(7):toWorld(vec(7,7,0)))
holoParent(23,holoEntity(6))
holoParent(24,holoEntity(6))
holoScale(23,vec(1,1.2,1)/1)
holoScale(24,vec(1,1.2,1)/1)
#holoColor(23,vec(100,100,100))
#holoColor(24,vec(100,100,100))
#rotor models/sprops/trans/air/prop_2b_s.mdl

holoScale(25,vec(1,1,1)/50)
holoAng(25,E:toWorld(ang(0,0,0)))
holoPos(25,E:toWorld(vec(0,0,10)))
holoParent(25,E)


holoScale(26,vec(1,1,1)/50)
#holoAng(26,E:toWorld(ang(0,0,0)))
#holoPos(26,E:toWorld(vec(0,0,10)))
holoParent(26,E)
MaxSpeed=50
I=2
}


MPH=toUnit("mph", E:vel():length())
T=(T+15)+(MPH*2)
holoAng(17,holoEntity(9):toWorld(ang(-90,0+T,0)))
holoAng(18,holoEntity(10):toWorld(ang(-90,0-T,0)))
holoAng(19,holoEntity(11):toWorld(ang(90,0-T,0)))
holoAng(20,holoEntity(12):toWorld(ang(-90,0+T,0)))



if(Active == 1)  {  
    MPH=toUnit("mph", entity():vel():length())
    #MPH=Rpm
    
    SoundPitch = MPH
          
    if (~Active) { E:soundPlay(1,0,"acf_extra/airfx/helirotor2.wav")}
    soundPitch(1,SoundPitch)
    
}
      
if(Active == 1)  {  
    #MPH=toUnit("mph", entity():vel():length())
    MPH=Speed
    SoundPitch = MPH*5
          
    if (~Active) { E:soundPlay(2,0,"vehicles/airboat/fan_blade_idle_loop1.wav") }
    soundPitch(2,SoundPitch)
    
}
if(Active){
    soundVolume(1,1)
    soundVolume(2,1)
}
else{
    
    soundVolume(1,0)
    soundVolume(2,0)
}

if(Active){Speed=10}
else{Speed=5}


#steuerung
if(Active){
#lightCreate(1,E:pos(),vec(255,255,255),1,10.1)

}
else{
#lightCreate(1,E:pos(),vec(0,0,0),1,10.1)

}
if(PrevWeapon){I=I+1}
if(NextWeapon){I=I-1}
if(Space){I=1500}
else{I=700}

#if(Dist<500){
E:applyForce(E:up()*I*5)
#}

E:applyForce(-E:vel()*E:mass()/150)

ForceT = E:up():cross(vec(0,0,1))
ForceT = E:toLocal(ForceT+E:pos())
ForceT *= 50 #2000 fuer volle stabilisierung
ForceT += vec(A-D,S-W,Mouse1-Mouse2)*50  #Gas voll
ForceT -= E:angVelVector()
ForceT *= 10
E:applyTorque(ForceT*E:inertia())

CamPos=E:toWorld(vec(150,0,20))
CamAng=(E:toWorld(ang(0,180,0)))
CamPar=E

#cameraCreate(1,CamPos,CamAng)
#cameraParent(1,CamPar)
#cameraToggle(1,Active,Driver)
@name quadrocopter by Sadira
@inputs Pod:wirelink
@outputs Rpm I Active Space CamPos:vector CamAng:angle CamPar:entity
@persist T I MaxSpeed Speed
@model models/hunter/blocks/cube1x1x05.mdl

interval(10)
E=entity()
E:setMass(500)
#E:propGravity(1)

#---------Pod Controller---------------
W = Pod["W",number]
A = Pod["A",number]
S = Pod["S",number]
D = Pod["D",number]
Space = Pod["Space",number]
Alt = Pod["Alt",number]
Shift = Pod["Shift",number]
PrevWeapon = Pod["PrevWeapon",number]
NextWeapon = Pod["NextWeapon",number]
R = Pod["R",number]
Driver =Pod["Entity",entity] 
Active=Pod["Active",number]
Mouse1=Pod["Mouse1",number]
Mouse2=Pod["Mouse2",number]
#---------------------------------------

if(dupefinished()){reset()}

if(first()|duped()){

E:setAlpha(0)



for(I=1,26){   
    holoCreate(I)
    holoShadow(I,3)
}
holoModel(7,"models/Combine_Helicopter/helicopter_bomb01.mdl")
holoScale(7,vec(1,1,1)/15)
holoAng(7,E:toWorld(ang(180,45,0)))
holoPos(7,E:toWorld(vec(0,0,5)))
holoParent(7,E)

holoModel(8,"models/props_wasteland/laundry_washer001a.mdl")
holoMaterial(8,"sprops/trans/wheels/wheel_d_rim2")
holoAng(8,E:toWorld(ang(0,0,0)))
holoPos(8,E:toWorld(vec(0,1,5.5)))
holoScale(8,vec(1,1,0.5)/4.3)
holoParent(8,E)


#staender
holoModel(1,"models/props_c17/playground_teetertoter_stan.mdl")
holoMaterial(1,"sprops/trans/wheels/wheel_d_rim2")
holoModel(2,"models/props_c17/playground_teetertoter_stan.mdl")
holoMaterial(2,"sprops/trans/wheels/wheel_d_rim2")
holoAng(1,E:toWorld(ang(180,0,0)))
holoAng(2,E:toWorld(ang(180,0,0)))
holoPos(1,E:toWorld(vec(0,10,5)))
holoPos(2,E:toWorld(vec(0,-10,5)))
holoScale(1,vec(1,1,1.2)/1.5)
holoScale(2,vec(1,1,1.2)/1.5)
holoParent(1,E)
holoParent(2,E)

#X chassis
holoModel(3,"models/sprops/misc/fittings/cred_12_6_tall.mdl")
holoMaterial(3,"sprops/trans/wheels/wheel_d_rim2")
holoModel(4,"models/sprops/misc/fittings/cred_12_6_tall.mdl")
holoMaterial(4,"sprops/trans/wheels/wheel_d_rim2")
holoAng(3,holoEntity(7):toWorld(ang(90,90,0)))
holoAng(4,holoEntity(7):toWorld(ang(90,-90,0)))
holoPos(3,holoEntity(7):toWorld(vec(0,18,-0)))
holoPos(4,holoEntity(7):toWorld(vec(0,-18,-0)))
holoParent(3,holoEntity(7))
holoParent(4,holoEntity(7))
holoScale(3,vec(0.7,1,3))
holoScale(4,vec(0.7,1,3))

holoModel(5,"models/sprops/misc/fittings/cred_12_6_tall.mdl")
holoMaterial(5,"sprops/trans/wheels/wheel_d_rim2")
holoModel(6,"models/sprops/misc/fittings/cred_12_6_tall.mdl")
holoMaterial(6,"sprops/trans/wheels/wheel_d_rim2")
holoAng(5,holoEntity(7):toWorld(ang(90,0,0)))
holoAng(6,holoEntity(7):toWorld(ang(-90,0,0)))
holoPos(5,holoEntity(7):toWorld(vec(18,0,-0)))
holoPos(6,holoEntity(7):toWorld(vec(-18,-0,-0)))
holoParent(5,holoEntity(7))
holoParent(6,holoEntity(7))
holoScale(5,vec(0.7,1,3))
holoScale(6,vec(0.7,1,3))

#motor halter
holoModel(9,"models/Combine_Helicopter/helicopter_bomb01.mdl")
holoMaterial(9,"phoenix_storms/gear")
holoModel(10,"models/Combine_Helicopter/helicopter_bomb01.mdl")
holoMaterial(10,"phoenix_storms/gear")
holoAng(9,holoEntity(7):toWorld(ang(0,0,0)))
holoAng(10,holoEntity(7):toWorld(ang(0,0,0)))
holoPos(9,holoEntity(7):toWorld(vec(36,0,-0)))
holoPos(10,holoEntity(7):toWorld(vec(-36,-0,-0)))
holoParent(9,holoEntity(7))
holoParent(10,holoEntity(7))
holoScale(9,vec(1.5,1,0.7)/5.4)
holoScale(10,vec(1.5,1,0.7)/5.4)
holoColor(9,vec(200,200,200))
holoColor(10,vec(200,200,200))

holoModel(11,"models/Combine_Helicopter/helicopter_bomb01.mdl")
holoMaterial(11,"phoenix_storms/gear")
holoModel(12,"models/Combine_Helicopter/helicopter_bomb01.mdl")
holoMaterial(12,"phoenix_storms/gear")
holoAng(11,holoEntity(7):toWorld(ang(0,90,0)))
holoAng(12,holoEntity(7):toWorld(ang(0,90,0)))
holoPos(11,holoEntity(7):toWorld(vec(0,36,-0)))
holoPos(12,holoEntity(7):toWorld(vec(-0,-36,-0)))
holoParent(11,holoEntity(7))
holoParent(12,holoEntity(7))
holoScale(11,vec(1.5,1,0.7)/5.4)
holoScale(12,vec(1.5,1,0.7)/5.4)
holoColor(11,vec(200,200,200))
holoColor(12,vec(200,200,200))


#motor models/engines/emotorsmall2.mdl
holoModel(13,"models/xqm/cylinderx1.mdl")
holoMaterial(13,"phoenix_storms/fender_chrome")
holoModel(14,"models/xqm/cylinderx1.mdl")
holoMaterial(14,"phoenix_storms/fender_chrome")
holoAng(13,holoEntity(9):toWorld(ang(-90,180,0)))
holoAng(14,holoEntity(10):toWorld(ang(-90,0,0)))
holoPos(13,holoEntity(9):toWorld(vec(0,0,-2)))
holoPos(14,holoEntity(10):toWorld(vec(-0,-0,-2)))
holoParent(13,holoEntity(7))
holoParent(14,holoEntity(7))
holoScale(13,vec(1,1,1)/3)
holoScale(14,vec(1,1,1)/3)
holoColor(13,vec(0,0,0))
holoColor(14,vec(0,0,0))

holoModel(15,"models/xqm/cylinderx1.mdl")
holoMaterial(15,"phoenix_storms/fender_chrome")
holoModel(16,"models/xqm/cylinderx1.mdl")
holoMaterial(16,"phoenix_storms/fender_chrome")
holoAng(15,holoEntity(11):toWorld(ang(-90,180,0)))
holoAng(16,holoEntity(12):toWorld(ang(-90,0,0)))
holoPos(15,holoEntity(11):toWorld(vec(0,0,-2)))
holoPos(16,holoEntity(12):toWorld(vec(-0,-0,-2)))
holoParent(15,holoEntity(7))
holoParent(16,holoEntity(7))
holoScale(15,vec(1,1,1)/3)
holoScale(16,vec(1,1,1)/3)
holoColor(15,vec(0,0,0))
holoColor(16,vec(0,0,0))

holoModel(17,"models/sprops/trans/air/prop_2b_s.mdl")
#holoMaterial(13,"sprops/trans/wheels/wheel_d_rim2")
holoModel(18,"models/sprops/trans/air/prop_2b_s.mdl")
#holoMaterial(14,"sprops/trans/wheels/wheel_d_rim2")
holoAng(17,holoEntity(9):toWorld(ang(-90,180,0)))
holoAng(18,holoEntity(10):toWorld(ang(-90,0,0)))
holoPos(17,holoEntity(9):toWorld(vec(0,0,-5)))
holoPos(18,holoEntity(10):toWorld(vec(-0,-0,-5)))
holoParent(17,holoEntity(6))
holoParent(18,holoEntity(6))
holoScale(17,vec(1,1,1)/1)
holoScale(18,vec(1,1,1)/1)
holoColor(17,vec(0,0,255))
holoColor(18,vec(255,0,0))

holoModel(19,"models/sprops/trans/air/prop_2b_s.mdl")
#holoMaterial(13,"sprops/trans/wheels/wheel_d_rim2")
holoModel(20,"models/sprops/trans/air/prop_2b_s.mdl")
#holoMaterial(14,"sprops/trans/wheels/wheel_d_rim2")
holoAng(19,holoEntity(11):toWorld(ang(90,180,0)))
holoAng(20,holoEntity(12):toWorld(ang(-90,0,0)))
holoPos(19,holoEntity(11):toWorld(vec(0,0,-5)))
holoPos(20,holoEntity(12):toWorld(vec(-0,-0,-5)))
holoParent(19,holoEntity(6))
holoParent(20,holoEntity(6))
holoScale(19,vec(1,1,1)/1)
holoScale(20,vec(1,1,1)/1)
holoColor(19,vec(0,0,255))
holoColor(20,vec(255,0,0))

#halbkreis deko models/sprops/misc/fittings/bend_long_90_6.mdl
holoModel(21,"models/sprops/misc/fittings/bend_long_90_6.mdl")
holoMaterial(21,"sprops/trans/wheels/wheel_d_rim2")
holoModel(22,"models/sprops/misc/fittings/bend_long_90_6.mdl")
holoMaterial(22,"sprops/trans/wheels/wheel_d_rim2")
holoAng(21,holoEntity(7):toWorld(ang(0,0,90)))
holoAng(22,holoEntity(7):toWorld(ang(-0,180,90)))
holoPos(21,holoEntity(7):toWorld(vec(-7,7,0)))
holoPos(22,holoEntity(7):toWorld(vec(7,-7,0)))

holoParent(21,holoEntity(6))
holoParent(22,holoEntity(6))
holoScale(21,vec(1,1.2,1)/1)
holoScale(22,vec(1,1.2,1)/1)
##holoColor(21,vec(100,100,100))
#holoColor(22,vec(100,100,100))

holoModel(23,"models/sprops/misc/fittings/bend_long_90_6.mdl")
holoMaterial(23,"sprops/trans/wheels/wheel_d_rim2")
holoModel(24,"models/sprops/misc/fittings/bend_long_90_6.mdl")
holoMaterial(24,"sprops/trans/wheels/wheel_d_rim2")
holoAng(23,holoEntity(7):toWorld(ang(0,90,90)))
holoAng(24,holoEntity(7):toWorld(ang(-0,-90,90)))
holoPos(23,holoEntity(7):toWorld(vec(-7,-7,0)))
holoPos(24,holoEntity(7):toWorld(vec(7,7,0)))
holoParent(23,holoEntity(6))
holoParent(24,holoEntity(6))
holoScale(23,vec(1,1.2,1)/1)
holoScale(24,vec(1,1.2,1)/1)
#holoColor(23,vec(100,100,100))
#holoColor(24,vec(100,100,100))
#rotor models/sprops/trans/air/prop_2b_s.mdl

holoScale(25,vec(1,1,1)/50)
holoAng(25,E:toWorld(ang(0,0,0)))
holoPos(25,E:toWorld(vec(0,0,10)))
holoParent(25,E)


holoScale(26,vec(1,1,1)/50)
#holoAng(26,E:toWorld(ang(0,0,0)))
#holoPos(26,E:toWorld(vec(0,0,10)))
holoParent(26,E)
MaxSpeed=50
I=2
}


MPH=toUnit("mph", E:vel():length())
T=(T+15)+(MPH*2)
holoAng(17,holoEntity(9):toWorld(ang(-90,0+T,0)))
holoAng(18,holoEntity(10):toWorld(ang(-90,0-T,0)))
holoAng(19,holoEntity(11):toWorld(ang(90,0-T,0)))
holoAng(20,holoEntity(12):toWorld(ang(-90,0+T,0)))



if(Active == 1)  {  
    MPH=toUnit("mph", entity():vel():length())
    #MPH=Rpm
    
    SoundPitch = MPH
          
    if (~Active) { E:soundPlay(1,0,"acf_extra/airfx/helirotor2.wav")}
    soundPitch(1,SoundPitch)
    
}
      
if(Active == 1)  {  
    #MPH=toUnit("mph", entity():vel():length())
    MPH=Speed
    SoundPitch = MPH*5
          
    if (~Active) { E:soundPlay(2,0,"vehicles/airboat/fan_blade_idle_loop1.wav") }
    soundPitch(2,SoundPitch)
    
}
if(Active){
    soundVolume(1,1)
    soundVolume(2,1)
}
else{
    
    soundVolume(1,0)
    soundVolume(2,0)
}

if(Active){Speed=10}
else{Speed=5}


#steuerung
if(Active){
#lightCreate(1,E:pos(),vec(255,255,255),1,10.1)

}
else{
#lightCreate(1,E:pos(),vec(0,0,0),1,10.1)

}
if(PrevWeapon){I=I+1}
if(NextWeapon){I=I-1}
if(Space){I=1500}
else{I=700}

#if(Dist<500){
E:applyForce(E:up()*I*5)
#}

E:applyForce(-E:vel()*E:mass()/150)

ForceT = E:up():cross(vec(0,0,1))
ForceT = E:toLocal(ForceT+E:pos())
ForceT *= 50 #2000 fuer volle stabilisierung
ForceT += vec(A-D,S-W,Mouse1-Mouse2)*50  #Gas voll
ForceT -= E:angVelVector()
ForceT *= 10
E:applyTorque(ForceT*E:inertia())

CamPos=E:toWorld(vec(150,0,20))
CamAng=(E:toWorld(ang(0,180,0)))
CamPar=E

#cameraCreate(1,CamPos,CamAng)
#cameraParent(1,CamPar)
#cameraToggle(1,Active,Driver)
