@name Egp-Touch-Example
@inputs User:entity Egp:wirelink
@outputs 
@persist Toggle
@trigger 
interval(50)

if(changed(Egp:entity())){
Egp:egpClear()

#left one#
Egp:egpBox(1,vec2(155,255),vec2(50,50))
##########

#right one#
Egp:egpBox(2,vec2(355,255),vec2(50,50))
##########

function number wirelink:touch(Index:number,Cursor:vector2){
return inrange(Cursor,This:egpPos(Index)-This:egpSize(Index),This:egpPos(Index)+This:egpSize(Index))
}
}
if(Egp:entity()){
Curs = Egp:egpCursor(User)
local KeyE = User:keyUse()
################################### 
#untoggled!
if(Egp:touch(1,Curs) & KeyE){
    Egp:egpColor(1,vec(255,0,0))    
        }else{
    Egp:egpColor(1,vec(0,255,0))   
}
################################### 
#toggled#
if(KeyE){
if(Egp:touch(2,Curs)){
Toggle=!Toggle
}
}
if(changed(Toggle)&Toggle){     Egp:egpColor(2,vec(255,0,0))   }
if(changed(!Toggle)&!Toggle){     Egp:egpColor(2,vec(0,255,0))   }
###################################
 
}

@name Egp-Touch-Example
@inputs User:entity Egp:wirelink
@outputs 
@persist Toggle
@trigger 
interval(50)

if(changed(Egp:entity())){
Egp:egpClear()

#left one#
Egp:egpBox(1,vec2(155,255),vec2(50,50))
##########

#right one#
Egp:egpBox(2,vec2(355,255),vec2(50,50))
##########

function number wirelink:touch(Index:number,Cursor:vector2){
return inrange(Cursor,This:egpPos(Index)-This:egpSize(Index),This:egpPos(Index)+This:egpSize(Index))
}
}
if(Egp:entity()){
Curs = Egp:egpCursor(User)
local KeyE = User:keyUse()
################################### 
#untoggled!
if(Egp:touch(1,Curs) & KeyE){
    Egp:egpColor(1,vec(255,0,0))    
        }else{
    Egp:egpColor(1,vec(0,255,0))   
}
################################### 
#toggled#
if(KeyE){
if(Egp:touch(2,Curs)){
Toggle=!Toggle
}
}
if(changed(Toggle)&Toggle){     Egp:egpColor(2,vec(255,0,0))   }
if(changed(!Toggle)&!Toggle){     Egp:egpColor(2,vec(0,255,0))   }
###################################
 
}

