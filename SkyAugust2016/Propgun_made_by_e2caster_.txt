@name Propgun made by e2caster 
@inputs 
@outputs 
@persist Shoot:entity
@trigger
runOnTick(1) 
runOnTick(1)
O=owner()
E=entity()


if(first()){
    hint("Made by e2caster ",5)
holoCreate(1)
holoScale(1,vec(0.4,0.4,5))
holoModel(1,"hqtube")
holoColor(1,vec(50,50,50))
holoPos(1,(E:pos()+vec(1,1,50)))

holoCreate(2)
holoScale(2,vec(0.1,0.1,0.9))
holoModel(2,"hqtube")
holoColor(2,vec(150,50,50))

holoCreate(3)
holoScale(3,vec(0.4,0.4,1))
holoModel(3,"hqsphere")
holoColor(3,vec(50,50,50))
holoPos(3,(E:pos()+vec(1,4.5,50)))
holoAng(3,ang(0,0,90))
holoMaterial(3,"models/props_combine/tprings_globe")
holoAlpha(3,50)

holoCreate(4)
holoScale(4,vec(0.6,0.6,1))
holoModel(4,"hqcylinder")
holoColor(4,vec(55,255,25))
holoPos(4,(E:pos()+vec(0,1,17)))
holoAng(4,ang(0,0,0))

holoCreate(5)
holoScale(5,vec(0.1,0.1,0.1))
holoModel(5,"models/XQM/panel360.mdl")
holoColor(5,vec(50,50,50))
holoPos(5,(E:pos()+vec(0,1,10.5)))
holoAng(5,ang(90,0,0))
}
holoParent(2,1)
holoParent(5,1)
holoParent(3,1)
holoParent(4,1)
holoAng(2,owner():attachmentAng("anim_attachment_RH")+ang(0,0,19))
holoPos(2,owner():attachmentPos("anim_attachment_RH")+vec(0,0,0))
holoAng(1,owner():attachmentAng("anim_attachment_RH")+ang(90,0,0))
holoPos(1,owner():attachmentPos("anim_attachment_RH")+vec(0,0,7))
holoParentAttachment(1,owner(),"anim_attachment_RH")
if(O:weapon():type()=="weapon_rpg"){
O:weapon():setMaterial("effects/ar2_altfire1b")
holoAlpha(2,255)
}else{
holoPos(1,owner():attachmentPos("anim_attachment_Back")+vec(0,0,7))
holoAng(2,owner():attachmentAng("anim_attachment_Back")+ang(0,0,10))
holoAlpha(2,1)
}

if(changed(owner():keyAttack2()) & owner():weapon():type() == "weapon_rpg"&owner():keyAttack2() & owner():weapon():type() == "weapon_rpg"){
    Shoot=propSpawn("models/props_phx/ww2bomb.mdl",(O:pos()+vec(0,0,40))+O:eye()*55,0)
    Shoot:propGravity(0)
    Shoot:applyForce(O:eye()*999999999)
    Shoot:setAng(O:eyeAngles())
    Shoot:setTrails(10,0,4,"trails/smoke",vec(25,25,25),255)
    holoColor(4,vec(255,0,0))
}


timer("t",425)
if(clk("t")){
holoColor(4,vec(55,255,25))
}

#Remove this if its not on your server !#

AA=owner():aimPos()
B=owner():keyPressed("B")
if(changed(B)&B){
 owner():plySetPos(AA)   
}
#########################################
@name Propgun made by e2caster 
@inputs 
@outputs 
@persist Shoot:entity
@trigger
runOnTick(1) 
runOnTick(1)
O=owner()
E=entity()


if(first()){
    hint("Made by e2caster ",5)
holoCreate(1)
holoScale(1,vec(0.4,0.4,5))
holoModel(1,"hqtube")
holoColor(1,vec(50,50,50))
holoPos(1,(E:pos()+vec(1,1,50)))

holoCreate(2)
holoScale(2,vec(0.1,0.1,0.9))
holoModel(2,"hqtube")
holoColor(2,vec(150,50,50))

holoCreate(3)
holoScale(3,vec(0.4,0.4,1))
holoModel(3,"hqsphere")
holoColor(3,vec(50,50,50))
holoPos(3,(E:pos()+vec(1,4.5,50)))
holoAng(3,ang(0,0,90))
holoMaterial(3,"models/props_combine/tprings_globe")
holoAlpha(3,50)

holoCreate(4)
holoScale(4,vec(0.6,0.6,1))
holoModel(4,"hqcylinder")
holoColor(4,vec(55,255,25))
holoPos(4,(E:pos()+vec(0,1,17)))
holoAng(4,ang(0,0,0))

holoCreate(5)
holoScale(5,vec(0.1,0.1,0.1))
holoModel(5,"models/XQM/panel360.mdl")
holoColor(5,vec(50,50,50))
holoPos(5,(E:pos()+vec(0,1,10.5)))
holoAng(5,ang(90,0,0))
}
holoParent(2,1)
holoParent(5,1)
holoParent(3,1)
holoParent(4,1)
holoAng(2,owner():attachmentAng("anim_attachment_RH")+ang(0,0,19))
holoPos(2,owner():attachmentPos("anim_attachment_RH")+vec(0,0,0))
holoAng(1,owner():attachmentAng("anim_attachment_RH")+ang(90,0,0))
holoPos(1,owner():attachmentPos("anim_attachment_RH")+vec(0,0,7))
holoParentAttachment(1,owner(),"anim_attachment_RH")
if(O:weapon():type()=="weapon_rpg"){
O:weapon():setMaterial("effects/ar2_altfire1b")
holoAlpha(2,255)
}else{
holoPos(1,owner():attachmentPos("anim_attachment_Back")+vec(0,0,7))
holoAng(2,owner():attachmentAng("anim_attachment_Back")+ang(0,0,10))
holoAlpha(2,1)
}

if(changed(owner():keyAttack2()) & owner():weapon():type() == "weapon_rpg"&owner():keyAttack2() & owner():weapon():type() == "weapon_rpg"){
    Shoot=propSpawn("models/props_phx/ww2bomb.mdl",(O:pos()+vec(0,0,40))+O:eye()*55,0)
    Shoot:propGravity(0)
    Shoot:applyForce(O:eye()*999999999)
    Shoot:setAng(O:eyeAngles())
    Shoot:setTrails(10,0,4,"trails/smoke",vec(25,25,25),255)
    holoColor(4,vec(255,0,0))
}


timer("t",425)
if(clk("t")){
holoColor(4,vec(55,255,25))
}

#Remove this if its not on your server !#

AA=owner():aimPos()
B=owner():keyPressed("B")
if(changed(B)&B){
 owner():plySetPos(AA)   
}
#########################################
