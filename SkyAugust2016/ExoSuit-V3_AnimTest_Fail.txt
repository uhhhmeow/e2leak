@name ExoSuit-V3 AnimTest Fail
@inputs Cam:wirelink
@outputs 
@persist Parts:string E2Name:string Hcount Ccount SpawnCounter [Holo Clip]:gtable MechStatus:string Mode:string WorldDownVec:vector
@persist [FRLeg FLLeg BRLeg BLLeg]:table
@model models/hunter/blocks/cube05x05x05.mdl
E = entity()
if(first()){

WorldDownVec = vec(0,0,-1)
runOnTick(1)
runOnLast(1)
#include "GeneralMechFunctions"
function entity h(N){ return holoEntity(N) }
function setMode(Name:string){ Mode = Name }
#######################################################################################################################
#######################################################################################################################
#######################################################################################################################

timer("setup",800)

E2Name = "ExoSuit-V3"

Parts = "Search"

function table:spawn(Shading){
  local Index = This[1,number]
  local Vector = This[2,vector]
  local Scale = This[3,vector]
  local Angle = This[4,angle]
  local Color = This[5,vector]
  local Model = This[6,string]
  local Parent = This[7,number]
  local Material = This[8,string]

   if(Parent==0){
    PEnt = entity() 
   }else{
    PEnt = holoEntity(Parent)
   }

  holoCreate(Index,PEnt:toWorld(Vector),Scale,entity():toWorld(Angle),Color,Model)
  holoMaterial(Index,Material)
  holoDisableShading(Index,Shading)


   if(Parent == 0){
    holoParent(Index,entity())
   }else{
    holoParent(Index,Parent)
   }

}
function table:clip(){
  local Index = This[1,number]
  local Pos = This[2,vector]
  local Direction = This[3,vector]
  local SecondIndex = This[4,number]
}

  function spawnHolos(){
   switch (Parts){
       case "Search", 
        if(clk("setup")){
         stoptimer("setup")
          Holo = gTable("Holo"+E2Name) 
          Clip = gTable("Clip"+E2Name)       
          Hcount = Holo:count()
          Ccount = Clip:count()
          entity():soundPlay(1,soundDuration("buttons/button18.wav"),"buttons/button18.wav")         
         Parts = "SpawnHolo" 
        }
       break

       case "SpawnHolo",
        while(perf() & holoCanCreate() & SpawnCounter < Hcount){
        SpawnCounter+=1         
         Holo[SpawnCounter,table]:spawn(0)
          if(SpawnCounter >= Hcount){          
           SpawnCounter = 0
           Parts = Ccount > 0 ? "LoadClips" : "showStatus"            
          }        
        }
        
       break
            
       case "LoadClips",
        while(perf() & SpawnCounter < Ccount){
         SpawnCounter += 1         
        if(SpawnCounter >= Ccount){
          SpawnCounter = 0
          Parts = "showStatus"
        
       }
      }
       break
    
      case "showStatus",
          entity():soundPlay(2,soundDuration("buttons/button14.wav"),"buttons/button14.wav")         
           printColor(vec(255),"[",vec(255,0,0),"E2",vec(255),"]",vec(255,255,0),owner():name()+" the Hololoader spawned "+Hcount+" hologram/s and created "+Ccount+" holoclips")    
           Parts = "finished"
#         Holo:clear()
#         Clip:clear() 
          timer("runMech",1)
     break
    
  
     case "finished",
     runOnTick(0)
     setMode("setupMech")
     Parts = ""
     break

  }
 }
#######################################################################################################################
#######################################################################################################################
#######################################################################################################################

function table setupLeg(Speed,StartAnim,EndCounter,Sheight,Glength,MaxDist,Vel,Base:entity,FeetHolo){

return table(
"Speed" = Speed
,"StartAnim" = StartAnim
,"EndCounter" = EndCounter
,"SHeight" = Sheight
,"Glength" = Glength
,"MaxDist" = MaxDist
,"Vel" = Vel
,"Base" = Base
,"VecDown" = WorldDownVec
,"FeetHolo" = FeetHolo
)


}
 function table:anim(EndVector:vector,Speed){
 local T = This
#[-----------Counter & AnimFix----------]#
#local Speed = T["Speed",number]
local EndCounter = T["EndCounter",number]
local StartAnim = T["StartAnim",number]
local Sheight = T["Sheight",number]
#local EndVector = T["EndVector",vector]

local Glength = T["Glength",number]
local MaxDist = T["MaxDist",number]
local Vel = T["Vel",number]
local Base = T["Base",entity]
local VecDown = T["VecDown",vector]
local FeetHolo = T["FeetHolo",number]

if(T["Parts",string]!="Setupanim"&T["Parts",string]!="Doanim"){ T["Parts",string] = "Setupanim" }

T["Count.data",number] = (T["Count.data",number] + 1) % EndCounter
#[---------------Ranger-----------------]#

 rangerFilter(Base)
 rangerFilter(Seat)
 rangerFilter(PropLock["Locker",entity])
 local TempA = rangerOffset(Glength,EndVector+Base:vel():setZ(0)/Vel,VecDown)
 T["RangerPos",vector]=TempA:position()

#[------------------Anim----------------]#

 switch (T["Parts",string]){
    case "Setupanim",
    if(TempA:hit()){
     if(T["Count.data",number]==StartAnim){
      if(T["StartPos.data",vector]:distance(  T["RangerPos",vector] )>=MaxDist){ 
       T["StartPos.data",vector] = T["Bezier",vector]
       T["Distance.data",number] = (T["StartPos.data",vector]- T["RangerPos",vector]):length()
#       T["Middle.data",vector] = mix( T["RangerPos",vector],T["StartPos.data",vector],0.99) + vec(0,0,max(T["Distance.data",number] / Sheight,MaxDist/5)) 
       T["Middle.data",vector] = T["RangerPos",vector] + vec(0,0,max(T["Distance.data",number] / Sheight,MaxDist/5)) 
       T["SaveRanger",ranger] = rangerOffset(99999,T["RangerPos",vector],vec(0,0,-1))
      T["Parts",string] = "Doanim"
      }  
     }  
    }
    break  
    case "Doanim",
    
      T["GaitVelocity",number] = T["GaitVelocity",number]*0.1 + (Base:vel():length() + abs(Base:angVelVector():length()))
      T["GC",number] = T["GC",number] + (0.01 + 0.02*T["GaitVelocity",number]/(10 * Speed))
      T["Move_Count.data",number] = min(T["Move_Count.data",number] + T["GC",number],1)
     
       T["Bezier",vector] = bezier(T["StartPos.data",vector],T["Middle.data",vector], T["RangerPos",vector],T["Move_Count.data",number])    

          if(T["Move_Count.data",number]>=1){
            holoEntity(FeetHolo):soundPlay(FeetHolo*randint(999),1.3,"npc/dog/dog_footstep"+randint(1,4)+".wav")
           
            T["Move_Count.data",number] = T["GaitVelocity",number] = T["GC",number] = 0
            T["Parts",string] = "Setupanim"
          }
        
    break
 }
#[-----------------Foot-----------------]#
  if(Base){
    local HT = T["SaveRanger",ranger]:hitNormal():toAngle()
    holoAng(FeetHolo,Base:toWorld(ang(-HT[1]+90,0,0)))    
  } 
#[--------------------------------------]#
 }
function table:setOrder(Speed,StartAnim, MaxCounter, Vel, Sheight) {
    This["Speed",number] = Speed
    This["EndCounter",number] = MaxCounter
    This["StartAnim",number] = StartAnim
    This["Vel", number] = Vel
    This["Sheight", number] = Sheight
}
function vector table:fixIt(TempV:vector){
   This["Middle.data", vector] = TempV
 This["StartPos.data", vector] = TempV
 This["RangerPos",vector] = TempV
 This["Bezier",vector] = TempV
#This["Parts",string]="Doanim"
}
#######################################################################################################################
#######################################################################################################################
#######################################################################################################################

function setupMech(){
Cam:checkSeat(E:toWorld(vec(30,0,-20)),E:toWorld(ang(0,-90,0)),E,Seat)

setMode("setupWalk")
}
#######################################################################################################################
#######################################################################################################################
#######################################################################################################################

local StepSpeed = 7
local StepDelay = 8
local MaxStepHeight = 2
local StepVel = 5
local Add = -5
FRLeg = setupLeg(StepSpeed,1,StepDelay*2,MaxStepHeight,125,10,StepVel,E,7)
FLLeg = setupLeg(StepSpeed,1+StepDelay,StepDelay*2,MaxStepHeight,125,10,StepVel,E,8)
BRLeg = setupLeg(StepSpeed,4+StepDelay,StepDelay*2,MaxStepHeight,125,10,StepVel,E,7)
BLLeg = setupLeg(StepSpeed,4,StepDelay*2,MaxStepHeight,125,10,StepVel,E,8)


function setupWalk(){
local StepSpeed = 7
local StepDelay = 8
local MaxStepHeight = 2
local StepVel = 5
local Add = -5
FRLeg:setOrder(StepSpeed,1,StepDelay*2,StepVel,MaxStepHeight)
FLLeg:setOrder(StepSpeed,1+StepDelay,StepDelay*2,StepVel,MaxStepHeight)
BRLeg:setOrder(StepSpeed,4+StepDelay,StepDelay*2,StepVel,MaxStepHeight)
BLLeg:setOrder(StepSpeed,4,StepDelay*2,StepVel,MaxStepHeight)
setMode("walking")
}

function walking(){
local StepSpeed = 7

local Add = -5
FRLeg:anim(holoEntity(1):toWorld(vec(0,Add,0)),StepSpeed)
nik(55,55,holoEntity(1):pos(),FRLeg["Bezier",vector],1,5,E)
FLLeg:anim(holoEntity(2):toWorld(vec(0,-Add,0)),StepSpeed)
nik(55,55,holoEntity(2):pos(),FLLeg["Bezier",vector],2,6,E)

BRLeg:anim(holoEntity(3):toWorld(vec(0,-Add,0)),StepSpeed)
ik3seg(E,holoEntity(3),holoEntity(9),holoEntity(11),BRLeg["Bezier",vector],40,35,50,15)
BLLeg:anim(holoEntity(4):toWorld(vec(0,Add,0)),StepSpeed)
ik3seg(E,holoEntity(4),holoEntity(10),holoEntity(12),BLLeg["Bezier",vector],40,35,50,15)



}

}
   spawnHolos()  
  
if(clk("runMech")&Parts == ""){
timer("runMech",60) 
   Mode()

if(last()){
holoDeleteAll()    
}
}
