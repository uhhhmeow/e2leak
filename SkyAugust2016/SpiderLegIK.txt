@name SpiderLegIK
@inputs 
@outputs 
@persist Move:table
@model models/hunter/blocks/cube025x025x025.mdl
interval(60)
 E = entity()
if(first()|duped()){
E:propGravity(0)
E:setAlpha(0)
#include "GeneralMechFunctions"
c(1,E:toWorld(vec(0,0,0)),vec(1),E:toWorld(ang()),vec(255),"hqsphere",E,0)
c(2,holoEntity(1):toWorld(vec(0,0,0)),vec(1,1,1),E:toWorld(ang()),vec(255,0,0),"hqsphere",1,255)
c(3,holoEntity(2):toWorld(vec(0,0,80)),vec(1,1,1),E:toWorld(ang()),vec(255,0,0),"hqsphere",2,255)
c(4,holoEntity(3):toWorld(vec(0,0,80)),vec(1,1,1),E:toWorld(ang()),vec(255,0,0),"hqsphere",3,255)

c(5,holoEntity(2):toWorld(vec(0,0,40)),vec(0.7,0.7,6),E:toWorld(ang()),vec(255),"hqcylinder",2,255)
c(6,holoEntity(3):toWorld(vec(0,0,40)),vec(0.7,0.7,6),E:toWorld(ang()),vec(255),"hqcylinder",3,255)


function ik(L1,L2,Base:entity,End:vector,H1,H2,H3){
        local Rotation = ang(0, 0, 0)
        local Dir = (End - holoEntity(H1):pos())
        local AxisA = E:toLocalAxis(Dir):rotate(Rotation)
        local AxisB = holoEntity(H1):toLocalAxis(Dir)
        local LegCount = min(Dir:length(), L1 + L2)
        local Hip_p = atan(-AxisB[3], AxisB[1]) + acos((LegCount^2 + L1^2 - L2^2) / (2 * L1 * LegCount)) - 90
        local Knee = acos((L1^2 + L2^2 - LegCount^2) / (2*L1*L2))
        holoAng(H1, Base:toWorld(ang(0, atan(AxisA[2], AxisA[1]) + 180, 0) - Rotation))
        holoAng(H2, holoEntity(H1):toWorld(ang(Hip_p+180, 0, 0)))
        holoAng(H3, holoEntity(H2):toWorld(ang(Knee+180, 0, 0)))            
}

}
if(E){
   local Aim = owner():aimPos()
Move:setMoveVector(0,E,Aim,20,10,100,1,"Sl1")
   
   ik(80,80,E,Move["FinalVec"+"Sl1",vector] ,1,2,3)

}
@name SpiderLegIK
@inputs 
@outputs 
@persist Move:table
@model models/hunter/blocks/cube025x025x025.mdl
interval(60)
 E = entity()
if(first()|duped()){
E:propGravity(0)
E:setAlpha(0)
#include "GeneralMechFunctions"
c(1,E:toWorld(vec(0,0,0)),vec(1),E:toWorld(ang()),vec(255),"hqsphere",E,0)
c(2,holoEntity(1):toWorld(vec(0,0,0)),vec(1,1,1),E:toWorld(ang()),vec(255,0,0),"hqsphere",1,255)
c(3,holoEntity(2):toWorld(vec(0,0,80)),vec(1,1,1),E:toWorld(ang()),vec(255,0,0),"hqsphere",2,255)
c(4,holoEntity(3):toWorld(vec(0,0,80)),vec(1,1,1),E:toWorld(ang()),vec(255,0,0),"hqsphere",3,255)

c(5,holoEntity(2):toWorld(vec(0,0,40)),vec(0.7,0.7,6),E:toWorld(ang()),vec(255),"hqcylinder",2,255)
c(6,holoEntity(3):toWorld(vec(0,0,40)),vec(0.7,0.7,6),E:toWorld(ang()),vec(255),"hqcylinder",3,255)


function ik(L1,L2,Base:entity,End:vector,H1,H2,H3){
        local Rotation = ang(0, 0, 0)
        local Dir = (End - holoEntity(H1):pos())
        local AxisA = E:toLocalAxis(Dir):rotate(Rotation)
        local AxisB = holoEntity(H1):toLocalAxis(Dir)
        local LegCount = min(Dir:length(), L1 + L2)
        local Hip_p = atan(-AxisB[3], AxisB[1]) + acos((LegCount^2 + L1^2 - L2^2) / (2 * L1 * LegCount)) - 90
        local Knee = acos((L1^2 + L2^2 - LegCount^2) / (2*L1*L2))
        holoAng(H1, Base:toWorld(ang(0, atan(AxisA[2], AxisA[1]) + 180, 0) - Rotation))
        holoAng(H2, holoEntity(H1):toWorld(ang(Hip_p+180, 0, 0)))
        holoAng(H3, holoEntity(H2):toWorld(ang(Knee+180, 0, 0)))            
}

}
if(E){
   local Aim = owner():aimPos()
Move:setMoveVector(0,E,Aim,20,10,100,1,"Sl1")
   
   ik(80,80,E,Move["FinalVec"+"Sl1",vector] ,1,2,3)

}
