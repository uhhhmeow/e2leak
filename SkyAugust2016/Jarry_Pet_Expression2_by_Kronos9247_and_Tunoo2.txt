@name Jarry Pet Expression2 by Kronos9247 and Tunoo2
@inputs 
@outputs 
@persist Prop:entity RegisteredJobs:array NoJobs:array Name:string
@persist Jobs:array Targets:array Cmds:array FoodP:array EssensVerbrauch:number
@persist WC:entity FoodLevel:number KakaLevel:number WC_Satz:array
@persist CloseJobs:number KakaTime:number Kaka:number FoodProps:array
@trigger 

E=entity()

interval(10)

runOnLast(1)
runOnChat(1)

if(first()) {
    Name="Jarry"
    CloseJobs = 0
    KakaLevel = 20
    EssensVerbrauch=0
    FoodLevel=-10
    
    #ifdef setName(string)
    
    setName(Name + " Pet")
    
    #endif
    
    holoCreate(1)
    holoModel(1, "models/holograms/hq_rcube.mdl")
    holoMaterial(1, "sprops/textures/gear_metal")
    holoPos(1, E:pos()+vec(0,0,5))
    
    holoCreate(2)
    holoScale(2, vec(0.1))
    holoColor(2, vec(255))
    holoPos(2, holoEntity(1):pos()+vec(6,2.5,2.5))
    holoParent(2, 1)
    holoCreate(3)
    holoScale(3, vec(0.05))
    holoColor(3, vec(0))
    holoPos(3, holoEntity(1):pos()+vec(6.5,2.5,2.5))
    holoParent(3, 1)
    
    holoCreate(4)
    holoScale(4, vec(0.1))
    holoColor(4, vec(255))
    holoPos(4, holoEntity(1):pos()+vec(6,-2.5,2.5))
    holoParent(4, 1)
    holoCreate(5)
    holoScale(5, vec(0.05))
    holoColor(5, vec(0))
    holoPos(5, holoEntity(1):pos()+vec(6.5,-2.5,2.5))
    holoParent(5, 1)
    
    holoCreate(6)
    holoScale(6, vec(0.12,0.12,0.12))
    holoColor(6, vec(200))
    holoPos(6, holoEntity(1):pos()+vec(0,-6.5,1.7))
    holoMaterial(6, "sprops/textures/gear_metal")
    holoParent(6, 1)
    
    holoCreate(7)
    holoScale(7, vec(0.1,0.1,0.4))
    holoColor(7, vec(255))
    holoPos(7, holoEntity(1):pos()+vec(0,-6.5,0))
    holoMaterial(7, "sprops/textures/sprops_wood1")
    holoParent(7, 6)
    
    holoCreate(8)
    holoScale(8, vec(0.12,0.12,0.12))
    holoColor(8, vec(200))
    holoPos(8, holoEntity(1):pos()+vec(0,6.5,1.7))
    holoMaterial(8, "sprops/textures/gear_metal")
    holoParent(8, 1)
    
    holoCreate(9)
    holoScale(9, vec(0.1,0.1,0.4))
    holoColor(9, vec(255))
    holoPos(9, holoEntity(1):pos()+vec(0,6.5,0))
    holoMaterial(9, "sprops/textures/sprops_wood1")
    holoParent(9, 8)
    
    NoJobs=array("Das verstehe ich nicht!",
                "Ich verstehe das nicht!",
                "Ich glaube du meintest was anderes!")
                
    WC_Satz=array("Ich muss aufs WC!",
                  "Ich muss aufs Schtilleroertchen!",
                  "Ich muss mein Geschaeft erledigen!")
                
    FoodProps=array("models/props_junk/watermelon01.mdl",
                    "models/props_junk/garbage_takeoutcarton001a.mdl",
                    "models/food/burger.mdl",
                    "models/food/hotdog.mdl",
                    "models/noesis/donut.mdl")
    
    RegisteredJobs=array("kille",
                        "folge",
                        "schlafen",
                        "finde",
                        "ops",
                        "fussball",
                        "basketball")
                        
    #hablehabluabulahableha
    
    function array split(Index:number) {
        
        return Jobs[Index, string]:explode(" ")
    }
    function vector2 transformeVector(Vec:vector) {
        
        return vec2(Vec:x(), Vec:y())
    }
    function vector transformeVector2(Vec:vector2, Z:number) {
        
        return vec(Vec:x(), Vec:y(), Z)
    }
    function number setHight() {
        R=rangerOffset(10,holoEntity(1):pos(),-holoEntity(1):up())
        holoPos(1,holoEntity(1):pos()+vec(0,0,-R:distance()+6))
        
    }
    function void moveTo(Vec:vector2, Speed:number) {
        setHight()
        holoAng(1, (transformeVector2(Vec,holoEntity(1):pos():z())-holoEntity(1):pos()):toAngle())
        holoPos(1, holoEntity(1):pos()+holoEntity(1):forward()*Speed)
        
    }
    function number inRange(Entity:vector, Entity2:vector, Range:number) {
        local Number=0
        if(Entity:distance(Entity2) <= Range) { Number=1 }
        
        return Number
    }
    function string covertToString(Strings:array, Begin:number) {
        local Cmd = Strings[Begin, string]
        if(Strings:count() > Begin) {
            for(I=(Begin+1), Strings:count()) {
                Cmd += " " + Strings[I, string]
                
            }
            
        }
        
        return Cmd
    }
    function array ifJob(String:string) {
        local Number = 0
        local Cmd = ""
        
        for(I=1, RegisteredJobs:count()) {
        
            if(String:find(RegisteredJobs[I, string])) { 
                Number = 1 
                Cmd = RegisteredJobs[I, string]
                
            }
        
        }
        
        return array(Number, Cmd)
    }
    function entity getPlayer(String:string) {
        local Players = players()
        local Player = noentity()
        
        for(I=1, Players:count()) {
            
            if(String:find(Players[I, entity]:name())) {
                Player = Players[I, entity]
                
            }
            
        }
        
        return Player
    }
    function void checkJob(JobIndex:number, UseTarget:number) {
        
        if(Targets[JobIndex, entity]:isPlayer()==0&&UseTarget==1) {
            Jobs:removeString(JobIndex)
            Targets:removeEntity(JobIndex)
            Cmds:removeString(JobIndex)
            
        }
        
    }
    function number ifThisJob(Cmd:string, UseTarget:number, JobIndex:number) {
        local Player = Targets[JobIndex, entity]
        local Start = 0
        
        if(Cmds[JobIndex, string] == Cmd) {
            if(UseTarget == 1 && Player != noentity()) {
                checkJob(JobIndex, UseTarget)
                Start=1
                
            }
            elseif(UseTarget == 0) {
                Start=1
                
            }
            
        }
        
        return Start
    }
    function number findWord(Cmd:string, JobIndex:number) {
        
        return Jobs[JobIndex, string]:find(Cmd)
    }
    function number ifThisJob(Cmd:string, Cmd2:string, UseTarget:number, JobIndex:number) {
        local Player = Targets[JobIndex, entity]
        local Start = 0
        
        if(Cmds[JobIndex, string]== Cmd) {
            if(UseTarget == 1 && Player != noentity()) {
                checkJob(JobIndex, UseTarget)
                Start=1
                
            }
            elseif(UseTarget == 0) {
                Start=1
                
            }
            
        }
        
        return Start
    }
    function void printChat(Text:string) {
        printColor(vec(255,255,0), Name, vec(255), ": " + Text)
        
    }
    function void searchFood() {
        timer("updateSearchingFood",1)
        
        if(clk("updateSearchingFood")) {
            
            findIncludePlayerProps(owner())
            #findIncludeClass("food_*")
            findByName("models/food/burger.mdl")
            findInSphere(holoEntity(1):pos(),10000)
            FoodP:clear()
            FoodP=findToArray()
        }
    }
    
}

if(EssensVerbrauch == 1) {
if(FoodLevel < KakaLevel) {
searchFood()
#print(FoodP:count())
print(FoodLevel)
if(FoodP[1, entity]) {
    CloseJobs=1
    
    Boost = holoEntity(1):pos():distance(FoodP[1, entity]:pos())/100
    if(!inRange(FoodP[1, entity]:pos(), holoEntity(1):pos(), 20)) {
        moveTo(transformeVector(FoodP[1, entity]:pos()),1*Boost)
            
    }
    else {
        FoodP[1, entity]:propBreak()
        FoodP:removeEntity(1)
        FoodLevel += 1
        CloseJobs=0
        
    }
}
}

if(changed(FoodLevel) && FoodLevel >= KakaLevel) { printChat(WC_Satz[randint(1,WC_Satz:count()), string]) }

if(FoodLevel >= KakaLevel) {
    
    if(WC) {
        CloseJobs = 1
        
        Boost = holoEntity(1):pos():distance(WC:pos())/100
        if(!inRange(WC:pos(), holoEntity(1):pos(), 10)) {
            moveTo(transformeVector(WC:pos()),1*Boost)
            
        }
        else {
            Kaka=1
            KakaTime=randint(300,1000)
            timer("Kaka", KakaTime)
            printChat(WC_Satz[randint(1,WC_Satz:count()), string])
            
        }
        
    }
    else {
        printChat(WC_Satz[randint(1,WC_Satz:count()), string])
        FoodLevel=0
        
    }
    
    
}

if(Kaka==1) {
    if(clk("Kaka")) {
        CloseJobs=0
        Kaka=0
        FoodLevel=0
        
    }
}
}



if(CloseJobs == 0) {
local Player = Targets[1, entity]
if(ifThisJob("folge", 1, 1)) {
    
    Boost = holoEntity(1):pos():distance(Player:pos())/100
    
    if(!inRange(Player:pos(), holoEntity(1):pos(), 50)) {
        moveTo(transformeVector(Player:pos()),1*Boost)
    
    }
    
}
elseif(ifThisJob("kille", 1, 1)) {
    
    Boost = holoEntity(1):pos():distance(Player:pos())/100
    
    if(!inRange(Player:pos(), holoEntity(1):pos(), 50)) {
        moveTo(transformeVector(Player:pos()),2*Boost)
    
    }
    else {
        local Prop = propSpawn("models/props_phx/misc/potato_launcher_explosive.mdl", holoEntity(1):pos(), ang(0), 1)
        Prop:propBreak()
        
    }
    
    if(!Player:isAlive()) {
        Jobs:removeString(1)
        Targets:removeEntity(1)
        Cmds:removeString(1)
        printChat("Ich habe " + Player:name() + " gekillt!")
          
    }
    
}
elseif(ifThisJob("schlafen", 0, 1)) {
    
}
elseif(ifThisJob("basketball", 0, 1)) {
    # [[153,56,7] sprops/trans/lights/light_plastic	]#
    
    if(findWord("spiele", 1)==1||findWord("Spiele", 1)==1) {
        
    }
}
elseif(ifThisJob("fussball", 0, 1)) {
    if(findWord("spiele", 1)==1||findWord("Spiele", 1)==1) {
        findByModel("models/props_phx/misc/soccerball.mdl")
        findInSphere(holoEntity(1):pos(), 10000)
        local Ball = findClosest(holoEntity(1):pos())
        
        Boost = holoEntity(1):pos():distance(Ball:pos())/100
        
        if(Ball) {
        Ball:propFreeze(0)
        
        if(!inRange(Ball:pos(), holoEntity(1):pos(), 20)) {
            moveTo(transformeVector(Ball:pos()),1.5*Boost)
    
        }
        else {
            local Ran = randint(1,3)
            
            if(Ran == 1) {
                 Ball:applyForce(holoEntity(1):forward()*Ball:mass()*randint(70,250))
            }
            elseif(Ran == 2) {
                 Ball:applyForce(holoEntity(1):right()*Ball:mass()*randint(70,250))
            }
            elseif(Ran == 3) {
                 Ball:applyForce(-holoEntity(1):right()*Ball:mass()*randint(70,250))
            }
            
        }
        }
        else {
            printChat("Ich habe keinen Ball zum Spielen!")
    
            Jobs:removeString(1)
            Targets:removeEntity(1)
            Cmds:removeString(1)
        }
        
    }
}
elseif(ifThisJob("ops", 0, 1)) {
    printChat("Ich habe " + ops() + " OPS und " + round(ops()/100) + "% OPS auslastung!")
    
    Jobs:removeString(1)
    Targets:removeEntity(1)
    Cmds:removeString(1)
}
elseif(ifThisJob("finde", 0, 1)) {
    
    if(findWord("expression2", 1)==1||findWord("e2", 1)==1||findWord("chip", 1)==1) {
        Boost = holoEntity(1):pos():distance(entity():pos())/100
        
        if(!inRange(entity():pos(), holoEntity(1):pos(), 50)) {
            moveTo(transformeVector(entity():pos()),1*Boost)
            
        }
        else {
            Jobs:removeString(1)
            Targets:removeEntity(1)
            Cmds:removeString(1)
            
        }
    }
    
}
}

Last=owner():lastSaid():explode(" ")
if(chatClk(owner())&Last:string(1)=="//do"&&Last:count()==1){
    Jobs:clear()
    hideChat(1)
    printColor(vec(255), "Du mich von meinen Aufgaben entbunden!")
    
}
elseif(chatClk(owner())&Last:string(1)=="//do"&&Last:count()>=2){
    hideChat(1)
    local JobData = ifJob(covertToString(Last, 2))
    local Number = JobData[1, number]
    local Cmd = JobData[2, string]
    
    if(Number == 1) {
        local Player = getPlayer(covertToString(Last, 2))
        
        Jobs:pushString(covertToString(Last, 2))
        if(Player:isPlayer()==1) { Targets:pushEntity(Player) }
        else { Targets:pushEntity(noentity()) }
        Cmds:pushString(Cmd)
        
        printColor(vec(255), "Das mache ich gleich Master!")
        
    }
    else {
        printColor(vec(255), NoJobs[randint(1,NoJobs:count()),string])
        local Frage = randint(1,100)
        local Player = players()
        local Cmd = Last[2, string]
        if(Last:count() > 2) {
            for(I=3, Last:count()) {
                Cmd += " " + Last[I, string]
                
            }
            
        }
        
        if(Frage >= 90) { 
            chatPrint(vec(255,255,0), Name, vec(255), ": Wist ihr was ", vec(0,255,255), "" + owner():name(), vec(255), " mit ''" + Cmd + "'' gemeint hat?")
            
        }
        
    }
    
    
}
elseif(chatClk(owner())&Last:string(1)=="//don't"&&Last:count()==1){
    hideChat(1)
    
    printChat("Das ist eine kleine Liste von Aufgaben:")
    
    for(I=1, Jobs:count()) {
        printChat("   -- ["+I+"] " + Jobs[I, string])
        
    }
    
}
elseif(chatClk(owner())&Last:string(1)=="//don't"&&Last:count()==2){
    hideChat(1)
    local Number = Last[2, string]:toNumber()
    
    if(Number <= Jobs:count()) {
        printChat("Du hast mich von der " + Number + ". Aufgabe erloest!")
        Jobs:removeString(Number)
        Targets:removeEntity(Number)
        Cmds:removeString(Number)
        
    }
    else {
        printChat("Diese Aufgabe gibt es nicht!")
        
    }
    
}
elseif(chatClk(owner())&Last:string(1)=="//spawn"&&Last:count()==2){
    hideChat(1)
    
    if(Last[2, string] == "WC") {
        if(!WC) {
            WC=propSpawn("models/props_c17/metalPot002a.mdl", owner():aimPos()+vec(0,0,2), ang(), 1)
        }
        elseif(WC) {
            WC:propDelete()
            WC=propSpawn("models/props_c17/metalPot002a.mdl", owner():aimPos()+vec(0,0,2), ang(), 1)
        }
        
    }
    elseif(Last[2, string] == "Haus") {
        
    }
    elseif(Last[2, string] == "Fussball") {
        local Prop = propSpawn("models/props_phx/misc/soccerball.mdl", entity():pos()+vec(0,0,100), ang(0), 1)
        Prop:propFreeze(0)
        
    }
    elseif(Last[2, string] == "Basketball") {
        # [[153,56,7] sprops/trans/lights/light_plastic	]#
        local Prop = propSpawn("models/props_phx/misc/soccerball.mdl", entity():pos()+vec(0,0,100), ang(0), 1)
        Prop:setMaterial("sprops/trans/lights/light_plastic")
        Prop:setColor(vec(153,56,7))
        Prop:propFreeze(0)
        
    }
    
}

if(last()) {
    propDeleteAll()
    
}


