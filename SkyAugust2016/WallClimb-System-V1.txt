@name WallClimb-System-V1
@inputs 
@outputs 
@persist [N1 N2 N3 N4]:vector
@trigger 
@model models/hunter/blocks/cube05x05x05.mdl
interval(100)

E=entity()
N1=E:toWorld(vec(40,40,-40))
N2=E:toWorld(vec(40,-40,-40))
N3=E:toWorld(vec(-40,40,-40))
N4=E:toWorld(vec(-40,-40,-40))
if(first()){
    E:propGravity(0)
function number c(I,V:vector,S:vector,A:angle,Col:vector,Model:string,Alpha:number){ holoCreate(I,V,S,A,Col,Model) holoAlpha(I,Alpha) }
function number c(I,V:vector,S:vector,A:angle,Col:vector,Model:string,Parent:entity,Alpha:number){ holoCreate(I,V,S,A,Col,Model) holoAlpha(I,Alpha) holoParent(I,Parent) }
function number c(I,V:vector,S:vector,A:angle,Col:vector,Model:string,Parent:number,Alpha:number){ holoCreate(I,V,S,A,Col,Model) holoAlpha(I,Alpha) holoParent(I,Parent) }



function applyStuff(){
rangerFilter(E)
HoverRan = rangerOffset(1000,E:toWorld(vec(0,0,0)),E:up()*-1)
FR = 20
RL = 20
rangerFilter(E)
R1 = rangerOffset(140,E:toWorld(vec(FR,RL,50)),E:up()*-1)
rangerFilter(E)
R2 = rangerOffset(140,E:toWorld(vec(FR,-RL,50)),E:up()*-1)
rangerFilter(E)
R3 = rangerOffset(140,E:toWorld(vec(-FR,RL,50)),E:up()*-1)
rangerFilter(E)
R4 = rangerOffset(140,E:toWorld(vec(-FR,-RL,50)),E:up()*-1)

local Vec = ((HoverRan:position()+E:up()*40)-E:pos())
E:applyForce(Vec*8+(-E:vel()*0.4)*E:mass())

local Pitch = (E:toLocal((R1:position()-N1)):z()-E:toLocal((R4:position()-N4)):z())
local Roll = (E:toLocal((R2:position()-N2)):z()-E:toLocal((R3:position()-N3)):z())


local A1 = -Pitch-Roll
local A2 = Pitch-Roll
local Ang = ang(A1,E:toLocal(E:angles()):yaw(),A2)
E:applyAngForce(((Ang)*E:mass())+(-E:angVel()*E:mass()*0.6))
}


c(1,E:toWorld(vec(0,0,0)),vec(0.4),ang(),vec(255,0,0),"hqsphere",255)
c(2,E:toWorld(vec(0,0,0)),vec(0.4),ang(),vec(255,0,0),"hqsphere",255)
c(3,E:toWorld(vec(0,0,0)),vec(0.4),ang(),vec(255,0,0),"hqsphere",255)
c(4,E:toWorld(vec(0,0,0)),vec(0.4),ang(),vec(255,0,0),"hqsphere",255)
c(5,E:toWorld(vec(0,0,0)),vec(0.4),ang(),vec(0,255,0),"hqsphere",255)
c(6,E:toWorld(vec(0,0,0)),vec(0.4),ang(),vec(0,0,0),"hqsphere",255)
c(7,E:toWorld(vec(0,0,0)),vec(0.4),ang(),vec(0,0,0),"hqsphere",255)
c(8,E:toWorld(vec(0,0,0)),vec(0.4),ang(),vec(0,0,0),"hqsphere",255)
c(9,E:toWorld(vec(0,0,0)),vec(0.4),ang(),vec(0,0,0),"hqsphere",255)
}
holoPos(1,N1)
holoPos(2,N2)
holoPos(3,N3)
holoPos(4,N4)


applyStuff()
holoPos(5,HoverRan:position())
holoPos(6,R1:position())
holoPos(7,R2:position())
holoPos(8,R3:position())
holoPos(9,R4:position())
@name WallClimb-System-V1
@inputs 
@outputs 
@persist [N1 N2 N3 N4]:vector
@trigger 
@model models/hunter/blocks/cube05x05x05.mdl
interval(100)

E=entity()
N1=E:toWorld(vec(40,40,-40))
N2=E:toWorld(vec(40,-40,-40))
N3=E:toWorld(vec(-40,40,-40))
N4=E:toWorld(vec(-40,-40,-40))
if(first()){
    E:propGravity(0)
function number c(I,V:vector,S:vector,A:angle,Col:vector,Model:string,Alpha:number){ holoCreate(I,V,S,A,Col,Model) holoAlpha(I,Alpha) }
function number c(I,V:vector,S:vector,A:angle,Col:vector,Model:string,Parent:entity,Alpha:number){ holoCreate(I,V,S,A,Col,Model) holoAlpha(I,Alpha) holoParent(I,Parent) }
function number c(I,V:vector,S:vector,A:angle,Col:vector,Model:string,Parent:number,Alpha:number){ holoCreate(I,V,S,A,Col,Model) holoAlpha(I,Alpha) holoParent(I,Parent) }



function applyStuff(){
rangerFilter(E)
HoverRan = rangerOffset(1000,E:toWorld(vec(0,0,0)),E:up()*-1)
FR = 20
RL = 20
rangerFilter(E)
R1 = rangerOffset(140,E:toWorld(vec(FR,RL,50)),E:up()*-1)
rangerFilter(E)
R2 = rangerOffset(140,E:toWorld(vec(FR,-RL,50)),E:up()*-1)
rangerFilter(E)
R3 = rangerOffset(140,E:toWorld(vec(-FR,RL,50)),E:up()*-1)
rangerFilter(E)
R4 = rangerOffset(140,E:toWorld(vec(-FR,-RL,50)),E:up()*-1)

local Vec = ((HoverRan:position()+E:up()*40)-E:pos())
E:applyForce(Vec*8+(-E:vel()*0.4)*E:mass())

local Pitch = (E:toLocal((R1:position()-N1)):z()-E:toLocal((R4:position()-N4)):z())
local Roll = (E:toLocal((R2:position()-N2)):z()-E:toLocal((R3:position()-N3)):z())


local A1 = -Pitch-Roll
local A2 = Pitch-Roll
local Ang = ang(A1,E:toLocal(E:angles()):yaw(),A2)
E:applyAngForce(((Ang)*E:mass())+(-E:angVel()*E:mass()*0.6))
}


c(1,E:toWorld(vec(0,0,0)),vec(0.4),ang(),vec(255,0,0),"hqsphere",255)
c(2,E:toWorld(vec(0,0,0)),vec(0.4),ang(),vec(255,0,0),"hqsphere",255)
c(3,E:toWorld(vec(0,0,0)),vec(0.4),ang(),vec(255,0,0),"hqsphere",255)
c(4,E:toWorld(vec(0,0,0)),vec(0.4),ang(),vec(255,0,0),"hqsphere",255)
c(5,E:toWorld(vec(0,0,0)),vec(0.4),ang(),vec(0,255,0),"hqsphere",255)
c(6,E:toWorld(vec(0,0,0)),vec(0.4),ang(),vec(0,0,0),"hqsphere",255)
c(7,E:toWorld(vec(0,0,0)),vec(0.4),ang(),vec(0,0,0),"hqsphere",255)
c(8,E:toWorld(vec(0,0,0)),vec(0.4),ang(),vec(0,0,0),"hqsphere",255)
c(9,E:toWorld(vec(0,0,0)),vec(0.4),ang(),vec(0,0,0),"hqsphere",255)
}
holoPos(1,N1)
holoPos(2,N2)
holoPos(3,N3)
holoPos(4,N4)


applyStuff()
holoPos(5,HoverRan:position())
holoPos(6,R1:position())
holoPos(7,R2:position())
holoPos(8,R3:position())
holoPos(9,R4:position())
