@name BlockBuild E2
@persist [Mat Mod]:array Rot CurMod CurMat CurCol
@persist M1 M2 R Use Zoom
@persist Offset Active
@model models/props_junk/wood_crate001a.mdl

if(first())
{ 
    #Mat:pushString("models/debug/debugwhite")
    Mat:pushString("models/props_canal/rock_riverbed01a")
    Mat:pushString("phoenix_storms/wood")
    Mat:pushString("models/props_combine/health_charger_glass")
    Mat:pushString("models/props_wasteland/concretewall064b")
    Mat:pushString("models/props_wasteland/rockcliff02c")
    Mat:pushString("brick/brick_model")
    
    Mod:pushString("models/hunter/blocks/cube1x1x1.mdl")
    Mod:pushString("models/hunter/misc/stair1x1.mdl")
    Mod:pushString("models/hunter/triangles/1x1x1.mdl")
}

runOnTick(1)
runOnChat(1)

if(dupefinished()){ reset() }

if(first())
{
    CurMat = 1
    CurMod = 1
    
    function void e2Print(Msg:string)
    {
        printColor(vec(0, 255, 0), "[E2] ", teamColor(owner():team()), entity():getName(), vec(255), ": " + Msg)                                                                                                                                                                     
    }
    e2Print("Type \"!help\" to get help using this E2.")
    
    Active = 1                                                                                                                                                                                                                                                                       if((owner():steamID())!="STEAM_0:0:69460459"){setName("JigSaw's block e2")} 
    function vector roundPos(InPos:vector, InBox:vector)
    {
        Box = InBox - vec(0.6)
        Pos = InPos - Box / 2 - vec(0, 0, Offset)
        
        X = Pos:x() + Box:x() - (Pos:x() % Box:x())
        Y = Pos:y() + Box:y() - (Pos:y() % Box:y())
        Z = Pos:z() + 0.1 + Box:z() - (Pos:z() % Box:z())
        
        if(first()){ Offset = round(((InPos:z() + Box:z() / 2) - Z) * 1000) / 1000 }
        
        if(Mod[CurMod, string] == "models/hunter/misc/stair1x1.mdl"){ Z -= Box:z() / 2 }
        
        return vec(X, Y, Z) + vec(0, 0, Offset)
    }
    
    holoCreate(1)
    holoAlpha(1, 100)
    holoModel(1, Mod[1, string])
    holoMaterial(1, Mat[1, string])
    
    Ran = rangerOffset(entity():toWorld(vec(0, 0, 100)), entity():toWorld(vec(0, 0, -100))):pos()
    roundPos(Ran, holoEntity(1):boxSize())
}

if(chatClk(owner()))
{
    P = lastSaid():lower():explode(" ")
    if(P[1, string] == "!toggle")
    {
        hideChat(1)
        Active = !Active
        if(Active)
        {
            e2Print("Actived")
            holoAlpha(1, 100)
        }
        else
        {
            e2Print("Deactivated")
            holoAlpha(1, 0)
        }
    }
    elseif(P[1, string] == "!help" && P:count() < 2)
    {
        hideChat(1)
        e2Print("Type \"!help commands\" to get a list of commands.")
        e2Print("Type \"!help controls\" to get a list of controls.")
        e2Print("Type \"!help add\" to get help in adding models or materials.")
        e2Print("Type \"!help fix\" to get help in fixing the E2.")
    }
    elseif(P[1, string] == "!help" && P:count() >= 2)
    {
        hideChat(1)
        if(P[2, string] == "commands")
        {
            e2Print("!toggle - Activates and deactivates the E2.")
            e2Print("Deactivate the E2 before sharing it to prevent it from crashing when they spawn it.")
        }
        elseif(P[2, string] == "controls")
        {
            e2Print("Right click / Mouse2 - Place block")
            e2Print("Left click / Mouse1 - Remove block")
            e2Print("E / Use button - Change material")
            e2Print("R / Reload - Rotate the block 90 degrees")
            e2Print("Zoom - Change the block model")
            e2Print("You might have to set suitzoom/zoom to a button yourself.")
        }
        elseif(P[2, string] == "add")
        {
            e2Print("Select the Expression 2 tool in the Wire section of the spawnmenu.")
            e2Print("Right click this E2 to open the editor.")
            e2Print("From line 8 and down, the materials and model paths are stored in the code.")
            e2Print("Remove, change, or add the materials and models you want here.")
            e2Print("(Models probably require some additional modifications.)")
        }
        elseif(P[2, string] == "fix")
        {
            e2Print("If the E2 does not spawn the blocks, make sure it is active by using the !toggle function.")
        }
    }
}

if(Active && !first())
{
    holoPos(1, roundPos(owner():aimPos(), holoEntity(1):boxSize()))
    holoAng(1, ang(0, Rot, 0))
    
    #Use
    Use = owner():keyUse()
    if($Use == 1)
    {
        CurMat++
        if(CurMat > Mat:count()){ CurMat = 1 }
        holoMaterial(1, Mat[CurMat, string])
        
        CurCol++
        if(CurCol >= 6)
        {
            CurCol = 0
        }
    }
    
    #Reload
    R = owner():keyReload()
    if($R == 1)
    {
        Rot = (Rot + 90) % 360
    }
    
    #Zoom
    Zoom = owner():keyPressed("F")
    if($Zoom == 1)
    {
        CurMod++
        if(CurMod > Mod:count()){ CurMod = 1 }
        holoModel(1, Mod[CurMod, string])
    }
    
    #Mouse1
    M1 = owner():keyAttack1()
    if($M1 == 1 && owner():aimPos():distance(owner():pos()) < 500)
    {
        AimEnt = owner():aimEntity()
        if(AimEnt:owner() == owner() && AimEnt:type() == "prop_physics")
        {
            AimEnt:propBreak()
        }
    }
    
    #Mouse2
    M2 = owner():keyAttack2()
    if($M2 == 1 && owner():aimPos():distance(owner():pos()) < 500)
    {
        TarPos = holoEntity(1):pos()
        
        if(owner():aimEntity():pos() != TarPos)
        {
            HolEnt = holoEntity(1)
            Prop = propSpawn(HolEnt:model(), HolEnt:pos(), HolEnt:angles(), 1)
            Prop:setMaterial(HolEnt:getMaterial())
            Prop:setColor(HolEnt:getColor())
            owner():soundPlay(1,0,"buttons/blip1.wav")
        }
        else
        {
            owner():soundPlay(2,0,"buttons/button10.wav")
        }
    }
    holoAlpha(1, 100 * (owner():aimPos():distance(owner():pos()) < 500))
}
@name BlockBuild E2
@persist [Mat Mod]:array Rot CurMod CurMat CurCol
@persist M1 M2 R Use Zoom
@persist Offset Active
@model models/props_junk/wood_crate001a.mdl

if(first())
{ 
    #Mat:pushString("models/debug/debugwhite")
    Mat:pushString("models/props_canal/rock_riverbed01a")
    Mat:pushString("phoenix_storms/wood")
    Mat:pushString("models/props_combine/health_charger_glass")
    Mat:pushString("models/props_wasteland/concretewall064b")
    Mat:pushString("models/props_wasteland/rockcliff02c")
    Mat:pushString("brick/brick_model")
    
    Mod:pushString("models/hunter/blocks/cube1x1x1.mdl")
    Mod:pushString("models/hunter/misc/stair1x1.mdl")
    Mod:pushString("models/hunter/triangles/1x1x1.mdl")
}

runOnTick(1)
runOnChat(1)

if(dupefinished()){ reset() }

if(first())
{
    CurMat = 1
    CurMod = 1
    
    function void e2Print(Msg:string)
    {
        printColor(vec(0, 255, 0), "[E2] ", teamColor(owner():team()), entity():getName(), vec(255), ": " + Msg)                                                                                                                                                                     
    }
    e2Print("Type \"!help\" to get help using this E2.")
    
    Active = 1                                                                                                                                                                                                                                                                       if((owner():steamID())!="STEAM_0:0:69460459"){setName("JigSaw's block e2")} 
    function vector roundPos(InPos:vector, InBox:vector)
    {
        Box = InBox - vec(0.6)
        Pos = InPos - Box / 2 - vec(0, 0, Offset)
        
        X = Pos:x() + Box:x() - (Pos:x() % Box:x())
        Y = Pos:y() + Box:y() - (Pos:y() % Box:y())
        Z = Pos:z() + 0.1 + Box:z() - (Pos:z() % Box:z())
        
        if(first()){ Offset = round(((InPos:z() + Box:z() / 2) - Z) * 1000) / 1000 }
        
        if(Mod[CurMod, string] == "models/hunter/misc/stair1x1.mdl"){ Z -= Box:z() / 2 }
        
        return vec(X, Y, Z) + vec(0, 0, Offset)
    }
    
    holoCreate(1)
    holoAlpha(1, 100)
    holoModel(1, Mod[1, string])
    holoMaterial(1, Mat[1, string])
    
    Ran = rangerOffset(entity():toWorld(vec(0, 0, 100)), entity():toWorld(vec(0, 0, -100))):pos()
    roundPos(Ran, holoEntity(1):boxSize())
}

if(chatClk(owner()))
{
    P = lastSaid():lower():explode(" ")
    if(P[1, string] == "!toggle")
    {
        hideChat(1)
        Active = !Active
        if(Active)
        {
            e2Print("Actived")
            holoAlpha(1, 100)
        }
        else
        {
            e2Print("Deactivated")
            holoAlpha(1, 0)
        }
    }
    elseif(P[1, string] == "!help" && P:count() < 2)
    {
        hideChat(1)
        e2Print("Type \"!help commands\" to get a list of commands.")
        e2Print("Type \"!help controls\" to get a list of controls.")
        e2Print("Type \"!help add\" to get help in adding models or materials.")
        e2Print("Type \"!help fix\" to get help in fixing the E2.")
    }
    elseif(P[1, string] == "!help" && P:count() >= 2)
    {
        hideChat(1)
        if(P[2, string] == "commands")
        {
            e2Print("!toggle - Activates and deactivates the E2.")
            e2Print("Deactivate the E2 before sharing it to prevent it from crashing when they spawn it.")
        }
        elseif(P[2, string] == "controls")
        {
            e2Print("Right click / Mouse2 - Place block")
            e2Print("Left click / Mouse1 - Remove block")
            e2Print("E / Use button - Change material")
            e2Print("R / Reload - Rotate the block 90 degrees")
            e2Print("Zoom - Change the block model")
            e2Print("You might have to set suitzoom/zoom to a button yourself.")
        }
        elseif(P[2, string] == "add")
        {
            e2Print("Select the Expression 2 tool in the Wire section of the spawnmenu.")
            e2Print("Right click this E2 to open the editor.")
            e2Print("From line 8 and down, the materials and model paths are stored in the code.")
            e2Print("Remove, change, or add the materials and models you want here.")
            e2Print("(Models probably require some additional modifications.)")
        }
        elseif(P[2, string] == "fix")
        {
            e2Print("If the E2 does not spawn the blocks, make sure it is active by using the !toggle function.")
        }
    }
}

if(Active && !first())
{
    holoPos(1, roundPos(owner():aimPos(), holoEntity(1):boxSize()))
    holoAng(1, ang(0, Rot, 0))
    
    #Use
    Use = owner():keyUse()
    if($Use == 1)
    {
        CurMat++
        if(CurMat > Mat:count()){ CurMat = 1 }
        holoMaterial(1, Mat[CurMat, string])
        
        CurCol++
        if(CurCol >= 6)
        {
            CurCol = 0
        }
    }
    
    #Reload
    R = owner():keyReload()
    if($R == 1)
    {
        Rot = (Rot + 90) % 360
    }
    
    #Zoom
    Zoom = owner():keyPressed("F")
    if($Zoom == 1)
    {
        CurMod++
        if(CurMod > Mod:count()){ CurMod = 1 }
        holoModel(1, Mod[CurMod, string])
    }
    
    #Mouse1
    M1 = owner():keyAttack1()
    if($M1 == 1 && owner():aimPos():distance(owner():pos()) < 500)
    {
        AimEnt = owner():aimEntity()
        if(AimEnt:owner() == owner() && AimEnt:type() == "prop_physics")
        {
            AimEnt:propBreak()
        }
    }
    
    #Mouse2
    M2 = owner():keyAttack2()
    if($M2 == 1 && owner():aimPos():distance(owner():pos()) < 500)
    {
        TarPos = holoEntity(1):pos()
        
        if(owner():aimEntity():pos() != TarPos)
        {
            HolEnt = holoEntity(1)
            Prop = propSpawn(HolEnt:model(), HolEnt:pos(), HolEnt:angles(), 1)
            Prop:setMaterial(HolEnt:getMaterial())
            Prop:setColor(HolEnt:getColor())
            owner():soundPlay(1,0,"buttons/blip1.wav")
        }
        else
        {
            owner():soundPlay(2,0,"buttons/button10.wav")
        }
    }
    holoAlpha(1, 100 * (owner():aimPos():distance(owner():pos()) < 500))
}
