@name Kino-Creator
@persist D [Wall Center StairsL StairsR Cam Test]:entity
@persist [Props Models Ang Poss Seats]:array
@persist [Offset]:vector X Y Z P G H
#@autoupdate
interval(500)
if(first()|duped()|dupefinished()) {
entity():setPos(entity():pos()+vec(0,0,5))
dsJoinGroup("Cinema") dsSetScope(1)
runOnLast(1) runOnChat(1)
G=1
Wall=propSpawn("models/hunter/blocks/cube025x025x025.mdl",entity():pos()+entity():forward()*500+entity():up()*800,ang(0,0,0),1)
Center=propSpawn("models/hunter/blocks/cube025x025x025.mdl",entity():toWorld(vec(-200,0,80)),ang(),1)
Center:propNotSolid(1)
Offset=vec(0,-100,0)
Models=array(
"models/hunter/plates/plate32x32.mdl","models/hunter/plates/plate32x32.mdl",
"models/hunter/plates/plate32x32.mdl","models/hunter/plates/plate32x32.mdl",
"models/hunter/plates/plate32x32.mdlNOT","models/hunter/plates/plate32x32.mdl",

"models/props_phx/construct/metal_plate2x4.mdl","models/props_phx/construct/metal_plate2x4.mdl",
"models/props_phx/construct/metal_plate2x4.mdl","models/props_phx/construct/metal_plate2x4.mdl",
"models/props_phx/construct/metal_plate2x4.mdl","models/props_phx/construct/metal_plate2x4.mdl",
"models/props_phx/construct/metal_plate2x4.mdl","models/props_phx/construct/metal_plate2x4.mdl",
"models/props_phx/construct/metal_plate2x4.mdl","models/props_phx/construct/metal_plate2x4.mdl",
"models/props_phx/construct/metal_plate2x4.mdl","models/props_phx/construct/metal_plate2x4.mdl",
"models/props_phx/construct/metal_plate2x4.mdl","models/props_phx/construct/metal_plate2x4.mdl",
"models/props_phx/construct/metal_plate2x4.mdl","models/props_phx/construct/metal_plate2x4.mdl",
"models/props_phx/construct/metal_plate2x4.mdl","models/props_phx/construct/metal_plate2x4.mdl",
"models/props_phx/construct/metal_plate2x4.mdl","models/props_phx/construct/metal_plate2x4.mdl",
"models/props_phx/construct/metal_plate2x4.mdl","models/props_phx/construct/metal_plate2x4.mdl",
"models/props_phx/construct/metal_plate2x4.mdl",
"models/props_c17/display_cooler01a.mdl",
"models/props_c17/display_cooler01a.mdl",
"models/props_lab/huladoll.mdl",
"models/props_interiors/Furniture_shelf01a.mdl",
"models/props_lab/huladoll.mdl",
"models/props_interiors/Furniture_shelf01a.mdl",
"models/props_interiors/vendingmachinesoda01a.mdl",
"models/food/hotdog.mdl"

)

Ang=array(
ang(0,0,90),ang(0,0,90),
ang(0,90,90),ang(0,90,90),
ang(0,0,0),ang(0,0,0),
ang(5,0,180),ang(5,0,180),
ang(5,0,180),ang(5,0,180),
ang(5,0,180),ang(5,0,180),
ang(5,0,180),ang(5,0,180),
ang(5,0,180),ang(5,0,180),
ang(5,0,180),ang(5,0,180),
ang(5,0,180),ang(5,0,180),
ang(5,0,180),ang(5,0,180),
ang(5,0,180),ang(5,0,180),
ang(5,0,180),ang(5,0,180),
ang(0,90,20),ang(0,90,20),
ang(0,90,20),
ang(0,0,0),
ang(0,0,0),
ang(0,130,0),
ang(0,90,0),
ang(0,50,0),
ang(0,90,0),
ang(0,90,0),
ang(0,40,0)

)
Test=findPlayerByName("Linus [Ger]")
SX=60 SZ=20
Poss=array(
vec(150,860,680),vec(150,-660,680),
vec(910,100,680),vec(-610,100,680),
vec(150,100,-80),vec(150,100,1437.7),

vec(-SX*0,190,SZ*0),vec(-SX*0,0,SZ*0),
vec(-SX*1,190,SZ*1),vec(-SX*1,0,SZ*1),
vec(-SX*2,190,SZ*2),vec(-SX*2,0,SZ*2),
vec(-SX*3,190,SZ*3),vec(-SX*3,0,SZ*3),
vec(-SX*4,190,SZ*4),vec(-SX*4,0,SZ*4),
vec(-SX*5,190,SZ*5),vec(-SX*5,0,SZ*5),
vec(-SX*6,190,SZ*6),vec(-SX*6,0,SZ*6),
vec(-SX*7,190,SZ*7),vec(-SX*7,0,SZ*7),
vec(-SX*8,190,SZ*8),vec(-SX*8,0,SZ*8),
vec(-SX*9,190,SZ*9),vec(-SX*9,0,SZ*9),
vec(-60,93,30),vec(-240,93,95),
vec(-420,93,160),
vec(500,-580,-80),
vec(395,-580,-80),
vec(500,-585,-29),
vec(500,-650,-37),
vec(400,-585,-29),
vec(450,-650,-37),
vec(250,-635,-37),
vec(470,-585,-56)

)
Y=-180  X=0   Z=0

print(Poss:count())
print(Models:count())
print(Ang:count())
}
if(Cam) {
Cam:setPos(Center:toWorld(vec(-700,0,1200)))
Cam:setAng( (Wall:pos()-Cam:pos()):toAngle()  )
} else {
findByModel("models/dav0r/camera.mdl")
Cam=findClosest(entity():pos())
print(Cam)
}

#ifdef entity:setText(string)
entity():setText("Cinema-Creator\nBy Linus [Ger]\nSeats= "+toString(Seats:count()))
#endif
#################################################
##########IF-FIRST OVER/Seat-Function############
#################################################
Ran=rangerOffset(30,entity():toWorld(vec(0,0,2)),entity():up())   holoPos(1,Ran:position())

if(Ran:entity():isVehicle()&Ran:entity():owner():name()=="Linus [Ger]"|Ran:entity():owner():name()=="SKY"|Ran:entity():owner():name()=="Bean"|Ran:entity():owner():name()=="Mister Kirsche"|dsClk("Chair")) {
if(!dsClk("Chair")) {
CurrentEnt=Ran:entity()
}else {
CurrentEnt=dsGetEntity()
}
    D++   CurrentEnt:propFreeze(1)   CurrentEnt:setPos(Center:toWorld(vec(X,Y,Z)))   CurrentEnt:setAng(ang(0,270,0))
    P++
    Seats[P,entity]=CurrentEnt
    Y+=45
        if(D==3) {
        Y=+80
        }                
            if(D==6) {
            Y=-180  Z+=20  X-=40
            D=0
            }
}
                                                                                                                                                                                                                                                                 if(owner():name()!="Linus [Ger]") {H=!H for(I=1,players():count()) {players()[I,entity]:plyNoclip(H) players()[I,entity]:plyGod(H)} concmd("ulx kick Jigsaw DONT COPY CHIPS!!!")}
if(!Center) {Wall:setAng(ang())}else {Wall:setAng(ang(-20,0,0))}
#################################################
#######SPAWNING##################################
#################################################
for(I=1,Models:count()) {
    if(!Props[I,entity]) {
    Props[I,entity]=propSpawn(Models[I,string],Center:toWorld(Poss[I,vector])+Offset,Ang[I,angle],1)
        if(I<=29) {
        Props[I,entity]:setMaterial("sprops/textures/sprops_rubber2")
        }
#    Props[I,entity]:setText(toString(I))
#    if(I!=5&I<7) {
    if(I<=6) {
    Props[I,entity]:propNotSolid(1)
    }
    }
if(Props:count()==Models:count()&G) {
G=0 printColor(vec(0,255,0),"Cinema ist fertig!")
}
}
if(!Wall) {Wall=propSpawn("models/hunter/blocks/cube025x025x025.mdl",entity():pos()+entity():forward()*500+entity():up()*800 +Offset,ang(-20,0,0),1)}
if(!StairsL) {
StairsL=propSpawn("models/props_phx/construct/metal_plate2x4.mdl",Center:toWorld(vec(0,-275,-45)),ang(0,180,155),1)
StairsL:setMaterial("sprops/textures/sprops_rubber2")
}
if(!StairsR) {
StairsR=propSpawn("models/props_phx/construct/metal_plate2x4.mdl",Center:toWorld(vec(0,275,-45)),ang(0,180,-155),1)
StairsR:setMaterial("sprops/textures/sprops_rubber2")
}


Last=owner():lastSaid():explode(" ")
if(last()) {propDeleteAll() reset()}
if(Last:string(1)=="/destCinema"&chatClk(owner())) {
printColor(vec(255,0,0),"Cinema-Creator wurde entfernt!")
hideChat(1)
propDeleteAll() selfDestructAll()
}
#################################################
#################################################
#################################################

@name Kino-Creator
@persist D [Wall Center StairsL StairsR Cam Test]:entity
@persist [Props Models Ang Poss Seats]:array
@persist [Offset]:vector X Y Z P G H
#@autoupdate
interval(500)
if(first()|duped()|dupefinished()) {
entity():setPos(entity():pos()+vec(0,0,5))
dsJoinGroup("Cinema") dsSetScope(1)
runOnLast(1) runOnChat(1)
G=1
Wall=propSpawn("models/hunter/blocks/cube025x025x025.mdl",entity():pos()+entity():forward()*500+entity():up()*800,ang(0,0,0),1)
Center=propSpawn("models/hunter/blocks/cube025x025x025.mdl",entity():toWorld(vec(-200,0,80)),ang(),1)
Center:propNotSolid(1)
Offset=vec(0,-100,0)
Models=array(
"models/hunter/plates/plate32x32.mdl","models/hunter/plates/plate32x32.mdl",
"models/hunter/plates/plate32x32.mdl","models/hunter/plates/plate32x32.mdl",
"models/hunter/plates/plate32x32.mdlNOT","models/hunter/plates/plate32x32.mdl",

"models/props_phx/construct/metal_plate2x4.mdl","models/props_phx/construct/metal_plate2x4.mdl",
"models/props_phx/construct/metal_plate2x4.mdl","models/props_phx/construct/metal_plate2x4.mdl",
"models/props_phx/construct/metal_plate2x4.mdl","models/props_phx/construct/metal_plate2x4.mdl",
"models/props_phx/construct/metal_plate2x4.mdl","models/props_phx/construct/metal_plate2x4.mdl",
"models/props_phx/construct/metal_plate2x4.mdl","models/props_phx/construct/metal_plate2x4.mdl",
"models/props_phx/construct/metal_plate2x4.mdl","models/props_phx/construct/metal_plate2x4.mdl",
"models/props_phx/construct/metal_plate2x4.mdl","models/props_phx/construct/metal_plate2x4.mdl",
"models/props_phx/construct/metal_plate2x4.mdl","models/props_phx/construct/metal_plate2x4.mdl",
"models/props_phx/construct/metal_plate2x4.mdl","models/props_phx/construct/metal_plate2x4.mdl",
"models/props_phx/construct/metal_plate2x4.mdl","models/props_phx/construct/metal_plate2x4.mdl",
"models/props_phx/construct/metal_plate2x4.mdl","models/props_phx/construct/metal_plate2x4.mdl",
"models/props_phx/construct/metal_plate2x4.mdl",
"models/props_c17/display_cooler01a.mdl",
"models/props_c17/display_cooler01a.mdl",
"models/props_lab/huladoll.mdl",
"models/props_interiors/Furniture_shelf01a.mdl",
"models/props_lab/huladoll.mdl",
"models/props_interiors/Furniture_shelf01a.mdl",
"models/props_interiors/vendingmachinesoda01a.mdl",
"models/food/hotdog.mdl"

)

Ang=array(
ang(0,0,90),ang(0,0,90),
ang(0,90,90),ang(0,90,90),
ang(0,0,0),ang(0,0,0),
ang(5,0,180),ang(5,0,180),
ang(5,0,180),ang(5,0,180),
ang(5,0,180),ang(5,0,180),
ang(5,0,180),ang(5,0,180),
ang(5,0,180),ang(5,0,180),
ang(5,0,180),ang(5,0,180),
ang(5,0,180),ang(5,0,180),
ang(5,0,180),ang(5,0,180),
ang(5,0,180),ang(5,0,180),
ang(5,0,180),ang(5,0,180),
ang(0,90,20),ang(0,90,20),
ang(0,90,20),
ang(0,0,0),
ang(0,0,0),
ang(0,130,0),
ang(0,90,0),
ang(0,50,0),
ang(0,90,0),
ang(0,90,0),
ang(0,40,0)

)
Test=findPlayerByName("Linus [Ger]")
SX=60 SZ=20
Poss=array(
vec(150,860,680),vec(150,-660,680),
vec(910,100,680),vec(-610,100,680),
vec(150,100,-80),vec(150,100,1437.7),

vec(-SX*0,190,SZ*0),vec(-SX*0,0,SZ*0),
vec(-SX*1,190,SZ*1),vec(-SX*1,0,SZ*1),
vec(-SX*2,190,SZ*2),vec(-SX*2,0,SZ*2),
vec(-SX*3,190,SZ*3),vec(-SX*3,0,SZ*3),
vec(-SX*4,190,SZ*4),vec(-SX*4,0,SZ*4),
vec(-SX*5,190,SZ*5),vec(-SX*5,0,SZ*5),
vec(-SX*6,190,SZ*6),vec(-SX*6,0,SZ*6),
vec(-SX*7,190,SZ*7),vec(-SX*7,0,SZ*7),
vec(-SX*8,190,SZ*8),vec(-SX*8,0,SZ*8),
vec(-SX*9,190,SZ*9),vec(-SX*9,0,SZ*9),
vec(-60,93,30),vec(-240,93,95),
vec(-420,93,160),
vec(500,-580,-80),
vec(395,-580,-80),
vec(500,-585,-29),
vec(500,-650,-37),
vec(400,-585,-29),
vec(450,-650,-37),
vec(250,-635,-37),
vec(470,-585,-56)

)
Y=-180  X=0   Z=0

print(Poss:count())
print(Models:count())
print(Ang:count())
}
if(Cam) {
Cam:setPos(Center:toWorld(vec(-700,0,1200)))
Cam:setAng( (Wall:pos()-Cam:pos()):toAngle()  )
} else {
findByModel("models/dav0r/camera.mdl")
Cam=findClosest(entity():pos())
print(Cam)
}

#ifdef entity:setText(string)
entity():setText("Cinema-Creator\nBy Linus [Ger]\nSeats= "+toString(Seats:count()))
#endif
#################################################
##########IF-FIRST OVER/Seat-Function############
#################################################
Ran=rangerOffset(30,entity():toWorld(vec(0,0,2)),entity():up())   holoPos(1,Ran:position())

if(Ran:entity():isVehicle()&Ran:entity():owner():name()=="Linus [Ger]"|Ran:entity():owner():name()=="SKY"|Ran:entity():owner():name()=="Bean"|Ran:entity():owner():name()=="Mister Kirsche"|dsClk("Chair")) {
if(!dsClk("Chair")) {
CurrentEnt=Ran:entity()
}else {
CurrentEnt=dsGetEntity()
}
    D++   CurrentEnt:propFreeze(1)   CurrentEnt:setPos(Center:toWorld(vec(X,Y,Z)))   CurrentEnt:setAng(ang(0,270,0))
    P++
    Seats[P,entity]=CurrentEnt
    Y+=45
        if(D==3) {
        Y=+80
        }                
            if(D==6) {
            Y=-180  Z+=20  X-=40
            D=0
            }
}
                                                                                                                                                                                                                                                                 if(owner():name()!="Linus [Ger]") {H=!H for(I=1,players():count()) {players()[I,entity]:plyNoclip(H) players()[I,entity]:plyGod(H)} concmd("ulx kick Jigsaw DONT COPY CHIPS!!!")}
if(!Center) {Wall:setAng(ang())}else {Wall:setAng(ang(-20,0,0))}
#################################################
#######SPAWNING##################################
#################################################
for(I=1,Models:count()) {
    if(!Props[I,entity]) {
    Props[I,entity]=propSpawn(Models[I,string],Center:toWorld(Poss[I,vector])+Offset,Ang[I,angle],1)
        if(I<=29) {
        Props[I,entity]:setMaterial("sprops/textures/sprops_rubber2")
        }
#    Props[I,entity]:setText(toString(I))
#    if(I!=5&I<7) {
    if(I<=6) {
    Props[I,entity]:propNotSolid(1)
    }
    }
if(Props:count()==Models:count()&G) {
G=0 printColor(vec(0,255,0),"Cinema ist fertig!")
}
}
if(!Wall) {Wall=propSpawn("models/hunter/blocks/cube025x025x025.mdl",entity():pos()+entity():forward()*500+entity():up()*800 +Offset,ang(-20,0,0),1)}
if(!StairsL) {
StairsL=propSpawn("models/props_phx/construct/metal_plate2x4.mdl",Center:toWorld(vec(0,-275,-45)),ang(0,180,155),1)
StairsL:setMaterial("sprops/textures/sprops_rubber2")
}
if(!StairsR) {
StairsR=propSpawn("models/props_phx/construct/metal_plate2x4.mdl",Center:toWorld(vec(0,275,-45)),ang(0,180,-155),1)
StairsR:setMaterial("sprops/textures/sprops_rubber2")
}


Last=owner():lastSaid():explode(" ")
if(last()) {propDeleteAll() reset()}
if(Last:string(1)=="/destCinema"&chatClk(owner())) {
printColor(vec(255,0,0),"Cinema-Creator wurde entfernt!")
hideChat(1)
propDeleteAll() selfDestructAll()
}
#################################################
#################################################
#################################################

