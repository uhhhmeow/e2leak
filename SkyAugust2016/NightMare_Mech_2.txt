@name NightMare Mech 2
@inputs 
@outputs  
@persist [E  Fuss1 Fuss2 Seat]:entity [F1P F2P FR2 FR1 Ranger]:vector EntityA:number Stampf Jump Lock PW T Counter Ar:array BG
@persist Shield:entity
@trigger none
interval(100)
#runOnTick(1)
runOnChat(1)
runOnLast(1)
DistanceBetweenEntity=40
if(last()){E:propDelete() Fuss1:propDelete() Fuss2:propDelete()}
if(!E){E=propSpawn("models/hunter/blocks/cube075x075x075.mdl",entity():pos()+vec(0,0,150),1) E:setMass(50000)}
if(!Fuss1&E){Fuss1=propSpawn("models/hunter/blocks/cube025x025x025.mdl",E:pos()+vec(0,50,-130),0) Fuss1:setMass(50000) Fuss1:setAlpha(0) E:soundPlay(7,1,"clock/tick.wav")}
if(!Fuss2&E){Fuss2=propSpawn("models/hunter/blocks/cube025x025x025.mdl",E:pos()+vec(0,-50,-130),0) Fuss2:setMass(50000) Fuss2:setAlpha(0) E:soundPlay(8,1,"clock/tick.wav")}
if(!Seat&owner():aimEntity():isVehicle()&owner():aimEntity():owner()==owner()&owner():isAlive()){Seat=owner():aimEntity()}
Driver=Seat:driver()
 E:setAlpha(0)
    Key=owner():keyPressed("1")
    if(changed(Key)&Key){Lock+=1}
    if(changed(Lock>=2)&Lock>=2){Lock=0}
    if(changed(Lock==1)&Lock==1){print("Locked") entity():propNotSolid(1)}
        if(changed(Lock==0)&Lock==0){print("unLocked") entity():propNotSolid(0)}
rangerFilter(E)
Rango=rangerOffset(150,E:toWorld(vec(0,0,0)),vec(0,0,-1)):position()

if(Driver){
W=Driver:keyForward()
A=Driver:keyLeft()
S=Driver:keyBack()
D=Driver:keyRight()
Shift=Driver:keySprint()
Space =Driver:keyJump()
Mouse1=Driver:keyAttack1()
Mouse2=Driver:keyAttack2()
}
if(changed(Seat)&Seat){print("seat found !")}
if(first()){
    F1P=Fuss1:pos()
F2P=Fuss2:pos()
function entity c(Index:number,Posi:vector,Scale:vector,Angle:angle,Colo:vector,Model:string,Parent:entity,Alpha:number){
 holoCreate(Index) holoPos(Index,Posi) holoScale(Index,Scale) holoAng(Index,Angle) holoColor(Index,Colo) holoModel(Index,Model) 
holoParent(Index,Parent) holoAlpha(Index,Alpha)
return holoEntity(Index)  
}
function void apply(Foot1:entity,Foot2:entity,Enti:entity,DistHover:number,HoverPower:number){
local VectorHover=(Foot1:pos()+Foot2:pos())/2
local  Hover=((VectorHover+vec(0,0,DistHover))-(E:pos()-vec(0,0,25)))*HoverPower
local Vel=-E:vel()*0.5
Enti:applyForce((Hover+Vel)*E:mass())
} 
##########holos#######
###base legs###
c(1,E:toWorld(vec(0,40,0)),vec(1,1,1),E:toWorld(ang()),vec(255),"0",E,0)
c(2,E:toWorld(vec(0,-40,0)),vec(1,1,1),E:toWorld(ang()),vec(255),"0",E,0)
c(3,holoEntity(1):toWorld(vec(-40.5,0,-40.5)),vec(1,1,1),E:toWorld(ang()),vec(255),"0",holoEntity(1),0)
c(4,holoEntity(2):toWorld(vec(-40.5,0,-40.5)),vec(1,1,1),E:toWorld(ang()),vec(255),"0",holoEntity(2),0)
c(5,holoEntity(3):toWorld(vec(-75,0,0)),vec(1,1,1),E:toWorld(ang()),vec(255),"0",holoEntity(3),0)
c(6,holoEntity(4):toWorld(vec(-75,0,0)),vec(1,1,1),E:toWorld(ang()),vec(255),"0",holoEntity(4),0)
c(7,holoEntity(1):toWorld(vec(0,0,0)),vec(1,1,1),E:toWorld(ang()),vec(255),"0",holoEntity(1),0)
c(8,holoEntity(2):toWorld(vec(0,0,0)),vec(1,1,1),E:toWorld(ang()),vec(255),"0",holoEntity(2),0)
###############
####leg dekoration########
c(9,E:toWorld(vec(0,40,0)),vec(2,2,2),E:toWorld(ang(90,90,0)),vec(255,0,0),"cylinder",holoEntity(1),255) holoMaterial(9,"debug/debugdrawflat")
c(10,E:toWorld(vec(0,-40,0)),vec(2,2,2),E:toWorld(ang(90,90,0)),vec(255,0,0),"cylinder",holoEntity(2),255) holoMaterial(10,"debug/debugdrawflat")
c(11,holoEntity(1):toWorld(vec(-40.5,0,-40.5)),vec(2,2,2),E:toWorld(ang(90,90,0)),vec(255,0,0),"cylinder",holoEntity(1),255) holoMaterial(11,"debug/debugdrawflat")
c(12,holoEntity(2):toWorld(vec(-40.5,0,-40.5)),vec(2,2,2),E:toWorld(ang(90,90,0)),vec(255,0,0),"cylinder",holoEntity(2),255) holoMaterial(12,"debug/debugdrawflat")
c(13,holoEntity(3):toWorld(vec(-75,0,0)),vec(2.5,2.5,1),E:toWorld(ang(55,0,0)),vec(255,0,0),"hqcylinder",holoEntity(3),255) holoMaterial(13,"debug/debugdrawflat")
c(14,holoEntity(4):toWorld(vec(-75,0,0)),vec(2.5,2.5,1),E:toWorld(ang(55,0,0)),vec(255,0,0),"hqcylinder",holoEntity(4),255) holoMaterial(14,"debug/debugdrawflat")
c(15,holoEntity(7):toWorld(vec(0,0,0)),vec(3,3,2),E:toWorld(ang(90,90,0)),vec(0,0,0),"hqtube",holoEntity(7),255) 
c(16,holoEntity(8):toWorld(vec(0,0,0)),vec(3,3,2),E:toWorld(ang(90,90,0)),vec(0),"hqtube",holoEntity(8),255)
c(17,holoEntity(1):toWorld(vec(-40.5,0,-40.5)),vec(3,3,2),E:toWorld(ang(90,90,0)),vec(0),"hqtube",holoEntity(1),255)
c(18,holoEntity(2):toWorld(vec(-40.5,0,-40.5)),vec(3,3,2),E:toWorld(ang(90,90,0)),vec(0),"hqtube",holoEntity(2),255)
c(19,holoEntity(13):toWorld(vec(0,0,0)),vec(3.6,3.6,0.5),E:toWorld(ang(0,0,0)),vec(0,0,0),"hqcylinder",holoEntity(3),255)
c(20,holoEntity(14):toWorld(vec(0,0,0)),vec(3.6,3.6,0.5),E:toWorld(ang(0,0,0)),vec(0),"hqcylinder",holoEntity(4),255)
c(21,holoEntity(1):toWorld(vec(-20.25,0,-20.25)),vec(3.6,2.4,1.2),E:toWorld(ang(-45,0,90)),vec(0,0,0),"hqcylinder",holoEntity(1),255)
c(22,holoEntity(2):toWorld(vec(-20.25,0,-20.25)),vec(3.6,2.4,1.2),E:toWorld(ang(-45,0,90)),vec(0),"hqcylinder",holoEntity(2),255)
c(23,holoEntity(3):toWorld(vec(-37.5,0,0)),vec(4.5,2.5,1.2),E:toWorld(ang(-5,0,90)),vec(0),"hqcylinder",holoEntity(3),255)
c(24,holoEntity(4):toWorld(vec(-37.5,0,0)),vec(4.5,2.5,1.2),E:toWorld(ang(-5,0,90)),vec(0),"hqcylinder",holoEntity(4),255)
c(25,holoEntity(13):toWorld(vec(0,0,5)),vec(2,2,2),E:toWorld(ang(0,0,0)),vec(0,0,0),"hqsphere",holoEntity(3),255)
c(26,holoEntity(14):toWorld(vec(0,0,5)),vec(2,2,2),E:toWorld(ang(0,0,0)),vec(0),"hqsphere",holoEntity(4),255)
c(27,holoEntity(3):toWorld(vec(-37.5,0,0)),vec(4.2,2,2.5),E:toWorld(ang(-5,0,90)),vec(255,0,0),"hqsphere",holoEntity(23),255)  holoMaterial(27,"debug/debugdrawflat")
c(28,holoEntity(4):toWorld(vec(-37.5,0,0)),vec(4.2,2,2.5),E:toWorld(ang(-5,0,90)),vec(255,0,0),"hqsphere",holoEntity(24),255)  holoMaterial(28,"debug/debugdrawflat")
c(29,holoEntity(1):toWorld(vec(-20.25,0,-20.25)),vec(4.5,2,2),E:toWorld(ang(-45,0,90)),vec(255,0,0),"hqsphere",holoEntity(1),255)  holoMaterial(29,"debug/debugdrawflat")
c(30,holoEntity(2):toWorld(vec(-20.25,0,-20.25)),vec(4.5,2,2),E:toWorld(ang(-45,0,90)),vec(255,0,0),"hqsphere",holoEntity(2),255)  holoMaterial(30,"debug/debugdrawflat")
#debug/debugdrawflat
##############
####body usw#####
c(1000,E:toWorld(vec(0,0,0)),vec(1,1,1),E:toWorld(ang(90,0,90)),vec(0),"hqcylinder",E,255)
c(31,E:toWorld(vec(0,20,0)),vec(2,2,3),E:toWorld(ang(90,0,90)),vec(9,0,0),"hqcylinder",E,255)
c(32,E:toWorld(vec(0,-20,0)),vec(2,2,3),E:toWorld(ang(90,0,90)),vec(0),"hqcylinder",E,255)
c(33,E:toWorld(vec(0,0,0)),vec(3.3,3.3,3),E:toWorld(ang(90,90,0)),vec(0,0,0),"hqcylinder",E,255)
c(34,E:toWorld(vec(0,14.9,0)),vec(3.5,3.5,0.5),E:toWorld(ang(90,90,0)),vec(255,0,0),"hqcylinder",E,255)
c(35,E:toWorld(vec(0,-14.9,0)),vec(3.5,3.5,0.5),E:toWorld(ang(90,90,0)),vec(255,0,0),"hqcylinder",E,255)
 holoMaterial(34,"debug/debugdrawflat")
 holoMaterial(35,"debug/debugdrawflat")
#####neck###
c(36,E:toWorld(vec(0,0,30)),vec(4.5,2.5,1.5),E:toWorld(ang(90,90,0)),vec(0),"hqcylinder",holoEntity(1000),255)
c(37,E:toWorld(vec(0,0,30)),vec(4.6,2.6,0.3),E:toWorld(ang(90,90,0)),vec(255),"hqcylinder",holoEntity(36),255)
c(46,E:toWorld(vec(0,0,30)),vec(4.4,2.4,3.5),E:toWorld(ang(90,90,0)),vec(255,0,0),"hqsphere",holoEntity(37),255)
 holoMaterial(46,"debug/debugdrawflat")
 holoMaterial(37,"debug/debugdrawflat")
#################
c(38,E:toWorld(vec(0,0,60)),vec(4.6,4.6,2),E:toWorld(ang(180,90,0)),vec(0),"dome",holoEntity(36),255)
c(39,E:toWorld(vec(0,0,61)),vec(5,5,0.5),E:toWorld(ang(180,90,0)),vec(255,0,0),"dome",holoEntity(38),255)
 holoMaterial(39,"debug/debugdrawflat")
c(40,E:toWorld(vec(0,0,90)),vec(5,5,5),E:toWorld(ang(0,0,0)),vec(0),"hqtube",holoEntity(38),105)
c(41,E:toWorld(vec(0,0,120)),vec(5,5,1),E:toWorld(ang(0,0,0)),vec(0),"dome",holoEntity(38),255)
c(42,E:toWorld(vec(0,-30,90)),vec(0.5,5,0.5),E:toWorld(ang(0,90,90)),vec(0,0,0),"hqcylinder",holoEntity(38),255)
c(43,E:toWorld(vec(0,30,90)),vec(0.5,5,0.5),E:toWorld(ang(0,90,90)),vec(0,0,0),"hqcylinder",holoEntity(38),255)

c(44,E:toWorld(vec(0,-30,90)),vec(0.4,5,1),E:toWorld(ang(0,90,90)),vec(255,0,0),"hqsphere",holoEntity(38),255)
c(45,E:toWorld(vec(0,30,90)),vec(0.4,5,1),E:toWorld(ang(0,90,90)),vec(255,0,0),"hqsphere",holoEntity(38),255)
 holoMaterial(44,"debug/debugdrawflat")
 holoMaterial(45,"debug/debugdrawflat")
#################
############33333333
####awesome effect weapons#########
c(47,E:toWorld(vec(0,0,140)),vec(1,1,1),E:toWorld(ang(0,0,0)),vec(255,0,0),"hqsphere",holoEntity(38),255)
 holoMaterial(47,"debug/debugdrawflat")
c(48,holoEntity(47):toWorld(vec(60,0,0)),vec(3,3,0.001),holoEntity(47):toWorld(ang(70,0,0)),vec(255,0,0),"",holoEntity(47),155)
c(49,holoEntity(47):toWorld(vec(-60,0,0)),vec(3,3,0.001),holoEntity(47):toWorld(ang(-70,0,0)),vec(255,0,0),"",holoEntity(47),155)
c(50,holoEntity(47):toWorld(vec(0,60,0)),vec(3,3,0.001),holoEntity(47):toWorld(ang(70,90,0)),vec(255,0,0),"",holoEntity(47),155)
c(51,holoEntity(47):toWorld(vec(0,-60,0)),vec(3,3,0.001),holoEntity(47):toWorld(ang(-70,90,0)),vec(255,0,0),"",holoEntity(47),155)

 holoMaterial(48,"debug/debugdrawflat")
 holoMaterial(49,"debug/debugdrawflat")
 holoMaterial(50,"debug/debugdrawflat")
 holoMaterial(51,"debug/debugdrawflat")
################################
    rangerFilter(E)
FR1=rangerOffset(150,E:toWorld(vec(0,DistanceBetweenEntity,0)),vec(0,0,-1)):position()
rangerFilter(E)
FR2=rangerOffset(150,E:toWorld(vec(0,-DistanceBetweenEntity,0)),vec(0,0,-1)):position()
function void angholo(I,AnG:angle){
    holoAng(I,AnG)
}
FR1=Fuss1:pos()
FR2=Fuss2:pos()
function void antifrozen(Eny:entity){
if(Eny:isFrozen()){Eny:propFreeze(0)}   
}
function holoange(EntiHolo){
 holoAng(EntiHolo,E:angles()+ang(0,0,0))   
}
function void footanimstep(Fuss:entity,FussPosition:vector){
   local   Di = vec2(Fuss:pos()):distance(vec2(FussPosition))
       local Ve = (FussPosition + vec(0,0,Di/1.6) - Fuss:pos()) - Fuss:vel() * 0.2
        Fuss:applyForce(Ve*2 * Fuss:mass())
   # Fuss:setPos(Ve)
}
function void animation(Hip1,HipSide,KneeHip1,FootHolo,Footi:entity){
        local    Dist = holoEntity(Hip1):pos() - Footi:pos()       
       local Leng = Dist:length()            
    local Ang1 = Dist:toAngle()
   local  Ang2 = ang(asin(Leng / 85), 0, 0)
        
local  Ang3 = (Ang1):setRoll(bearing(holoEntity(Hip1):pos(), E:angles(), Footi:pos()) )
        
 holoAng(Hip1, Ang3)
      holoAng(HipSide, holoEntity(Hip1):toWorld(-Ang2) )
          #  holoAng(KneeHip1, holoEntity(HipSide):toWorld(-Ang2/4) )
   holoAng(KneeHip1,(holoEntity(KneeHip1):pos()-Footi:pos()):toAngle())#+holoEntity(HipSide):angles():pitch()+holoEntity(HipSide):angles():roll())
        holoAng(FootHolo, E:angles() )

}
function void hoverforce(Enti:entity){
    Enti:applyForce(((Rango+vec(0,0,140)-Enti:massCenter())*2-Enti:vel())*Enti:mass())
}
    function void force(EntityA:entity,Speed:number){
EntityA:applyForce(EntityA:forward()*Speed*(W-S))
    }

function void antiAngles(EntityAnles:angle,TheEntity:entity){
   local Ve = TheEntity:toLocal(rotationVector(quat(EntityAnles)/quat(TheEntity))+TheEntity:pos())
        TheEntity:applyTorque((150*Ve - 25*TheEntity:angVelVector())*TheEntity:inertia())   
}
function void seatPos(Seaty:entity){
   local The_Vec=holoEntity(38):toWorld(vec(0,10,-10))
local Angling=holoEntity(38):toWorld(ang(180,180,0))
    Seaty:setPos(The_Vec)
    Seaty:setAng(Angling)
}
}
Time=700
   timer("laufen",Time)
if(clk("laufen")){
 Stampf=!Stampf   
}
    Velocity=clamp(abs(E:vel():length())+DistanceBetweenEntity,0,-90)
if(changed(Stampf==1)&Stampf==1){ if((W|A|D|S)&!Space){Fuss2:soundPlay(1,1,"ambient/machines/floodgate_stop1.wav") 
    Fuss1:soundPlay(4,1,"buttons/button18.wav")}
      if(!Space & (W|A|S|D)){
   rangerFilter(E)
FR1=rangerOffset(1500,E:toWorld(vec((W-S)*110,DistanceBetweenEntity+(A-D)*110/1.5*Shift,0)),vec(0,0,-1)):position() 

}
if(Space){
     rangerFilter(E)
FR1=rangerOffset(1500,E:toWorld(vec(0,110,0)),vec(0,0,-1)):position() 
  
}
}
if(changed(Stampf==0)&Stampf==0){ if((W|A|D|S)&!Space){Fuss1:soundPlay(2,1,"ambient/machines/floodgate_stop1.wav")
     Fuss2:soundPlay(3,1,"buttons/button18.wav")}
          if(!Space & (W|A|S|D)){
   rangerFilter(E)
FR2=rangerOffset(1500,E:toWorld(vec((W-S)*110,-DistanceBetweenEntity+(A-D)*110/1.5*Shift,0)),vec(0,0,-1)):position() 

}
if(Space){
     rangerFilter(E)
FR2=rangerOffset(1500,E:toWorld(vec(0,-110,0)),vec(0,0,-1)):position() 
  
}
}
if(Driver){
 if((W|S)&!Shift){ 
        EntityA=(EntityA+(A-D)*3)%360
    }   
}
Dist=Rango:distance(E:pos())

if(!W&!S){
#Eye=vec(Driver:eye():dot(E:right()),Driver:eye():dot(E:right()),0):y() 
}
#EAngle=ang(0,EntityA-(Eye*60),0)
EAngle=ang(0,EntityA,0)
if(changed(Driver)&Driver){
 Seat:printColorDriver(vec(255,255,0),"Current OPS=="+toString(ops()))   
}
if(clk("ops")){
timer("ops",2000)
if(changed(clk("ops"))&clk("ops")){
     Seat:printColorDriver(vec(255,255,0),"Current OPS=="+toString(ops()))   
}
}
###the functions###
if(changed(!Driver)&!Driver){PW=30
    rangerFilter(E)
FR1=rangerOffset(150,E:toWorld(vec(0,DistanceBetweenEntity,0)),vec(0,0,-1)):position()
rangerFilter(E)
FR2=rangerOffset(150,E:toWorld(vec(0,-DistanceBetweenEntity,0)),vec(0,0,-1)):position()
    }
if(changed(Driver)&Driver){PW=65}
antiAngles(EAngle,E)
footanimstep(Fuss1,FR1)
footanimstep(Fuss2,FR2)
apply(Fuss1,Fuss2,E,PW,7)
seatPos(Seat)
animation(1,7,3,5,Fuss1)
animation(2,8,4,6,Fuss2)
antifrozen(Fuss1)
antifrozen(Fuss2)
antifrozen(E)
holoange(13)
holoange(14)
holoange(19)
holoange(20)
################33
if(first()){E:soundPlay(0,1,"friends/friend_online.wav")}
if(changed(Driver)&Driver){
 E:soundPlay(5,1,"hl1/fvox/activated.wav")   
}
if(changed(!Driver)&!Driver){
     E:soundPlay(6,1,"hl1/fvox/deactivated.wav")   
}
if(changed(Seat)&Seat){Seat:propFreeze(1)}
 timer("parent",500)
if(clk("parent")&Seat){
   Seat:parentTo(holoEntity(38))
}
if(Driver!=owner()){Seat:killPod()}
####the neckanim###
if(!Driver&T<90){
 T=T+3
if(T>90){T=90}
angholo(1000,E:toWorld(ang(-T+90,180,90)))
}
if(Driver&T>1){    
  T=T-3
if(T<3){T=0}
angholo(1000,E:toWorld(ang(-T+90,180,90)))   }
angholo(38,E:toWorld(ang(180,90,0)))
##################
 Counter++
if(Driver){
M1=Driver:keyAttack1()
M2=Driver:keyAttack2()

if(changed(M1)&M1){
 Shield=propSpawn("models/hunter/blocks/cube2x2x05.mdl",Driver:shootPos()+Driver:eye()*150,1)    Shield:setAlpha(0)
}
if(M1){
BG=BG+0.2
if(BG>6){BG=6}
holoScale(48,vec(3+BG,3+BG,0.001))
holoPos(48,Driver:shootPos()+Driver:eye()*150)
holoAng(48,Driver:eyeAngles()+ang(90,0,0))
Shield:setPos(Driver:shootPos()+Driver:eye()*150)
Shield:setAng(Driver:eyeAngles()+ang(90,0,0))
}


if(!M1&BG>3){
    BG=BG-0.2
 holoScale(48,vec(BG,BG,0.001))   
}
if(changed(!M1)&!M1){
Shield:propDelete()
 holoAng(48,holoEntity(47):toWorld(ang(70,0,0)))   

holoPos(48,holoEntity(47):toWorld(vec(60,0,0))) 
}

  holoAng(47,E:toWorld(ang(0,Counter*5,0)))

}
if(!Driver){
    holoAng(47,E:toWorld(ang(0,Counter*5,0)))
}
@name NightMare Mech 2
@inputs 
@outputs  
@persist [E  Fuss1 Fuss2 Seat]:entity [F1P F2P FR2 FR1 Ranger]:vector EntityA:number Stampf Jump Lock PW T Counter Ar:array BG
@persist Shield:entity
@trigger none
interval(100)
#runOnTick(1)
runOnChat(1)
runOnLast(1)
DistanceBetweenEntity=40
if(last()){E:propDelete() Fuss1:propDelete() Fuss2:propDelete()}
if(!E){E=propSpawn("models/hunter/blocks/cube075x075x075.mdl",entity():pos()+vec(0,0,150),1) E:setMass(50000)}
if(!Fuss1&E){Fuss1=propSpawn("models/hunter/blocks/cube025x025x025.mdl",E:pos()+vec(0,50,-130),0) Fuss1:setMass(50000) Fuss1:setAlpha(0) E:soundPlay(7,1,"clock/tick.wav")}
if(!Fuss2&E){Fuss2=propSpawn("models/hunter/blocks/cube025x025x025.mdl",E:pos()+vec(0,-50,-130),0) Fuss2:setMass(50000) Fuss2:setAlpha(0) E:soundPlay(8,1,"clock/tick.wav")}
if(!Seat&owner():aimEntity():isVehicle()&owner():aimEntity():owner()==owner()&owner():isAlive()){Seat=owner():aimEntity()}
Driver=Seat:driver()
 E:setAlpha(0)
    Key=owner():keyPressed("1")
    if(changed(Key)&Key){Lock+=1}
    if(changed(Lock>=2)&Lock>=2){Lock=0}
    if(changed(Lock==1)&Lock==1){print("Locked") entity():propNotSolid(1)}
        if(changed(Lock==0)&Lock==0){print("unLocked") entity():propNotSolid(0)}
rangerFilter(E)
Rango=rangerOffset(150,E:toWorld(vec(0,0,0)),vec(0,0,-1)):position()

if(Driver){
W=Driver:keyForward()
A=Driver:keyLeft()
S=Driver:keyBack()
D=Driver:keyRight()
Shift=Driver:keySprint()
Space =Driver:keyJump()
Mouse1=Driver:keyAttack1()
Mouse2=Driver:keyAttack2()
}
if(changed(Seat)&Seat){print("seat found !")}
if(first()){
    F1P=Fuss1:pos()
F2P=Fuss2:pos()
function entity c(Index:number,Posi:vector,Scale:vector,Angle:angle,Colo:vector,Model:string,Parent:entity,Alpha:number){
 holoCreate(Index) holoPos(Index,Posi) holoScale(Index,Scale) holoAng(Index,Angle) holoColor(Index,Colo) holoModel(Index,Model) 
holoParent(Index,Parent) holoAlpha(Index,Alpha)
return holoEntity(Index)  
}
function void apply(Foot1:entity,Foot2:entity,Enti:entity,DistHover:number,HoverPower:number){
local VectorHover=(Foot1:pos()+Foot2:pos())/2
local  Hover=((VectorHover+vec(0,0,DistHover))-(E:pos()-vec(0,0,25)))*HoverPower
local Vel=-E:vel()*0.5
Enti:applyForce((Hover+Vel)*E:mass())
} 
##########holos#######
###base legs###
c(1,E:toWorld(vec(0,40,0)),vec(1,1,1),E:toWorld(ang()),vec(255),"0",E,0)
c(2,E:toWorld(vec(0,-40,0)),vec(1,1,1),E:toWorld(ang()),vec(255),"0",E,0)
c(3,holoEntity(1):toWorld(vec(-40.5,0,-40.5)),vec(1,1,1),E:toWorld(ang()),vec(255),"0",holoEntity(1),0)
c(4,holoEntity(2):toWorld(vec(-40.5,0,-40.5)),vec(1,1,1),E:toWorld(ang()),vec(255),"0",holoEntity(2),0)
c(5,holoEntity(3):toWorld(vec(-75,0,0)),vec(1,1,1),E:toWorld(ang()),vec(255),"0",holoEntity(3),0)
c(6,holoEntity(4):toWorld(vec(-75,0,0)),vec(1,1,1),E:toWorld(ang()),vec(255),"0",holoEntity(4),0)
c(7,holoEntity(1):toWorld(vec(0,0,0)),vec(1,1,1),E:toWorld(ang()),vec(255),"0",holoEntity(1),0)
c(8,holoEntity(2):toWorld(vec(0,0,0)),vec(1,1,1),E:toWorld(ang()),vec(255),"0",holoEntity(2),0)
###############
####leg dekoration########
c(9,E:toWorld(vec(0,40,0)),vec(2,2,2),E:toWorld(ang(90,90,0)),vec(255,0,0),"cylinder",holoEntity(1),255) holoMaterial(9,"debug/debugdrawflat")
c(10,E:toWorld(vec(0,-40,0)),vec(2,2,2),E:toWorld(ang(90,90,0)),vec(255,0,0),"cylinder",holoEntity(2),255) holoMaterial(10,"debug/debugdrawflat")
c(11,holoEntity(1):toWorld(vec(-40.5,0,-40.5)),vec(2,2,2),E:toWorld(ang(90,90,0)),vec(255,0,0),"cylinder",holoEntity(1),255) holoMaterial(11,"debug/debugdrawflat")
c(12,holoEntity(2):toWorld(vec(-40.5,0,-40.5)),vec(2,2,2),E:toWorld(ang(90,90,0)),vec(255,0,0),"cylinder",holoEntity(2),255) holoMaterial(12,"debug/debugdrawflat")
c(13,holoEntity(3):toWorld(vec(-75,0,0)),vec(2.5,2.5,1),E:toWorld(ang(55,0,0)),vec(255,0,0),"hqcylinder",holoEntity(3),255) holoMaterial(13,"debug/debugdrawflat")
c(14,holoEntity(4):toWorld(vec(-75,0,0)),vec(2.5,2.5,1),E:toWorld(ang(55,0,0)),vec(255,0,0),"hqcylinder",holoEntity(4),255) holoMaterial(14,"debug/debugdrawflat")
c(15,holoEntity(7):toWorld(vec(0,0,0)),vec(3,3,2),E:toWorld(ang(90,90,0)),vec(0,0,0),"hqtube",holoEntity(7),255) 
c(16,holoEntity(8):toWorld(vec(0,0,0)),vec(3,3,2),E:toWorld(ang(90,90,0)),vec(0),"hqtube",holoEntity(8),255)
c(17,holoEntity(1):toWorld(vec(-40.5,0,-40.5)),vec(3,3,2),E:toWorld(ang(90,90,0)),vec(0),"hqtube",holoEntity(1),255)
c(18,holoEntity(2):toWorld(vec(-40.5,0,-40.5)),vec(3,3,2),E:toWorld(ang(90,90,0)),vec(0),"hqtube",holoEntity(2),255)
c(19,holoEntity(13):toWorld(vec(0,0,0)),vec(3.6,3.6,0.5),E:toWorld(ang(0,0,0)),vec(0,0,0),"hqcylinder",holoEntity(3),255)
c(20,holoEntity(14):toWorld(vec(0,0,0)),vec(3.6,3.6,0.5),E:toWorld(ang(0,0,0)),vec(0),"hqcylinder",holoEntity(4),255)
c(21,holoEntity(1):toWorld(vec(-20.25,0,-20.25)),vec(3.6,2.4,1.2),E:toWorld(ang(-45,0,90)),vec(0,0,0),"hqcylinder",holoEntity(1),255)
c(22,holoEntity(2):toWorld(vec(-20.25,0,-20.25)),vec(3.6,2.4,1.2),E:toWorld(ang(-45,0,90)),vec(0),"hqcylinder",holoEntity(2),255)
c(23,holoEntity(3):toWorld(vec(-37.5,0,0)),vec(4.5,2.5,1.2),E:toWorld(ang(-5,0,90)),vec(0),"hqcylinder",holoEntity(3),255)
c(24,holoEntity(4):toWorld(vec(-37.5,0,0)),vec(4.5,2.5,1.2),E:toWorld(ang(-5,0,90)),vec(0),"hqcylinder",holoEntity(4),255)
c(25,holoEntity(13):toWorld(vec(0,0,5)),vec(2,2,2),E:toWorld(ang(0,0,0)),vec(0,0,0),"hqsphere",holoEntity(3),255)
c(26,holoEntity(14):toWorld(vec(0,0,5)),vec(2,2,2),E:toWorld(ang(0,0,0)),vec(0),"hqsphere",holoEntity(4),255)
c(27,holoEntity(3):toWorld(vec(-37.5,0,0)),vec(4.2,2,2.5),E:toWorld(ang(-5,0,90)),vec(255,0,0),"hqsphere",holoEntity(23),255)  holoMaterial(27,"debug/debugdrawflat")
c(28,holoEntity(4):toWorld(vec(-37.5,0,0)),vec(4.2,2,2.5),E:toWorld(ang(-5,0,90)),vec(255,0,0),"hqsphere",holoEntity(24),255)  holoMaterial(28,"debug/debugdrawflat")
c(29,holoEntity(1):toWorld(vec(-20.25,0,-20.25)),vec(4.5,2,2),E:toWorld(ang(-45,0,90)),vec(255,0,0),"hqsphere",holoEntity(1),255)  holoMaterial(29,"debug/debugdrawflat")
c(30,holoEntity(2):toWorld(vec(-20.25,0,-20.25)),vec(4.5,2,2),E:toWorld(ang(-45,0,90)),vec(255,0,0),"hqsphere",holoEntity(2),255)  holoMaterial(30,"debug/debugdrawflat")
#debug/debugdrawflat
##############
####body usw#####
c(1000,E:toWorld(vec(0,0,0)),vec(1,1,1),E:toWorld(ang(90,0,90)),vec(0),"hqcylinder",E,255)
c(31,E:toWorld(vec(0,20,0)),vec(2,2,3),E:toWorld(ang(90,0,90)),vec(9,0,0),"hqcylinder",E,255)
c(32,E:toWorld(vec(0,-20,0)),vec(2,2,3),E:toWorld(ang(90,0,90)),vec(0),"hqcylinder",E,255)
c(33,E:toWorld(vec(0,0,0)),vec(3.3,3.3,3),E:toWorld(ang(90,90,0)),vec(0,0,0),"hqcylinder",E,255)
c(34,E:toWorld(vec(0,14.9,0)),vec(3.5,3.5,0.5),E:toWorld(ang(90,90,0)),vec(255,0,0),"hqcylinder",E,255)
c(35,E:toWorld(vec(0,-14.9,0)),vec(3.5,3.5,0.5),E:toWorld(ang(90,90,0)),vec(255,0,0),"hqcylinder",E,255)
 holoMaterial(34,"debug/debugdrawflat")
 holoMaterial(35,"debug/debugdrawflat")
#####neck###
c(36,E:toWorld(vec(0,0,30)),vec(4.5,2.5,1.5),E:toWorld(ang(90,90,0)),vec(0),"hqcylinder",holoEntity(1000),255)
c(37,E:toWorld(vec(0,0,30)),vec(4.6,2.6,0.3),E:toWorld(ang(90,90,0)),vec(255),"hqcylinder",holoEntity(36),255)
c(46,E:toWorld(vec(0,0,30)),vec(4.4,2.4,3.5),E:toWorld(ang(90,90,0)),vec(255,0,0),"hqsphere",holoEntity(37),255)
 holoMaterial(46,"debug/debugdrawflat")
 holoMaterial(37,"debug/debugdrawflat")
#################
c(38,E:toWorld(vec(0,0,60)),vec(4.6,4.6,2),E:toWorld(ang(180,90,0)),vec(0),"dome",holoEntity(36),255)
c(39,E:toWorld(vec(0,0,61)),vec(5,5,0.5),E:toWorld(ang(180,90,0)),vec(255,0,0),"dome",holoEntity(38),255)
 holoMaterial(39,"debug/debugdrawflat")
c(40,E:toWorld(vec(0,0,90)),vec(5,5,5),E:toWorld(ang(0,0,0)),vec(0),"hqtube",holoEntity(38),105)
c(41,E:toWorld(vec(0,0,120)),vec(5,5,1),E:toWorld(ang(0,0,0)),vec(0),"dome",holoEntity(38),255)
c(42,E:toWorld(vec(0,-30,90)),vec(0.5,5,0.5),E:toWorld(ang(0,90,90)),vec(0,0,0),"hqcylinder",holoEntity(38),255)
c(43,E:toWorld(vec(0,30,90)),vec(0.5,5,0.5),E:toWorld(ang(0,90,90)),vec(0,0,0),"hqcylinder",holoEntity(38),255)

c(44,E:toWorld(vec(0,-30,90)),vec(0.4,5,1),E:toWorld(ang(0,90,90)),vec(255,0,0),"hqsphere",holoEntity(38),255)
c(45,E:toWorld(vec(0,30,90)),vec(0.4,5,1),E:toWorld(ang(0,90,90)),vec(255,0,0),"hqsphere",holoEntity(38),255)
 holoMaterial(44,"debug/debugdrawflat")
 holoMaterial(45,"debug/debugdrawflat")
#################
############33333333
####awesome effect weapons#########
c(47,E:toWorld(vec(0,0,140)),vec(1,1,1),E:toWorld(ang(0,0,0)),vec(255,0,0),"hqsphere",holoEntity(38),255)
 holoMaterial(47,"debug/debugdrawflat")
c(48,holoEntity(47):toWorld(vec(60,0,0)),vec(3,3,0.001),holoEntity(47):toWorld(ang(70,0,0)),vec(255,0,0),"",holoEntity(47),155)
c(49,holoEntity(47):toWorld(vec(-60,0,0)),vec(3,3,0.001),holoEntity(47):toWorld(ang(-70,0,0)),vec(255,0,0),"",holoEntity(47),155)
c(50,holoEntity(47):toWorld(vec(0,60,0)),vec(3,3,0.001),holoEntity(47):toWorld(ang(70,90,0)),vec(255,0,0),"",holoEntity(47),155)
c(51,holoEntity(47):toWorld(vec(0,-60,0)),vec(3,3,0.001),holoEntity(47):toWorld(ang(-70,90,0)),vec(255,0,0),"",holoEntity(47),155)

 holoMaterial(48,"debug/debugdrawflat")
 holoMaterial(49,"debug/debugdrawflat")
 holoMaterial(50,"debug/debugdrawflat")
 holoMaterial(51,"debug/debugdrawflat")
################################
    rangerFilter(E)
FR1=rangerOffset(150,E:toWorld(vec(0,DistanceBetweenEntity,0)),vec(0,0,-1)):position()
rangerFilter(E)
FR2=rangerOffset(150,E:toWorld(vec(0,-DistanceBetweenEntity,0)),vec(0,0,-1)):position()
function void angholo(I,AnG:angle){
    holoAng(I,AnG)
}
FR1=Fuss1:pos()
FR2=Fuss2:pos()
function void antifrozen(Eny:entity){
if(Eny:isFrozen()){Eny:propFreeze(0)}   
}
function holoange(EntiHolo){
 holoAng(EntiHolo,E:angles()+ang(0,0,0))   
}
function void footanimstep(Fuss:entity,FussPosition:vector){
   local   Di = vec2(Fuss:pos()):distance(vec2(FussPosition))
       local Ve = (FussPosition + vec(0,0,Di/1.6) - Fuss:pos()) - Fuss:vel() * 0.2
        Fuss:applyForce(Ve*2 * Fuss:mass())
   # Fuss:setPos(Ve)
}
function void animation(Hip1,HipSide,KneeHip1,FootHolo,Footi:entity){
        local    Dist = holoEntity(Hip1):pos() - Footi:pos()       
       local Leng = Dist:length()            
    local Ang1 = Dist:toAngle()
   local  Ang2 = ang(asin(Leng / 85), 0, 0)
        
local  Ang3 = (Ang1):setRoll(bearing(holoEntity(Hip1):pos(), E:angles(), Footi:pos()) )
        
 holoAng(Hip1, Ang3)
      holoAng(HipSide, holoEntity(Hip1):toWorld(-Ang2) )
          #  holoAng(KneeHip1, holoEntity(HipSide):toWorld(-Ang2/4) )
   holoAng(KneeHip1,(holoEntity(KneeHip1):pos()-Footi:pos()):toAngle())#+holoEntity(HipSide):angles():pitch()+holoEntity(HipSide):angles():roll())
        holoAng(FootHolo, E:angles() )

}
function void hoverforce(Enti:entity){
    Enti:applyForce(((Rango+vec(0,0,140)-Enti:massCenter())*2-Enti:vel())*Enti:mass())
}
    function void force(EntityA:entity,Speed:number){
EntityA:applyForce(EntityA:forward()*Speed*(W-S))
    }

function void antiAngles(EntityAnles:angle,TheEntity:entity){
   local Ve = TheEntity:toLocal(rotationVector(quat(EntityAnles)/quat(TheEntity))+TheEntity:pos())
        TheEntity:applyTorque((150*Ve - 25*TheEntity:angVelVector())*TheEntity:inertia())   
}
function void seatPos(Seaty:entity){
   local The_Vec=holoEntity(38):toWorld(vec(0,10,-10))
local Angling=holoEntity(38):toWorld(ang(180,180,0))
    Seaty:setPos(The_Vec)
    Seaty:setAng(Angling)
}
}
Time=700
   timer("laufen",Time)
if(clk("laufen")){
 Stampf=!Stampf   
}
    Velocity=clamp(abs(E:vel():length())+DistanceBetweenEntity,0,-90)
if(changed(Stampf==1)&Stampf==1){ if((W|A|D|S)&!Space){Fuss2:soundPlay(1,1,"ambient/machines/floodgate_stop1.wav") 
    Fuss1:soundPlay(4,1,"buttons/button18.wav")}
      if(!Space & (W|A|S|D)){
   rangerFilter(E)
FR1=rangerOffset(1500,E:toWorld(vec((W-S)*110,DistanceBetweenEntity+(A-D)*110/1.5*Shift,0)),vec(0,0,-1)):position() 

}
if(Space){
     rangerFilter(E)
FR1=rangerOffset(1500,E:toWorld(vec(0,110,0)),vec(0,0,-1)):position() 
  
}
}
if(changed(Stampf==0)&Stampf==0){ if((W|A|D|S)&!Space){Fuss1:soundPlay(2,1,"ambient/machines/floodgate_stop1.wav")
     Fuss2:soundPlay(3,1,"buttons/button18.wav")}
          if(!Space & (W|A|S|D)){
   rangerFilter(E)
FR2=rangerOffset(1500,E:toWorld(vec((W-S)*110,-DistanceBetweenEntity+(A-D)*110/1.5*Shift,0)),vec(0,0,-1)):position() 

}
if(Space){
     rangerFilter(E)
FR2=rangerOffset(1500,E:toWorld(vec(0,-110,0)),vec(0,0,-1)):position() 
  
}
}
if(Driver){
 if((W|S)&!Shift){ 
        EntityA=(EntityA+(A-D)*3)%360
    }   
}
Dist=Rango:distance(E:pos())

if(!W&!S){
#Eye=vec(Driver:eye():dot(E:right()),Driver:eye():dot(E:right()),0):y() 
}
#EAngle=ang(0,EntityA-(Eye*60),0)
EAngle=ang(0,EntityA,0)
if(changed(Driver)&Driver){
 Seat:printColorDriver(vec(255,255,0),"Current OPS=="+toString(ops()))   
}
if(clk("ops")){
timer("ops",2000)
if(changed(clk("ops"))&clk("ops")){
     Seat:printColorDriver(vec(255,255,0),"Current OPS=="+toString(ops()))   
}
}
###the functions###
if(changed(!Driver)&!Driver){PW=30
    rangerFilter(E)
FR1=rangerOffset(150,E:toWorld(vec(0,DistanceBetweenEntity,0)),vec(0,0,-1)):position()
rangerFilter(E)
FR2=rangerOffset(150,E:toWorld(vec(0,-DistanceBetweenEntity,0)),vec(0,0,-1)):position()
    }
if(changed(Driver)&Driver){PW=65}
antiAngles(EAngle,E)
footanimstep(Fuss1,FR1)
footanimstep(Fuss2,FR2)
apply(Fuss1,Fuss2,E,PW,7)
seatPos(Seat)
animation(1,7,3,5,Fuss1)
animation(2,8,4,6,Fuss2)
antifrozen(Fuss1)
antifrozen(Fuss2)
antifrozen(E)
holoange(13)
holoange(14)
holoange(19)
holoange(20)
################33
if(first()){E:soundPlay(0,1,"friends/friend_online.wav")}
if(changed(Driver)&Driver){
 E:soundPlay(5,1,"hl1/fvox/activated.wav")   
}
if(changed(!Driver)&!Driver){
     E:soundPlay(6,1,"hl1/fvox/deactivated.wav")   
}
if(changed(Seat)&Seat){Seat:propFreeze(1)}
 timer("parent",500)
if(clk("parent")&Seat){
   Seat:parentTo(holoEntity(38))
}
if(Driver!=owner()){Seat:killPod()}
####the neckanim###
if(!Driver&T<90){
 T=T+3
if(T>90){T=90}
angholo(1000,E:toWorld(ang(-T+90,180,90)))
}
if(Driver&T>1){    
  T=T-3
if(T<3){T=0}
angholo(1000,E:toWorld(ang(-T+90,180,90)))   }
angholo(38,E:toWorld(ang(180,90,0)))
##################
 Counter++
if(Driver){
M1=Driver:keyAttack1()
M2=Driver:keyAttack2()

if(changed(M1)&M1){
 Shield=propSpawn("models/hunter/blocks/cube2x2x05.mdl",Driver:shootPos()+Driver:eye()*150,1)    Shield:setAlpha(0)
}
if(M1){
BG=BG+0.2
if(BG>6){BG=6}
holoScale(48,vec(3+BG,3+BG,0.001))
holoPos(48,Driver:shootPos()+Driver:eye()*150)
holoAng(48,Driver:eyeAngles()+ang(90,0,0))
Shield:setPos(Driver:shootPos()+Driver:eye()*150)
Shield:setAng(Driver:eyeAngles()+ang(90,0,0))
}


if(!M1&BG>3){
    BG=BG-0.2
 holoScale(48,vec(BG,BG,0.001))   
}
if(changed(!M1)&!M1){
Shield:propDelete()
 holoAng(48,holoEntity(47):toWorld(ang(70,0,0)))   

holoPos(48,holoEntity(47):toWorld(vec(60,0,0))) 
}

  holoAng(47,E:toWorld(ang(0,Counter*5,0)))

}
if(!Driver){
    holoAng(47,E:toWorld(ang(0,Counter*5,0)))
}
