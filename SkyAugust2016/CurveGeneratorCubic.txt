@name CurveGeneratorCubic
@inputs [E1 E2 E3 E4]:entity
@outputs 
@persist T:array C

runOnTick(1)
Count = 10

if(first()){
    for(I=1,Count){
     holoCreate(I)
    }

}

  for(P=1,Count){
    
    C = (C + 0.001) % 1
    N = C * P * 0.1
    local Part1 = mix(E1:pos(),E2:pos(),N )
    local Part2 = mix(E2:pos(),E3:pos(),N )
    local Part3 = mix(E3:pos(),E4:pos(),N )

    local M1 = mix(Part1,Part2,N )
    local M2 = mix(Part2,Part3,N )

    local X1 = mix(M1,M2,N)


    T:unshiftVector(holoEntity(1):pos()) 

    local Dist = (holoEntity(P):pos()-holoEntity(P+1):pos())
    holoPos(P-ceil(C),X1)
    holoAng(P,Dist:toAngle())
  }    

