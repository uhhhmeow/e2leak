@name iRunner
@inputs P:wirelink
@outputs 
@persist Speed SPACE Props:array Walksound Walkl Walks Walki E:entity H:array R1:angle R2:angle Lvel:vector
@model models/hunter/blocks/cube025x05x025.mdl
@trigger 
interval(20)
if(first() | duped())
{
E=entity()
Props=E:getConstraints()
#holoCreate(0,E:pos(),vec(1,2,1),E:angles())

holoCreate(1,E:pos(),vec(2,0.5,0.5)*0.95,E:angles())
holoCreate(2,E:pos(),vec(2,0.5,0.5)*0.95,E:angles())

holoCreate(3,E:pos(),vec(2,0.5,0.5)*0.95,E:angles())
holoCreate(4,E:pos(),vec(2,0.5,0.5)*0.95,E:angles())

holoCreate(5,E:pos()+E:forward()*10,vec(0.2,0.7,1)*0.95,E:angles())
holoParent(5,4)
holoCreate(6,E:pos()+E:forward()*10,vec(0.2,0.7,1)*0.95,E:angles())
holoParent(6,2)

holoCreate(7,E:pos()-E:forward()*10,vec(1.1,1.1,0.6),E:angles()+ang(90,90,0))
holoModel(7,"hqcylinder2")
holoParent(7,3)
holoCreate(8,E:pos()-E:forward()*10,vec(1.1,1.1,0.6),E:angles()+ang(90,90,0))
holoModel(8,"hqcylinder2")
holoParent(8,1)

holoCreate(9,E:pos()-E:forward()*10,vec(0.6,0.6,0.6),E:angles()+ang(90,90,0))
holoModel(9,"hqcylinder2")
holoParent(9,4)
holoCreate(10,E:pos()-E:forward()*10,vec(0.6,0.6,0.6),E:angles()+ang(90,90,0))
holoModel(10,"hqcylinder2")
holoParent(10,2)

holoMaterial(1,"phoenix_storms/mat/mat_phx_metallic")
holoMaterial(2,"phoenix_storms/mat/mat_phx_metallic")
holoMaterial(3,"phoenix_storms/mat/mat_phx_metallic")
holoMaterial(4,"phoenix_storms/mat/mat_phx_metallic")
holoMaterial(5,"phoenix_storms/mat/mat_phx_metallic")
holoMaterial(6,"phoenix_storms/mat/mat_phx_metallic")
holoMaterial(7,"phoenix_storms/mat/mat_phx_metallic")
holoMaterial(8,"phoenix_storms/mat/mat_phx_metallic")
holoMaterial(9,"phoenix_storms/mat/mat_phx_metallic")
holoMaterial(10,"phoenix_storms/mat/mat_phx_metallic")

holoColor(5,vec(0,0,255))
holoColor(6,vec(0,0,255))
holoColor(7,vec(170,170,255))
holoColor(8,vec(170,170,255))
holoColor(9,vec(90,90,255))
holoColor(10,vec(90,90,255))

holoColor(1,vec(130,130,255))
holoColor(2,vec(70,70,255))
holoColor(3,vec(130,130,255))
holoColor(4,vec(70,70,255))
H[0,entity]=holoEntity(0)
H[1,entity]=holoEntity(1)
Walks=20
Walkl=60
E:setMass(100)
}
rangerHitWater(1)
rangerFilter(E)
rangerFilter(Props)
R=rangerOffset(46,E:pos(),vec(0,0,-1))
Speed=E:velL():x()
Turnspeed=E:angVel():yaw()/19
W=P["W",number]
A=P["A",number]
S=P["S",number]
D=P["D",number]
ACT=P["Active",number]
SHIFT=P["Shift",number]
SPACE=P["Space",number]
CONTROL=P["Control",number]
M1=P["Mouse1",number]
Walkl=abs(Speed/10)
Walki+=Walks*((E:velL():x()>0)*2 -1)
if(Walksound<Walki)
    {
    Walksound=Walki+1
    soundPlay(0,0,"npc/dog/dog_footstep"+randint(1,4):toString()+".wav")
    soundVolume(0,min(abs(E:velL():x())/110,1))
    }
Sin=sin(Walki)
Cos=cos(Walki)
Distv=acos((min(R:distance(),41))/41)
WsubT=Walkl-Turnspeed
WplusT=Walkl+Turnspeed
R1=E:toWorld(ang(Sin*WplusT+90-Distv,0,0))
R2=E:toWorld(ang((Sin-Cos)*WplusT +Walkl+90+Distv,0,0))
ER=E:right()*10
EF=-E:forward()*6
holoPos(1,EF+ER+E:pos()+R1:forward()*10)
holoAng(1,R1)
holoPos(2,EF+ER+E:pos()+R1:forward()*20 + R2:forward()*10)
holoAng(2,R2)

holoAng(6,R2+ang(Distv,0,0))

R1=E:toWorld(ang(-Sin*WsubT+90-Distv,0,0))
R2=E:toWorld(ang((-Sin+Cos)*WsubT +Walkl+90+Distv,0,0))
holoPos(3,EF-ER+E:pos()+R1:forward()*10)
holoAng(3,R1)
holoPos(4,EF-ER+E:pos()+R1:forward()*20 + R2:forward()*10)
holoAng(4,R2)


holoAng(5,R2+ang(Distv,0,0))
if(R:hit())
{
E:applyForce(E:forward()*1040*(W-S)*(0.5+SHIFT/2)+vec(0,0,-20+ACT*20-SPACE*17+($SPACE & !SPACE)*(300-R:distance()*6)+46-CONTROL*30-R:distance()+cos(Walki*2)*Walkl/6)*300 - E:vel()*vec(2.3,2.3,20) +E:right()*E:velL():y()*30)
E:applyAngForce(ang(min($Speed*3,0),(A-D)*30,-Sin*Walkl/2 + E:velL():y()*1.5)*100-E:angles()*ang(1,0,1)*300-E:angVel()*30)
}
@name iRunner
@inputs P:wirelink
@outputs 
@persist Speed SPACE Props:array Walksound Walkl Walks Walki E:entity H:array R1:angle R2:angle Lvel:vector
@model models/hunter/blocks/cube025x05x025.mdl
@trigger 
interval(20)
if(first() | duped())
{
E=entity()
Props=E:getConstraints()
#holoCreate(0,E:pos(),vec(1,2,1),E:angles())

holoCreate(1,E:pos(),vec(2,0.5,0.5)*0.95,E:angles())
holoCreate(2,E:pos(),vec(2,0.5,0.5)*0.95,E:angles())

holoCreate(3,E:pos(),vec(2,0.5,0.5)*0.95,E:angles())
holoCreate(4,E:pos(),vec(2,0.5,0.5)*0.95,E:angles())

holoCreate(5,E:pos()+E:forward()*10,vec(0.2,0.7,1)*0.95,E:angles())
holoParent(5,4)
holoCreate(6,E:pos()+E:forward()*10,vec(0.2,0.7,1)*0.95,E:angles())
holoParent(6,2)

holoCreate(7,E:pos()-E:forward()*10,vec(1.1,1.1,0.6),E:angles()+ang(90,90,0))
holoModel(7,"hqcylinder2")
holoParent(7,3)
holoCreate(8,E:pos()-E:forward()*10,vec(1.1,1.1,0.6),E:angles()+ang(90,90,0))
holoModel(8,"hqcylinder2")
holoParent(8,1)

holoCreate(9,E:pos()-E:forward()*10,vec(0.6,0.6,0.6),E:angles()+ang(90,90,0))
holoModel(9,"hqcylinder2")
holoParent(9,4)
holoCreate(10,E:pos()-E:forward()*10,vec(0.6,0.6,0.6),E:angles()+ang(90,90,0))
holoModel(10,"hqcylinder2")
holoParent(10,2)

holoMaterial(1,"phoenix_storms/mat/mat_phx_metallic")
holoMaterial(2,"phoenix_storms/mat/mat_phx_metallic")
holoMaterial(3,"phoenix_storms/mat/mat_phx_metallic")
holoMaterial(4,"phoenix_storms/mat/mat_phx_metallic")
holoMaterial(5,"phoenix_storms/mat/mat_phx_metallic")
holoMaterial(6,"phoenix_storms/mat/mat_phx_metallic")
holoMaterial(7,"phoenix_storms/mat/mat_phx_metallic")
holoMaterial(8,"phoenix_storms/mat/mat_phx_metallic")
holoMaterial(9,"phoenix_storms/mat/mat_phx_metallic")
holoMaterial(10,"phoenix_storms/mat/mat_phx_metallic")

holoColor(5,vec(0,0,255))
holoColor(6,vec(0,0,255))
holoColor(7,vec(170,170,255))
holoColor(8,vec(170,170,255))
holoColor(9,vec(90,90,255))
holoColor(10,vec(90,90,255))

holoColor(1,vec(130,130,255))
holoColor(2,vec(70,70,255))
holoColor(3,vec(130,130,255))
holoColor(4,vec(70,70,255))
H[0,entity]=holoEntity(0)
H[1,entity]=holoEntity(1)
Walks=20
Walkl=60
E:setMass(100)
}
rangerHitWater(1)
rangerFilter(E)
rangerFilter(Props)
R=rangerOffset(46,E:pos(),vec(0,0,-1))
Speed=E:velL():x()
Turnspeed=E:angVel():yaw()/19
W=P["W",number]
A=P["A",number]
S=P["S",number]
D=P["D",number]
ACT=P["Active",number]
SHIFT=P["Shift",number]
SPACE=P["Space",number]
CONTROL=P["Control",number]
M1=P["Mouse1",number]
Walkl=abs(Speed/10)
Walki+=Walks*((E:velL():x()>0)*2 -1)
if(Walksound<Walki)
    {
    Walksound=Walki+1
    soundPlay(0,0,"npc/dog/dog_footstep"+randint(1,4):toString()+".wav")
    soundVolume(0,min(abs(E:velL():x())/110,1))
    }
Sin=sin(Walki)
Cos=cos(Walki)
Distv=acos((min(R:distance(),41))/41)
WsubT=Walkl-Turnspeed
WplusT=Walkl+Turnspeed
R1=E:toWorld(ang(Sin*WplusT+90-Distv,0,0))
R2=E:toWorld(ang((Sin-Cos)*WplusT +Walkl+90+Distv,0,0))
ER=E:right()*10
EF=-E:forward()*6
holoPos(1,EF+ER+E:pos()+R1:forward()*10)
holoAng(1,R1)
holoPos(2,EF+ER+E:pos()+R1:forward()*20 + R2:forward()*10)
holoAng(2,R2)

holoAng(6,R2+ang(Distv,0,0))

R1=E:toWorld(ang(-Sin*WsubT+90-Distv,0,0))
R2=E:toWorld(ang((-Sin+Cos)*WsubT +Walkl+90+Distv,0,0))
holoPos(3,EF-ER+E:pos()+R1:forward()*10)
holoAng(3,R1)
holoPos(4,EF-ER+E:pos()+R1:forward()*20 + R2:forward()*10)
holoAng(4,R2)


holoAng(5,R2+ang(Distv,0,0))
if(R:hit())
{
E:applyForce(E:forward()*1040*(W-S)*(0.5+SHIFT/2)+vec(0,0,-20+ACT*20-SPACE*17+($SPACE & !SPACE)*(300-R:distance()*6)+46-CONTROL*30-R:distance()+cos(Walki*2)*Walkl/6)*300 - E:vel()*vec(2.3,2.3,20) +E:right()*E:velL():y()*30)
E:applyAngForce(ang(min($Speed*3,0),(A-D)*30,-Sin*Walkl/2 + E:velL():y()*1.5)*100-E:angles()*ang(1,0,1)*300-E:angVel()*30)
}
