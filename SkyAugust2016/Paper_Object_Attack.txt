@name Paper Object Attack
@inputs Enable Clr Spawner:entity
@outputs Spawn Undo
@persist Count TCount Target:entity Delay
@trigger all
if( first() | duped() )
{
    interval(1)
}
if(clk())
{
    if(Enable & Target)
    {
        interval(1)
        Undo = 0
        if( TCount < 100 ){ Spawn = !Spawn }
        
        if( !Count )
        {
            # Reload the list of props
            findIncludePlayerProps(entity():owner())
            Count = findByModel(Spawner:model())
            TCount = Count
            print( "Loaded " + Count + " Spawned Weapon Props of Type " + Spawner:model() + "." )
        }
        if( Count > 0 )
        {
            if( findResult(Count) != Spawner )
            {
                # Apply force to the next item in the list
                DifPos = Target:pos()-findResult(Count):pos()
                findResult(Count):applyForce( 10000000000000 * DifPos )
                findResult(Count):setMass(50000)
            }
            
            # Move on to the next prop
            Count--
        }
    }elseif(Enable){
        interval(1000)
        if( Delay )
        {
            if( entity():owner():aimEntity() )
            {
                print( "Target Locked!" )
                Target = entity():owner():aimEntity()
            }
        }else{
            Delay = 1
        }
    }else{
        interval(1)
        Delay = 0
        Count = 0
        Spawn = 0
        Undo = !Undo
    }
}
@name Paper Object Attack
@inputs Enable Clr Spawner:entity
@outputs Spawn Undo
@persist Count TCount Target:entity Delay
@trigger all
if( first() | duped() )
{
    interval(1)
}
if(clk())
{
    if(Enable & Target)
    {
        interval(1)
        Undo = 0
        if( TCount < 100 ){ Spawn = !Spawn }
        
        if( !Count )
        {
            # Reload the list of props
            findIncludePlayerProps(entity():owner())
            Count = findByModel(Spawner:model())
            TCount = Count
            print( "Loaded " + Count + " Spawned Weapon Props of Type " + Spawner:model() + "." )
        }
        if( Count > 0 )
        {
            if( findResult(Count) != Spawner )
            {
                # Apply force to the next item in the list
                DifPos = Target:pos()-findResult(Count):pos()
                findResult(Count):applyForce( 10000000000000 * DifPos )
                findResult(Count):setMass(50000)
            }
            
            # Move on to the next prop
            Count--
        }
    }elseif(Enable){
        interval(1000)
        if( Delay )
        {
            if( entity():owner():aimEntity() )
            {
                print( "Target Locked!" )
                Target = entity():owner():aimEntity()
            }
        }else{
            Delay = 1
        }
    }else{
        interval(1)
        Delay = 0
        Count = 0
        Spawn = 0
        Undo = !Undo
    }
}
