@name SProps alphabet
@inputs KB:wirelink
@persist Ran:ranger Center:entity
@outputs X Z H C
if(first()) {
runOnTick(1) runOnLast(1)
holoCreate(0,vec(),vec(1,0.5,0.3),ang(),vec(0,255,255),"rcube")
}
if(!Center) {Center=propSpawn("models/hunter/blocks/cube025x025x025.mdl",entity():toWorld(vec(0,50,20)),ang(0,-180,0),1)}
KB:entity():setPos(entity():toWorld(vec(0,-50,50)))
KB:entity():setAng(entity():angles()+ang(50,-90,0))
if(last()) {propDeleteAll()}
holoPos(0,Center:toWorld(vec(-X*12,30,41.5+Z*-15)))
if(changed(KB["Memory",number])&KB["Memory",number]) {
Buchstabe=toChar(KB["Memory",number])
Space=KB["Memory",number]==32
BackSpace=KB["Memory",number]==127
Enter=KB["Memory",number]==13
AusRuf=KB["Memory",number]==33
Shift=KB["Memory",number]==155
KlammerZu=KB["Memory",number]==40
KlammerAuf=KB["Memory",number]==42
DoppelPunkt=KB["Memory",number]==62

if(!Enter&!BackSpace&!Space&!AusRuf&!Shift&!KlammerZu&!KlammerAuf&!DoppelPunkt&KB["Memory",number]<=122&KB["Memory",number]>=37) {
H++   X++
holoCreate(H,Center:toWorld(vec(-X*12,30,50+Z*-15)),vec(1),Center:angles(),vec(255),"models/sprops/misc/alphanum/alphanum_"+Buchstabe+".mdl")
holoParent(H,Center)
}

if(Enter) {
Z++   X=0 H++
}

if(Space) {
X++
H++
}

if(AusRuf) {
H++   X++
holoCreate(H,Center:toWorld(vec(-X*12,30,50+Z*-15)),vec(1),Center:angles(),vec(255),"models/sprops/misc/alphanum/alphanum_xmark.mdl")
holoParent(H,Center)
}
if(KlammerAuf) {
H++   X++
holoCreate(H,Center:toWorld(vec(-X*12,30,50+Z*-15)),vec(1),Center:angles(),vec(255),"models/sprops/misc/alphanum/alphanum_lpar.mdl")
holoParent(H,Center)
}
if(KlammerZu) {
H++   X++
holoCreate(H,Center:toWorld(vec(-X*12,30,50+Z*-15)),vec(1),Center:angles(),vec(255),"models/sprops/misc/alphanum/alphanum_rpar.mdl")
holoParent(H,Center)
}
if(DoppelPunkt) {
H++   X++
holoCreate(H,Center:toWorld(vec(-X*12,30,50+Z*-15)),vec(1),Center:angles(),vec(255),"models/sprops/misc/alphanum/alphanum_colon.mdl")
holoParent(H,Center)
}
if(X>0&BackSpace) {
X--
holoDelete(H)
H--
}elseif(X==0&BackSpace&Z>0) {
Z--
X=H
}


}
@name SProps alphabet
@inputs KB:wirelink
@persist Ran:ranger Center:entity
@outputs X Z H C
if(first()) {
runOnTick(1) runOnLast(1)
holoCreate(0,vec(),vec(1,0.5,0.3),ang(),vec(0,255,255),"rcube")
}
if(!Center) {Center=propSpawn("models/hunter/blocks/cube025x025x025.mdl",entity():toWorld(vec(0,50,20)),ang(0,-180,0),1)}
KB:entity():setPos(entity():toWorld(vec(0,-50,50)))
KB:entity():setAng(entity():angles()+ang(50,-90,0))
if(last()) {propDeleteAll()}
holoPos(0,Center:toWorld(vec(-X*12,30,41.5+Z*-15)))
if(changed(KB["Memory",number])&KB["Memory",number]) {
Buchstabe=toChar(KB["Memory",number])
Space=KB["Memory",number]==32
BackSpace=KB["Memory",number]==127
Enter=KB["Memory",number]==13
AusRuf=KB["Memory",number]==33
Shift=KB["Memory",number]==155
KlammerZu=KB["Memory",number]==40
KlammerAuf=KB["Memory",number]==42
DoppelPunkt=KB["Memory",number]==62

if(!Enter&!BackSpace&!Space&!AusRuf&!Shift&!KlammerZu&!KlammerAuf&!DoppelPunkt&KB["Memory",number]<=122&KB["Memory",number]>=37) {
H++   X++
holoCreate(H,Center:toWorld(vec(-X*12,30,50+Z*-15)),vec(1),Center:angles(),vec(255),"models/sprops/misc/alphanum/alphanum_"+Buchstabe+".mdl")
holoParent(H,Center)
}

if(Enter) {
Z++   X=0 H++
}

if(Space) {
X++
H++
}

if(AusRuf) {
H++   X++
holoCreate(H,Center:toWorld(vec(-X*12,30,50+Z*-15)),vec(1),Center:angles(),vec(255),"models/sprops/misc/alphanum/alphanum_xmark.mdl")
holoParent(H,Center)
}
if(KlammerAuf) {
H++   X++
holoCreate(H,Center:toWorld(vec(-X*12,30,50+Z*-15)),vec(1),Center:angles(),vec(255),"models/sprops/misc/alphanum/alphanum_lpar.mdl")
holoParent(H,Center)
}
if(KlammerZu) {
H++   X++
holoCreate(H,Center:toWorld(vec(-X*12,30,50+Z*-15)),vec(1),Center:angles(),vec(255),"models/sprops/misc/alphanum/alphanum_rpar.mdl")
holoParent(H,Center)
}
if(DoppelPunkt) {
H++   X++
holoCreate(H,Center:toWorld(vec(-X*12,30,50+Z*-15)),vec(1),Center:angles(),vec(255),"models/sprops/misc/alphanum/alphanum_colon.mdl")
holoParent(H,Center)
}
if(X>0&BackSpace) {
X--
holoDelete(H)
H--
}elseif(X==0&BackSpace&Z>0) {
Z--
X=H
}


}
