@name PropDrone flying thing
@inputs Egp:wirelink 
@outputs Screen:string
@persist [Model Pos2 Ang Props]:array [E Seat Driver]:entity Pos:vector  B1:entity B2:entity Seat2:entity Plate:entity NN:entity
@persist Ball:entity
@trigger 
#################controlls###########
W=Driver:keyForward()
A=Driver:keyLeft()
S=Driver:keyBack()
D=Driver:keyRight()
Space=Driver:keyJump()
Shift=Driver:keySprint()
Mouse1=Driver:keyAttack1()
Mouse2=Driver:keyAttack2()
Alt=Driver:keyWalk()

Come=Driver:keyPressed("0")
#############3props###########


Driver=Seat:driver()
if(first()|duped()|dupefinished()){
runOnTick(1)
runOnChat(1)
runOnLast(1)
E=entity()
Seat=E:isWeldedTo()
SPAWN=1    
  holoCreate(1,Seat:toWorld(vec(0,0,0)),vec(0.5,0.5,0.5),ang(),vec(255,255,0),"hqsphere")
    holoParent(1,Seat)
    
      holoCreate(2,holoEntity(1):toWorld(vec(0,0,20)),vec(13,13,10),ang(90,0,90),vec(255,255,0),"hqcylinder")
    holoParent(2,1)
    holoMaterial(2,"models/props_combine/tprings_globe")
  
 
Model=array(
"models/hunter/tubes/tube4x4x05.mdl","models/hunter/tubes/tube4x4x05.mdl","models/hunter/tubes/tube4x4x1to2x2.mdl",
"models/hunter/tubes/tube4x4x1to2x2.mdl","models/hunter/tubes/tube1x1x2.mdl","models/hunter/tubes/tube1x1x2.mdl",
"models/hunter/tubes/tube2x2x1c.mdl","models/hunter/tubes/tube2x2x1c.mdl","models/hunter/tubes/tube4x4x2c.mdl",
"models/hunter/tubes/tube1x1x2.mdl","models/hunter/tubes/tube1x1x2.mdl","models/props_phx/construct/metal_angle360.mdl",
"models/props_phx/construct/metal_angle360.mdl","models/hunter/misc/sphere2x2.mdl","models/hunter/misc/sphere2x2.mdl"
,"models/hunter/tubes/tubebend2x2x90outer.mdl","models/hunter/tubes/tubebend2x2x90outer.mdl","models/hunter/tubes/tube1x1x2.mdl"
,"models/hunter/tubes/tube1x1x2.mdl","models/hunter/tubes/tube4x4x2to2x2.mdl","models/props_phx/construct/metal_angle360.mdl"
,"models/props_phx/construct/metal_dome360.mdl","models/props_phx/construct/metal_dome360.mdl","models/props_phx/construct/metal_plate_curve360x2.mdl"
,"models/hunter/tubes/tube1x1x3.mdl","models/hunter/tubes/tube1x1x3.mdl","models/hunter/tubes/tube1x1x3.mdl"
,"models/hunter/tubes/tube1x1x3.mdl","models/props_phx/construct/metal_plate_curve360x2.mdl","models/hunter/tubes/tube1x1x3.mdl"
,"models/hunter/tubes/tube1x1x3.mdl","models/hunter/tubes/tube1x1x3.mdl","models/hunter/tubes/tube1x1x3.mdl"
,"models/hunter/tubes/circle2x2.mdl","models/hunter/tubes/tube4x4x3c.mdl","models/hunter/tubes/tube4x4x05c.mdl"
,"models/hunter/plates/plate1x6.mdl","models/hunter/triangles/1x1.mdl","models/hunter/triangles/1x1.mdl"
,"","","",
"","",""
,"","",""
,"","",""
,"","",""
,"","",""
)
Pos2=array(
vec(0,55,0),vec(0,-55,0),vec(0,115,0),
vec(0,-115,0),vec(0,-70,0),vec(0,170,0),
vec(0,-150,0),vec(0,150,0),vec(-20,0,0),
vec(-160,0,-23),vec(-160,0,23),vec(0,185,0),
vec(0,-180,0),vec(0,-210,0),vec(0,210,0)
,vec(0,120,0),vec(0,-120,0),vec(-150,-50,0)
,vec(-150,50,0),vec(100,0,0),vec(140,0,0)
,vec(-50,0,80),vec(10,0,-80),vec(-10,-210,0)
,vec(50,-210,30),vec(50,-180,0),vec(50,-240,0)
,vec(50,-210,-30),vec(0,210,5),vec(50,180,0)
,vec(50,240,0),vec(50,210,30),vec(50,210,-30)
,vec(5,-3,-80),vec(-120,0,-20),vec(-180,0,150)
,vec(-180,0,155),vec(-180,164,155),vec(-180,-164,155)
,vec(0,0,0),vec(0,0,0),vec(0,0,0)
,vec(0,0,0),vec(0,0,0),vec(0,0,0)
,vec(0,0,0),vec(0,0,0),vec(0,0,0)
,vec(0,0,0),vec(0,0,0),vec(0,0,0)
,vec(0,0,0),vec(0,0,0),vec(0,0,0)

)
Ang=array(
ang(0,0,90),ang(0,0,90),ang(0,180,90),
ang(180,0,90),ang(0,0,90),ang(0,0,90),
ang(90,0,90),ang(90,0,90),ang(0,0,90),
ang(0,90,90),ang(0,90,90),ang(0,0,90),
ang(0,0,90),ang(0,0,0),ang(0,0,0)
,ang(0,0,180),ang(0,180,180),ang(0,90,90)
,ang(0,90,90),ang(0,90,90),ang(90,0,0)
,ang(0,0,0),ang(0,0,180),ang(0,90,90)
,ang(0,-90,-90),ang(0,-90,-90),ang(0,-90,-90)
,ang(0,-90,-90),ang(0,90,90),ang(0,-90,-90)
,ang(0,-90,-90),ang(0,-90,-90),ang(0,-90,-90)
,ang(0,0,0),ang(90,0,0),ang(-90,0,0)
,ang(0,0,0),ang(0,0,0),ang(0,0,180)
,ang(0,0,0),ang(0,0,0),ang(0,0,0)
,ang(0,0,0),ang(0,0,0),ang(0,0,0)
,ang(0,0,0),ang(0,0,0),ang(0,0,0)
,ang(0,0,0),ang(0,0,0),ang(0,0,0)

)    

}
Pos=holoEntity(1):toWorld(vec(0,0,20))
timer("SPAWN",100)
if(changed(E)&E|clk("SPAWN")){

    for(P=1,Model:count()){
        if(!Props[P,entity]){
        Props[P,entity]=propSpawn(Model[P,string],Pos+Pos2[P,vector],Ang[P,angle],1)
        Props[P,entity]:parentTo(holoEntity(1))
        Props[P,entity]:setMaterial("phoenix_storms/gear")
        Props[P,entity]:setColor(vec(255,0,0))
        Props[24,entity]:propNotSolid(1)
        }
    }
    
}



if(last()){
 propDeleteAll()   
}
#####################
holoAng(1,Driver:eyeAngles())
Props[21,entity]:setAlpha(0)
Props[15,entity]:setAng(Driver:eyeAngles())
Props[14,entity]:setAng(Driver:eyeAngles())
   Seat:setAng(ang())
    Seat:propFreeze(1)
    #########
    
    #########DUESEN#########
    if(first()){
        
    }
    
    
    #######################
    
    
    
    if(W){
 Seat:setPos(Seat:pos()+Driver:eye()*15)       
}
   if(S){
 Seat:setPos(Seat:pos()+Driver:eye()*-15)       
}
    if(W&Shift){
 Seat:setPos(Seat:pos()+Driver:eye()*55)       
}
   if(S&Shift){
 Seat:setPos(Seat:pos()+Driver:eye()*-55)       
}
noCollideAll(Seat,1)

Seat:setAlpha(0)
timer("solide",150)
timer("reload",250)
if(Mouse1&clk("reload")){
 B1=propSpawn("models/props_phx/ww2bomb.mdl",(holoEntity(1):toWorld(vec(150,200,0))),0)
   B1:propGravity(0)
B1:setTrails(30,0,10,"trails/smoke",vec(50,50,50),255)
B1:applyForce(holoEntity(1):forward()*B1:mass()*5000)
B1:setAng(Driver:eyeAngles())
}
if(Mouse2&clk("reload")){
 B2=propSpawn("models/props_phx/ww2bomb.mdl",(holoEntity(1):toWorld(vec(150,-200,0))),0)
   B2:propGravity(0)
B2:setTrails(30,0,10,"trails/smoke",vec(50,50,50),255)
B2:applyForce(holoEntity(1):forward()*B2:mass()*5000)
B2:setAng(Driver:eyeAngles())
} if(Driver:name()!=owner():name()){Seat:killPod()   }

#########egp############
timer("egp",150)
if(changed(E)&E|clk("egp")){
 
for(Pa=1,players():count()){
Ply=players()[Pa,entity] 
   Egp:egp3DTracker(Pa+100,Ply:pos())
Egp:egpText(Pa+5,Pa+" "+Ply:name(),vec2())
Egp:egpSize(Pa+5,20)
Egp:egpParent(Pa+5,Pa+100)

if(Ply:isAlive()){Egp:egpColor(Pa+5,vec(255,255,0))}else{Egp:egpColor(Pa+5,vec(255,0,0))}
}
    
}

######################
Screen="
Made by Sky 
"
Last=lastSaid():explode(" ")
if(chatClk(owner())&Last:string(1)=="/spawn"){
    hideChat(1)
 Plate=propSpawn("models/hunter/plates/plate24x24.mdl",(Seat:toWorld(vec(0,0,-30))),ang(),1)   
    Plate:setMaterial("phoenix_storms/gear")
    Plate:setColor(vec(25,50,25))
    Plate:setAlpha(150)
    Plate:parentTo(Seat)
}
if(chatClk(owner())&Last:string(1)=="/remove"){
    hideChat(1)
Plate:propDelete()
}

#########################

R=Driver:keyReload()
if(Come){
    Seat:killPod()
}
if(changed(R)&R){
 Ball=propSpawn("models/hunter/misc/sphere375x375.mdl",(holoEntity(1):toWorld(vec(350,0,0))),0)
Ball:setTrails(30,0,2,"trails/electric",vec(25,88,25),255)
Ball:setMaterial("models/vortigaunt/pupil")
Ball:setColor(25,88,25)
Ball:setMass(50000)
Ball:propGravity(0)
print("Ball has been spawned wait for the remove befor spawning a new one !")
}
Ball:applyForce(Ball:forward()*Ball:mass()*15000)
Ball:setAng(Driver:eyeAngles())
timer("remove",5500)
if(clk("remove")){
 Ball:propDelete()  
}
if(changed(!Ball)&!Ball){ 
print("Ball has been removed")
}
@name PropDrone flying thing
@inputs Egp:wirelink 
@outputs Screen:string
@persist [Model Pos2 Ang Props]:array [E Seat Driver]:entity Pos:vector  B1:entity B2:entity Seat2:entity Plate:entity NN:entity
@persist Ball:entity
@trigger 
#################controlls###########
W=Driver:keyForward()
A=Driver:keyLeft()
S=Driver:keyBack()
D=Driver:keyRight()
Space=Driver:keyJump()
Shift=Driver:keySprint()
Mouse1=Driver:keyAttack1()
Mouse2=Driver:keyAttack2()
Alt=Driver:keyWalk()

Come=Driver:keyPressed("0")
#############3props###########


Driver=Seat:driver()
if(first()|duped()|dupefinished()){
runOnTick(1)
runOnChat(1)
runOnLast(1)
E=entity()
Seat=E:isWeldedTo()
SPAWN=1    
  holoCreate(1,Seat:toWorld(vec(0,0,0)),vec(0.5,0.5,0.5),ang(),vec(255,255,0),"hqsphere")
    holoParent(1,Seat)
    
      holoCreate(2,holoEntity(1):toWorld(vec(0,0,20)),vec(13,13,10),ang(90,0,90),vec(255,255,0),"hqcylinder")
    holoParent(2,1)
    holoMaterial(2,"models/props_combine/tprings_globe")
  
 
Model=array(
"models/hunter/tubes/tube4x4x05.mdl","models/hunter/tubes/tube4x4x05.mdl","models/hunter/tubes/tube4x4x1to2x2.mdl",
"models/hunter/tubes/tube4x4x1to2x2.mdl","models/hunter/tubes/tube1x1x2.mdl","models/hunter/tubes/tube1x1x2.mdl",
"models/hunter/tubes/tube2x2x1c.mdl","models/hunter/tubes/tube2x2x1c.mdl","models/hunter/tubes/tube4x4x2c.mdl",
"models/hunter/tubes/tube1x1x2.mdl","models/hunter/tubes/tube1x1x2.mdl","models/props_phx/construct/metal_angle360.mdl",
"models/props_phx/construct/metal_angle360.mdl","models/hunter/misc/sphere2x2.mdl","models/hunter/misc/sphere2x2.mdl"
,"models/hunter/tubes/tubebend2x2x90outer.mdl","models/hunter/tubes/tubebend2x2x90outer.mdl","models/hunter/tubes/tube1x1x2.mdl"
,"models/hunter/tubes/tube1x1x2.mdl","models/hunter/tubes/tube4x4x2to2x2.mdl","models/props_phx/construct/metal_angle360.mdl"
,"models/props_phx/construct/metal_dome360.mdl","models/props_phx/construct/metal_dome360.mdl","models/props_phx/construct/metal_plate_curve360x2.mdl"
,"models/hunter/tubes/tube1x1x3.mdl","models/hunter/tubes/tube1x1x3.mdl","models/hunter/tubes/tube1x1x3.mdl"
,"models/hunter/tubes/tube1x1x3.mdl","models/props_phx/construct/metal_plate_curve360x2.mdl","models/hunter/tubes/tube1x1x3.mdl"
,"models/hunter/tubes/tube1x1x3.mdl","models/hunter/tubes/tube1x1x3.mdl","models/hunter/tubes/tube1x1x3.mdl"
,"models/hunter/tubes/circle2x2.mdl","models/hunter/tubes/tube4x4x3c.mdl","models/hunter/tubes/tube4x4x05c.mdl"
,"models/hunter/plates/plate1x6.mdl","models/hunter/triangles/1x1.mdl","models/hunter/triangles/1x1.mdl"
,"","","",
"","",""
,"","",""
,"","",""
,"","",""
,"","",""
)
Pos2=array(
vec(0,55,0),vec(0,-55,0),vec(0,115,0),
vec(0,-115,0),vec(0,-70,0),vec(0,170,0),
vec(0,-150,0),vec(0,150,0),vec(-20,0,0),
vec(-160,0,-23),vec(-160,0,23),vec(0,185,0),
vec(0,-180,0),vec(0,-210,0),vec(0,210,0)
,vec(0,120,0),vec(0,-120,0),vec(-150,-50,0)
,vec(-150,50,0),vec(100,0,0),vec(140,0,0)
,vec(-50,0,80),vec(10,0,-80),vec(-10,-210,0)
,vec(50,-210,30),vec(50,-180,0),vec(50,-240,0)
,vec(50,-210,-30),vec(0,210,5),vec(50,180,0)
,vec(50,240,0),vec(50,210,30),vec(50,210,-30)
,vec(5,-3,-80),vec(-120,0,-20),vec(-180,0,150)
,vec(-180,0,155),vec(-180,164,155),vec(-180,-164,155)
,vec(0,0,0),vec(0,0,0),vec(0,0,0)
,vec(0,0,0),vec(0,0,0),vec(0,0,0)
,vec(0,0,0),vec(0,0,0),vec(0,0,0)
,vec(0,0,0),vec(0,0,0),vec(0,0,0)
,vec(0,0,0),vec(0,0,0),vec(0,0,0)

)
Ang=array(
ang(0,0,90),ang(0,0,90),ang(0,180,90),
ang(180,0,90),ang(0,0,90),ang(0,0,90),
ang(90,0,90),ang(90,0,90),ang(0,0,90),
ang(0,90,90),ang(0,90,90),ang(0,0,90),
ang(0,0,90),ang(0,0,0),ang(0,0,0)
,ang(0,0,180),ang(0,180,180),ang(0,90,90)
,ang(0,90,90),ang(0,90,90),ang(90,0,0)
,ang(0,0,0),ang(0,0,180),ang(0,90,90)
,ang(0,-90,-90),ang(0,-90,-90),ang(0,-90,-90)
,ang(0,-90,-90),ang(0,90,90),ang(0,-90,-90)
,ang(0,-90,-90),ang(0,-90,-90),ang(0,-90,-90)
,ang(0,0,0),ang(90,0,0),ang(-90,0,0)
,ang(0,0,0),ang(0,0,0),ang(0,0,180)
,ang(0,0,0),ang(0,0,0),ang(0,0,0)
,ang(0,0,0),ang(0,0,0),ang(0,0,0)
,ang(0,0,0),ang(0,0,0),ang(0,0,0)
,ang(0,0,0),ang(0,0,0),ang(0,0,0)

)    

}
Pos=holoEntity(1):toWorld(vec(0,0,20))
timer("SPAWN",100)
if(changed(E)&E|clk("SPAWN")){

    for(P=1,Model:count()){
        if(!Props[P,entity]){
        Props[P,entity]=propSpawn(Model[P,string],Pos+Pos2[P,vector],Ang[P,angle],1)
        Props[P,entity]:parentTo(holoEntity(1))
        Props[P,entity]:setMaterial("phoenix_storms/gear")
        Props[P,entity]:setColor(vec(255,0,0))
        Props[24,entity]:propNotSolid(1)
        }
    }
    
}



if(last()){
 propDeleteAll()   
}
#####################
holoAng(1,Driver:eyeAngles())
Props[21,entity]:setAlpha(0)
Props[15,entity]:setAng(Driver:eyeAngles())
Props[14,entity]:setAng(Driver:eyeAngles())
   Seat:setAng(ang())
    Seat:propFreeze(1)
    #########
    
    #########DUESEN#########
    if(first()){
        
    }
    
    
    #######################
    
    
    
    if(W){
 Seat:setPos(Seat:pos()+Driver:eye()*15)       
}
   if(S){
 Seat:setPos(Seat:pos()+Driver:eye()*-15)       
}
    if(W&Shift){
 Seat:setPos(Seat:pos()+Driver:eye()*55)       
}
   if(S&Shift){
 Seat:setPos(Seat:pos()+Driver:eye()*-55)       
}
noCollideAll(Seat,1)

Seat:setAlpha(0)
timer("solide",150)
timer("reload",250)
if(Mouse1&clk("reload")){
 B1=propSpawn("models/props_phx/ww2bomb.mdl",(holoEntity(1):toWorld(vec(150,200,0))),0)
   B1:propGravity(0)
B1:setTrails(30,0,10,"trails/smoke",vec(50,50,50),255)
B1:applyForce(holoEntity(1):forward()*B1:mass()*5000)
B1:setAng(Driver:eyeAngles())
}
if(Mouse2&clk("reload")){
 B2=propSpawn("models/props_phx/ww2bomb.mdl",(holoEntity(1):toWorld(vec(150,-200,0))),0)
   B2:propGravity(0)
B2:setTrails(30,0,10,"trails/smoke",vec(50,50,50),255)
B2:applyForce(holoEntity(1):forward()*B2:mass()*5000)
B2:setAng(Driver:eyeAngles())
} if(Driver:name()!=owner():name()){Seat:killPod()   }

#########egp############
timer("egp",150)
if(changed(E)&E|clk("egp")){
 
for(Pa=1,players():count()){
Ply=players()[Pa,entity] 
   Egp:egp3DTracker(Pa+100,Ply:pos())
Egp:egpText(Pa+5,Pa+" "+Ply:name(),vec2())
Egp:egpSize(Pa+5,20)
Egp:egpParent(Pa+5,Pa+100)

if(Ply:isAlive()){Egp:egpColor(Pa+5,vec(255,255,0))}else{Egp:egpColor(Pa+5,vec(255,0,0))}
}
    
}

######################
Screen="
Made by Sky 
"
Last=lastSaid():explode(" ")
if(chatClk(owner())&Last:string(1)=="/spawn"){
    hideChat(1)
 Plate=propSpawn("models/hunter/plates/plate24x24.mdl",(Seat:toWorld(vec(0,0,-30))),ang(),1)   
    Plate:setMaterial("phoenix_storms/gear")
    Plate:setColor(vec(25,50,25))
    Plate:setAlpha(150)
    Plate:parentTo(Seat)
}
if(chatClk(owner())&Last:string(1)=="/remove"){
    hideChat(1)
Plate:propDelete()
}

#########################

R=Driver:keyReload()
if(Come){
    Seat:killPod()
}
if(changed(R)&R){
 Ball=propSpawn("models/hunter/misc/sphere375x375.mdl",(holoEntity(1):toWorld(vec(350,0,0))),0)
Ball:setTrails(30,0,2,"trails/electric",vec(25,88,25),255)
Ball:setMaterial("models/vortigaunt/pupil")
Ball:setColor(25,88,25)
Ball:setMass(50000)
Ball:propGravity(0)
print("Ball has been spawned wait for the remove befor spawning a new one !")
}
Ball:applyForce(Ball:forward()*Ball:mass()*15000)
Ball:setAng(Driver:eyeAngles())
timer("remove",5500)
if(clk("remove")){
 Ball:propDelete()  
}
if(changed(!Ball)&!Ball){ 
print("Ball has been removed")
}
