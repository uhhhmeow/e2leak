@name Laser
@inputs [Ent1 Ent2 Ent3 Ent4 Ent5 Ent6]:entity 
@persist [O]:entity [Arr S]:array Modus
@outputs Arr:array Target:entity
if(first()|duped()|dupefinished()|Ent1|Ent2|Ent3|Ent4|Ent5|Ent6){
    runOnChat(1)
    runOnTick(1)
    O=owner()
#    entity():setText("Laser")
    noCollideAll(Ent1,1)
    noCollideAll(Ent2,1)
    noCollideAll(Ent3,1)
    noCollideAll(Ent4,1)
    noCollideAll(Ent5,1)
    noCollideAll(Ent6,1)
    Ent1:propFreeze(1)
    Ent2:propFreeze(1)
    Ent3:propFreeze(1)
    Ent4:propFreeze(1)
    Ent5:propFreeze(1)
    Ent1:propNotSolid(1)
    Ent2:propNotSolid(1)
    Ent3:propNotSolid(1)
    Ent4:propNotSolid(1)
    Ent5:propNotSolid(1)
    Ent6:propNotSolid(1)
    Ent1:setAlpha(0)
    Ent2:setAlpha(0)
    Ent3:setAlpha(0)
    Ent4:setAlpha(0)
    Ent5:setAlpha(0)
    Arr=array(owner())
}

for(P=1,players():count()) {
Spieler=players()[P,entity]
if(Spieler:aimEntity()==owner()) {
Ent1:setAng((Spieler:pos()-Ent1:pos()+vec(0,0,5)):toAngle()+ang(90,0,0))
Ent2:setAng((Spieler:pos()-Ent2:pos()+vec(0,0,5)):toAngle()+ang(90,0,0))
Ent3:setAng((Spieler:pos()-Ent3:pos()+vec(0,0,5)):toAngle()+ang(90,0,0))
Ent4:setAng((Spieler:pos()-Ent4:pos()+vec(0,0,5)):toAngle()+ang(90,0,0))
Ent5:setAng((Spieler:pos()-Ent5:pos()+vec(0,0,5)):toAngle()+ang(90,0,0))
Ent6:setAng((Spieler:pos()-Ent6:pos()+vec(0,0,5)):toAngle()+ang(90,0,0))
Ent1:pewFire(1)
Ent2:pewFire(1)
Ent3:pewFire(1)
Ent4:pewFire(1)
Ent5:pewFire(1)
Ent6:pewFire(1)
}
elseif(Spieler:aimEntity()!=owner()) {
    Ent1:pewFire(0)
    Ent2:pewFire(0)
    Ent3:pewFire(0)
    Ent4:pewFire(0)
    Ent5:pewFire(0)
    Ent6:pewFire(0)
}
}
Ent1:setPos(owner():pos()+vec(20,20,120))
Ent2:setPos(owner():pos()+vec(-20,-20,120))
Ent3:setPos(owner():pos()+vec(-20,20,120))
Ent4:setPos(owner():pos()+vec(20,-20,120))
Ent5:setPos(owner():pos()+vec(0,0,120))
Ent6:setPos(owner():pos()+vec(0,0,150))


if((owner():keyPressed("t"))){
    Ent1:setAng((owner():aimPos()-Ent1:pos()):toAngle()+ang(90,0,0))
    Ent2:setAng((owner():aimPos()-Ent2:pos()):toAngle()+ang(90,0,0))
    Ent3:setAng((owner():aimPos()-Ent3:pos()):toAngle()+ang(90,0,0))
    Ent4:setAng((owner():aimPos()-Ent4:pos()):toAngle()+ang(90,0,0))
    Ent5:setAng((owner():aimPos()-Ent5:pos()):toAngle()+ang(90,0,0))
    Ent6:setAng((owner():aimPos()-Ent5:pos()):toAngle()+ang(90,0,0))
    Ent1:pewFire(1)
    Ent2:pewFire(1)
    Ent3:pewFire(1)
    Ent4:pewFire(1)
    Ent5:pewFire(1)
    Ent6:pewFire(1)
}

#if(owner():steamID()!="STEAM_0:0:58329845"){
#hint("THIS IS NOT YOURS !!!!",999)
#selfDestructAll()
#}

if(chatClk(owner())&owner():lastSaid()=="/hide") {
hideChat(1)
Modus=1
print("Du bist nun unsichtbar")
}
if(Modus==1) {
owner():weapon():setMaterial("effects/ar2_altfire1b")
owner():setMaterial("effects/ar2_altfire1b")
}
if(chatClk(owner())&owner():lastSaid()=="/show") {
hideChat(1)
Modus=0
print("Du bist wieder sichtbar")
}
if(Modus==0) {

owner():weapon():setMaterial("")
owner():setMaterial("")
}
@name Laser
@inputs [Ent1 Ent2 Ent3 Ent4 Ent5 Ent6]:entity 
@persist [O]:entity [Arr S]:array Modus
@outputs Arr:array Target:entity
if(first()|duped()|dupefinished()|Ent1|Ent2|Ent3|Ent4|Ent5|Ent6){
    runOnChat(1)
    runOnTick(1)
    O=owner()
#    entity():setText("Laser")
    noCollideAll(Ent1,1)
    noCollideAll(Ent2,1)
    noCollideAll(Ent3,1)
    noCollideAll(Ent4,1)
    noCollideAll(Ent5,1)
    noCollideAll(Ent6,1)
    Ent1:propFreeze(1)
    Ent2:propFreeze(1)
    Ent3:propFreeze(1)
    Ent4:propFreeze(1)
    Ent5:propFreeze(1)
    Ent1:propNotSolid(1)
    Ent2:propNotSolid(1)
    Ent3:propNotSolid(1)
    Ent4:propNotSolid(1)
    Ent5:propNotSolid(1)
    Ent6:propNotSolid(1)
    Ent1:setAlpha(0)
    Ent2:setAlpha(0)
    Ent3:setAlpha(0)
    Ent4:setAlpha(0)
    Ent5:setAlpha(0)
    Arr=array(owner())
}

for(P=1,players():count()) {
Spieler=players()[P,entity]
if(Spieler:aimEntity()==owner()) {
Ent1:setAng((Spieler:pos()-Ent1:pos()+vec(0,0,5)):toAngle()+ang(90,0,0))
Ent2:setAng((Spieler:pos()-Ent2:pos()+vec(0,0,5)):toAngle()+ang(90,0,0))
Ent3:setAng((Spieler:pos()-Ent3:pos()+vec(0,0,5)):toAngle()+ang(90,0,0))
Ent4:setAng((Spieler:pos()-Ent4:pos()+vec(0,0,5)):toAngle()+ang(90,0,0))
Ent5:setAng((Spieler:pos()-Ent5:pos()+vec(0,0,5)):toAngle()+ang(90,0,0))
Ent6:setAng((Spieler:pos()-Ent6:pos()+vec(0,0,5)):toAngle()+ang(90,0,0))
Ent1:pewFire(1)
Ent2:pewFire(1)
Ent3:pewFire(1)
Ent4:pewFire(1)
Ent5:pewFire(1)
Ent6:pewFire(1)
}
elseif(Spieler:aimEntity()!=owner()) {
    Ent1:pewFire(0)
    Ent2:pewFire(0)
    Ent3:pewFire(0)
    Ent4:pewFire(0)
    Ent5:pewFire(0)
    Ent6:pewFire(0)
}
}
Ent1:setPos(owner():pos()+vec(20,20,120))
Ent2:setPos(owner():pos()+vec(-20,-20,120))
Ent3:setPos(owner():pos()+vec(-20,20,120))
Ent4:setPos(owner():pos()+vec(20,-20,120))
Ent5:setPos(owner():pos()+vec(0,0,120))
Ent6:setPos(owner():pos()+vec(0,0,150))


if((owner():keyPressed("t"))){
    Ent1:setAng((owner():aimPos()-Ent1:pos()):toAngle()+ang(90,0,0))
    Ent2:setAng((owner():aimPos()-Ent2:pos()):toAngle()+ang(90,0,0))
    Ent3:setAng((owner():aimPos()-Ent3:pos()):toAngle()+ang(90,0,0))
    Ent4:setAng((owner():aimPos()-Ent4:pos()):toAngle()+ang(90,0,0))
    Ent5:setAng((owner():aimPos()-Ent5:pos()):toAngle()+ang(90,0,0))
    Ent6:setAng((owner():aimPos()-Ent5:pos()):toAngle()+ang(90,0,0))
    Ent1:pewFire(1)
    Ent2:pewFire(1)
    Ent3:pewFire(1)
    Ent4:pewFire(1)
    Ent5:pewFire(1)
    Ent6:pewFire(1)
}

#if(owner():steamID()!="STEAM_0:0:58329845"){
#hint("THIS IS NOT YOURS !!!!",999)
#selfDestructAll()
#}

if(chatClk(owner())&owner():lastSaid()=="/hide") {
hideChat(1)
Modus=1
print("Du bist nun unsichtbar")
}
if(Modus==1) {
owner():weapon():setMaterial("effects/ar2_altfire1b")
owner():setMaterial("effects/ar2_altfire1b")
}
if(chatClk(owner())&owner():lastSaid()=="/show") {
hideChat(1)
Modus=0
print("Du bist wieder sichtbar")
}
if(Modus==0) {

owner():weapon():setMaterial("")
owner():setMaterial("")
}
