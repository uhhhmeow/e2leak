@name SpectreFusion Holo Gunship

@inputs Pod:wirelink Cam:wirelink

@persist GunSec Flir CamTog Ranger:ranger CamPos:vector Bullet:entity 

@model models/hunter/blocks/cube025x025x025.mdl



#Run 20x per second

interval(1)

    

#Assign entity

E = entity()

    

#Primary Controls

E:applyForce(E:forward()*E:mass()*300*(Pod["W",number]-Pod["S",number]))

E:applyForce(E:up()*E:mass()*80*(Pod["Space",number]-Pod["Shift",number]))

E:applyAngForce(ang(0,1,0)*E:mass()*20*(Pod["A",number]-Pod["D",number]))



#Speed and Angle Stability

E:applyForce(-E:vel()*E:mass()*0.3)

E:applyAngForce(-E:angVel()*E:mass()*0.6)

E:applyAngForce((ang(0,E:angles():yaw(),0)-E:angles())*E:mass()*5)

  

#Camera Position Toggle

if(changed(Pod["Mouse2",number])&Pod["Mouse2",number]){CamTog = CamTog + 1}

if(CamTog >= 2){CamTog = 0}

if(CamTog == 0){CamPos = E:pos()+E:forward()*-550+E:up()*30}

if(CamTog == 1){CamPos = E:pos()+E:forward()*50+E:up()*350}

    

#Camera

Cam["Position",vector] = CamPos

Cam["Activated",number] = Pod["Active",number]

Cam["Angle",angle] = Pod["Entity",entity]:driver():eyeAngles()

    

#Camera FLIR Vision

if(changed(Pod["R",number])&Pod["R",number]){Flir = Flir + 1}

if(Flir >= 2){Flir = 0}

Cam["FLIR",number] = Flir

    

#Turret Timer

GunSec++



#Turret Fire

if(Pod["Mouse1",number] & changed(int(GunSec/5))){

    Ranger = rangerOffset(CamPos,CamPos+((vec(25000,0,0)):rotate(Pod["Entity",entity]:driver():eyeAngles())))

    Bullet:propBreak()

    Bullet = propSpawn("models/props_phx/mk-82.mdl",Ranger:pos()+vec(0,0,50),ang(90,0,0),1)

    Bullet:setAlpha(0)

}

Bullet:propBreak()

    

#Hologram Creation

if(first()){

    E:propGravity(0)

    E:soundPlay(1,0,"vehicles/apc/apc_idle1.wav")

    #MIDDLE

    #Main Body Box

    holoCreate(1,E:pos()+vec(0,0,48),vec(14,9,9),ang(0,0,0),vec(100,100,10),"cube")

    holoCreate(2,E:pos()+vec(40,0,46),vec(14,8,8),ang(0,0,0),vec(100,40,90),"cube")
    
    holoCreate(3,E:pos()+vec(10,0,100),vec(30,1,5),ang(0,90,90),vec(10,100,180),"cube")

    holoCreate(4,E:pos()+vec(30,0,20),vec(30,1,5),ang(0,90,90),vec(100,20,100),"cube")
  
    holoCreate(5,E:pos()+vec(30,0,40),vec(30,1,8),ang(0,0,90),vec(100,40,1),"cube")

    holoCreate(6,E:pos()+vec(150,0,70),vec(10,1,7),ang(30,0,90),vec(10,100,100),"cube")

    holoParent(1,E)

    holoParent(2,E)

    holoParent(3,E)

    holoParent(4,E)

    holoParent(5,E)

    holoParent(6,E)

    holoParent(7,E)

    holoParent(8,E)

    holoParent(9,E)

    holoParent(10,E)

    holoParent(11,E)

    holoParent(12,E)

    holoParent(13,E)

    holoParent(14,E)

    holoParent(15,E)

    holoParent(16,E)

    holoParent(17,E)

    holoParent(18,E)

    holoParent(19,E)

    holoParent(20,E)

    holoParent(21,E)

    holoParent(22,E)

    holoParent(23,E)

    holoParent(24,E)

    holoParent(25,E)

    holoParent(26,E)

    holoParent(27,E)

    holoParent(28,E)

    holoParent(29,E)

    holoParent(30,E)

    holoParent(31,E)

    holoParent(32,E)

    holoParent(33,E)

    holoParent(34,E)

    holoParent(35,E)

    holoParent(36,E)

    holoParent(37,E)

    holoParent(38,E)

    holoParent(39,E)

    holoParent(40,E)

    holoParent(41,E)

    holoParent(42,E)

}

@name SpectreFusion Holo Gunship

@inputs Pod:wirelink Cam:wirelink

@persist GunSec Flir CamTog Ranger:ranger CamPos:vector Bullet:entity 

@model models/hunter/blocks/cube025x025x025.mdl



#Run 20x per second

interval(1)

    

#Assign entity

E = entity()

    

#Primary Controls

E:applyForce(E:forward()*E:mass()*300*(Pod["W",number]-Pod["S",number]))

E:applyForce(E:up()*E:mass()*80*(Pod["Space",number]-Pod["Shift",number]))

E:applyAngForce(ang(0,1,0)*E:mass()*20*(Pod["A",number]-Pod["D",number]))



#Speed and Angle Stability

E:applyForce(-E:vel()*E:mass()*0.3)

E:applyAngForce(-E:angVel()*E:mass()*0.6)

E:applyAngForce((ang(0,E:angles():yaw(),0)-E:angles())*E:mass()*5)

  

#Camera Position Toggle

if(changed(Pod["Mouse2",number])&Pod["Mouse2",number]){CamTog = CamTog + 1}

if(CamTog >= 2){CamTog = 0}

if(CamTog == 0){CamPos = E:pos()+E:forward()*-550+E:up()*30}

if(CamTog == 1){CamPos = E:pos()+E:forward()*50+E:up()*350}

    

#Camera

Cam["Position",vector] = CamPos

Cam["Activated",number] = Pod["Active",number]

Cam["Angle",angle] = Pod["Entity",entity]:driver():eyeAngles()

    

#Camera FLIR Vision

if(changed(Pod["R",number])&Pod["R",number]){Flir = Flir + 1}

if(Flir >= 2){Flir = 0}

Cam["FLIR",number] = Flir

    

#Turret Timer

GunSec++



#Turret Fire

if(Pod["Mouse1",number] & changed(int(GunSec/5))){

    Ranger = rangerOffset(CamPos,CamPos+((vec(25000,0,0)):rotate(Pod["Entity",entity]:driver():eyeAngles())))

    Bullet:propBreak()

    Bullet = propSpawn("models/props_phx/mk-82.mdl",Ranger:pos()+vec(0,0,50),ang(90,0,0),1)

    Bullet:setAlpha(0)

}

Bullet:propBreak()

    

#Hologram Creation

if(first()){

    E:propGravity(0)

    E:soundPlay(1,0,"vehicles/apc/apc_idle1.wav")

    #MIDDLE

    #Main Body Box

    holoCreate(1,E:pos()+vec(0,0,48),vec(14,9,9),ang(0,0,0),vec(100,100,10),"cube")

    holoCreate(2,E:pos()+vec(40,0,46),vec(14,8,8),ang(0,0,0),vec(100,40,90),"cube")
    
    holoCreate(3,E:pos()+vec(10,0,100),vec(30,1,5),ang(0,90,90),vec(10,100,180),"cube")

    holoCreate(4,E:pos()+vec(30,0,20),vec(30,1,5),ang(0,90,90),vec(100,20,100),"cube")
  
    holoCreate(5,E:pos()+vec(30,0,40),vec(30,1,8),ang(0,0,90),vec(100,40,1),"cube")

    holoCreate(6,E:pos()+vec(150,0,70),vec(10,1,7),ang(30,0,90),vec(10,100,100),"cube")

    holoParent(1,E)

    holoParent(2,E)

    holoParent(3,E)

    holoParent(4,E)

    holoParent(5,E)

    holoParent(6,E)

    holoParent(7,E)

    holoParent(8,E)

    holoParent(9,E)

    holoParent(10,E)

    holoParent(11,E)

    holoParent(12,E)

    holoParent(13,E)

    holoParent(14,E)

    holoParent(15,E)

    holoParent(16,E)

    holoParent(17,E)

    holoParent(18,E)

    holoParent(19,E)

    holoParent(20,E)

    holoParent(21,E)

    holoParent(22,E)

    holoParent(23,E)

    holoParent(24,E)

    holoParent(25,E)

    holoParent(26,E)

    holoParent(27,E)

    holoParent(28,E)

    holoParent(29,E)

    holoParent(30,E)

    holoParent(31,E)

    holoParent(32,E)

    holoParent(33,E)

    holoParent(34,E)

    holoParent(35,E)

    holoParent(36,E)

    holoParent(37,E)

    holoParent(38,E)

    holoParent(39,E)

    holoParent(40,E)

    holoParent(41,E)

    holoParent(42,E)

}

