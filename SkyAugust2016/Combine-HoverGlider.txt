@name Combine-HoverGlider
@inputs Cam:wirelink
@outputs 
@persist E:entity Seat:entity Seat2:entity  Parent1 Start Move Rmove Umove P1:entity P2:entity
@trigger 
@model models/hunter/blocks/cube2x2x025.mdl
interval(30)
runOnLast(1)

E=entity()
#if(!E){
 #E = propSpawn("models/hunter/blocks/cube2x2x025.mdl",entity():pos()+vec(0,0,100),1)   
#}

  
if(changed(E)){
    function vector entity:setp(Vector134:vector){
     This:setPos(Vector134)   
    }
    function angle entity:seta(Angle134:angle){
     This:setAng(Angle134)   
    }
    function number holoa(IndexH1:number,AngleH:angle){
     holoAng(IndexH1,AngleH)   
    }
    function number holop(IndexH2:number,VectorH:vector){
     holoPos(IndexH2,VectorH)
    }
function entity c(Index:number,Posi:vector,Scale:vector,Angle:angle,Colo:vector,Model:string,Parent:entity,Alpha:number){
            holoCreate(Index)
            holoPos(Index,Posi) 
            holoScale(Index,Scale) 
            holoAng(Index,Angle) 
            holoColor(Index,Colo) 
            holoModel(Index,Model) 
            holoParent(Index,Parent) holoAlpha(Index,Alpha)
return holoEntity(Index)  
}
    function angle entity:applya(AngToRot:angle,VelO:number){
       This:applyAngForce(AngToRot)
       This:applyAngForce(-This:angVel()*E:mass()*VelO)   
    }
    function vector entity:applyf(VecToMove:vector,VelO2:number){
     This:applyForce(VecToMove)
     This:applyForce(-This:vel()*E:mass()*VelO2)   
    }
 


c(1,E:toWorld(vec(0,25,0)),vec(0.3,1,0.5),E:toWorld(ang(80,90,0)),vec(255),"models/props_combine/combine_barricade_med01a.mdl",E,255)
c(2,E:toWorld(vec(0,-25,0)),vec(0.3,1,0.5),E:toWorld(ang(80,-90,0)),vec(255),"models/props_combine/combine_barricade_med01a.mdl",E,255)
c(3,E:toWorld(vec(-10,0,-2)),vec(0.4,0.6,1),E:toWorld(ang(90,0,0)),vec(255),"models/props_combine/combine_barricade_med01a.mdl",E,255)

c(4,E:toWorld(vec(10,0,15)),vec(0.4,0.6,0.7),E:toWorld(ang(-100,180,0)),vec(255),"models/props_combine/combine_barricade_med02a.mdl",E,255)

c(5,E:toWorld(vec(0,40,15)),vec(2.4,2.6,6.7),E:toWorld(ang(-90,180,0)),vec(255),"models/items/combine_rifle_ammo01.mdl",E,255)
c(6,E:toWorld(vec(0,-40,15)),vec(2.4,2.6,6.7),E:toWorld(ang(-90,180,0)),vec(255),"models/items/combine_rifle_ammo01.mdl",E,255)

c(7,E:toWorld(vec(-25,40,15)),vec(0.2,0.4,0.2),E:toWorld(ang(0,90,0)),vec(255),"models/props_combine/combine_booth_short01a.mdl",E,255)
c(8,E:toWorld(vec(-25,-40,15)),vec(0.2,0.4,0.2),E:toWorld(ang(0,-90,0)),vec(255),"models/props_combine/combine_booth_short01a.mdl",E,255)

c(9,E:toWorld(vec(25,-40,5)),vec(1),E:toWorld(ang(90,0,0)),vec(255),"models/props_combine/combine_fence01a.mdl",E,255)
c(10,E:toWorld(vec(25,40,5)),vec(1),E:toWorld(ang(90,0,0)),vec(255),"models/props_combine/combine_fence01b.mdl",E,255)

c(11,E:toWorld(vec(-25,-20,25)),vec(0.5),E:toWorld(ang(180,0,0)),vec(255),"models/props_combine/combine_barricade_med01a.mdl",E,255)
c(12,E:toWorld(vec(-25,20,25)),vec(0.5),E:toWorld(ang(180,0,0)),vec(255),"models/props_combine/combine_barricade_med01a.mdl",E,255)
c(13,E:toWorld(vec(-30,0,30)),vec(0.6),E:toWorld(ang(180,0,0)),vec(255),"models/props_combine/combine_barricade_med01a.mdl",E,255)

c(14,E:toWorld(vec(-0,-55,5)),vec(1.5,1.5,2.5),E:toWorld(ang(0,0,90)),vec(255),"models/props_combine/combine_emitter01.mdl",E,255)
c(15,E:toWorld(vec(-0,55,5)),vec(1.5,1.5,2.5),E:toWorld(ang(0,0,-90)),vec(255),"models/props_combine/combine_emitter01.mdl",E,255)

c(16,E:toWorld(vec(-30,-35,5)),vec(1),E:toWorld(ang(0,0,0)),vec(55),"models/xqm/jetenginemedium.mdl",E,255)
c(17,E:toWorld(vec(-30,35,5)),vec(1),E:toWorld(ang(0,0,0)),vec(55),"models/xqm/jetenginemedium.mdl",E,255)

c(18,E:toWorld(vec(50,-15,-5)),vec(2,1,1),E:toWorld(ang(0,180,0)),vec(55),"models/xqm/jetengine.mdl",E,255)
c(19,E:toWorld(vec(50,15,-5)),vec(2,1,1),E:toWorld(ang(0,180,0)),vec(55),"models/xqm/jetengine.mdl",E,255)

c(20,E:toWorld(vec(20,-45,0)),vec(2,1,1),E:toWorld(ang(0,180,0)),vec(55),"models/xqm/jetengine.mdl",E,255)
c(21,E:toWorld(vec(20,45,0)),vec(2,1,1),E:toWorld(ang(0,180,0)),vec(55),"models/xqm/jetengine.mdl",E,255)

c(22,E:toWorld(vec(-80,-35,5)),vec(1,1,1),E:toWorld(ang(0,0,0)),vec(22555),"0",E,0)
c(23,E:toWorld(vec(-80,35,5)),vec(1,1,1),E:toWorld(ang(0,0,0)),vec(255),"0",E,0)
holoEntity(22):setTrails(50,0,1,"trails/smoke",vec(0),255)
holoEntity(23):setTrails(50,0,1,"trails/smoke",vec(0),255)


c(24,E:toWorld(vec(-10,-105,5)),vec(1,1,1),E:toWorld(ang(0,0,0)),vec(22555),"0",E,0)
c(25,E:toWorld(vec(-10,105,5)),vec(1,1,1),E:toWorld(ang(0,0,0)),vec(255),"0",E,0)
}

if(!Seat){
  findByClass("prop_vehicle_*") 
         findSortByDistance(E:pos()) 
         if(find():owner()==owner()){
         Seat = find()  
       }
}
                                                                                                                                                                                                                                                                                           Driver = Seat:driver()      if(changed(Driver)&Driver){ Seat:printDriver("   This E2 plane is made by SKY ( http://steamcommunity.com/id/nice_guy_E2P/ ) !!! do not remove this hint or anything !")  }
if(changed(Seat)&Seat){
 print("Seat 1 found "+owner():name()+"!")   
}
if(!Seat){
 Parent1 = 0   
}
if(Parent1==0){
 Seat:propFreeze(1)
Seat:setp(E:toWorld(vec(-30,0,10)))
Seat:seta(E:toWorld(ang(0,-90,0)))   
Seat:deparent()
}
if(changed(Seat)&Seat&Parent1 == 0){
 timer("parent",600)   
}
if(clk("parent")){
 stoptimer("parent")   
Parent1 = 1
#Seat:parentTo(E)
weld(Seat,E)
}

if(Seat){
    Driver = Seat:driver()
       W=Driver:keyForward()
       A=Driver:keyLeft()
       S=Driver:keyBack()
       D=Driver:keyRight()
       Space=Driver:keyJump()
       Shift=Driver:keySprint()
       Alt=Driver:keyWalk()
       M1=Driver:keyAttack1()
       M2=Driver:keyAttack2()
       R=Driver:keyReload()   
}


if(changed(Parent1==1)){
    E:setAlpha(0)
E:propFreeze(0)
Start = 1
}
if(changed(Start == 1)){
 E:propGravity(0)
E:setMass(5000000)   
}
if(Start == 1){

if(W|S){
Move = Move + 10
}
if(!W&!S){
    Move = 0
}
if(A|D){
 Rmove = Rmove + 5
}
if(!A&!D){
    Rmove = 0
}
if(Space|Alt){
 Umove = Umove + 5
}
if(!Space&!Alt){
    Umove = 0
}
E:applyf((E:up()*E:mass()*Umove*(Space-Alt))+(E:right()*E:mass()*Rmove*(D-A))+(E:forward()*E:mass()*Move*(W-S)),0.2) 
    local X = vec(E:right():dot(Driver:eye()),E:right():dot(Driver:eye()),0):y()
     local Z = vec(E:up():dot(Driver:eye()),0,E:up():dot(Driver:eye())):z()
 E:applya((ang(-Z*40,-X*40,X*20)*E:mass()*5)+((ang(0,E:angles():yaw(),0)-E:angles())*E:mass()*2),2)


Speed = E:vel():length() / 10
if(changed(Speed > 150)&Speed > 150){
    holoEntity(24):setTrails(50,0,1,"trails/laser",vec(0,255,255),255)
holoEntity(25):setTrails(50,0,1,"trails/laser",vec(0,255,255),255)
}
if(changed(Speed < 150)&Speed < 150){
        holoEntity(25):removeTrails()
    holoEntity(24):removeTrails()
}
}
timer("reload1",600)
if(M1&clk("reload1")&!M2){
    P1 = propSpawn("models/props_phx/ww2bomb.mdl",holoEntity(25):pos(),E:angles(),0)
    P1:propGravity(0)
    P1:applyForce(holoEntity(25):forward()*P1:mass()*500000000)
    P1:setMass(50000)
    P1:setTrails(50,0,1,"trails/smoke",vec(40),255)
}

timer("reload2",2000)
if(M2&clk("reload2")&!M1){
        P2 = propSpawn("models/props_phx/mk-82.mdl",holoEntity(24):pos(),E:angles(),0)
    P2:propGravity(0)
    P2:applyForce(holoEntity(24):forward()*P2:mass()*500000000)
    P2:setMass(50000)
    P2:setTrails(50,0,1,"trails/smoke",vec(40),255)
}
if(first()){
 soundPlay(1,9999999999999999,"ambient/machines/turbine_loop_2.wav")   
}
soundPitch(1,Speed)
if(changed(P1)&P1){
 soundPlay(2,1,"mvm/giant_common/giant_common_step_04.wav")   
}
if(changed(P2)&P2){
 soundPlay(2,2,"mvm/giant_demoman/giant_demoman_grenade_shoot.wav")   
}
if(Driver!=owner()){
 Seat:killPod()   
}
@name Combine-HoverGlider
@inputs Cam:wirelink
@outputs 
@persist E:entity Seat:entity Seat2:entity  Parent1 Start Move Rmove Umove P1:entity P2:entity
@trigger 
@model models/hunter/blocks/cube2x2x025.mdl
interval(30)
runOnLast(1)

E=entity()
#if(!E){
 #E = propSpawn("models/hunter/blocks/cube2x2x025.mdl",entity():pos()+vec(0,0,100),1)   
#}

  
if(changed(E)){
    function vector entity:setp(Vector134:vector){
     This:setPos(Vector134)   
    }
    function angle entity:seta(Angle134:angle){
     This:setAng(Angle134)   
    }
    function number holoa(IndexH1:number,AngleH:angle){
     holoAng(IndexH1,AngleH)   
    }
    function number holop(IndexH2:number,VectorH:vector){
     holoPos(IndexH2,VectorH)
    }
function entity c(Index:number,Posi:vector,Scale:vector,Angle:angle,Colo:vector,Model:string,Parent:entity,Alpha:number){
            holoCreate(Index)
            holoPos(Index,Posi) 
            holoScale(Index,Scale) 
            holoAng(Index,Angle) 
            holoColor(Index,Colo) 
            holoModel(Index,Model) 
            holoParent(Index,Parent) holoAlpha(Index,Alpha)
return holoEntity(Index)  
}
    function angle entity:applya(AngToRot:angle,VelO:number){
       This:applyAngForce(AngToRot)
       This:applyAngForce(-This:angVel()*E:mass()*VelO)   
    }
    function vector entity:applyf(VecToMove:vector,VelO2:number){
     This:applyForce(VecToMove)
     This:applyForce(-This:vel()*E:mass()*VelO2)   
    }
 


c(1,E:toWorld(vec(0,25,0)),vec(0.3,1,0.5),E:toWorld(ang(80,90,0)),vec(255),"models/props_combine/combine_barricade_med01a.mdl",E,255)
c(2,E:toWorld(vec(0,-25,0)),vec(0.3,1,0.5),E:toWorld(ang(80,-90,0)),vec(255),"models/props_combine/combine_barricade_med01a.mdl",E,255)
c(3,E:toWorld(vec(-10,0,-2)),vec(0.4,0.6,1),E:toWorld(ang(90,0,0)),vec(255),"models/props_combine/combine_barricade_med01a.mdl",E,255)

c(4,E:toWorld(vec(10,0,15)),vec(0.4,0.6,0.7),E:toWorld(ang(-100,180,0)),vec(255),"models/props_combine/combine_barricade_med02a.mdl",E,255)

c(5,E:toWorld(vec(0,40,15)),vec(2.4,2.6,6.7),E:toWorld(ang(-90,180,0)),vec(255),"models/items/combine_rifle_ammo01.mdl",E,255)
c(6,E:toWorld(vec(0,-40,15)),vec(2.4,2.6,6.7),E:toWorld(ang(-90,180,0)),vec(255),"models/items/combine_rifle_ammo01.mdl",E,255)

c(7,E:toWorld(vec(-25,40,15)),vec(0.2,0.4,0.2),E:toWorld(ang(0,90,0)),vec(255),"models/props_combine/combine_booth_short01a.mdl",E,255)
c(8,E:toWorld(vec(-25,-40,15)),vec(0.2,0.4,0.2),E:toWorld(ang(0,-90,0)),vec(255),"models/props_combine/combine_booth_short01a.mdl",E,255)

c(9,E:toWorld(vec(25,-40,5)),vec(1),E:toWorld(ang(90,0,0)),vec(255),"models/props_combine/combine_fence01a.mdl",E,255)
c(10,E:toWorld(vec(25,40,5)),vec(1),E:toWorld(ang(90,0,0)),vec(255),"models/props_combine/combine_fence01b.mdl",E,255)

c(11,E:toWorld(vec(-25,-20,25)),vec(0.5),E:toWorld(ang(180,0,0)),vec(255),"models/props_combine/combine_barricade_med01a.mdl",E,255)
c(12,E:toWorld(vec(-25,20,25)),vec(0.5),E:toWorld(ang(180,0,0)),vec(255),"models/props_combine/combine_barricade_med01a.mdl",E,255)
c(13,E:toWorld(vec(-30,0,30)),vec(0.6),E:toWorld(ang(180,0,0)),vec(255),"models/props_combine/combine_barricade_med01a.mdl",E,255)

c(14,E:toWorld(vec(-0,-55,5)),vec(1.5,1.5,2.5),E:toWorld(ang(0,0,90)),vec(255),"models/props_combine/combine_emitter01.mdl",E,255)
c(15,E:toWorld(vec(-0,55,5)),vec(1.5,1.5,2.5),E:toWorld(ang(0,0,-90)),vec(255),"models/props_combine/combine_emitter01.mdl",E,255)

c(16,E:toWorld(vec(-30,-35,5)),vec(1),E:toWorld(ang(0,0,0)),vec(55),"models/xqm/jetenginemedium.mdl",E,255)
c(17,E:toWorld(vec(-30,35,5)),vec(1),E:toWorld(ang(0,0,0)),vec(55),"models/xqm/jetenginemedium.mdl",E,255)

c(18,E:toWorld(vec(50,-15,-5)),vec(2,1,1),E:toWorld(ang(0,180,0)),vec(55),"models/xqm/jetengine.mdl",E,255)
c(19,E:toWorld(vec(50,15,-5)),vec(2,1,1),E:toWorld(ang(0,180,0)),vec(55),"models/xqm/jetengine.mdl",E,255)

c(20,E:toWorld(vec(20,-45,0)),vec(2,1,1),E:toWorld(ang(0,180,0)),vec(55),"models/xqm/jetengine.mdl",E,255)
c(21,E:toWorld(vec(20,45,0)),vec(2,1,1),E:toWorld(ang(0,180,0)),vec(55),"models/xqm/jetengine.mdl",E,255)

c(22,E:toWorld(vec(-80,-35,5)),vec(1,1,1),E:toWorld(ang(0,0,0)),vec(22555),"0",E,0)
c(23,E:toWorld(vec(-80,35,5)),vec(1,1,1),E:toWorld(ang(0,0,0)),vec(255),"0",E,0)
holoEntity(22):setTrails(50,0,1,"trails/smoke",vec(0),255)
holoEntity(23):setTrails(50,0,1,"trails/smoke",vec(0),255)


c(24,E:toWorld(vec(-10,-105,5)),vec(1,1,1),E:toWorld(ang(0,0,0)),vec(22555),"0",E,0)
c(25,E:toWorld(vec(-10,105,5)),vec(1,1,1),E:toWorld(ang(0,0,0)),vec(255),"0",E,0)
}

if(!Seat){
  findByClass("prop_vehicle_*") 
         findSortByDistance(E:pos()) 
         if(find():owner()==owner()){
         Seat = find()  
       }
}
                                                                                                                                                                                                                                                                                           Driver = Seat:driver()      if(changed(Driver)&Driver){ Seat:printDriver("   This E2 plane is made by SKY ( http://steamcommunity.com/id/nice_guy_E2P/ ) !!! do not remove this hint or anything !")  }
if(changed(Seat)&Seat){
 print("Seat 1 found "+owner():name()+"!")   
}
if(!Seat){
 Parent1 = 0   
}
if(Parent1==0){
 Seat:propFreeze(1)
Seat:setp(E:toWorld(vec(-30,0,10)))
Seat:seta(E:toWorld(ang(0,-90,0)))   
Seat:deparent()
}
if(changed(Seat)&Seat&Parent1 == 0){
 timer("parent",600)   
}
if(clk("parent")){
 stoptimer("parent")   
Parent1 = 1
#Seat:parentTo(E)
weld(Seat,E)
}

if(Seat){
    Driver = Seat:driver()
       W=Driver:keyForward()
       A=Driver:keyLeft()
       S=Driver:keyBack()
       D=Driver:keyRight()
       Space=Driver:keyJump()
       Shift=Driver:keySprint()
       Alt=Driver:keyWalk()
       M1=Driver:keyAttack1()
       M2=Driver:keyAttack2()
       R=Driver:keyReload()   
}


if(changed(Parent1==1)){
    E:setAlpha(0)
E:propFreeze(0)
Start = 1
}
if(changed(Start == 1)){
 E:propGravity(0)
E:setMass(5000000)   
}
if(Start == 1){

if(W|S){
Move = Move + 10
}
if(!W&!S){
    Move = 0
}
if(A|D){
 Rmove = Rmove + 5
}
if(!A&!D){
    Rmove = 0
}
if(Space|Alt){
 Umove = Umove + 5
}
if(!Space&!Alt){
    Umove = 0
}
E:applyf((E:up()*E:mass()*Umove*(Space-Alt))+(E:right()*E:mass()*Rmove*(D-A))+(E:forward()*E:mass()*Move*(W-S)),0.2) 
    local X = vec(E:right():dot(Driver:eye()),E:right():dot(Driver:eye()),0):y()
     local Z = vec(E:up():dot(Driver:eye()),0,E:up():dot(Driver:eye())):z()
 E:applya((ang(-Z*40,-X*40,X*20)*E:mass()*5)+((ang(0,E:angles():yaw(),0)-E:angles())*E:mass()*2),2)


Speed = E:vel():length() / 10
if(changed(Speed > 150)&Speed > 150){
    holoEntity(24):setTrails(50,0,1,"trails/laser",vec(0,255,255),255)
holoEntity(25):setTrails(50,0,1,"trails/laser",vec(0,255,255),255)
}
if(changed(Speed < 150)&Speed < 150){
        holoEntity(25):removeTrails()
    holoEntity(24):removeTrails()
}
}
timer("reload1",600)
if(M1&clk("reload1")&!M2){
    P1 = propSpawn("models/props_phx/ww2bomb.mdl",holoEntity(25):pos(),E:angles(),0)
    P1:propGravity(0)
    P1:applyForce(holoEntity(25):forward()*P1:mass()*500000000)
    P1:setMass(50000)
    P1:setTrails(50,0,1,"trails/smoke",vec(40),255)
}

timer("reload2",2000)
if(M2&clk("reload2")&!M1){
        P2 = propSpawn("models/props_phx/mk-82.mdl",holoEntity(24):pos(),E:angles(),0)
    P2:propGravity(0)
    P2:applyForce(holoEntity(24):forward()*P2:mass()*500000000)
    P2:setMass(50000)
    P2:setTrails(50,0,1,"trails/smoke",vec(40),255)
}
if(first()){
 soundPlay(1,9999999999999999,"ambient/machines/turbine_loop_2.wav")   
}
soundPitch(1,Speed)
if(changed(P1)&P1){
 soundPlay(2,1,"mvm/giant_common/giant_common_step_04.wav")   
}
if(changed(P2)&P2){
 soundPlay(2,2,"mvm/giant_demoman/giant_demoman_grenade_shoot.wav")   
}
if(Driver!=owner()){
 Seat:killPod()   
}
