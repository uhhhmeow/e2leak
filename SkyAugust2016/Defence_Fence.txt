@name Defence Fence
@model models/props_phx/games/chess/black_king.mdl
@inputs
@outputs Fire Range
@persist [O E Weapon]:entity [IO Fire Range]:number [Targets Autos]:array Model:string I TT Timer   Up RP:entity

if(first()|dupefinished()){
    runOnChat(1)
    E=entity()
    E:setMass(50000)
    O=owner()
   # if((owner():steamID())!="STEAM_0:0:21814766"){selfDestruct()} 
    #Model="models/props_phx/mk-82.mdl"
    #Model="models/props_junk/propane_tank001a.mdl"
    Model="models/props_phx/amraam.mdl"
    #Model="models/props_c17/oildrum001_explosive.mdl"
    Range=100
    IO=1
    Grad=45
holoCreate(1000,E:toWorld(vec(0,0,100)),vec(1,1,1),E:toWorld(ang()))

    holoCreate(0,E:toWorld(vec(0,0,50)),vec(1,1,1),E:toWorld(ang()))
    holoCreate(100,E:toWorld(vec(sin(Grad)*Range,cos(Grad)*Range,60)),vec(1,1,1),E:toWorld(ang()))
    holoCreate(101,E:toWorld(vec(sin(Grad*2)*Range,cos(Grad*2)*Range,40)),vec(1,1,1),E:toWorld(ang()))
    holoCreate(102,E:toWorld(vec(sin(Grad*3)*Range,cos(Grad*3)*Range,20)),vec(1,1,1),E:toWorld(ang()))
    holoCreate(103,E:toWorld(vec(sin(Grad*4)*Range,cos(Grad*4)*Range,0)),vec(1,1,1),E:toWorld(ang()))
    holoParent(0,E)
    
    holoParent(100,0)
    holoParent(101,0)
    holoParent(102,0)
    holoParent(103,0)
    
    holoAlpha(0,0)
    holoAlpha(100,0)
    holoAlpha(101,0)
    holoAlpha(102,0)
    holoAlpha(103,0)
    #Color=vec(50,255,50)
    #Color=vec(0,161,225)
    #pink
    Color=vec(255,0,161)
    ###############################
    #String="trails/laser"
    #String="trails/smoke"
    String="trails/plasma"
    #String="trails/electric"
    #String="trails/LOL"
    L=2
    holoEntity(100):setTrails(100,10,L,String,Color,255,1,6)
    holoEntity(101):setTrails(100,10,L,String,Color,255,2,3)
    holoEntity(102):setTrails(100,10,L,String,Color,255,3,3)
    holoEntity(103):setTrails(100,10,L,String,Color,255,4,3)
    
    
    
timer("holos",1000)
timer("array",100)
timer("arrayv",100)
timer("action",1000)
}
interval(100)
holoAng(0,holoEntity(0):toWorld(ang(0,5,0)))



#
# ARRAY
#
if(clk("array")){

findExcludePlayer(owner():name())
#findExcludePlayer("Konsti")
findExcludePlayer("Sky")
#findExcludePlayer("petrik2700")
findExcludePlayer("[FG] Im James")
findExcludePlayer("Alpha")
findExcludePlayer("Hodor")
findExcludePlayer("Toni")
#findExcludePlayer("pandal")


findIncludeClass("player")
#findIncludeClass("prop_vehicle_*")

findInSphere(E:pos(),Range*IO) 
Targets = findToArray()

timer("array",110)}

if(clk("arrayv")){
#findExcludePlayerProps(owner())
findIncludeClass("prop_vehicle_*")
findInSphere(E:pos(),Range*IO) 
Autos = findToArray()

timer("arrayv",110)}


if(Autos[1,entity]){
    
    if(Autos[1,entity]:driver():type()=="player"){
    Direction=(Autos[1,entity]:boxCenterW()-holoEntity(1000):pos()):toAngle()
    holoAng(1000,Direction)
}
else{holoAng(1000,E:toWorld(ang()))}
    if(Fire==0){Fire=1}
    if(Fire==1){Fire=0}
    }
    else{Fire=0}
#
# ACTION
#
if(clk("action")){

for(I=1,Targets:count()){
    Target=Targets:entity(I)
        if(Target:health()>0|Target:inVehicle()){
            
  
            #concmd("say @god "+Target:name()+" 0")
        #if(Target:health()>0&!Target:inVehicle()){
            Bomb=propSpawn(Model,Target:toWorld(vec(0,0,25)),Target:toWorld(ang(0,0,0)),1)
            Bomb:propBreak()  
        }
}

timer("action",1000)}
    

if(clk("holos")){
    Grad=30
    Color=vec(100,100,100)
    
    holoPos(100,E:toWorld(vec(sin(Grad)*Range,cos(Grad)*Range,60)))
    holoPos(101,E:toWorld(vec(sin(Grad*2)*Range,cos(Grad*2)*Range,40)))
    holoPos(102,E:toWorld(vec(sin(Grad*3)*Range,cos(Grad*3)*Range,20)))
    holoPos(103,E:toWorld(vec(sin(Grad*4)*Range,cos(Grad*4)*Range,0)))
    
    holoCreate(1,E:toWorld(vec(sin(Grad-30)*Range,cos(Grad-30)*Range,0)),vec(0.4,0.4,10),ang(),Color)
    holoCreate(2,E:toWorld(vec(sin(Grad)*Range,cos(Grad)*Range,0)),vec(0.4,0.4,10),ang(),Color)
    holoCreate(3,E:toWorld(vec(sin(Grad*2)*Range,cos(Grad*2)*Range,0)),vec(0.4,0.4,10),ang(),Color)
    holoCreate(4,E:toWorld(vec(sin(Grad*3)*Range,cos(Grad*3)*Range,0)),vec(0.4,0.4,10),ang(),Color)
    holoCreate(5,E:toWorld(vec(sin(Grad*4)*Range,cos(Grad*4)*Range,0)),vec(0.4,0.4,10),ang(),Color)
    holoCreate(6,E:toWorld(vec(sin(Grad*5)*Range,cos(Grad*5)*Range,0)),vec(0.4,0.4,10),ang(),Color)
    holoCreate(7,E:toWorld(vec(sin(Grad*6)*Range,cos(Grad*6)*Range,0)),vec(0.4,0.4,10),ang(),Color)
    holoCreate(8,E:toWorld(vec(sin(Grad*7)*Range,cos(Grad*7)*Range,0)),vec(0.4,0.4,10),ang(),Color)
    holoCreate(9,E:toWorld(vec(sin(Grad*8)*Range,cos(Grad*8)*Range,0)),vec(0.4,0.4,10),ang(),Color)
    holoCreate(10,E:toWorld(vec(sin(Grad*9)*Range,cos(Grad*9)*Range,0)),vec(0.4,0.4,10),ang(),Color)
    holoCreate(11,E:toWorld(vec(sin(Grad*10)*Range,cos(Grad*10)*Range,0)),vec(0.4,0.4,10),ang(),Color)
    holoCreate(12,E:toWorld(vec(sin(Grad*11)*Range,cos(Grad*11)*Range,0)),vec(0.4,0.4,10),ang(),Color)

timer("holos",120000)}

if(chatClk(owner())){
H=owner():lastSaid():explode(" ") 


if(H:string(1)=="fence") {hideChat(1)
    if(H:string(2)=="on"){IO=1}
if(H:string(2)=="off"){IO=0}}

if(H:string(1)=="load") {hideChat(1)
    if(H:string(2)=="mk"){Model="models/props_phx/mk-82.mdl"}
if(H:string(2)=="amran"){Model="models/props_phx/amraam.mdl"}
if(H:string(2)=="barrel"){Model="models/props_c17/oildrum001_explosive.mdl"}}

if(H:string(1)=="!range") {hideChat(1)
Range=H:string(2):toNumber(),stoptimer("holos"),timer("holos",1000)
}

if(H:string(1)=="!igniter") {hideChat(1)
    Weapon=owner():aimEntity(),timer("weapon",500),print("Weapon: "+Weapon:type())
    holoPos(1000,Weapon:pos()),holoAng(1000,Weapon:toWorld(ang(-90,0,0)))}

}

if(clk("weapon")){
    timer("weapon2",500)
    #Weapon:createWire(entity(),"Length","Fire")
    #Weapon:createWire(entity(),"A","Fire")
    Weapon:parentTo(holoEntity(1000))
}
if(clk("weapon2")){
    holoPos(1000,E:toWorld(vec(0,0,100)))

    
    
    
}


@name Defence Fence
@model models/props_phx/games/chess/black_king.mdl
@inputs
@outputs Fire Range
@persist [O E Weapon]:entity [IO Fire Range]:number [Targets Autos]:array Model:string I TT Timer   Up RP:entity

if(first()|dupefinished()){
    runOnChat(1)
    E=entity()
    E:setMass(50000)
    O=owner()
   # if((owner():steamID())!="STEAM_0:0:21814766"){selfDestruct()} 
    #Model="models/props_phx/mk-82.mdl"
    #Model="models/props_junk/propane_tank001a.mdl"
    Model="models/props_phx/amraam.mdl"
    #Model="models/props_c17/oildrum001_explosive.mdl"
    Range=100
    IO=1
    Grad=45
holoCreate(1000,E:toWorld(vec(0,0,100)),vec(1,1,1),E:toWorld(ang()))

    holoCreate(0,E:toWorld(vec(0,0,50)),vec(1,1,1),E:toWorld(ang()))
    holoCreate(100,E:toWorld(vec(sin(Grad)*Range,cos(Grad)*Range,60)),vec(1,1,1),E:toWorld(ang()))
    holoCreate(101,E:toWorld(vec(sin(Grad*2)*Range,cos(Grad*2)*Range,40)),vec(1,1,1),E:toWorld(ang()))
    holoCreate(102,E:toWorld(vec(sin(Grad*3)*Range,cos(Grad*3)*Range,20)),vec(1,1,1),E:toWorld(ang()))
    holoCreate(103,E:toWorld(vec(sin(Grad*4)*Range,cos(Grad*4)*Range,0)),vec(1,1,1),E:toWorld(ang()))
    holoParent(0,E)
    
    holoParent(100,0)
    holoParent(101,0)
    holoParent(102,0)
    holoParent(103,0)
    
    holoAlpha(0,0)
    holoAlpha(100,0)
    holoAlpha(101,0)
    holoAlpha(102,0)
    holoAlpha(103,0)
    #Color=vec(50,255,50)
    #Color=vec(0,161,225)
    #pink
    Color=vec(255,0,161)
    ###############################
    #String="trails/laser"
    #String="trails/smoke"
    String="trails/plasma"
    #String="trails/electric"
    #String="trails/LOL"
    L=2
    holoEntity(100):setTrails(100,10,L,String,Color,255,1,6)
    holoEntity(101):setTrails(100,10,L,String,Color,255,2,3)
    holoEntity(102):setTrails(100,10,L,String,Color,255,3,3)
    holoEntity(103):setTrails(100,10,L,String,Color,255,4,3)
    
    
    
timer("holos",1000)
timer("array",100)
timer("arrayv",100)
timer("action",1000)
}
interval(100)
holoAng(0,holoEntity(0):toWorld(ang(0,5,0)))



#
# ARRAY
#
if(clk("array")){

findExcludePlayer(owner():name())
#findExcludePlayer("Konsti")
findExcludePlayer("Sky")
#findExcludePlayer("petrik2700")
findExcludePlayer("[FG] Im James")
findExcludePlayer("Alpha")
findExcludePlayer("Hodor")
findExcludePlayer("Toni")
#findExcludePlayer("pandal")


findIncludeClass("player")
#findIncludeClass("prop_vehicle_*")

findInSphere(E:pos(),Range*IO) 
Targets = findToArray()

timer("array",110)}

if(clk("arrayv")){
#findExcludePlayerProps(owner())
findIncludeClass("prop_vehicle_*")
findInSphere(E:pos(),Range*IO) 
Autos = findToArray()

timer("arrayv",110)}


if(Autos[1,entity]){
    
    if(Autos[1,entity]:driver():type()=="player"){
    Direction=(Autos[1,entity]:boxCenterW()-holoEntity(1000):pos()):toAngle()
    holoAng(1000,Direction)
}
else{holoAng(1000,E:toWorld(ang()))}
    if(Fire==0){Fire=1}
    if(Fire==1){Fire=0}
    }
    else{Fire=0}
#
# ACTION
#
if(clk("action")){

for(I=1,Targets:count()){
    Target=Targets:entity(I)
        if(Target:health()>0|Target:inVehicle()){
            
  
            #concmd("say @god "+Target:name()+" 0")
        #if(Target:health()>0&!Target:inVehicle()){
            Bomb=propSpawn(Model,Target:toWorld(vec(0,0,25)),Target:toWorld(ang(0,0,0)),1)
            Bomb:propBreak()  
        }
}

timer("action",1000)}
    

if(clk("holos")){
    Grad=30
    Color=vec(100,100,100)
    
    holoPos(100,E:toWorld(vec(sin(Grad)*Range,cos(Grad)*Range,60)))
    holoPos(101,E:toWorld(vec(sin(Grad*2)*Range,cos(Grad*2)*Range,40)))
    holoPos(102,E:toWorld(vec(sin(Grad*3)*Range,cos(Grad*3)*Range,20)))
    holoPos(103,E:toWorld(vec(sin(Grad*4)*Range,cos(Grad*4)*Range,0)))
    
    holoCreate(1,E:toWorld(vec(sin(Grad-30)*Range,cos(Grad-30)*Range,0)),vec(0.4,0.4,10),ang(),Color)
    holoCreate(2,E:toWorld(vec(sin(Grad)*Range,cos(Grad)*Range,0)),vec(0.4,0.4,10),ang(),Color)
    holoCreate(3,E:toWorld(vec(sin(Grad*2)*Range,cos(Grad*2)*Range,0)),vec(0.4,0.4,10),ang(),Color)
    holoCreate(4,E:toWorld(vec(sin(Grad*3)*Range,cos(Grad*3)*Range,0)),vec(0.4,0.4,10),ang(),Color)
    holoCreate(5,E:toWorld(vec(sin(Grad*4)*Range,cos(Grad*4)*Range,0)),vec(0.4,0.4,10),ang(),Color)
    holoCreate(6,E:toWorld(vec(sin(Grad*5)*Range,cos(Grad*5)*Range,0)),vec(0.4,0.4,10),ang(),Color)
    holoCreate(7,E:toWorld(vec(sin(Grad*6)*Range,cos(Grad*6)*Range,0)),vec(0.4,0.4,10),ang(),Color)
    holoCreate(8,E:toWorld(vec(sin(Grad*7)*Range,cos(Grad*7)*Range,0)),vec(0.4,0.4,10),ang(),Color)
    holoCreate(9,E:toWorld(vec(sin(Grad*8)*Range,cos(Grad*8)*Range,0)),vec(0.4,0.4,10),ang(),Color)
    holoCreate(10,E:toWorld(vec(sin(Grad*9)*Range,cos(Grad*9)*Range,0)),vec(0.4,0.4,10),ang(),Color)
    holoCreate(11,E:toWorld(vec(sin(Grad*10)*Range,cos(Grad*10)*Range,0)),vec(0.4,0.4,10),ang(),Color)
    holoCreate(12,E:toWorld(vec(sin(Grad*11)*Range,cos(Grad*11)*Range,0)),vec(0.4,0.4,10),ang(),Color)

timer("holos",120000)}

if(chatClk(owner())){
H=owner():lastSaid():explode(" ") 


if(H:string(1)=="fence") {hideChat(1)
    if(H:string(2)=="on"){IO=1}
if(H:string(2)=="off"){IO=0}}

if(H:string(1)=="load") {hideChat(1)
    if(H:string(2)=="mk"){Model="models/props_phx/mk-82.mdl"}
if(H:string(2)=="amran"){Model="models/props_phx/amraam.mdl"}
if(H:string(2)=="barrel"){Model="models/props_c17/oildrum001_explosive.mdl"}}

if(H:string(1)=="!range") {hideChat(1)
Range=H:string(2):toNumber(),stoptimer("holos"),timer("holos",1000)
}

if(H:string(1)=="!igniter") {hideChat(1)
    Weapon=owner():aimEntity(),timer("weapon",500),print("Weapon: "+Weapon:type())
    holoPos(1000,Weapon:pos()),holoAng(1000,Weapon:toWorld(ang(-90,0,0)))}

}

if(clk("weapon")){
    timer("weapon2",500)
    #Weapon:createWire(entity(),"Length","Fire")
    #Weapon:createWire(entity(),"A","Fire")
    Weapon:parentTo(holoEntity(1000))
}
if(clk("weapon2")){
    holoPos(1000,E:toWorld(vec(0,0,100)))

    
    
    
}


