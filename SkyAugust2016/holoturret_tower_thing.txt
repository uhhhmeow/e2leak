@name holoturret
@inputs 
@outputs SCREEN:number
@persist Counter CC
@trigger 
@model models/props_phx/construct/metal_angle360.mdl
Counter++
CC++,+5000
E=entity()
if(first()){
runOnTick(1)
holoCreate(1,E:toWorld(vec(0,0,3)),vec(6,6,1),ang(0,0,0),vec(75,75,75),"hqcylinder")   
   holoCreate(2,E:toWorld(vec(0,0,5)),vec(0.8,0.8,0.1),ang(0,0,0),vec(75,75,75),"models/hunter/tubes/tube2x2x025.mdl")    
  holoCreate(3,E:toWorld(vec(0,0,16)),vec(0.9,0.9,0.2),ang(0,0,0),vec(100,0,100),"models/hunter/tubes/tube2x2x025.mdl")    
 holoCreate(4,E:toWorld(vec(0,0,16)),vec(0.7,0.7,0.2),ang(0,0,0),vec(0,0,50),"models/props_phx/construct/metal_angle360.mdl")  
 holoCreate(5,E:toWorld(vec(0,0,46)),vec(3.7,3.7,5.2),ang(0,0,0),vec(255,0,100),"hqcylinder")  
 holoCreate(6,E:toWorld(vec(0,0,77)),vec(0.47,0.47,0.45),ang(0,0,0),vec(55,0,100),"models/props_phx/construct/metal_dome360.mdl")  
 holoCreate(7,E:toWorld(vec(0,0,85)),vec(1,1,1),ang(0,0,0),vec(0,255,100),"icosphere3")  
 holoCreate(8,E:toWorld(vec(0,0,85)),vec(0.6,0.6,0.6),ang(0,0,0),vec(0,255,100),"models/hunter/tubes/tube2x2x05.mdl")  
holoParent(8,E)
}
################
holoMaterial(8,"models/alyx/emptool_glow")
holoMaterial(7,"phoenix_storms/gear")
holoMaterial(6,"phoenix_storms/gear")
holoMaterial(5,"phoenix_storms/gear")
holoMaterial(4,"phoenix_storms/gear")
holoMaterial(2,"phoenix_storms/gear")
holoMaterial(3,"phoenix_storms/gear")
################
holoParent(7,E)
holoParent(6,E)
holoParent(5,E)
holoParent(4,E)
holoParent(3,E)
holoParent(2,E)
holoParent(1,E)
################
holoAlpha(6,150)
holoAng(7,ang()+Counter*2.5)
holoPos(8,holoEntity(5):pos()+vec(0,0,cos(CC+250)*28))
SCREEN=cos(Counter)*28
@name holoturret
@inputs 
@outputs SCREEN:number
@persist Counter CC
@trigger 
@model models/props_phx/construct/metal_angle360.mdl
Counter++
CC++,+5000
E=entity()
if(first()){
runOnTick(1)
holoCreate(1,E:toWorld(vec(0,0,3)),vec(6,6,1),ang(0,0,0),vec(75,75,75),"hqcylinder")   
   holoCreate(2,E:toWorld(vec(0,0,5)),vec(0.8,0.8,0.1),ang(0,0,0),vec(75,75,75),"models/hunter/tubes/tube2x2x025.mdl")    
  holoCreate(3,E:toWorld(vec(0,0,16)),vec(0.9,0.9,0.2),ang(0,0,0),vec(100,0,100),"models/hunter/tubes/tube2x2x025.mdl")    
 holoCreate(4,E:toWorld(vec(0,0,16)),vec(0.7,0.7,0.2),ang(0,0,0),vec(0,0,50),"models/props_phx/construct/metal_angle360.mdl")  
 holoCreate(5,E:toWorld(vec(0,0,46)),vec(3.7,3.7,5.2),ang(0,0,0),vec(255,0,100),"hqcylinder")  
 holoCreate(6,E:toWorld(vec(0,0,77)),vec(0.47,0.47,0.45),ang(0,0,0),vec(55,0,100),"models/props_phx/construct/metal_dome360.mdl")  
 holoCreate(7,E:toWorld(vec(0,0,85)),vec(1,1,1),ang(0,0,0),vec(0,255,100),"icosphere3")  
 holoCreate(8,E:toWorld(vec(0,0,85)),vec(0.6,0.6,0.6),ang(0,0,0),vec(0,255,100),"models/hunter/tubes/tube2x2x05.mdl")  
holoParent(8,E)
}
################
holoMaterial(8,"models/alyx/emptool_glow")
holoMaterial(7,"phoenix_storms/gear")
holoMaterial(6,"phoenix_storms/gear")
holoMaterial(5,"phoenix_storms/gear")
holoMaterial(4,"phoenix_storms/gear")
holoMaterial(2,"phoenix_storms/gear")
holoMaterial(3,"phoenix_storms/gear")
################
holoParent(7,E)
holoParent(6,E)
holoParent(5,E)
holoParent(4,E)
holoParent(3,E)
holoParent(2,E)
holoParent(1,E)
################
holoAlpha(6,150)
holoAng(7,ang()+Counter*2.5)
holoPos(8,holoEntity(5):pos()+vec(0,0,cos(CC+250)*28))
SCREEN=cos(Counter)*28
