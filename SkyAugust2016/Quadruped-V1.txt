@name Quadruped-V1
@inputs 
@outputs 
@persist [L1,L2,L3,L4]:table
interval(60)
E = entity()
if(first()){
function number c(I,V:vector,S:vector,A:angle,Col:vector,Model:string,Alpha:number){ holoCreate(I,V,S,A,Col,Model) holoAlpha(I,Alpha) }
function number c(I,V:vector,S:vector,A:angle,Col:vector,Model:string,Parent:entity,Alpha:number){ holoCreate(I,V,S,A,Col,Model) holoAlpha(I,Alpha) holoParent(I,Parent) }
function number c(I,V:vector,S:vector,A:angle,Col:vector,Model:string,Parent:number,Alpha:number){ holoCreate(I,V,S,A,Col,Model) holoAlpha(I,Alpha) holoParent(I,Parent) }
function number c(I,V:vector,S:vector,A:angle,Col:vector,Model:string,Parent:number,Alpha:number,Mat:string){ holoCreate(I,V,S,A,Col,Model) holoAlpha(I,Alpha) holoParent(I,Parent)     holoMaterial(I,Mat) }
function number c(I,V:vector,S:vector,A:angle,Col:vector,Model:string,Parent:entity,Alpha:number,Mat:string){ holoCreate(I,V,S,A,Col,Model) holoAlpha(I,Alpha) holoParent(I,Parent)     holoMaterial(I,Mat) }
function number c(I,V:vector,S:vector,A:angle,Col:vector,Model:string,Alpha:number,Mat:string){ holoCreate(I,V,S,A,Col,Model) holoAlpha(I,Alpha)    holoMaterial(I,Mat) }


function vector curve(Start:vector,End:vector,Counter,MaxH){
local Dist = (Start-End):length()
local Mid = mix(Start,End,0.5)+entity():up()*(Dist / MaxH)
local Bezier = bezier(Start,Mid,End,Counter)
return Bezier
}



  function table:setAnim(Order,End:vector,DelaySpeed,StepSpeed,Vel){
  if(This["Modes",string]!="Run"&This["Modes",string]!="Save"){ This["Modes",string] = "Save" }

   local T = This  

    if(entity()){
       local CounterNumber = clamp(0.025*max(E:vel():setZ(0):length()*0.025,1),0.01,0.3) * DelaySpeed
       T["Counter",number] = (T["Counter",number] + CounterNumber) % 1
    
    
    switch (T["Modes",string]){
     case "Save",
      if(entity()&rangerOffset(1000,entity():toWorld(End),entity():up()*-1):hit()){
        
       local Number = CounterNumber
       local Startanim = Order
       local Endanim = min(Order + 0.1 + Number,0.9)

         if(T["Counter",number]>Startanim & T["Counter",number]<Endanim){
          local Ranger = rangerOffset(1000,entity():toWorld(End+E:velL():setZ(0)/Vel),entity():up()*-1)
          if(Ranger:position():distance(T["Curve",vector])>5){
           T["Oldpos",vector] = T["Curve",vector]
           T["Modes",string] = "Run"
         }    
    
    
      }
     }
     break
     case "Run",
      local StepNumber = clamp(0.1 + 0.02*E:vel():setZ(0):length()*0.05,0.05,0.4) * StepSpeed

      local NextstepPos = (E:velL():setZ(0) * (0.2 + ( StepNumber * CounterNumber)) ) 

      T["AnimCounter",number] = min(T["AnimCounter",number] + StepNumber, 1) 
      local Ranger = rangerOffset(1000,entity():toWorld(End + NextstepPos),entity():up()*-1)
      
      T["Curve",vector] = curve(T["Oldpos",vector],Ranger:position(),T["AnimCounter",number],4)
    
      if(T["AnimCounter",number]>=1){
       T["AnimCounter",number] = 0
       T["Modes",string] = "Save"
      }
     break
    }
   }
  }



c(1,E:pos(),vec(1),ang(),vec(255,0,0),"cube",255)

c(2,E:pos(),vec(1),ang(),vec(255,0,0),"cube",255)

c(3,E:pos(),vec(1),ang(),vec(255,0,0),"cube",255)

c(4,E:pos(),vec(1),ang(),vec(255,0,0),"cube",255)
}
local DelaySpeed = 1
local StepSpeed = 1
L1:setAnim(0,vec(40,40,0),DelaySpeed,StepSpeed,4)
L2:setAnim(0.25,vec(40,-40,0),DelaySpeed,StepSpeed,4)
L3:setAnim(0.5,vec(-40,40,0),DelaySpeed,StepSpeed,4)
L4:setAnim(0.75,vec(-40,-40,0),DelaySpeed,StepSpeed,4)

if(E){
  local P1 = L1["Curve",vector]
  local P2 = L2["Curve",vector]
  local P3 = L3["Curve",vector]
  local P4 = L4["Curve",vector]

  holoPos(1,P1)
  holoPos(2,P2)
  holoPos(3,P3)
  holoPos(4,P4)
    
}
