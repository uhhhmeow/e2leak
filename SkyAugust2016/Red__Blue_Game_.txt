@name Red | Blue Game 
@inputs 
@outputs 
@persist [RBP1 RBP2 BBP1 BBP2 ]:entity FlagPos:vector FPosTable:table FlagPRand:number
@persist GameStatus:string Game:entity
@persist BlueTeamPlayer:array RedTeamPlayer:array
@persist [AddRedPlayer,AddBluePlayer]:entity FlagOwner:entity
@trigger 
runOnTick(1)
runOnChat(1)

Game = entity(100)


Last = owner():lastSaid():explode(" ")



############~~~~~~~~~Functions~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+

function number chatAsk(AskIndex:number,StringAsk:string){
if(chatClk(owner())&Last:string(AskIndex)==StringAsk){return 1}else{return 0}
}

function entity holoBuild(HIndex:number,HPos:vector,HScale:vector,HAngle:angle,HColor:vector,HModel:string){
holoCreate(HIndex)
holoPos(HIndex,HPos)
holoScale(HIndex,HScale)
holoAng(HIndex,HAngle)
holoColor(HIndex,HColor)
holoModel(HIndex,HModel)
}


function number gInrange(InPlayer:entity,InOrgin:entity,InSizeX:vector2,InSizeY:vector2,InSizeZ:vector2){
if(inrange(InPlayer:pos():x(),InOrgin:pos():x()-InSizeX:x(),InOrgin:pos():x()+InSizeX:y()) 
&inrange(InPlayer:pos():y(),InOrgin:pos():y()-InSizeY:x(),InOrgin:pos():y()+InSizeY:y()) 
&inrange(InPlayer:pos():z(),InOrgin:pos():z()-InSizeZ:x(),InOrgin:pos():z()+InSizeZ:y()) 
){return 1}else{return 0}
}



function void flagDrop(FlagPos:vector,FlagAngle:angle){
holoUnparent(2)
holoColor(3,vec(255))
holoBuild(1,FlagOwner:pos(),vec(5,5,0.1),ang(0),vec(90),"hq_cylinder")
holoPos(2,FlagPos)
if(changed(FlagOwner:isAlive())&!FlagOwner:isAlive() ){Rand = randint(0,180)}
holoAng(2,FlagAngle)
}




############~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+







if(chatAsk(1,"RedBase")&!RBP1){

printColor(vec(60),"[",vec(70),"Game",vec(60),"]",vec(240,240,160),":",vec(60),"[",vec(255,40,40), "RedBase Is Spawned",vec(60),"]")    

RBP1 = propSpawn("models/props_phx/misc/bunker01.mdl",owner():pos(),ang(0),1)
RBP1:setColor(vec(255,0,0))
RBP1:setMaterial("models/props_canal/metalwall005b")

holoBuild(30,RBP1:pos()+vec(10,0,2.5),vec(3,3,0.09),ang(0),vec(255,0,0),"hq_cylinder")

}









if(chatAsk(1,"BlueBase")&!BBP1){
    
printColor(vec(60),"[",vec(70),"Game",vec(60),"]",vec(240,240,160),":",vec(60),"[",vec(40,40,255), "BlueBase Is Spawned",vec(60),"]")     
    
BBP1 = propSpawn("models/props_phx/misc/bunker01.mdl",owner():pos(),ang(0),1)
BBP1:setColor(vec(0,0,255))
BBP1:setMaterial("models/props_canal/metalwall005b")
}









if(chatAsk(1,"GameFlag")){




FlagPRand = randint(1,10)
print(FlagPRand)
FPosTable = table(
vec(-9119.551758,1088.020142,-11135.718750), # White Place
vec(314.114471,-148.014374,-11135.718750), #litle spawn hous
vec(4582.997559,0.260494,-8639.718750), # Coper Roof
vec(475.109802,2819.592773,-9343.718750), # Yellow Hous Roof
vec(-774.101624,-6964.628906,448.281250), # Big Tower
vec(10871.269531,9585.694336,-11007.718750), # Toxic Water
vec(-4247.338379,11004.434570,-10815.824219), #Mine Field
vec(-1404.776978,4869.097656,-11135.718750), # Box Opened Hous
vec(2308.502930,-777.477844,-10095.718750), # Bigest White House By Spawn
vec(1554.616699,-3387.387695,-11135.718750) # Some Point On Spawn 
)




holoBuild(1,FPosTable[10,vector],vec(5,5,0.1),ang(0),vec(90),"hq_cylinder")
holoBuild(2,FPosTable[10,vector]+vec(0,0,17),vec(0.15,0.15,3),ang(0),vec(30),"hq_cylinder")
holoBuild(3,FPosTable[10,vector]+vec(6,0,30),vec(0.7,0.05,1),ang(90,0,0),vec(255),"prism"),holoParent(3,2)



GameStatus = "On"
}


if(chatAsk(1,"GameAdd")&RBP1&BBP1){

#~~~
if(chatAsk(2,"Red")&RedTeamPlayer:count()!=4){
AddRedPlayer = findPlayerByName( Last:string(3) )
RedTeamPlayer:pushEntity(AddRedPlayer)
AddRedPlayer:plySetPos(RBP1:pos()+vec(0,0,3))

printColor(vec(60),"[",vec(70),"Game",vec(60),"]",vec(240,240,160),":",vec(60),"[",vec(240,240,160),AddRedPlayer:name()+" Joint The ",vec(255,0,0),"Red",vec(240,240,160)," Team",vec(60),"]")   

}
elseif(chatAsk(2,"Blue")&BlueTeamPlayer:count()!=4){
AddBluePlayer = findPlayerByName( Last:string(3) )
BlueTeamPlayer:pushEntity(AddBluePlayer)
AddBluePlayer:plySetPos(BBP1:pos()+vec(0,0,3))
printColor(vec(60),"[",vec(70),"Game",vec(60),"]",vec(240,240,160),":",vec(60),"[",vec(240,240,160),AddRedPlayer:name()+" Joint The ",vec(0,0,255),"Blue",vec(240,240,160)," Team",vec(60),"]")   
}
#~~~Print
if(RedTeamPlayer:count()==3){
printColor(vec(60),"[",vec(70),"Game",vec(60),"]",vec(240,240,160),":",vec(60),"[",vec(255,135,0), "Max", vec(255,0,0)," Red",vec(255,135,0)," Player are Reached",vec(60),"]")
}
if(BlueTeamPlayer:count()==3){
printColor(vec(60),"[",vec(70),"Game",vec(60),"]",vec(240,240,160),":",vec(60),"[",vec(255,135,0), "Max", vec(0,0,255)," Blue",vec(255,135,0)," Player are Reached",vec(60),"]")
}







}



#####Erkennung

for(RedPlayers=1,RedTeamPlayer:count()){
holoBuild(RedPlayers+10,RedTeamPlayer[RedPlayers,entity]:pos(),vec(6),ang(0),vec(255,0,0),"hq_sphere")
holoParent(RedPlayers+10,RedTeamPlayer[RedPlayers,entity])
holoAlpha(RedPlayers+10,100)
holoAng(RedPlayers+10,ang(0))
}


for(BluePlayers=1,BlueTeamPlayer:count()){
holoBuild(BluePlayers+20,BlueTeamPlayer[BluePlayers,entity]:pos(),vec(6),ang(0),vec(0,0,255),"hq_sphere")
holoParent(BluePlayers+20,BlueTeamPlayer[BluePlayers,entity])
holoAlpha(BluePlayers+20,100)
holoAng(BluePlayers+20,ang(0))
}



##########

if(GameStatus == "On"){

for(RedPlayers=1,RedTeamPlayer:count()){

if(gInrange(RedTeamPlayer[RedPlayers,entity],holoEntity(1),vec2(50),vec2(50),vec2(50))&RedTeamPlayer[RedPlayers,entity]:isAlive()){

FlagOwner = RedTeamPlayer[RedPlayers,entity]
holoDelete(1)    
holoColor(3,vec(255,0,0))



holoAng(2,owner():attachmentAng("anim_attachment_LH")+ang(20,0,0))
holoPos(2,owner():attachmentPos("anim_attachment_LH")+vec())
holoParentAttachment(2,RedTeamPlayer[RedPlayers,entity],"anim_attachment_LH")

}

###Dead Drop

if(FlagOwner:isPlayer()){
if(!FlagOwner:isAlive()){
flagDrop(holoEntity(1):pos()+vec(0,0,1),ang(0,Rand,90))
}}

#KeyDrop Red

#if(FlagOwner:keyPressed("r")){

#holoUnparent(2)
#holoColor(3,vec(255))
#holoBuild(1,entity():pos(),vec(5,5,0.1),ang(0),vec(90),"hq_cylinder")
#holoPos(2,holoEntity(1):pos()+vec(0,0,1))
#if(changed(FlagOwner:isAlive())&!FlagOwner:isAlive() ){Rand = randint(0,180)}
#holoAng(2,ang(0,Rand,90))

#}




}









for(BluePlayers=1,BlueTeamPlayer:count()){

if(gInrange(BlueTeamPlayer[BluePlayers,entity],holoEntity(1),vec2(50),vec2(50),vec2(50))&BlueTeamPlayer[BluePlayers,entity]:isAlive()){

FlagOwner = BlueTeamPlayer[BluePlayers,entity]

holoDelete(1)    
holoColor(3,vec(0,0,255))

if(FlagOwner:isAlive()){
holoAng(2,owner():attachmentAng("anim_attachment_LH")+ang(20,0,0))
holoPos(2,owner():attachmentPos("anim_attachment_LH")+vec())
holoParentAttachment(2,BlueTeamPlayer[BluePlayers,entity],"anim_attachment_LH")
}

}

#DeadDrop

if(FlagOwner:isPlayer()){
if(!FlagOwner:isAlive()){
flagDrop(holoEntity(1):pos()+vec(0,0,1),ang(0,Rand,90))
}}

#KeyDrop












}














































}


@name Red | Blue Game 
@inputs 
@outputs 
@persist [RBP1 RBP2 BBP1 BBP2 ]:entity FlagPos:vector FPosTable:table FlagPRand:number
@persist GameStatus:string Game:entity
@persist BlueTeamPlayer:array RedTeamPlayer:array
@persist [AddRedPlayer,AddBluePlayer]:entity FlagOwner:entity
@trigger 
runOnTick(1)
runOnChat(1)

Game = entity(100)


Last = owner():lastSaid():explode(" ")



############~~~~~~~~~Functions~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+

function number chatAsk(AskIndex:number,StringAsk:string){
if(chatClk(owner())&Last:string(AskIndex)==StringAsk){return 1}else{return 0}
}

function entity holoBuild(HIndex:number,HPos:vector,HScale:vector,HAngle:angle,HColor:vector,HModel:string){
holoCreate(HIndex)
holoPos(HIndex,HPos)
holoScale(HIndex,HScale)
holoAng(HIndex,HAngle)
holoColor(HIndex,HColor)
holoModel(HIndex,HModel)
}


function number gInrange(InPlayer:entity,InOrgin:entity,InSizeX:vector2,InSizeY:vector2,InSizeZ:vector2){
if(inrange(InPlayer:pos():x(),InOrgin:pos():x()-InSizeX:x(),InOrgin:pos():x()+InSizeX:y()) 
&inrange(InPlayer:pos():y(),InOrgin:pos():y()-InSizeY:x(),InOrgin:pos():y()+InSizeY:y()) 
&inrange(InPlayer:pos():z(),InOrgin:pos():z()-InSizeZ:x(),InOrgin:pos():z()+InSizeZ:y()) 
){return 1}else{return 0}
}



function void flagDrop(FlagPos:vector,FlagAngle:angle){
holoUnparent(2)
holoColor(3,vec(255))
holoBuild(1,FlagOwner:pos(),vec(5,5,0.1),ang(0),vec(90),"hq_cylinder")
holoPos(2,FlagPos)
if(changed(FlagOwner:isAlive())&!FlagOwner:isAlive() ){Rand = randint(0,180)}
holoAng(2,FlagAngle)
}




############~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+







if(chatAsk(1,"RedBase")&!RBP1){

printColor(vec(60),"[",vec(70),"Game",vec(60),"]",vec(240,240,160),":",vec(60),"[",vec(255,40,40), "RedBase Is Spawned",vec(60),"]")    

RBP1 = propSpawn("models/props_phx/misc/bunker01.mdl",owner():pos(),ang(0),1)
RBP1:setColor(vec(255,0,0))
RBP1:setMaterial("models/props_canal/metalwall005b")

holoBuild(30,RBP1:pos()+vec(10,0,2.5),vec(3,3,0.09),ang(0),vec(255,0,0),"hq_cylinder")

}









if(chatAsk(1,"BlueBase")&!BBP1){
    
printColor(vec(60),"[",vec(70),"Game",vec(60),"]",vec(240,240,160),":",vec(60),"[",vec(40,40,255), "BlueBase Is Spawned",vec(60),"]")     
    
BBP1 = propSpawn("models/props_phx/misc/bunker01.mdl",owner():pos(),ang(0),1)
BBP1:setColor(vec(0,0,255))
BBP1:setMaterial("models/props_canal/metalwall005b")
}









if(chatAsk(1,"GameFlag")){




FlagPRand = randint(1,10)
print(FlagPRand)
FPosTable = table(
vec(-9119.551758,1088.020142,-11135.718750), # White Place
vec(314.114471,-148.014374,-11135.718750), #litle spawn hous
vec(4582.997559,0.260494,-8639.718750), # Coper Roof
vec(475.109802,2819.592773,-9343.718750), # Yellow Hous Roof
vec(-774.101624,-6964.628906,448.281250), # Big Tower
vec(10871.269531,9585.694336,-11007.718750), # Toxic Water
vec(-4247.338379,11004.434570,-10815.824219), #Mine Field
vec(-1404.776978,4869.097656,-11135.718750), # Box Opened Hous
vec(2308.502930,-777.477844,-10095.718750), # Bigest White House By Spawn
vec(1554.616699,-3387.387695,-11135.718750) # Some Point On Spawn 
)




holoBuild(1,FPosTable[10,vector],vec(5,5,0.1),ang(0),vec(90),"hq_cylinder")
holoBuild(2,FPosTable[10,vector]+vec(0,0,17),vec(0.15,0.15,3),ang(0),vec(30),"hq_cylinder")
holoBuild(3,FPosTable[10,vector]+vec(6,0,30),vec(0.7,0.05,1),ang(90,0,0),vec(255),"prism"),holoParent(3,2)



GameStatus = "On"
}


if(chatAsk(1,"GameAdd")&RBP1&BBP1){

#~~~
if(chatAsk(2,"Red")&RedTeamPlayer:count()!=4){
AddRedPlayer = findPlayerByName( Last:string(3) )
RedTeamPlayer:pushEntity(AddRedPlayer)
AddRedPlayer:plySetPos(RBP1:pos()+vec(0,0,3))

printColor(vec(60),"[",vec(70),"Game",vec(60),"]",vec(240,240,160),":",vec(60),"[",vec(240,240,160),AddRedPlayer:name()+" Joint The ",vec(255,0,0),"Red",vec(240,240,160)," Team",vec(60),"]")   

}
elseif(chatAsk(2,"Blue")&BlueTeamPlayer:count()!=4){
AddBluePlayer = findPlayerByName( Last:string(3) )
BlueTeamPlayer:pushEntity(AddBluePlayer)
AddBluePlayer:plySetPos(BBP1:pos()+vec(0,0,3))
printColor(vec(60),"[",vec(70),"Game",vec(60),"]",vec(240,240,160),":",vec(60),"[",vec(240,240,160),AddRedPlayer:name()+" Joint The ",vec(0,0,255),"Blue",vec(240,240,160)," Team",vec(60),"]")   
}
#~~~Print
if(RedTeamPlayer:count()==3){
printColor(vec(60),"[",vec(70),"Game",vec(60),"]",vec(240,240,160),":",vec(60),"[",vec(255,135,0), "Max", vec(255,0,0)," Red",vec(255,135,0)," Player are Reached",vec(60),"]")
}
if(BlueTeamPlayer:count()==3){
printColor(vec(60),"[",vec(70),"Game",vec(60),"]",vec(240,240,160),":",vec(60),"[",vec(255,135,0), "Max", vec(0,0,255)," Blue",vec(255,135,0)," Player are Reached",vec(60),"]")
}







}



#####Erkennung

for(RedPlayers=1,RedTeamPlayer:count()){
holoBuild(RedPlayers+10,RedTeamPlayer[RedPlayers,entity]:pos(),vec(6),ang(0),vec(255,0,0),"hq_sphere")
holoParent(RedPlayers+10,RedTeamPlayer[RedPlayers,entity])
holoAlpha(RedPlayers+10,100)
holoAng(RedPlayers+10,ang(0))
}


for(BluePlayers=1,BlueTeamPlayer:count()){
holoBuild(BluePlayers+20,BlueTeamPlayer[BluePlayers,entity]:pos(),vec(6),ang(0),vec(0,0,255),"hq_sphere")
holoParent(BluePlayers+20,BlueTeamPlayer[BluePlayers,entity])
holoAlpha(BluePlayers+20,100)
holoAng(BluePlayers+20,ang(0))
}



##########

if(GameStatus == "On"){

for(RedPlayers=1,RedTeamPlayer:count()){

if(gInrange(RedTeamPlayer[RedPlayers,entity],holoEntity(1),vec2(50),vec2(50),vec2(50))&RedTeamPlayer[RedPlayers,entity]:isAlive()){

FlagOwner = RedTeamPlayer[RedPlayers,entity]
holoDelete(1)    
holoColor(3,vec(255,0,0))



holoAng(2,owner():attachmentAng("anim_attachment_LH")+ang(20,0,0))
holoPos(2,owner():attachmentPos("anim_attachment_LH")+vec())
holoParentAttachment(2,RedTeamPlayer[RedPlayers,entity],"anim_attachment_LH")

}

###Dead Drop

if(FlagOwner:isPlayer()){
if(!FlagOwner:isAlive()){
flagDrop(holoEntity(1):pos()+vec(0,0,1),ang(0,Rand,90))
}}

#KeyDrop Red

#if(FlagOwner:keyPressed("r")){

#holoUnparent(2)
#holoColor(3,vec(255))
#holoBuild(1,entity():pos(),vec(5,5,0.1),ang(0),vec(90),"hq_cylinder")
#holoPos(2,holoEntity(1):pos()+vec(0,0,1))
#if(changed(FlagOwner:isAlive())&!FlagOwner:isAlive() ){Rand = randint(0,180)}
#holoAng(2,ang(0,Rand,90))

#}




}









for(BluePlayers=1,BlueTeamPlayer:count()){

if(gInrange(BlueTeamPlayer[BluePlayers,entity],holoEntity(1),vec2(50),vec2(50),vec2(50))&BlueTeamPlayer[BluePlayers,entity]:isAlive()){

FlagOwner = BlueTeamPlayer[BluePlayers,entity]

holoDelete(1)    
holoColor(3,vec(0,0,255))

if(FlagOwner:isAlive()){
holoAng(2,owner():attachmentAng("anim_attachment_LH")+ang(20,0,0))
holoPos(2,owner():attachmentPos("anim_attachment_LH")+vec())
holoParentAttachment(2,BlueTeamPlayer[BluePlayers,entity],"anim_attachment_LH")
}

}

#DeadDrop

if(FlagOwner:isPlayer()){
if(!FlagOwner:isAlive()){
flagDrop(holoEntity(1):pos()+vec(0,0,1),ang(0,Rand,90))
}}

#KeyDrop












}














































}


