@name Box
@inputs 
@outputs 
@persist [E Ply]:entity T C Use Num Ang
@model models/hunter/blocks/cube05x05x025.mdl

if(first())
{
    E=entity()
    E:setAlpha(0)
    
    holoCreate(1,E:toWorld(vec(0,0,-2.5)),vec(1.9,1.9,0.5),ang(),vec(100,100,100))
    holoMaterial(1,"phoenix_storms/metalbox")
    holoParent(1,E)
    
    holoCreate(2,E:toWorld(vec(0,11.45,1.55)),vec(2,0.1,1.2),ang(),vec()+255,"hq_rcube_thin")
    holoMaterial(2,"phoenix_storms/metal_plate")
    holoParent(2,E)
    
    holoCreate(3,E:toWorld(vec(0,-11.45,1.55)),vec(2,0.1,1.2),ang(),vec()+255,"hq_rcube_thin")
    holoMaterial(3,"phoenix_storms/metal_plate")
    holoParent(3,E)
    
    holoCreate(4,E:toWorld(vec(11.45,0,1.55)),vec(0.1,2,1.2),ang(),vec()+255,"hq_rcube_thin")
    holoMaterial(4,"phoenix_storms/metal_plate")
    holoParent(4,E)
    
    holoCreate(5,E:toWorld(vec(-11.45,0,1.55)),vec(0.1,2,1.2),ang(),vec()+255,"hq_rcube_thin")
    holoMaterial(5,"phoenix_storms/metal_plate")
    holoParent(5,E)
    
    holoCreate(6,E:toWorld(vec(0,11,9)),vec(0.2,0.2,0.2),ang())
    holoAlpha(6,0)
    holoParent(6,E)
    
    holoCreate(7,E:toWorld(vec(0,-0.4,9.2)),vec(2,1.94,0.1),ang(),vec(150,150,150),"hq_rcube_thin")
    holoMaterial(7,"phoenix_storms/metal")
    holoParent(7,6)
    
    holoCreate(8,E:toWorld(vec(8,11.5,9)),vec(0.2,0.2,0.5),ang(90,0,0),vec(50,50,50),"hq_cylinder")
    holoMaterial(8,"phoenix_storms/fender_chrome")
    holoParent(8,E)
    
    holoCreate(9,E:toWorld(vec(-8,11.5,9)),vec(0.2,0.2,0.5),ang(90,0,0),vec(50,50,50),"hq_cylinder")
    holoMaterial(9,"phoenix_storms/fender_chrome")
    holoParent(9,E)
    
    holoCreate(10,E:toWorld(vec()),vec(0.4,0.4,0.8),E:angles(),vec()+255,"hq_cylinder")
    holoMaterial(10,"phoenix_storms/fender_chrome")
    holoParent(10,E)
    
    holoCreate(11,E:toWorld(vec()),vec(0.3,0.3,0.8),E:angles(),vec()+255,"hq_cylinder")
    holoMaterial(11,"phoenix_storms/fender_chrome")
    holoParent(11,E)
    
    holoCreate(12,E:toWorld(vec()),vec(0.2,0.2,0.8),E:angles(),vec()+255,"hq_cylinder")
    holoMaterial(12,"phoenix_storms/fender_chrome")
    holoParent(12,E)
    
    holoCreate(13,E:toWorld(vec()),vec(0.1,0.1,0.8),E:angles(),vec()+255,"hq_cylinder")
    holoMaterial(13,"phoenix_storms/fender_chrome")
    holoParent(13,E)
    
    holoCreate(14,E:toWorld(vec(0,0,5)),vec(0.2,0.2,0.2),E:angles(),vec(),"hq_sphere")
    holoAlpha(14,255)
    holoParent(14,13)
    
    holoCreate(15,holoEntity(14):toWorld(vec(0,0,2)),vec(0.1,0.1,0.3),ang(),vec()+255,"hq_cylinder")
    holoMaterial(15,"phoenix_storms/fender_chrome")
    holoParent(15,14)
    
    holoCreate(16,E:toWorld(vec(0,0,10)),vec(1.5,1.5,0.05),ang(0,-90,0),vec()+255,"hq_rcube_thin")
    holoDisableShading(16,1)
    holoMaterial(16,"models/rendertarget")
    holoParent(16,15)
    
    holoCreate(17,holoEntity(16):toWorld(vec()),vec(1.53,1.53,0.3),ang(),vec(),"hq_rcube_thin")
    holoClip(17,vec(0,0,0.1),vec(0,0,-0.1),0)
    holoClipEnabled(17,1)
    holoParent(17,16)
}

interval(100)

T=T%100
C--

if(T%1==0)
{
    for(I=1,players():count())
    {
        Ply=players()[I,entity]
        
        if(Ply:shootPos():distance(E:pos())<90)
        {
            if(Ply:keyUse()&Ply:aimEntity()==E&C<1)
            {
                Use=!Use
                C=5
                

        soundPlay(2,0,"/ui/gamestartup7.mp3") 
        Ready=0

        
        
            }
        }
    }
}
if (!Use) {soundStop(2)} 
Num=clamp(Num+(Use-0.5)*2.2*3.2,0,150)
Ang=clamp(Ang+(Use-0.7)*2*3.2,0,50)
R=inrange(Num,0,80)
R2=inrange(Num,30,150)

holoAng(6,E:toWorld(ang(0,0,-clamp(Num,0,30)*5)))

if(R)
{
    for(I=1,4)
    {
        Idx=I+9
        Z=clamp((Num-20)/25*35,0,(I*8)-5)
       
        holoPos(Idx,E:toWorld(vec(0,0,Z-0.5)))
    }
}

if(!Use)
{
    if(changed(R2)&R2)
    {
        E:soundPlay(1,2,"/weapons/sentry_move_medium3.wav")
    }
}

if(changed(!R2)&R2){E:soundPlay(1,2,"/weapons/sentry_move_medium3.wav")}

if(R2){holoAng(14,holoEntity(13):toWorld(ang(0,0,clamp(0,Ang,0))))}


