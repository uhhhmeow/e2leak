@name EGP-WAYPOINTMAP
@inputs [EGP BUS]:wirelink
@outputs
@persist LCR SCR DRAW M OX OY TI R BX BY SX SY XM YM EY [O E EGPE]:entity DP:vector
@persist SETUP LWS WPN OK WAY:array
@trigger 

if(first()){
    runOnTick(1)
    runOnFile(1)   
    
    dsJoinGroup("shuttle")
    dsSetScope(0)
    
    E=entity()
    O=owner()
    
    fileLoad("ukes_shuttle/shuttle["+map()+"].txt")
    
    M=50
    
    OX=250
    OY=250
    LCR=1
    
    SX=OX
    SY=OY
    BX=OX
    BY=OY
    
    XM=-1
    YM=1

    printColor(vec(255,255,255),("EGP-WAYPOINTMAP by "),vec(0,255,255),("Uke"),vec(255,255,255),("."))
    
}

if(fileClk()){
    if (fileStatus() == _FILE_404){printColor(vec(255,0,0),("Status: NOT FOUND.")),timer("reset",1000)}
    elseif(fileStatus() == _FILE_OK){printColor(vec(0,255,0),("Status: OK.")),OK=1}
    elseif(fileStatus() == _FILE_TIMEOUT){printColor(vec(255,0,0),("Status: TIMEOUT.")),timer("reset",1000)}
    elseif(fileStatus() == _FILE_TRANSFER_ERROR){printColor(vec(255,0,0),("Status: TRANSFER ERROR.")),timer("reset",1000)}
    elseif(fileStatus() == _FILE_UNKNOWN){printColor(vec(180,180,180),("Status: UNKNOWN.")),timer("reset",1000)}
    WPN=fileRead():explode("\n"):count()
    SCR=WPN
    printColor(vec(0,240,255),("LOADED \n"+fileName()+"\nWITH "),vec(250,250,150)*(WPN!=0),(WPN-1),vec(0,240,255),(" WAYPOINT"+((WPN-1) != 1 ? "S." : ".")))
    
    }

if((SETUP==0&OK==1)&->EGP){
        FILE=fileRead()
        if(LWS<WPN){
            LWS++
            TSTR=FILE:explode("\n"):string(LWS)
            
            if(LWS==2){R=angnorm(180+EGP:entity():angles():yaw())}
            if(LWS>1){
                WAY[LWS,vector4]=vec4(TSTR:explode("|"):string(1):explode(","):string(1):toNumber(),TSTR:explode("|"):string(1):explode(","):string(2):toNumber(),TSTR:explode("|"):string(1):explode(","):string(3):toNumber(),TSTR:explode("|"):string(2):toNumber())
            
                WVEC=(vec2(WAY[LWS,vector4])/M*vec2(XM,YM)):rotate(R)+vec2(OX,OY)
                if(WVEC:x()<SX|LWS==2){SX=WVEC:x()}
                if(WVEC:y()<SY|LWS==2){SY=WVEC:y()}
                if(WVEC:x()>BX|LWS==2){BX=WVEC:x()}
                if(WVEC:y()>BY|LWS==2){BY=WVEC:y()}
                
                if(BY>375){EY=-((BY-375)/2)}else{EY=0}
            }
            
            print(_HUD_PRINTCENTER,("SETUP: "+floor((LWS/WPN)*100,0)+"%"))
        
        }elseif(->EGP){
                        
            EGP:egpClear()
                        
            EGP:egpRoundedBox(289,vec2((BX+SX)/2,(BY+SY)/2)+vec2(0,EY),vec2(abs(BX-SX)+100,abs(BY-SY)+100))
            EGP:egpAlpha(289,200)
            EGP:egpColor(289,vec(0,0,0))
            
            EGP:egpBox(291,vec2(OX,OY),vec2(15,15))
            EGP:egpAlpha(291,240)
            EGP:egpColor(291,vec(255,155,0))
            
            EGP:egpBox(292,vec2(OX,OY),vec2(2,2))
            EGP:egpAlpha(292,240)
            EGP:egpPoly(293,vec2(0,16),vec2(4,8),vec2(4,-8),vec2(-4,-8),vec2(-4,8))
            EGP:egpColor(293,vec(0,255,255))
            EGP:egpParent(293,292)
            
            EGP:egpText(294,"-YOU ARE HERE.",vec2(SX-15,BY+EY+15))
            EGP:egpAlpha(294,240)
            EGP:egpColor(294,vec(255,155,0))
            EGP:egpSize(294,25)
            
            EGP:egpText(295,map()+" ("+WPN+" Waypoint"+(WPN==1 ? "" : "s" )+")  -"+O:name(),vec2(SX-15,SY+EY-30))
            EGP:egpAlpha(295,200)
            EGP:egpColor(295,vec(255,255,255))
            EGP:egpSize(295,17)
            
            OY=250+EY
            
            SETUP=1
            printColor(vec(200,200,200),("SETUP DONE"))

    }
        
}elseif(!->EGP&first()){hint("Setup will start if you link an EGP-EMITTER to the EGP-WAYPOINTMAP",50)}
    
if(SETUP==1&DRAW==0&WPN>=1&TI<curtime()&->EGP){
    LCR++
    
    if(LCR<WPN){
        P=(vec2(WAY[LCR,vector4]:x(),WAY[LCR,vector4]:y())/M*vec2(XM,YM)):rotate(R)+vec2(OX,OY)
        NP=(vec2(WAY[LCR+1,vector4]:x(),WAY[LCR+1,vector4]:y())/M*vec2(XM,YM)):rotate(R)+vec2(OX,OY)
        EGP:egpLine(LCR,P,NP)
        }
    
    if(WAY[LCR,vector4]:w()==1){
        SCR++        
        P2=(vec2(WAY[LCR,vector4]:x(),WAY[LCR,vector4]:y())/M*vec2(XM,YM)):rotate(R)+vec2(OX,OY)
        EGP:egpBox(SCR,P2,vec2(8,8))
        EGP:egpAlpha(SCR,200)
        EGP:egpColor(SCR,vec(0,255,0))
    }
    
    if(LCR>=WPN){
        P=(vec2(WAY[WAY:count(),vector4]:x(),WAY[WAY:count(),vector4]:y())/M*vec2(XM,YM)):rotate(R)+vec2(OX,OY)
        NP=(vec2(WAY[2,vector4]:x(),WAY[2,vector4]:y())/M*vec2(XM,YM)):rotate(R)+vec2(OX,OY)
        EGP:egpLine(LCR,P,NP)
        
        EGP:egpText(296,".",vec2(0,0))
        EGP:egpAlpha(296,255)
        EGP:egpColor(296,vec(0,200,255))
        EGP:egpSize(296,16)
        
        DRAW=1
        }
    print(_HUD_PRINTCENTER,("DRAW: "+floor((LCR/WPN)*100,0)+"%"))
    TI=curtime()+0.1
    }
    
if(SETUP==1&DRAW==1&TI<curtime()){
    
    EGPE=EGP:entity()
    
    EGP:egpOrder(293,0)
    
    P4=(vec2(EGPE:pos():x(),EGPE:pos():y())/M*vec2(XM,YM)):rotate(R)+vec2(OX,OY)
    EGP:egpPos(291,P4)
    
    BE=BUS:entity()
    
    P5=(vec2(BE:pos():x(),BE:pos():y())/M*vec2(XM,YM)):rotate(R)+vec2(OX,OY)
    EGP:egpPos(292,P5)
    
    A5=angnorm(BE:angles():yaw()-R-90)
    EGP:egpAngle(292,A5)
    
    EGP:egpColor(293,vec(255*(BUS["FAIL",number]|BUS["PAUSE",number]),255*(!BUS["FAIL",number]),255*(!BUS["FAIL",number])*(!BUS["PAUSE",number])))
    EGP:egpOrder(293,0)
    
    if(BUS["SC",number]>0){
        EGP:egpSetText(296,BUS["PSC",number]+"/"+BUS["SC",number])
        EGP:egpPos(296,P5+vec2(7,-21))
    }else{
        EGP:egpSetText(296,"")
    }
       
    TI=curtime()+0.2 
}

if(dsClk()&dsGetString()=="reset"){reset(),print("MAP RESET (UPDATE)")}
