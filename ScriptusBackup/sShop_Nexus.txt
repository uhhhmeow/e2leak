@name sShop Nexus
@inputs ForceExecution
@persist [Items Queue Chat Processing Cannister]:table [Owner Entity]:entity Interest MaxProcessTime SpawnWeapons
@persist [Lua_Explosion Lua_SpawnWeapon]:string Lua:entity

#[
    sShop Nexus
    Quickly sells items via PropCore, NexusCore, holoModelAny, and TylerB's MoneyRequest
    
    Use .addshipment and .removeshipment to add/remove shipments to the vending list
]#

interval(100) # required for moneyClk
Time=curtime()

# Main

if(first()) {
    Interest=1.25 # Price multiplier
    MaxProcessTime=10
    SpawnWeapons=1 # Use Lua to spawn weapons
    runOnChat(1)
    
    timer("ads",0)
    
    Owner=owner()
    Entity=entity()
    
    if(SpawnWeapons) {
        #Entity:setPos(vec(-5e3))
    }
    
    # Entity:propNotSolid(1)
    Entity:setAlpha(0)
    
    #include "lib/ct/shipmentmetadata"
    
    function ulx_PM(Player:entity,Message:string) {
        if(Player!=Owner) {
            concmd("ulx psay \""+Player:name()+"\" \""+Message+"\"")
        } else {
            printColor(vec(255,255,0),"[sNexus] ",vec(255),Message)
        }
    }
    
    function number cmd_addShipment(Args:array) {
        local Ent=Owner:aimEntity()
        local Success=0
        local Name=""
        
        if(!Ent:isShipment()) {
            printColor(vec(255,255,0),"[sNexus] ",vec(255,0,0),"This is not a valid shipment.")
            return 0
        }
        
        local Type=Ent:shipmentType()
        local Dat=ShipmentMetaData[Type,table]
        Dat["Entity",entity]=Ent
        Dat["Cooldown",number]=Time
        Dat["Max",number]=Ent:shipmentAmount()
        
        Items[Type,table]=Dat
        
        printColor(vec(255,255,0),"[sNexus] ",vec(255),"Wrote shipment data for ",vec(0,255,0),Dat["Name",string],vec(255),".")
        
        return 1
    }
    
    function number cmd_removeShipment(Args:array) {
        local Ent=Owner:aimEntity()
        local Success=0
        local Name=""
        foreach(I,V:table=Items) {
            if(V["Entity",entity]==Ent) {
                Name=V["Name",string]
                Items:removeTable(I)
                Success=1
                break
            }
        }
        
        if(Success) {
            printColor(vec(255,255,0),"[sNexus] ",vec(255),"Removed ",vec(0,255,0),Name,vec(255)," from vending list.")
            return 1
        } else {
            printColor(vec(255,255,0),"[sNexus] ",vec(255),"This entity is not on the vending list.")
            return 0
        }
    }
    
    function string getShipmentFromName(Name:string) {
        Name=Name:lower()
        foreach(I,V:table=Items) {
            if(V["Name",string]:lower():matchFirst(Name)) {
                return V["Type",string]
            }
        }
        return "NULL"
    }
    
    function number cleanMissing() {
        if(SpawnWeapons) {return 0}
        foreach(I,V:table=Items) {
            if(V["Entity",entity]:shipmentAmount()<1) {
                printColor(vec(255,255,0),"[sNexus] ",vec(255,0,0),V["Name",string],vec(255)," has been exhausted.")
                Items:removeTable(I)
            }
        }
        return 1
    }
    
    function number cmd_ownerBuyItem(Args:array) {
        cleanMissing()
        if(Args[1,string]!="*") {
            local Item=getShipmentFromName(Args[1,string])
            if(Item!="NULL" & Items[Item,table]["Max",number]>0) {
                Items[Item,table]["Max",number]=Items[Item,table]["Max",number]-1
                Queue:pushTable(
                    table(
                        "Owner"=Owner,
                        "Order"=array(Item),
                        "Sent"=0
                    )
                )
                printColor(vec(255,255,0),"[sNexus] ",vec(255),"Vending one ",vec(0,255,0),ShipmentMetaData[Item,table]["Name",string],vec(255),".")
            } else {
                printColor(vec(255,255,0),"[sNexus] ",vec(255),"No such item: ",vec(255,0,0),Args[1,string],vec(255),".")
            }
        } else {
            local Arr=array()
            foreach(I,V:table=Items) {
                if(V["Max",number]>0) {
                    Arr:pushString(I)
                    Items[I,table]["Max",number]=Items[I,table]["Max",number]-1
                }
            }
            Queue:pushTable(
                table(
                    "Owner"=Owner,
                    "Order"=Arr,
                    "Sent"=0
                )
            )
        }
    }
    
    function number cmd_playerBuyItem(Player:entity,Item:string) {
        cleanMissing()
        local RawItem=Item
        local Item=getShipmentFromName(Item)
        if(Item!="NULL" & !Processing[Player:steamID(),table] & Items[Item,table]["Max",number]>0) {
            local Price=ceil(ShipmentMetaData[Item,table]["Price",number]*Interest)
            moneyRequest(Player,Price,"sNexus - Buying "+ShipmentMetaData[Item,table]["Name",string],10)
            Processing[Player:steamID(),table]=table(
                "Owner"=Player,
                "Order"=array(Item),
                "Expires"=Time+MaxProcessTime,
                "Sent"=0
            )
            timer("TimeoutCheck",MaxProcessTime*1000+500)
        } elseif (Processing[Player:steamID(),table]) {
            ulx_PM(Player,"You already have a pending order.")
        } elseif(Item=="NULL") {
            ulx_PM(Player,"No such item for sale: "+RawItem+".")
        }
    }
    
    
    
    Chat=table(
        "addshipment"="cmd_addShipment",
        "removeshipment"="cmd_removeShipment",
        "qbuy"="cmd_ownerBuyItem"
    )
    
    findByClass("spawned_weapon")
    findInSphere(Entity:pos(),300)
    
    ###########################
    # Headcrab Cannister Code #
    ###########################
    
    local HoloID=holoRemainingSpawns()-1
    local Channel=11
    
    Cannister=table("Active"=table(),"Count"=0,"Holo"=HoloID,"Sound"=Channel)
    holoCreate(HoloID,vec(),vec(),ang())
    
    # spawnHeadcrabCannister(int HoloID, vector Start, vector End, string CallbackFunction)
        # callBackFunction(int HoloID, vector EjectPoint)
    
    function spawnHeadcrabCannister(I,Start:vector,End:vector,Callback:string) {
        holoCreate(I)
        holoModel(I,"models/props_combine/headcrabcannister01b.mdl")
        holoEntity(I):setTrails(45,15,3,"trails/smoke",vec(255),255)#*0.35)
        holoPos(I,Start)
        Cannister["Active",table]["Cannister_"+I,table]=
            table(
                "ID"=I,
                "Start"=Start,
                "Pos"=Start,
                "Timer"=0,
                "End"=End,
                "Callback"=Callback,
                "Speed"=750
            )
        holoCreate(Cannister["Holo",number],End,vec(),ang())
        holoEntity(Cannister["Holo",number]):soundPlay("Incoming",0,"HeadcrabCanister.IncomingSound")
        if(Cannister["Count",number]==0) {
            timer("Cannister_Loop",1)
        }
        Cannister["Count",number]=Cannister["Count",number]+1
    }
    
    function onCannisterLanded(Eject:vector,Velocity:vector) {
        Queue[1,table]["Landed",number]=1
        Queue[1,table]["EjectPos",vector]=Eject
        Queue[1,table]["EjectVel",vector]=Velocity
    }
    
    #########################
    # Lua Strings - CT Only #
    #########################
    findByClass("lua_run")
    Lua=find()
    
    Lua_Explosion="
        local Pos=Vector(%d,%d,%d)
        for i=1,5 do ParticleEffect(\"grenade_final\",Pos,Angle(0, 0, 0),nil) end
        util.ScreenShake(Pos,500,500,2,500)
        
        local Explode=ents.Create('env_explosion')
	       Explode:SetPos(Pos)
	       Explode:Spawn()
	       Explode:SetKeyValue('iMagnitude','0')
	       Explode:SetKeyValue('spawnflags','64')
	       Explode:Fire('Explode',0,0 )
    "
    
    function lua_Run(String:string) {
        # Scriptis' Lua Environment
        local Exec=
            format(
                "
                local SteamID=[[%s]]
                local Owner=nil
                for i,v in pairs(player.GetAll()) do
                	if v:SteamID()==SteamID then
                		Owner=v
                	end
                end
                
                -- Owner:ChatPrint('- Start pcall -')
                -- Owner:ChatPrint('RunString: '..tostring(RunString))
                
                local s,e=pcall(function()
                	%s
                end)
                
                -- Owner:ChatPrint('State: '..tostring(s))
                -- Owner:ChatPrint('Error: '..tostring(e))
                -- Owner:ChatPrint('- End pcall- ')
                "
                ,
                Owner:steamID(),
                String
            )
        # Lua:set("RunPassedString",Exec)
        luaRun(Exec)
    }
    
    function lua_Explosion(V:vector) {
        local String=format(Lua_Explosion,V:x(),V:y(),V:z())
        lua_Run(String)
    }
    
    function lua_SpawnWeapon(Class:string,Pos:vector) {
        local String=
        format(
            "
            Owner:ChatPrint('Spawning weapon...')
            local Class=[[%s]]
            local Model=[[%s]]
            local Position=Vector(%d,%d,%d)
            
            local Weapon=ents.Create('spawned_weapon')
            Weapon:SetWeaponClass(Class)
            Weapon:SetModel(Model)
            Weapon.ammoadd = weapons.Get(Class) and weapons.Get(Class).Primary.DefaultClip or 0
            Weapon.ShareGravgun = true
            Weapon:SetPos(Position)
            Weapon.nodupe = true
            Weapon:Spawn()
            ",
            
            Class,
            ShipmentMetaData[Class,table]["Model",string],
            Pos:x(),Pos:y(),Pos:z()
        )
        lua_Run(String)
    }
    
    ########################
    # Admin Weapon Spawner #
    ########################
    
    if(SpawnWeapons) {
        foreach(Type,V:table=ShipmentMetaData) {
            local Dat=ShipmentMetaData[Type,table]
            Dat["Entity",entity]=noentity()
            Dat["Cooldown",number]=Time
            Dat["Max",number]=9e99
            Items[Type,table]=Dat
        }
    }
}

# Chat Commands

if(chatClk() & lastSaid():sub(1,1)=="!") {
    local Ply=lastSpoke()
    local Cmd=Ply:lastSaid():sub(2):explode(" ")
    local Command=Cmd:shiftString()
    
    if(lastSpoke()==Owner) {
        hideChat(1)
        if(Chat[Command,string]) {
            Chat[Command,string](Cmd)
        } else {
            printColor(vec(255,255,0),"[sNexus] ",vec(255),"No such owner command: ",vec(255,0,0),Command,vec(255),".")
        }
    } else {
        if(Command=="qbuy" & Cmd[1,string]) {
            local Ray=rangerOffset(Ply:pos()+Ply:aabbSize()*vec(0,0,1.5),Ply:pos()*vec(1,1,0)+vec(0,0,9000))
            if(!Ray:hit()) {
                cmd_playerBuyItem(Ply,Cmd[1,string])
            } else {
                ulx_PM(Ply,"You must have direct line of sight to the sky to use !qbuy.")
            }
        }
    }
}

# Processing
if(moneyClk()) {
    local Ply=moneyClkPlayer()
    ulx_PM(Ply,"Your order has been pushed to processing.  A cannister will land nearby.")
    local New=Processing[Ply:steamID(),table]
    for(I=1,New["Order",array]:count()) {
        local V=New["Order",array][I,string]
        Items[V,table]["Max",number]=Items[V,table]["Max",number]-1
    }
    Queue:pushTable(New)
    Processing:removeTable(Ply:steamID())
    
} elseif (moneyNoClk()) {
    local Ply=moneyNoClkPlayer()
    Processing:removeTable(Ply:steamID())
    printColor(vec(255,255,0),"[sNexus] ",vec(255,0,0),Ply:name()+"'s",vec(255)," processing order failed.")
    ulx_PM(Ply,"Your order failed because you didn't pay.")
}

if(clk("TimeoutCheck")) {
    foreach(I,V:table=Processing) {
        local Ply=V["Owner",entity]
        local Expires=V["Expires",number]
        if(Time>=Expires) {
            Processing:removeTable(Ply:steamID())
            ulx_PM(Ply,"Your order timed out.")
            
            printColor(vec(255,255,0),"[sNexus] ",vec(255,0,0),Ply:name()+"'s",vec(255)," request timed out.")
        }
    }
}

# Vending
local Current=Queue[1,table]
if(Current & Current["Landed",number]) {
    setName("sShopNexus\nProcessing order for:\n"+Queue[1,table]["Owner",entity]:name())
    if(!Current["Box",entity]) {
        local Box=propSpawn("models/items/item_item_crate.mdl",Current["EjectPos",vector],0)
        Current["Box",entity]=Box
        Current["BoxScale",number]=0.1
        Current["Box",entity]:applyForce(Current["EjectVel",vector]*5000+(Box:pos()-Current["Owner",entity]:attachmentPos("eyes"))*-Box:mass())
        holoCreate(100,Box:pos(),vec(0.1),Box:angles())
        holoParent(100,Box)
        holoModel(100,Box:model())
        Box:setAlpha(0)
        if(SpawnWeapons) {
            Queue[1,table]["SendTime",number]=Time+4
        }
    }
    if(Current["Order",array]:count()>0 & !SpawnWeapons) {
        local Item=Current["Order",array][1,string]
        if(Items[Item,table]["Cooldown",number]<Time) {
            Items[Item,table]["Cooldown",number]=Time+2
            # Items[Item,table]["Entity",entity]:set("use",1)
            local LuaStr=format("Entity(%d):Use(Entity(%d),Entity(%d),3,0)",Items[Item,table]["Entity",entity]:id(),Entity:id(),Entity:id())
            lua_Run(LuaStr)
            # print(LuaStr)
            Queue[1,table]["Order",array]:shiftString()
            Queue[1,table]["SendTime",number]=Time+4
        }
    } elseif (!Current["Sent",number] & Current["SendTime",number]<Time) {
        if(!SpawnWeapons) {
            findIncludeClass("spawned_weapon")
            findInSphere(Entity:pos(),300)
            All=findToArray()
            local I=0
            foreach(I,V:entity=All){
                I++
                if(V:isValid()) {
                    V:setEntPos(Current["Box",entity]:pos()+vec(0,0,I*10))
                }
            }
        } else {
            local Iteration=0
            foreach(I,V:string=Current["Order",array]) {
                Iteration++
                lua_SpawnWeapon(V,Current["Box",entity]:pos()+vec(0,0,Iteration*10))
            }
        }
        Current["Box",entity]:propBreak()
        Queue:removeTable(1)
        holoDelete(100)
        #holoDelete(1)
        
        # Cleanup missing shipments
        cleanMissing()
    }
    Current["BoxScale",number]=clamp(Current["BoxScale",number]+0.03,0,1)
    holoScale(100,vec(Current["BoxScale",number]))
} elseif (Current & !Current["Launched",number]) {
    Queue[1,table]["Launched",number]=1
    local Pos=Current["Owner",entity]:attachmentPos("eyes")+Current["Owner",entity]:eyeAngles():forward()*200+randvec(vec(-500,-500,0),vec(500,500,0))
    local Ray=rangerOffset(Pos,Pos:setZ(-1000))
    local End=Ray:pos()
    local Start=vec(0,0,-9e99)
    while(!Start:isInWorld()) {
        Start=End:setZ(5000)+vec(randint(-3000,3000),randint(-3000,3000),0)
    }
    # local Ranger=rangerOffset(Current["Owner",entity]:attachmentPos("eyes")+vec(0,0,10),Current["Owner",entity]:attachmentPos("eyes")+vec(0,0,10000))
    # local Start=Ranger:pos()
    local MaxID=1
    foreach(I,V:table=Cannister["Active",table]) {
        if(V["ID",number]>=MaxID) {MaxID=V["ID",number]+1}
    }
    spawnHeadcrabCannister(MaxID,Start,End,"onCannisterLanded")
} elseif (Current & !Current["Landed",number]) {
    setName("sShopNexus\nDelivery in progress")
} else {
    setName("sShopNexus\nIdle")
}

if(clk("Cannister_Loop")) {
    if(Cannister["Count",number]>0) {
        timer("Cannister_Loop",100)
    }
    
    foreach(I,V:table=Cannister["Active",table]) {
        if(V["Timer",number]==0) {
            local Offset=(V["End",vector]-V["Start",vector]):normalized()*V["Speed",number]
            local Final=V["Pos",vector]+Offset
            local Ray=rangerOffset(V["Pos",vector],Final)
            local FinPos=Ray:pos()+vec(0,0,5)
            holoAng(V["ID",number],Offset:toAngle()+ang(-180,0,0))
            if(!Ray:hit()) {
                Cannister["Active",table][I,table]["Pos",vector]=Final
                holoPos(V["ID",number],Final)
            } else {
                holoEntity(V["ID",number]):soundPlay("Explode",0,"HeadcrabCanister.SkyboxExplosion")
                Cannister["Active",table][I,table]["Pos",vector]=FinPos
                holoPos(V["ID",number],FinPos)
                lua_Explosion(FinPos)
                Cannister["Active",table][I,table]["Timer",number]=0.1
            }
        } else {
            Cannister["Active",table][I,table]["Timer",number]=Cannister["Active",table][I,table]["Timer",number]+0.1
            if(Cannister["Active",table][I,table]["Timer",number]==3) {
                holoAnim(V["ID",number],"open")
                holoEntity(V["ID",number]):soundPlay("Open",0,"HeadcrabCanister.Open")
            } elseif (V["Timer",number]<5) {
            } elseif (V["Timer",number]==5) {
                local Ent=holoEntity(V["ID",number])
                V["Callback",string](Ent:toWorld(vec(30,0,0)),Ent:forward())
                #holoEntity(V["ID",number]):soundPlay(Cannister["Sound",number]+1,0,"garrysmod/balloon_pop_cute.wav")
                holoEntity(V["ID",number]):soundPlay("Eject",0,"doors/door_metal_thin_close2.wav")
            } elseif (V["Timer",number]<15) {
            } elseif (V["Timer",number]<20) {
                local Iteration=(20-V["Timer",number])/5
                holoAlpha(V["ID",number],Iteration*255)
            } else {
                holoDelete(V["ID",number])
                Cannister["Count",number]=Cannister["Count",number]-1
                Cannister["Active",table]:removeTable(I)
            }
        }
    }
}

if(clk("ads")) {
    timer("ads",120000)
    
    local Weapons=array()
    local Selected=array()
    
    foreach(I,V:table=ShipmentMetaData) {
        if(Items[I,table]["Max",number]<=0) {continue}
        Weapons:pushString(V["Name",string]:explodeRE("\ |\-")[1,string]:lower())
        }
    for(I=1,3) {
        Selected:pushString(Weapons:removeString(randint(Weapons:count())))
    }
    
    concmd("say \"/advert sShop Nexus: Instant delivery, great prices.  !qbuy "+Selected:concat(", !qbuy ")+"\"")
}

