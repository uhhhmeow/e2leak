@name Aperture Door
@inputs [A B C D]:entity Open
@persist Cur [Aa Ab Ba Bb Ca Cb Da Db]:vector Active
@persist [Anaa Anba Anca Anda]:angle

Cur=clamp(Cur+(Open ? 0.1 : -0.1),0,1)

#A B
#D C

if (((~A | ~B | ~C | ~D) | first()) & (A&B&C&D)) {
    Aa=A:pos()
    Ba=B:pos()
    Ca=C:pos()
    Da=D:pos()
    Anaa=A:angles()
    Anba=B:angles()
    Anca=C:angles()
    Anda=D:angles()
    
    Anab=A:angles()+ang(0,0,45)
    Anbb=A:angles()+ang(0,0,45)
    Ancb=A:angles()+ang(0,0,45)
    Andb=A:angles()+ang(0,0,45)
    Size=47.5
    Ab=Ba+vec(Size,0,0)
    Bb=Ca-vec(0,0,Size)
    Cb=Da-vec(Size,0,0)
    Db=Aa+vec(0,0,Size)
    
    Active=1
}

if (Active) {
    A:setPos(Cur * (Ab-Aa) + Aa)
    A:setAng(Cur * (Anab+Anaa) + Anaa)
    B:setPos(Cur * (Bb-Ba) + Ba)
    B:setAng(Cur * (Anbb+Anba) + Anba)
    C:setPos(Cur * (Cb-Ca) + Ca)
    C:setAng(Cur * (Ancb+Anca) + Anca)
    D:setPos(Cur * (Db-Da) + Da)
    D:setAng(Cur * (Andb+Anda) + Anda)
}

interval(100)
