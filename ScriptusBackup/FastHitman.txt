@name FastHitman
@persist Target:entity Price Current:entity Buyer:entity Cur Credit
@inputs Pot:wirelink AMT
@outputs Eject
runOnChat(1)
Price=300
local Ply=lastSpoke()
CMD=lastSaid():sub(2)
Args=CMD:explode(" ")
PM=Ply:name():explode(" ")[1,string]
if (chatClk() & Ply==owner() & Ply:lastSaid():sub(1,1)=="-") {
    hideChat(1)
    if (Args[1,string]=="accept") {
        timer("sayaccept",1000)
    }
    if (Args[1,string]=="complete") {
        Target=noentity()
        timer("saydone",1000)
    }
    if (Args[1,string]=="ad") {
        timer("sayad",1000)
    }
    
} elseif (chatClk() & Ply:lastSaid():sub(1,1)=="-" & !Buyer) {
    if (Args[1,string]=="hit" & !Buyer & !Target) {
        Players=players()
        local Him=noentity()
        for (I=1, Players:count()) {
            if (Players[I,entity]:name():lower():matchFirst(Args[2,string]:lower())) {
                Him=Players[I,entity]
                break
            }
        }
        if (!Him) {
            concmd("say /pm "+PM+" 404: Player not found.  Try again?")
        } else {
            concmd("say /pm "+PM+" Please insert $"+Price+" above to place a hit on "+Him:name())
            Buyer=Ply
            Current=Him
            Pot:entity():setPos(Buyer:attachmentPos("eyes")+vec(0,0,20))
            Pot:entity():propNotSolid(0)
            Pot:entity():setAlpha(255)
            timer("timeout",20000)
        }
    }

}
if (Buyer) {PM=Buyer:name():explode(" ")[1,string]}
if (AMT==Price & !Target) {
    Pot:entity():setPos(owner():pos()+vec(0,0,100))
    Pot:entity():setAlpha(0)
    Pot:entity():propNotSolid(1)
    Eject=AMT
    stoptimer("timeout")
    Target=Current
    Pot:entity():propNotSolid(0)
    concmd("say /pm "+PM+" Hit accepted.")
    timer("sayaccept",1000)
    print("Target: "+Target:name())
} elseif (AMT>Price & !Target) {
    Eject=AMT
}
if (clk("timeout")) {
    Pot:entity():setPos(owner():pos()+vec(0,0,100))
    Pot:entity():setAlpha(0)
    Pot:entity():propNotSolid(1)
    Eject=AMT
    concmd("say /pm "+PM+" Purchase timed out.")
}
if (first() | changed(Pot)) {Cur=AMT}
if (clk("sayaccept")) {
    concmd("say /advert Hit accepted!")
}
if (clk("saydone")) {
    concmd("say /advert Hit completed!")
}
if (clk("sayad")) {
    concmd("say /advert Say -hit [Player] to request a hit!")
}
Pot:entity():setAng(ang(180,0,0))
