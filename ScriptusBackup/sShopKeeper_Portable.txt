@name sShopKeeper Portable
@persist [Owner Entity User]:entity [Chatter Footstep]:array LastChatter

@persist Graph:table Open:table

@persist Path:array CurPath CurPos:vector
Time=curtime()

if(first()) {
    #include "lib/ct/shipmentmetadata"
    #include "e2shared/graph_gm_construct"
    
    # super-small a* pathing function
    
    function table path(A:string,B:string,Format) {
        local Out=array()
        Graph=Graph_Construct:clone()
        Open=table()
        Open[A,number]=1
        
        Graph[A,table]["parent",string]="__END"
        Graph[A,table]["s",number]=2
        
        local StartPos=Graph[A,table]["Pos",vector]
        local EndPos=Graph[B,table]["Pos",vector]
        local HeightOffset=vec(0,0,15)
        local Traversed_Nodes=0
        local Traversed_Distance=0
        local Nodes_Checked=0
        local Dbg=5
        local OpenAmt=1
        
        while(perf()) {
            if(OpenAmt==0) {break}
            local BestGuess=""
            local BestH=9e99
            local BestState=0
            
            foreach(I,V:number=Open) {
                if(V!=1) {continue}
                local ThisPos=Graph[I,table]["Pos",vector]
                local Total=Graph[Graph[I,table]["parent",string],table]["heur",number]
                local LastPos=Graph[Graph[I,table]["parent",string],table]["Pos",vector]
                
                local Traverse=(ThisPos-LastPos):length()
                local Heur=
                    Total+
                    (EndPos-ThisPos):length()+
                    Traverse
                
                Dbg++
                holoCreate(Dbg,(ThisPos+LastPos)/2,vec(Traverse/3,0.5,0.5),(ThisPos-LastPos):toAngle())
                holoModel(Dbg,"models/hunter/plates/plate1.mdl")
                holoMaterial(Dbg,"debug/debugdrawflat")
                
                # prevent moving through entities
                local Ranger=rangerOffset(
                    ThisPos+HeightOffset,
                    LastPos+HeightOffset
                )
                
                #[if(Ranger:entity()) {
                    holoColor(Dbg,vec(255,0,0))
                    Dbg++
                    holoCreate(Dbg,Ranger:pos(),vec(3),ang())
                    holoColor(Dbg,vec(255,0,0))
                    holoMaterial(Dbg,"debug/debugdrawflat")
                    
                    Graph[I,table]["s",number]=2
                    Open:removeString(I)
                    continue
                }]#
                
                Graph[I,table]["heur",number]=Heur
                Graph[I,table]["len",number]=Traverse
                
                if(Heur<BestH) {
                    BestGuess=I
                    BestH=Heur
                    BestState=V
                }
            }
            
            print(BestGuess,BestState)
            
            if(BestState!=1) {
                return table("success"=0,"error"="Could not find path.","path"=array(),"checked"=Nodes_Checked)
            } elseif (BestGuess==B) {
                local Parent=B
                print("got")
                while(perf()) {
                    if(Parent=="__END") {break}
                    switch(Format) {
                        case 0,
                            Out:unshiftString(Parent)
                        break
                        case 1,
                            Out:unshiftVector(Graph[Parent,table]["Pos",vector])
                        break
                    }
                    Traversed_Nodes++
                    Traversed_Distance+=Graph[Parent,table]["len",number]
                    Parent=Graph[Parent,table]["parent",string]
                }
                return table("success"=1,"path"=Out,"nodes"=Traversed_Nodes,"dist"=Traversed_Distance,"checked"=Nodes_Checked)
            }
            
            Open[BestGuess,number]=2 #:removeString(BestGuess)
            OpenAmt--
            Graph[BestGuess,table]["s",number]=2
            
            foreach(I,V:string=Graph[BestGuess,table]["N",array]) {
                if(!Graph[V,table]["s",number]) {
                    Graph[V,table]["s",number]=1
                    Graph[V,table]["parent",string]=BestGuess
                    Nodes_Checked++
                    OpenAmt++
                    Open[V,number]=1
                    Dbg++
                    holoCreate(Dbg,Graph[V,table]["Pos",vector])
                    holoColor(Dbg,vec(0,0,255))
                    holoMaterial(Dbg,"debug/debugdrawflat")
                }
            }
        }
    }
    
    function table path(A:string,B:string) {
        return path(A,B,0)
    }
    
    Owner=owner()
    Entity=entity()
    holoCreate(1,Entity:toWorld(vec()),vec(1),Entity:toWorld(ang()))
    holoModel(1,"models/player/eli.mdl")
    holoPlayerColor(1,vec(255))
    holoSetPose(1,"move_x",1)
    holoPlayerColor(1,vec(255))
    holoAnim(1,"run_all_01")
    holoBoneScale(1,"ValveBiped.Bip01_Head1",vec(0.8))
    
    #holoCreate(2,Entity:toWorld(vec(17,2.5,43)),vec(0.6),Entity:toWorld(ang()))
    #holoModel(2,"models/Items/item_item_crate.mdl")
    #holoParentAttachment(2,holoEntity(1),"anim_attachment_RH")
    
    Chatter=array(
        "eli_lab.eli_portal01",
        "eli_lab.eli_portal02",
        "eli_lab.eli_thing",
        "eli_lab.eli_vilebiz01",
        "eli_lab.eli_vilebiz02",
        "eli_lab.eli_vilebiz03",
        "eli_lab.eli_vilebiz04"
    )
    
    Footstep=array(
        "player/footsteps/cleats_conc_01.wav",
        "player/footsteps/cleats_conc_02.wav",
        "player/footsteps/cleats_conc_03.wav",
        "player/footsteps/cleats_conc_04.wav"
    )
    
    
    function number chatter() {
        if(Time<LastChatter) {return 1}
        LastChatter=Time+5
        holoEntity(1):soundPlay(0,0,Chatter[randint(Chatter:count()),string])
        soundPitch(0,255)
        return 0
    }
    
    local Start="51772"
    local End="35654"
    local Out=path(Start,End,1)
    if(Out["success",number]) {
        Path=Out["path",array]
        print("Node count: "+Path:count())
        printTable(Out)
        holoPos(1,Path[1,vector])
        CurPath=1
        CurPos=Path[1,vector]
        
        timer("runpath_init",0)
        timer("runsound",500)
        Running=1
    } else {
        printTable(Out)
    }
}

if(randint(1,10)==5) {
    chatter()
}

if(clk("runsound")) {
    timer("runsound",500)
    if(Running) {
        holoEntity(1):soundPlay("Footstep",0,Footstep[randint(Footstep:count()),string])
        soundPitch("Footstep",randint(100,125))
        soundVolume("Footstep",1)
    }
}

if(clk("runpath_init")) {
    if(CurPath<=Path:count()) {
        timer("runpath_init",100)
        local TargetPos=Path[CurPath,vector]
        local RDiff=(TargetPos-CurPos)
        local Diff=RDiff:normalized()
        CurPos=CurPos+Diff*15
        holoAng(1,RDiff:toAngle()*ang(0,1,0))
        holoPos(1,CurPos)
        if(RDiff:length()<15) {CurPath++}
        if(CurPath>Path:count()) {
            holoAnim(1,"idle_all_01")
            timer("spawnbox",1000)
            Running=0
        }
    }
}

if(clk("spawnbox")) {
    holoAnim(1,"idle_magic")
}
