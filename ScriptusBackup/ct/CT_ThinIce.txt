@name CT ThinIce
@persist [Plates Players]:array Reward Countdown Maxcount Price Waiting On Timeout Fling
@persist Player:entity Stopping [Door1 Door2 Door3]:entity Loading Updateplayers
@persist Starting Melting:array Speed Stages Stage:array StageColor:array
@persist [TimeQue Fail Available]:array AnimX AnimTime Mode:string Modes:array
@persist ChanceMode Props:array Occupied:array
@inputs [Scr Playscr]:wirelink User:entity [DoorEnter DoorIn DoorOut Landing]:wirelink
@inputs Plates:array
@outputs Flingout Flingin
if (first()) {
    Price=50
    Reward=2000
    Timeout=10 #time to pay
    Fling=50000 #rate of the forcers
    Maxcount=30 #cap for countdown
    Speed=3 #speed in seconds for plates to melt
    Stages=10 #number of hits the ice can take
    ChanceMode=3 #chance to get a special mode
    Modes=array("Sudden Death","Crowbar Fight","Rainbows","Rain")
    for (I=1, Stages) {
        local Minus=(255/Stages)*I
        StageColor[I,vector]=vec(255,255,255)-vec(0,Minus,Minus)
        #StageColor[I,vector]=hsv2rgb((360/Stages)*I,1,1)
    }
    timer("countdown",1000)
    for (I=1, Plates:count()) {
            Plates[I,entity]:setAlpha(255)
            Plates[I,entity]:propNotSolid(0)
    }
    function number occupied(E:entity) {
        for (I=1, Occupied:count()) {
            if (Occupied[I,entity]==E) {return 1}
        }
        return 0
    }
    function entity getPlate() {
        local Ent=Plates[randint(1,Plates:count()),entity]
        while (occupied(Ent)) {
            Ent=Plates[randint(1,Plates:count()),entity]
        }
        Occupied[Occupied:count()+1,entity]=Ent
        return Ent
    }
    function prop(Num) {
        local Models=array(
        "models/props_c17/oildrum001.mdl",
        "models/props_borealis/bluebarrel001.mdl",
        "models/props_c17/FurnitureFridge001a.mdl",
        "models/props_interiors/Furniture_Couch02a.mdl",
        "models/props_wasteland/controlroom_filecabinet002a.mdl")
        local Model=Models[randint(Models:count()),string]
        local E=propSpawn(Model,1)
        local Pos=vec(0,0,0)
        if (Num!=2) {
            Pos=getPlate():pos()+vec(0,0,E:aabbSize():z())
        } else {
            Pos=getPlate():pos()+vec(0,0,E:aabbSize():z()+500)
        }
        E:setPos(Pos)
        E:setAng(ang(0,random(360),0))
        if (!Num) {
            E:setAlpha(128)
            E:propNotSolid(1)
        } else {
            E:propFreeze(0)
        }
        E:setMass(251)
        if (Num==2) {E:setMass(50000)}
        Props[Props:count()+1,entity]=E
    }
    function number isPlaying(E:entity) {
        for (I=1, Players:count()) {
            if (Players[I,entity]==E) {
                return 1
            }
        }
        return 0
    }
    function open(Self:entity,Num) {
        #print("Setstate: "+Num)
        if (Num) {
            Self:setAlpha(0)
            Self:propNotSolid(1)
        } else {
            Self:setAlpha(255)
            Self:propNotSolid(0)
        }
    }
    function setLoop(S:string) {
        soundPurge()
        for (I=1, 7) {
            Scr:entity():soundPlay(I,0,S)
        }
    }
    function sound(S:string) {
        soundStop(8)
        soundStop(9)
        soundStop(10)
        soundStop(11)
        for (I=1, 4) {
            Scr:entity():soundPlay(7+I,0,S)
        }
    }
    function number melting(E:entity) {
        for (I=1, Melting:count()) {
            if (Melting[I,entity]==E) {return 1}
        }
        return 0
    }
    function number getID(E:entity) {
        for (I=1, Plates:count()) {
            if (Plates[I,entity]==E) {return I}
        }
        return 0
    }
    function number getAvaPlate(E:entity) {
        for (I=1, Available:count()) {
            if (Available[I,entity]==E) {return I}
        }
        return 0
    }
        Fail=array(
        "/ct_forcedownload/ctgame_localfail1.wav",
        "/ct_forcedownload/ctgame_localfail2.wav",
        "/ct_forcedownload/ctgame_localfail3.wav"
        )
    TimeQue=array(
        30="/ct_forcedownload/ctb_30sec.wav",
        20="/ct_forcedownload/ctb_20sec.wav",
        10="/ct_forcedownload/ctb_10sec.wav",
        5="/ct_forcedownload/ct_5sec.wav",
        4="/ct_forcedownload/ct_4sec.wav",
        3="/ct_forcedownload/ct_3sec.wav",
        2="/ct_forcedownload/ct_2sec.wav",
        1="/ct_forcedownload/ct_1sec.wav"
    )
    open(Door1,1)
    setLoop("ct_forcedownload/ctgame_transit1.mp3")
    timer("long",1200)
}
if (changed(Scr) | first()) {
    Scr:egpClear()
}
if (changed(Playscr) | first()) {
    Playscr:egpClear()
}

interval(100)
if (first() | changed(DoorEnter) | changed(DoorIn) | changed(DoorOut)) {
    Door1=DoorEnter:entity()
    Door2=DoorIn:entity()
    Door3=DoorOut:entity()
}
#ifdef moneyRequest(entity,number,string,number)
if (User & !On & !Waiting & !Starting & !isPlaying(User) & User:pos():distance(Playscr:entity():pos())<60) {
   # moneyRequest(entity ply,number amount,string title,number timeout)
    moneyRequest(User,Price,"Thin Ice",Timeout)
    Player=User
    Updateplayers=1
    open(Door1,0)
    timer("timeout",Timeout*1000)
}
#else
if (User & !On & !Waiting & !Starting & !isPlaying(User) & User:pos():distance(Playscr:entity():pos())<100) {
    stoptimer("timeout")
    Player=User
    Players[Players:count()+1,entity]=Player
    Waiting=0
    Flingin=Fling
    Countdown+=5
    setLoop("ct_forcedownload/ctgame_entergame.mp3")
    timer("long",2700)
    open(Door2,1)
    concmd("FAdmin")
    
}

#endif
if (changed(On) | changed(Playscr) | first()) {
    Playscr:egpBox(3,vec2(256,256),vec2(512,512))
    Playscr:egpMaterial(3,"skybox/sky_halloweendn")
    Playscr:egpBox(1,vec2(256,256),vec2(512,50))
    Playscr:egpText(2,"Press Anywhere to Begin",vec2(256,256))
    Playscr:egpAlign(2,1,1)
    Playscr:egpSize(2,40)
    if (!On) {
        Playscr:egpColor(1,vec(0,128,0))
    } else {
        Playscr:egpSetText(2,"Game in Progress")
        Playscr:egpColor(1,vec(128,0,0))
    }
}
if (clk("timeout")) {
    Waiting=0
    Flingout=Fling
    open(Door3,1)
    local Name=toChar(34)+(Player:name())+toChar(34)
    concmd("fadmin sethealth "+Name+" 1")
    Player=noentity()
}
#ifdef moneyClk()
if (moneyClk()) {
    stoptimer("timeout")
    Players[Players:count()+1,entity]=Player
    Waiting=0
    Flingin=Fling
    Countdown+=5
    setLoop("ct_forcedownload/ctgame_entergame.mp3")
    timer("long",2700)
    open(Door2,1)
    concmd("FAdmin")
}
if (moneyNoClk()) {
    stoptimer("timeout")
    
    Waiting=0
    Flingout=Fling
    open(Door3,1)
    Player=noentity()
}
#endif
if (Flingout | Flingin & !Stopping) {
    timer("stopping",1000)
    Stopping=1
}
if (clk("stopping")) {
    Flingin=0
    Flingout=0
    Stopping=0
    open(Door1,1)
    open(Door2,0)
    open(Door3,0)
}
open(Landing:entity(),On)

#disco
if (changed(On) & !On) {Loading=0}
if (clk("disco")) {
if (!On & !Starting) {
    for (I=1, Plates:count()) {
        Plates[I,entity]:setColor(vec(random(255),random(255),random(255)))
    }
} elseif (changed(On) & !Starting) {
    for (I=1, Plates:count()) {
        Plates[I,entity]:setColor(vec(255,255,255))
    }
}
timer("disco",1000)
}
if (first()) {timer("disco",1000)}

if (Players:count()>1 & !Loading) {
    Loading=1
}
if (changed(Loading)) {
    Countdown=Maxcount
}
if (clk("long")) {
    setLoop("ct_forcedownload/ctgame_longloop.wav")
}
#kick losers
for (I=1, Players:count()) {
    if (!Players[I,entity]:isAlive() | !Players[I,entity]:isValid() | Players[I,entity]:pos():z()+10<Plates[1,entity]:pos():z()) {
        print(Players[I,entity]:name()+" has been eliminated.")
        sound(Fail[randint(Fail:count()),string])
        Players:removeEntity(I)
        Updateplayers=1
    }
}
#screen
if (clk()) {
    Scr:egpClear()
    Scr:egpText(1,"Canadian's",vec2(256-(271-256),0))
    Scr:egpSize(1,100)
    Scr:egpAlign(1,2,1)
    Scr:egpText(2,"Turf",vec2(271,0))
    Scr:egpAlign(2,0,1)
    Scr:egpColor(2,vec(255,0,0))
    Scr:egpSize(2,100)
    #Scr:egpFont(1,"Coolvetica")
    #Scr:egpFont(2,"Coolvetica")
    Scr:egpText(3,"Thin Ice",vec2(201,50))
    Scr:egpColor(3,vec(0,255,255))
    Scr:egpSize(3,70)
        for (I=1, Players:count()) {
        Index=20+I*5
        Scr:egpBox(Index,vec2(0,30+(I*60)),vec2(256,50))
        Scr:egpBox(Index+1,Scr:egpPos(Index),vec2(251,45))
        Scr:egpColor(Index,vec(128,128,128))
        Scr:egpColor(Index+1,vec(172,172,172))
        Scr:egpText(Index+2,Players[I,entity]:name(),Scr:egpPos(Index))
        Scr:egpSize(Index+2,30)
        Scr:egpColor(Index+2,vec(0,0,255))
        Scr:egpAlign(Index+2,1,1)
    }
    if (Loading & !On) {
        Scr:egpCircle(4,vec2(325,256),vec2(80,80))
        Scr:egpCircle(5,Scr:egpPos(4),vec2(70,70))
        Scr:egpColor(4,vec(128,128,128))
        Scr:egpColor(5,vec(172,172,172))
        Scr:egpText(6,""+Countdown,Scr:egpPos(5))
        Scr:egpAlign(6,1,1)
        Scr:egpSize(6,40)
        Scr:egpColor(6,vec(0,0,255))
    }
    if (Mode) {
        Scr:egpBox(7,vec2(325,400),vec2(205,45))
        Scr:egpBox(8,vec2(325,400),vec2(200,40))
        Scr:egpText(9,Mode+"!",vec2(325,400))
        Scr:egpAlign(9,1,1)
        Scr:egpColor(9,vec(255,0,0))
        Scr:egpSize(9,30)
        Scr:egpColor(7,vec(128,128,128))
        Scr:egpColor(8,vec(172,172,172))
    }
}
#timers
if (clk("countdown")) {
    timer("countdown",1000)
    if (Loading & !On & !Starting) {
        Countdown--
        if (TimeQue[Countdown,string]) {
            sound(TimeQue[Countdown,string])
        }
        if (Countdown<1) {
            Loading=0
            Starting=1
            setLoop("ct_forcedownload/ctgame_beginningintro.mp3")
            AnimTime=(21/Plates:count())*1000
            #select mode
            if (randint(1,ChanceMode)==1) {
                Mode=Modes[randint(Modes:count()),string]
        } else {Mode="Normal"}
            timer("startup",21000)
            timer("ultimatecountdown",AnimTime)
            for (I=1, randint(6,10)) {prop(0)}
        }
    }
}
if (clk("ultimatecountdown")) {
    AnimX++
    if (AnimX<=Plates:count()) {
        Plates[AnimX,entity]:setColor(vec(255,255,255))
        if (AnimX<Plates:count()) {timer("ultimatecountdown",AnimTime)}
    }
}

if (clk("startup")) {
    #timer("long",1000)
    print("Game is starting!")
    Available=Plates
    On=1
    HPP=100-58
    for (I=1, Props:count()) {
        Props[I,entity]:setAlpha(255)
        Props[I,entity]:propNotSolid(0)
        Props[I,entity]:propFreeze(0)
    }
    for (I=1, Players:count()) {
        local Player=Players[I,entity]
        local Name=toChar(34)+(Player:name())+toChar(34)
        if (Mode!="Normal") {
            setLoop("ct_forcedownload/ctgame_new5.mp3")
            timer("long",2500)
    } else {timer("long",1000)}
        if (Mode=="Crowbar Fight") {
            #concmd("fadmin strip "+Name)
            concmd("fadmin giveweapon "+Name+" weapon_crowbar")
            concmd("fadmin sethealth "+Name+" "+(100+HPP))
        } elseif (Mode=="Sudden Death") {
            concmd("fadmin sethealth "+Name+" 1")
        } elseif (Mode=="Rainbows") {
            for (I=1, Stages) {
            #local Minus=(255/Stages)*I
            #StageColor[I,vector]=vec(255,255,255)-vec(0,Minus,Minus)
            StageColor[I,vector]=hsv2rgb((360/Stages)*I,1,1)
        }
        } else {
            concmd("fadmin sethealth "+Name+" "+(100+HPP))
        }
    }
}
if (On) {
    #take all the players and set the plates below them to melt
    findByClass("prop_physics")
    findClipToEntities(Available)
    for (I=1, Players:count()) {
        local Plate=findClosest(Players[I,entity]:pos())
        ID=getID(Plate)
        Stage[ID,number]=Stage[ID,number]+1
        Plate:setColor(StageColor[Stage[ID,number],vector])
        if (Stage[ID,number]>Stages) {
            Plate:setAlpha(128)
            Plate:propNotSolid(1)
            Melting[ID,number]=1
            Available:removeEntity(getAvaPlate(Plate))
        }
    }
    if (Players:count()<=0) {
        timer("resetti",5000)
    }
    if (Players:count()==1) {
        sound("ct_forcedownload/ctgame_win3.mp3")
        #ifdef moneyGive(entity,number)
        moneyGive(Players[1,entity],Reward)
        #endif
        if (Mode=="Sudden Death") {
            local Name=toChar(34)+(Players[1,entity]:name())+toChar(34)
            concmd("fadmin sethealth "+Name+" 100")
        }
        
        for (I=1, Plates:count()) {
            Plates[I,entity]:setAlpha(128)
            Plates[I,entity]:propNotSolid(1)
            Players:clear()
        }
    }
}

if (clk("resetti")) {
    for (I=1, Props:count()) {
        Props[I,entity]:propDelete()
    }
    reset()
}
for (I=1, Props:count()) {
    if (Props[I,entity]:pos():z()<Plates[1,entity]:pos():z()) {
        Props[I,entity]:propDelete()
        Props:removeEntity(I)
    }
}

if (Mode=="Rain" & randint(1,15)==3 & On) {
    prop(2)
}
