@name Square-er
@persist X Y [Model Mat]:string Size LineX LineY All Start:vector Running
@outputs Plates:array
@inputs Run
if (first()) {
    Model=entity():isWeldedTo():model()
    Size=entity():isWeldedTo():aabbSize():x()
    Start=entity():isWeldedTo():pos()
    Mat=entity():isWeldedTo():getMaterial()
    All=5 #size of arena
}
if (Run & !Running) {
    for (I=1, Plates:count()) {
        Plates[I,entity]:propDelete()
    }
    Plates:clear()
    Running=1
    LineX=0
    LineY=0
    Model=entity():isWeldedTo():model()
    Size=-entity():isWeldedTo():aabbSize():x()
    Start=entity():isWeldedTo():pos()
    Mat=entity():isWeldedTo():getMaterial()
    All=10 #size of arena
    timer("Make",1000)
    print("Building square...")
}

if (clk("Make")) {
        LineX++
    if (LineX>All) {
        LineY++
        LineX=1
        #print("Row "+LineY)
    }
    if (LineY<=All-1) {
    E=propSpawn(Model,Start+vec(Size*(LineX),Size*(LineY),0),1)
    #E:setMaterial(Mat)
    #E:setColor(vec(random(255),random(255),random(255)))
    Plates[Plates:count()+1,entity]=E
        timer("Make",50)
    } else {
        print("Done!")
        Running=0
        for (I=1, Plates:count()) {
            #Plates[I,entity]:setColor(vec(255,255,255))
        }
    }
}
