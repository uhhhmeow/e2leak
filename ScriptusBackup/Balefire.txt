@name Balefire
@persist [Owner Entity]:entity
@persist Attacking

interval(100)
Attacking=Owner:weapon():type()=="weapon_physcannon" && Owner:keyAttack1()

if(first()) {
    Owner=owner()
    Entity=entity()
    
    function number vector:spawnExplosion(Radius:number,Delay:number) {
        luaRun(format("
            timer.Simple(%d / 240,function()
                local Pos=Vector(%d,%d,%d)
                -- for i=1,1 do ParticleEffect(\"grenade_final\",Pos,Angle(0, 0, 0),nil) end
                util.ScreenShake(Pos,500,500,2,500)
                
                local Explode=ents.Create('env_explosion')
        	       Explode:SetPos(Pos)
        	       Explode:Spawn()
        	       Explode:SetKeyValue('iMagnitude','%d')
        	       --Explode:SetKeyValue('spawnflags','64')
        	       Explode:Fire('Explode',0,0 )
            end)
        ",Delay,This:x(),This:y(),This:z(),Radius))
        
        return 1
    }
}

if(Attacking && changed(Attacking)) {
    local Trace=Owner:eyeTrace():toTable()
    local Start=Trace["StartPos",vector]
    local End=Trace["HitPos",vector]
    local Dir=(End-Start):normalized()
    
    for(I=1,3) {
        local Diff=Start-End
        local Delay=1
        local Len=(Start-End):length()
        local CurLen=512
        local MaxOffset=Len/4
        local ClassyOffset=randvec(vec(-MaxOffset),vec(MaxOffset,MaxOffset,MaxOffset))
        local Average=(Start+End)/2+ClassyOffset
        while(CurLen<Len) {
            CurLen+=128
            local RealPos=CurLen*Dir+Start
            local Pos=bezier(Start,Average,End,CurLen/Len)
            local Ranger=rangerOffset(RealPos,Pos)
            Delay+=5
            Ranger:pos():spawnExplosion(128,Delay)
        }
    }
}
#[
    HitBox	=	0
    HitNonWorld	=	0
    MatType	=	78
    HitPos	=	[-2143.8022460938,5700.5737304688,-271.33197021484]
    HitTexture	=	**displacement**
    StartSolid	=	0
    Normal	=	[-0.67056119441986,0.7394397854805,-0.05980234593153]
    HitWorld	=	1
    RealStartPos	=	[1257.7969970703,1949.5699462891,32.03125]
    HitNoDraw	=	0
    HitSky	=	0
    PhysicsBone	=	0
    Fraction	=	0.15480847656727
    Entity	=	Entity [0][worldspawn]
    HitNormal	=	[0.13285422325134,-0.14565218985081,0.98037505149841]
    Hit	=	1
    FractionLeftSolid	=	0
    StartPos	=	[1257.7969970703,1949.5699462891,32.03125]
    HitGroup	=	0
]#
