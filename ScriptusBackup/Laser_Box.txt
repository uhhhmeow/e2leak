@name Laser Box
@model models/hunter/blocks/cube05x05x05.mdl
@persist [Owner Entity]:entity Ignited:table
runOnTick(1)
runOnLast(1)

if(first()) {
    Owner=owner()
    Entity=entity()
    
    # Wrapper
    holoCreate(1)
    holoModel(1,"models/hunter/plates/plate8.mdl")
    holoMaterial(1,"models/alyx/emptool_glow")
    
    # Internal
    holoCreate(2)
    holoModel(2,"models/hunter/plates/plate8.mdl")
    holoMaterial(2,"models/props_lab/cornerunit_cloud")
    holoColor(2,vec(255,0,0))
    holoParent(2,1)
    holoAlpha(2,128)
}

rangerFilter(Entity)
local Start=Entity:pos()
local End=Entity:toWorld(vec(0,0,5000))
local Ray=rangerOffset(Start,End)
local Hit=Ray:pos()
local Ent=Ray:entity()


local Mid=(Start+Hit)/2
local Size=((Start-Hit):length()/379.6)
local Ang=toWorldAng(vec(),ang(0,90,0),vec(),(Start-Hit):toAngle())
holoPos(1,Mid)
holoAng(1,Ang)
holoScale(1,vec(1,Size,1))
holoScale(2,vec(0.8,Size,0.8))
holoAlpha(1,255)
holoAlpha(2,128)

local Time=curtime()

if(Ent:isValid()) {
    Ent:ignite()
    Ignited[Ent:id():toString(),array]=array(Ent,Time+5)
}

foreach(I,V:array=Ignited) {
    if(Time>V[2,number]) {
        if(V[1,entity]:isValid()) {
            V[1,entity]:extinguish()
        }
        Ignited:removeTable(I)
    }
}

if(last()) {
    foreach(I,V:array=Ignited) {
        if(V[1,entity]:isValid()) {
            V[1,entity]:extinguish()
        }
    }
}
