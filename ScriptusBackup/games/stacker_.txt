@name Stacker 
@inputs X Y Use AMT Scr:wirelink Door:entity
@outputs RST Dist Open
@persist Current HSN:array HSS:array Location:vector
@persist Speed Column Row Player:entity Rdy Gameover
@persist Point Dir Needed Playing Credit Price Debug
@model models/hunter/plates/plate.mdl 

Price=0
Credit=AMT-Current
if (first()) {
HSN[1,string]="null"
HSS[1,number]=0
HSN[2,string]="null"
HSS[2,number]=0
}
Debug=0
Location=vec(ceil(X*30),ceil(Y*18),0)
if (first()) {RST=1 Needed=5}
Scr:writeString("Stacker                       ",0,0,999,900)
Scr:writeString("High Scores",16,0,999,900)

if (RST==1) {timer("RSTing",100)}
if (clk("RSTing")) {RST=0 timer("Ready",10000)}
if (clk("Ready") & !Playing) {RST=1}
for (I=1,15) {
    if (I<10) {
    Scr:writeString("  "+toString(I),14,I,999,900)
} else {
Scr:writeString(" "+toString(I),14,I,999,900)
}
}
for (I=1,3) {
    Scr:writeString("                ",14,I+15,0,900)
}
Scr:writeString("Get 300+ = $200!",14,16,190,900)
Scr:writeString("By Scriptis",14,17,190,900)
Dist=5
if (Playing==0) {stoptimer("Going") Needed=5 Speed=1000 Row=0 Point=0}



if (Credit<Price & Playing==0 & !Gameover) {
    Scr:writeString("Insert $"+toString(Price-Credit)+"",1,9,111,999,1)
}
if (Credit>=Price & Playing==0 & Use & !Gameover) {
        print("Starting game")
        findByClass("player")
        findSortByDistance(entity():pos())
        Player=find()
        Speed=1000
        Row=0
        Point=5
        Gameover=0
        timer("Waiting",750)
        soundPlay(1,0,"ct_forcedownload/ctb_gamestart.wav")
        soundVolume(1,0.5)
        soundPlay(0,0,"ct_forcedownload/sp_a4_finale4_b3.wav")
        Playing=1
}
if (Credit>=Price & Playing==0 & !Gameover) {
    if (inrange(Location,vec(0,2,0),vec(14,18,0))) {
    Scr:writeString("Start",5,9,999,0)

    } elseif (!Playing) {
        Scr:writeString("Start",5,9,555,0)
}
}
if (Playing==1) {
    timer("Going",Speed)
}
if (clk("Going") & Playing & Column<17) {
    if (Dir) {
    Point++
} else {
Point--
}
Scr:writeString("              ",0,16-Column,0,0)
Scr:writeString(" ",Point,16-Column,0,999)
if (Point>11 | Point<1) {Dir=!Dir}
timer("Going",Speed)
}
if (Playing & changed(Use) & Rdy & Use) {
        if (Point==Needed | Debug) {
        Column++ 

        Scr:writeString("              ",0,16-Column,0,0)
        Scr:writeString(" ",Point,16-Column,0,999)
        Speed=Speed/1.5
    } elseif (Rdy) { 
            for (I=1,HSS:count()) {
    if (HSS[I,number]<=Column) {
       print(3,"High score #"+I+" added! Score:"+Column)
        HSS:insertNumber(I,Column)            
        HSN:insertString(I,Player:name())
        break}
        }
        Current+=Price
        if (Column*50>250) { 
            print("Rewards!")
            #ifdef giveMoney()
            giveMoney(Player,200)
            #endif
        }
            for (I=1,HSS:count()) {
        if (I>15) {break}
        if (HSS[I,number]>0) {
        Scr:writeString(HSN[I,string]:sub(1,8)+": "+HSS[I,number]*50,17,I) 
    }
}


        Column=0
        Gameover=1
        Playing=0
        
        
    }
}
Scr:writeString(" ",5,17,0,999)
if (Column>17) {Playing=0 reset()}
    for (I=1,HSS:count()) {
        if (I>15) {break}
        if (HSS[I,number]>0) {
    Scr:writeString(HSN[I,string]:sub(1,8)+": "+HSS[I,number]*50,17,I) 
    }
}

if (Gameover==1) {

    timer("Gameover",3000)
    Scr:writeString("Game over",2,5,900)
    
}
#music


if (clk("Gameover")) {Gameover=0}
if (changed(Credit) & !Gameover) {RST=1}
if (changed(Playing) & !Gameover) {RST=1}
if (changed(Gameover) & !Gameover) {RST=1}
if (clk("Waiting")) {Rdy=1}
if (Gameover) {soundPurge()}
        if (changed(Column)) {
            if (Column==3) {soundPlay(0,0,"ct_forcedownload/sp_a4_finale4_b3.wav")}
            if (Column==4) {soundPlay(0,0,"ct_forcedownload/sp_a4_finale1_b2p2.wav")}
            if (Column==6) {soundPlay(0,0,"ct_forcedownload/sp_a4_finale4_b5.wav")}
            if (Column==5) {soundPlay(1,0,"ct_forcedownload/ctb_victory.wav")}
            if (Column==7) {soundPlay(1,0,"ct_forcedownload/ctb_overtime.wav")}
            soundVolume(1,0.5)
}
if (Playing==1) {
    Door:setAlpha(90)
    Open=0
} else {
    Door:setAlpha(0)
    Open=1
}
