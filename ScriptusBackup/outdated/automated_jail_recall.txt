@name Automated Jail Recall
@inputs AMT Scr:wirelink X Y Use A1:entity A2:entity A3:entity A4:entity A5:entity A6:entity A7:entity A8:entity A9:entity A10:entity Door:entity
@outputs RST Open
@persist Menu Arrested:array Location:vector Curperson:entity Curnumber

if (first()) {Menu=1 RST=1}
Location=vec(ceil(X*30),ceil(Y*18),0)
Arrested[1,entity]=A1
Arrested[2,entity]=A2
Arrested[3,entity]=A3
Arrested[4,entity]=A4
Arrested[5,entity]=A5
Arrested[6,entity]=A6
Arrested[7,entity]=A7
Arrested[8,entity]=A8
Arrested[9,entity]=A9
Arrested[10,entity]=A10
if (Menu==2) {
    if (!(Arrested[Curnumber,entity]:isPlayer())) {Curperson=noentity() Curnumber=0 Menu=1}
    Door:setColor(0,0,0)
    Open=1
    Scr:writeString(Curperson:name(),15-round(Curperson:name():length()/2),8)
    Scr:writeString("Insert $200 to unjail",15-round("Insert $200 to unjail":length()/2),9)
    Scr:writeString("                              ",0,15,0,900)
    Scr:writeString("Cancel",12,15,555,900)
    if (Location:y()==16) {Scr:writeString("Cancel",12,15,999,900)}
    if (Location:y()==16 & Use) {Curperson=noentity() Curnumber=0 Menu=1}
    EXP=Curperson:name():explode(" ")
    if (AMT==200) {concmd("rp_unarrest "+EXP[1,string]) Curnumber=0 Curperson=noentity() Menu=1}
    if (AMT>200 | AMT<200) {
        if (AMT>0) {
        findByClass("player")
        findSortByDistance(entity():pos())
        User=find()
        giveMoney(User,AMT)
    }
}
} else {Door:setColor(180,180,180) Open=0}
if (RST==1) {timer("RST",100)}
if (clk("RST")) { RST=0 timer("RST2",10000)}
if (clk("RST2")) {RST=1}
if (Menu==1) {
    Scr:writeString("[SJail]",12,0,900,0)
    for (I=1, Arrested:count()) {
        if (Arrested[I,entity]:isPlayer()) {
            Person=Arrested[I,entity]
            Scr:writeString(Person:name(),15-round(Person:name():length()/2),I,555,0)
            if (Location:y()==I+1) {Scr:writeString(Person:name(),15-round(Person:name():length()/2),I,999,0)}
            if (Use==1 & Location:y()==I+1) {Curnumber=I Curperson=Arrested[I,entity] print(Curperson:name()) Menu=2}
    }
    }
}
if (changed(Menu)) {RST=1}
entity():setColor(0,0,0,0)

