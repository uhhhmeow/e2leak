@name Jukebox
@inputs Scr:wirelink AMT
@outputs Range
@persist Menu Page Songs:array Music:array Cur Current:vector TintStage
@trigger all
Range=5
if (first()) {
    Change=1
    Scr:egpClear()
    for (I=1,13) {
        Songs[I,string]="/ui/gamestartup"+I+".mp3"
    }
}
Music=Songs
if (Menu==0) {
    Scr:egpBox(3,vec2(0,0),vec2(512*2,512*2))
    Scr:egpColor(3,0,0,255,255)
    #Scr:egpMaterial(3,"models/wireframe")
    Scr:egpBox(1,vec2(0,0),vec2(1024,120))
    Scr:egpColor(1,vec(80,80,80))
    Scr:egpText(2,"SDJ",vec2(256,30))
    Scr:egpSize(2,50)
    Scr:egpAlign(2,1,1)
    Scr:egpText(4,"Press a song to buy it!",vec2(256,490))
    Scr:egpAlign(4,1,1)
    Scr:egpSize(4,40)
    Scr:egpColor(4,255,0,0,255)
    for (I=1,5) {
        MyMusic=Music[I+(Page*5),string]
        Scr:egpText(10+I,MyMusic,vec2(256,120+(I*50)))
        Scr:egpAlign(10+I,1,1)
        Scr:egpSize(10+I,30)
    }
    #models/error/new_light 1
}
if (clk("Background")) {Change=1}
if (Change) {
Speed=5
if (TintStage==0) {
    Current+=vec(Speed,0,-Speed)
    if (Current:x()>254) {TintStage=1}
}
if (TintStage==1) {
    Current+=vec(-Speed,Speed,0)
    if (Current:y()>254) {TintStage=2}
}
if (TintStage==2) {
    Current+=vec(0,-Speed,Speed)
    if (Current:z()>254) {TintStage=0}
}
Scr:egpColor(4,-Current)
Scr:egpColor(3,Current)
timer("Background",500)
Change=0
}
