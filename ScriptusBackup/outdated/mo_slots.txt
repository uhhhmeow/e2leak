@name Mo Slots
@persist Cost Credit LastAMT Reel:array Symbols:array Price
#atari graphics chipset
@persist Menu:string Background:vector Current
@inputs Scr:wirelink
@inputs AMT
@outputs Reset Eject
if (first()) {#define all functions
    Price=20 #default price
    Symbols=array("@","$","-","#","7")
    function reel() {
        Reel[1,string]=Symbols[randint(1,Symbols:count()),string]
        Reel[2,string]=Symbols[randint(1,Symbols:count()),string]
        Reel[3,string]=Symbols[randint(1,Symbols:count()),string]
}
    reel()
    Menu=" ":repeat(30*18) #reset menu
    function writeCenter(T:string,L) {
        Scr:writeString(T,15-(ceil(T:length()/2)),L,vec(255,255,255),Background)
    }
    function writeCenterColor(T:string,L,Color:vector,Color2:vector) {
        Scr:writeString(T,15-(ceil(T:length()/2)),L,Color,Color2)
    }
    
    function drawDetail() {
        writeCenter("Mo' Slots",0)
        writeCenterColor(" ":repeat(11),3,vec(255,255,255),vec(255,255,255))
        writeCenterColor(" ":repeat(11),4,vec(255,255,255),vec(255,255,255))
        writeCenterColor(" ":repeat(11),5,vec(255,255,255),vec(255,255,255))
        writeCenterColor(Reel:concat("  "),4,vec(255,255,255),vec(80,80,80))
        writeCenter("Insert $"+Price+" to Spin",10)
        writeCenter("A A B or B A A is worth $"+Price,11)
        writeCenter("A A A is worth $"+round(AMT/2),12)
    }
    
    function draw() {
        Scr:writeString(" ":repeat(30*18),0,0,vec(0,0,0),Background)
        drawDetail()
    }
    function setBackground(V:vector) {Background=V}
    function resetall() {Menu=" ":repeat(30*18) Reset=1 timer("reset",50)}
    resetall()
    timer("mreset",1000)
}
if (clk("reset")) {Reset=0 timer("draw",50)}
setBackground(vec(128+64,128+64,128+64))
draw()
if (AMT-Current>=Cost) {Credit=AMT-Cost Current=AMT}
if (AMT<Current) {Current=AMT}
if (clk("mreset")) {resetall() timer("nreset",10)}
if (clk("nreset")) {Reset=0 timer("rreset",10)}
if (clk("rreset")) {draw() timer("mreset",15000)}
#/atari graphics chipset
