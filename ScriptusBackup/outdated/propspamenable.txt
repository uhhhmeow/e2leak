@name PropSounds
@persist Props:array Lastprops:array Propsused:array Count
#Count=0
runOnTick(1)
function array getArray(S:string) {
    findByClass(S)
    findIncludePlayerProps(owner():name())
    return findToArray()
}
if (first()) {Lastprops=getArray("prop_physics")}
if (findCanQuery()) {
Lastprops=Props
Props=getArray("prop_physics")
for (I=1, Props:count()) {
    if (Lastprops[I,entity]!=Props[I,entity] & Propsused[Props[I,entity]:id(),entity]!=Props[I,entity]) {
    ID=randint(1,10)
    soundVolume(ID,0)
    Lastprops[I,entity]:soundPlay(ID,0.1,"/ambient/alarms/warningbell1.wav")
    Propsused[Props[I,entity]:id(),entity]=Props[I,entity]
    Count++
} elseif  (Props:count()>Lastprops:count()) {Count--}
}
}
print(_HUD_PRINTCENTER,"Props spawned: "+Count)
