@name Wireless Shipment Getter
@outputs Items:array Prices:array Count:array
@persist Pos:gtable Last Boxes:array Currentbox Scanned:array
@trigger all
runOnTick(1)
if (first()) {Currentbox=1 EntID=-1}
#Finding
Pos=gTable("ws")
findByModel("models/Items/item_item_crate.mdl")
findSortByDistance(entity():pos())
Boxes=findToArray()
#cleaning
for (I=1, Boxes:count()) {
    if (Boxes[I,entity]:pos():distance(entity():pos())>200) {Boxes:remove(I)}
    for (B=1, Scanned:count()) {
        if (Boxes[I,entity]==Scanned[B,entity]) {Boxes:remove(I)}
    }
}
#main
interval(500)
Item=Pos[2,string]
Counts=Pos[3,number]
Price=Pos[4,number]

if (Item & (!(Last==Boxes[Currentbox,entity]:id()))) {
    Items[Items:count()+1,string]=Item
    Prices[Prices:count()+1,number]=Price
    Count[Count:count()+1,number]=Counts
    Scanned[Scanned:count()+1,number]=Boxes[Currentbox,entity]:id()
    Last=Boxes[Currentbox,entity]:id()
    Currentbox++
}


if (Currentbox<Boxes:count()) {
    Pos[1,vector]=Boxes[Currentbox,entity]:pos()
} else {Pos[1,vector]=entity():pos()+vec(0,0,50)}
