@name Roulette Table
@persist [Owner Entity]:entity
if(first()) {
    Owner=owner()
    Entity=entity()
    local Offset=45
    
    holoCreate(1,Entity:toWorld(vec(0,0,30)),vec(0.5,0.3,0.5),Entity:toWorld(ang(0,Offset,90)))
    holoParent(1,Entity)
    holoModel(1,"models/hunter/tubes/tubebend1x2x90b.mdl")
    
    holoCreate(2,Entity:toWorld(vec(0,0,30)),vec(0.5,0.3,0.5),Entity:toWorld(ang(0,90+Offset,90)))
    holoParent(2,Entity)
    holoModel(2,"models/hunter/tubes/tubebend1x2x90b.mdl")
    
    holoCreate(3,Entity:toWorld(vec(0,0,30)),vec(0.5,0.3,0.5),Entity:toWorld(ang(0,180+Offset,90)))
    holoParent(3,Entity)
    holoModel(3,"models/hunter/tubes/tubebend1x2x90b.mdl")
    
    holoCreate(4,Entity:toWorld(vec(0,0,30)),vec(0.5,0.3,0.5),Entity:toWorld(ang(0,270+Offset,90)))
    holoParent(4,Entity)
    holoModel(4,"models/hunter/tubes/tubebend1x2x90b.mdl")
    
    
}
