@name lolyes
@persist Current
@inputs E:wirelink
@outputs Boom
W=E:entity()
if (E & !Boom) {
    Players=players()
    Current++
    if (Current>Players:count()) {
        Current=1
    }
    W:setPos(Players[Current,entity]:pos())
    Boom=1
} else {
    Boom=0
}
entity():setAlpha(0)
W:setAlpha(0)
W:propNotSolid(1)
