@name Standard Lawset
@persist Laws:array LawCt

if(first()) {
    concmd("say /clearlaws")
    Laws=array(
        "All weapons/raiding tools are illegal, always.",
        "Do not disrespect nor disobey any government official.",
        "Bugbait, singing, micspam and loud noises are illegal.",
        "You may not loiter about nor enter the PD.",
        "You may not request an audience with the mayor.",
        "In a bank, the safe owner is responsible for that safe.",
        "Officers are exempt from the law where !rules is not broken.",
        "Publicly advertising organizations of any sort is illegal."
    )
    
    timer("MakeLaws",1500)
}

if(clk("MakeLaws")) {
    LawCt++
    local ThisLaw=Laws[LawCt,string]
    if(ThisLaw) {
        printColor(vec(255,255,0),"[aLaws] ",vec(255),ThisLaw)
        concmd("say \"/addlaw " + ThisLaw +"\"")
        timer("MakeLaws",1000)
    } else {
        selfDestruct()
    }
}
