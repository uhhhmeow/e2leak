@name Orbiting Shitty Helicopters
@persist [Owner Entity]:entity Tick Seed:table

interval(10)

if(first()) {
    Owner=findPlayerByName("Fas") owner()
    Entity=entity()
    
    for(I=1,8) {
        holoCreate(I,vec(),vec(0.1),ang(),vec(255),"models/combine_helicopter.mdl")
        holoAnim(I,"ACT_HELICOPTER_CRASHING")
    }
    
    # "models/effects/vol_light64x256.mdl"
    function array pointsInSphere4(PointsCt,Radius,Center:vector) {
        local Points=array()
        local Pi=pi()
        local Pi2=Pi*2
        local LatStep=Pi/(PointsCt-1)
        local Slice=-0.5*Pi
        for(I=0,PointsCt-1,1) {
            local R=cosr(Slice)
            local Z=sinr(Slice)
            local Nb=ceil(Pi2*R/LatStep)
            local Db=Pi2/Nb
            if(I>=PointsCt) {Nb=1 Db=0}
            local B=0
            for(V=0,Nb-1) {
                B+=Db
                local X=R*cosr(B)
                local Y=R*sinr(B)
                Points:pushVector(vec(X,Y,Z)*Radius+Center)
            }
            Slice+=LatStep
        }
        return Points
    }
    
    local Center=Entity:toWorld(vec())
    holoCreate(10,Center,vec(),ang())
    local Index=11
    
   #[ 
    foreach(I,V:vector=pointsInSphere4(8,20,vec())) {
        local V=holoEntity(10):toWorld(V)
        holoCreate(
            Index,
            V,
            vec(2,2,0.5),
            toWorldAng(vec(),ang(270,0,0),vec(),(V-Center):toAngle()),
            vec(255),
            "models/effects/vol_light64x256.mdl"
        )
        holoParent(Index,10)
        Seed[Index,number] = randint(1,360)
        Index++
    }
    
    foreach(I,V:vector=pointsInSphere4(10,13,vec())) {
        local V=holoEntity(10):toWorld(V)
        holoCreate(
            Index,
            V,
            vec(0.3,0.3,0.5)*0.72,
            toWorldAng(vec(),ang(270,0,0),vec(),(V-Center):toAngle()),
            vec(255),
            "models/hunter/plates/plate05x05.mdl"
        )
        holoParent(Index,10)
        # holoDisableShading(Index,1)
        holoMaterial(Index,"debug/env_cubemap_model")
        Index++
    }
    ]#
    
    holoCreate(Index,holoEntity(10):toWorld(vec(0,0,30)),vec(0.2,0.2,0),holoEntity(10):toWorld(ang()),vec(255),"models/hunter/tubes/circle4x4.mdl")
    holoMaterial(Index,"debug/debugdrawflat")
    holoParent(Index,11)
    Index++
    
    holoCreate(Index,holoEntity(10):toWorld(vec(0,0,45)),vec(1),holoEntity(10):toWorld(ang(-90,0,0)),vec(255),"models/props_junk/harpoon002a.mdl")
    holoParent(Index,11)
    holoClip(Index,1,vec(-15,0,0),vec(-1,0,0),0)
    holoClipEnabled(Index,1,1)
    Index++
    
    lightCreate(1,holoEntity(10):toWorld(vec(0,0,15)))
    lightColor(1,vec(255))
    lightParent(1,holoEntity(10))
    
    local Base=Owner:attachmentPos("eyes")
    local BaseAng=Owner:attachmentAng("eyes")
    
    local FinPos=toWorld(vec(-4,0,-3),BaseAng,Base,BaseAng)
    holoCreate(201,FinPos,vec(1),BaseAng)
    holoModel(201,"models/player/items/engineer/drg_brainiac_hair.mdl")
    holoColor(201,vec(1))
    holoParentAttachment(201,Owner,"eyes")
    
    local FinPos=toWorld(vec(-3,2,-2),BaseAng,Base,BaseAng)
    local FinAng=toWorldAng(Base,BaseAng,Base,ang(0,-90,15))
    holoCreate(202,FinPos,vec(1.1),FinAng)
    holoModel(202,"models/player/items/engineer/prospector_hat.mdl")
    holoParentAttachment(202,Owner,"eyes")
    
    #models/player/items/engineer/professor_speks.mdl
    local FinPos=toWorld(vec(-4.1,0,-2.5),BaseAng,Base,BaseAng)
    local FinAng=toWorldAng(Base,BaseAng,Base,ang(0,0,5))
    holoCreate(203,FinPos,vec(1),FinAng)
    holoModel(203,"models/player/items/demo/ttg_glasses.mdl")
    holoParentAttachment(203,Owner,"eyes")
    holoColor(203,vec(0,255,0))
    
    for(I=201,203) {
        holoVisible(I,Owner,0)
    }
}

if(clk()) {
    Tick+=0.01
    
    for(I=1,8) {
        holoAng(I,ang(randint(360),randint(360),randint(360)))
        holoScale(I,vec(randint(100)/500,randint(100)/500,randint(100)/500)*0.3)
        local Offset=I*45
        holoPos(I,(Owner:attachmentPos("eyes")+vec(cos(Tick*120+Offset)*30,sin(Tick*120+Offset)*30,sin(Tick*Offset)*10)))
        
        local Scale=randint(100)/100
        holoEntity(I):soundPlay("l"+I,0,"NPC_AttackHelicopter.RotorsLoud")
        holoEntity(I):soundPlay("b"+I,0,"NPC_AttackHelicopter.RotorBlast")
        
        soundPitch("l"+I,255*Scale)
        soundPitch("b"+I,255*Scale)
    }
    
    holoAng(10,ang(0,Tick*8,0))
    holoPos(10,Owner:toWorld(vec(0,0,120)))
    
    local Offset=curtime()*10
    
    for(I=11,77) {
        local Alpha=(sin(16*Tick+Seed[I,number])+1)/2
        local Color=hsv2rgb((Offset+I*15)%360,1,1)*0.75
        holoColor(I,Color*Alpha/2)
    }
}
