@name l33tspeak
@persist [Input Output]:string [Owner Entity]:entity Filter:table
runOnChat(1)

if(first()) {
    Owner=owner()
    Entity=entity()
    
    Filter=table(
        "a"="4",
        "i"="1",
        "o"="0",
        "t"="7",
        "m"="/\\/\\",
        "b"="6",
        "c"="(",
        "d"="[)",
        "e"="&",
        "i"="!"
    )
    
    function string l33t(Input:string) {
        local Breakdown=Input:explode("")
        local Output=""
        foreach(Index,Char:string=Breakdown) {
            Char=Char:lower()
            Output+=Filter[Char,string]?:Char
        }
        return Output
    }
}

if(chatClk(Owner)) {
    local In=Owner:lastSaid()
    if(In!=Output) {
        hideChat(1)
        Input=In
        Output=l33t(In)
        timer("say",700)
    }
}

if(clk("say")) {
    concmd("say \""+Output+"\"")
}
