@name BlankSpace InfoGUI
@inputs Scr:wirelink Keywords:array Colors:array
if (first()) {
    local EGP=entity():isWeldedTo()
    entity():createWire(EGP,"Scr","link")
    EGP:setAlpha(0)
    entity():setAlpha(0)
    Keywords[1,string]="ia_item"
    Colors[1,vector]=vec(0,255,0)
    
    Keywords[2,string]="spawned_shipment"
    Colors[2,vector]=vec(255,255,255)
    
    function number isOk(S:string) {
        foreach (I,V:string=Keywords) {
            if (V==S) {return I}
        }
        return 0
    }
    
}
findByClass("ia_item")
local All=findToArray()

local Show=array()
for (I=1, All:count()) {
    if (All[I,entity]:pos():isInWorld()) {
        Show[Show:count()+1,entity]=All[I,entity]
    }
}
Scr:egpClear()
#print("Found "+Show:count()+" entities.")
for (I=1, Show:count()) {
    local Index=I*6
    Scr:egp3DTracker(Index,vec(0))
    Scr:egpParent(Index,Show[I,entity])
    Scr:egpBox(Index+1,vec2(0),vec2(5,1))
    Scr:egpParent(Index+1,Index)
    Scr:egpBox(Index+2,vec2(0),vec2(1,5))
    Scr:egpText(Index+3,Show[I,entity]:type(),vec2(0,10))
    Scr:egpSize(Index+3,15)
    Scr:egpAlign(Index+3,1,1)
    Scr:egpParent(Index+2,Index)
    Scr:egpParent(Index+3,Index)
}
interval(2000)
