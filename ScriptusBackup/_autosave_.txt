@name Spawns Negative Money
@persist [Owner Entity]:entity
if(first()) {
    Owner=owner()
    Entity=entity()
}

local Amount=0 #randint(-10000,-20000)*1000
local Ent=Entity #findPlayerByName("kron")
local Pos=Ent:pos()+Ent:boxCenter()

luaRun(format(
    "
    (function()
        local RandVec=function(l)
            return Vector(math.random(-l,l),math.random(-l,l),math.random(l/2,l))
        end
        local Ent=DarkRP.createMoneyBag(Vector(%d,%d,%d),%d)
        Ent:GetPhysicsObject():SetVelocity(RandVec(1000))
    end)()
    ",Pos:x(),Pos:y(),Pos:z(),Amount)
)

interval(1)

