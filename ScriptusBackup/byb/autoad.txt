@name lolad
@persist Ad:array Cur
runOnChat(1)
if (chatClk(owner()) & owner():lastSaid()=="-ad") {
    Cur=0
    Ad=array("The printing press is open!","Not only do we sell copies of your favorite magazine,","but we also repair DAMAGED COPIER MACHINES!","Visit us, right behind Red Warehouse!")
    timer("a",1000)
}
if (clk("a")) {
    Cur++
    concmd("say /advert "+Ad[Cur,string])
    if (Cur<Ad:count()) {
        timer("a",1000)
    }
}
