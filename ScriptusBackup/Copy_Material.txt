@name Copy Material
@persist Use Using
if (owner():keyZoom() & !Using) {
    Use=1
    Using=1
} elseif (!owner():keyZoom()) {
    Using=0
}
if (Use) {
    Ent=owner():aimEntity()
    Col=Ent:getColor()
    Mat=Ent:getMaterial()
    concmd("material_override "+Mat)
    concmd("colour_r " + Col:x() +"; colour_g "+Col:y()+"; colour_b "+Col:z()+"; colour_a "+Ent:getAlpha())
    printColor(vec(255),"Got appearance data of ",vec(0,255,0),Ent:type())
    printColor(vec(255),"Material: ",vec(0,255,0),Mat ? Mat : "null")
    printColor(vec(255),"Color: (",vec(0,255,0),Col:x():toString(),", ",Col:y():toString(),", ",Col:z():toString(),", ",Ent:getAlpha(),vec(255),")")
}
Use=0

interval(1)
