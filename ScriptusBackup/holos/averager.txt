@name Averager
@inputs In
@outputs A
@persist All:array LastIn
@trigger all
if (first()) {LastIn=In}
All[All:count()+1,number]=In-LastIn
if (In>LastIn) {
LastIn=In
}
A=round(All:average())
