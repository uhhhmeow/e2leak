@name Scriptis' Polygen
@inputs New:entity
@persist Iteration MaxIteration Pos:vector Dist
@trigger New

if (first()) {
    Dist=30
    Pos=entity():pos()
    MaxIteration=5
    Iteration=0
}

#[if (clk("go") & Iteration<MaxIteration) {
    New=propSpawn("models/XQM/panel360.mdl",1)
    local Ang=ang(0,(360/MaxIteration)*Iteration+30,0)
    local PropPos=toWorld(vec(0,Dist,0),ang(0),Pos,Ang)
    print(Ang)
    New:setPos(PropPos)
    New:setAng(Ang+ang(0,0,0))
    Iteration++
    Last=New
    timer("go",1000)
}]#

if (~New & New) {
    local Ang=ang(0,(360/MaxIteration)*Iteration+30,0)
    local PropPos=toWorld(vec(0,Dist,0),ang(0),Pos,Ang)
    print(Ang)
    New:setPos(PropPos)
    New:setAng(Ang+ang(90,0,0))
    Iteration++
    Last=New
    print(New:pos())
}
