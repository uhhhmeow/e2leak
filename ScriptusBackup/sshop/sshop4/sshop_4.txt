@name SShop 4
@inputs W:wirelink Amt ShotgunR SniperR Mp5R M4a1R Mac10R AK47R
@outputs ShotgunU SniperU Mp5U M4a1U Mac10U AK47U
@persist E:entity [Wep,Cost,Desc]:array Sel St CurrentCredit Credit RST
Credit=Amt-CurrentCredit

interval(100)

if(findCanQuery()) {
    findByClass("player")
    O = findClosest(E:pos())
}

#if(changed(->W) & ->W) {
#    reset()
#}

if(first() | RST) {
    W:egpClear()
    E = entity()
    
    Wep[1,string] = "Shotgun", Cost[1,number] = 250, Desc[1,string] = "Used by Italians, it has been a cheap alternative to stabbing"
    Wep[2,string] = "Sniper", Cost[2,number] = 400, Desc[2,string] = "Better gather some lumber and tents, because you're going to be camping like hell"
    Wep[3,string] = "Mp5", Cost[3,number] = 300, Desc[3,string] = "Just your generic automatic gun."
    Wep[4,string] = "M4a1", Cost[4,number] = 350, Desc[4,string] = "Widely known for killing middle-easterns"
    Wep[5,string] = "Mac10", Cost[5,number] = 300, Desc[5,string] = "A little baby gun that your 5-year-old niece uses"

    function number pIR(Num:number) {
        if(inrange(W:egpCursor(O):x(),W:egpPos(Num):x()-(W:egpSize(Num):x()/2),W:egpPos(Num):x()+(W:egpSize(Num):x()/2))&
        inrange(W:egpCursor(O):y(),W:egpPos(Num):y()-(W:egpSize(Num):y()/2),W:egpPos(Num):y()+(W:egpSize(Num):y()/2))) {
            return 1
        } else {
            return 0
        }
    }
    
    W:egpBox(250,vec2(256,256),vec2(512,512))
    W:egpMaterial(250,"maps/noicon")
    #W:egpMaterial(250,"gui/gradient_up")
    #W:egpColor(250,vec(0,0,255))
    
    for(I=6,1,-1) {
        W:egpRoundedBox(I+50,vec2(200,145+((I-1)*60)),vec2(310,80))
        W:egpColor(I+50,(ioGetInputNumber(Wep[I,string]+"R") > 0 ? vec(0,255,0) : vec(180,0,0)))

        W:egpRoundedBox(I,vec2(200,145+((I-1)*60)),vec2(300,70))
        W:egpColor(I,vec()+100)

        W:egpText(I+100,Wep[I,string],vec2(130,135+((I-1)*60)))
        W:egpFont(I+100,"",40)
        W:egpColor(I+100,(ioGetInputNumber(Wep[I,string]+"R") > 0 ? vec(0,255,0) : vec(255,0,0)))
    
    }
    
    W:egpCircle(19,vec2(433,83),vec2(70,70))
    W:egpColor(19,vec())
    
    W:egpRoundedBox(14,vec2(256,84),vec2(480,90))
    W:egpColor(14,vec(0,0,0))
    
    W:egpRoundedBox(15,vec2(256,84),vec2(470,80))
    W:egpColor(15,vec()+175)
    
    W:egpRoundedBox(16,vec2(435,296),vec2(90,390))
    W:egpColor(16,vec())
    
    W:egpRoundedBox(17,vec2(435,296),vec2(80,380))
    W:egpColor(17,vec()+175)
    
    W:egpText(18,"[SShop 4]",vec2(40,50))
    W:egpFont(18,"",60)
    W:egpColor(18,vec(0,190,0))

    W:egpCircle(20,vec2(433,83),vec2(65,65))
    W:egpColor(20,vec()+175)
    
    W:egpBox(21,vec2(435,90),vec2(90,90))
    W:egpMaterial(21,"console/gmod_logo")
    
    W:egpText(22,"Cost:",vec2(400,150))
    W:egpColor(22,vec())
    W:egpFont(22,"",35)
    
    W:egpRoundedBox(24,vec2(435,445),vec2(70,70))
    W:egpColor(24,vec())
    
    W:egpRoundedBox(25,vec2(435,445),vec2(60,60))
    W:egpColor(25,vec(0,255,0))
    
    W:egpText(26,"Buy",vec2(412,430))
    W:egpFont(26,"",30)
    W:egpColor(26,vec())
    
    W:egpRoundedBox(27,vec2(435,365),vec2(70,70))
    W:egpColor(27,vec())
    
    W:egpRoundedBox(28,vec2(435,365),vec2(60,60))
    W:egpColor(28,vec(0,150,255))
    
    W:egpText(29,"Info",vec2(409,350))
    W:egpFont(29,"",30)
    W:egpColor(29,vec())
    
    
    #W:egpMaterial(1,"models/cheeze/wires/leather2")
    RST=0
}

if(!St) {
    for(I=1,6) {
        if(pIR(I)) {
            for(K=1,6) {
                if(K != I & K != Sel) {
                    W:egpColor(K,vec()+100)
                }
            }
            if(I != Sel) {
                W:egpColor(I,vec()+150)
            }
        }
    }
    
    if(changed(O:keyUse())&O:keyUse()) {
        for(I=1,6) {
            if(pIR(I)) {
                Sel = I
                W:egpColor(I,vec()+200)
                
                W:egpText(23,(Cost[Sel,number]-Credit):toString(),vec2(398,180))
                W:egpFont(23,"",45)
                W:egpColor(23,vec())
                
                if(ioGetInputNumber(Wep[I,string]+"R")) {
                    W:egpColor(25,vec(0,255,0))
                } else {
                    W:egpColor(25,vec(255,0,0))
                }
            }
        }
        
        if(pIR(27) & Sel) {
            W:egpClear()
            
            W:egpBox(1,vec2(256,256),vec2(512,512))
            W:egpMaterial(1,"maps/noicon")
            
            W:egpRoundedBox(2,vec2(256,256),vec2(450,450))
            W:egpColor(2,vec())
            
            W:egpRoundedBox(3,vec2(256,256),vec2(440,440))
            W:egpColor(3,vec()+150)
            
            W:egpText(4,Wep[Sel,string],vec2(80,40))
            W:egpFont(4,"",70)
            W:egpColor(4,vec()+50)
            
            W:egpBox(5,vec2(256,120),vec2(400,5))
            W:egpColor(5,vec()+80)
            
            W:egpTextLayout(6,Desc[Sel,string],vec2(80,140),vec2(350,400))
            W:egpFont(6,"",30)
            W:egpColor(6,vec())
            
            W:egpRoundedBox(7,vec2(120,410),vec2(100,70))
            W:egpColor(7,vec())
            
            W:egpRoundedBox(8,vec2(120,410),vec2(90,60))
            W:egpColor(8,vec(0,150,255))
            
            W:egpText(9,"Back",vec2(85,390))
            W:egpFont(9,"",35)
            W:egpColor(9,vec())
            
            St = 1
        }
        if(pIR(24) & Sel & ioGetInputNumber(Wep[Sel,string]+"R")) {
            W:egpClear()
            
            W:egpBox(1,vec2(256,256),vec2(512,512))
            W:egpMaterial(1,"maps/noicon")
            
            W:egpRoundedBox(2,vec2(256,256),vec2(450,450))
            W:egpColor(2,vec())
            
            W:egpRoundedBox(3,vec2(256,256),vec2(440,440))
            W:egpColor(3,vec()+150)
            
            W:egpBox(5,vec2(256,120),vec2(400,5))
            W:egpColor(5,vec()+80)
                
            W:egpRoundedBox(7,vec2(120,410),vec2(100,70))
            W:egpColor(7,vec())
            
            W:egpRoundedBox(8,vec2(120,410),vec2(90,60))
            W:egpColor(8,vec(0,150,255))
            
            W:egpText(9,"Back",vec2(85,390))
            W:egpFont(9,"",35)
            W:egpColor(9,vec())
            
            
            if(Credit < Cost[Sel,number]) {
                St = 2
                
                W:egpText(4,"Failed!",vec2(80,40))
                W:egpFont(4,"",70)
                W:egpColor(4,vec()+50)
            
                W:egpTextLayout(6,"You didn't put in the correct amount of money!  You were short by "+(Cost[Sel,number] - Credit):toString()+" dollars!",vec2(80,140),vec2(350,400))
                W:egpFont(6,"",30)
                W:egpColor(6,vec())
            } elseif (Credit>=Cost[Sel,number]) {
                St = 3
                
                W:egpText(4,"Purchased",vec2(80,40))
                W:egpFont(4,"",70)
                W:egpColor(4,vec()+50)
                CurrentCredit+=Cost[Sel,number]
                W:egpTextLayout(6,"Vending gun...",vec2(80,140),vec2(350,400))
                W:egpFont(6,"",30)
                W:egpColor(6,vec())
                ioSetOutput(Wep[Sel,string]+"U",1)
            }
        }
    }
} else {
    if(changed(O:keyUse()) & O:keyUse()) {
        if(pIR(7)) {
            ioSetOutput(Wep[Sel,string]+"U",0)
            W:egpClear()
            St=0
            RST=1
        }
    }
}
entity():setColor(0,0,0,0)            
#By Muffin
