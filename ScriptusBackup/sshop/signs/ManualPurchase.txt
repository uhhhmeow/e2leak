@name Manual Purchase Sign
@inputs Scr:wirelink
Scr:egpClear()
Scr:egpRoundedBox(1,vec2(256,128+(356/2)),vec2(256,356))
Scr:egpAlpha(1,128)
Scr:egpColor(1,vec(128,128,128))
Scr:egpRoundedBox(2,Scr:egpPos(1),Scr:egpSize(1)-vec2(20,20))
Scr:egpAlpha(2,72)
Scr:egpColor(2,vec(172,172,172))
Scr:egpText(3,"MANUAL PURCHASE",Scr:egpPos(2)-(Scr:egpSize(2)*vec2(0,0.5))+vec2(0,10))
Scr:egpSize(3,27)
Scr:egpAlign(3,1)
Scr:egpTextLayout(4,"- Boring
- Slow
- Stressful
- Pain in the ass
+ Order shipments",Scr:egpPos(2)-(Scr:egpSize(2)/2)+vec2(5,50),Scr:egpSize(2))
Scr:egpColor(4,vec(256,0,0))
Scr:egpSize(4,25)
