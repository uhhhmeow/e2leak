@name SShop13Core
@inputs Shipment:entity User:wirelink
@outputs Fire
@persist Pos:vector E:entity
@persist Shipments Current
@model models/hunter/blocks/cube025x025x025.mdl
runOnTick(1)
if (changed(Shipment) & Shipment) {
Pos=Shipment:pos()+Shipment:aabbSize()*vec(0,0,1)
Shipments+=1
Fire=0
}
if (first() | changed(User)) {E=User:entity()}
#E:applyForce(((Pos - E:pos()) * 2 - E:vel() / 2) * E:mass() * vec(1,1,1))
if (!Pos) {Pos=entity():toWorld(vec(0,0,5))}
User:entity():applyForce(((Pos - E:pos()) * 2 - E:vel() / 2) * E:mass() * vec(1,1,1))
if (E:pos():distance(Pos)<12 & Shipments!=Current) {Fire=1 Current=Shipments}
#print(_HUD_PRINTCENTER,toString(round(E:pos():distance(Pos))))
