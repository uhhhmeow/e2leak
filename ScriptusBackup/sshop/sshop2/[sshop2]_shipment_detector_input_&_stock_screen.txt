@name [SShop2] Shipment Detector Input & Stock Screen
@inputs Name:string Cost Scr:wirelink Buy
@outputs Items:array Prices:array Range Current Instock:array RST Dist Use
@persist Addon
Range=15
if (first()) {Addon=1 Current=18}
entity():setColor(0,0,0,0)
if (clk("Adding")) {Addon=1}
if (Addon) {
    Current+=32
    if (Current>32*9+18) {Current=18}
    timer("Finding",2500)
    Addon=0
}
if (clk("Finding")) {
    Cur=(Current-18)/32
    Items[Cur,string]=Name
    Prices[Cur,number]=(ceil(Cost/10)+50)*(Cost>0)
    timer("Adding",2500)
}
for (I=1,Items:count()) {
    Scr:writeString(Items[I,string],0,I)
    Scr:writeString(Prices[I,number]:toString(),12,I)
}
if (first()) {RST=1}
if (RST==1) {timer("RST",100)}
if (clk("RST")) {RST=0 timer("RST2",10000)}
if (clk("RST2")) {RST=1}
entity():setColor(0,0,0,0)
interval(500)
# This was very, very painful to make
# CTRL+V is no longer my friend
# D:
# I hate post-paste modification
# -Scriptis.
# Oh, and by the way, if your not me, than good job stealing, you freeloading jerk...
if (Buy) {
    Dist=18+(32*(Buy))
    timer("Buyer",2000)
} else {Dist=18}
if (clk("Buyer")) {Use=1 timer("RST",1000)}
if (clk("RST")) {Use=0 Dist=18}
entity():setColor(0,0,0,0)

