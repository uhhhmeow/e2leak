@name Votifier-Out
@inputs Scr:wirelink
@persist Votes:table
if (first()) {
    Question="Uplift the weapon ban?"
    Answers=array(
       "Yes",
        "No"
    )
}
interval(2000)
Vote=gTable("Vote")
Votes:clear()
local Total=0
for (I=1, Vote:count()) {
    Votes[Answers[I,string],number]=Votes[Answers[I,string],number]+1
    Total++
}
local Cur=0
foreach (I,V:number=Votes) {
    Cur++
    Scr:egpText(Cur,I+" - "+round(V/Total*100)+"% ("+V+")",vec2(0,30*Cur))
}
