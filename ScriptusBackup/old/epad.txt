@name EPad
@inputs X Y Use Scr:wirelink
@outputs A RST XD YD
@persist Code:string Cur:string Entered Location:vector Wait
@trigger all
@model models/hunter/plates/plate.mdl
Code="7417"
A=Entered
Location=vec(ceil(X*30),ceil(Y*18),0)
if (first()) {RST=1}
if (RST==1) {timer("RST",100)}
if (clk("RST")) {RST=0 timer("RST2",10000)}
if (clk("RST2")) {RST=1}
XD=Location:x()
YD=Location:y()
if (Entered) {
Scr:writeString("Press anywhere to close",15-round("Press anywhere to close":length()/2),8)
if (Use) {Entered=0}
    
} else {

Scr:writeString("1",14,6,999,222)
Scr:writeString("2",15,6,999,222)
Scr:writeString("3",16,6,999,222)
Scr:writeString("4",14,7,999,222)
Scr:writeString("5",15,7,999,222)
Scr:writeString("6",16,7,999,222)
Scr:writeString("7",14,8,999,222)
Scr:writeString("8",15,8,999,222)
Scr:writeString("9",16,8,999,222)
Scr:writeString("C",14,9,0,900)
Scr:writeString("0",15,9,999,222)
Scr:writeString("E",16,9,0,190)





}

if (Use & !Wait & !Entered) {
    if (Location:y()==7) {
        if (Location:x()==15) {Cur=Cur+"1"}
        if (Location:x()==16) {Cur=Cur+"2"}
        if (Location:x()==17) {Cur=Cur+"3"}
    }
    if (Location:y()==8) {
        if (Location:x()==15) {Cur=Cur+"4"}
        if (Location:x()==16) {Cur=Cur+"5"}
        if (Location:x()==17) {Cur=Cur+"6"}
    }
        if (Location:y()==9) {
        if (Location:x()==15) {Cur=Cur+"7"}
        if (Location:x()==16) {Cur=Cur+"8"}
        if (Location:x()==17) {Cur=Cur+"9"}
    }
    if (Location:y()==10) {
        if (Location:x()==15) {Cur="" RST=1}
        if (Location:x()==16) {Cur=Cur+"0"}
        if (Location:x()==17) {
            if (Cur==Code) {Cur="" Entered=1} else {Cur=""}
        }
        
    }
    timer("UseClk",200)
    Wait=1
    Scr:writeString("   ",14,5,0,222)
    Scr:writeString(Cur:length():toString(),14,5,999,222)
}
if (clk("UseClk")) {Wait=0}
if (changed(Entered)) {RST=1}

