@name Saucer2 Entity Arm
@model models/hunter/plates/plate1x1.mdl
@persist [Entity Owner Target]:entity

if(first()) {
    Entity=entity()
    Owner=owner()
    
    ## base ##
    
    holoCreate(1,vec(),vec(1),ang(),vec(128),"models/hunter/tubes/circle4x4.mdl")
    holoMaterial(1,"phoenix_storms/dome")
    # 
    holoCreate(2,vec(0,0,-2),vec(0.5,0.5,1),ang(),vec(255),"models/hunter/tubes/circle4x4.mdl")
    holoParent(2,1)
    holoMaterial(2,"phoenix_storms/plastic")
    
    ## rotating lights ##
    
    holoCreate(3,vec(),vec(),ang())
    holoParent(3,1)
    
    for(I=4,12) {
        local Ang=ang(0,I*45,0)
        
        holoCreate(I,Ang:forward()*80+vec(0,0,8),vec(0.6),toWorldAng(vec(),ang(-10,0,0),vec(),Ang),vec(255),"models/props_c17/light_industrialbell02_on.mdl")
        holoParent(I,3)
    }
    
    ## light rack ##
    holoCreate(13,vec(0,0,-2),vec(0.89,0.89,0.5),ang(),vec(255),"models/hunter/tubes/tube4x4x025.mdl")
    holoParent(13,1)
    
    ## arm ##
    
    # portal
    holoCreate(14,vec(0,0,-3.6),vec(0.3,0.3,0),ang(),vec(255),"models/hunter/tubes/circle4x4.mdl")
    holoMaterial(14,"debug/debugdrawflat")
    holoParent(14,1)
    
    # pivot
    
    holoCreate(15,vec(0,0,-15),vec(1),ang(180,0,0),vec(255),"models/Mechanics/robotics/xfoot.mdl")
    holoParent(15,14)
    
    # arm 1
    holoCreate(16,vec(0,0,-30),vec(1),ang(-90,90,0),vec(255),"models/Mechanics/robotics/h3.mdl")
    holoParent(16,14)
    
    # arm 2
    holoCreate(17,vec(0,0,-150),vec(1),ang(-90,90,0),vec(255),"models/Mechanics/robotics/c3.mdl")
    holoParent(17,14)
    
    # arm 1->arm 2 detailing
    holoCreate(18,vec(0,0,-90),vec(1.1,0.5,0.5),ang(),vec(255),"models/XQM/cylinderx1.mdl")
    holoParent(18,16)
    holoMaterial(18,"phoenix_storms/iron_rails")
    
    # end effector base
    holoCreate(19,vec(0,0,-220),vec(1),ang(0,0,90),vec(255),"models/Mechanics/robotics/d1.mdl")
    holoParent(19,17)
    
    # end effector claws
    holoCreate(20,vec(15,0,-230),vec(1),ang(90,90,0),vec(255),"models/Mechanics/robotics/b1.mdl")
    holoParent(20,19)
    
    holoCreate(21,vec(-15,0,-230),vec(1),ang(90,-90,0),vec(255),"models/Mechanics/robotics/b1.mdl")
    holoParent(21,19)
    
    ## joints ##
    
    holoCreate(22,vec(0,0,-3.6),vec(1),ang(),vec(255),"models/hunter/blocks/cube05x05x05.mdl")
    holoMaterial(22,"models/wireframe")
    holoParent(22,1)
    
    holoCreate(23,vec(0,0,-3.6),vec(1),ang(),vec(255),"models/hunter/blocks/cube05x05x05.mdl")
    holoMaterial(23,"models/wireframe")
    holoParent(23,1)
    
    holoCreate(24,vec(0,0,-90),vec(1),ang(),vec(255),"models/hunter/blocks/cube05x05x05.mdl")
    holoMaterial(24,"models/wireframe")
    holoParent(24,1)
    
    ## reparent to joints ##
    holoParent(15,22)
    holoParent(16,23)
    holoParent(17,24)
    
    holoParent(24,16)
    holoParent(23,15)
    
    
    ## lights ##
    lightCreate(1,vec())
    lightParent(1,holoEntity(1))
    lightColor(1,vec(16))
    
    holoPos(1,Entity:pos())
    holoParent(1,Entity)
}

local Time=curtime()
holoAng(3,Entity:toWorld(ang(0,Time*15,0)))

holoAng(22,holoEntity(1):toWorld(ang(0,Time*15,0)))
holoAng(23,holoEntity(22):toWorld(ang(0,0,Time*15)))
holoAng(24,holoEntity(23):toWorld(ang(0,0,Time*15)))

interval(100)
