#helpful EGP material list
#scriptis 2013
#last updated 6/17/13

##weapons
#weapons/weapon_bd_mp7
#weapons/weapon_mad_357
#weapons/weapon_mad_57
#weapons/weapon_mad_admin
#weapons/weapon_mad_ak47
#weapons/weapon_mad_alyxgun
#weapons/weapon_mad_ar2
#weapons/weapon_mad_aug
#weapons/weapon_mad_awp
#weapons/weapon_mad_c4
#weapons/weapon_mad_charge
#weapons/weapon_mad_combine_sniper
#weapons/weapon_mad_crossbow
#weapons/weapon_mad_crow
#weapons/weapon_mad_deagle
#weapons/weapon_mad_dual
#weapons/weapon_mad_engineer (gravgun)
#weapons/weapon_mad_famas
#weapons/weapon_mad_fists
#weapons/weapon_mad_flash
#weapons/weapon_mad_g3
#weapons/weapon_mad_galil
#weapons/weapon_mad_glock
#weapons/weapon_mad_gnome
#weapons/weapon_mad_grenade
#weapons/weapon_mad_knife
#weapons/weapon_mad_m249
#weapons/weapon_mad_m3
#weapons/weapon_mad_m4
#weapons/weapon_mad_mac10
#weapons/weapon_mad_magnade
#weapons/weapon_mad_medic
#weapons/weapon_mad_mine
#weapons/weapon_mad_mp5
#weapons/weapon_mad_mp7 (nomad)
#weapons/weapon_mad_p228
#weapons/weapon_mad_p90
#weapons/weapon_mad_rpg
#weapons/weapon_mad_scout
#weapons/weapon_mad_sg550
#weapons/weapon_mad_sg552
#weapons/weapon_mad_smoke
#weapons/weapon_mad_spas
#weapons/weapon_mad_tmp
#weapons/weapon_mad_ump
#weapons/weapon_mad_usp
#weapons/weapon_mad_usp_match (HL2 USP)
#weapons/weapon_mad_xm1014
#your new background phoenix_storms/concrete0

##skybox
#sky background skybox/cxrt broken
#sky background 2 skybox/havlf broken
#3 skybox/militia_hdrup
#4 skybox/sky_day01_04rt broken
#5 skybox/sky_halloweendn

##backgrounds (picture frame, etc)
#console/background_2fort
#console/background_gravelpit
#console/background_mvm
#console/background_upward
#console/background_xmas2011
#console/portal2_product_1
#console/portal2_product_2
#console/title_blue
#console/title_fullmoon
#console/title_red
#console/title_team
#console/title_team_halloween
#console/title_team_halloween2011
#console/title_team_halloween2012
#console/title_war
#console/title_war_widescreen
#vgui/main_menu/meta_cityonfire512
#vgui/main_menu/motd_mechaupdate
#vgui/main_menu/motd_robo_2013
#vgui/main_menu/screamfortress
#vgui/illustrations/gamemode_merasmus
#vgui/appchooser/background_ep2
#vgui/appchooser/background_episodic
#vgui/appchooser/background_hl2
#vgui/appchooser/background_orange
#vgui/appchooser/background_portal
#vgui/appchooser/background_tf
#vgui/appchooser/ep2
#vgui/appchooser/episodic
#vgui/appchooser/hl2
#vgui/appchooser/tf
#vgui/steam_trading

##buttons
#vgui/button_central_disabled
#vgui/button_central_hover


##misc
#gold star - vgui/importtool_goldstar
#computer overlay - vgui/pipboy_overlay
#playstation network - vgui/psn_logo
#shopping cart - vgui/store_cart
#steam workshop background - vgui/workshop_blue_top

##gradients
#gui/gradient_up
#gui/gradient_down
#gui/center_gradient


##cursors
#vgui/cursors/arrow
#vgui/cursors/waitarrow
#vgui/cursors/hand

##ui
#vgui/spawnmenu/hover
#vgui/button_motd (mail)
#vgui/new



#use this for the vending menu -> environment maps/pipemetal001a

##character backgrounds
#console/characters/fwk_engineer
#console/characters/fwk_demoman
#console/characters/fwk_sniper
#console/characters/hwn_soldier
#console/characters/mvm_sniper_bot
#console/characters/xms2011_heavy

