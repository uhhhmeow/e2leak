@name Gear Lock
@outputs A
@persist Timer IdlePicked:array Cleanup:array HeldTime Entity:entity
@model models/hunter/plates/plate05x05.mdl

if(first()) {
    Entity=entity()
    
    holoCreate(1,Entity:toWorld(vec(0,3.8,0.75)),vec(0.5,0.5,0.3),Entity:toWorld(ang()),vec(255),"models/Mechanics/gears/gear12x12.mdl")
    holoCreate(2,Entity:toWorld(vec(0,-7.2,0.75)),vec(0.3,0.3,0.2),Entity:toWorld(ang()),vec(255),"models/Mechanics/gears/gear12x12.mdl")
    holoCreate(3,Entity:toWorld(vec(-18,-7,2)),vec(1),Entity:toWorld(ang(180,15,-90)),vec(255),"models/weapons/w_crowbar.mdl")
    holoCreate(4,holoEntity(3):toWorld(vec(-18,0,0)),vec(0),Entity:toWorld(ang(180,15,-90)))
    holoCreate(5,Entity:pos(),vec(1),Entity:angles(),vec(255),entity():model())
    holoParent(3,4)
    
    holoParent(1,Entity)
    holoParent(2,Entity)
    holoParent(3,Entity)
    holoParent(5,Entity)
    
    holoMaterial(5,"models/props_combine/metal_combinebridge001")
    
    Entity:setAlpha(0)
    IdlePicked=array(
        "weapons/357/357_reload1.wav",
        "weapons/357/357_reload3.wav",
        "weapons/357/357_reload4.wav"
    )
    
    function string rs() {
        local Ar=array("a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z")
        local Out=""
        for(I=1,randint(5)) {
            Out=Out+Ar[randint(Ar:count()),string]
        }
        return Out
    }
    
    function onPicked() {
        Entity:soundPlay(rs(),0,"ambient/machines/spindown.wav")
        Entity:soundPlay(rs(),0,"ambient/machines/zap2.wav")
        Entity:soundPlay(rs(),0,"weapons/crowbar/crowbar_impact1.wav")
        holoColor(3,vec(255))
        
        holoAlpha(3,255)
        holoMaterial(3,"")
        holoColor(3,vec(255))
        holoVisible(3,players(),1)
        Timer=70
        A=1
    }
    
    function onPickEnded() {
        Entity:soundPlay(rs(),0,"weapons/crowbar/crowbar_impact2.wav")
        Entity:soundPlay(rs(),0,"ambient/machines/spinup.wav")
        holoVisible(3,players(),0)
        holoAlpha(3,128)
        holoMaterial(3,"debug/debugdrawflat")
        holoColor(3,vec(0,255,0))
        
        local Ent=propSpawn("models/weapons/w_crowbar.mdl",holoEntity(3):pos(),holoEntity(3):angles(),0)
        Ent:applyForce((vec(0,0,50)+Entity:up()*50)*Ent:mass())
        Cleanup:pushEntity(Ent)
        noCollideAll(Ent,1)
        timer("Cleanup",5000)
        
        A=0
    }

    holoVisible(3,players(),0)
    holoAlpha(3,128)
    holoMaterial(3,"debug/debugdrawflat")
    holoColor(3,vec(0,255,0))
    #if(first()) {onPicked()}
}
local Time=curtime()*80

if(Timer==0) {
    holoAng(1,Entity:toWorld(ang(0,Time,0)))
    holoAng(2,Entity:toWorld(ang(0,-Time,0)))
    findByClass("player")
    local Player=findClosest(Entity:pos())
    if(Player:weapon():type():matchFirst("lockpick")) {
        holoVisible(3,Player,1)
        if(Player:keyAttack1() & Player:aimEntity()==Entity) {
            HeldTime=clamp(HeldTime+1,0,80)
            if(HeldTime%10==0) {
                Entity:soundPlay(rs(),0,IdlePicked[randint(IdlePicked:count()),string])
            }
            
            if(HeldTime==80) {
                HeldTime=0
                onPicked()
            }
        } else {HeldTime=clamp(HeldTime-1,0,80)}
        local HeldCol=HeldTime/80
        holoColor(3,HeldCol*vec(-255,255,0)+vec(255,0,0))
    } else {
        holoVisible(3,Player,0)
    }
} else {
    Timer--
    if(Timer%10==0 & Timer>0) {
        Entity:soundPlay(rs(),0,"ambient/machines/ticktock1.wav")
    }
    local Sin=sin(Time*5000)*3
    holoAng(1,Entity:toWorld(ang(0,Sin,0)))
    holoAng(2,Entity:toWorld(ang(0,-Sin,0)))
    holoAng(4,Entity:toWorld(ang(180,15+Sin/3,-90)))
    if(Timer==0) {
        onPickEnded()
    }
}
interval(100)

if(clk("Cleanup")) {
    foreach(I,V:entity=Cleanup) {
        V:propDelete()
    }
    Cleanup:clear()
}
