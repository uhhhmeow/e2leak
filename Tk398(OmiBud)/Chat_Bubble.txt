@name Chat Bubble
runOnTick(1)
if(first()){
    for(I=1,maxPlayers()){
        holoCreate(I)
        holoModel(I,"models/extras/info_speech.mdl")
        holoAlpha(I,0)
    }
}

for(I=1,numPlayers()){
holoPos(I,players():entity(I):pos()+vec(0,0,100))
holoAng(I,ang(0,curtime()*50,0))
if (players():entity(I):isTyping())
{
    holoAlpha(I,255)
}
else
{
    holoAlpha(I,0)
}
}
