@name Mini tank
@inputs 
@outputs 
@persist 
@trigger 

if(first()) {
    #Body
    holoCreate(0)
    
    #Ranger holo
    holoCreate(1)
    holoScale(1, vec(0.2,0.2,0.2))
    holoParent(1, 0)
    
    runOnTick(1)
    
}

Ra = rangerOffset(8, holoEntity(0):pos(), holoEntity(0):up()*-1) 
holoPos(1, Ra:position())

if(Ra:hit()) {
    holoPos(0, -Ra:position() + entity():pos() + vec(0,0,1))
}
