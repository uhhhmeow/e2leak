@name Mini gun
@inputs 
@outputs 
@persist 
@trigger 

if(first()) {
    runOnTick(1)
    #tubes
    holoCreate(1)
    holoCreate(2)
    holoCreate(3)
    
    #things
    holoCreate(4)
    holoCreate(5)
    
    
    #tubes
    holoModel(1, "hqcylinder")
    holoScale(1, vec(0.4,0.4,5))
    holoAng(1, ang(90,0,0))
    holoPos(1, entity():pos() + entity():right()*-3.5 + entity():up()*18)
    
    holoModel(2, "hqcylinder")
    holoScale(2, vec(0.4,0.4,5))
    holoAng(2, ang(90,0,0))
    holoPos(2, entity():pos() + entity():right()*3.5 + entity():up()*18)
    
    holoModel(3, "hqcylinder")
    holoScale(3, vec(0.4,0.4,5))
    holoAng(3, ang(90,0,0))
    holoPos(3, entity():pos() + entity():forward()*-4 + entity():up()*18)
    
    #things
    holoModel(4, "hqcylinder")
    holoScale(4, vec(1.2,1.2,0.3))
    holoPos(4, entity():pos() + entity():up()*10 + entity():forward()*-1)
    
    holoModel(5, "hqcylinder")
    holoScale(5, vec(1.2,1.2,0.3))
    holoPos(5, entity():pos() + entity():up()*35 + entity():forward()*-1)
}
