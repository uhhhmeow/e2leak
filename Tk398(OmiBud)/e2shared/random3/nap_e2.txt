@name Nap e2
@inputs 
@outputs 
@persist I
@trigger 
interval(1)

if(first()) {
    holoTextCreate(1)
    holoText(1,"Darkmind is taking a nap")
    holoTextParent(1, entity())
}


holoTextPos(1, owner():pos() + owner():up()*100)
holoTextAng(1, owner():angles() + ang(0,0,90))
