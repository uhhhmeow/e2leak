@name AA gun
@inputs [Body Gun ]:entity Pod:wirelink 
@outputs 
@persist 
@trigger 


if(first()) {
    runOnTick(1)
}

Active = Pod["Active",number]
Chair = Pod["Entity",entity]

TargetAngle = Chair:driver():eyeAngles()
E = Body
Torque = E:toLocal(rotationVector(quat(TargetAngle)/quat(E))+E:pos())

Angles = owner():eyeAngles():setYaw( owner():eyeAngles():yaw() )
Angles2 = owner():eyeAngles():setPitch( owner():eyeAngles():pitch() * 90 )

if(Active) {
    Body:setAng( Angles2:setPitch(0) )
    Gun:setAng( Angles:setRoll(0):setYaw(0))

}

