@name Holo Radar
@inputs 
@persist Players:array Target:entity
@trigger 

runOnTick(1)
runOnChat(1)
S = owner():lastSaid():explode(" ")

if (first()) {
    #Find all players
    findByClass("player")
    Players = findToArray()

    #Spawn holograms for each player
    for(I=1,Players:count()) {
        holoCreate(I)
        holoModel(I,"hqicosphere2")
        holoScale(I,vec(4,4,4)/12)
        holoMaterial(I,"models/debug/debugwhite")
    }
    #Black background
    holoCreate(100)
    holoScale(100,vec(200,200,1)/12)
    holoColor(100,vec())
    holoAlpha(100,150)
    holoMaterial(100,"models/debug/debugwhite")
    

}

if(S:string(1)=="/t" && chatClk(owner())){
    Target = findPlayerByName(S:string(2))
}

#Move holograms
for(I=1,Players:count()) {
    holoPos(I,entity():pos() + entity():toLocal(Players[I,entity]:pos()) / 100 + vec(0,0,10))
    if(Players[I,entity] == owner()){
        holoColor(I, vec(0,255,0))
    }elseif(Players[I,entity] == Target){
        holoColor(I, vec(255,0,0))
    }elseif(Players[I,entity] != owner()|Target){
        holoColor(I, vec(255,255,255))
    }
}


    

