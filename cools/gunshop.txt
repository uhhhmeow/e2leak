@name Weapon Vending Machine
@inputs C:wirelink Up Down Select Inserted Recieved R1 R2 R3 R4 R5 R6
@outputs U1 U2 U3 U4 U5 U6 Hydro
@persist V1 V2 V3 V4 V5 V6 V7 V8 Value
@persist ShotgunPrice MAC10Price AK47Price M16Price SniperPrice MP5Price
@persist OldMoney Waiting Vend FirstRun TakeMoney
@trigger Up Down Select R1 R2 R3 R4 R5 R6
 
if (first()) {
    TakeMoney = 0
    FirstRun = 1
    Hydro = 186
    Vend = 0
    #Set Up The GUI
    C:writeCell(2041,1)
    C:writeString("      Gun Vending Machine     ",0,0,999,0)
    C:writeString("        Guns For Sale!!       ",0,1,999,0)
    C:writeString("Gun:       Price:       Stock?",0,3,100,50)
    C:writeString(" ============================ ",0,11,999,0)
    C:writeString(" ============================ ",0,12,999,0)
    C:writeString(" ============================ ",0,13,999,0)
    C:writeString(" ============================ ",0,14,999,0)
    C:writeString(" ============================ ",0,15,999,0)
    C:writeString(" ============================ ",0,16,999,0)
    #Prices For Guns
    ShotgunPrice = 200
    MAC10Price = 300
    AK47Price = 400
    SniperPrice = 500
    M16Price = 400
    MP5Price = 350
    
}
#Change Selection
if(Up&~Up){Value-=1}
if(Down&~Down){Value+=1}
Value=clamp(Value,1,6)

#Assign V1-6 from Value
V1 = (Value==1 ? 1 : 0)
V2 = (Value==2 ? 1 : 0)
V3 = (Value==3 ? 1 : 0)
V4 = (Value==4 ? 1 : 0)
V5 = (Value==5 ? 1 : 0)
V6 = (Value==6 ? 1 : 0)

#Display Prices
C:writeString("Shotgun     $"+ShotgunPrice+"           ",0,4,999,V1*60) 
C:writeString("MAC-10      $"+MAC10Price  +"           ",0,5,999,V2*60)
C:writeString("AK-47       $"+AK47Price   +"           ",0,6,999,V3*60)
C:writeString("M-16        $"+M16Price    +"           ",0,7,999,V4*60)
C:writeString("Sniper      $"+SniperPrice +"           ",0,8,999,V5*60)
C:writeString("MP-5        $"+MP5Price    +"           ",0,9,999,V6*60)

#Display Stock
if(!R1){C:writeString("N",27,4,900,V1*60)}else{C:writeString("Y",27,4,999,V1*60)}
if(!R2){C:writeString("N",27,5,900,V2*60)}else{C:writeString("Y",27,5,999,V2*60)}
if(!R3){C:writeString("N",27,6,900,V3*60)}else{C:writeString("Y",27,6,999,V3*60)}
if(!R4){C:writeString("N",27,7,900,V4*60)}else{C:writeString("Y",27,7,999,V4*60)}
if(!R5){C:writeString("N",27,8,900,V5*60)}else{C:writeString("Y",27,8,999,V5*60)}
if(!R6){C:writeString("N",27,9,900,V6*60)}else{C:writeString("Y",27,9,999,V6*60)}

#Selection Made
if (Select& ~Select & Vend == 0){
    #Find Sale Price
    CurrentPrice = ShotgunPrice*V1+ MAC10Price*V2+ AK47Price*V3+ M16Price*V4+ SniperPrice*V5+ MP5Price*V6
    #Enough Money? - Too much is enough; if your dumb enough to put more in, your fault!
    
    if(Value==1){if(R1>0){if (Inserted>=CurrentPrice){TakeMoney = 1 LolWait = 1}else{LolPrice = 1}}else{Lolstock = 1}}
    if(Value==2){if(R2>0){if (Inserted>=CurrentPrice){TakeMoney = 1 LolWait = 1}else{LolPrice = 1}}else{Lolstock = 1}}
    if(Value==3){if(R3>0){if (Inserted>=CurrentPrice){TakeMoney = 1 LolWait = 1}else{LolPrice = 1}}else{Lolstock = 1}}
    if(Value==4){if(R4>0){if (Inserted>=CurrentPrice){TakeMoney = 1 LolWait = 1}else{LolPrice = 1}}else{Lolstock = 1}}
    if(Value==5){if(R5>0){if (Inserted>=CurrentPrice){TakeMoney = 1 LolWait = 1}else{LolPrice = 1}}else{Lolstock = 1}}
    if(Value==6){if(R6>0){if (Inserted>=CurrentPrice){TakeMoney = 1 LolWait = 1}else{LolPrice = 1}}else{Lolstock = 1}}
    #Not Enough Money :-(
    if(LolPrice){
        C:writeString("Not Enough Money",7,13,900,0,1)
        C:writeString("Insert$"+(CurrentPrice - Inserted),10,15,900,0)
        timer("Reset",3000)
    }
    #If all goes to plan (right money + stock), show a "wait" message
    if(LolWait){
        hint("Lol",3)
        C:writeString(" _       _____    __________" ,1,12,999,0)
        C:writeString("| |     / /   |  /  _/_  __/" ,1,13,999,0)
        C:writeString("| | /| / / /| |  / /  / /   " ,1,14,999,0)   
        C:writeString("| |/ |/ / ___ |_/ /  / /    "  ,1,15,999,0) 
        C:writeString("|__/|__/_/  |_/___/ /_/     "   ,1,16,999,0)         
        Vend = Value
        LolWait = 0
    }
    #If out of stock, show a Message
    if(Lolstock){
        C:writeString("Out of Stock",9,13,900,0,1)
        C:writeString("Choose Another",8,15,900,0)
        timer("Reset",3000)
    }
}

#Taking the money and giving the gun
if (TakeMoney){
    if(FirstRun){timer("TakeTheMoney",1) FirstRun = 0}
    #Timings for the hydrolics to take the money
    #If were not waiting for anything (First run through), set how much we are waiting for
    if(!Waiting){Waiting = CurrentPrice}
    #When the Money in the recived container equals how much we had plus what we are waitng for, dispence!
    if(Recieved == OldMoney + Waiting){
        if(Vend==1){U1 = 1 timer("ResetUsers",100) OldMoney += Waiting Waiting = TakeMoney = 0}
        if(Vend==2){U2 = 1 timer("ResetUsers",100) OldMoney += Waiting Waiting = TakeMoney = 0}
        if(Vend==3){U3 = 1 timer("ResetUsers",100) OldMoney += Waiting Waiting = TakeMoney = 0}
        if(Vend==4){U4 = 1 timer("ResetUsers",100) OldMoney += Waiting Waiting = TakeMoney = 0}
        if(Vend==5){U5 = 1 timer("ResetUsers",100) OldMoney += Waiting Waiting = TakeMoney = 0}
        if(Vend==6){U6 = 1 timer("ResetUsers",100) OldMoney += Waiting Waiting = TakeMoney = 0}
        timer("Reset",3000)
        Vend = 0
        FirstRun = 1
    }
    else{
        timer("CheckAgain",100)
    }
}
#But Wait! theres more (behind the scenes code!)

#Reset the message at the bottom
if(clk("Reset")){
    C:writeString(" ============================ ",0,12,999,0)
    C:writeString(" ============================ ",0,13,999,0)
    C:writeString(" ============================ ",0,14,999,0)
    C:writeString(" ============================ ",0,15,999,0)
    C:writeString(" ============================ ",0,16,999,0)
}

if(clk("TakeTheMoney")){
    Hydro = 140
    timer("P2",500)
}
if(clk("P2")){
    Hydro = 186
}
    
if(clk("ResetUsers")){U1 = U2 = U3 = U4 = U5 = U6 = 0}
