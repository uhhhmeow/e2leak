@name Wut Shield
@persist Timer Player:entity
interval(1)

Timer=Timer+1*0.1
T=Timer

holoCreate(1) holoCreate(2)

holoModel(1,"sphere")
holoAng(1,ang(T,T,0))
holoColor(1,vec(0,0,220))
holoAlpha(1, 220)
holoScale(1, vec(200,200,200))
holoScale(2, vec(200,200,200))
holoModel(2,"sphere")
holoAng(2,ang(T,T,0))
holoColor(2,vec(255,215,0))
holoScale(2, vec(0,0,220))

