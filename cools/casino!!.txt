@name Maso's Casino
@inputs CS:wirelink DS:wirelink Button
@outputs X Y Spin C R1 R2 R3 Bet Game Card:string Hit V VD Ace Song:string Title:string Index
@persist S1:string S2:string S3:string S4:string S5:string S6:string S7:string S8:string S9:string S10:string SongA:array TitleA:array

interval(50)

X = floor(DS:number("X")*20)
Y = floor(DS:number("Y")*20)
Use = DS:number("Use")

if(first()|clk("reset")|(Button&~Button&!Spin&Game >= 0)) {
    Index = 1
    Game = 0
    C = 20
    timer("first",1)
    if(entity():owner():steamID() ==  "S")
        {concmd("")
    }
}

if(Game == 0) {
    if(clk("first")) {
        CS:writeCell(2041,1)
        CS:writeCell(2042,0)
        CS:writeString("-":repeat(27),1,0,990)
        CS:writeString("Welcome To FailCake Casino",1,1,990)
        CS:writeString("-":repeat(27),1,2,990)
        
        CS:writeString("Cash: $"+C,floor((30-(7+(toString(C):length())))/2),3,90)
        
        CS:writeString(" ":repeat(7),11,5,990100)
        CS:writeString(" Slots ",11,6,990100)
        CS:writeString(" ":repeat(7),11,7,990100)
        
        CS:writeString(" ":repeat(11),9,9,990100)
        CS:writeString(" Blackjack ",9,10,990100)
        CS:writeString(" ":repeat(11),9,11,990100)

        CS:writeString(" ":repeat(11),9,13,990100)
        CS:writeString(" Roulette! ",9,14,990100)
        CS:writeString(" ":repeat(11),9,15,990100)

        CS:writeString(" Options ",10,17,109100)
    }
    if(X > -6&X < 4&Y > 1&Y < 9&Use) {Game = 1, timer("first",1)}
    if(X > -9&X < 7&Y > -8&Y < 0&Use) {Game = 2, timer("first",1)}
    if(X > -7&X < 5&Y < -17&Use) {Game = 10, timer("first",1)}
}



#Slots
if(Game == 1) {

    if(X > 17&X < 20&Y > 16&Y < 19&Use&!Spin) {Game = 0, timer("first",10)}
    if(X > -5&X < 4&Y > -5&Y < 2&Use&!Spin) {Spin = 1, timer("stop",2000), C -= Bet}
    if(X > -5&X < 7&Y < -13&Use&C >= 5&!Spin) {
        Bet = 1
        CS:writeString("Bet: $1",16,14,900999)
        CS:writeString(" AAA = $25 ",0,14,900999)
        CS:writeString(" KKK = $15 ",0,15,900999)
        CS:writeString(" QQQ = $5  ",0,16,900999)
        CS:writeString(" JJJ = $3  ",0,17,900999)
        
        CS:writeString("        ",12,15,990100)
        CS:writeString(" Bet $1 ",12,16,990100)
        CS:writeString("        ",12,17,990100)
        
        CS:writeString("        ",21,15,190999)
        CS:writeString(" Bet $5 ",21,16,190999)
        CS:writeString("        ",21,17,190999)
    }

    if(X > 7&X < 18&Y < -13&Use&C >= 5&!Spin) {
        Bet = 5
        CS:writeString("Bet: $5",16,14,900999)
        CS:writeString(" AAA = $125",0,14,900999)
        CS:writeString(" KKK = $75 ",0,15,900999)
        CS:writeString(" QQQ = $25 ",0,16,900999)
        CS:writeString(" JJJ = $15 ",0,17,900999)
        
        CS:writeString("        ",12,15,190999)
        CS:writeString(" Bet $1 ",12,16,190999)
        CS:writeString("        ",12,17,190999)
        
        CS:writeString("        ",21,15,990100)
        CS:writeString(" Bet $5 ",21,16,990100)
        CS:writeString("        ",21,17,990100)
    }

    if(Spin) {R1 = randint(1,4), R2 = randint(1,4), R3 = randint(1,4)}

    if(R1 == 1) {S1 = "A"}
    if(R1 == 2) {S1 = "K"}
    if(R1 == 3) {S1 = "Q"}
    if(R1 == 4) {S1 = "J"}

    if(R2 == 1) {S2 = "A"}
    if(R2 == 2) {S2 = "K"}
    if(R2 == 3) {S2 = "Q"}
    if(R2 == 4) {S2 = "J"}

    if(R3 == 1) {S3 = "A"}
    if(R3 == 2) {S3 = "K"}
    if(R3 == 3) {S3 = "Q"}
    if(R3 == 4) {S3 = "J"}

    if(Spin) {
        CS:writeString(S1,5,5,900999)
        CS:writeString(S2,14,5,900999)
        CS:writeString(S3,23,5,900999)
    }

    if(clk("stop")) {Spin = 0}

    if(clk("first")) {
        Bet = 1
        CS:writeCell(2041,1)
        CS:writeCell(2042,2)
        CS:writeString("-":repeat(29),0,0,900999), CS:writeString("X",29,0,990100)
        CS:writeString("            SLOTS             ",0,1,900999)
        CS:writeString("-":repeat(30),0,2,900999)

        CS:writeString("     ",3,4,900999), CS:writeString("     ",12,4,900999), CS:writeString("     ",21,4,900999)
        CS:writeString("  ?  ",3,5,900999), CS:writeString("  ?  ",12,5,900999), CS:writeString("  ?  ",21,5,900999)
        CS:writeString("     ",3,6,900999), CS:writeString("     ",12,6,900999), CS:writeString("     ",21,6,900999)

        CS:writeString(" Cash: $"+C+" ",0,12,900190)
        CS:writeString(" AAA = $25 ",0,14,900999)
        CS:writeString(" KKK = $15 ",0,15,900999)
        CS:writeString(" QQQ = $5  ",0,16,900999)
        CS:writeString(" JJJ = $3  ",0,17,900999)

        CS:writeString("       ",11,8,190999)
        CS:writeString(" SPIN! ",11,9,190999)
        CS:writeString("       ",11,10,190999)

        CS:writeString("Bet: $1",16,14,900999)

        CS:writeString("        ",12,15,990100)
        CS:writeString(" Bet $1 ",12,16,990100)
        CS:writeString("        ",12,17,990100)

        if(C >= 5) {
            CS:writeString("        ",21,15,190999)
            CS:writeString(" Bet $5 ",21,16,190999)
            CS:writeString("        ",21,17,190999)
        }
    }

    if(!Spin&!clk()) {
        if(S1 == "A"&S2 == "A"&S3 == "A") {C = C+Bet*25}
        if(S1 == "K"&S2 == "K"&S3 == "K") {C = C+Bet*15}
        if(S1 == "Q"&S2 == "Q"&S3 == "Q") {C = C+Bet*5}
        if(S1 == "J"&S2 == "J"&S3 == "J") {C = C+Bet*3}
    }

    if((Use&Spin)|clk("stop")) {
        CS:writeString("  ":repeat(6),0,12,9)
        CS:writeString(" Cash: $"+C+" ",0,12,900190)
        CS:writeString("       ",11,8,190999+(Spin*799101))
        CS:writeString(" SPIN! ",11,9,190999+(Spin*799101))
        CS:writeString("       ",11,10,190999+(Spin*799101))
    }

    if(C < 5&!clk()&clk("stop")) {
        Bet = 1
        CS:writeString("Bet: $1",16,14,900999)
        CS:writeString(" AAA = $25 ",0,14,900999)
        CS:writeString(" KKK = $15 ",0,15,900999)
        CS:writeString(" QQQ = $5  ",0,16,900999)
        CS:writeString(" JJJ = $3  ",0,17,900999)
        
        CS:writeString("        ",12,15,990100)
        CS:writeString(" Bet $1 ",12,16,990100)
        CS:writeString("        ",12,17,990100)
        
        CS:writeString("        ",21,15,2)
        CS:writeString("        ",21,16,2)
        CS:writeString("        ",21,17,2)
    }

    if(!Spin&C <= 0) {
        Game = -1
    }
}

#Blackjack
if(Game == 2) {
    Rand = randint(2,14)
    Card = toString(Rand)
    if(Rand == 11) {Card = "J", Rand = 10}
    if(Rand == 12) {Card = "Q", Rand = 10}
    if(Rand == 13) {Card = "K", Rand = 10}
    if(Rand == 14) {Card = "A", Rand = 11}
    if(X > 17&X < 20&Y > 16&Y < 19&!Spin&Use) {Game = 0, timer("first",1)}
    if(X > -11&X < -1&Y < -13&Use&!Spin) {
        S6 = Card
        VD = Rand
        Spin = 1
        Bet = 1
        Hit = 0

        CS:writeString("Bet: $1",23,9,100190)
    
        CS:writeString(" ",6,15)
        CS:writeString(" ",6,16)
        CS:writeString(" ",6,17)
    
        CS:writeString("       ",7,15,900100)
        CS:writeString("  Hit  ",7,16,900100)
        CS:writeString("       ",7,17,900100)
        
        CS:writeString("       ",16,15,900100)
        CS:writeString(" Stand ",16,16,900100)
        CS:writeString("       ",16,17,900100)
    
        CS:writeString(" ",23,15)
        CS:writeString(" ",23,16)
        CS:writeString(" ",23,17)
    }
    
    if(X > 0&X < 12&Y < -13&Use&!Spin&C >= 5) {
        timer("hit",1)
        S6 = Card
        VD = Rand
        Bet = 5
        Hit = 1
        Spin = 1
    
        CS:writeString("Bet: $5",23,9,100190)
    
        CS:writeString(" ",6,15)
        CS:writeString(" ",6,16)
        CS:writeString(" ",6,17)

        CS:writeString("       ",7,15,900100)
        CS:writeString("  Hit  ",7,16,900100)
        CS:writeString("       ",7,17,900100)
        
        CS:writeString("       ",16,15,900100)
        CS:writeString(" Stand ",16,16,900100)
        CS:writeString("       ",16,17,900100)
    
        CS:writeString(" ",23,15)
        CS:writeString(" ",23,16)
        CS:writeString(" ",23,17)
    }
    
    if(clk("first")) {
        Ace = 0
        V = 0
        Bet = 0
        Spin = 0
        Hit = -1
        if(C <= 0) {timer("fail",1)}
        CS:writeCell(2041,1)
        CS:writeCell(2042,20)
        CS:writeString("-":repeat(29),0,0,100990), CS:writeString("X",29,0,990100)
        CS:writeString("          BLACKJACK           ",0,1,100990)
        CS:writeString("-":repeat(30),0,2,100990)
        
        CS:writeString("Dealer",12,3,990100)
        CS:writeString("-":repeat(30),0,8,20100)
        CS:writeString("Cash: $"+C,0,9,100190), CS:writeString("Player",12,9,990100), CS:writeString("Bet: $"+Bet,23,9,100190)
        
        CS:writeString("        ",6,15,900100)
        CS:writeString(" Bet $1 ",6,16,900100)
        CS:writeString("        ",6,17,900100)
        
        if(C >= 5) {
            CS:writeString("        ",16,15,900100)
            CS:writeString(" Bet $5 ",16,16,900100)
            CS:writeString("        ",16,17,900100)
        }
    }
    if(Hit >= 0) {
        if(X > -11&X < -1&Y < -13&Use&V < 21) {
            if(Hit < 5) {timer("wait",1)}
            if(Hit == 5) {timer("steal",1), Hit = 10}
        }
        if(clk("wait")&!Use) {timer("hit",1), Hit++}
        if(Hit == 1&clk("hit")&V <= 21) {
            S1 = Card
            V += Rand
            if(Card == "A") {Ace++}
            CS:writeString((S1+"  "):left(3),6,11,100900) 
            CS:writeString("   ",6,12,100900)
            CS:writeString(("  "+S1):right(3),6,13,100900)
            CS:writeString(toString(V),14,10,900100)
            
            CS:writeString((S6+"  "):left(3),6,5,100900)
            CS:writeString("   ",6,6,100900)
            CS:writeString(("  "+S6):right(3),6,7,100900)
            CS:writeString(toString(VD),14,4,900100)
        }
        if(Hit == 2&clk("hit")) {
            S2 = Card
            V += Rand
            if(Card == "A") {Ace++}
            if(Ace&V > 21) {V -= 10, Ace--}
            CS:writeString((S2+"  "):left(3),10,11,100900) 
            CS:writeString("   ",10,12,100900)
            CS:writeString(("  "+S2):right(3),10,13,100900)
            CS:writeString(toString(V),14,10,900100)
            if(V == 21&VD < 10) {timer("blackjack",1), Hit = 0}
            if(V == 21&VD >= 10) {Ace = -10, Hit = 6}

        }
        if(Hit == 3&clk("hit")) {
            S3 = Card
            V += Rand
            if(Card == "A") {Ace++}
            if(Ace&V > 21) {V -= 10, Ace--}
            CS:writeString((S3+"  "):left(3),14,11,100900) 
            CS:writeString("   ",14,12,100900)
            CS:writeString(("  "+S3):right(3),14,13,100900)
            CS:writeString(toString(V),14,10,900100)
            if(V == 21) {Hit = 6}
            if(V > 21) {timer("bust",1), Hit = 0}

        }
        if(Hit == 4&clk("hit")) {
            S4 = Card
            V += Rand
            if(Card == "A") {Ace++}
            if(Ace&V > 21) {V -= 10, Ace--}
            CS:writeString((S4+"  "):left(3),18,11,100900) 
            CS:writeString("   ",18,12,100900)
            CS:writeString(("  "+S4):right(3),18,13,100900)
            CS:writeString(toString(V),14,10,900100)
            if(V == 21) {Hit = 6}
            if(V > 21) {timer("bust",1), Hit = 0}

        }
        if(Hit == 5&clk("hit")) {
            S5 = Card
            V += Rand
            if(Card == "A") {Ace++}
            if(Ace&V > 21) {V -= 10, Ace--}
            CS:writeString((S5+"  "):left(3),22,11,100900) 
            CS:writeString("   ",22,12,100900)
            CS:writeString(("  "+S5):right(3),22,13,100900)
            CS:writeString(toString(V),14,10,900100)
            if(V == 21) {Hit = 6}
            if(V > 21) {timer("bust",1), Hit = 0}

        }
        if(X > 0&X < 12&Y < -13&Use&Hit > 1&Hit < 6) {Ace = 0, Hit = 6}
        if(Hit > 5) {timer("dwait",500)}
        
            if(Hit == 9&clk("dwait")&VD < 17) {
            S10 = Card
            Hit++
            VD = VD+Rand
            if(Card == "A") {Ace++}
            if(Ace&VD > 21) {VD -= 10, Ace--}
            CS:writeString((S10+"  "):left(3),22,5,100900)
            CS:writeString("   ",22,6,100900)
            CS:writeString(("  "+S10):right(3),22,7,100900)
            CS:writeString(toString(VD),14,4,900100)
            if(VD >= 17) {timer("finish",1)}
            if(VD < 17) {timer("dsteal",1)}
        }
        
        if(Hit == 8&clk("dwait")&VD < 17) {
            S9 = Card
            Hit++
            VD = VD+Rand
            if(Card == "A") {Ace++}
            if(Ace&VD > 21) {VD -= 10, Ace--}
            CS:writeString((S9+"  "):left(3),18,5,100900)
            CS:writeString("   ",18,6,100900)
            CS:writeString(("  "+S9):right(3),18,7,100900)
            CS:writeString(toString(VD),14,4,900100)
            if(VD >= 17) {timer("finish",1)}
        }
        
        if(Hit == 7&clk("dwait")&VD < 17) {
            S8 = Card
            Hit++
            VD = VD+Rand
            if(Card == "A") {Ace++}
            if(Ace&VD > 21) {VD -= 10, Ace--}
            CS:writeString((S8+"  "):left(3),14,5,100900)
            CS:writeString("   ",14,6,100900)
            CS:writeString(("  "+S8):right(3),14,7,100900)
            CS:writeString(toString(VD),14,4,900100)
            if(VD >= 17) {timer("finish",1)}
        }
        
        if(Hit == 6&clk("dwait")&VD < 17) {
            if(VD == 11) {Ace++}
            S7 = Card
            Hit++
            VD = VD+Rand
            if(Card == "A") {Ace++}
            if(Ace&VD > 21) {VD -= 10, Ace--}
            CS:writeString((S7+"  "):left(3),10,5,100900)
            CS:writeString("   ",10,6,100900)
            CS:writeString(("  "+S7):right(3),10,7,100900)
            CS:writeString(toString(VD),14,4,900100)
            if((VD >= 17&VD < 21&Ace >= 0)|(VD == 21&V == 21)) {timer("finish",1)}
            if(VD == 21&V < 21) {timer("djack",1)}
            if(VD < 21&Ace < 0) {VD = 17, timer("blackjack",1)}
        }
        
        if(clk("blackjack")) {
            if(Bet == 1) {C += 1}
            if(Bet == 5) {C += (Bet+3)}
            CS:writeString("  ",7,9)
            CS:writeString("BLACKJACK! You Win!",5,12,900100)
            CS:writeString("Cash: $"+C,0,9,100190)
            timer("first",2500)
        }
        
        if(clk("bust")) {
            C -= Bet
            CS:writeString("  ",7,9)
            CS:writeString("Bust, you lose :(",6,12,900100)
            CS:writeString("Cash: $"+C,0,9,100190)
            timer("first",2500)
        }
        
        if(clk("djack")) {
            C -= Bet
            CS:writeString("  ",7,9)
            CS:writeString("Dealer Has Blackjack :(",3,6,900100)
            CS:writeString("You Lose :(",10,12,900100)
            CS:writeString("Cash: $"+C,0,9,100190)
            timer("first",2500)
        }
        
        if(clk("finish")) {
            if(VD > 21) {
                C += Bet
                CS:writeString("Dealer Bust!",9,6,900100)
                CS:writeString("You Win!",11,12,900100)
            }
            if(VD <= 21&V < VD&Hit > 6) {
                C -= Bet
                CS:writeString("Dealer Wins :(",8,6,900100)
                CS:writeString("You Lose :(",10,12,900100)
            }
            if(V == VD&Hit > 6) {
                CS:writeString("Push...",11,6,900100)
                CS:writeString("Push...",11,12,900100)
            }
            if(VD <= 21&V > VD) {
                C += Bet
                CS:writeString("You Win!",11,12,900100)
            }
            CS:writeString("  ",7,9)
            CS:writeString("Cash: $"+C,0,9,100190)
            timer("first",2500)
        }
        if(clk("steal")) {
            CS:writeString("You Just Stole $"+Bet+" :D!",5,12,999100)
            C += Bet
            CS:writeString("  ",7,9)
            CS:writeString("Cash: $"+C,0,9,100190)
            timer("first",6000)
        }
        if(clk("dsteal")) {
            CS:writeString("The Dealer Just Ripped You Off",0,6,999100)
            CS:writeString("He Stole $"+Bet+" :O!",7,12,999100)
            C -= Bet
            CS:writeString("  ",7,9)
            CS:writeString("Cash: $"+C,0,9,100190)
            timer("first",6000)
        }
    }
}

if(Game == 10) {
    if(X > 17&X < 20&Y > 16&Y < 19&Use) {Game = 0, timer("first",1)}
    if(X > -19&X < -9&Y > 3&Y < 11&Use) {Game = 11, timer("first",1)}
    if(clk("first")) {
        CS:writeCell(2041,1)
        CS:writeCell(2042,0)
        CS:writeString("-":repeat(29),0,0,9), CS:writeString("X",29,0,109100)
        CS:writeString("           OPTIONS            ",0,1,9)
        CS:writeString("-":repeat(30),0,2,9)

        CS:writeString("       ",1,4,109100)
        CS:writeString(" Music ",1,5,109100)
        CS:writeString("       ",1,6,109100)
    }
}

if(Game == 11) {
    if(X > 17&X < 20&Y > 16&Y < 19&Use) {Game = 0, timer("first",1)}
    if(X > -10&X < -1&Y > 3&Y < 10&Use) {timer("prev",1)}
    if(X > 0&X < 9&Y > 3&Y < 10&Use) {timer("next",1)}
    if(X > -10&X < -1&Y < -13&Use) {timer("stop",1)}
    if(X > 0&X < 9&Y < -13&Use) {soundStop(0), timer("play",1)}
    
    
    if(clk("first")) {
        CS:writeCell(2041,1)
        CS:writeCell(2042,0)
        CS:writeString("-":repeat(29),0,0,9), CS:writeString("X",29,0,109100)
        CS:writeString("            MUSIC             ",0,1,9)
        CS:writeString("-":repeat(30),0,2,9)

        CS:writeString("      ",8,4,109100)
        CS:writeString(" Prev ",8,5,109100)
        CS:writeString("      ",8,6,109100)
        
        CS:writeString("      ",16,4,109100)
        CS:writeString(" Next ",16,5,109100)
        CS:writeString("      ",16,6,109100)

        CS:writeString("Song: "+Index,11,8,990100)
        
        CS:writeString(Title,floor((30-Title:length())/2),10,900100)

        CS:writeString("      ",8,15,109100)
        CS:writeString(" Stop ",8,16,109100)
        CS:writeString("      ",8,17,109100)
        
        CS:writeString("      ",16,15,109100)
        CS:writeString(" Play ",16,16,109100)
        CS:writeString("      ",16,17,109100)
    }

    if(clk("next")&!Use) {
        CS:writeCell(2039,8)
        CS:writeCell(2039,10)
        Index++
        if(Index > 32) {Index = 1}
        if(Index < 1) {Index = 32}
        Title = TitleA:string(Index)
        Song = SongA:string(Index)
        CS:writeString("Song: "+Index,11,8,990100)
        CS:writeString(Title,floor((30-Title:length())/2),10,900100)
    }
    
    if(clk("prev")&!Use) {
        CS:writeCell(2039,8)
        CS:writeCell(2039,10)
        Index--
        if(Index > 32) {Index = 1}
        if(Index < 1) {Index = 32}
        Title = TitleA:string(Index)
        Song = SongA:string(Index)
        CS:writeString("Song: "+Index,11,8,990100)
        CS:writeString(Title,floor((30-Title:length())/2),10,900100)
    }
    
    if(clk("play")&!Use) {soundPlay(0,0,Song)}
    if(clk("stop")&!Use) {soundStop(0)}
}


if(clk("fail")) {
    Game = -2
    CS:writeCell(2041,1)
    CS:writeCell(2042,9)
    CS:writeString("YOU FAIL! ":repeat(54),0,0)
    timer("reset",10000)
}


if(Game ==  -1) {timer("fail",1000)}

if(first()) {
    TitleA:setString(1,"Particle Ghost")
    TitleA:setString(2,"Lab Practicum")
    TitleA:setString(3,"Dark Energy")
    TitleA:setString(4,"The Innsbruck Experiment")
    TitleA:setString(5,"Pulse Phase")
    TitleA:setString(6,"Ravenholm Reprise")
    TitleA:setString(7,"Hard Fought")
    TitleA:setString(8,"Broken Symmetry")
    TitleA:setString(9,"Not Supposed To Be Here")
    TitleA:setString(10,"Kaon")
    TitleA:setString(11,"LG Orbifold")
    TitleA:setString(12,"Suppression Field")
    TitleA:setString(13,"Nova Prospekt")
    TitleA:setString(14,"CP Violation")
    TitleA:setString(15,"Triage At Dawn")
    TitleA:setString(16,"Miscount Detected")
    TitleA:setString(17,"Our Resurrected Teleport")
    TitleA:setString(18,"Hunter Down")
    TitleA:setString(19,"Apprehension And Evasion")
    TitleA:setString(20,"Calabi-Yau Model")
    TitleA:setString(21,"Brane Scan")
    TitleA:setString(22,"Slow Light")
    TitleA:setString(23,"Probably Not A Problem")
    TitleA:setString(24,"Requiem For Ravenholm")
    TitleA:setString(25,"Entanglement")
    TitleA:setString(26,"Highway")
    TitleA:setString(27,"A Red Letter Day")
    TitleA:setString(28,"Sand Traps")
    TitleA:setString(29,"CP Violation (Remix)")
    TitleA:setString(30,"Trainstation Part 1")
    TitleA:setString(31,"Trainstation Part 2")
    TitleA:setString(32,"Radio")

    SongA:setString(1,"music/hl2_song1.mp3")
    SongA:setString(2,"music/hl2_song2.mp3")
    SongA:setString(3,"music/hl2_song3.mp3")
    SongA:setString(4,"music/hl2_song4.mp3")
    SongA:setString(5,"music/hl2_song6.mp3")
    SongA:setString(6,"music/hl2_song7.mp3")
    SongA:setString(7,"music/hl2_song12_long.mp3")
    SongA:setString(8,"music/hl2_song13.mp3")
    SongA:setString(9,"music/hl2_song14.mp3")
    SongA:setString(10,"music/hl2_song15.mp3")
    SongA:setString(11,"music/hl2_song16.mp3")
    SongA:setString(12,"music/hl2_song17.mp3")
    SongA:setString(13,"music/hl2_song19.mp3")
    SongA:setString(14,"music/hl2_song20_submix0.mp3")
    SongA:setString(15,"music/hl2_song23_suitsong3.mp3")
    SongA:setString(16,"music/hl2_song25_teleporter.mp3")
    SongA:setString(17,"music/hl2_song26.mp3")
    SongA:setString(18,"music/hl2_song28.mp3")
    SongA:setString(19,"music/hl2_song29.mp3")
    SongA:setString(20,"music/hl2_song30.mp3")
    SongA:setString(21,"music/hl2_song31.mp3")
    SongA:setString(22,"music/hl2_song32.mp3")
    SongA:setString(23,"music/hl2_song33.mp3")
    SongA:setString(24,"music/ravenholm_1.mp3")
    SongA:setString(25,"music/hl2_song0.mp3")
    SongA:setString(26,"music/hl2_song8.mp3")
    SongA:setString(27,"music/hl2_song10.mp3")
    SongA:setString(28,"music/hl2_song11.mp3")
    SongA:setString(29,"music/hl2_song20_submix4.mp3")
    SongA:setString(30,"music/hl2_song26_trainstation1.mp3")
    SongA:setString(31,"music/hl2_song27_trainstation2.mp3")
    SongA:setString(32,"music/radio1.mp3")
    Song = SongA:string(Index)
    Title = TitleA:string(Index)
}
