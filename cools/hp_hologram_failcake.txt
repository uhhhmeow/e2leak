@name Holo Hp and Ap Display
@persist Timer
interval(10)

Chip=entity()
Me=owner()
Hp=Me:health()
Ap=Me:armor()
D=Timer
Timer=Timer+1*5


if(first()&Hp!=0)
{
    hint("Brought to you by Zeroedge9 and [HS] Done Fear",7)
   
    holoCreate(16)
    holoCreate(17)
}
holoPos(16,Me:pos()+vec(0,0,120))
holoModel(16,"bloc")
holoScale(16,vec(1,1,2))
holoAng(16,ang(0,D,0))


holoPos(17,Me:pos()+vec(0,0,120))
holoModel(17,"bloc")
holoScale(17,vec(1,2,1))
holoAng(17,ang(0,D,0))
if(changed(Hp)){
if(Hp>=91)
{
    holoColor(16,vec(0,255,0),255)
    holoColor(17,vec(0,255,0),255)
}
if(Hp>=80&Hp<91)
{
    holoColor(16,vec(196,224,36),255)
    holoColor(17,vec(196,224,36),255)
}
if(Hp>=70&Hp<80)
{
    holoColor(16,vec(224,162,31),255)
    holoColor(17,vec(224,162,31),255)
}
if(Hp>=60&Hp<70)
{
    holoColor(16,vec(219,96,36),255)
    holoColor(17,vec(219,96,36),255)
}
if(Hp>=50&Hp<60)
{
    holoColor(16,vec(255,0,0),255)
    holoColor(17,vec(255,0,0),255)
}
if(Hp<=50)
{
    holoColor(16,vec(255,0,0),25)
    holoColor(17,vec(255,0,0),25)
}
if(Hp<=1)
{
    holoCreate(1)
    holoCreate(2)
    holoCreate(3)
    holoCreate(4)
    holoCreate(5)
    holoCreate(6)
    holoCreate(7)
    holoCreate(8)
    holoCreate(9)
    holoCreate(10)
    holoCreate(11)
    holoCreate(12)
    holoCreate(13)
    holoCreate(14)
    holoCreate(15)
}
elseif(Hp>1)
{
    holoDelete(1)
    holoDelete(2)
    holoDelete(3)
    holoDelete(4)
    holoDelete(5)
    holoDelete(6)
    holoDelete(7)
    holoDelete(8)
    holoDelete(9)
    holoDelete(10)
    holoDelete(11)
    holoDelete(12)
    holoDelete(13)
    holoDelete(14)
    holoDelete(15)
}
}

holoPos(1,owner():pos()+vec(70,0,60)  )
holoPos(2,owner():pos()+vec(55,0,50)  )
holoPos(3,owner():pos()+vec(55,0,70)  )
holoPos(4,owner():pos()+vec(40,0,70)  )
holoModel(1,"bloc")
holoModel(2,"bloc")
holoModel(3,"bloc")
holoModel(4,"bloc")
holoScale(1,vec(0.5,0.5,2))
holoScale(2,vec(2,0.5,0.5))
holoScale(3,vec(2,0.5,0.5))
holoScale(4,vec(0.5,0.5,4))

holoPos(5,owner():pos()+vec(25,0,65)  )
holoPos(6,owner():pos()+vec(10,0,50)  )
holoPos(7,owner():pos()+vec(10,0,65)  )
holoPos(8,owner():pos()+vec(10,0,80)  )
holoModel(5,"bloc")
holoModel(6,"bloc")
holoModel(7,"bloc")
holoModel(8,"bloc")
holoScale(5,vec(0.5,0.5,3))
holoScale(6,vec(2,0.5,0.5))
holoScale(7,vec(2,0.5,0.5))
holoScale(8,vec(2,0.5,0.5))


holoPos(9,owner():pos()+vec(-30,0,65)  )
holoPos(10,owner():pos()+vec(-25,0,65)  )
holoPos(11,owner():pos()+vec(-20,0,65)  )

holoModel(9,"bloc")
holoModel(10,"bloc")
holoModel(11,"bloc")

holoScale(9,vec(0.5,0.5,3))
holoScale(10,vec(1,0.5,0.5))
holoScale(11,vec(0.5,0.5,3))

holoAng(9,ang(15,0,0))
holoAng(11,ang(-15,0,0))
#d

holoPos(12,owner():pos()+vec(-80,0,70)  )
holoPos(13,owner():pos()+vec(-65,0,50)  )
holoPos(14,owner():pos()+vec(-65,0,70)  )
holoPos(15,owner():pos()+vec(-50,0,60)  )
holoModel(12,"bloc")
holoModel(13,"bloc")
holoModel(14,"bloc")
holoModel(15,"bloc")
holoScale(15,vec(0.5,0.5,2))
holoScale(13,vec(2,0.5,0.5))
holoScale(14,vec(2,0.5,0.5))
holoScale(12,vec(0.5,0.5,4))

holoColor(1,vec(0,0,0),255)
holoColor(2,vec(0,0,0),255)
holoColor(3,vec(0,0,0),255)
holoColor(4,vec(0,0,0),255)
holoColor(5,vec(0,0,0),255)
holoColor(6,vec(0,0,0),255)
holoColor(7,vec(0,0,0),255)
holoColor(8,vec(0,0,0),255)
holoColor(9,vec(0,0,0),255)
holoColor(10,vec(0,0,0),255)
holoColor(11,vec(0,0,0),255)
holoColor(12,vec(0,0,0),255)
holoColor(13,vec(0,0,0),255)
holoColor(14,vec(0,0,0),255)
holoColor(15,vec(0,0,0),255)

if(Ap!=0)
{
    holoCreate(18)
    holoCreate(19)
}
elseif(Ap==0)
{
    holoDelete(18)
    holoDelete(19)
}
holoModel(18,"bloc")
holoColor(18,vec(255,255,0))
holoPos(18,Me:pos()+vec(0,0,158))
holoScale(18,vec(2,1,2))
holoAng(18,ang(0,D,0))

holoModel(19,"prism")
holoColor(19,vec(255,255,0))
holoPos(19,Me:pos()+vec(0,0,140))
holoScale(19,vec(2,1,1))
holoAng(19,ang(0,D,180))
