@name Prisoner Container
@inputs [Floor Front]:entity Override Eject
@persist ID:string IsMaster Open
@autoupdate on

Global=gTable("Saucer")

if(first()) {
    if(!Global["PrisonerPod",number]) {
        Global["PrisonerPod",number]=1
        Global["PrisonerPods",table]=table()
    }
    
    if(!Global["Master",entity] | Global["Master",entity]==entity()) {
        Global["Master",entity]=entity()
        Global["PrisonerPods",table]=table()
        IsMaster=1
        print("Claimed master status.")
    }
    
    ID="Prisoner_"+Floor:id()
    Global["PrisonerPods",table][ID,table]=table("entity"=Floor,"id"=ID,"occupant"=noentity(),"open"=0)
    
    holoCreate(1)
    holoModel(1,"models/props_lab/hev_case.mdl")
    holoPos(1,Floor:toWorld(vec(-3,0,-10)))
    holoAng(1,Floor:angles())
    holoParent(1,Floor)
    
    function number imprison(Entity:entity) {
        print("I",Entity)
        if(!Entity){return 0}
        foreach(I,V:table=Global["PrisonerPods",table]) {
            print(I)
            if(!V["occupant",entity]:isValid()) {
                Global["PrisonerPods",table][I,table]["occupant",entity]=Entity
                Entity:setPos(V["entity",entity]:toWorld(vec(0,0,3)))
                Entity:applyForce(-Entity:vel())
                return 1
            }
        }
        return 0
    }
}

if(Front:getMaterial()=="sprites/heatwave") {
    Open=1
    Global["PrisonerPods",table][ID,table]["open",number]=1
}

Open=Global["PrisonerPods",table][ID,table]["open",number]

if(changed(Open)) {
    holoAnim(1,Open ? "open" : "close")
    Front:propNotSolid(Open)
    Floor:soundPlay(0,1.384,"doors/doormove2.wav")
}

if(~Override) {
    Global["PrisonerPods",table][ID,table]["open",number]=Override
}

local Size=Floor:aabbSize()/2
local Pos=Floor:pos()
local Min=Pos-Size
local Max=Pos+Size
local MinVec=minVec(Min,Max)
local MaxVec=maxVec(Min,Max)+vec(0,0,100)
findInBox(MinVec,MaxVec)
    findIncludeClass("player")
    findIncludeClass("prop_physics")
    findIncludeClass("ngii_c_printer")
    findIncludeClass("ngii_e_printer")
    findIncludeClass("ngii_a_printer")
Occupant=noentity()

foreach(I,V:entity=findToArray()) {
    if(V:isFrozen()) {continue}
    Occupant=V
    if(~Eject) {
        V:setPos(Global["Hull",entity]:toWorld(V:aabbSize()*vec(0,0,-1.5)))
        Global["Sick",table][V:id(),number]=curtime()+8
    }
}

Global["PrisonerPods",table][ID,table]["occupant",entity]=Occupant
interval(100)
