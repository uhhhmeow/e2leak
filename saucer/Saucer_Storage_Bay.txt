@name Saucer Storage Bay
@inputs [Light19 Light20 Light22 Light23]:entity
@model models/hunter/plates/plate1x1.mdl

if(first()) {
    Entity=entity()
    Owner=owner()
    Lights=array(Light19,Light20,Light22,Light23)
    local Index=0
    foreach(I,V:entity=Lights) {
        #if(!V) {continue}
        V:setMaterial("debug/debugdrawflat")
        noCollideAll(V,1)
        Index++
        lightCreate(Index,V:pos())
        lightColor(Index,vec(128))
        lightParent(Index,V)
    }
    Entity:setAlpha(0)
}
