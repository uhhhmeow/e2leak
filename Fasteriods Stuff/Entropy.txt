@name Snak.e
@inputs 
@outputs 
@persist Amt T Speed
@trigger 

interval(10)

if(first()){
Amt=100
Scale=0.25
Speed=5
Mdl="hq_icosphere"
holoCreate(0)
holoAlpha(0,0)
for(I=1,Amt)
{
holoCreate(I)   
holoModel(I,Mdl)
Colors=array(vec(255,0,0),vec(255,55,0),vec(255,100,0),vec(100,0,0),vec(255,0,0),vec(255,255,0),vec(255,200,0),vec(255,255,255))
holoColor(I,Colors:vector(round(random(0.5,8.5))))
holoScale(I,vec(1,1,1)*Scale)  
holoPos(I,entity():pos())

}}

   
T=T+Speed
X=sin(T)*1000
Y=cos(T)*1000
Z=sin(T*2)*25
holoPos(0,vec(X,Y,0)+entity():pos())
for(I=1,Amt)
{
holoPos(I,(holoEntity(I):pos()+randvec(vec(-Speed),vec(Speed)))+vec(0,0,0.5))
}
