@name Hud
@inputs 
@outputs 
@persist Spawned
@trigger
runOnTick(1)
if(!Spawned){
    holoCreate(1)
    holoModel(1,"hq_rcube_thin")
    holoColor(1,vec(50))
    holoScale(1,vec(0.7,0.8,0.5))
    Spawned=1
}
C=entity():isConstrainedTo()
D=C:driver()

if(D:isValid()){
holoPos(1, owner():attachmentPos("eyes"))
holoAng(1, owner():attachmentAng("eyes"))
holoParentAttachment(1, owner(), "eyes")
}
else{holoEntity(1):deparent()
    holoPos(1,entity():pos())
    holoAng(1,entity():angles())}
