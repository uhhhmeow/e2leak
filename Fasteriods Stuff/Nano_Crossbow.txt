@name Nano Crossbow
@persist Bolts:array Count Bullet:entity

interval(30)
if(first()) {
    Rangers = array()     
}
findByClass("crossbow_bolt")
findExcludePlayerProps("^")
findIncludePlayerProps(owner())
Bolts = findToArray()
Count = Bolts:count()

if(changed(Count) & !first() & $Count == 1){
    owner():soundPlay(1,0,"npc/combine_gunship/attack_start2.wav")
}



for(I=0, Count) {
rangerFilter(Bolts[I, entity])
rangerFilter(owner())
    
    if(changed(Count)) {
        Bullet=Bolts[Count, entity]
    Bolts[Count, entity]:setTrails(100, 1,1 , "trails/laser", vec(255,100,0), 255, 0, 0)
    Bolts[Count, entity]:applyForce(Bolts[Count, entity]:forward()*4000)   
    }
    
    if(Bolts[I,entity]:isValid()) { 
        Rangers[I, ranger] = rangerOffset(150,Bolts[I,entity]:pos(),Bolts[I,entity]:forward())

    }
    if(changed(Rangers[I, ranger]:hit())& Rangers[I, ranger]:hit()) {
Rangers[I, ranger]:entity():propDelete()
Rangers[I, ranger]:entity():propFreeze(0)
Rangers[I, ranger]:entity():removeAllConstraints()
        if(Rangers[I, ranger]:entity():isPlayer()) {
            hint("Crossbow hit: "+Rangers[I, ranger]:entity():name(), 10)   
        }

        
    }   
}
