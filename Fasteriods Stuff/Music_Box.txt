@name Music Box
@inputs 
@outputs A B C D E F G OCT
@persist Clock
@trigger

## Tempo  Settings ##
interval(500)
Clock=Clock+1
##  500 = 120 BPM  ##

## First Beat ##
if(Clock==1)
{
A=1
B=0
C=0
D=0
E=0
F=0
G=0
}

if(Clock==2)
{
A=0
B=1
C=0
D=0
E=0
F=0
G=0
}

if(Clock==3)
{
A=0
B=0
C=1
D=0
E=0
F=0
G=0
}

if(Clock==4)
{
A=0
B=0
C=0
D=1
E=0
F=0
G=0
OCT=0
}


if(Clock==5)
{
A=0
B=0
C=0
D=0
E=1
F=0
G=0
OCT=0
}


if(Clock==6)
{
A=0
B=0
C=0
D=0
E=0
F=1
G=0
OCT=0
}


if(Clock==7)
{
A=0
B=0
C=0
D=0
E=0
F=0
G=1
OCT=0
}

if(Clock==8)
{
A=0
B=0
C=0
D=0
E=0
F=0
G=0
OCT=1
}

# Repeat Sign / Last Note #
if(Clock==9)
{
Clock=1
A=1       
B=0                #  For your piece to  #               
C=0                #  successfully loop, #
D=0                #  the last note must #
E=0                #  must be identical  #
F=0                #  to the first one.  #
G=0
OCT=0
}



