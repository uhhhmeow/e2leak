@name Solid Holograms! E2 Extension 

#Spawner

#[
local function createSolidHologram(self, index, pos, scale, ang, color, model)
	if not pos   then pos   = self.entity:GetPos() end
	if not scale then scale = Vector(1,1,1) end
	if not ang   then ang   = self.entity:GetAngles() end
	
	local Holo = CheckIndex(self, index)
	if not Holo then 
		Holo = {}
		SetIndex(self, index, Holo)
	end
	
	model = "models/Holograms/"..(model or "cube")..".mdl"
	
	local prop 
	
	if validEntity(Holo.ent) then
		prop = Holo.ent
		prop:SetPos( pos )
		prop:SetAngles( ang )
		prop:SetModel( model )
	else
		prop = MakeHolo(self.player, pos, ang, model, {}, {})
		prop:Activate()
		prop:Spawn()
		prop:SetMoveType(MOVETYPE_NONE)	--default config
		PlayerAmount[self.player] = PlayerAmount[self.player]+1
		Holo.ent = prop
		Holo.e2owner = self
		
		Holo.ent:SetSolid(SOLID_VPHYSICS)--standable

/*remove the following 2 lines if you dont like the effect, or one of the 2 to get a nice combo.*/
	Holo.ent.PhysgunDisabled = true--no physgun
	Holo.ent:SetUnFreezable( true )--no unfreeze
/**/
	end
	
	if not validEntity(prop) then return nil end
	if color then prop:SetColor(color[1],color[2],color[3],255) end
	
	rescale(Holo, scale)
	
	return prop
end
]#

#E2 Function Code

#[
e2function entity holoCreatePhys(index)
	if BlockList[self.player:SteamID()] == true or CheckSpawnTimer( self ) == false then return end
	local Holo = CheckIndex(self, index)
	if not Holo and PlayerAmount[self.player] >= GetConVar("wire_holograms_max"):GetInt() then return end
	
	local ret = createSolidHologram(self, index)
	if validEntity(ret) then return ret end
end
#]
