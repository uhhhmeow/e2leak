@name Portal Device
@inputs 
@outputs 
@persist Portal:entity T P1:vector P2:vector One Two Kick
@trigger 
runOnTick(1)

if(Kick){
    Portal:ejectPod()
Kick=0}

if(first()){
    W=vec(255,255,255)
    P=vec(150,0,255)

printColor(P,"          --------[",W,"Fasteroid's Portal Device",P,"]--------")
printColor(W,"Left or right click with your crowbar to place a portal.")

printColor(W,"               Press E on a portal to teleport.")
    Portal=seatSpawn("models/hunter/misc/sphere025x025.mdl",1)
holoCreate(1)
holoCreate(2)
holoCreate(3)
holoCreate(4)
holoModel(1,"hq_icosphere")
holoModel(2,"hq_icosphere")
holoModel(3,"hq_icosphere")
holoModel(4,"hq_icosphere")
holoParent(2,1)
holoParent(4,3)
holoAlpha(1,200)
holoAlpha(2,100)
holoAlpha(3,200)
holoAlpha(4,100)
holoColor(1,vec(150,0,255))
holoColor(2,vec(150,0,255))
holoColor(3,vec(150,0,255))
holoColor(4,vec(150,0,255))
holoMaterial(2,"models/shiny")
holoMaterial(4,"models/shiny")
Portal:setAlpha(0)
}
#Portal Animation#
T=T+1
A=((sin(-T)/12)+0.5)*4
B=((sin(T)/8)+1)*4
holoScale(1,vec(A,A,A))
holoScale(2,vec(B,B,B))
holoScale(3,vec(A,A,A))
holoScale(4,vec(B,B,B))

if(owner():weapon():type()=="weapon_crowbar"){

if(owner():keyAttack1()){
    if(!One){
    One=1  
    P1=owner():aimPos()
    holoPos(1,P1)
    owner():soundPlay(1,5,"ambient/water/drip4.wav")
}}
else{
One=0}
    
    if(owner():keyAttack2()){
    if(!Two){
    Two=1  
    P2=owner():aimPos()
    holoPos(3,P2)
    owner():soundPlay(1,5,"ambient/water/drip3.wav")
}}
else{
Two=0}
    
    }

if(owner():pos():distance(P1)<200){

if(Portal:driver()==owner()){
    Portal:setPos(P2)

Kick=1}
else{
    Portal:setPos(P1)
}
}

if(owner():pos():distance(P2)<200){

if(Portal:driver()==owner()){
    Portal:setPos(P1)

Kick=1}
else{
    Portal:setPos(P2)
}
}
