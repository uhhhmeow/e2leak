@name Natural Selection Targeting
@inputs 
@outputs Out:string
@persist Best:vector Dist Prop:entity New:vector Out:string Gen
@trigger 

runOnTick(1)

if(first()){
    Dist=99999
timer("clk",5000)
holoCreate(1)
holoModel(1,"cplane")
holoPos(1,entity():pos()+entity():forward()*700+vec(0,0,0))
holoScale(1,vec(9))
holoColor(1,vec(255,0,0))

holoCreate(2)
holoModel(2,"cplane")
holoPos(2,entity():pos()+entity():forward()*700+vec(0,0,1))
holoScale(2,vec(6))


holoCreate(3)
holoModel(3,"cplane")
holoPos(3,entity():pos()+entity():forward()*700+vec(0,0,2))
holoScale(3,vec(3))
holoColor(3,vec(255,0,0))

holoCreate(4)
holoModel(4,"models/props_phx/construct/metal_angle360.mdl")
holoPos(4,entity():pos()+entity():forward()*700+vec(0,0,2))
holoScale(4,vec(10,10,0.1))
holoColor(4,vec(0,0,0),55)
holoMaterial(4,"models/debug/debugwhite")
holoParent(1,entity())
holoParent(2,entity())
holoParent(3,entity())


}
if(clk("clk")){
timer("clk",3000)
Clk=1}
else{Clk=0}

if(Clk){
    Distance=Prop:pos():distance(entity():pos()+entity():forward()*700)
    if(Distance<Dist){
        Out="Closest Approach: "+round(Distance)+"                   Velocity: "+New+"                                      Generation: "+Gen
        Dist=Distance
        holoScale(4,vec(Dist/(95/2),Dist/(95/2),0.1))
        Best=New
       Gen=Gen+1}
    Prop:propDelete()
    Prop=propSpawn("models/hunter/blocks/cube025x025x025.mdl",0)
    Prop:setMass(10)
    Prop:setMaterial("models/debug/debugwhite")
    Prop:setColor(vec(255,0,0))
    New=Best+vec(randint(10,-10),randint(10,-10),randint(10,-10))
    Prop:applyForce(New*100)

    }
    
    

Distance2=Prop:pos():distance(entity():pos()+entity():forward()*700)
    if(Distance2<Dist){
    Prop:setColor(0,255,0)}
    else{Prop:setColor(vec(255,0,0))}
