@name Complex Vehicle 
@inputs Chair:entity Ent1:entity Ent2:entity
@outputs
#Main Persists 
@persist Turn T Turn2 Ranger:ranger
#Hologram Persists
@persist Flip
@trigger 


runOnTick(1)

K=Chair:driver()
if(changed(K:isValid())&K:isValid()){
            W=vec(255,255,255)
        P=vec(0,255,0)

Chair:printColorDriver(P,"           --------[",W,"Fasteroid's GyroSphere v.1.0",P,"]--------")
Chair:printColorDriver(W,"  Fun to Drive, 100% Prop Push Proof, Easy to maneuver!")}

Ent2:noCollideAll(0)
Ent1:noCollideAll(0)

if(first()|dupefinished()){


    soundPlay(1,9001,"physics/metal/paintcan_roll_loop1.wav")
    
     soundPlay(2,9001,"ambient/machines/spin_loop.wav")
    soundPlay(3,9001,"ambient/machines/machine6.wav")
    soundVolume(2,0.25)
    entity():propFreeze(0)
    Turn=0
    Ent2:setAng(ang(0,0,0))
    holoCreate(1,Ent1:pos())
    holoModel(1,Ent1:model())
    holoScale(1,vec(1.01,1.01,0.5))
    holoPos(1,Ent1:pos())
    holoAng(1,Ent1:angles())
    holoMaterial(1,"models/ihvtest/eyeball_l")
    holoParent(1,Ent1)
    
    holoCreate(2,Ent2:pos())
    holoModel(2,"models/sprops/geometry/sphere_84.mdl")
    holoScale(2,vec(1.1))
    holoAng(2,Ent2:angles())
    holoMaterial(2,"debug/env_cubemap_model")
    holoParent(2,Ent1)
    holoShadow(2,1)
    holoAlpha(2,0)
    
    holoCreate(3,Ent2:pos()+vec(0,0,0))
    holoModel(3,"models/props_phx/construct/metal_dome360.mdl")
    holoScale(3,vec(0.99))
    holoAng(3,ang(0,0,0))
    holoMaterial(3,"debug/env_cubemap_model")
    holoParent(3,2)
    holoShadow(3,1)
    holoAlpha(3,50)
    
    holoCreate(4,Ent2:pos()-vec(0,0,0))
    holoModel(4,"models/props_phx/construct/metal_dome360.mdl")
    holoScale(4,vec(0.99))
    holoAng(4,ang(0,0,180))
    holoMaterial(4,"debug/env_cubemap_model")
    holoParent(4,2)
    holoShadow(4,1)
    holoAlpha(4,50)
    
      holoCreate(5,Ent2:pos())
    holoModel(5,"models/props_phx/construct/metal_dome360.mdl")
    holoScale(5,vec(0.9))
    holoAng(5,ang(0,0,180))
    holoMaterial(5,"models/debug/debugwhite")
    holoParent(5,Ent1)
    holoShadow(5,1)
    holoColor(5,vec(63))
    holoAlpha(5,255)



holoCreate(7,Ent2:pos()-vec(0,0,0))
    holoModel(7,"models/props_phx/construct/metal_dome360.mdl")
    holoScale(7,vec(1))
    holoAng(7,ang(0,0,180))
    holoMaterial(7,"debug/env_cubemap_model")
    holoParent(7,Ent1)
    holoShadow(7,1)
    holoAlpha(7,50)
    
    holoCreate(8,Ent2:pos()-vec(0,0,0))
    holoModel(8,"models/props_phx/construct/metal_dome360.mdl")
    holoScale(8,vec(1))
    holoAng(8,ang(0,0,360))
    holoMaterial(8,"debug/env_cubemap_model")
    holoParent(8,Ent1)
    holoShadow(8,1)
    holoAlpha(8,50)



    Ent2:propDraw(1)
    Ent2:setAlpha(0)
}

if(Chair:driver():isValid()){
Ent2:propFreeze(0)



FakePos2=vec(Ent2:pos():x(),Ent2:pos():y(),0)
FakeVel2=vec(Ent2:vel():x(),Ent2:vel():y(),0)
if(K:keyForward()){
Ent2:applyForce(Ent1:right()*-20*Ent2:mass()) 
}

if(K:keyBack()){
Ent2:applyForce(Ent1:right()*20*Ent2:mass()) 
}





if(K:keyLeft()){
     if(Turn2<10){
        Turn2=Turn2+0.5
    }
}

if(K:keyRight()){
    if(Turn2>-10){
        Turn2=Turn2-0.5
    }
}

if(!K:keyRight()&!K:keyLeft()){
    if(Turn2!=0){
        
        if(Turn2>-1.01&Turn2<0|Turn2<1.01&Turn2>0){
        #Ignore the line below if the movement isn't 'smooth' enough
        #Be careful as this can cause infinite spinning!
        Turn2=0
        }
        
        if(Turn2<0){
            Turn2=Turn2+1.5
        }
        if(Turn2>0){
            Turn2=Turn2-1.5
        }
        
    }
    
}
    Turn=Turn+Turn2

}

if(Ent1:isValid()&Ent2:isValid()){
Ent1:setPos(Ent2:pos()+vec(0,0,0))
Ent1:setAng(ang(0,Turn,0))
}
T=T+5
    G=(sin(T/2)+1)
    H=(sin(-T/2)+1)
    holoColor(1,vec(0,255*G,0))

holoAng(7,Ent2:toWorld(ang(0,0,0)))
holoAng(8,Ent2:toWorld(ang(0,0,180)))
Derp=players()
foreach(Derp,K:entity=Derp)
{
    if(K:pos():distance(Ent1:pos())<100){
    Yeet=1
}
}

    if(!K:isValid()&Yeet){
        
    if(Flip!=180){
Flip=Flip+5}}

else{
if(Flip!=0){
    Flip=Flip-5}
    }
    
    if(Flip>175){
        Ent2:propNotSolid(1)
        Ent2:propFreeze(1)
        }
        else{Ent2:propNotSolid(0)
    }
        
        holoAng(3,Ent1:toWorld(ang(0,0,Flip)))
        
        
        if(K:isValid()){
            if(Flip==0){
               
            holoVisible(3,players(),0)
            holoVisible(4,players(),0)
            holoVisible(8,players(),1)
            holoVisible(7,players(),1)
    }}
        else
        {
            holoVisible(3,players(),1)
            holoVisible(4,players(),1)
            holoVisible(8,players(),0)
            holoVisible(7,players(),0)
        }
FakePos=vec(Ent2:pos():x(),Ent2:pos():y(),0)
FakeVel=vec(Ent2:vel():x(),Ent2:vel():y(),0)

Speed=FakePos:distance(FakePos+FakeVel)
Speed=Speed*0.001
if(Speed>1){
Speed=1}

 Ranger = rangerOffset(30,Ent1:pos()-vec(0,0,50),Ent1:pos()- vec(0,0,65))
rangerHitEntities(0)
rangerDefaultZero(1)
if(Ranger:hit()){
soundPitch(1,(Speed*125))
soundVolume(1,0.5)}
else{soundVolume(1,0)}


if(K:isValid()){
soundPitch(2,(abs(Turn2)/10)*255)
soundPitch(3,((Speed/4+1)*100))
}
else{
soundPitch(2,0)
soundPitch(3,100)
}

soundVolume(3,5)


if(K:keyAttack1()){
Ent2:applyForce((Chair:forward()+Chair:up())*4000)}


#[
if(changed(changed(Flip))&changed(Flip)&Flip!=180&Flip!=0){
soundPlay(3,9001,"doors/heavy_metal_move1.wav")}
if(!changed(Flip)){
soundStop(3)}
#]
