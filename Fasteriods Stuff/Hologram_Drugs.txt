@name Hologram Drugs
@inputs 
@outputs 
@persist 
@trigger 
@model models/props_junk/watermelon01.mdl

interval(10)
if(first()|dupefinished()){
holoCreate(1)
holoMaterial(1,"models/props_combine/stasisfield_beam")
holoModel(1,"hq_icosphere")
holoScale(1,vec(-6,-6,-6))
holoCreate(2)
holoMaterial(2,"models/shadertest/predator")
holoModel(2,"hq_icosphere")
holoScale(2,vec(-5,-5,-5))
R=random(0,360)
holoAng(1,ang(R,R,R))
holoParent(2,1)
holoVisible(1,players(),0)
holoVisible(2,players(),0)
holoVisible(1,owner(),1)
holoVisible(2,owner(),1)
}
holoPos(1,owner():pos()+vec(0,0,70)+(owner():vel()/5))
Colors=array(vec(255,0,0),vec(0,255,0),vec(0,0,255),vec(255,255,0),vec(255,0,255),vec(0,255,255),vec(0,0,0),vec(255,255,255))
E=Colors:vector(round(random(0.5,8.5)))
holoColor(1,E)

