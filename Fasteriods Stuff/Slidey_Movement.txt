@name Basic Vehicle
@inputs Chair:entity Ent1:entity Ent2:entity
@outputs 
@persist Turn T Turn2
@trigger 
runOnTick(1)

if(first()|dupefinished()){
    Turn=0
    Ent2:setAng(ang(0,0,0))
    holoCreate(1,Ent1:pos())
    holoModel(1,Ent1:model())
    holoScale(1,vec(1.01,1.01,0.5))
    holoPos(1,Ent1:pos())
    holoAng(1,Ent1:angles())
    holoMaterial(1,"models/ihvtest/eyeball_l")
    holoParent(1,Ent1)
    holoCreate(2,Ent2:pos())
    holoModel(2,"models/sprops/geometry/sphere_84.mdl")
    holoScale(2,vec(1.1))
    holoAng(2,Ent2:angles())
    holoMaterial(2,"debug/env_cubemap_model")
    holoParent(2,Ent1)
    holoShadow(2,1)
    holoAlpha(2,70)
    Ent2:propDraw(1)
    Ent2:setAlpha(0)
}

if(Chair:driver():isValid()){
K=Chair:driver()

if(K:keyForward()){
Ent2:applyForce(Ent1:right()*-10*Ent2:mass()) 
}

if(K:keyBack()){
Ent2:applyForce(Ent1:right()*10*Ent2:mass()) 
}
if(K:keyLeft()){
     if(Turn2<10){
        Turn2=Turn2+0.5
    }
}

if(K:keyRight()){
    if(Turn2>-10){
        Turn2=Turn2-0.5
    }
}

if(!K:keyRight()&!K:keyLeft()){
    if(Turn2!=0){
        
        if(Turn2<0){
            Turn2=Turn2+0.5
        }
        if(Turn2>0){
            Turn2=Turn2-0.5
        }
        
    }
    
}
    Turn=Turn+Turn2

}

if(Ent1:isValid()&Ent2:isValid()){
Ent1:setPos(Ent2:pos()+vec(0,0,0))
Ent1:setAng(ang(0,Turn,0))
}
T=T+5
    G=(sin(T/2)+1)
    H=(sin(-T/2)+1)
    holoColor(1,vec(0,255*G,0))

Ent2:noCollideAll(0)
    holoAng(2,Ent2:angles())


