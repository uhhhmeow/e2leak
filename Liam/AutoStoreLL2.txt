
@name GanStore Store
@inputs [E U1 U2 U3 U4 U5]:wirelink User:entity
@persist [FunDir Ball]:vector2 TempPlayer:entity Reset Index IndexS [WepNames WepPrices WepHolo WepRemaining WepUsers]:array
@trigger
 
interval(10)
 
if(first()) {
    function number egpClick(Player:entity,ID){
        return inrange(E:egpCursor(Player),E:egpPos(ID)-(E:egpSize(ID)/2),E:egpPos(ID)+(E:egpSize(ID)/2))
    }
   
    function entity entity:shipmentInStock(){
        RD=rangerOffset(100,This:pos()+This:up()*5,This:up())
        if(RD:entity():type():matchFirst("shipment")){return RD:entity()}else{return noentity()}
    }
    WepUsers=array(noentity(),noentity(),noentity(),noentity(),noentity())
}
if(changed(E:entity())&E:entity()){
    Index = 1,IndexS=1,timer("Search weapons",1000)
}
if(clk("Search weapons")){
    for(A=1,5){
        if(WepUsers[A,entity]:shipmentInStock()){
            if(WepRemaining[A,number]<=0){
                local Shipment = WepUsers[A,entity]:shipmentInStock()
                local Remaining = Shipment:shipmentAmount()
                WepNames[A,string]=Shipment:shipmentName()
                WepPrices[A,number]=int(Shipment:shipmentPrice()/Remaining)
                WepHolo[A,string]=""#we need a table holding matching holos to the wep names
                WepRemaining[A,number]=Remaining
            }
        }else{
            WepRemaining[A,number]=0
        }
           
    }
    timer("Search weapons",4000)
}
if(changed(U1:entity())&U1:entity()){WepUsers[1,entity]=U1:entity()}
if(changed(U2:entity())&U2:entity()){WepUsers[2,entity]=U2:entity()}
if(changed(U3:entity())&U3:entity()){WepUsers[3,entity]=U3:entity()}
if(changed(U4:entity())&U4:entity()){WepUsers[4,entity]=U4:entity()}
if(changed(U5:entity())&U5:entity()){WepUsers[5,entity]=U5:entity()}
   
if(clk("Stop Fire")){
    U1["Fire",number]=0
    U2["Fire",number]=0
    U3["Fire",number]=0
    U4["Fire",number]=0
    U5["Fire",number]=0
    Index=2,E:egpRemove(20),timer("Update",50)
}
if(Index == 1){
    E:egpClear()
    #Background
    E:egpRoundedBox(1,vec2(250,250),vec2(550,550)),E:egpColor(1,vec(0,0,255)),E:egpMaterial(1,"gui/gradient")
    #Background
   
    #Title Outline
    E:egpRoundedBoxOutline(2,vec2(250,80),vec2(350,150)),E:egpColor(2,vec(255,255,255))
    #Title Outline
   
    #TitleBox
    E:egpRoundedBox(3,vec2(250,80),vec2(348,148)),E:egpColor(3,vec(97,97,97))
    #TitleBox
   
    #Title Text
    E:egpText(4,"Lucky & Liam's Store",vec2(90,50)),E:egpSize(4,35)
    #Title Text
   
   
    #LoginOutline
    E:egpRoundedBoxOutline(5,vec2(250,350),vec2(350,150)),E:egpColor(5,vec(255,255,255))
    #Login Outline
   
    #Login (BUTTON)
    E:egpRoundedBox(6,vec2(250,350),vec2(348,148)),E:egpColor(6,vec(97,97,97))
    E:egpText(7,"Press E (Use) To Login!",vec2(80,345)),E:egpSize(7,35)
    #
   
    #Your Moving Gmod Logo Code
    E:egpBox(8,vec2(256,256),vec2(80,80))
    E:egpMaterial(8,"gui/gmod_logo")
    E:egpText(9,"GunStore",vec2(0,500)),E:egpSize(9,10)
    E:egpText(10,"E2 Demo",vec2(390,500)),E:egpSize(10,10)
    Index = 1.1
    FunDir=vec2(random(-8,8),random(-8,8))
    Ball = vec2(256,256)
    interval(100)
}elseif(Index==1.1){
    #Your Moving Gmod Logo Code
    interval(100)
    Ball=Ball+FunDir*4
    E:egpPos(8,Ball)
    if(!inrange(Ball,vec2(70,140),vec2(440,440))){
        if(Ball:x()<=70&FunDir:x()<0){
            FunDir=FunDir:setX(clamp(-FunDir:x()+random(-1,1),-5,5))
        }elseif(Ball:x()>=440&FunDir:x()>0){
            FunDir=FunDir:setX(clamp(-FunDir:x()+random(-1,1),-5,5))
        }elseif(Ball:y()<=140&FunDir:y()<0){
            FunDir=FunDir:setY(clamp(-FunDir:y()+random(-1,1),-5,5))
        }elseif(Ball:y()>=440&FunDir:y()>0){
            FunDir=FunDir:setY(clamp(-FunDir:y()+random(-1,1),-5,5))
        }
    }
   
    if(~User&User){
        if(egpClick(User,6)){
            E:egpClear()
            #Background
            E:egpRoundedBox(9,vec2(250,250),vec2(550,550)),E:egpColor(9,vec(0,0,255)),E:egpMaterial(9,"gui/gradient")
            #Welcome Text Box
            E:egpRoundedBox(10,vec2(250,80),vec2(450,150)),E:egpColor(10,vec(47,47,47))
            E:egpTextLayout(11,"Welcome, "+User:name()+"!",vec2(30,20),vec2(420,150)),E:egpSize(11,40)
            #Main HUD
            E:egpRoundedBox(12,vec2(250,320),vec2(450,320)),E:egpColor(12,vec(27,27,27))
            #Next Button
            E:egpBox(17,vec2(425,385),vec2(75,75)),E:egpMaterial(17,"gui/spawnmenu_toggle")
            #Previous Button
            E:egpBox(18,vec2(75,385),vec2(75,75)),E:egpMaterial(18,"gui/spawnmenu_toggle_back")
            IndexS=1
            if(WepUsers[IndexS,entity]:shipmentInStock()){
                #Purchase Box
                E:egpRoundedBox(13,vec2(350,250),vec2(150,150)),E:egpColor(13,vec(0,255,0))
                E:egpText(14,"Purchase",vec2(275,285)),E:egpSize(14,40),E:egpAngle(14,45)
                #Amount Left
                E:egpText(15,"Remaining: "+WepRemaining[IndexS,number],vec2(40,190))
                #Weapon Name
                E:egpText(16,WepNames[IndexS,string],vec2(165,445)),E:egpSize(16,30)
                #Price
                E:egpText(19,"Price: $"+WepPrices[IndexS,number],vec2(40,210))
            }else{
                E:egpText(16,"Out Of Stock",vec2(165,445)),E:egpSize(16,30)
            }
           
            #Weapon slot indicators 1-5
            E:egpBox(21,vec2(156,500),vec2(15,15)),E:egpColor(21,vec(0,55,200))
            E:egpBox(22,vec2(206,500),vec2(15,15)),E:egpColor(22,vec(0,0,90))
            E:egpBox(23,vec2(256,500),vec2(15,15)),E:egpColor(23,vec(0,0,90))
            E:egpBox(24,vec2(306,500),vec2(15,15)),E:egpColor(24,vec(0,0,90))
            E:egpBox(25,vec2(356,500),vec2(15,15)),E:egpColor(25,vec(0,0,90))
            Index = 2,TempPlayer=User,timer("AFK=?",500)
        }
    }
}elseif(Index == 2) {
    if(clk("AFK=?")){
        if(TempPlayer:pos():distance(E:entity():pos())>80){Index=1}else{timer("AFK=?",5000)}
    }
   
    if(~User&User){
        if(egpClick(User,17)){
            if(IndexS<5){IndexS++}else{IndexS=1}
        }elseif(egpClick(User,18)){
            if(IndexS>1){IndexS--}else{IndexS=5}
        }elseif(egpClick(User,13)){
            if(WepUsers[IndexS,entity]:shipmentInStock()){
                moneyRequest(User,WepPrices[IndexS,number],15,"Purchase of "+WepNames[IndexS,string]+" $"+WepPrices[IndexS,number])
            }
        }
    }
    if(moneyClk()){
        if(IndexS==1){U1["Fire",number]=1}
        elseif(IndexS==2){U2["Fire",number]=1}
        elseif(IndexS==3){U3["Fire",number]=1}
        elseif(IndexS==4){U4["Fire",number]=1}
        elseif(IndexS==5){U5["Fire",number]=1}
        soundPlay(1,2,"buttons/weapon_confirm.wav")
        printColor(vec(150,150,150),"GunShop: ",vec(200,200,255),moneyClkPlayer():name(),vec(150,150,150)," purchased a ",vec(55,55,255),WepNames[IndexS,string],vec(255,255,0)," ($"+WepPrices[IndexS,number]+")")
        timer("Stop Fire",1500)
        E:egpBox(20,vec2(256,256),vec2(512,512)),E:egpColor(20,vec()),E:egpAlpha(20,200)
        Index=4
    }
    if(moneyNoClk()){
        soundPlay(1,2,"buttons/weapon_cant_buy.wav")
    }
   
    if(Index == 4){E:egpClear(),E:egpBox(26,vec2(250,250),vec2(250,250)),E:egpMaterial(26,"VGUI/loading-rotate"),timer("back to main",2000)}
    E:egpAngle(26,curtime()*45)
    if(clk("back to main")){reset()}
       
    if(changed(IndexS)|clk("Update")){
        if(WepUsers[IndexS,entity]:shipmentInStock()){
            WepRemaining[IndexS,number]=WepUsers[IndexS,entity]:shipmentInStock():shipmentAmount()
            #Purchase Box
            E:egpRoundedBox(13,vec2(350,250),vec2(150,150)),E:egpColor(13,vec(0,255,0))
            E:egpText(14,"Purchase",vec2(275,285)),E:egpSize(14,40),E:egpAngle(14,45)
            #Amount Left
            E:egpText(15,"Remaining: "+WepRemaining[IndexS,number],vec2(40,190))
            #Weapon Name
            E:egpText(16,WepNames[IndexS,string],vec2(165,445)),E:egpSize(16,30)
            #Price
            E:egpText(19,"Price: $"+WepPrices[IndexS,number],vec2(40,210))
        }else{
            E:egpRemove(13),E:egpRemove(14),E:egpRemove(15),E:egpRemove(16),E:egpRemove(19)
            E:egpText(16,"Out Of Stock",vec2(165,445)),E:egpSize(16,30)
        }
        if(IndexS==1){E:egpColor(21,vec(0,55,200))}else{E:egpColor(21,vec(0,0,90))}
        if(IndexS==2){E:egpColor(22,vec(0,55,200))}else{E:egpColor(22,vec(0,0,90))}
        if(IndexS==3){E:egpColor(23,vec(0,55,200))}else{E:egpColor(23,vec(0,0,90))}
        if(IndexS==4){E:egpColor(24,vec(0,55,200))}else{E:egpColor(24,vec(0,0,90))}
        if(IndexS==5){E:egpColor(25,vec(0,55,200))}else{E:egpColor(25,vec(0,0,90))}
    }
}
