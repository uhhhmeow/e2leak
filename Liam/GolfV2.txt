@name minigame_golf_V2
@inputs
@outputs
@persist [Kraft Plys Col]:array Ziel:vector Index Dran Schla
if(first()){
    Plys[1,entity]=owner()
    Col[1,vector]=randvec(100,255)
    local Ent=holoCreate(1,entity():pos()+vec(0,0,5),vec(0.3,0.3,0.3),ang(),Col[1,vector])
    Ent:setTrails(3,0,10,"trails/smoke",Col[1,vector],255)
    holoModel(1,"hq_sphere")
    Dran=1
    Index=0
    runOnChat(1)
    holoCreate(5,entity():pos()+vec(0,5,2),vec(1,0.2,0.5)),holoModel(5,"hq_rcube")
    holoCreate(6,entity():pos()+vec(4,5,22),vec(0.3,0.3,3)),holoModel(6,"hq_cylinder"),holoParent(6,5)
    holoCreate(7,entity():pos(),vec(0.7,0.7,0.2),ang(),vec())
    holoModel(7,"hqicosphere2")
    holoCreate(8,entity():pos()+vec(4,0,20),vec(0.1,0.1,4),ang(),vec(150,150,0))
    holoModel(8,"hqcylinder"),holoParent(8,7)
    holoCreate(9,entity():pos()+vec(-2,0,38),vec(1,0.1,1),ang(90,180,0),vec(155,0,0))
    holoModel(9,"hqcone"),holoParent(9,7)
    print("Type -add name to add a player to play with")
    print("Hit E to set the goal")
}
interval(40)
if(Index==0){
    Ziel=owner():aimPos()
    holoPos(7,Ziel)
    if(owner():keyUse()){
        Index=1
        runOnChat(0)
    }
    if(chatClk(owner())){
        local Ex=owner():lastSaid():lower():explode(" ")
        if(Ex[1,string]=="-add"){
            hideChat(1)
            if(findPlayerByName(Ex[2,string])){
                local C=Plys:count()+1
                Plys[C,entity]=findPlayerByName(Ex[2,string])
                Col[C,vector]=randvec(100,255)
                local Ent=holoCreate(C,entity():pos()+vec(0,0,5),vec(0.3,0.3,0.3),ang(),Col[C,vector])
                Ent:setTrails(3,0,10,"trails/smoke",Col[C,vector],255)
                holoModel(C,"hq_sphere")
            }
        }
    }
}elseif(Index==1){
    holoPos(5,holoEntity(Dran):pos()+(Plys[Dran,entity]:pos()-holoEntity(Dran):pos()):normalized()*10-Plys[Dran,entity]:forward()*Kraft[Dran,number])
    holoAng(5,ang(0,90+Plys[Dran,entity]:eyeAngles():yaw(),Kraft[Dran,number]))
    if(Plys[Dran,entity]:keyReload()){
        timer("Wechsel",100)
    }elseif(Plys[Dran,entity]:keyAttack1()){
        holoAng(Dran,ang(-Kraft[Dran,number]*Schlag*2,Plys[Dran,entity]:eyeAngles():yaw(),0))
        if(Kraft[Dran,number]<20){Kraft[Dran,number]=Kraft[Dran,number]+0.5}
    }elseif(Kraft[Dran,number]>1){
        Index=2
        holoPos(5,holoEntity(Dran):pos()+(Plys[Dran,entity]:pos()-holoEntity(Dran):pos()):normalized()*10)
        holoAng(5,ang(0,90+Plys[Dran,entity]:eyeAngles():yaw(),0))
    }
    if(clk("Wechsel")){
        Schlag= !Schlag
        if(Schlag){
            holoScale(5,vec(1,0.4,0.5)),holoModel(5,"hq_sphere")
        }else{
            holoScale(5,vec(1,0.2,0.5)),holoModel(5,"hq_rcube")
        }
    }
}elseif(Index==2){
    if(Kraft[Dran,number]>0.2){
        local Ent=holoEntity(Dran)
        if(Ent:pos():distance(Ziel)<8){
            Index=3
            print(Plys[Dran,entity]:name()+" wins the golf match")
        }
        if(Ent:forward():z()>-0.99){
            local R2=rangerOffset(7,Ent:pos(),vec(0,0,-1))
            if(!R2:hit()){
                holoAng(Dran,Ent:angles():rotateAroundAxis(Ent:right(),Kraft[Dran,number]/5-4.5))
            }
        }
        if(Ent:forward():z()<-0.4){
            Kraft[Dran,number]=Kraft[Dran,number]+0.2
        }
        rangerFilter(Plys)
        Rang=rangerOffset(Kraft[Dran,number]*1.6,Ent:pos(),Ent:forward())
        if(Rang:hit()){
            #Bounce off objects, by LuckyGuy
            holoAng(Dran,(Ent:forward()+(Rang:hitNormal()-Ent:forward())/2):toAngle())
            Kraft[Dran,number]=Kraft[Dran,number]/1.2
        }
        holoPos(Dran,Ent:pos()+Ent:forward()*Kraft[Dran,number])
        Kraft[Dran,number]=Kraft[Dran,number]-0.1
    }else{
        Index=1
        if(Dran<Plys:count()){Dran++}else{Dran=1}
    }
}