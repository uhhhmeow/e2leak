@name minigame_TownBattle_Server
@inputs Egp:wirelink
@outputs Game:table
@persist GeldUhr GeldMulti NoobProtectionTime Time WarGoingOn Con1 WarStep
@persist [BauZeit BauKosten SpawnPositions WorldLog Used_Unit_Index UKeys]:array
@persist [TeamNames Used_TownNames Units NameToSteamID Dmg]:table UnitCounter
@trigger 

if(first()){
    Egp:egpClear()
    GeldMulti=50#Multiplys money by export lvl
    GeldUhr=20#In Seconds
    NoobProtectionTime=8#In Minutes
    BauZeit=array(20,20,20,15,600,300,40,30)
    BauKosten=array(300,1000,100,12,1500,650,150,120)
    Dmg["7",array]=array(0.1,20,3)
    Dmg["9",array]=array(0.1,0.33,0.09091)
    Dmg["11",array]=array(3,10,1)
	#Dmg array mit kehrwerten?
    TeamNames["[255,255,255]",string]="Independent"
    TeamNames["[255,0,0]",string]="Red"
    TeamNames["[0,255,0]",string]="Green"
    TeamNames["[0,0,255]",string]="Blue"
    TeamNames["[255,255,0]",string]="Yellow"
    TeamNames["[255,0,255]",string]="Noob Protected"
    SpawnPositions=array(
        vec2(62.110297309028,14.034939236111),
        vec2(437.60942925347,275.06110975477),
        vec2(175.66417100694,15.595998128255),
        vec2(291.74761284722,18.91995578342),
        vec2(408.85780164931,22.842820909288),
        vec2(351.62174479167,104.13351101345),
        vec2(226.55392795139,112.35520087348),
        vec2(112.84543185764,104.3038160536),
        vec2(294.80750868056,192.67789035373),
        vec2(455.53206380208,132.8428378635),
        vec2(175.66417100694,192.81386311849),
        vec2(59.457302517361,187.57942030165),
        vec2(96.741644965278,264.91910129123),
        vec2(223.67578125,278.59843614366),
        vec2(332.29530164931,294.78207736545),
        vec2(405.38015407986,361.49655490451),
        vec2(332.29530164931,435.33451334635),
        vec2(238.13297526042,376.66497802734),
        vec2(134.47493489583,347.96268717448),
        vec2(56.804307725694,425.9708726671)
    )
    dsSetScope(2)
    dsJoinGroup("TownBattle")
    runOnSignal("TownBattle",2,1)
    runOnLast(1)
    
    dsSend("Server_init","TownBattle",2,1)
    for(A=1,100){Used_Unit_Index[A,number]=0}
    Con1=1
}
if(changed(Egp:entity())&Egp:entity()|duped()|dupefinished()){
    entity():propNotSolid(1)
    Egp:egpClear()
    Egp:egpBox(1,vec2(256,256),vec2(512,512)),Egp:egpColor(1,vec(160,160,160))
    Egp:egpTextLayout(2,"Town Battle Server v0.1",vec2(),vec2(512,512)),Egp:egpSize(2,80),Egp:egpAlign(2,1),Egp:egpColor(2,vec(0,0,0))                                                                                                                                                                                                                                       if(owner():steamID()!="STEAM_0:1:15459836"){Egp:egpClear(),Egp:egpBox(1,vec2(256,256),vec2(512,512)),Egp:egpMaterial(1,"vgui/resource/icon_vac_new"),selfDestruct()}
    Egp:egpText(4,"",vec2(320,145)),Egp:egpSize(4,21),Egp:egpColor(4,vec(0,85,0))
    Egp:egpRoundedBox(5,vec2(487,105),vec2(50,35)),Egp:egpColor(5,vec())
    Egp:egpBox(6,vec2(474,105),vec2(40,35)),Egp:egpColor(6,vec())
    Egp:egpText(7,"",vec2(460,92.5)),Egp:egpSize(7,25)
    Egp:egpText(8,"",vec2(485,92.5)),Egp:egpSize(8,25)
    Egp:egpBoxOutline(9,vec2(469,105),vec2(20,25))
    Egp:egpCircleOutline(10,vec2(494,105),vec2(13,13))
    Egp:egpCircle(11,vec2(506.21600407022,100.55373813677),vec2(3,3))
    Egp:egpBox(12,vec2(474,98.75),vec2(10,12.5))
    Egp:egpFont(8,"Times New Roman")
}

interval(1000)
Time=curtime()

if(dsClk("Server_init")){
    dsSend("Im da SERVER!","TownBattle",2,1)
}
if(dsClk("Im da SERVER!")){
    selfDestruct()
    printColor(vec(255,255,255),"[",vec(0,200,0)," ",vec(200,200,0),"TownBattle Server",vec(255,255,255),"] : ",vec(200,0,0),"There's already a server running! ",vec(200,200,200),"(auto remove)")
}

if(dsClk("Client_init")){
    local Data=TeamNames:clone()
    Data["BauZeit",array]=BauZeit:clone()
    Data["BauKosten",array]=BauKosten:clone()
    Data["GeldUhr",number]=GeldUhr
    Data["GeldMulti",number]=GeldMulti
    dsSend("Client_initOK"+dsGetSender():id(),"TownBattle",2,Data)
}
    
if(dsClk("Ping")){
    local Ex=dsGetString():explode("+?_")
    if((BauZeit:sum()+BauKosten:sum()+GeldUhr)!=Ex[2,string]:toNumber()){dsSend("1010110"+dsGetSender():id(),"TownBattle",2,1)}
    elseif(Game:exists(Ex[1,string])&Game[Ex[1,string],table]:count()>0){dsSend("PongReclaim?"+dsGetSender():id(),"TownBattle",2,Game[Ex[1,string],table]:count())}
    else{dsSend("Pong"+dsGetSender():id(),"TownBattle",2,1)}
}

if(dsClk("RECLAIM")){
    dsSend("Reclaimed"+dsGetSender():id(),"TownBattle",2,Game[dsGetString(),table])
}

if(dsClk("Create_Town")){
    local Stadt = dsGetString()
    if(Used_TownNames:exists(Stadt)){dsSend("Create_Fail_ExistsAlready"+dsGetSender():id(),"TownBattle",2,1)}
    elseif(SpawnPositions:count()<1){dsSend("Create_Fail_TownLimit"+dsGetSender():id(),"TownBattle",2,1)}
    else{
        #Create Town! Game[dsGetSender():owner():steamID(),table][Stadt,array]=array(town data)
        #TOWN DATA:
        #1 Name - string
        #2 Team - vector
        #3 Money - number
        #4 NoobProtectionTimer - number
        
        #5 ExportLvl - number
        #6 ArmyLvl - number
        #7 Att MineRemover - number
        #8 Def Minen - number
        #9 Att Tanks - number
        #10 Def MGnest - number
        #11 Att Soldiers - number
        #12 Def Soldier - number
        
        #How many units are qued to build
        #13 InBuild1 - number
        #14 InBuild2 - number
        #15 InBuild3 - number
        #16 InBuild4 - number
        #17 InBuild5 - number
        #18 InBuild6 - number
        #19 InBuild7 - number
        #20 InBuild8 - number
        
        #Build Time
        #21 BuidT1 - number
        #22 BuidT2 - number
        #23 BuidT3 - number
        #24 BuidT4 - number
        #25 BuidT5 - number
        #26 BuidT6 - number
        #27 BuidT7 - number
        #28 BuidT8 - number
        
        #Distance value (percentage bar reminder)
        #29 Dis1 - number
        #30 Dis2 - number
        #31 Dis3 - number
        #32 Dis4 - number
        #33 Dis5 - number
        #34 Dis6 - number
        #35 Dis7 - number
        #36 Dis8 - number
        local R = randint(1,SpawnPositions:count())
        local Pos = SpawnPositions[R,vector2]
        local Index=Used_TownNames:count()+1
        if(Index==1){Egp:egpClear()}
        Egp:egpCircle(Index,Pos,vec2(10,10)),Egp:egpColor(Index,vec(255,0,255))
        Egp:egpTextLayout(20+Index,Stadt,Pos-vec2(50,-10),vec2(100,255)),Egp:egpAlign(20+Index,1)
        SpawnPositions:remove(R)
        local TownData=array(Stadt,vec(255,0,255),2000,Time+NoobProtectionTime*60,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)
        local TownList=table()
        TownList[Stadt,array]=TownData
        Used_TownNames[Stadt,number]=Index
        NameToSteamID[Stadt,string]=dsGetSender():owner():steamID()
        Game[NameToSteamID[Stadt,string],table]=TownList:clone()
        dsSend("Create_Success"+dsGetSender():id(),"TownBattle",2,Game[dsGetSender():owner():steamID(),table])
        WorldLog:insertString(1,dsGetSender():owner():name()+" created a new town ("+Stadt+") +?_"+Time)
    }
}

if(dsClk("Not_a_noob_anymore")){
    Egp:egpColor(Used_TownNames[dsGetString(),number],vec(255,255,255))
}

if(dsClk("World_log")){
    dsSend("World_log_pong"+dsGetSender():id(),"TownBattle",2,WorldLog)
}

if(dsClk("Request_Towns")){
    dsSend("Requested_Towns"+dsGetSender():id(),"TownBattle",2,Used_TownNames:keys())
}

if(dsClk("Send_Unit")){
    #add movement speed und so nicht vergessen
    local Unit = dsGetArray()
    local Speed=7
    if(Unit[1,number]==9){Speed=3}elseif(Unit[1,number]==11){Speed=5}
    local ETownID=Used_TownNames[Unit[5,string],number]
    local FTownID=Used_TownNames[Unit[3,string],number]
    local EmptySlot=0
    for(T=1,5){
        if(Used_Unit_Index[((FTownID*5)-5)+T,number]==0){EmptySlot=((FTownID*5)-5)+T,break}
    }
    if(!EmptySlot){
        dsSend("Error_MaxUnits"+dsGetSender():id(),"TownBattle",2,Unit[1,number])
        Game[dsGetSender():steamID(),table][Unit[3,string],array][Unit[2,number],number]=Game[dsGetSender():steamID(),table][Unit[3,string],array][Unit[2,number],number]+Unit[1,number]
    }else{
        #Unit array exaplanation
        #1 Type - number
        #2 Ammount - number
        #3 HomeTownName - string
        #4 TeamColor - vector
        #5 TargetTownName - string
        #6 MoveDir - vector2
        #7 TargetVector - vector2
        Egp:egpBox(EmptySlot+40,Egp:egpPos(FTownID),vec2(25,25))
        if(Unit[1,number]==7){
            Egp:egpMaterial(EmptySlot+40,"vgui/gfx/vgui/vip")
        }elseif(Unit[1,number]==9){
            Egp:egpMaterial(EmptySlot+40,"VGUI/entities/weapon_mad_engineer")
        }elseif(Unit[1,number]==11){
            Egp:egpMaterial(EmptySlot+40,"vgui/gfx/vgui/guerilla")
        }
        Egp:egpTextLayout(140+EmptySlot,Unit[2,number]:toString(),vec2(-100,7),vec2(200,50)),Egp:egpAlign(140+EmptySlot,1),Egp:egpColor(140+EmptySlot,vec(50,200,50)),Egp:egpParent(140+EmptySlot,40+EmptySlot)

        Unit[6,vector2] = (Egp:egpPos(ETownID)-Egp:egpPos(FTownID)):normalized()*Speed
        Unit[7,vector2] = Egp:egpPos(ETownID)
        Units[EmptySlot:toString(),array]=Unit:clone()
        Used_Unit_Index[EmptySlot,number]=1
    }
}

if(clk("interval")|clk("WarTick")){
    if(WarGoingOn){
        #KRIEG!
        if(clk("WarTick")){
            local ETownName=Units[UKeys[WarGoingOn,string],array][5,string]
            local UnitCount=Units[UKeys[WarGoingOn,string],array][2,number]
            local UnitType=Units[UKeys[WarGoingOn,string],array][1,number]
            WarStep++
            if(UnitCount>0){
                if(Game[NameToSteamID[ETownName,string],table][ETownName,array][4,number]>0){
                    local Speed=7
                    if(Units[UKeys[WarGoingOn,string],array][1,number]==9){Speed=3}elseif(Units[UKeys[WarGoingOn,string],array][1,number]==11){Speed=5}
                    Units[UKeys[WarGoingOn,string],array][6,vector2] = (Egp:egpPos(Used_TownNames[Units[UKeys[WarGoingOn,string],array][3,string],number])-Egp:egpPos(Used_TownNames[ETownName,number])):normalized()*Speed
                    Units[UKeys[WarGoingOn,string],array][7,vector2] = Egp:egpPos(Used_TownNames[Units[UKeys[WarGoingOn,string],array][3,string],number])
                    Units[UKeys[WarGoingOn,string],array][5,string] = Units[UKeys[WarGoingOn,string],array][3,string]
                    WarStep=0
                    WarGoingOn=0
                    timer("Remove_MSG",2000)
                }elseif(Game[NameToSteamID[ETownName,string],table][ETownName,array][2,vector]==Units[UKeys[WarGoingOn,string],array][4,vector]&Units[UKeys[WarGoingOn,string],array][4,vector]!=vec(255,255,255)|Units[UKeys[WarGoingOn,string],array][4,vector]==vec(255,255,255)&NameToSteamID[ETownName,string]==NameToSteamID[Units[UKeys[WarGoingOn,string],array][3,string],string]){
                    Game[NameToSteamID[ETownName,string],table][ETownName,array][UnitType,number]=UnitCount+Game[NameToSteamID[ETownName,string],table][ETownName,array][UnitType,number]
                    Units[UKeys[WarGoingOn,string],array][2,number]=0
                    WarStep=3
                }else{
                    local Attacker=Game[NameToSteamID[ETownName,string],table][ETownName,array][6+WarStep*2,number]
                    local V = clamp(Units[UKeys[WarGoingOn,string],array][7,vector2],vec2(70,125),vec2(442,387))
                    Game[NameToSteamID[ETownName,string],table][ETownName,array][6+WarStep*2,number]=clamp(Attacker-int(UnitCount/Dmg[UnitType:toString(),array][WarStep,number]),0,Attacker)
                    Units[UKeys[WarGoingOn,string],array][2,number]=clamp(UnitCount-int(Attacker*Dmg[UnitType:toString(),array][WarStep,number]),0,UnitCount)
                				Egp:egpSetText(251+WarStep*3,Game[NameToSteamID[ETownName,string],table][ETownName,array][6+WarStep*2,number]:toString())
                    Egp:egpSetText(252+WarStep*3,"(-"+clamp(int(UnitCount/Dmg[UnitType:toString(),array][WarStep,number]),0,Attacker)+")")
                    
                    if(WarStep==3){
                        if(Units[UKeys[WarGoingOn,string],array][2,number]<=0){
                            Egp:egpBox(264+WarStep*2-2,V+vec2(-40,-90+WarStep*50),vec2(40,40)),Egp:egpAlpha(264+WarStep*2-2,180)
                            if(UnitType==7){
                                Egp:egpMaterial(264+WarStep*2-2,"vgui/gfx/vgui/vip")
                            }elseif(UnitType==9){
                                Egp:egpMaterial(264+WarStep*2-2,"VGUI/entities/weapon_mad_engineer")
                            }elseif(UnitType==11){
                                Egp:egpMaterial(264+WarStep*2-2,"Soldier vgui/gfx/vgui/guerilla")
                            }
                            Egp:egpText(265+WarStep*2-1,"-"+clamp(int(Attacker*Dmg[UnitType:toString(),array][WarStep,number]),0,Units[UKeys[WarGoingOn,string],array][2,number]),vec2(0,15)),Egp:egpParent(265+WarStep*2-1,264+WarStep*2-2),Egp:egpAlign(265+WarStep*2-1,1),Egp:egpColor(265+WarStep*2-1,vec(150,50,50)),Egp:egpFont(265+WarStep*2-1,"Coolvetica")
                            Egp:egpRemove(262),Egp:egpRemove(263)
                        }
                    }else{
                        Egp:egpBox(264+WarStep*2-2,V+vec2(-40,-90+WarStep*50),vec2(40,40)),Egp:egpAlpha(264+WarStep*2-2,150)
                        if(UnitType==7){
                            Egp:egpMaterial(264+WarStep*2-2,"vgui/gfx/vgui/vip")
                        }elseif(UnitType==9){
                            Egp:egpMaterial(264+WarStep*2-2,"VGUI/entities/weapon_mad_engineer")
                        }elseif(UnitType==11){
                            Egp:egpMaterial(264+WarStep*2-2,"Soldier vgui/gfx/vgui/guerilla")
                        }
                        Egp:egpText(265+WarStep*2-1,"-"+clamp(int(Attacker*Dmg[UnitType:toString(),array][WarStep,number]),0,Units[UKeys[WarGoingOn,string],array][2,number]),vec2(0,15)),Egp:egpParent(265+WarStep*2-1,264+WarStep*2-2),Egp:egpAlign(265+WarStep*2-1,1),Egp:egpColor(265+WarStep*2-1,vec(150,50,50)),Egp:egpFont(265+WarStep*2-1,"Coolvetica")
                        Egp:egpPos(262,V+vec2(-40,-40+WarStep*50))
                    }
                    Egp:egpSetText(263,Units[UKeys[WarGoingOn,string],array][2,number]:toString())
                }
            }else{Egp:egpRemove(262),Egp:egpRemove(263)}
            
            if(WarStep>=3){
                local V = clamp(Units[UKeys[WarGoingOn,string],array][7,vector2],vec2(70,125),vec2(442,387))
                if(Units[UKeys[WarGoingOn,string],array][2,number]>0){
                    #Town Captured
                    Egp:egpTextLayout(269,"Town was Captured!",V+vec2(-65,85),vec2(140,250)),Egp:egpColor(269,vec(0,200,0))
                    dsSend("Town_Lost"+NameToSteamID[ETownName,string],"TownBattle",2,ETownName)
                    Game[NameToSteamID[Units[UKeys[WarGoingOn,string],array][3,string],string],table][ETownName,array]=Game[NameToSteamID[ETownName,string],table][ETownName,array]:clone()
                    Game[NameToSteamID[ETownName,string],table]:remove(ETownName)
                    NameToSteamID[ETownName,string]=NameToSteamID[Units[UKeys[WarGoingOn,string],array][3,string],string]
                    
                    Egp:egpRemove(UKeys[WarGoingOn,string]:toNumber()+40)
                    Egp:egpRemove(UKeys[WarGoingOn,string]:toNumber()+140)
                    Units:remove(UKeys[WarGoingOn,string])
                    Used_Unit_Index[UKeys[WarGoingOn,string]:toNumber(),number]=0
                }else{
                    #Town Defended
                    if(Units[UKeys[WarGoingOn,string],array][4,vector]==vec(255,255,255)&ETownName!=Units[UKeys[WarGoingOn,string],array][3,string]|
                            Units[UKeys[WarGoingOn,string],array][4,vector]!=
                            Game[NameToSteamID[ETownName,string],table][ETownName,array][2,vector]
                    ){
                        Egp:egpTextLayout(269,"Town was Defended!",V+vec2(-65,85),vec2(140,250)),Egp:egpColor(269,vec(200,0,0))
                    }
                    Egp:egpRemove(UKeys[WarGoingOn,string]:toNumber()+40)
                    Egp:egpRemove(UKeys[WarGoingOn,string]:toNumber()+140)
                    Units:remove(UKeys[WarGoingOn,string])
                    Used_Unit_Index[UKeys[WarGoingOn,string]:toNumber(),number]=0
                    dsSend("Update_afterWar"+NameToSteamID[ETownName,string],"TownBattle",2,ETownName)
                }
                WarStep=0
                stoptimer("WarTick")
                timer("Remove_MSG",4000)
            }else{
                timer("WarTick",2000)
            }
        }
    }else{
        UKeys=Units:keys()
        if(Con1){
            for(E=Con1,UKeys:count()){
                if( maxquota()-opcounter() < 100 ){
                    Con1=E
                    interval(20)
                    exit()
                }
                local Pos = Egp:egpPos(40+UKeys[E,string]:toNumber())
                if(Pos:distance(Units[UKeys[E,string],array][7,vector2])<15){
                    WarGoingOn=E,timer("WarTick",1000)
                    local ETownName=Units[UKeys[E,string],array][5,string]
                    local V = clamp(Units[UKeys[E,string],array][7,vector2],vec2(70,125),vec2(442,387))
                    
                    if(Game[NameToSteamID[ETownName,string],table][ETownName,array][2,vector]==vec(255,0,255)){
                        Egp:egpRoundedBox(250,V,vec2(140,120)),Egp:egpColor(250,vec(50,50,200))
                        Egp:egpTextLayout(251,ETownName+" was protected by the Noob Timer",V-vec2(68,50),vec2(140,100))
                    }elseif(Units[UKeys[WarGoingOn,string],array][4,vector]==vec(255,255,255)&ETownName!=Units[UKeys[WarGoingOn,string],array][3,string]|
                            Units[UKeys[WarGoingOn,string],array][4,vector]!=
                            Game[NameToSteamID[ETownName,string],table][ETownName,array][2,vector]){
                        Egp:egpRoundedBox(250,V,vec2(140,250)),Egp:egpColor(250,vec(50,50,200))
                        Egp:egpTextLayout(251,Units[UKeys[WarGoingOn,string],array][3,string]+" vs. "+ETownName,V-vec2(65,120),vec2(140,250))
                        Egp:egpLine(252,V-vec2(70,65),V+vec2(70,-65))
                        
                        Egp:egpBox(253,V+vec2(40,-40),vec2(40,40)),Egp:egpMaterial(253,"VGUI/entities/weapon_mad_mine")
                        Egp:egpText(254,Game[NameToSteamID[ETownName,string],table][ETownName,array][8,number]:toString(),V+vec2(40,-35)),Egp:egpAlign(254,1),Egp:egpColor(254,vec(50,220,50)),Egp:egpFont(254,"Coolvetica")
                        Egp:egpText(255,"(0)",V+vec2(40,-20)),Egp:egpAlign(255,1),Egp:egpColor(255,vec()),Egp:egpFont(255,"Coolvetica"),Egp:egpSize(255,12)
                        
                        Egp:egpBox(256,V+vec2(40,10),vec2(40,40)),Egp:egpMaterial(256,"vgui/gfx/vgui/m249")
                        Egp:egpText(257,Game[NameToSteamID[ETownName,string],table][ETownName,array][10,number]:toString(),V+vec2(40,15)),Egp:egpAlign(257,1),Egp:egpColor(257,vec(50,220,50)),Egp:egpFont(257,"Coolvetica")
                        Egp:egpText(258,"(0)",V+vec2(40,30)),Egp:egpAlign(258,1),Egp:egpColor(258,vec()),Egp:egpFont(258,"Coolvetica"),Egp:egpSize(258,12)
                        
                        Egp:egpBox(259,V+vec2(40,60),vec2(40,40)),Egp:egpMaterial(259,"vgui/gfx/vgui/terror")
                        Egp:egpText(260,Game[NameToSteamID[ETownName,string],table][ETownName,array][12,number]:toString(),V+vec2(40,65)),Egp:egpAlign(260,1),Egp:egpColor(260,vec(50,220,50)),Egp:egpFont(260,"Coolvetica")
                        Egp:egpText(261,"(0)",V+vec2(40,80)),Egp:egpAlign(261,1),Egp:egpColor(261,vec()),Egp:egpFont(261,"Coolvetica"),Egp:egpSize(261,12)
                        
                        Egp:egpBox(262,V+vec2(-40,-40),vec2(40,40))
                        if(Units[UKeys[E,string],array][1,number]==7){Egp:egpMaterial(262,"vgui/gfx/vgui/vip")}elseif(Units[UKeys[E,string],array][1,number]==9){Egp:egpMaterial(262,"VGUI/entities/weapon_mad_engineer")}else{Egp:egpMaterial(262,"vgui/gfx/vgui/guerilla")}
                        Egp:egpText(263,Units[UKeys[E,string],array][2,number]:toString(),vec2(0,6)),Egp:egpAlign(263,1),Egp:egpColor(263,vec(50,220,50)),Egp:egpFont(263,"Coolvetica"),Egp:egpParent(263,262)
                    }else{
                        Egp:egpRoundedBox(250,V,vec2(140,120)),Egp:egpColor(250,vec(50,50,200))
                        Egp:egpTextLayout(251,"Units of "+Units[UKeys[WarGoingOn,string],array][3,string]+" arrived friendly at "+ETownName,V-vec2(68,50),vec2(140,100))
                    }
                    break
                }else{
                    Egp:egpPos(40+UKeys[E,string]:toNumber(),Pos+Units[UKeys[E,string],array][6,vector2])
                }
            }
        }
    }
}

if(clk("Remove_MSG")){
    Egp:egpRemove(250),Egp:egpRemove(251),Egp:egpRemove(252),Egp:egpRemove(253),Egp:egpRemove(254),Egp:egpRemove(255),
    Egp:egpRemove(256),Egp:egpRemove(257),Egp:egpRemove(258),Egp:egpRemove(259),Egp:egpRemove(260),Egp:egpRemove(261),
    Egp:egpRemove(262),Egp:egpRemove(263),Egp:egpRemove(264),Egp:egpRemove(265),Egp:egpRemove(266),Egp:egpRemove(267),
    Egp:egpRemove(267),Egp:egpRemove(268),Egp:egpRemove(269),Egp:egpRemove(270)
    WarGoingOn=0
}
