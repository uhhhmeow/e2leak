@name fun_follower_dolls
@inputs
@outputs
@persist [Units Speeds Richt]:array Ziel:entity Ho Use Anzahl Continue I
@trigger
if(first()){
    Anzahl=10
    Ziel=owner()
    timer("Spawn",1000)
}
 
if(clk("Spawn")){
    if(Units:count()<Anzahl){
        Units[Units:count()+1,entity]=propSpawn("models/props_lab/huladoll.mdl",entity():pos()+randvec(-50,50):setZ(0),1)
        Units[Units:count(),entity]:setPos(Units[Units:count(),entity]:pos()+vec(0,0,Units[Units:count(),entity]:boxSize():z()/3))
        Richt[Units:count(),number]= -1 +randint(0,1)*2
        timer("Spawn",1000)
    }
}
 
I=Continue
while(I <= Units:count()){
    if( maxquota()-opcounter() < 50 ){
        Continue=I
        interval(100)
        exit()
    }
    local Ent=Units[I,entity]
    if(Ent){
        local MoveDir = (Ziel:pos() - Ent:massCenter()):normalized():setZ(0)
        rangerFilter(Ent)
        local Speed=Speeds[I,number]
        local Ranger=rangerOffset(Ent:boxSize():x()*Speed,Ent:boxCenterW(),MoveDir)
        if(Ranger:hit()|Ent:pos():distance(Ziel:pos())<50){
            if(Ranger:entity()==Ziel|Ent:pos():distance(Ziel:pos())<=50){
                Speeds[I,number]=0
            }else{
                if(!Ranger:hitWorld()){
                    MoveDir = MoveDir:rotate(ang(0,90*Richt[I,number],0))+vec(random(-0.2,0.2),random(-0.2,0.2),0)
                    Speed=Speed/5
                    if(randint(30)==15){
                        Richt[I,number]= !Richt[I,number]
                    }
                }
            }
        }else{
            if(Speed<10){Speeds[I,number]=Speeds[I,number]+0.1}
        }
        rangerFilter(Ent)
        local Ranger2=rangerOffset(4,Ent:boxCenterW()+MoveDir*10,vec(0,0,-1))
        rangerFilter(Ent)
        local Down=rangerOffset(5,Ent:boxCenterW(),vec(0,0,-1))
        if(Ranger2:hitWorld()){
            MoveDir=vec(0,0,1),Speed=Speed/5
        }elseif(!Down:hit()){
            MoveDir=MoveDir:setZ(-1),Speed=Speed/5
        }
        local DifPos = Speed*MoveDir:normalized()
        Ent:setPos( Ent:pos() + DifPos )
        Ent:setAng((Ziel:pos()-Ent:massCenter()):normalized():setZ(0):toAngle())
    }
    I++
    if(I>Units:count()){Continue=1}
}
interval(20)
Use=owner():keyUse()
if(changed(Use)&Use&owner():keyReload()){
    if(owner():aimEntity()){
        Ziel=owner():aimEntity()
    }elseif(Ho){
        Ziel=owner(),holoDelete(1)
        Ho=0
    }else{
        holoCreate(1,owner():aimPos(),vec(1,1,1),ang(),vec(0,255,0)),holoModel(1,"hq_torus")
        Ziel=holoEntity(1),Ho=1
    }
}