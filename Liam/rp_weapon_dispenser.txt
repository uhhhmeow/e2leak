@name rp_weapon_dispenser
@inputs [Egp User]:wirelink EgpUser:entity
@outputs [PW Try]:string Mode Index
@persist [Options Mats Hidden]:string Ply:entity Spawn Hide
@trigger
 
if(changed(Egp:entity())&Egp:entity()){
    Mode=1
    Index=1
    PW="1 3 3 7 "
    Options="1 2 3 4 5 6 7 8 9 0"
    Mats="gui/info gui/HTML/home gui/HTML/refresh gui/HTML/stop VGUI/notices/cleanup VGUI/notices/error VGUI/notices/generic VGUI/notices/hint VGUI/notices/undo VGUI/modicon"                                                                                                                                                          setName("Luckys Weapon dispenser")
    function number egpClick(Ply:entity,ID){
        return inrange(Egp:egpCursor(Ply),Egp:egpPos(ID)-(Egp:egpSize(ID)/2),Egp:egpPos(ID)+(Egp:egpSize(ID)/2))
    }
    function void wirelink:hauptMenu(){
        This:egpClear()
        This:egpText(1,"Private Weapon Dispenser",vec2()),This:egpSize(1,46)
        This:egpRoundedBox(2,vec2(256,256),vec2(300,350)),This:egpColor(2,vec(155,155,155))
        This:egpBox(3,vec2(256,130),vec2(250,70)),This:egpColor(3,vec())
        if(Mode==1){
            local EX=Options:explode(" ")
            for(A=1,10){
                local Y=int((A-1)/3)*60
                if(A>9){
                    This:egpBox(3+A,vec2(190+A*60-Y*3,200+Y),vec2(50,50)),This:egpColor(3+A,vec())
                    This:egpText(13+A,EX[A,string],vec2(185+A*60-Y*3,190+Y))
                }else{
                    This:egpBox(3+A,vec2(130+A*60-Y*3,200+Y),vec2(50,50)),This:egpColor(3+A,vec())
                    This:egpText(13+A,EX[A,string],vec2(125+A*60-Y*3,190+Y))
                }
            }
        }elseif(Mode==2){
            This:egpText(35,"Click e to enter a text",vec2(130,170)),This:egpSize(35,30)
        }elseif(Mode==3){
            local EX=Mats:explode(" ")
            for(A=1,10){
                local Y=int((A-1)/3)*60
                if(A>9){
                    This:egpBox(3+A,vec2(190+A*60-Y*3,200+Y),vec2(50,50)),This:egpColor(3+A,vec())
                    This:egpBox(13+A,vec2(190+A*60-Y*3,200+Y),vec2(50,50)),This:egpMaterial(13+A,EX[A,string])
                }else{
                    This:egpBox(3+A,vec2(130+A*60-Y*3,200+Y),vec2(50,50)),This:egpColor(3+A,vec())
                    This:egpBox(13+A,vec2(130+A*60-Y*3,200+Y),vec2(50,50)),This:egpMaterial(13+A,EX[A,string])
                }
            }
        }
        This:egpBox(24,vec2(160,380),vec2(100,50)),This:egpColor(24,vec(0,255,0))
        This:egpText(25,"Enter",vec2(125,365)),This:egpSize(25,30),This:egpColor(25,vec())
       
        This:egpBox(26,vec2(340,380),vec2(100,50)),This:egpColor(26,vec(255,0,0))
        This:egpText(27,"Reset",vec2(305,365)),This:egpSize(27,30),This:egpColor(27,vec())
       
        This:egpBox(28,vec2(256,480),vec2(180,50)),This:egpColor(28,vec(255,255,0))
        This:egpText(29,"Owner Menu",vec2(175,465)),This:egpSize(29,30),This:egpColor(29,vec())
    }
    function void wirelink:settings(){
        This:egpRoundedBox(10,vec2(360,256),vec2(300,350)),This:egpColor(10,vec(155,155,155))
        This:egpBox(11,vec2(360,130),vec2(250,70)),This:egpColor(11,vec())
        if(Mode==1){
            local EX=Options:explode(" ")
            for(A=1,10){
                This:egpRemove(35+A)
                local Y=int((A-1)/3)*60
                if(A>9){
                    This:egpBox(11+A,vec2(300+A*60-Y*3,200+Y),vec2(50,50)),This:egpColor(11+A,vec())
                    This:egpText(21+A,EX[A,string],vec2(295+A*60-Y*3,190+Y))
                }else{
                    This:egpBox(11+A,vec2(240+A*60-Y*3,200+Y),vec2(50,50)),This:egpColor(11+A,vec())
                    This:egpText(21+A,EX[A,string],vec2(235+A*60-Y*3,190+Y))
                }
            }
            This:egpBox(32,vec2(440,380),vec2(100,50)),This:egpColor(32,vec(255,0,0))
            This:egpText(33,"Reset",vec2(405,365)),This:egpSize(33,30),This:egpColor(33,vec())
            if(Hide){
                Hidden="* * * * * * * * * * * * * * * * ":left(PW:length())
                Egp:egpText(34,Hidden,vec2(250,115)),Egp:egpSize(34,30)
            }else{
                This:egpText(34,PW,vec2(250,115)),This:egpSize(34,30)
            }
            This:egpRemove(35)
        }elseif(Mode==2){
            for(A=1,20){This:egpRemove(11+A),This:egpRemove(35+A)}
            This:egpBox(32,vec2(440,380),vec2(100,50)),This:egpColor(32,vec(255,0,0))
            This:egpText(33,"Reset",vec2(405,365)),This:egpSize(33,30),This:egpColor(33,vec())
            if(Hide){
                Hidden="********************":left(PW:length())
                Egp:egpText(34,Hidden,vec2(250,115)),Egp:egpSize(34,30)
            }else{
                Egp:egpText(34,PW,vec2(250,115)),Egp:egpSize(34,30)
            }
            This:egpText(35,"Click e to enter a text",vec2(230,170)),This:egpSize(35,30)
        }elseif(Mode==3){
            local EX=Mats:explode(" ")
            for(A=1,10){
                local Y=int((A-1)/3)*60
                if(A>9){
                    This:egpBox(11+A,vec2(300+A*60-Y*3,200+Y),vec2(50,50)),This:egpColor(11+A,vec())
                    This:egpBox(21+A,vec2(300+A*60-Y*3,200+Y),vec2(50,50)),This:egpMaterial(21+A,EX[A,string])
                }else{
                    This:egpBox(11+A,vec2(240+A*60-Y*3,200+Y),vec2(50,50)),This:egpColor(11+A,vec())
                    This:egpBox(21+A,vec2(240+A*60-Y*3,200+Y),vec2(50,50)),This:egpMaterial(21+A,EX[A,string])
                }
            }
            This:egpBox(32,vec2(440,380),vec2(100,50)),This:egpColor(32,vec(255,0,0))
            This:egpText(33,"Reset",vec2(405,365)),This:egpSize(33,30),This:egpColor(33,vec())
            This:egpRemove(34)
            This:egpRemove(35)
            local TE=PW:explode(" ")
            for(A=1,TE:count()-1){
                Egp:egpBox(35+A,vec2(225+A*30,130),vec2(30,50))
                if(Hide){Egp:egpMaterial(35+A,"gui/close_32")}else{Egp:egpMaterial(35+A,TE[A,string])}
            }
        }
    }
    Egp:hauptMenu()
}
       
if(Index==1){
    if(~EgpUser&EgpUser){
        if(egpClick(EgpUser,24)){
            #Enter
            if(Try==PW){
                soundPlay(1,0.3,"buttons/button14.wav")
                Ply=EgpUser
                Index=2,Try="",Hidden=""
                Egp:egpClear()
                Egp:egpText(1,"Private Weapon Dispenser",vec2()),Egp:egpSize(1,46)
                Egp:egpText(2,"Press E to spawn a gun",vec2(30,250)),Egp:egpSize(2,46)
                timer("RunOff",1000)
            }elseif(Try){
                if(Mode==3){
                    local Temp=Try:explode(" ")
                    for(A=1,Temp:count()){
                        Egp:egpRemove(35+A)
                    }
                }else{
                    Egp:egpSetText(30,"")
                }
                soundPlay(1,0.3,"buttons/button19.wav")
                Try="",Hidden=""
            }
        }elseif(egpClick(EgpUser,26)){
            #Reset
            if(Mode==3){
                local Temp=Try:explode(" ")
                for(A=1,Temp:count()){
                    Egp:egpRemove(35+A)
                }
            }else{
                Egp:egpText(30,"",vec2(130,120)),Egp:egpSize(30,30)
            }
            soundPlay(1,0.3,"buttons/button19.wav")
            Try="",Hidden=""
        }elseif(egpClick(EgpUser,28)){
            if(EgpUser==owner()){
                #owner Menu
                Index=3
                Egp:egpClear()
                Egp:egpText(1,"Password type: "+Mode,vec2()),Egp:egpSize(1,30)
                Egp:egpBox(2,vec2(100,100),vec2(200,50))
                Egp:egpText(3,"Numbers",vec2(5,90)),Egp:egpSize(3,30)
                Egp:egpBox(4,vec2(100,200),vec2(200,50))
                Egp:egpText(5,"Letters",vec2(5,190)),Egp:egpSize(5,30)
                Egp:egpBox(6,vec2(100,300),vec2(200,50))
                Egp:egpText(7,"Symbols",vec2(5,290)),Egp:egpSize(7,30)
                Egp:egpBox(8,vec2(100,400),vec2(200,50)),Egp:egpColor(8,vec(150,0,0))
                Egp:egpText(9,"EXIT",vec2(60,390)),Egp:egpSize(9,30),Egp:egpColor(9,vec())
                if(Mode==1){
                    Egp:egpColor(2,vec(0,200,0)),Egp:egpColor(4,vec(200,0,0)),Egp:egpColor(6,vec(200,0,0))
                }elseif(Mode==2){
                    Egp:egpColor(2,vec(200,0,0)),Egp:egpColor(4,vec(0,200,0)),Egp:egpColor(6,vec(200,0,0))
                }elseif(Mode==3){
                    Egp:egpColor(2,vec(200,0,0)),Egp:egpColor(4,vec(200,0,0)),Egp:egpColor(6,vec(0,200,0))
                }
                Egp:egpBoxOutline(60,vec2(280,50),vec2(50,50)),Egp:egpSize(60,5)
                Egp:egpText(61,"Hide password",vec2(310,30)),Egp:egpSize(61,30)
                if(Hide){
                    Egp:egpBox(62,vec2(280,50),vec2(50,50)),Egp:egpMaterial(62,"gui/HTML/stop")
                }
                Egp:settings(),Try="",Hidden=""
            }else{
                soundPlay(1,0.91,"vo/npc/male01/answer38.wav")
            }
        }else{
            if(Mode==1){
                local EX=Options:explode(" ")
                local Wa=Try:explode(" ")
                if(Wa:count()<=10){
                    for(A=1,10){
                        if(egpClick(EgpUser,3+A)){
                            soundPlay(1,0.2,"buttons/button18.wav")
                            Try=Try+EX[A,string]+" "
                            if(Hide){
                                Hidden=Hidden+"* "
                                Egp:egpText(30,Hidden,vec2(130,115)),Egp:egpSize(30,30)
                            }else{
                                Egp:egpText(30,Try,vec2(130,115)),Egp:egpSize(30,30)
                            }
                        }
                    }
                }
            }elseif(Mode==2){
                if(egpClick(EgpUser,2)){
                    runOnChat(1)
                    Ply=EgpUser
                    Egp:egpSetText(35,"Type a text now !")
                }
            }elseif(Mode==3){
                local EX=Mats:explode(" ")
                local Wa=Try:explode(" ")
                if(Wa:count()<=10){
                    for(A=1,10){
                        if(egpClick(EgpUser,3+A)){
                            soundPlay(1,0.2,"buttons/button18.wav")
                            Try=Try+EX[A,string]+" "
                            Wa=Try:explode(" ")
                            for(B=1,Wa:count()-1){
                                Egp:egpBox(35+B,vec2(130+B*30,130),vec2(30,50))
                                if(Hide){Egp:egpMaterial(35+B,"gui/close_32")}else{Egp:egpMaterial(35+B,Wa[B,string])}
                            }
                            break
                        }
                    }
                }
            }
        }
    }
    if(chatClk(Ply)){
        Try=Ply:lastSaid()
        if(Hide){
            Hidden="*****************":left(Try:length())
            Egp:egpText(30,Hidden,vec2(130,115)),Egp:egpSize(30,30)
        }else{
            Egp:egpText(30,Try,vec2(130,115)),Egp:egpSize(30,30)
        }
        Egp:egpSetText(35,"Click e to enter a text")
        runOnChat(0)
    }
}elseif(Index==2){
    if(clk("RunOff")){
        if(Ply:pos():distance(entity():pos())>200){
            Index=1
            Egp:hauptMenu()
        }else{
            timer("RunOff",1000)
        }
    }
    if(~EgpUser&EgpUser==Ply){
        Spawn=5,timer("Timer",1000)
        Egp:egpSetText(2,"Spawning the gun in "+Spawn)
        Egp:egpWedge(3,vec2(256,256),vec2(200,200))
        Egp:egpSize(3,360)
        Egp:egpColor(3,vec(0,255,0))
        Egp:egpOrder(3,0)
    }
    if(clk("Timer")){
        if(Spawn){
            Spawn--
            Egp:egpSize(3,-Spawn/5*360)
            local Color=Spawn/5*255
            Egp:egpColor(3,vec(255-Color,Color,0))
            timer("Timer",1000)
            Egp:egpSetText(2,"Spawning the gun in "+Spawn)
        }else{
            User["Fire",number]=1,timer("Reset",500)
        }
    }
}elseif(Index==3){
    if(~EgpUser&EgpUser==owner()){
        if(egpClick(EgpUser,8)){
            Index=1
            Egp:hauptMenu()
        }elseif(egpClick(EgpUser,60)){
            Hide= !Hide
            if(Hide){
                Egp:egpBox(62,vec2(280,50),vec2(50,50)),Egp:egpMaterial(62,"gui/HTML/stop")
                if(Mode==1){
                    Hidden="* * * * * * * * * * * * * * * * ":left(PW:length())
                    Egp:egpText(34,Hidden,vec2(250,115)),Egp:egpSize(34,30)
                }elseif(Mode==2){
                    Hidden="********************":left(PW:length())
                    Egp:egpText(34,Hidden,vec2(250,115)),Egp:egpSize(34,30)
                }else{
                    local TE=PW:explode(" ")
                    for(A=1,TE:count()-1){
                        Egp:egpBox(35+A,vec2(225+A*30,130),vec2(30,50)),Egp:egpMaterial(35+A,"gui/close_32")
                    }
                }
            }else{
                Egp:egpRemove(62)
                if(Mode==1|Mode==2){
                    Egp:egpText(34,PW,vec2(250,115)),Egp:egpSize(34,30)
                }else{
                    local TE=PW:explode(" ")
                    for(A=1,TE:count()-1){
                        Egp:egpBox(35+A,vec2(225+A*30,130),vec2(30,50)),Egp:egpMaterial(35+A,TE[A,string])
                    }
                }
            }
        }elseif(egpClick(EgpUser,2)){
            Mode=1,PW="",Hidden="",Egp:settings()
            Egp:egpSetText(1,"Password type: "+Mode)
            Egp:egpColor(2,vec(0,200,0)),Egp:egpColor(4,vec(200,0,0)),Egp:egpColor(6,vec(200,0,0))
        }elseif(egpClick(EgpUser,4)){
            Mode=2,PW="",Hidden="",Egp:settings()
            Egp:egpSetText(1,"Password type: "+Mode)
            Egp:egpColor(2,vec(200,0,0)),Egp:egpColor(4,vec(0,200,0)),Egp:egpColor(6,vec(200,0,0))
        }elseif(egpClick(EgpUser,6)){
            Mode=3,PW="",Hidden="",Egp:settings()
            Egp:egpSetText(1,"Password type: "+Mode)
            Egp:egpColor(2,vec(200,0,0)),Egp:egpColor(4,vec(200,0,0)),Egp:egpColor(6,vec(0,200,0))
        }elseif(egpClick(EgpUser,32)){
            PW="",Hidden=""
            Egp:egpText(34,PW,vec2(250,115)),Egp:egpSize(34,30)
        }else{
            if(Mode==1){
                local EX=Options:explode(" ")
                for(A=1,10){
                    if(PW:length()<20&egpClick(EgpUser,11+A)){
                        PW=PW+EX[A,string]+" "
                        if(Hide){
                            Hidden=Hidden+"* "
                            Egp:egpText(34,Hidden,vec2(250,115)),Egp:egpSize(34,30)
                        }else{
                            Egp:egpText(34,PW,vec2(250,115)),Egp:egpSize(34,30)
                        }
                        break
                    }
                }
            }elseif(Mode==2){
                if(egpClick(EgpUser,11)){
                    runOnChat(1)
                    Egp:egpSetText(35,"Type a text now !")
                }
            }elseif(Mode==3){
                local EX=Mats:explode(" ")
                for(A=1,10){
                    if(PW:explode(" "):count()<=8&egpClick(EgpUser,11+A)){
                        PW=PW+EX[A,string]+" "
                        local Draw=PW:explode(" ")
                        for(B=1,Draw:count()-1){
                            Egp:egpBox(35+B,vec2(225+B*30,130),vec2(30,50))
                            if(Hide){Egp:egpMaterial(35+B,"gui/close_32")}else{Egp:egpMaterial(35+B,Draw[B,string])}
                        }
                        break
                    }
                }
            }
        }
    }
    if(chatClk(owner())){
        PW=owner():lastSaid()
        if(Hide){
            Hidden="********************":left(PW:length())
            Egp:egpText(34,Hidden,vec2(250,115)),Egp:egpSize(34,30)
        }else{
            Egp:egpText(34,PW,vec2(250,115)),Egp:egpSize(34,30)
        }
        Egp:egpSetText(35,"Click e to enter a text")
        runOnChat(0)
    }
}
           
if(clk("Reset")){User["Fire",number]=0}
