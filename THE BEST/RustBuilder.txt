@name RUST Building
@persist Building:number Type:string Gx Gy Gz X Y Z Scale Pr:entity ANGOFF:number ROTSCALE:number
@persist Holding:number LS:string
@persist BASEPROP:string POLEPROP:string MATERIAL:string WALLPROP:string STAIRPROP:string
@persist DOORPROP:string WINDOWPROP:string RAMPPROP:string GLASSPROP:string ROTWALLPROP:string
@persist PM:string PA:angle PP:vector PMAT:number FILLERPROP:string
@model models/bull/gates/microcontroller2.mdl
 
########################################
#       RUST STRUCTURE BUILDER         #
#                   #
# Based on the construction mechanism  #
#   in the game "Rust" by Facepunch    #
########################################
# Controls are as following by default #
# LMB:  Place tile                     #
# RMB:  Change tile                    #
# R:    Rotate tile                    #
# Z:    Undo tile                      #
########################################
 
 
runOnTick(1)
runOnChat(1)
 
# Check if the player is using the tool or not.
Building = (owner():weapon():type() == "weapon_crowbar") # This can be changed to what WEP or SWEP you want.
 
# "Enable" or "Disable" the holographic preview if the player is building or not.
holoAlpha(0,Building*100)
holoMaterial(0,"models/wireframe")
 
 
 
# Custom function for spawning props, materializing and setting their properties.
function void spawnCustom(CModel:string,CPos:vector,CAng:angle,CMat:number){
    if( !propCanCreate() ){
        timer("spawnprop",10)
        PM = CModel
        PP = CPos
        PA = CAng
        PMAT = CMat
    }else{
        Pr = propSpawn(CModel,1)
        Pr:setPos(CPos)
        Pr:setAng(CAng)
        if(CMat){ Pr:setMaterial(MATERIAL) }
}}
 
if( clk("spawnprop") ){
        spawnCustom("" + PM,vec() + PP,ang() + PA,0 + PMAT)
    }
   
# Define everything the first time, so it is not executed every server tick.
if( first() ){
    holoCreate(0)                               # Creates the holographic preview. So it can be worked with.
    propSpawnEffect(1)                          # Enables an effect when spawning props.
    Type = "base"                                       # Sets the default tile.
    Building = 0                                        # Variable for checking if you are building or not.
    Gx = 16                                                     # Default grid x-size
    Gy = 16                                                     # Default grid y-size
    Gz = 16                                                     # Default grid z-size
    Scale = 4                                           # Scaling, this only affects the holographic preview and the grid. Not the props.
    ANGOFF = 0                                          # Default rotation angle.
   
    # Define all props here.
    BASEPROP = "models/hunter/blocks/cube4x4x05.mdl"                    # Used as the foundation and ceiling.
    POLEPROP = "models/hunter/blocks/cube05x4x05.mdl"                   # A pole or pillar.
    WALLPROP = "models/hunter/blocks/cube4x4x025.mdl"                   # Used as the wall.
    ROTWALLPROP = "models/hunter/blocks/cube4x6x025.mdl"                # This is used as a wall, but only when the rotation is 45, 135 etc. ( So it fills the whole block )
    STAIRPROP = "models/hunter/blocks/cube1x4x025.mdl"                  # This is used in every part of the staircase. (Which contains 4 props)
    DOORPROP = "models/hunter/blocks/cube1x4x025.mdl"                   # Used to make doorways.
    WINDOWPROP = "models/hunter/blocks/cube1x4x025.mdl"                 # Used to make windows.
    GLASSPROP = "models/props_phx/construct/glass/glass_plate2x2.mdl"   # Used to make glass in windows.
    RAMPPROP = "models/hunter/blocks/cube1x6x025.mdl"                   # Used to make ramps or roof filler pieces.
    FILLERPROP = "models/hunter/blocks/cube1x4x025.mdl"   # Used to fil in halved walls
    MATERIAL = "models/props/CS_militia/roofbeams01"                    # Default material used for props.
    ROTSCALE = 45                                       # Angular rotation grid.
    Holding = 0                                         # Variable for checking if you are holding down a key.
}
 
# Check if the player is building and not holding a key, so double placement is not an issue.
if( Building && !Holding ){
       
        # Rotate if the player holds RELOAD (R)
        if( owner():keyReload() ){
            ANGOFF = ANGOFF + ROTSCALE
            hint("Rotate "+(ANGOFF),4)
        }
        # Change material
        if( owner():keyUse() ){
            if( MATERIAL == "models/props/CS_militia/roofbeams01" ){ # If current tile is "base"
                MATERIAL = "models/props_combine/health_charger_glass"# Set tile to "pole" ... and etc.
            }elseif(MATERIAL=="models/props_combine/health_charger_glass"){
                MATERIAL = "phoenix_storms/Metalfloor_2-3"
        }else{
                MATERIAL = "models/props/CS_militia/roofbeams01"
    }
        hint("Material: "+MATERIAL,4)
    }
       
        # Change tile if the player is holding ATTACK2 (RMB)
        if( owner():keyAttack2() ){
            if( Type == "base" ){       # If current tile is "base"
                Type = "pole"           # Set tile to "pole" ... and etc.
            }elseif(Type=="pole"){
                Type = "wall"
                }elseif(Type=="wall"){
                Type = "stair"
                }elseif(Type=="stair"){
                Type = "door"
                }elseif(Type=="door"){
                Type = "door2"
                }elseif(Type=="door2"){
                Type = "window"
                }elseif(Type=="window"){
                Type = "window2"
                }elseif(Type=="window2"){
                Type = "ramp"
                }elseif(Type=="ramp"){
                Type = "filler"
        }elseif(Type=="filler"){
                Type = "base"
        }
                # Tell the player the new tile
                hint("Tile \""+Type+"\"",4)
       }          
       
       # Place a tile if the player is holding ATTACK1 (LMB)
       if( owner():keyAttack1() ){
        if( Type == "base" ){                                   # If Tile is "base" then spawn the correct props.
            spawnCustom(BASEPROP,vec(X,Y,Z),ang(),1)
        }elseif(Type=="pole"){                                  # If Tile is "pole" then spawn the correct props. ... etc
            spawnCustom(POLEPROP,vec(X+6,Y,Z),ang(0,0,90),1)
                }elseif(Type=="wall"){
        if( (ANGOFF / 90) == floor(ANGOFF/90) ) {
        spawnCustom(WALLPROP,vec(X,Y,Z),ang(0,ANGOFF,90),1)
    }else{
        spawnCustom(ROTWALLPROP,vec(X,Y,Z),ang(90,ANGOFF,90),1)
}
    } elseif(Type=="stair"){
            spawnCustom(STAIRPROP,vec(X+(Scale*12),Y+(Scale*12*0.25),Z+(Scale*12)),ang(0,0,30),1)
            spawnCustom(STAIRPROP,vec(X,Y-(Scale*12*2),Z),ang(0,90,0),1)
            spawnCustom(STAIRPROP,vec(X,Y+(Scale*12*2),Z+(Scale*12*2)),ang(0,90,0),1)
            spawnCustom(STAIRPROP,vec(X-(Scale*12),Y+(Scale*12*0.25),Z-(Scale*12)),ang(0,180,30),1)
    } elseif(Type=="door"){
            spawnCustom(DOORPROP,vec(X,Y-(Scale*12*1.5),Z),ang(0,90,90),1)
            spawnCustom(DOORPROP,vec(X,Y+(Scale*12*1.5),Z),ang(0,90,90),1)
            spawnCustom(DOORPROP,vec(X,Y,Z+(Scale*12*1.5)),ang(90,90,90),1)
    } elseif(Type=="door2"){
            spawnCustom(DOORPROP,vec(X-(Scale*12*1.5),Y,Z),ang(0,180,90),1)
            spawnCustom(DOORPROP,vec(X+(Scale*12*1.5),Y,Z),ang(0,180,90),1)
            spawnCustom(DOORPROP,vec(X,Y,Z+(Scale*12*1.5)),ang(90,180,90),1)
    } elseif(Type=="window"){
            spawnCustom(GLASSPROP,vec(X,Y+(Scale*12*0.5),Z-(Scale*12*0.5)),ang(0,90,90),0)
            spawnCustom(WINDOWPROP,vec(X,Y,Z-(Scale*12*1.5)),ang(90,90,90),1)
            spawnCustom(WINDOWPROP,vec(X,Y-(Scale*12*1.5),Z),ang(0,90,90),1)
            spawnCustom(WINDOWPROP,vec(X,Y+(Scale*12*1.5),Z),ang(0,90,90),1)
            spawnCustom(WINDOWPROP,vec(X,Y,Z+(Scale*12*1.5)),ang(90,90,90),1)
               
    } elseif(Type=="window2"){
            spawnCustom(GLASSPROP,vec(X-(Scale*12*0.5),Y,Z-(Scale*12*0.5)),ang(0,180,90),0)
            spawnCustom(WINDOWPROP,vec(X,Y,Z-(Scale*12*1.5)),ang(90,180,90),1)
            spawnCustom(WINDOWPROP,vec(X-(Scale*12*1.5),Y,Z),ang(0,180,90),1)
            spawnCustom(WINDOWPROP,vec(X+(Scale*12*1.5),Y,Z),ang(0,180,90),1)
            spawnCustom(WINDOWPROP,vec(X,Y,Z+(Scale*12*1.5)),ang(90,180,90),1)
    } elseif(Type=="ramp"){
        Pr = propSpawn(RAMPPROP,ang(0,ANGOFF,45),1)
            Pr:setPos(vec(X,Y,Z)) Pr:setMaterial(MATERIAL)
    } elseif(Type=="filler"){
        spawnCustom(FILLERPROP,vec(X,Y,Z),ang(90,ANGOFF,0),1)
    } elseif(Type==""){
       
    }
   
    hint("Placed \""+Type+"\"",4)
    Pr:setMaterial(MATERIAL)                    # Sets the spawned props material, this was used before multiple props per spawn was made.
}
}
 
 
# Show the holographic preview for the tiles.
if( Building ){
   
    # Reset all XYZ coordinates with their respective scaled values.
    X = round( owner():aimPos():x() / Gx )*Gx
    Y = round( owner():aimPos():y() / Gy )*Gy
    Z = ceil( (owner():aimPos():z()-Gz/2) / Gz )*Gz
    # Reset the angle so you don't need holoAng(0,ang()) in every block not using another value for angle.
   holoAng(0,ang())
   
    if( Type == "base" ){                                                       # Again, if tile is "base" ...
        holoModel(0,"block")                                            # Sets the model of the holographic preview.
       holoScale(0,vec(Scale*4,Scale*4,Scale*0.5)) # Sets the scale of the hologram.
        Gx = Gy = Gz = Scale*12*4                                       # Sets the grid size, so some tiles can be placed more freely than others.
   }elseif( Type == "pole" ){
       holoModel(0,"block")
       holoScale(0,vec(Scale*0.5,Scale*0.5,Scale*4))
       Gx = Gy = Scale*12*2
       Gz = Scale*12*4
       Z = Z + Scale*12*2
   }elseif( Type == "wall" ){
       holoModel(0,"block")
       holoScale(0,vec(Scale*4,Scale*0.25,Scale*4))
        holoAng(0,ang(0,ANGOFF,0))                                      # Sets the angle of the preview, so you can rotate tiles before placing them.
       Gx = Gy = Scale*12*2
       Gz = Scale*12*4
       Z = Z + Scale*12*2
   }elseif( Type == "stair" ){
       holoModel(0,"block")
       holoScale(0,vec(Scale*3,Scale*3,Scale*0.5))
       holoAng(0,ang(0,0,30))
       Gx = Gy = Scale*12*4
       Gz = Scale*12*4
       Z = Z + Scale*12*2
        }elseif( Type == "door" ){
       holoModel(0,"block")
       holoScale(0,vec(Scale*0.5,Scale*3,Scale*3))
       Gx = Gy = Scale*12*2
       Gz = Scale*12*4
       Z = Z + Scale*12*2
        }elseif( Type == "door2" ){
       holoModel(0,"block")
       holoScale(0,vec(Scale*0.5,Scale*3,Scale*3))
       holoAng(0,ang(0,90,0))
       Gx = Gy = Scale*12*2
       Gz = Scale*12*4
       Z = Z + Scale*12*2
   }elseif( Type == "window" ){
       holoModel(0,"block")
       holoScale(0,vec(Scale*0.5,Scale*2,Scale*2))
       Gx = Gy = Scale*12*2
       Gz = Scale*12*4
       Z = Z + Scale*12*2
   }elseif( Type == "window2" ){
       holoModel(0,"block")
       holoScale(0,vec(Scale*0.5,Scale*2,Scale*2))
       holoAng(0,ang(0,90,0))
       Gx = Gy = Scale*12*2
       Gz = Scale*12*4
       Z = Z + Scale*12*2
   }elseif( Type == "ramp" ){
       holoModel(0,"block")
       holoScale(0,vec(Scale*0.5,Scale*5,Scale*0.5))
       holoAng(0,ang(0,ANGOFF,45))
       Gx = Gy = Scale*12
       Gz = Scale*12*4
       Z = Z + Scale*12*2
   }elseif( Type == "filler" ){
       holoModel(0,"block")
       holoScale(0,vec(Scale*0.5,Scale*4,Scale*1))
       holoAng(0,ang(0,ANGOFF,0))
       Gx = Gy = Scale*12
       Gz = Scale*12*2
       Z = Z + Scale*12*0.5
   }
   
   # Set the holographic position after the respective values have been altered.
   holoPos(0,vec(X,Y,Z))
   
}
 
# Sets the message shown in the chips information-popup.
setName("RUST Building Tool by GlitchDetector
Used by "+owner():name() + "
 
Current tool: "+Type+"")
 
# Checks if the player is holding down a key, used so you cannot execute two actions in a row without releasing a key.
Holding = ( owner():keyReload() || owner():keyAttack1() || owner():keyAttack2() || owner():keyUse() )
