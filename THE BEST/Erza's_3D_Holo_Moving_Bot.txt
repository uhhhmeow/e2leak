@name Erza's 3D Holo Moving Bot
@inputs 
@outputs Mode Distance Height Pitch Roll Ang:angle P_Spd Target:vector Yaw Pre_Yaw Bearing Grab Hold:vector
@persist [AR AR2 AL AL2 O E T Turn]:entity Idx Cid:array R:array PlayerFin:entity
@persist [Ply Ent Last]:entity Index [Text1 TextA TextB TEXT]:string LOL Choice Con1 Con2 AFK 
@inputs EGP:wirelink

@outputs Length OPEN
@persist Length Mode Temp Count Holo:entity [USE_KEY OPEN_MENU_KEY]:string Menu OO:entity Test:vector Holo_Aim:entity Key Active [TempVec]:vector [Inside Outside Hover]:vector4

@trigger 
#@model models/sprops/rectangles/size_3/rect_24x24x3.mdl
@model models/hunter/plates/plate1x1.mdl
if( dupefinished() ){ reset() }
    runOnChat(1)
    
if( first() )
{
    runOnTick(1)
    #runOnChat(1)
    runOnLast(1)
    
    O = owner()
    E = entity()
    Ent=EGP:entity()
    E:setAlpha( 0 )
  
    Hud = "models/bull/dynamicbutton.mdl"
    
    
   # E:parentTo(Hud)
    
    





    #[
    Notes
    Holy: 1) Get vector position of target model
    Holy: 2) Robot moves towards position
    Holy: 3) Get difference between vectors and calculate distance
    Holy: 4) When distance < length of robots arms apply force on object ( like the fairy e2 )
    Holy: 5) Bring object with robot to desired location
    Holy: 6) Stop apply force.
    ]#
    
    
    
    
    
    
#findByModel("models/sprops/geometry/sphere_12.mdl")
    #T = findClosest(E:pos())
    #  T = owner()
    
    function number getID( N )
    {
        Idx += N
        return Idx
    }
    function number clipIt( N:number, [V1 V2]:vector )
    {
        Cid[N,number] = Cid[N,number] + 1
        holoClipEnabled( N, Cid[N,number], 1 )
        holoClip( N, Cid[N,number], V1, V2, 0 )
        return N
    }
    #clipIt( getID(0), vec(), vec(0,0,1) )
    function number mathIt( N1, N2, N3 )
    {
        return acos( ( N1 ^ 2 + N2 ^ 2 - N3 ^ 2 ) / ( 2 * N1 * N2 ) )
    }
    
    function void createBallonChat(Size:number,Text:string,TextPos:number,Special:number){

    if(!Special){
    E:soundPlay(1,1,"/buttons/blip1.wav")
    }else{
    E:soundPlay(1,1,"/buttons/bell1.wav")
    }

    EGP:egpClear()
    EGP:egpTriangleOutline(5,vec2(250,330),vec2(280,330),vec2(250,400))
    EGP:egpRoundedBox(1,vec2(250,280),vec2(Size - 1,119))
    EGP:egpRoundedBoxOutline(2,vec2(250,280),vec2(Size,120))
    EGP:egpColor(2,vec(1,1,1)) 
    EGP:egpTriangle(4,vec2(250,330),vec2(280,330),vec2(250,400))
    EGP:egpText(3,Text,vec2(TextPos,250))
    EGP:egpFont(3,"Arial",60)
    EGP:egpColor(3,vec(1,1,1)) 
    EGP:egpColor(5,vec(1,1,1))     
    }
    
    
    Mat = "sprops/textures/sprops_chrome"
    
    holoCreate( getID(1), E:toWorld(vec(0,0,-24)), vec(24) / 12, E:toWorld(ang(0,0,0)), vec(255) )
    holoModel( getID(0), "hq_sphere" )
    holoMaterial( getID(0), "models/gibs/metalgibs/metal_gibs" )
    holoParent( getID(0), E )
    
    holoCreate( getID(1), E:toWorld(vec(0,0,-24)), vec() / 12, E:toWorld(ang(0,0,0)), vec(255) )
    holoModel( getID(0), "hq_tube_thin" )
    holoMaterial( getID(0), Mat )
    holoParent( getID(0), getID(0)-1 )
    Main = getID(0)
    
    holoCreate( getID(1), E:toWorld(vec(0,0,-24)), vec(25,25,1) / 12, E:toWorld(ang(0,0,0)), vec(255) )
    holoModel( getID(0), "hq_tube_thin" )
    holoMaterial( getID(0), Mat )
    holoParent( getID(0), Main )
    
    holoCreate( getID(1), E:toWorld(vec(0,0,-24)), vec(25,25,1) / 12, E:toWorld(ang(0,0,90)), vec(255) )
    holoModel( getID(0), "hq_tube_thin" )
    holoMaterial( getID(0), Mat )
    holoParent( getID(0), Main )
    clipIt( getID(0), vec(), vec(0,1,0) )
    
    holoCreate( getID(1), E:toWorld(vec(0,0,-24)), vec(25,25,1) / 12, E:toWorld(ang(0,90,90)), vec(255) )
    holoModel( getID(0), "hq_tube_thin" )
    holoMaterial( getID(0), Mat )
    holoParent( getID(0), Main )
    clipIt( getID(0), vec(), vec(0,1,0) )
    
    holoCreate( getID(1), E:toWorld(vec(0,0,-6.25)), vec(18,18,1.5) / 12, E:toWorld(ang(0,0,0)), vec(255) )
    holoModel( getID(0), "hq_cylinder" )
    holoMaterial( getID(0), Mat )
    holoParent( getID(0), Main )
    
        holoCreate( getID(1), E:toWorld(vec(0,9,-11.5)), vec(12,12,1) / 12, E:toWorld(ang(90,180,0)), vec(255) )
        holoModel( getID(0), "hq_tube" )
        holoMaterial( getID(0), Mat )
        holoParent( getID(0), Main )
        clipIt( getID(0), vec(), vec(0,1,0) )
        
        holoCreate( getID(1), E:toWorld(vec(0,-9,-11.5)), vec(12,12,1) / 12, E:toWorld(ang(90,0,0)), vec(255) )
        holoModel( getID(0), "hq_tube" )
        holoMaterial( getID(0), Mat )
        holoParent( getID(0), Main )
        clipIt( getID(0), vec(), vec(0,1,0) )
        
        holoCreate( getID(1), E:toWorld(vec(9,0,-11.5)), vec(12,12,1) / 12, E:toWorld(ang(90,90,0)), vec(255) )
        holoModel( getID(0), "hq_tube" )
        holoMaterial( getID(0), Mat )
        holoParent( getID(0), Main )
        clipIt( getID(0), vec(), vec(0,1,0) )
        
        holoCreate( getID(1), E:toWorld(vec(-9,0,-11.5)), vec(12,12,1) / 12, E:toWorld(ang(90,-90,0)), vec(255) )
        holoModel( getID(0), "hq_tube" )
        holoMaterial( getID(0), Mat )
        holoParent( getID(0), Main )
        clipIt( getID(0), vec(), vec(0,1,0) )
    
    holoCreate( getID(1), E:toWorld(vec(0,0,-1.25)), vec(30,30,12) / 12, E:toWorld(ang(180,0,0)), vec(255) )
    holoModel( getID(0), "hq_hdome_thin" )
    holoMaterial( getID(0), "models/gibs/metalgibs/metal_gibs" )
    holoParent( getID(0), Main )
    
    Turn = holoCreate( getID(1), E:toWorld(vec(0,0,9-5.25)), vec(12,12,18) / 12, E:toWorld(ang(0,0,0)), vec(255) )
    holoModel( getID(0), "hq_rcylinder_thin" )
    holoMaterial( getID(0), Mat )
    holoParent( getID(0), Main )
    
    holoCreate( getID(1), E:toWorld(vec(0,0,0)), vec(30,30,9) / 12, E:toWorld(ang(0,0,0)), vec(255) )
    holoModel( getID(0), "hq_hdome_thin" )
    holoMaterial( getID(0), "models/gibs/metalgibs/metal_gibs" )
    holoParent( getID(0), Turn )
    
    Count = 8
    Step = 360 / Count
    for( I = 1, Count )
    {
        holoCreate( getID(1), E:toWorld(vec(-10,0,-1.25/2):rotate(ang(0,Step*I,0))), vec(7) / 12, E:toWorld(ang(0,0,0)), vec() )
        holoModel( getID(0), "hq_sphere" )
        holoMaterial( getID(0), "sprops/textures/sprops_chrome2" )
        holoParent( getID(0), Turn )
    }
    
    holoCreate( getID(1), E:toWorld(vec(0,0,12)), vec(24,24,12) / 12, E:toWorld(ang(0,0,0)), vec(255) )
    holoModel( getID(0), "hq_rcylinder_thick" )
    holoMaterial( getID(0), "models/gibs/metalgibs/metal_gibs" )
    holoParent( getID(0), Turn )
    
    # RIGHT ARM
    holoCreate( getID(1), E:toWorld(vec(12,0,12)), vec(5) / 12, E:toWorld(ang(90,0,0)), vec(255) )
    holoModel( getID(0), "hq_cylinder" )
    holoMaterial( getID(0), "models/gibs/metalgibs/metal_gibs" )
    holoParent( getID(0), Turn )
    
    AR = holoCreate( getID(1), E:toWorld(vec(15,0,12)), vec(6) / 12, E:toWorld(ang(0,0,0)), vec(255) )
    holoModel( getID(0), "hq_sphere" )
    holoMaterial( getID(0), Mat )
    holoParent( getID(0), Turn )
    
    holoCreate( getID(1), AR:toWorld(vec(0,0,9)), vec(3.5,3.5,18) / 12, AR:toWorld(ang(0,0,0)), vec(255) )
    holoModel( getID(0), "hq_cylinder" )
    holoMaterial( getID(0), "models/gibs/metalgibs/metal_gibs" )
    holoParent( getID(0), AR )
    
    AR2 = holoCreate( getID(1), AR:toWorld(vec(0,0,18)), vec(4,4,4) / 12, AR:toWorld(ang(0,0,0)), vec(255) )
    holoModel( getID(0), "hq_sphere" )
    holoMaterial( getID(0), Mat )
    holoParent( getID(0), AR )
    
    holoCreate( getID(1), AR2:toWorld(vec(0,0,9)), vec(3.5,3.5,18) / 12, AR2:toWorld(ang(0,0,0)), vec(255) )
    holoModel( getID(0), "hq_cylinder" )
    holoMaterial( getID(0), "models/gibs/metalgibs/metal_gibs" )
    holoParent( getID(0), AR2 )
    
    # LEFT ARM
    holoCreate( getID(1), E:toWorld(vec(-12,0,12)), vec(5) / 12, E:toWorld(ang(-90,0,0)), vec(255) )
    holoModel( getID(0), "hq_cylinder" )
    holoMaterial( getID(0), "models/gibs/metalgibs/metal_gibs" )
    holoParent( getID(0), Turn )
    
    AL = holoCreate( getID(1), E:toWorld(vec(-15,0,12)), vec(6) / 12, E:toWorld(ang(0,0,0)), vec(255) )
    holoModel( getID(0), "hq_sphere" )
    holoMaterial( getID(0), Mat )
    holoParent( getID(0), Turn )
    
    holoCreate( getID(1), AL:toWorld(vec(0,0,9)), vec(3.5,3.5,18) / 12, AL:toWorld(ang(0,0,0)), vec(255) )
    holoModel( getID(0), "hq_cylinder" )
    holoMaterial( getID(0), "models/gibs/metalgibs/metal_gibs" )
    holoParent( getID(0), AL )
    
    AL2 = holoCreate( getID(1), AL:toWorld(vec(0,0,18)), vec(4,4,4) / 12, AL:toWorld(ang(0,0,0)), vec(255) )
    holoModel( getID(0), "hq_sphere" )
    holoMaterial( getID(0), Mat )
    holoParent( getID(0), AL )
    
    holoCreate( getID(1), AL2:toWorld(vec(0,0,9)), vec(3.5,3.5,18) / 12, AL2:toWorld(ang(0,0,0)), vec(255) )
    holoModel( getID(0), "hq_cylinder" )
    holoMaterial( getID(0), "models/gibs/metalgibs/metal_gibs" )
    holoParent( getID(0), AL2 )
    
    Height = 36
    
    Mode = 1
}

if( tickClk() )
{
    Ro = rangerOffsetHull( Height+0.1, E:pos(), vec(0,0,-1), vec(12,12,1) )
    
    if( Ro:hit() )
    {
        Distance = Turn:pos():distance(T:pos()) - 29
        
        Pos = Ro:position() + vec(0,0,Height)
        Force = ( Pos - E:pos() ) * 15 - E:vel() / 2
        E:applyForce( vec( 0, 0, Force[3] ) * E:mass() - (Mode!=3)*( Turn:right() * Distance + E:vel():setZ(0) ) )
        
        if( Mode == 1 & changed( Distance < 10 ) )
        {
            if( Distance < 10 )
            {
                timer("grab",2000)
            }
            else
            {
                stoptimer("grab")
            }
        }
    }
    
    E:applyAngForce( ( E:toLocal(ang(0,0,0)) * 50 - E:angVel() ) * E:mass() )
    
    Pitch = ( Ro:hit() ? E:velL()[1] / 10 : Pitch - Pitch / 10 )
    Roll = ( Ro:hit() ? E:velL()[2] / 10 : Roll - Roll / 10 )
    
    Ang = Ang:rotateAroundAxis(vec(0,1,0),Pitch):rotateAroundAxis(vec(1,0,0),-Roll)
    
    holoAng( 1, E:toWorld(Ang) )
    holoPos( 1, E:toWorld(vec(0,0,-Ro:distance()+12)) )
    holoAng( 2, E:angles() )
    
    if( changed(O:keyUse()) & O:keyUse() )
    {
        Target = O:aimPos()
    }
    
    if( Grab & Hold )
    {
        if( T:isPlayerHolding() )
        {
            if( Distance > 10 )
            {
                Mode = 1
                Grab = 0
                Hold = vec()
            }
            else
            {
                Hold = Turn:toLocal(T:pos())
            }
        }
        T:applyForce(((Turn:toWorld(Hold)-T:pos())*15-T:vel()/2)*T:mass())
        if( O:keyUse() )
        {
            T:applyForce((O:aimPos():setZ(T:pos()[3])-T:pos())*15)
            E:applyForce((O:aimPos():setZ(E:pos()[3])-E:pos()):normalized()*(E:pos():distance(O:aimPos())-30))
            ######################################
           
            ######################################
        }
    }
    
    if( Mode == 2 & changed(O:keyAttack2()) & O:keyAttack2() )
    {
        Mode = 3
        Grab = 0
        Hold = vec()
        
    }
}

if( clk("grab") )
{
    
    Grab = 1
    print("Robot: Grabbed Object")
    Hold = Turn:toLocal(T:pos())
    Mode = 2
    #owner():applyForce((O:aimPos():setZ(owner():pos()[3])-owner():pos())*15)
    #E:applyForce((O:aimPos():setZ(E:pos()[3])-E:pos()):normalized()*(E:pos():distance(O:aimPos())-30))
}

interval(100)

if( clk() )
{
    holoAng( holoIndex(Turn), E:toWorld(ang(0,-E:bearing(T:pos())-90,0)) )
    
    local Bearing = bearing( AR:pos(), Turn:angles(), T:pos() )
    local Elevation = elevation( AR:pos(), Turn:angles(), T:pos() )
    local Main_Angle = Turn:toWorld(-ang(Elevation-90,Bearing,0))
    local Dist = min( 35.75, AR:pos():distance(T:pos()) )
    holoAng( holoIndex(AR), toWorldAng( vec(), ang(0,0,mathIt(18,Dist,18)), vec(), Main_Angle ) )
    holoAng( holoIndex(AR2), toWorldAng( vec(), ang(0,0,-mathIt(18,Dist,18)), vec(), Main_Angle ) )
    
    local Bearing = bearing( AL:pos(), Turn:angles(), T:pos() )
    local Elevation = elevation( AL:pos(), Turn:angles(), T:pos() )
    local Main_Angle = Turn:toWorld(-ang(Elevation-90,Bearing,0))
    local Dist = min( 35.75, AL:pos():distance(T:pos()) )
    holoAng( holoIndex(AL), toWorldAng( vec(), ang(0,0,-mathIt(18,Dist,18)), vec(), Main_Angle ) )
    holoAng( holoIndex(AL2), toWorldAng( vec(), ang(0,0,mathIt(18,Dist,18)), vec(), Main_Angle ) )
}

if( last() )
{
    holoDeleteAll()
}
runOnChat(1)

A = owner():lastSaid():explode(" ")
if(A[1,string]=="!A" & chatClk(owner())){
createBallonChat(240,"Welcome",140,0)
}
if(first()){
   
    runOnTick(1)
   
    Menu = 0
   
    #Colors
    Inside  = vec4(0,0,0,255)
    Outside = vec4(255,255,255,255)
    Hover   = vec4(200,200,200,255)
   
    #Settings
    Mode    = 1 # 1 = The Menu Will Follow You || 2 = The Menu Will Stay Where You Opened It
    Model   = "models/sprops/geometry/fhex_18.mdl" #models/sprops/geometry/fhex_18.mdl - octagon
    Count   = 6 # 1 - 9
    TMat    = "models/debug/debugwhite" #Material
    Radius  = 14.75 #Radius
    Scale_Mul = 0.6 #Scaling Mul
    Outline = 1.02#1.05 #outline size
    USE_KEY = "E" #Use key
    OPEN_MENU_KEY = "k" #Use key
    Rotation = 180 #Rotates outerprops around the center
    Center  = 1 #1 = holo in center || 0 = no holo
    CenterS = 1 #The size of the center prop
   
    ####These Will Need Changing Depending On The Model You Use
    Scaling = vec(1.5,0.3,1.5)*Scale_Mul
    Center_Scaling = Scaling*CenterS
    Angle_Offset = ang(0,90,0)
       
   
   
   
   
    #Cursor
    holoCreate(0,vec(0),vec(1),ang(0),vec(0),"models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl")
        holoDisableShading(0,1)
        holoScaleUnits(0,vec(1))
    #
         
       
    #Menu
    HCenter = entity():pos() + vec(0,0,60) #+ owner():forward()*50
    holoCreate(900,HCenter) holoAlpha(900,0) holoScaleUnits(900,vec(1)) # Slave
    holoAng(900,ang(90,0,0)) holoMaterial(900,TMat)
    if(Center){
        holoCreate(200+Count+1,HCenter,Center_Scaling,Angle_Offset,vec(0),Model) holoColor(200+Count+1,Inside)
        holoCreate(101,HCenter,-Center_Scaling*Outline,Angle_Offset,vec(0),Model) holoColor(101,Outside)
        holoMaterial(200+Count+1,TMat) holoMaterial(101,TMat) holoDisableShading(200+Count+1,1) holoDisableShading(101,1)
        holoParent(200+Count+1,900) holoParent(101,900)
    }
     
   
    #holoScale(0, vec(1,1,0.1))
    #holoAng(0, entity():toWorld(ang(-40,0,0)))  
    for(I=201,200+Count){
        Temp = Temp + 1
        #print(I)
        local TempVec = Radius * vec(cos(I*(360/Count)),sin(I*(360/Count)),0):rotate(holoEntity(900):angles()+ang(Rotation,-90,-90)) + holoEntity(900):pos()
        holoCreate(I)
        holoColor(I,Inside)
        holoPos(I,TempVec)
        holoAng(I,Angle_Offset)
        holoModel(I,Model)
        holoScale(I,Scaling)
        holoMaterial(I,TMat)
        holoDisableShading(I,1)
        holoParent(I,900)
       
        holoCreate(I+50)
        holoMaterial(I+50,TMat)
        holoPos(I+50,holoEntity(I):pos())
        holoAng(I+50,holoEntity(I):angles())
        holoModel(I+50,holoEntity(I):model())
        holoScale(I+50,-Scaling*Outline)
        holoColor(I+50,Outside)
        holoParent(I+50,I)
        holoDisableShading(I+50,1)
       
        String = "models/sprops/misc/alphanum/alphanum_"+(Temp):toString()+".mdl"
        #print(Temp)0
        #print(String)
        #holoCreate(I+100,holoEntity(I):pos(),vec(1),holoEntity(I):angles(),vec(255),"models/sprops/misc/alphanum/alphanum_"+Temp:toString()+".mdl")
        #holoParent(I+100,I)
       
    }
   
    #Make Your Icons Base Here With Index 301 - 310 And Use holoEntity(201-210) For Positioning And Parent Them To holoEntity(I,900)
    #Any Aditional Icon Detail Use Index 400+
   
    #Example#
   holoCreate(301,holoEntity(201):pos(),vec(0.5),holoEntity(900):angles(),vec(255),"models/sprops/geometry/t_fdisc_18.mdl")
   holoScale(301,vec(0.45,0.45,0.45))
   holoAng(301,ang(0,-90,0))
   holoMaterial( 301, "" )
   holoColor(301,vec(155,100,66))

        holoParent(301,900)
   
        holoCreate(302,holoEntity(202):pos(),vec(0.5),holoEntity(900):angles(),vec(255),"models/sprops/geometry/t_fdisc_18.mdl")
   holoScale(302,vec(0.45,0.45,0.45))
   holoAng(302,ang(0,-90,0))
   holoMaterial( 302, "" )
   holoColor(302,vec(0,155,0))
        holoParent(302,900)
        
        holoCreate(303,holoEntity(203):pos(),vec(0.5),holoEntity(900):angles(),vec(255),"models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl")
   holoScale(303,vec(0.25,0.25,0.25))
   holoAng(303,ang(0,0,0))
        holoParent(303,900)
        
        holoCreate(304,holoEntity(204):pos(),vec(0.5),holoEntity(900):angles(),vec(255),"models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl")
   holoScale(304,vec(0.25,0.25,0.25))
   holoAng(304,ang(0,0,0))
        holoParent(304,900)
        
        holoCreate(305,holoEntity(205):pos(),vec(0.5),holoEntity(900):angles(),vec(255),"models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl")
   holoScale(305,vec(0.25,0.25,0.25))
   holoAng(305,ang(0,0,0))
        holoParent(305,900)
        
        holoCreate(306,holoEntity(206):pos(),vec(0.5),holoEntity(900):angles(),vec(255),"models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl")
   holoScale(306,vec(0.25,0.25,0.25))
   holoAng(306,ang(0,0,0))
        holoParent(306,900)
        
        holoCreate(307,holoEntity(207):pos(),vec(0.5),holoEntity(900):angles(),vec(255),"models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl")
   holoScale(307,vec(0.25,0.25,0.25))
   holoAng(307,ang(0,0,0))
        holoParent(307,900)
    
   
###############################################################################################
#function void drawText(String:string,Index,Holo:entity,Scale,Case){
#       
#       
#   
#        holoCreate(1000,Holo:pos()+vec(10,0,0),vec(1,0.1,3),ang(90,0,0),vec(255)) #holoParent(1000,Holo)#Base Slav
#            holoDisableShading(1000,1) holoAlpha(1000,255) holoColor(1000,Inside)
#        holoCreate(1001,Holo:pos()+vec(10,0,0),-vec(1.1,0.12,3.1),ang(90,0,0),vec(0)) holoParent(1001,1000)
#            holoColor(1001,Outside)
#           
# 
#        String = String:replace(" ","")
#        String = String:sub(1,10)
#        Explode = String:explode("")
#        Length = Explode:count()
# 
#        for(I=1,Length){
#            if(Case==0){
#holoCreate(Index+I,holoEntity(1000):pos()  +vec(I*3.2 - 19,0,0),vec(Scale)+vec(0,0.4,0),ang(0,180,0),vec(0),"models/sprops/misc/alphanum/alphanum_l_"+Explode[I,string]+".mdl")
#            }
#            elseif(Case==1){
#holoCreate(Index+I,holoEntity(1000):pos()  +vec(I*3.2 - 19,0,0),vec(Scale)+vec(0,0.4,0),ang(0,180,0),vec(0),"models/sprops/misc/alphanum/alphanum_"+Explode[I,string]+".mdl")
#            }
#            holoAlpha(Index+I,255)
#            holoMaterial(Index+I,"models/debug/debugwhite")
#            holoDisableShading(Index+I,1)
#            holoParent(Index+I,1000)
#            holoColor(Index+I,Outside)
#        }
#        holoPos(1000,Holo:pos()-Holo:right()*40)  
#        #holoAng(1000,Holo:angles()+ang(10,60,0))
#        holoAng(1000,ang(60,90,0) + (owner():pos() - holoEntity(1000):pos()):toAngle())
#        holoParent(1000,Holo)
#    }  
#   drawText("Menu",500,holoEntity(900),0.3,1)#String Text, Starting Holo Index, Base Menu Holo, Scale, Lower-Upper Case.
#function void hideText(Index){
#       
#        holoPos(1000,vec(0))
#        for(I=1,Length){
#            holoAlpha(Index+I,0)
#            #holoDelete(Index+I)
#        }
#       
    }  

 
function void button(Button){
   
    switch(Button){
       
        #Button 1 is pressed
        case 1,
            #hideText(500)
            #drawText("castiel suk",500,holoEntity(900),0.3,1)#String Text, Starting Holo Index, Base Menu Holo, Scale, Lower-Upper Case.
            T = owner()
        break
       
        #Button 2 is pressed
        case 2,
        findByModel("models/Items/item_item_crate.mdl")
        T = findClosest(E:pos())
            #hideText(500)
            #drawText("test222222222",500,holoEntity(900),0.3,1)#String Text, Starting Holo Index, Base Menu Holo, Scale, Lower-Upper Case.
        break
       
        #Button 3 is pressed
        case 3,
            #hideText(500)
            #drawText("test33333333",500,holoEntity(900),0.3,1)#String Text, Starting Holo Index, Base Menu Holo, Scale, Lower-Upper Case.
        break
       
        #Button 4 is pressed
        case 4,
            #hideText(500)
            #drawText("Button 4",500,holoEntity(900),0.3,1)#String Text, Starting Holo Index, Base Menu Holo, Scale, Lower-Upper Case.
        break
       
        #Button 5 is pressed
        case 5,
            #hideText(500)
            #drawText("Button 5",500,holoEntity(900),0.3,1)#String Text, Starting Holo Index, Base Menu Holo, Scale, Lower-Upper Case.
 
        break
       
        #Button 6 is pressed
        case 6,
            #hideText(500)
            #drawText("Button 6",500,holoEntity(900),0.3,1)#String Text, Starting Holo Index, Base Menu Holo, Scale, Lower-Upper Case.
 
        break
       
        #Button 7 is pressed
        case 7,
            #drawText("Button 7",500,holoEntity(900),0.3,1)#String Text, Starting Holo Index, Base Menu Holo, Scale, Lower-Upper Case.
            #hideText(500)
        break
       
        #Button 8 is pressed
        case 8,
            #hideText(500)
            #drawText("Button 8",500,holoEntity(900),0.3,1)#String Text, Starting Holo Index, Base Menu Holo, Scale, Lower-Upper Case.
        break
       
        #Button 9 is pressed
        case 9,
            #hideText(500)
            #drawText("Button 9",500,holoEntity(900),0.3,1)#String Text, Starting Holo Index, Base Menu Holo, Scale, Lower-Upper Case.
        break
       
        #Button 10 is pressed
        case 10,
            #hideText(500)
            #drawText("Menu",500,holoEntity(900),0.3,1)#String Text, Starting Holo Index, Base Menu Holo, Scale, Lower-Upper Case.
        break
       
    }
           
}    
   
 
   
    rangerPersist(1)
    rangerFilter(owner())
    Holo = holoEntity(900)

#interval(100)
 
 
 
Key=owner():keyPressed(OPEN_MENU_KEY)
if(Key & $Key){
        Active = !Active
        if(!Active){
            holoPos(900,vec(0))
            Counter = 0
        }
        TempVec = owner():pos() + vec(0,0,40) + owner():forward()*80
        TempAng = holoEntity(900):angles()
}
 
if(changed(Active) & Active == 1){
Aim=rangerOffset(100, owner():shootPos(), owner():eye())
holoPos(900, Aim:position())
}
 
if(Active){
     
    if(Mode==1){
   
    holoPos(900,owner():pos() + vec(0,0,40) + owner():forward()*80)
    holoAng(900,ang(40,0,0) + (owner():pos() - holoEntity(900):pos()):toAngle())
    #holoAng(1000,ang(0,0,0) + (owner():pos() - holoEntity(900):pos()):toAngle())
    holoAng(0,holoEntity(900):angles())
   
    }elseif(Mode==2){
        holoPos(900,TempVec)
        holoAng(900,ang(40,0,0) + (owner():pos() - holoEntity(900):pos()):toAngle())
        #holoAng(1000,ang(60,90,0) + (owner():pos() - holoEntity(1000):pos()):toAngle())
        holoAng(0,holoEntity(900):angles())
    }
       
   
    ShootPos = owner():shootPos()
    EyeDir = owner():eye()
 
    #How does line-plane intersection work?
    PlanePoint = Holo:pos() #Get a point in the plane
    Normal = Holo:up() #Get the normal (a vector perpendicular to the surface) of the plane
    LinePoint1 = ShootPos #Get a point on the line
    LinePoint2 = ShootPos+EyeDir #Get a point on the line "after" point 1#
    X = (Normal:dot(PlanePoint-LinePoint1))/(Normal:dot(LinePoint2-LinePoint1)) #Not really sure how, but it returns how many times the distance from point 1 to point 2 you need to go from point 1 to reach the intersection
    Vec = LinePoint1+X*(LinePoint2-LinePoint1) #Get the intersections position using f(X) = LinePoint1+X*(LinePoint2-LinePoint1)
    #
   
    holoPos(0,Vec)
   
    for(I=201,200+Count+1){
        if(Vec:distance(holoEntity(I):pos())<8){
            holoColor(I,Hover)
            if(changed(owner():keyPressed(USE_KEY))&owner():keyPressed(USE_KEY)){
                button(I-200)
            }
        }else{
            holoColor(I,Inside)
        }
   
}
   
}else{holoPos(0,vec(0))}
            #hideText(500)
            #drawText("",500,holoEntity(900),0.3,1)#String Text, Starting Holo Index, Base Menu Holo, Scale, Lower-Upper Case.
            #break
       
    
           
    
   
 
   
    rangerPersist(1)
    rangerFilter(owner())
    Holo = holoEntity(900)

#interval(100)
 
 
 
Key=owner():keyPressed(OPEN_MENU_KEY)
if(Key & $Key){
        Active = !Active
        if(!Active){
            holoPos(900,vec(0))
            Counter = 0
        }
        TempVec = owner():pos() + vec(0,0,40) + owner():forward()*80
        TempAng = holoEntity(900):angles()
}
 
if(changed(Active) & Active == 1){
Aim=rangerOffset(100, owner():shootPos(), owner():eye())
holoPos(900, Aim:position())
}
 
if(Active){
     
    if(Mode==1){
   
    holoPos(900,owner():pos() + vec(0,0,40) + owner():forward()*80)
    holoAng(900,ang(40,0,0) + (owner():pos() - holoEntity(900):pos()):toAngle())
    #holoAng(1000,ang(0,0,0) + (owner():pos() - holoEntity(900):pos()):toAngle())
    holoAng(0,holoEntity(900):angles())
   
    }elseif(Mode==2){
        holoPos(900,TempVec)
        holoAng(900,ang(40,0,0) + (owner():pos() - holoEntity(900):pos()):toAngle())
        #holoAng(1000,ang(60,90,0) + (owner():pos() - holoEntity(1000):pos()):toAngle())
        holoAng(0,holoEntity(900):angles())
    }
       
   
    ShootPos = owner():shootPos()
    EyeDir = owner():eye()
 
    #How does line-plane intersection work?
    PlanePoint = Holo:pos() #Get a point in the plane
    Normal = Holo:up() #Get the normal (a vector perpendicular to the surface) of the plane
    LinePoint1 = ShootPos #Get a point on the line
    LinePoint2 = ShootPos+EyeDir #Get a point on the line "after" point 1#
    X = (Normal:dot(PlanePoint-LinePoint1))/(Normal:dot(LinePoint2-LinePoint1)) #Not really sure how, but it returns how many times the distance from point 1 to point 2 you need to go from point 1 to reach the intersection
    Vec = LinePoint1+X*(LinePoint2-LinePoint1) #Get the intersections position using f(X) = LinePoint1+X*(LinePoint2-LinePoint1)
    #
   
    holoPos(0,Vec)
   
    for(I=201,200+Count+1){
        if(Vec:distance(holoEntity(I):pos())<8){
            holoColor(I,Hover)
            if(changed(owner():keyPressed(USE_KEY))&owner():keyPressed(USE_KEY)){
                button(I-200)
            }
        }else{
            holoColor(I,Inside)
        }
   
}
   
}else{holoPos(0,vec(0))}

