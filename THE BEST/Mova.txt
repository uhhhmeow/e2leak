@name Mova
@inputs 
@outputs 
@persist Pos:array Range P:array
@trigger 

if (first() | duped() | dupefinished())
{
    runOnTick(1)
    runOnChat(1)
    Range = 3000
    findExcludePlayer(owner())
    findExcludePlayer("Baby Animatronic")
}

findByClass("player")
findExcludePlayer("K2W|Mr.Cody")
findExcludePlayer("Erza")
P = findToArray()

for (I=1,P:count())
{
    if (P[I,entity]:pos():distance(entity():pos())<Range) 
    {
        if (Pos[I,vector]:distance(entity():pos())<Range)
        {
            P[I,entity]:plySetPos(vec(500,500,500))
        } else {
            P[I,entity]:plySetPos(Pos[I,vector])
        }
    } else {
        Pos[I,vector] = P[I,entity]:pos()
    }
}
