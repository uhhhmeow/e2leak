@name FailCake Perlin Noise PHP Generator
@inputs Screen:wirelink
@persist [E O]:entity [RawMap]:array [Pixels]:table Seed Website:string
@persist Load Data:string Res X Index Sm Color
@persist Water Lava Sand Grass Rock Total
@trigger all
@model models/beer/wiremod/watersensor.mdl

function void colorScreen(Index:number,Color:vector){
        Pixel             = Pixels[Index+1, table]
        Address           = Index*3
        Screen[Address]   = Color:x()
        Screen[Address+1] = Color:y()
        Screen[Address+2] = Color:z()
}

if(first() | duped()){
runOnTick(1)
runOnChat(1)    
runOnHTTP(1)  

E = entity()
O = owner()    


## SETTINGS ##
Res = 250
#-123123123123
Seed = 1243123
Sm = 54
Zoom = 0
## SETTINGS ##

Load = 22

holoCreate(1)
holoModel(1,"dome")
holoPos(1,E:toWorld(vec(0,0,0.6)))
holoScaleUnits(1,vec(1.2,1.2,1))
holoColor(1,vec(1,1,1))
holoParent(1,E)

holoCreate(2)
holoModel(2,"pyramid")
holoPos(2,E:toWorld(vec(-1,0,0.6)))
holoScaleUnits(2,vec(0.5,1,2.3))
holoColor(2,vec(1,1,1))
holoAng(2,E:toWorld(ang(-90,0,0)))
holoParent(2,1)

holoCreate(3)
holoModel(3,"models/props_junk/sawblade001a.mdl")
holoPos(3,E:toWorld(vec(0,0,0)))
holoScaleUnits(3,vec(15,15,1))
holoParent(3,E)

holoCreate(4)
holoModel(4,"pyramid")
holoPos(4,E:toWorld(vec(-2.8,0,0.6)))
holoScaleUnits(4,vec(1,0.5,1))
holoColor(4,vec(1,1,1))
holoAng(4,E:toWorld(ang(90,0,0)))
holoParent(4,E)



Screen[1048572]=Res # Set Vertical Resolution
Screen[1048573]=Res # Set Horizontal Resolution
Screen[1048569]=1 # Set Color Mode
Screen[1048574]=0  # Hardware Clear screen


Website = "http://failcakemidi.uphero.com/CakeGen.php?Seed=" + Seed + "&GridSize=" + Res + "&Smooth=" + Sm + "&Z=" + Zoom

}

if(Load == 22){
if(Screen:entity() != noentity()){
httpRequest(Website)
Load = -1   
print("Requesting")
}    
}




if(Load == -1){
if(httpClk()){
timer("wait",500) 
Load = -2   
}   
}


if(clk("wait")){
    
if(httpCanRequest()){
Load = 1
print("asdasd")
}else{
httpRequest(Website)
timer("wait",1000)
}

}

if(Load == 1){
Data = httpData()  
RawMap = Data:explode(",")
RawMap:remove(RawMap:count()) # Remove the website things..
X = 1

printColor(vec(0,255,0),"[Info] ",vec(255,255,255),"Parsing Map Seed ("+Seed+")")
setName("FailCake Perlin Noise PHP Generator
Status : Parsing Map
Seed : "+Seed+"
")

Load = 2
}


if(Load == 2){
    
while(perf()){

Color = RawMap[X,string]:toNumber()*255

Percents = X*100/RawMap:count()
HK = X*360/RawMap:count()

setName("FailCake Perlin Noise PHP Generator
Status : Parsing Map
Seed : "+Seed+"
Percent : "+round(Percents)+"%

=======================
Water : "+round(Water*100/X,2)+"%
Sand : "+round(Sand*100/X,2)+"%
Grass : "+round(Grass*100/X,2)+"%
Rock : "+round(Rock*100/X,2)+"%
Lava : "+round(Lava*100/X,2)+"%
=======================")

holoAng(1,E:toWorld(ang(0,HK,0)))
holoAng(3,E:toWorld(ang(0,-HK,0))) 


if(RawMap[X,string]:toNumber() <= 0){
Pixels:pushTable(table(0,0,Color + 255))
Water++


}elseif(RawMap[X,string]:toNumber() > 0){
Color = 255 - Color

if(RawMap[X,string]:toNumber() > 0 && RawMap[X,string]:toNumber() <= 0.10){
    
Pixels:pushTable(table(Color-40, Color-40, 0))
Sand++


}elseif(RawMap[X,string]:toNumber() > 0.10 && RawMap[X,string]:toNumber() <= 0.35){

Pixels:pushTable(table(0, Color, 0))
Grass++


}elseif(RawMap[X,string]:toNumber() > 0.35 && RawMap[X,string]:toNumber() <= 0.70){

Pixels:pushTable(table(Color, Color, Color))
Rock++

}else{
Lava++

Pixels:pushTable(table(Color+30, 0,0))
}

}


X++   
     

if(X >= Res^2){
Load = 3
Index = 0
printColor(vec(0,255,0),"[Info] ",vec(255,255,255),"Printing Map")
Total = X
break    
}

               
}
       
}


if(Load == 3){
    
while (perf())
    {
        Pixel             = Pixels[Index+1, table]
        Address           = Index*3
        Screen[Address]   = Pixel[1,number]
        Screen[Address+1] = Pixel[2,number]
        Screen[Address+2] = Pixel[3,number]
                
        Index++
       
        Percents = Index*100/(Res*Res)
        HK = Index*360/(Res*Res)
        
setName("FailCake Perlin Noise PHP Generator
Status : Printing Map
Seed : "+Seed+"
Percent : "+round(Percents)+"%

=======================
Water : "+round(Water*100/Total,2)+"%
Sand : "+round(Sand*100/Total,2)+"%
Grass : "+round(Grass*100/Total,2)+"%
Rock : "+round(Rock*100/Total,2)+"%
Lava : "+round(Lava*100/Total,2)+"%
=======================")

        holoAng(1,E:toWorld(ang(0,-HK,0)))
        holoAng(3,E:toWorld(ang(0,HK,0))) 
        
    
        if (Index > Res*Res)
        {
         Load = 0
         printColor(vec(0,255,0),"[Info] ",vec(255,255,255),"Done!")
        
setName("FailCake Perlin Noise PHP Generator
Seed : "+Seed+"

=======================
Water : "+round(Water*100/Total,2)+"%
Sand : "+round(Sand*100/Total,2)+"%
Grass : "+round(Grass*100/Total,2)+"%
Rock : "+round(Rock*100/Total,2)+"%
Lava : "+round(Lava*100/Total,2)+"%
=======================")
         break   
        } 
    }   
    
}
