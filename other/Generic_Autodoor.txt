@name Generic Autodoor
@inputs Door:entity
@persist Holder:entity
@outputs Open

interval(100)
A=gTable("Doorlist"):toTable()

if (Holder & Holder:keyUse()) {    
    Open=1
} else {
    Open=0
    Holder=noentity()
    findByClass("player")
    Ply=findClosest(entity():pos())
    if (A:exists(Ply:steamID()) & Ply:keyUse() & Ply:aimEntity()==Door) {
        Holder=Ply
    }
}
