@name UFOThing
@inputs Forward Back Left Right Up Down Fast
@persist Pos:vector Ang:angle Rot This:entity Speed Turbo
@outputs Angle
@trigger All
#include "lib/force/func"
if (first()) {
    This=entity():isWeldedTo()
    Ang=ang(0,0,0)
    Pos=This:pos()
    Rot=This:angles():yaw()
    Speed=2
    Turbo=0.5
}

if (Forward) {
    Pos=This:toWorld(vec(This:aabbSize():length()*Turbo,0,0))
} elseif (Back) {
    Pos=This:toWorld(vec(-This:aabbSize():length()*Turbo,0,0))
}

if (Left) {
    Ang=Ang+ang(0,Speed,0)
}
if (Right) {
    Ang=Ang-ang(0,Speed,0)
}
if (Ang:yaw()<-180) {
    Ang=ang(Ang:pitch(),180,Ang:roll())
} elseif (Ang:yaw()>180) {
    Ang=ang(Ang:pitch(),-180,Ang:roll())
}

if (Up) {
    Pos=Pos+vec(0,0,Speed)
} elseif (Down) {
    Pos=Pos-vec(0,0,Speed)
}
if (Fast) {Turbo=2} else {Turbo=0.5}
Angle=Ang:yaw()

This:move(Pos)
This:rot(Ang)

runOnTick(1)
