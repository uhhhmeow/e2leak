@name ParticleCore Test
@persist [Owner Entity]:entity

if(first()) {
    Owner=owner()
    Entity=entity()
    # particle(number Duration,number StartSize,number EndSize,string String,vector RGB,vector Position,vector Velocity,number Pitch)
    particleGravity(vec())
    
}
particle(15,16,16,"effects/spark",vec(255),Entity:pos(),randvec(vec(-500),vec(500)),0)
interval(1)
