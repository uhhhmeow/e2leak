@name sRadio 3
@persist [Songs Meta]:table [Owner Entity]:entity Current:table
@persist Group:string Master
@model models/props_lab/citizenradio.mdl
runOnChat(Master)

# Declarations
if(first()) {
    Group="sRadio3"
    
    dsJoinGroup(Group)
    local Global=gTable(Group)
    
    Owner=owner()
    Entity=entity()
    
    # Entity:removeNoCollideAll()
    
    Songs=table(
        # TF2
        array("More Gun",192,"/ui/gamestartup10.mp3"),
        array("Right Behind You",100,"/ui/gamestartup7.mp3"),
        array("A Little Heart to Hurt",95,"/ui/gamestartup11.mp3"),
        array("Team Fortress 2",72,"/ui/gamestartup1.mp3"),
        array("Playing With Danger",242,"/ui/gamestartup2.mp3"),
        array("Rocket Jump Waltz",39,"/ui/gamestartup3.mp3"),
        array("The Art of War",132,"/ui/gamestartup4.mp3"),
        array("Faster than a Speeding Bullet",85,"/ui/gamestartup5.mp3"),
        array("Petitee Chou-Fleur",102,"/ui/gamestartup6.mp3"),
        array("Intruder Alert",112,"/ui/gamestartup8.mp3"),
        array("Drunken Pipe Bomb",90,"/ui/gamestartup9.mp3"),
        array("MEDIC!",154,"/ui/gamestartup12.mp3"),
        array("A Little Heart to Hurt",95,"/ui/gamestartup11.mp3"),
        array("Still Alive",176,"/music/portal_still_alive.mp3"),
        array("Dreams of Cruelty",66,"/ui/gamestartup14.mp3"),
        array("The Calm",154,"/ui/gamestartup15.mp3"),
        array("ROBOTS",214,"/ui/gamestartup16.mp3"),
        array("Dapper Cadaver",118,"ui/gamestartup17.mp3"),
        array("Rise of the Living Bread",156,"ui/gamestartup18.mp3"),
        array("Red Bread",131,"ui/gamestartup19.mp3"),
        array("Three Days to Live",115,"ui/gamestartup20.mp3"),
        array("Seduce Me",79,"ui/gamestartup21.mp3"),
        array("Stink Lines",94,"ui/gamestartup22.mp3"),
        array("It Hates Me So Much",129,"ui/gamestartup23.mp3")
        
        #[
        # HL2
        array("CP Violation",46,"music/hl2_song20_submix0.mp3"),
        array("CP Violation (Remix)",63,"music/hl2_song20_submix4.mp3"),
        array("The Innsbruck Experiment",27,"music/hl2_song4.mp3"),
        array("Self Destruction",54,"music/vlvx_song12.mp3"),
        
        # Portal
        array("400 Degrees Kelvin",28,"music/portal_4000_degrees_kelvin.mp3")
        ]#
        
        
    )
    
    function string getTime(Num:number) {
        Min=floor(Num/60)
        Sec=Num-(Min*60)
        if (Sec>=10) {
            return ""+Min+":"+Sec
        } else {
            return ""+Min+":0"+Sec
        }
    }
    
    function number sr_receive(Data:array) {
        local Type=Data[1,string]
        switch(Type) {
            case "timer",
                Current["Timer",number]=Data[2,number]
            break
            
            case "start",
                Current=table(
                    "Name"=Data[2,string],
                    "Length"=Data[3,number],
                    "Timer"=Data[3,number],
                    "LengthStr"=getTime(Data[3,number]),
                    "Path"=Data[4,string],
                    "Active"=1
                )
                
                for(I=1,10) {
                    soundPlay(I,Data[3,number],Data[4,string])
                    soundVolume(I,1)
                }
            break
            
            case "end",
                Current["Active",number]=0
            break
        }
    }    
    
    function number sr_send(Data:array) {
        sr_receive(Data)
        dsSend(Group,Group,Data)
    }
    
    function number sr_playSongByName(Name:string) {
        local Name=Name:lower()
        for(I=1,Songs:count()) {
            local V=Songs[I,array]
            if(V[1,string]:lower():matchFirst(Name)) {
                printColor(vec(255,255,0),"[sRadio3] ",vec(255),"Playing ",vec(0,255,255),V[1,string],vec(255),".")
                sr_send(array("start"):add(V))
                stoptimer("master")
                stoptimer("newsong")
                timer("master",1000)
                return 1
            }
        }
        printColor(vec(255,255,0),"[sRadio3] ",vec(255),"Could not play song '",vec(255,0,0),Name,vec(255),"'.")
        return 0
    }
    
    if(!Global["Master",entity]:isValid() | Global["Master",entity]==Entity) {
        printColor(vec(255,255,0),"[sRadio3] ",vec(0,255,0),Entity:toString(),vec(255)," claimed ",vec(0,255,255),"master",vec(255)," status.")
        Global["Master",entity]=Entity
        Master=1
        timer("master",1000)
    }
}

# Master Iterators
if(clk("master")) {
    timer("master",1000)
    if(Current["Timer",number]>0) {
        sr_send(array("timer",Current["Timer",number]-1))
    } else {
        if(!Current["Active",number]) {
            sr_send(array("end"))
        }
        timer("newsong",3000)
    }
}

if(clk("newsong")) {
    sr_send(array("start"):add(Songs[randint(Songs:count()),array]))
    timer("master",1000)
}

# Receive Signal
if(dsClk()) {
    sr_receive(dsGetArray())
}

# Name formatter
setName(
    format("sRadio 3\n%s\n%s\n%s",
        Current["Active",number] ? Current["Name",string] : "Idle",
        getTime(Current["Timer",number]) + " / " + Current["LengthStr",string],
        Master ? "Master Station" : "Client Station"
    )
)

# Chat
if(Master & chatClk(Owner) & Owner:lastSaid():sub(1,1)=="-") {
    hideChat(1)
    local Args=Owner:lastSaid():sub(2):explode(" ")
    local Command=Args:shiftString()
        
    switch(Command) {
        case "play",
            sr_playSongByName(Args[1,string])
        break
    }
}





