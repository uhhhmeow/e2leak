@name sShopKeeper3
@persist [Owner Entity Hull ScrEnt User Keeper Block Nearest Radio]:entity Scr:wirelink
@persist [WeaponData Meta]:table
@persist [RED GREEN BLUE YELLOW CYAN WHITE GREY BLACK]:vector
@persist UseHeld UseTime UseStart:vector2 Tick XOffset OffsetXStart XOffsetGoal

interval(100)

Tick++
Color=hsv2rgb((Tick)%360,1,1)

UseHeld=User:keyUse()
UseStart=changed(UseHeld) ? Scr:egpCursor(User) : UseStart
OffsetXStart=changed(UseHeld) ? XOffset : OffsetXStart
UseTime=UseHeld ? UseTime+0.1 : 0
Use=UseTime<0.2 && !UseHeld && changed(UseHeld)

if(duped()) {selfDestructAll()}

if(first()) {
    runOnChat(1)
    runOnLast(1)

    # Definitions
    Owner=owner()
    Entity=entity()
    
    RED=vec(255,0,0)
    GREEN=vec(0,255,0)
    BLUE=vec(0,0,255)
    YELLOW=vec(255,255,0)
    CYAN=vec(0,255,255)
    WHITE=vec(255)
    GREY=vec(128)
    BLACK=vec(0)
    
    WeaponData=table(
        # GD shipments
        "fas2_ragingbull"=table(
            "Type"="fas2_ragingbull",
            "Name"="Raging Bull",
            "Model"="models/weapons/w_357.mdl",
            "Price"=3000/5,
            "SpawnStock"=5
        ),
        "fas2_p226"=table(
            "Type"="fas2_p226",
            "Name"="P226",
            "Model"="models/weapons/w_pist_p228.mdl",
            "Price"=2500/5,
            "SpawnStock"=5
        ),
        "fas2_ots33"=table(
            "Type"="fas2_ots33",
            "Name"="OTs-33",
            "Model"="models/weapons/world/pistols/ots33.mdl",
            "Price"=2500/5,
            "SpawnStock"=5
        ),
        "fas2_m1911"=table(
            "Type"="fas2_m1911",
            "Name"="M1911",
            "Model"="models/weapons/w_pistol.mdl",
            "Price"=2000/5,
            "SpawnStock"=5
        ),
        "fas2_deagle"=table(
            "Type"="fas2_deagle",
            "Name"="Desert Eagle",
            "Model"="models/weapons/w_pist_deagle.mdl",
            "Price"=3000/5,
            "SpawnStock"=5
        ),
        "fas2_glock20"=table(
            "Type"="fas2_glock20",
            "Name"="Glock 20",
            "Model"="models/weapons/w_pist_glock18.mdl",
            "Price"=1500/5,
            "SpawnStock"=5
        ),
        "fas2_m3s90"=table(
            "Type"="fas2_m3s90",
            "Name"="M3 Super 90",
            "Model"="models/weapons/w_shot_m3super90.mdl",
            "Price"=4500/5,
            "SpawnStock"=5
        ),
        
        # BMD Shipments
        "fas2_m24"=table(
            "Type"="fas2_m24",
            "Name"="M24",
            "Model"="models/weapons/w_snip_scout.mdl",
            "Price"=9000/5,
            "SpawnStock"=5
        ),
        "fas2_uzi"=table(
            "Type"="fas2_uzi",
            "Name"="IMI Uzi",
            "Model"="models/weapons/w_smg_mac10.mdl",
            "Price"=2500/5,
            "SpawnStock"=5
        ),
        "fas2_ak47"=table(
            "Type"="fas2_ak47",
            "Name"="AK-47",
            "Model"="models/weapons/w_rif_ak47.mdl",
            "Price"=5000/5,
            "SpawnStock"=5
        ),
        "fas2_g3"=table(
            "Type"="fas2_g3",
            "Name"="G3A3",
            "Model"="models/weapons/w_rif_galil.mdl",
            "Price"=7500/5,
            "SpawnStock"=5
        ),
        "fas2_mp5k"=table(
            "Type"="fas2_mp5k",
            "Name"="MP5K",
            "Model"="models/weapons/w_smg_mp5.mdl",
            "Price"=7500/5,
            "SpawnStock"=5
        ),
        "fas2_sks"=table(
            "Type"="fas2_sks",
            "Name"="SKS",
            "Model"="models/weapons/w_rif_ak47.mdl",
            "Price"=8000/5,
            "SpawnStock"=5
        ),
        "fas2_m14"=table(
            "Type"="fas2_m14",
            "Name"="M14",
            "Model"="models/weapons/w_snip_sg550.mdl",
            "Price"=10000/5,
            "SpawnStock"=5
        ),
        "lockpick"=table(
            "Type"="lockpick",
            "Name"="Lock Pick",
            "Model"="models/weapons/w_crowbar.mdl",
            "Price"=3000/10,
            "SpawnStock"=10
        )
    )
    
    ##include "lib/interstel/shipmentmetadata"
    
    #WeaponData=ShipmentMetaData
    
    Meta=table(
        "music"=array(
            "ambient/music/country_rock_am_radio_loop.wav",
            "ambient/music/cubanmusic1.wav",
            "ambient/music/flamenco.wav",
            "ambient/music/latin.wav",
            "ui/gamestartup21.mp3",
            "ui/gamestartup23.mp3"
        ),
        "greet"=array(
            "vo/npc/male01/hi01.wav",
            "vo/npc/male01/hi02.wav",
            "vo/npc/male01/heydoc01.wav",
            "vo/npc/male01/heydoc02.wav"
        )
    )
    
    # Init
    ScrEnt=entity():isWeldedTo()
    if(!ScrEnt | ScrEnt:type()!="gmod_wire_egp_emitter") {
        print(ScrEnt:type())
        printColor(YELLOW,"[sShopKeeper3] ",WHITE,"Please spawn the chip on an ",RED,"EGP emitter.")
        selfDestruct()
    } else {
        printColor(YELLOW,"[sShopKeeper3] ",GREEN,"Wire EGP Emitter ",WHITE,"linked.")
        Scr=ScrEnt:wirelink()
        Hull=propSpawn(
            "models/props/de_tides/vending_cart_base.mdl",
            ScrEnt:toWorld(vec(-28,0,18)),
            ScrEnt:toWorld(ang(0,180,0)),
            1
        )
        
        Block=propSpawn(
            "models/hunter/blocks/cube2x2x1.mdl",
            ScrEnt:toWorld(vec(-24,40,48)),
            ScrEnt:toWorld(ang(90,0,90)),
            1
        )
        
        Block:setAlpha(0)
        
        holoCreate(
            1,
            ScrEnt:toWorld(vec(0,35,0)),
            vec(1),
            ScrEnt:toWorld(ang(0,-90,0)),
            vec(255),
            "models/player/group03/male_07.mdl"
        )
        
        holoParent(1,Hull)
        Keeper=holoEntity(1)
        holoAnim(1,"idle_all_01")
        
        holoCreate(
            2,
            ScrEnt:toWorld(vec(-63,0,38)),
            vec(1),
            ScrEnt:toWorld(ang(0,285,0)),
            vec(255),
            "models/props_lab/citizenradio.mdl"
        )
        
        Radio=holoEntity(2)
        holoParent(2,Hull)
        
        function drawMenu0() {
            Scr:egpClear()
            
            Scr:egpBox(1,vec2(256,380),vec2(256,30))
            Scr:egpColor(1,vec4(1,1,1,128))
            
            Scr:egpBox(2,vec2(256,380),vec2(256,30))
            Scr:egpColor(2,vec4(1,1,1,128))
            Scr:egpMaterial(2,"gui/gradient_up")
            
            Scr:egpText(3,"Press Anywhere to Begin",vec2(256,380))
            Scr:egpAlign(3,1,1)
            Scr:egpSize(3,23)
        }
        
        function string prettyInt(N:number) {
            #print(N)
            local Str=ceil(N):toString():reverse():explode("")
            local Count=Str:count()
            local Add=1
            for (I=0, Str:count(), 3) {
                if (I!=0 & I!=Count & Str[I+1,string]) {Str:insertString(I+Add,",") Add++}
            }
            return Str:concat():reverse()
        }
        
        function number getAlpha(BasePos:number) {
            return clamp(600-abs(XOffset-BasePos),0,255)
        }
        
        print(getAlpha(200))
        
        function drawMenu1() {
            Scr:egpClear()
            Meta["Select",table]=table()
            
            local BaseY=360
            local Index=3
            local Iteration=0
            local BaseAng=ScrEnt:toWorld(ang(0,-90,0))
            local GunAng=ScrEnt:toWorld(ang())
            
            Scr:egpBox(1,vec2(256,BaseY+20),vec2(64,2))
            Scr:egpTriangle(2,vec2(256-40,BaseY+20),vec2(256-32,BaseY+15),vec2(256-32,BaseY+25))
            Scr:egpTriangle(3,vec2(256+40,BaseY+20),vec2(256+32,BaseY+15),vec2(256+32,BaseY+25))
            
            holoCreate(3,Scr:egpToWorld(vec2(256,BaseY)),vec(0.1),BaseAng)
            
            foreach(I,V:table=WeaponData) {
                Index++
                Iteration++
                local BaseX=256+(Iteration-1)*90
                
                Scr:egpText(Index,V["Name",string],vec2(BaseX,BaseY))
                Scr:egpAlign(Index,1,1)
                Scr:egpSize(Index,10)
                
                holoCreate(Index,Scr:egpToWorld(vec2(BaseX,BaseY-20)),vec(0.5),GunAng,vec(255),V["Model",string])
                
                Meta["Select",table][Iteration,string]=I
            }
            
            XOffset=0
        }
        
        function handleMenu1() {
            if(UseHeld) {
                XOffsetGoal=OffsetXStart-(UseStart:x()-Scr:egpCursor(User):x())
            }
            
            XOffset+=(XOffsetGoal-XOffset)/2
            
            local Index=3
            local Iteration=0
            local BaseY=360
            local GunAng=ScrEnt:toWorld(ang(abs(sin(Tick))*-15,0,0))
            foreach(I,V:table=WeaponData) {
                Index++
                Iteration++
                
                local BaseX=256+(Iteration-1)*90+XOffset
                holoAng(Index,GunAng)
                holoPos(Index,Scr:egpToWorld(vec2(BaseX,BaseY-20-abs(sin(Tick)*15))))
                
                local Alpha=getAlpha(BaseX)
                holoAlpha(Index,Alpha)
                Scr:egpAlpha(Index,Alpha)
                Scr:egpPos(Index,vec2(BaseX,BaseY))
            }
        }
        
        # entity:soundPlay(number index,number duration,string path,number fade)
        timer("radio_music",0)
    }
} elseif (last()) {
    Hull:propDelete()
    Block:propDelete()
}

if(clk()) {
    Nearest=noentity()
    
    if(!User) {
        findIncludeClass("player")
        findInSphere(Entity:pos(),200)
        Nearest=findClosest(Entity:pos())
        if(changed(Nearest) & Nearest:isValid()) {
            Keeper:soundPlay(1,1,Meta["greet",array][randint(Meta["greet",array]:count()),string])
        }
        
        if(Nearest:keyUse() & Scr:egpCursor(Nearest)!=vec2(0)) {
            User=Nearest
            drawMenu1()
        }
    }
    
    # Handle shopkeeper holo
    holoPlayerColor(1,Color)
    
    Target=User:isValid() ? User : Nearest
    
    if(Target:isValid()) {
        local DirAng=(Target:attachmentPos("eyes")-Keeper:attachmentPos("eyes")):toAngle()
        local LocAng=toLocalAng(vec(),Keeper:angles(),vec(),DirAng)
        
        Yaw=(-LocAng:yaw())
        Pitch=(-LocAng:pitch())
        
        holoSetPose(1,"head_yaw",Yaw)
        holoSetPose(1,"head_pitch",Pitch)
    }
    
    if(User:pos():distance(Keeper:pos())>200) {
        User=noentity()
        # holoDeleteAll()
        drawMenu0()
    } elseif (User) {
        handleMenu1()
    }
}

if(clk("radio_music")) {
    local Duration=randint(30,120)*1000
    Radio:soundPlay(2,Duration,Meta["music",array][randint(Meta["music",array]:count()),string])
    soundVolume(1,1)
    
    Meta["radio_fade",number]=1
    timer("radio_music",Duration+2500)
    stoptimer("radio_fade")
    timer("radio_fade",Duration-2000)
}

if(clk("radio_fade") & Meta["radio_fade",number]>0) {
    Meta["radio_fade",number]=Meta["radio_fade",number]-0.05
    soundVolume(2,Meta["radio_fade",number])
    if(Meta["radio_fade",number]!=0) {
        timer("radio_fade",100)
    } else {
        soundStop(2)
        soundVolume(2,1)
    }
}
