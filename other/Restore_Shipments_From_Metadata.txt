@name Restore Shipments from Metadata
@persist [Owner Entity Lua]:entity

if(first()) {
    #include "lib/ct/shipmentmetadata"
    
    Entity=entity()
    Owner=owner()
    findByClass("lua_run")
    Lua=find()
    function lua_Run(String:string) {
        # Scriptis' Lua Environment
        local Exec=
            format(
                "
                local SteamID=[[%s]]
                local Owner=nil
                for i,v in pairs(player.GetAll()) do
                	if v:SteamID()==SteamID then
                		Owner=v
                	end
                end
                
                local s,e=pcall(function()
                	RunString([[%s]])
                end)
                
                if not s then
                	Owner:ChatPrint(e)
                end
                "
                ,
                Owner:steamID(),
                String
            )
        Lua:set("RunPassedCode",Exec)
    }
    print("Lua: ",Lua)
    
}
