@name FCakeDDOS
@persist [Target E O]:entity E2_STATUS:string Thet:table
@trigger all


if(first() || duped()){
    
    runOnTick(1)
    
    E = entity()
    O = owner()
    Target = noentity()
    
    holoCreate(1)
    holoModel(1,"plane")
    holoMaterial(1,"models/debug/debugwhite")
    holoAlpha(1,0)
    holoParent(1,E)
    
    E2_STATUS = "SELECT"
}


if(E2_STATUS == "SELECT"){
    
 if(changed(O:keyUse()) && O:keyUse()){
    if(O:aimEntity():type() == "gmod_wire_expression2"){
        
        Target = O:aimEntity()
        E2_STATUS = "DDOS"
        
        holoAlpha(1,255)
        holoColor(1,vec(0,255,0))
    }
 }   

}

if(E2_STATUS == "DDOS" && Target != noentity()){
    
    local Ang = (E:pos() - Target:pos()):toAngle()
    local Dist = E:pos():distance(Target:pos())
    
    holoAng(1,Ang)
    holoScaleUnits(1,vec(Dist,1,1))
    holoPos(1,(E:pos()+Target:pos()) /2)
    
    while(perf(90)){   
     dsSendDirect("FWALL_NEWCONNECTION",Target,table())
 

    }
}

