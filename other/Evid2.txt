@name Holopad Export
#####
# Hologram spawning data
@persist [Holos Clips]:table HolosSpawned HolosStep LastHolo TotalHolos
@persist E:entity
#####


if (first() | duped())
{
    E = entity()

    function number addHolo(Pos:vector, Scale:vector, Colour:vector4, Angles:angle, Model:string, Material:string, Parent:number)
    {
        if (holoRemainingSpawns() < 1) {error("This model has too many holos to spawn! (" + TotalHolos + " holos!)"), return 0}
        
        holoCreate(LastHolo, E:toWorld(Pos), Scale, E:toWorld(Angles))
        holoModel(LastHolo, Model)
        holoMaterial(LastHolo, Material)
        holoColor(LastHolo, vec(Colour), Colour:w())

        if (Parent > 0) {holoParent(LastHolo, Parent)}
        else {holoParent(LastHolo, E)}

        local Key = LastHolo + "_"
        local I=1
        while (Clips:exists(Key + I))
        {
            holoClipEnabled(LastHolo, 1)
            local ClipArr = Clips[Key+I, array]
            holoClip(LastHolo, I, holoEntity(LastHolo):toLocal(E:toWorld(ClipArr[1, vector])), holoEntity(LastHolo):toLocalAxis(E:toWorldAxis(ClipArr[2, vector])), 0)
            I++
        }
        
        return LastHolo
    }

    ##########
    # HOLOGRAMS
    

    #[   ]#    Holos[1, array] = array(vec(0.0000, 17.9691, 33.5679), vec(1.2508, 1.0000, 1.0000), vec4(56, 56, 56, 255), ang(0.0000, 0.0000, 0.0000), "cylinder", "", 0)
    #[   ]#    Holos[2, array] = array(vec(15.0912, 2.2117, 12.0562), vec(1.0000, 1.0000, 1.0000), vec4(255, 0, 0, 255), ang(0.0000, -90.0000, -90.0000), "cylinder", "", 0)
    #[   ]#    Holos[3, array] = array(vec(-15.0910, 2.2120, 12.0560), vec(1.0000, 1.0000, 1.0000), vec4(255, 0, 0, 255), ang(0.0000, -90.0000, -90.0000), "cylinder", "", 0)
    #[   ]#    Holos[4, array] = array(vec(0.0000, 13.1731, 33.6349), vec(1.2540, 0.7880, 0.9870), vec4(56, 56, 56, 255), ang(0.0000, 0.0000, 0.0000), "cube", "", 0)
    #[   ]#    Holos[5, array] = array(vec(0.0000, 0.0000, 32.1259), vec(1.0000, 1.0000, 1.0000), vec4(255, 255, 255, 255), ang(0.0000, 0.0000, 0.0000), "models/maxofs2d/cube_tool.mdl", "", 0)
    #[   ]#    Holos[6, array] = array(vec(0.0000, 10.6785, 34.8005), vec(0.9950, 1.0000, 1.0000), vec4(255, 255, 255, 255), ang(0.0000, 0.0000, 0.0000), "cylinder", "models/props_debris/plasterwall040c", 0)
    #[   ]#    Holos[7, array] = array(vec(15.0123, 16.4200, 26.7371), vec(0.7607, 0.7553, 1.0000), vec4(255, 255, 255, 255), ang(-14.8713, -90.0000, -90.0000), "cylinder", "models/props_debris/plasterwall040c", 0)
    #[   ]#    Holos[8, array] = array(vec(0.0000, 10.3733, 45.6939), vec(0.2928, 0.2949, 1.5960), vec4(255, 255, 255, 255), ang(0.0000, -90.0000, -90.0000), "cylinder", "models/props_debris/plasterwall040c", 0)
    #[   ]#    Holos[9, array] = array(vec(-15.0120, 16.4200, 26.7370), vec(0.7607, 0.7553, 1.0000), vec4(255, 255, 255, 255), ang(-14.8713, -90.0000, -90.0000), "cylinder", "models/props_debris/plasterwall040c", 0)
    #[   ]#    Holos[10, array] = array(vec(0.0000, 10.3733, 40.5119), vec(0.4386, 0.4209, 1.8611), vec4(255, 255, 255, 255), ang(0.0000, 0.0000, 0.0000), "cylinder", "models/props_debris/plasterwall040c", 0)
    
    ##########
    
    TotalHolos = Holos:count()
    if (0 > holoClipsAvailable()) {error("A holo has too many clips to spawn on this server! (Max is " + holoClipsAvailable() + ")")}
}


#You may place code here if it doesn't require all of the holograms to be spawned.


if (HolosSpawned)
{
    #Your code goes here if it needs all of the holograms to be spawned!
}
else
{
    while (LastHolo <= Holos:count() & holoCanCreate() & perf())
    {
        local Ar = Holos[LastHolo, array]
        addHolo(Ar[1, vector], Ar[2, vector], Ar[3, vector4], Ar[4, angle], Ar[5, string], Ar[6, string], Ar[7, number])
        LastHolo++
    }
    
    if (LastHolo > Holos:count())
    {
        Holos:clear()
        Clips:clear()
        HolosSpawned = 1
        E:setAlpha(0)
    }

    interval(1000)
}
