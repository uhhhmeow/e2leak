@name holo shifter test v1
@persist GearPos:array Gear [O E]:entity

if(first() || duped()){
    
    
function void gearUpdate(){   
    holoPos(1,E:toWorld(GearPos[Gear,vector]))
}

   
}


    if(first() || duped()){
        runOnTick(1)

        E = entity()
        O = owner()

        holoCreate(1)
        holoScale(1, vec(0.15,0.15,0.15) )
        holoPos(1, entity():toWorld(vec(0,0,11.09) ))
        holoColor(1, vec4(200,200,200,255) )
        holoAng(1, ang(0,0,0 ))
        holoModel(1, "hq_sphere" )    
        holoParent(1, E)
        
        holoCreate(2)
        holoScale(2, vec(0.10,0.10,0.80) )
        holoPos(2, entity():toWorld(vec(0,0,7) ))
        holoColor(2, vec4(200,200,200,255) )
        holoAng(2, ang(0,0,0 ))
        holoModel(2, "cylinder" )    
        holoParent(2, 1)
        
        holoCreate(3)
        holoScale(3, vec(0.17,0.17,0.05) )
        holoPos(3, entity():toWorld(vec(0,0,2) ))
        holoColor(3, vec4(200,200,200,255) )
        holoAng(3, ang(0,0,0 ))
        holoModel(3, "cube" )    
        holoParent(3, 1)
        
        GearPos:pushVector(vec(0,0,11.09)) # Gear1 Pos
        GearPos:pushVector(vec(0,-2,11.09)) # Gear2 Pos
        GearPos:pushVector(vec(0,1,11.09)) # Gear3 Pos
        GearPos:pushVector(vec(0,-4,11.09)) # Gear4 Pos
        GearPos:pushVector(vec(0,3,11.09)) # Gear5 Pos
        
        Gear = 1
    }
    
if(changed(O:keyAttack1()) && O:keyAttack1()){
      if(Gear < 5){
        Gear+=1  
        print("Gear : "+ Gear) 
        gearUpdate()
    }  
    
}

if(changed(O:keyAttack2()) && O:keyAttack2()){
    
    if(Gear > 1){
        Gear-=1   
        print("Gear : "+ Gear)
        gearUpdate()
    }
}
