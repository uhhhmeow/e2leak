@name Gascan
@persist Use Using Reload Reloading
runOnTick(1)
Use=owner():keyUse() & !Using
Using=owner():keyUse()

Reload=owner():keyReload() & !Reloading
Reloading=owner():keyReload()

if (Use) {
    local E=propSpawn("models/props_phx/cannonball_solid.mdl",owner():aimPos(),1)
    E:propBreak()
}

if (Reload) {
    local E=propSpawn("models/props_junk/gascan001a.mdl",owner():aimPos(),1)
    E:propBreak()
}
