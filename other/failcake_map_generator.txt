@name FailCake Map Generator
@persist [E O]:entity [RawMap Map]:array Seed Website:string
@persist Load Data:string Res X Sm Color Prop:entity Model:string Y I
@trigger all

#sbox_E2_maxPropsPerSecond
#sbox_maxprops
if(first() | duped()){
runOnTick(1)
runOnChat(1)    
runOnHTTP(1)  

E = entity()
O = owner()    
E:propNotSolid(1)
E:setAlpha(0)
propSpawnEffect(0)
Model = "models/hunter/blocks/cube025x025x025.mdl"

## SETTINGS ##
Res = 15
#-123123123123
Seed = 0
Sm = 54
Zoom = 0
## SETTINGS ##

Website = "http://failcakemidi.uphero.com/CakeGen.php?Seed=" + Seed + "&GridSize=" + Res + "&Smooth=" + Sm + "&Z=" + Zoom
Load = 1
}

if(Load == 1){
    
timer("spawn_prop",30)

if(clk("spawn_prop")){
    
Prop = propSpawn(Model,E:toWorld(vec()),ang(0,0,0),1)
Prop:setMaterial("models/debug/debugwhite")
Prop:setPos(E:toWorld(vec(X*(Prop:boxSize():x()/2)*2,Y*(Prop:boxSize():y()/2)*2,15)))

Y++   

if(Y > Res){
Y=0
X+=1
}
     
if(X >= Res + 1){
Load = 2
Index = 0
stoptimer("spawn_prop")
printColor(vec(0,255,0),"[Info] ",vec(255,255,255),"Done") 
}

               
}
       
}

if(Load == 2){
    
}
