@name The Holy Bible of Scriptis
@inputs Scr:wirelink User:entity
@persist Draw Page
@trigger all

if (first()) {
    #include "lib/egp/func"
    function sexybg(Scr:wirelink) {
        Scr:egpClear()
        Scr:egpBox(300,vec2(256),vec2(512))
        Scr:egpColor(300,vec(64))
        local Lines=5
        local Dist=512/Lines
        CurSelection=0
        for (I=1, Lines+2) {
            Scr:egpLine(I+200,vec2(Dist*(I-2),-5),vec2(Dist*I,517))
            Scr:egpSize(I+200,30)
            Scr:egpMaterial(I+200,"gui/gradient_down")
            Scr:egpColor(I+200,vec(32))
        }
    }
    
}

if (~Scr | ~Page) {
    Draw=1
}

if (~User) {
    
}

Draw=0
