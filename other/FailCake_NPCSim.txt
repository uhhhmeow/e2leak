@name FailCake NPCSim
@persist [E O]:entity
@persist [NPCData E2Settings NPCS]:table
@persist [E2_STATUS]:string [PathMODE INDEX_HOLO]:number 
## EDITOR STUFF
@persist EDITOR_TYPE IS_SET PATH_SIZE [MapNode NodeConnections CleanTBL]:table Mouse_StartPos:vector
@persist NODEINX SAVE_NAME:string
## MainStuff
@persist [Nodes]:table PathLoaded
@trigger all

# Used for simulating citizens, like in city17
# By FailCake

## Materials ##
# editor/ground_node_hint -- WAY
# editor/dotted -- PATH
# editor/ai_relationship -- REQUEST STOP
###############
## FUNCTIONS ##
###############
if(first() || duped()){
    
    ## HOLO RELATED ##
    
    function number entity:isInHolo(HoloIndx:number){
        local HPos = holoEntity(HoloIndx):pos()
        local PPos = This:shootPos()
        local Mins = (HPos-PPos) 
        return acos((Mins:normalized()):dot(This:eye())) < atan(holoScaleUnits(HoloIndx):x() / 6 / Mins:length())
    }
    
    function void createHolo(Model:string,Pos:vector,Ang:angle,Scale:vector,Color:vector,Material:string,Alpha:number){
        holoCreate(INDEX_HOLO)
        holoModel(INDEX_HOLO,Model)
        holoColor(INDEX_HOLO,Color)
        holoPos(INDEX_HOLO,Pos)
        holoAlpha(INDEX_HOLO,Alpha)
        
        if(Model:find("models")){
            holoScale(INDEX_HOLO,Scale)
        }else{
            holoScaleUnits(INDEX_HOLO,Scale)
        }
        
        holoAng(INDEX_HOLO,Ang)
        holoParent(INDEX_HOLO,0)
        
        if(Material == ""){
            holoMaterial(INDEX_HOLO,"debug/debugdrawflat")
        }else{
            holoMaterial(INDEX_HOLO,Material)
        }
        
        INDEX_HOLO++
    }
    
    ## E2 RELATED ##
    
    function void updateNode(){
        if(EDITOR_TYPE == 0){ ## PATH
            holoMaterial(INDEX_HOLO,"editor/ground_node_hint")
        }elseif(EDITOR_TYPE == 1){ ## IDLE
            holoMaterial(INDEX_HOLO,"models/editor/overlay_helper")
        }
    }
    
    function number getScale(Start:entity,End:entity){
        local I = 0
        
        for(I = 0,NodeConnections:count()){
            local Nd = NodeConnections[I,table][2,entity]
            local Lst = NodeConnections[I,table][3,entity]
            
            if(Nd == Start && Lst == End){
                return NodeConnections[I,table][4,number]
            }
        }
        
        return 1
    }
    
    function void prepareNodes(){
        
        #MapNode:pushTable(table(Node,EDITOR_TYPE))
        
        local Nodx = MapNode[NODEINX,table]
        local Ent = Nodx[1,entity]
        local Type = Nodx[2,number]
        
        if(Ent:isValid()){
            
            local NextNode = MapNode[NODEINX + 1,table]
            local NextEnt = Ent:pos()
            local StartPos = Ent:pos()
            local EndPos = vec()
            
            if(NextNode:count() > 0){ # If its not the last node.
                EndPos = NextNode[1,entity]:pos()
            }
            
            CleanTBL:pushTable(table(NODEINX,StartPos,EndPos,getScale(Ent,NextNode[1,entity]),Type))
               
        }
    
        NODEINX += 1
    }
    
    function void saveSim(){ 
        fileWrite(">e2shared/npcSim/"+SAVE_NAME+".txt",vonEncode(CleanTBL))
        hint("Saved!",1000)
        
        local I = 0
        for(I = 0,MapNode:count()){
            local Ent = MapNode[I,table][1,entity]
            Ent:propDelete()   
        }
    }
    
    function void loadSim(File:string){
        if(fileCanLoad()){
           fileLoad(">e2shared/npcSim/"+File+".txt")
           E2_STATUS = "LOADING_MAP"
        }
    }
    
}

##########
## MAIN ##
##########
interval(10)

if(first() || duped()){
    runOnChat(1)
    runOnFile(1)
    
    propSpawnEffect(1)

    E = entity()
    O = owner()
    
    ## SETTINGS ##
    
    E2_STATUS = "EDITMODE"
    
    E2Settings["NPC_WLK_SPEED",number] = 1
    E2Settings["NPC_RUN_SPEED",number] = 1
    E2Settings["NPC_MAXIDLE",number] = 1
    E2Settings["NPC_MINIDLE",number] = 1
    
    E2Settings["NPC_MAX",number] = 10
    
    ## Setup Data ##
    #                 NPC MODEL - ARRAY ANIMS - 1 = Male, 0 = Female
    NPCData:pushTable(table("models/player/group01/male_03.mdl",array("idle","walk","run"),1))
    NPCData:pushTable(table("models/player/group01/male_02.mdl",array("idle","walk","run"),1))
    NPCData:pushTable(table("models/player/group01/male_06.mdl",array("idle","walk","run"),1))
    NPCData:pushTable(table("models/player/group01/male_07.mdl",array("idle","walk","run"),1))
    NPCData:pushTable(table("models/player/group01/female_01.mdl",array("idle","walk","run"),0))
    NPCData:pushTable(table("models/player/group01/female_02.mdl",array("idle","walk","run"),0))
    NPCData:pushTable(table("models/player/group01/female_06.mdl",array("idle","walk","run"),0))
    NPCData:pushTable(table("models/player/group01/female_04.mdl",array("idle","walk","run"),0))
    
    ## MAIN HOLO ##
    createHolo("cube",O:toWorld(vec(25,0,3)),O:toWorld(ang(-25,180,0)),vec(0.3,30,30),vec(1),"",0)
    holoParent(0,O:weapon())

    # START #
    #timer("prepareNPC",1000)
    INDEX_HOLO = 1
    
    printColor(vec(255,200,0),"[NPCSim]",vec(255)," Spawning NPCS...")
}

## THINK ##

if(E2_STATUS == "EDITMODE"){
    
    if(changed(O:keyUse()) && O:keyUse() && IS_SET){
        
            local Node = propSpawn("models/hunter/blocks/cube025x025x025.mdl",holoEntity(INDEX_HOLO):toWorld(vec(0,0,5)),holoEntity(INDEX_HOLO):angles(),1)
            Node:setAlpha(0)
            
            if(Node:isValid()){
                
                holoParent(INDEX_HOLO,Node)
                
                printColor(vec(255,200,0),"[NPCSim]",vec(255)," Path Node Created!")
                IS_SET = 0
                
                holoEntity(0):soundPlay(10,2,"replay/record_fail.wav")
                MapNode:pushTable(table(Node,EDITOR_TYPE))
                
                if(MapNode:count() > 1){
                    
                    if(!holoEntity(Node:id()):isValid()){
                        holoCreate(Node:id())
                    }   
                    
                    local LastNode = MapNode[MapNode:count() - 1,table][1,entity]
                    local Angs = (Node:pos() - LastNode:pos()):toAngle()
                    local Dist = Node:pos():distance(LastNode:pos())
                    
                    holoModel(Node:id(),"plane")
                    holoAng(Node:id(),Angs)
                    holoScaleUnits(Node:id(),vec(Dist,1,1))
                    holoPos(Node:id(),(Node:pos()+LastNode:pos()) / 2)
                    holoMaterial(Node:id(),"debug/debugdrawflat")
                    holoAlpha(Node:id(),100)
                    
                    NodeConnections:pushTable(table(Node:id(),Node,LastNode))
                }
                
                INDEX_HOLO += 2
                
            }
        
    }
    
    if(changed(O:keyReload()) && O:keyReload()){
        EDITOR_TYPE = !EDITOR_TYPE
        holoEntity(0):soundPlay(10,10,"replay/deleted_take.wav")
        updateNode()
    }
    
    if(changed(O:keyAttack2()) && O:keyAttack2()){
            
            if(!holoEntity(INDEX_HOLO):isValid()){
                holoCreate(INDEX_HOLO)
            }
            
            holoModel(INDEX_HOLO,"cube")
            holoAlpha(INDEX_HOLO,255)
            holoScaleUnits(INDEX_HOLO,vec(15,15,2))
            holoPos(INDEX_HOLO,O:aimPos())
            
            if(!holoEntity(INDEX_HOLO + 1):isValid()){
                holoCreate(INDEX_HOLO + 1)
            }
            
            holoAlpha(INDEX_HOLO + 1,30)
            holoScaleUnits(INDEX_HOLO + 1,vec(40,40,0.5))
            holoMaterial(INDEX_HOLO + 1,"models/debug/debugwhite")
            holoColor(INDEX_HOLO + 1,vec(255,50,0))
            holoPos(INDEX_HOLO + 1,holoEntity(INDEX_HOLO):toWorld(vec(0,0,0)))
            holoParent(INDEX_HOLO + 1,INDEX_HOLO)
            
            updateNode()
            
            holoEntity(0):soundPlay(10,2,"replay/showdetails.wav")
            
            IS_SET = 1
        
    
    }
    
    if(NodeConnections:count() > 0){
        
        local I = 0
        
        for(I = 0,NodeConnections:count()){
            
            local ID = NodeConnections[I,table][1,number]
            local Nd = NodeConnections[I,table][2,entity]
            local Lst = NodeConnections[I,table][3,entity]
            
            if(!Nd:isValid() || !Lst:isValid()){
                NodeConnections:remove(I)
                holoDelete(ID)
                continue   
            }
            
            if(O:isInHolo(ID)){
                
                if(changed(O:keyAttack1()) && O:keyAttack1()){
                    Mouse_StartPos = O:aimPos()   
                }
                
                if(O:keyAttack1()){
                    
                    local Scale = abs(Mouse_StartPos:distance(O:aimPos()))
                    
                    local Nd = NodeConnections[I,table][2,entity]
                    local Lst = NodeConnections[I,table][3,entity]
                    
                    local Angs = (Nd:pos() - Lst:pos()):toAngle()
                    local Dist = Nd:pos():distance(Lst:pos())
                    
                    holoAng(ID,Angs)
                    holoScaleUnits(ID,vec(Dist,Scale,1))
                    holoPos(ID,(Nd:pos()+Lst:pos()) / 2)
                    
                    holoColor(ID,vec(255,0,0))
                    # Resize
                    
                    NodeConnections[I,table][4,number] = Scale
                    
                    
                }else{  
                    holoColor(ID,vec(255,100,0))
                }
                
            }else{
                holoColor(ID,vec(255))
                continue
            }
        }
        
    }
    
}

if(E2_STATUS == "PLAYMODE"){
    for(I = 0,NodeConnections:count()){
    
    }
}


if(E2_STATUS == "PREPMODE"){
    if(clk("prepTick")){
        
        prepareNodes()
        
        if(NODEINX < MapNode:count()){
           timer("prepTick",300)
        }else{
            E2_STATUS = "SAVE_PREP"
            print("Map Prepared! Saving..")
            saveSim()
            
            holoEntity(0):soundPlay(10,10,"replay/youtube_uploadfinished.wav")
        }
        
    }
}

## TIMER CONTROL ##
if(clk("prepareNPC")){
    
    if(INDEX_HOLO < E2Settings["NPC_MAX",number]){
        timer("prepareNPC",50)  
    }else{
        stoptimer("prepareNPC")
        printColor(vec(255,200,0),"[NPCSim]",vec(255)," E2 Ready!")
        E2_STATUS = "IDLE"
        
        INDEX_HOLO = 100
    }
    
    local Model = NPCData[randint(1,NPCData:count()),table][1,string]
    holoCreate(INDEX_HOLO)
    holoModel(INDEX_HOLO,Model)
    holoPos(INDEX_HOLO,E:toWorld(vec(0,0,0)))
    
    holoAnim(INDEX_HOLO,"idle_suitcase")
    holoAlpha(INDEX_HOLO,0)
    
    if(holoEntity(INDEX_HOLO):isValid()){
        if(holoEntity(INDEX_HOLO):model() != Model){
            printColor(vec(255,200,0),"[NPCSim]",vec(255)," Invalid Model found : " + Model)
        }else{
            # Entity,IsWalking,IsIdle,Timer
            NPCS:pushTable(table(holoEntity(INDEX_HOLO),0,0,0)) 
            INDEX_HOLO += 1
        }
    }

}

## CHAT CONTROL ##
if(chatClk(O)){
    
    local Chat = O:lastSaid():lower():explode(" ")
    
    if(Chat[1,string] == "-load"){
        E2_STATUS = "LOADMODE"
    }elseif(Chat[1,string] == "-edit"){
        E2_STATUS = "EDITMODE"
    }elseif(Chat[1,string] == "-save"){
        if(E2_STATUS == "EDITMODE" && Chat[2,string] != ""){
            
            SAVE_NAME = Chat[2,string]
            
            E2_STATUS = "PREPMODE"
            NODEINX = 1
            timer("prepTick",200)
            CleanTBL:clear()
            
            holoEntity(0):soundPlay(10,10,"replay/youtube_startingupload.wav")
            
        }
    }elseif(Chat[1,string] == "-done"){
        E2_STATUS = "PLAYMODE"
    }
}
