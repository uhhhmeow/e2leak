@name Hologram Enderdragon
#@model models/hunter/blocks/cube05x1x025.mdl
#@model models/hunter/blocks/cube2x4x1.mdl
@persist Center:entity [Joints Lookup Meta]:table
@persist LookPos:vector TailHistory:array Scale
@persist [Owner Entity]:entity State:string
@persist Pitch Yaw Roll PitchGoal YawGoal RollGoal
@persist Speed Seat:entity CamController:wirelink
@persist Walking

#[
    This is my shot at making a functioning holographic joint system.
    Why the enderdragon?  I've already made a 3D model of it in another engine,
    making it easier for me to gather data and build the hologram.
    
    ~ Scriptis
]#
#include "EnderDragon_Graph"
AimAng=CamController["CamAng",angle]
AimAng=holoEntity(1):toWorld(clamp(holoEntity(1):toLocal(AimAng),ang(-50),ang(50)))
Owner=Seat:driver()

if(first()) {
    #include "mantle/angle"
    #include "mantle/force"
    
    #Owner=owner()
    State="anim_IdleWalk"
    Scale=4
    Speed=30
    Walking=0
    
    findByClass("prop_vehicle_prisoner_pod")
    foreach(I,V:entity=findToArray()) {if(V:owner()==owner()){Seat=V}}
    
    findByClass("gmod_wire_cameracontroller")
    foreach(I,V:entity=findToArray()) {if(V:owner()==owner()){CamController=V:wirelink()}}
    print("Camera Controller: ",CamController,CamController:entity():owner():name())
    print("Pod: ",Seat,Seat:owner():name())
    
    if(!Seat){error("Please spawn a pod.")}
    if(!CamController){error("Please spawn a Cam Controller.")}
    
    Entity=propSpawn("models/hunter/plates/plate1x1.mdl",Seat:toWorld(vec(0,0,-23.14/4)),Seat:angles(),0)
    Entity:propGravity(0)
    
    # Debug Holos
    for(I=500,550) {
        holoCreate(I)
        holoScale(I,vec(0.1))
        holoMaterial(I,"debug/debugdrawflat")
        holoAlpha(I,128)
        holoColor(I,vec(255,0,0))
    }
    
    function holo_EnderDragon(Scale) {
        local Parents=table()
        for(I=1,EnderGraph:count()) {
            local V=EnderGraph[I,table]
            #print("Creating",I)
            local ID=V["ID",number]
            local Parent=V["Parent",number]
            local Alpha=V["Alpha",number]
            local Pos=V["Pos",vector]*Scale
            local Ang=V["Ang",angle]
            local Color=V["Color",vector]*255
            local Size=V["Size",vector]/12*Scale
            
            holoCreate(ID)
            holoAlpha(ID,Alpha)
            holoAlpha(ID,255) # dbg
            holoPos(ID,Pos*2)
            holoAng(ID,Ang)
            holoScale(ID,Size*2)
            holoColor(ID,Color)
            Parents[toString(ID),array]=array(ID,Parent)
        }
        
        foreach(I,V:array=Parents) {
            holoParent(V[1,number],V[2,number])
        }
        
        # Metadata
        Meta["Neck0",vector]=holoEntity(55):toLocal(holoEntity(1):pos())
        Meta["NeckLength",number]=holoEntity(43):pos():distance(holoEntity(55):pos())*2
        Meta["NeckAng",angle]=(holoEntity(55):pos()-holoEntity(58):pos()):toAngle()+ang(0,-90,0)
        
        #tail1 - 67
        #tail12 - 99
        Meta["Tail0",vector]=holoEntity(64):toLocal(holoEntity(1):pos())
        Meta["TailLength",number]=holoEntity(99):pos():distance(holoEntity(67):pos())
        Meta["TailAng",angle]=ang(0,90,0)
        
        Center=holoEntity(1)
        holoPos(1,Entity:pos())
        holoAng(1,Entity:toWorld(ang(0,90,90)))
        holoParent(1,Entity)
        
        # Make all the joints, apply all the materials, color the eyes
        # Lookup table to save ops
        Lookup=table()
        for(I=1,EnderGraph:count()) {
            local V=EnderGraph[I,table]
            Lookup[V["Name",string],number]=V["ID",number]
            if(V["Name",string]=="EyeColor2") {
                holoColor(V["ID",number],vec(238,147,255))
                holoMaterial(V["ID",number],"debug/debugdrawflat")
            } elseif (V["Name",string]=="EyeColor1") {
                holoColor(V["ID",number],vec(182,0,255))
                holoMaterial(V["ID",number],"debug/debugdrawflat")
            } elseif (V["Color",vector]:length()>0.6) {
                holoMaterial(V["ID",number],"models/props/de_nuke/nukconcretewalla")
            }
        }
        
        # Create joints
        for(I=1,EnderGraph:count()) {
            local V=EnderGraph[I,table]
            local Name=V["Name",string]:explode(":")
            if(Name[1,string]=="Joint") {
                local Part0=Name[2,string]
                local Part1=Name[3,string]
                local JointName=Name[4,string]
                holoParent(V["ID",number],Lookup[Part0,number])
                holoParent(Lookup[Part1,number],V["ID",number])
                Joints[JointName,number]=V["ID",number]
                holoMaterial(V["ID",number],"models/wireframe") #dbg
            } else {holoAlpha(V["ID",number],64)} #dbg
        }
    }
    
    holo_EnderDragon(Scale)
    entity():setAlpha(0)
    
    function setRot(N:string,Ang:angle) {
        #print(N,Joints[N,number])
        holoAng(Joints[N,number],Center:toWorld(Ang))
    }
    
    function setRotW(N:string,Ang:angle) {
        holoAng(Joints[N,number],Ang)
    }
    
    # Animations
    function anim_IdleWalk(Tick) {
        local Offset=-30+(sin(Tick*30)*3)
        local Wing=ang(15,0,Offset)
        local Mid=ang(15,0,Offset+120)
        setRot("RightWing",Wing)
        setRot("RightWingMid",Mid)
        setRot("LeftWing",-Wing)
        setRot("LeftWingMid",-Mid)
        YawGoal=0
        RollGoal=0
    }
    
    function anim_Walk(Tick) {
        anim_IdleWalk(Tick)
        local Leg=ang(0,sin(Tick*350)*3,0)
        local MidLeg=Leg+Leg*4
        setRot("FrontRightLeg",Leg)
        setRot("LegFRM",MidLeg)
        setRot("BackLeftLeg",Leg)
        setRot("LegBLM",MidLeg)
        setRot("FrontLeftLeg",-Leg)
        setRot("LegFLM",-MidLeg)
        setRot("BackRightLeg",-Leg)
        setRot("LegBRM",-MidLeg)
    }
    
    function anim_Hover(Tick) {
        Roll=45
        local Leg=-ang(0,sin(Tick*150)*5+45,0)
        local Leg2=Leg+Leg/2
        local Wing=ang(0,0,sin(Tick*400)*50)
        local Wing2=Wing*0.5+Wing
        
        setRot("BackRightLeg",Leg)
        setRot("BackLeftLeg",Leg)
        setRot("FrontRightLeg",Leg)
        setRot("FrontLeftLeg",Leg)
        
        setRot("LegBRM",Leg2)
        setRot("LegBLM",Leg2)
        setRot("LegFRM",Leg2)
        setRot("LegFLM",Leg2)
        
        setRot("RightWing",Wing)
        setRot("LeftWing",-Wing)
        setRot("LeftWingMid",-Wing2)
        setRot("RightWingMid",Wing2)
    }
    
    function anim_Fly(Tick) {
        Roll=0
        local Leg=ang(0,-40+sin(Tick*175)*3,0)
        local Leg2=Leg+Leg/2
        local Wing=ang(0,0,sin(Tick*350)*5)
        local Wing2=Wing+Wing/2
        
        setRot("BackRightLeg",Leg)
        setRot("BackLeftLeg",Leg)
        setRot("FrontRightLeg",Leg)
        setRot("FrontLeftLeg",Leg)
        
        setRot("LegFRM",Leg2)
        setRot("LegFLM",Leg2)
        setRot("LegBLM",Leg2)
        setRot("LegBRM",Leg2)
        
        setRot("RightWing",Wing)
        setRot("LeftWing",-Wing)
        setRot("RightWingMid",Wing2)
        setRot("LeftWingMid",-Wing2)
    }
    
    function anim_Sit(Tick) {
        if(!Walking) {anim_Hover(Tick)} else {
            
        }
    }
    
    # Volatile Segment Handlers
    function handleNeck() {
        local Start=holoEntity(1):toWorld(-Meta["Neck0",vector])
        local Mid=Start+Entity:toWorld(Meta["NeckAng",angle]):forward()*Meta["NeckLength",number]/2
        local End=Start+AimAng:forward()*Meta["NeckLength",number]
        local N1=bezier(Start,Mid,End,1/6)
        local N2=bezier(Start,Mid,End,2/6)
        local N3=bezier(Start,Mid,End,3/6)
        local N4=bezier(Start,Mid,End,4/6)
        local N5=bezier(Start,Mid,End,5/6)
        local N6=End
        
        local Offset=ang(0,0,90)
        
        local A1=(N2-N1):toAngle()+Offset
        local A2=(N3-N2):toAngle()+Offset
        local A3=(N4-N3):toAngle()+Offset
        local A4=(N5-N4):toAngle()+Offset
        local A5=(N5-N4):toAngle()+Offset
        local A6=(N6-N5):toAngle()+Offset
        
        setRotW("Neck0",A1)
        setRotW("Neck1",A2)
        setRotW("Neck2",A3)
        setRotW("Neck3",A4)
        setRotW("Neck4",A5)
        setRotW("Neck5",A6)
    }
    
    function handleTail() {
        local HistLength=30
        if(TailHistory:count()>HistLength) {TailHistory:pop()}
        local TailAng=(Meta["TailAng",angle]):forward()
        local Last=Entity:toWorld(TailAng*-Meta["TailLength",number])
        TailHistory:unshiftVector(Last)
        local Start=holoEntity(1):toWorld(-Meta["Tail0",vector]/2)
        local Mid=(Start+Last)/2
        local Total=vec()
        for(I=1,HistLength) {
            Total+=TailHistory[I,vector]
        }
        local AvgVec=Total/HistLength
        local AvgAng=(AvgVec-Start):toAngle()
        local End=Start+AvgAng:forward()*Meta["TailLength",number]
        if(State:matchFirst("Walk")) {
            local Ray=rangerOffset(End,End-vec(0,0,24*Scale))
            End=Ray:hit() ? Ray:pos() : End
            local Ray=rangerOffset(Mid,Mid-vec(0,0,24*Scale))
            Mid=Ray:hit() ? Ray:pos() : Mid
        }
        local Offset=ang(0,0,90)
        local Bez=array()
        local MaxColor=vec(0,255,0)
        local MinColor=vec(255,0,0)
        for(I=1,12) {
            Bez:pushVector(bezier(Start,Mid,End,I/12))
        }
        
        #[ #dbg
        holoPos(501,Start)
        holoColor(501,vec(0,255,0))
        holoPos(502,Mid)
        holoColor(502,vec(255,255,0))
        holoPos(503,End)
        holoColor(503,vec(255,0,0))
        holoPos(504,Last)
        holoColor(504,vec(0,255,255))
        ]#
        
        setRotW("Tail1",(Bez[1,vector]-Bez[2,vector]):toAngle()+Offset)
        setRotW("Tail2",(Bez[2,vector]-Bez[3,vector]):toAngle()+Offset)
        setRotW("Tail3",(Bez[3,vector]-Bez[4,vector]):toAngle()+Offset)
        setRotW("Tail4",(Bez[4,vector]-Bez[5,vector]):toAngle()+Offset)
        setRotW("Tail5",(Bez[5,vector]-Bez[6,vector]):toAngle()+Offset)
        setRotW("Tail6",(Bez[6,vector]-Bez[7,vector]):toAngle()+Offset)
        setRotW("Tail7",(Bez[7,vector]-Bez[8,vector]):toAngle()+Offset)
        setRotW("Tail8",(Bez[8,vector]-Bez[9,vector]):toAngle()+Offset)
        setRotW("Tail9",(Bez[9,vector]-Bez[10,vector]):toAngle()+Offset)
        setRotW("Tail10",(Bez[10,vector]-Bez[11,vector]):toAngle()+Offset)
        setRotW("Tail11",(Bez[11,vector]-Bez[12,vector]):toAngle()+Offset)
    }
    
    #Entity:propGravity(0)
    #noCollideAll(Entity,0)
    
    #CamController:entity():setMaterial("debug/debugdrawflat")
    #CamController:entity():propNotSolid(1)
    #Seat:setMaterial("debug/debugdrawflat")
    #Seat:setAlpha(0)
    #Entity:setPos(Seat:toWorld(vec(0,-30,-20)))
    Entity:setAlpha(0)
    #Entity:setAng(Seat:toWorld(ang()))
    Entity:setMass(5000)
    #weld(Seat,Entity)
    CamController["Activated",number]=1
    CamController["Position",vector]=Seat:toWorld(vec(0,0,0))
    CamController["Parent",entity]=Entity
    Seat:propGravity(0)
    #Entity:parentTo(Seat)
    timer("Parent",250)
    holoPos(1,Entity:pos())
    holoAng(1,Entity:angles()+ang(0,90,90))
    entity():propNotSolid(1)
}
runOnLast(1)
if(last()){Seat:setAlpha(255) Seat:propGravity(1) Seat:propFreeze(1) CamController["Activated",number]=0}
owner():setAlpha(owner():inVehicle() ? 0 : 255)
# Animation Handler
State(curtime())
handleNeck()
handleTail()

# Movement Handler
Yaw=AimAng:yaw()
local Right=Owner:keyRight()
local Left=Owner:keyLeft()

local EntPos=Entity:pos()
local Move=
    (
        (Owner:keyForward() ? Entity:right()*-Speed : vec())
        +(Owner:keyBack() ? Entity:right()*Speed : vec())
        +(Right ? Entity:forward()*Speed : vec())
        +(Left ? Entity:forward()*-Speed : vec())
    )
    *vec(1,1,0)
    +(Owner:keyJump() ? vec(0,0,Speed) : vec())
    +(Owner:keySprint() ? vec(0,0,-Speed) : vec())

if(!Walking) {
    local FinPitch=0
    if(Right){FinPitch+=15}
    if(Left){FinPitch-=15}
    Pitch=FinPitch
}

if(Owner) {
    if((Move*vec(1,1,0)):length()>1) {
        State=Walking ? "anim_Walk" : "anim_Fly"
    } else {
        State=Walking ? "anim_IdleWalk" : "anim_Hover"
    }
} else {
    State="anim_Sit"
}

local Angle=
    (CamController["CamAng",angle]*ang(0,1,0)-ang(0,90,0))
local Offset=ang(Pitch,0,Roll)
Angle=toWorldAng(vec(),Offset,vec(),Angle)
Entity:moveNoReset(Entity:pos()+Move,1,0.03)
Entity:rotate(Angle)
#Entity:propGravity(State:matchFirst("Walk") ? 1 : 0)

interval(20)

print(_HUD_PRINTCENTER,State)
