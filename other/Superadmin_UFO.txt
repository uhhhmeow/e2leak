@name Superadmin UFO
@inputs W A S D Space Shift Flyer:entity
@outputs Murder
@persist Chair:entity

interval(100)
if (~Flyer & Flyer!=owner()) {
    Murder=1
}
if (!Flyer) {
    Murder=0
}

if (W) {
    Chair:setPos(Chair:forward()*1)
}

if (A) {
    Chair:setAng(Chair:angles()-ang(0,0,1))
}

if (D) {
    Chair:setAng(Chair:angles()+ang(0,0,1))
}

if (S) {
    Chair:setPos(Chair:forward()*-1)
}

if (Space) {
    Chair:setPos(Chair:pos()+vec(0,1,0))
}

if (Shift) {
    Chair:setPos(Chair:pos()-vec(0,1,0))
}

runOnChat(1)
if (chatClk(owner()) & owner():lastSaid()=="-setchair") {
    Chair=owner():aimEntity()
    print("CHAIR",Chair)
}
