@name Floor is Lava E2
@inputs 
@outputs 
@persist 
@trigger 

interval(100)

if(first()){
    concmd("say THE FLOOR IS LAVA! GET TO HIGH GROUND, QUICKLY!")
    entity():soundPlay(entity():id(),9000,"ambient/machines/deep_boil.wav")
holoCreate(1) holoModel(1,"models/hunter/plates/plate32x32.mdl") holoMaterial(1,"models/ihvtest/eyeball_l") holoColor(1,vec(255,100,0)) holoPos(1,entity():pos()+vec(0,0,10))
holoCreate(2) holoModel(2,"models/hunter/plates/plate32x32.mdl") holoMaterial(2,"models/props_lab/Tank_Glass001") holoColor(2,vec(255,100,0)) holoPos(2,entity():pos()+vec(0,0,15))
holoCreate(3) holoModel(3,"models/hunter/plates/plate32x32.mdl") holoMaterial(3,"models/effects/muzzleflash/blurmuzzle") holoColor(3,vec(255,100,0),100) holoPos(3,entity():pos()+vec(0,0,150))}
holoScale(1,vec(4,4,1))
holoScale(2,vec(4,4,1))
findIncludeClass("prop_physics")
findIncludeClass("gmod_thruster")
findIncludeClass("prop_vehicle_airboat")
findIncludeClass("acf_debris")
findIncludeClass("player")
findAllowPlayerProps(owner())
findIncludeClass("prop_vehicle_jeep")
findInBox(entity():pos()+vec(-704*4.5,-704*4.4,0),entity():pos()+vec(704*4.4,704*4.4,15))
findSortByDistance(entity():pos())
Derp=findToArray()

foreach(Derp,K:entity=Derp){
        Color=vec(4,6,20)
    K:setColor(K:getColor()-(Color))
    if(K:getColor()==vec(0,0,0)){
    K:propDelete()}
    if(!K:isOnFire()){K:soundPlay(K:id(),2,"ambient/fire/ignite.wav")}
    if(K:isPlayer()){
K:ignite()}
    K:propFreeze(1)
    K:setPos(K:pos()-vec(0,0,1))
}
