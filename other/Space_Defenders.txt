@name Space Defenders Main
@persist [Entity Owner]:entity
@persist [Meteors Players]:table

if(first()) {
    Entity=entity()
    Owner=owner()
    
    local Center=Entity:toWorld(vec(0,0,2000))
    local MapScale=8
    
    holoCreate(1,Center,vec(MapScale),ang(),vec(200,200,0),"models/hunter/misc/sphere375x375.mdl")
    holoMaterial(1,"debug/debugdrawflat")
    
    holoCreate(2,Center,vec(MapScale+0.1),ang(),vec(255,128,0),"models/hunter/misc/sphere375x375.mdl")
    holoMaterial(2,"debug/debugdrawflat")
    holoAlpha(2,128)
}
