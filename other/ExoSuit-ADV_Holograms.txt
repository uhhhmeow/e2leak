@name ExoSuit-ADV Holograms
@persist [HC CC C2] MechScale HoloBase RC:vector

local E2Name = "AdvExo"
if(first()){
Holo = gTable("Holo"+E2Name)
Clip = gTable("Clips"+E2Name)
LegStuff = gTable("LegStuff"+E2Name)

}
MechScale = 1
local EyeCol = vec(255,0,0)

NeonCol = vec(0,255,255)

local V = vec()
local A = ang()
local C = vec(255)
#local M = "models/props_debris/plasterwall040c"
local M = "phoenix_storms/Metalfloor_2-3"
local M = ""
local L = "debug/debugdrawflat"

local S = vec(1)
local Ent = entity()

local CamHeight = 85

FB = 0
RL = 30

LegL = 60


if(first()){

 function table holo(Index,Vector:vector,Scale:vector,Angle:angle,Color:vector,Model:string,Parent,Alpha,Material:string){
  HC++ Holo[HC,table] = table(Index,Vector,Scale,Angle,Color,Model,Parent,Alpha,Material)
 }
 function table clip(I,Position:vector,Direction:vector,Secondindex){
  CC++ Clip[CC,table] = table(I,Position,Direction,Secondindex)
 }

}


########################################################################
########################################################################
########################################################################


###legs skell###
holo(1,vec(0,RL,0),vec(1),ang(),vec(255,0,0),"cube",0,0,M)
holo(2,vec(0,-RL,0),vec(1),ang(),vec(255,0,0),"cube",0,0,M)

holo(3,vec(0,0,LegL),vec(1),ang(),vec(255,0,0),"cube",1,0,M)
holo(4,vec(0,0,LegL),vec(1),ang(),vec(255,0,0),"cube",2,0,M)

holo(5,vec(0,0,LegL),vec(1),ang(),vec(255,0,0),"cube",3,0,M)
holo(6,vec(0,0,LegL),vec(1),ang(),vec(255,0,0),"cube",4,0,M)
###----------###

###legs model###



#hips#
AddZ = 1

holo(7,vec(0,0,0),vec(2,2,1 + AddZ),ang(0,0,90),vec(20),"hqcylinder",1,255,M)
holo(8,vec(0,0,0),vec(2,2,1 + AddZ),ang(0,0,90),vec(20),"hqcylinder",2,255,M)

holo(9,vec(0,0,0),vec(1,1,1.3 + AddZ),ang(0,0,90),vec(20),"hq_tube",1,255,M)
holo(10,vec(0,0,0),vec(1,1,1.3 + AddZ),ang(0,0,90),vec(20),"hq_tube",2,255,M)

holo(11,vec(0,0,0),vec(2.5,2.5,0.5 + AddZ),ang(0,0,90),vec(20),"hq_tube_thin",1,255,M)
holo(12,vec(0,0,0),vec(2.5,2.5,0.5 + AddZ),ang(0,0,90),vec(20),"hq_tube_thin",2,255,M)



holo(13,vec(0,0,0),vec(2.45,2.45,0.45 + AddZ),ang(0,0,90),NeonCol,"hqcylinder",1,255,L)
holo(14,vec(0,0,0),vec(2.45,2.45,0.45 + AddZ),ang(0,0,90),NeonCol,"hqcylinder",2,255,L)

holo(15,vec(0,0,0),vec(0.8,0.8,1.2 + AddZ),ang(0,0,90),NeonCol,"hqcylinder",1,255,L)
holo(16,vec(0,0,0),vec(0.8,0.8,1.2 + AddZ),ang(0,0,90),NeonCol,"hqcylinder",2,255,L)

holo(17,vec(0,0,0),vec(2.1,2.1,1.1 + AddZ),ang(0,0,90),vec(20),"hq_tube_thin",1,255,M)
holo(18,vec(0,0,0),vec(2.1,2.1,1.1 + AddZ),ang(0,0,90),vec(20),"hq_tube_thin",2,255,M)



holo(19,vec(0,0,0),vec(2.45,4.45,0.45 + AddZ),ang(0,0,90),NeonCol,"hqcylinder",1,255,L)
holo(20,vec(0,0,0),vec(2.50,4.5,0.5 + AddZ),ang(0,0,90),vec(20),"hq_tube_thin",1,255,M)
clip(19,vec(0,-1,0),vec(0,-1,0),0)
clip(20,vec(0,-1,0),vec(0,-1,0),0)

holo(21,vec(0,0,0),vec(2.45,4.45,0.45 + AddZ),ang(0,0,90),NeonCol,"hqcylinder",2,255,L)
holo(22,vec(0,0,0),vec(2.50,4.5,0.5 + AddZ),ang(0,0,90),vec(20),"hq_tube_thin",2,255,M)
clip(21,vec(0,-1,0),vec(0,-1,0),0)
clip(22,vec(0,-1,0),vec(0,-1,0),0)

holo(23,vec(0,0,15),vec(2,0.8,1.5) + AddZ/2,ang(0,0,0),vec(20),"hq_tube_thin",1,255,M)
holo(24,vec(0,0,15),vec(2,0.8,1.5) + AddZ/2,ang(0,0,0),vec(20),"hq_tube_thin",2,255,M)

holo(25,vec(0,0,15),vec(1.95,0.75,1.25) + AddZ/2,ang(0,0,0),NeonCol,"hq_cylinder",1,255,M)
holo(26,vec(0,0,15),vec(1.95,0.75,1.25) + AddZ/2,ang(0,0,0),NeonCol,"hq_cylinder",2,255,M)
#----#



#upper mid parts#

holo(27,vec(0,0,LegL*0.6),vec(2 + AddZ/2,1 + AddZ/2,3)*0.8,ang(),vec(20),"hqcylinder",1,255,M)
holo(28,vec(0,0,LegL*0.6),vec(2 + AddZ/2,1 + AddZ/2,3)*0.8,ang(),vec(20),"hqcylinder",2,255,M)

holo(29,vec(0,0,LegL*0.6),vec(2.05 + AddZ/2,0.1,3)*0.8,ang(),NeonCol,"cube",1,255,L)
holo(30,vec(0,0,LegL*0.6),vec(2.05 + AddZ/2,0.1,3)*0.8,ang(),NeonCol,"cube",2,255,L)

holo(31,vec(0,0,LegL*0.4),vec(0.1,0.82 + AddZ/2,4.4)*1,ang(),NeonCol,"cube",1,255,L)
holo(32,vec(0,0,LegL*0.4),vec(0.1,0.82 + AddZ/2,4.4)*1,ang(),NeonCol,"cube",2,255,L)

holo(33,vec(0,0,LegL*0.8),vec(2.2 + AddZ/2,1.2 + AddZ/2,0.4 + AddZ/2)*0.8,ang(),vec(20),"hqcylinder",1,255,M)
holo(34,vec(0,0,LegL*0.8),vec(2.2 + AddZ/2,1.2 + AddZ/2,0.4 + AddZ/2)*0.8,ang(),vec(20),"hqcylinder",2,255,M)

#---------------#

#knee#
holo(35,vec(0,0,0),vec(3 + AddZ/2,1.5 + AddZ/2,3 + AddZ/2)*0.6,ang(),vec(20),"hqsphere",3,255,M)
holo(36,vec(0,0,0),vec(3 + AddZ/2,1.5 + AddZ/2,3 + AddZ/2)*0.6,ang(),vec(20),"hqsphere",4,255,M)

holo(37,vec(0,0,0),vec(3.07 + AddZ/2,3.07 + AddZ/2,0.2)*0.6,ang(0,0,90),NeonCol,"hqcylinder",3,255,M)
holo(38,vec(0,0,0),vec(3.07 + AddZ/2,3.07 + AddZ/2,0.2)*0.6,ang(0,0,90),NeonCol,"hqcylinder",4,255,M)


holo(39,vec(0,0,0),vec(1 + AddZ/2,1 + AddZ/2,2.2)*0.6,ang(0,0,90),vec(20),"hq_tube_thin",3,255,M)
holo(40,vec(0,0,0),vec(1 + AddZ/2,1 + AddZ/2,2.2)*0.6,ang(0,0,90),vec(20),"hq_tube_thin",4,255,M)

holo(41,vec(0,0,0),vec(0.95 + AddZ/2,0.95 + AddZ/2,2.1)*0.6,ang(0,0,90),NeonCol,"hq_cylinder",3,255,M)
holo(42,vec(0,0,0),vec(0.95 + AddZ/2,0.95 + AddZ/2,2.1)*0.6,ang(0,0,90),NeonCol,"hq_cylinder",4,255,M)

#----#


#lower leg part#

################
 ReL = 0.6

holo(43,vec(0,0,LegL*0.4)*ReL,vec(2 + AddZ/2,1 + AddZ/2,3)*ReL,ang(),vec(20),"hqcylinder",3,255,M)
holo(44,vec(0,0,LegL*0.4)*ReL,vec(2 + AddZ/2,1 + AddZ/2,3)*ReL,ang(),vec(20),"hqcylinder",4,255,M)

holo(45,vec(0,0,LegL*0.4)*ReL,vec(2.05 + AddZ/2,0.1,3)*ReL,ang(),NeonCol,"cube",3,255,L)
holo(46,vec(0,0,LegL*0.4)*ReL,vec(2.05 + AddZ/2,0.1,3)*ReL,ang(),NeonCol,"cube",4,255,L)

holo(47,vec(0,0,LegL*0.4)*ReL,vec(0.1,1.1 + AddZ/2,3)*ReL,ang(),NeonCol,"cube",3,255,L)
holo(48,vec(0,0,LegL*0.4)*ReL,vec(0.1,1.1 + AddZ/2,3)*ReL,ang(),NeonCol,"cube",4,255,L)

holo(49,vec(0,0,LegL*0.2)*ReL,vec(2.2 + AddZ/2,1.2 + AddZ/2,0.4 + AddZ/2)*ReL,ang(),vec(20),"hqcylinder",3,255,M)
holo(50,vec(0,0,LegL*0.2)*ReL,vec(2.2 + AddZ/2,1.2 + AddZ/2,0.4 + AddZ/2)*ReL,ang(),vec(20),"hqcylinder",4,255,M)

holo(51,vec(0,0,LegL*0.75)*ReL,vec(2.2 + AddZ/2,1.2 + AddZ/2,0.4 + AddZ/2)*ReL,ang(),vec(20),"hqcylinder",3,255,M)
holo(52,vec(0,0,LegL*0.75)*ReL,vec(2.2 + AddZ/2,1.2 + AddZ/2,0.4 + AddZ/2)*ReL,ang(),vec(20),"hqcylinder",4,255,M)

holo(53,vec(0,0,LegL*1)*ReL,vec(2 + AddZ/2,1 + AddZ/2,3)*(ReL/1.1),ang(),NeonCol,"hqcylinder",3,255,L)
holo(54,vec(0,0,LegL*1)*ReL,vec(2 + AddZ/2,1 + AddZ/2,3)*(ReL/1.1),ang(),NeonCol,"hqcylinder",4,255,L)

holo(55,vec(0,0,LegL*1.25)*ReL,vec(2.2 + AddZ/2,1.2 + AddZ/2,3)*(ReL/1.1),ang(),vec(20),"hqcylinder",3,255,M)
holo(56,vec(0,0,LegL*1.25)*ReL,vec(2.2 + AddZ/2,1.2 + AddZ/2,3)*(ReL/1.1),ang(),vec(20),"hqcylinder",4,255,M)

holo(57,vec(0,0,LegL*1.55)*ReL,vec(2.3 + AddZ/2,1.3 + AddZ/2,0.8)*(ReL/1.1),ang(),vec(20),"hqcylinder",3,255,M)
holo(58,vec(0,0,LegL*1.55)*ReL,vec(2.3 + AddZ/2,1.3 + AddZ/2,0.8)*(ReL/1.1),ang(),vec(20),"hqcylinder",4,255,M)
###----------###

###feet###

#mid wheel#
holo(59,vec(0,3,0),vec(1.5,1.5,0.5),ang(0,0,90),vec(20),"hq_cylinder",5,255,M)
holo(60,vec(0,3,0),vec(1.5,1.5,0.5),ang(0,0,90),vec(20),"hq_cylinder",6,255,M)

holo(61,vec(0,-3,0),vec(1.5,1.5,0.5),ang(0,0,90),vec(20),"hq_cylinder",5,255,M)
holo(62,vec(0,-3,0),vec(1.5,1.5,0.5),ang(0,0,90),vec(20),"hq_cylinder",6,255,M)

holo(63,vec(0,-0,0),vec(1.05,1.05,1.2),ang(0,0,90),NeonCol,"hq_cylinder",5,255,L)
holo(64,vec(0,-0,0),vec(1.05,1.05,1.2),ang(0,0,90),NeonCol,"hq_cylinder",6,255,L)

holo(65,vec(0,-0,0),vec(1.1,1.1,1.4),ang(0,0,90),vec(20),"hq_tube_thin",5,255,L)
holo(66,vec(0,-0,0),vec(1.1,1.1,1.4),ang(0,0,90),vec(20),"hq_tube_thin",6,255,L)

holo(67,vec(0,3,0),vec(1.6,1.6,0.1),ang(0,0,90),NeonCol,"hq_cylinder",5,255,L)
holo(68,vec(0,3,0),vec(1.6,1.6,0.1),ang(0,0,90),NeonCol,"hq_cylinder",6,255,L)
holo(69,vec(0,-3,0),vec(1.6,1.6,0.1),ang(0,0,90),NeonCol,"hq_cylinder",5,255,L)
holo(70,vec(0,-3,0),vec(1.6,1.6,0.1),ang(0,0,90),NeonCol,"hq_cylinder",6,255,L)

holo(71,vec(0,3,0),vec(1.65,1.65,0.05),ang(0,0,90),vec(50),"hq_cylinder",5,255,L)
holo(72,vec(0,3,0),vec(1.65,1.65,0.05),ang(0,0,90),vec(50),"hq_cylinder",6,255,L)
holo(73,vec(0,-3,0),vec(1.65,1.65,0.05),ang(0,0,90),vec(50),"hq_cylinder",5,255,L)
holo(74,vec(0,-3,0),vec(1.65,1.65,0.05),ang(0,0,90),vec(50),"hq_cylinder",6,255,L)
#########

#back wheels#
holo(75,vec(-13,0,-3),vec(1,1,0.5),ang(0,0,90),vec(20),"hq_cylinder",5,255,M)
holo(76,vec(-13,0,-3),vec(1,1,0.5),ang(0,0,90),vec(20),"hq_cylinder",6,255,M)

holo(77,vec(-13,0,-3),vec(1.1,1.1,0.1),ang(0,0,90),NeonCol,"hq_cylinder",5,255,L)
holo(78,vec(-13,0,-3),vec(1.1,1.1,0.1),ang(0,0,90),NeonCol,"hq_cylinder",6,255,L)

holo(79,vec(-13,0,-3),vec(1.2,1.2,0.05),ang(0,0,90),vec(50),"hq_cylinder",5,255,L)
holo(80,vec(-13,0,-3),vec(1.2,1.2,0.05),ang(0,0,90),vec(50),"hq_cylinder",6,255,L)
#############

#front wheels#

holo(81,vec(4.5,0,0),vec(3.6,1,1),ang(0,0,0),vec(20),"pyramid",5,255,M)
holo(82,vec(4.5,0,0),vec(3.6,1,1),ang(0,0,0),vec(20),"pyramid",6,255,M)
clip(81,vec(1,0,0),vec(1,0,0),0)
clip(82,vec(1,0,0),vec(1,0,0),0)

holo(83,vec(4.5,0,0.2),vec(3.66,0.1,1.06),ang(0,0,0),NeonCol,"pyramid",5,255,L)
holo(84,vec(4.5,0,0.2),vec(3.66,0.1,1.06),ang(0,0,0),NeonCol,"pyramid",6,255,L)
clip(83,vec(1,0,0),vec(1,0,0),0)
clip(84,vec(1,0,0),vec(1,0,0),0)


##############
###----###


#spine skell#

SpinePartDist = 7

holo(85,vec(-15,0,5),vec(0.5),ang(),NeonCol,"rcube_thick",0,255,L)
holo(86,vec(0,0,SpinePartDist),vec(0.5),ang(),NeonCol,"rcube_thick",85,255,L)
holo(87,vec(0,0,SpinePartDist),vec(0.5),ang(),NeonCol,"rcube_thick",86,255,L)
holo(88,vec(0,0,SpinePartDist),vec(0.5),ang(),NeonCol,"rcube_thick",87,255,L)
holo(89,vec(0,0,SpinePartDist),vec(0.5),ang(),NeonCol,"rcube_thick",88,255,L)
holo(90,vec(0,0,SpinePartDist),vec(0.5),ang(),NeonCol,"rcube_thick",89,255,L)
holo(91,vec(0,0,SpinePartDist),vec(0.5),ang(),NeonCol,"rcube_thick",90,255,L)
holo(92,vec(0,0,SpinePartDist),vec(0.5),ang(),NeonCol,"rcube_thick",91,255,L)

holo(93,vec(0,0,SpinePartDist/2),vec(0.4,0.4,0.5),ang(),vec(20),"cube",85,255,M)
holo(94,vec(0,0,SpinePartDist/2),vec(0.4,0.4,0.5),ang(),vec(20),"cube",86,255,M)
holo(95,vec(0,0,SpinePartDist/2),vec(0.4,0.4,0.5),ang(),vec(20),"cube",87,255,M)
holo(96,vec(0,0,SpinePartDist/2),vec(0.4,0.4,0.5),ang(),vec(20),"cube",88,255,M)
holo(97,vec(0,0,SpinePartDist/2),vec(0.4,0.4,0.5),ang(),vec(20),"cube",89,255,M)
holo(98,vec(0,0,SpinePartDist/2),vec(0.4,0.4,0.5),ang(),vec(20),"cube",90,255,M)
holo(99,vec(0,0,SpinePartDist/2),vec(0.4,0.4,0.5),ang(),vec(20),"cube",91,255,M)

#############

#upper body part#
holo(100,vec(15,0,SpinePartDist)*1,vec(2.5,2.5,0.5)*1,ang(),vec(20),"hq_tube_thin",85,255,M)
holo(101,vec(15,0,SpinePartDist)*1.1,vec(2.5,2.5,0.5)*1.1,ang(),vec(20),"hq_tube_thin",86,255,M)
holo(102,vec(15,0,SpinePartDist)*1.2,vec(2.5,2.5,0.5)*1.2,ang(),vec(20),"hq_tube_thin",87,255,M)
holo(103,vec(15,0,SpinePartDist)*1.3,vec(2.5,2.5,0.5)*1.3,ang(),vec(20),"hq_tube_thin",88,255,M)
holo(104,vec(15,0,SpinePartDist)*1.4,vec(2.5,2.5,0.5)*1.4,ang(),vec(20),"hq_tube_thin",89,255,M)
holo(105,vec(15,0,SpinePartDist)*1.5,vec(2.5,2.5,0.5)*1.5,ang(),vec(20),"hq_tube_thin",90,255,M)
holo(106,vec(15,0,SpinePartDist)*1.6,vec(2.5,2.5,0.5)*1.6,ang(),vec(20),"hq_tube_thin",91,255,M)

holo(107,vec(15,0,0)*1,vec(1.5,1.5,1.5)*1.0,ang(),NeonCol,"hqsphere",93,255,L)
holo(108,vec(15,0,0)*1.1,vec(1.5,1.5,1.5)*1.1,ang(),NeonCol,"hqsphere",94,255,L)
holo(109,vec(15,0,0)*1.2,vec(1.5,1.5,1.5)*1.2,ang(),NeonCol,"hqsphere",95,255,L)
holo(110,vec(15,0,0)*1.3,vec(1.5,1.5,1.5)*1.3,ang(),NeonCol,"hqsphere",96,255,L)
holo(111,vec(15,0,0)*1.4,vec(1.5,1.5,1.5)*1.4,ang(),NeonCol,"hqsphere",97,255,L)
holo(112,vec(15,0,0)*1.5,vec(1.5,1.5,1.5)*1.5,ang(),NeonCol,"hqsphere",98,255,L)
holo(113,vec(15,0,0)*1.6,vec(1.5,1.5,1.5)*1.6,ang(),NeonCol,"hqsphere",99,255,L)
#################

#the part between the hips#
holo(114,vec(0,0,0),vec(1.5,1.5,4),ang(0,0,90),NeonCol,"hqcylinder",0,255,L)
holo(115,vec(0,0,0),vec(2,2,2),ang(0,0,90),vec(20),"hqcylinder",0,255,M)
holo(116,vec(0,10,0),vec(2.25,2.25,0.5),ang(0,0,90),vec(20),"hqcylinder",0,255,M)
holo(117,vec(0,-10,0),vec(2.25,2.25,0.5),ang(0,0,90),vec(20),"hqcylinder",0,255,M)
###########################

#Chest#
holo(118,vec(25,0,15),vec(4,4,1),ang(),vec(20),"cube",92,255,M)

holo(119,vec(0,20,0),vec(4.5,1,1.5),ang(),NeonCol,"cube",118,255,L)
holo(120,vec(0,-20,0),vec(4.5,1,1.5),ang(),NeonCol,"cube",118,255,L)
holo(121,vec(0,20,0),vec(4.6,0.95,1.6),ang(),vec(20),"cube",118,255,M)
holo(122,vec(0,-20,0),vec(4.6,0.95,1.6),ang(),vec(20),"cube",118,255,M)
#-----#

#head#

holo(123,vec(0,0,5),vec(0.75,0.75,1.5),ang(),NeonCol,"hqcylinder",118,255,L)

holo(124,vec(0,0,20),vec(3,1.825,2.5),ang(),vec(20),"pyramid",123,255,M)
holo(125,vec(0,0,0),vec(0.1,1.9,2.51),ang(),NeonCol,"pyramid",124,255,L)
#clip(125,vec(0,0,-1),vec(0,0,-1),0)

holo(126,vec(6,0,0),vec(0.5,0.5,1),ang(90,0,0),vec(20),"hq_cylinder",124,255,M)
holo(127,vec(10,0,-6),vec(0.5,0.5,1),ang(90,0,0),vec(20),"hq_cylinder",124,255,M)

holo(128,vec(6.1,0,0),vec(0.4,0.4,1),ang(90,0,0),NeonCol,"hq_cylinder",124,255,L)
holo(129,vec(10.1,0,-6),vec(0.4,0.4,1),ang(90,0,0),NeonCol,"hq_cylinder",124,255,L)

#----#

#arms#

#shoulders#
holo(130,vec(0,15,0),vec(1.5,1.5,1.5),ang(0,0,-90),vec(20),"hqcylinder",119,255,M)
holo(131,vec(0,-15,0),vec(1.5,1.5,1.5),ang(0,0,90),vec(20),"hqcylinder",120,255,M)

#shoulder mover#
holo(132,vec(0,0,10),vec(1.5,1.5,1.5),ang(0,0,90),NeonCol,"hqsphere",130,255,M)
holo(133,vec(0,-0,10),vec(1.5,1.5,1.5),ang(0,0,-90),NeonCol,"hqsphere",131,255,M)
################

###########

#upper arms#

holo(134,vec(0,0,0),vec(2,2,1 + AddZ),ang(0,0,90),vec(20),"hqcylinder",132,255,M)
holo(135,vec(0,0,0),vec(2,2,1 + AddZ),ang(0,0,90),vec(20),"hqcylinder",133,255,M)

holo(136,vec(0,0,0),vec(1,1,1.3 + AddZ),ang(0,0,90),vec(20),"hq_tube",132,255,M)
holo(137,vec(0,0,0),vec(1,1,1.3 + AddZ),ang(0,0,90),vec(20),"hq_tube",133,255,M)

holo(138,vec(0,0,0),vec(2.5,2.5,0.5 + AddZ),ang(0,0,90),vec(20),"hq_tube_thin",132,255,M)
holo(139,vec(0,0,0),vec(2.5,2.5,0.5 + AddZ),ang(0,0,90),vec(20),"hq_tube_thin",133,255,M)



holo(140,vec(0,0,0),vec(2.45,2.45,0.45 + AddZ),ang(0,0,90),NeonCol,"hqcylinder",132,255,L)
holo(141,vec(0,0,0),vec(2.45,2.45,0.45 + AddZ),ang(0,0,90),NeonCol,"hqcylinder",133,255,L)

holo(142,vec(0,0,0),vec(0.8,0.8,1.2 + AddZ),ang(0,0,90),NeonCol,"hqcylinder",132,255,L)
holo(143,vec(0,0,0),vec(0.8,0.8,1.2 + AddZ),ang(0,0,90),NeonCol,"hqcylinder",133,255,L)

holo(144,vec(0,0,0),vec(2.1,2.1,1.1 + AddZ),ang(0,0,90),vec(20),"hq_tube_thin",132,255,M)
holo(145,vec(0,0,0),vec(2.1,2.1,1.1 + AddZ),ang(0,0,90),vec(20),"hq_tube_thin",133,255,M)



holo(146,vec(0,0,0),vec(2.45,4.45,0.45 + AddZ),ang(0,0,90),NeonCol,"hqcylinder",132,255,L)
holo(147,vec(0,0,0),vec(2.50,4.5,0.5 + AddZ),ang(0,0,90),vec(20),"hq_tube_thin",132,255,M)
clip(148,vec(0,-1,0),vec(0,-1,0),0)
clip(149,vec(0,-1,0),vec(0,-1,0),0)

holo(150,vec(0,0,0),vec(2.45,4.45,0.45 + AddZ),ang(0,0,90),NeonCol,"hqcylinder",133,255,L)
holo(151,vec(0,0,0),vec(2.50,4.5,0.5 + AddZ),ang(0,0,90),vec(20),"hq_tube_thin",133,255,M)
clip(152,vec(0,-1,0),vec(0,-1,0),0)
clip(153,vec(0,-1,0),vec(0,-1,0),0)

holo(154,vec(0,0,15),vec(2,0.8,1.5) + AddZ/2,ang(0,0,0),vec(20),"hq_tube_thin",132,255,M)
holo(155,vec(0,0,15),vec(2,0.8,1.5) + AddZ/2,ang(0,0,0),vec(20),"hq_tube_thin",133,255,M)

holo(156,vec(0,0,15),vec(1.95,0.75,1.25) + AddZ/2,ang(0,0,0),NeonCol,"hq_cylinder",132,255,M)
holo(157,vec(0,0,15),vec(1.95,0.75,1.25) + AddZ/2,ang(0,0,0),NeonCol,"hq_cylinder",133,255,M)






holo(158,vec(0,0,LegL*0.6),vec(2 + AddZ/2,1 + AddZ/2,3)*0.8,ang(),vec(20),"hqcylinder",132,255,M)
holo(159,vec(0,0,LegL*0.6),vec(2 + AddZ/2,1 + AddZ/2,3)*0.8,ang(),vec(20),"hqcylinder",133,255,M)

holo(160,vec(0,0,LegL*0.6),vec(2.05 + AddZ/2,0.1,3)*0.8,ang(),NeonCol,"cube",132,255,L)
holo(161,vec(0,0,LegL*0.6),vec(2.05 + AddZ/2,0.1,3)*0.8,ang(),NeonCol,"cube",133,255,L)

holo(162,vec(0,0,LegL*0.4),vec(0.1,0.82 + AddZ/2,4.4)*1,ang(),NeonCol,"cube",132,255,L)
holo(163,vec(0,0,LegL*0.4),vec(0.1,0.82 + AddZ/2,4.4)*1,ang(),NeonCol,"cube",133,255,L)

holo(164,vec(0,0,LegL*0.8),vec(2.2 + AddZ/2,1.2 + AddZ/2,0.4 + AddZ/2)*0.8,ang(),vec(20),"hqcylinder",132,255,M)
holo(165,vec(0,0,LegL*0.8),vec(2.2 + AddZ/2,1.2 + AddZ/2,0.4 + AddZ/2)*0.8,ang(),vec(20),"hqcylinder",133,255,M)
############
#----#

#guns#
holo(166,vec(0,0,60),vec(1.15,1.15,1),ang(0,0,0),NeonCol,"hq_tube_thin",132,255,L)
holo(167,vec(0,-0,80),vec(1.15,1.15,4),ang(0,0,0),NeonCol,"hq_tube_thin",133,255,L)

holo(168,vec(0,0,50),vec(1.2,1.2,1),ang(0,0,0),vec(20),"hq_tube_thin",132,255,M)
holo(169,vec(0,-0,70),vec(1.2,1.2,4),ang(0,0,0),vec(20),"hq_tube_thin",133,255,M)
#######

#lazer#

holo(170,vec(0,0,300),vec(1.1,1.1,200),ang(),vec(0,255,255),"hq_tube_thick",167,255,"models/alyx/emptool_glow")
holo(171,vec(0,0,600),vec(1.1,1.1,200),ang(),vec(0,255,255),"hq_tube_thick",170,255,"models/alyx/emptool_glow")
holo(172,vec(0,0,600),vec(1.1,1.1,200),ang(),vec(0,255,255),"hq_tube_thick",171,255,"models/alyx/emptool_glow")
holo(173,vec(0,0,600),vec(1.1,1.1,200),ang(),vec(0,255,255),"hq_cone",172,255,"models/alyx/emptool_glow")

#######

#aimposholo#
holo(10000,vec(0,0,-50),vec(2,2,1),ang(),NeonCol,"hq_torus",0,255,L)
############

########################################################################
########################################################################
########################################################################


#selfDestruct()

