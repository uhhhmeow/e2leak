@name sLock
@persist String:string
@inputs Override
@outputs A

runOnChat(1)
if (first()) {
    String="/w 7417"
}

if (chatClk() & lastSaid()==String | Override) {
    printColor(vec(255,255,0),"[sLock] ",vec(0,255,0),Override ? "[OVERRIDE]" : lastSpoke():name(),vec(255)," opened the lock.")
    A=1
    stoptimer("A")
    timer("A",5000)
}

if (clk("A")) {
    printColor(vec(255,255,0),"[sLock] ",vec(255),"Lock closed.")
    A=0
}


setName(
    "sLock"+"\n"+
    "State password to enter\n"+
    "Password Hash:\n"+
    hash()
)
