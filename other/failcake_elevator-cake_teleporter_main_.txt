@name FailCake Elevator-Cake Teleporter Main 
@persist E:entity GTABLE:gtable Wall:entity Effect Up KK Rot Speed RotReset
@trigger all
@model models/props_lab/teleplatform.mdl

interval(100)


function void teleportPlayertoElevator(){
GTABLE["teleporter",entity]:propFreeze(1)   
GTABLE["teleporter",entity]:setPos(GTABLE["DESTINATION_CHAIR",vector]) 
GTABLE["teleporter",entity]:setAng(E:toWorld(ang(0,-90,0)))      
}

function void resetALL(){
print("forcereset")
GTABLE["ForceReset",number] = 1
}



if(first() | duped()){
runOnLast(1)

propSpawnEffect(0)    
Effect = 0  
E = entity()
GTABLE = gTable("sourceLink",2)
GTABLE["ResetMT",number] = 0
GTABLE["teleporter",entity]:propNotSolid(0)

holoCreate(1)  
holoModel(1,"models/props_lab/teleportring.mdl")
holoPos(1,E:toWorld(vec(0,0,20)))
holoAng(1,E:toWorld(ang(0,-180,0)))
holoParent(1,E)

holoCreate(2)  
holoModel(2,"models/props_lab/teleportring.mdl")
holoPos(2,E:toWorld(vec(0,0,30)))
holoAng(2,E:toWorld(ang(0,-180,0)))
holoParent(2,E)

holoCreate(3)  
holoModel(3,"models/props_lab/teleportring.mdl")
holoPos(3,E:toWorld(vec(0,0,40)))
holoAng(3,E:toWorld(ang(0,-180,0)))
holoParent(3,E)  


## Grid
holoCreate(4)  
holoModel(4,"models/props_lab/teleportgate.mdl")
holoPos(4,E:toWorld(vec(53,0,30)))
holoAng(4,E:toWorld(ang(0,0,0)))
holoParent(4,E)  


Wall = propSpawn("models/props_building_details/Storefront_Template001a_Bars.mdl",E:toWorld(vec(55,0,50)),ang(0,0,0),1)
Wall:setAlpha(0)
GTABLE["teleporter",entity]:setPos(E:toWorld(vec(0,0,15)))    
GTABLE["teleporter",entity]:setAng(E:toWorld(ang(0,-90,0)))  
}

if(GTABLE["ResetMT",number]){
print("* Teleporter Body Reseted! *")
GTABLE["ResetMT",number] = 0
reset() 
}

if(GTABLE["BOK",number] == 1){
if(changed(GTABLE["BActive",number]) & GTABLE["BActive",number] == 1){
Effect = 1 
holoEntity(4):soundPlay(5,0,"doors/metal_move1.wav")
}

if(changed(GTABLE["BStart",number]) & GTABLE["BStart",number] == 1){
Effect = 2 
holoEntity(4):soundPlay(5,0,"doors/metal_move1.wav")
}
}



if(Effect == 1){
    
if(Up < 50){
Up+=5  
holoPos(4,E:toWorld(vec(53,0,30 + Up)))   
}else{
Effect = 0
Wall:propNotSolid(1)
holoEntity(4):soundPlay(5,3,"doors/metal_stop1.wav")
}
   
}elseif(Effect == 2){

if(Up > 0){
Up-=5
holoPos(4,E:toWorld(vec(53,0,30 + Up)))   
}else{
Effect = 3
Up = 0
Wall:propNotSolid(0)
holoEntity(4):soundPlay(5,3,"doors/metal_stop1.wav")
}

}elseif(Effect == 3){

if(Speed < 150){
Speed+=1   
}

if(Speed == 100){
E:soundPlay(8,0,"ambient/levels/labs/teleport_postblast_thunder1.wav")     
}

if(Speed == 110){
E:soundPlay(9,0,"HL1/ambience/particle_suck2.wav")     
}

if(Speed == 150){
teleportPlayertoElevator()
GTABLE["teleporter",entity]:propNotSolid(1)
timer("resettele",3000)
soundStop(9)
soundStop(8)
soundStop(6)
soundStop(7)
Effect = 0
Speed = 0
RotReset = 1
E:soundPlay(9,0,"ambient/machines/teleport"+randint(3,4)+".wav")     
}

if(Speed == 2){
E:soundPlay(6,0,"ambient/levels/labs/teleport_rings_loop2.wav")    
E:soundPlay(7,0,"ambient/levels/labs/machine_ring_resonance_loop1.wav")   
}

Rot+=Speed

GTABLE["teleporter",entity]:setAng(E:toWorld(ang(0,-90+Rot*2,0)))  
holoAng(1,ang(0,Rot,0))
holoAng(2,ang(0,Rot/0.5,0))
holoAng(3,ang(0,Rot/1.5,0))

}

if(RotReset){
 
if(Speed > 1){
Speed-=3
}

if(Rot < 0){
Rot-=Speed
}else{
Rot = 0
RotReset = 0
}

holoAng(1,ang(0,Rot,0))
holoAng(2,ang(0,Rot/0.5,0))
holoAng(3,ang(0,Rot/1.5,0))   
}

if(clk("resettele")){
GTABLE["KillPly",number] = 1 
GTABLE["teleporter",entity]:propNotSolid(0)
}

if(GTABLE["ResetAs",number]){
GTABLE["ResetAs",number] = 0
resetALL()    
}


if(last()){
Wall:propDelete()    
}
