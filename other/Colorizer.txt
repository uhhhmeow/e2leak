@name Colorizer
@inputs Entity:entity

interval(100)
Entity:setColor(hsv2rgb((curtime()*100%360),1,1))
Entity:setMaterial("debug/debugdrawflat")
