if (first()) {
    holoCreate(1,entity():toWorld(vec(0,0,30)))
    holoModel(1,"hq_icosphere")
    holoScale(1,vec(-1,-1,-1))
    holoMaterial(1,"debug/debugdrawflat")
}
interval(100)
#holoColor(1,vec(random(256),random(256),random(256)))
holoPos(1,holoEntity(1):pos()+vec(0,0,1))
