@name Admin Minutes Game
@inputs Stop
@outputs Length
@persist Stopped

if(~Stop && Stop) {Stopped=1}

if(!Stopped) {Length=randint(1440*365)}

interval(1)
