@name Polymesh_Weed
@inputs Scr:wirelink

if(first()) {
    
    # Scriptis - EGP:drawWeed(Index,Offset,Scale)
    
    function number wirelink:drawWeed(Index:number,Offset:vector2,Scale:vector2) {
        local BaseCenter=vec2(-90.833,-41.334)
        
        local Map=table(
            # Weed
            table(
                array(vec2(90.833,194.667),vec2(107.5,190.042),vec2(132,214.167)),
                array(vec2(132,214.167),vec2(107.5,190.042),vec2(155.333,192.354)),
                array(vec2(132,214.167),vec2(155.333,192.354),vec2(178.667,222.333)),
                array(vec2(155.333,192.354),vec2(178,196.75),vec2(178.667,222.333)),
                array(vec2(192.5,172.5),vec2(178.667,222.333),vec2(178,196.75)),
                array(vec2(208.875,229),vec2(178.667,222.333),vec2(192.5,172.5)),
                array(vec2(190,238.667),vec2(178.667,222.333),vec2(208.875,229)),
                array(vec2(170,247.167),vec2(178.667,222.333),vec2(190,238.667)),
                array(vec2(170,247.167),vec2(156.833,230.834),vec2(178.667,222.333)),
                array(vec2(145.333,251),vec2(156.833,230.834),vec2(170,247.167)),
                array(vec2(145.333,251),vec2(135.167,244.167),vec2(156.833,230.834)),
                array(vec2(126.833,251.667),vec2(135.167,244.167),vec2(145.333,251)),
                array(vec2(242,200.334),vec2(208.875,229),vec2(192.5,172.5)),
                array(vec2(214.5,230.834),vec2(208.875,229),vec2(242,200.334)),
                array(vec2(208.875,229),vec2(214.5,230.834),vec2(214.5,231)),
                array(vec2(208.875,229),vec2(214.5,231),vec2(214.5,242)),
                array(vec2(214.5,242),vec2(208,247.167),vec2(208.875,229)),
                array(vec2(217.25,261.334),vec2(208,247.167),vec2(214.5,242)),
                array(vec2(217.25,261.334),vec2(209.75,261.334),vec2(208,247.167)),
                array(vec2(214.5,274.667),vec2(209.75,261.334),vec2(217.25,261.334)),
                array(vec2(214.5,274.667),vec2(217.25,261.334),vec2(220,272.334)),
                array(vec2(248,223),vec2(214.5,230.834),vec2(242,200.334)),
                array(vec2(214.5,230.834),vec2(248,223),vec2(239.333,242)),
                array(vec2(239.333,242),vec2(248,223),vec2(283.333,238.667)),
                array(vec2(283.333,252.334),vec2(239.333,242),vec2(283.333,238.667)),
                array(vec2(283.333,252.334),vec2(283.333,238.667),vec2(298.333,251)),
                array(vec2(248,223),vec2(242,200.334),vec2(271.833,193.167)),
                array(vec2(248,223),vec2(271.833,193.167),vec2(283.333,217.334)),
                array(vec2(283.333,217.334),vec2(271.833,193.167),vec2(304,189)),
                array(vec2(283.333,217.334),vec2(304,189),vec2(313.333,206)),
                array(vec2(313.333,206),vec2(304,189),vec2(333.667,191.084)),
                array(vec2(313.333,206),vec2(333.667,191.084),vec2(340.333,193.167)),
                array(vec2(242,200.334),vec2(192.5,172.5),vec2(230.667,173.334)),
                array(vec2(242,200.334),vec2(230.667,173.334),vec2(260.333,186)),
                array(vec2(230.667,173.334),vec2(260.333,136),vec2(260.333,186)),
                array(vec2(283.333,160),vec2(260.333,186),vec2(260.333,136)),
                array(vec2(300.667,132),vec2(283.333,160),vec2(260.333,136)),
                array(vec2(300.667,132),vec2(260.333,136),vec2(288,116)),
                array(vec2(300.667,132),vec2(288,116),vec2(313.333,102.667)),
                array(vec2(192.5,172.5),vec2(230.5,160),vec2(230.667,173.334)),
                array(vec2(230.5,160),vec2(192.5,172.5),vec2(192.5,112)),
                array(vec2(192.5,112),vec2(230.5,119),vec2(230.5,160)),
                array(vec2(192.5,112),vec2(227,92),vec2(230.5,119)),
                array(vec2(227,92),vec2(192.5,112),vec2(205,57)),
                array(vec2(227,92),vec2(205,57),vec2(220,63.334)),
                array(vec2(205,57),vec2(214.5,41.334),vec2(220,63.334)),
                array(vec2(230.667,119),vec2(230.5,119),vec2(227,92)),
                array(vec2(192.5,172.5),vec2(178,196.75),vec2(192.333,172.5)),
                array(vec2(192.333,172.5),vec2(178,196.75),vec2(150.5,172)),
                array(vec2(150.5,172),vec2(170,143.25),vec2(192.333,172.5)),
                array(vec2(150.5,172),vec2(120.667,127.673),vec2(170,143.25)),
                array(vec2(170,143.25),vec2(120.667,127.673),vec2(111.667,102.667))
            )
        )
        
        local Drawn=0
        local Components=Map:count()
        local Topmost=9e99
        local Leftmost=9e99
        local Rightmost=0
        local Bottommost=0
        local ScaleStroke=Scale+vec2(0.1)
        local Abs=vec2()
        
        for(ID=1,Components) {
            local Part=Map[ID,table]
            local PartCount=Part:count()
            Drawn+=PartCount
            for(I=1,PartCount) {
                local Triangle=Part[I,array]
                Index++
                
                Scr:egpTriangle(
                    Index,
                    (Triangle[1,vector2]+BaseCenter)*Scale+Offset,
                    (Triangle[2,vector2]+BaseCenter)*Scale+Offset,
                    (Triangle[3,vector2]+BaseCenter)*Scale+Offset
                )
                
                Abs+=((Triangle[1,vector2]+Triangle[2,vector2]+Triangle[3,vector2])/3)
            }
        }
        
        for(I=(Index-Drawn),Index) {
            # Scr:egpColor(I,hsv2rgb((((Index-I)/Drawn)*360),1,1))
            # Scr:egpColor(I,randvec(vec(250,250,0),vec(255,255,0)))
            # Scr:egpMaterial(I,"gui/gradient_up")
            # Scr:egpColor(I,vec4(50,50,1,200))
            Scr:egpColor(I,vec(1))
        }
        
        return Drawn
    }
}

Scr:egpClear()

Scr:egpBox(1,vec2(85,256),vec2(174,512))
Scr:egpColor(1,vec(198,24,15))

Scr:egpBox(2,vec2(427,256),vec2(174,512))
Scr:egpColor(2,vec(2,127,83))

Scr:egpBox(3,vec2(256),vec2(174,512))
Scr:egpColor(3,vec(225,187,62))

Scr:egpBox(4,vec2(256),vec2(512))
Scr:egpMaterial(4,"gui/gradient_up")
Scr:egpColor(4,vec4(1,1,1,200))

Scr:drawWeed(5,vec2(108.43,95.76)*(1.25*0.75),vec2(1.25))
