@name Party Floor
@persist Pixels:array

if(first()) {
    findByClass("gmod_wire_pixel")
    local All=findToArray()
    foreach(I,V:entity=All) {Pixels[I,wirelink]=V:wirelink()}
}

interval(1000)

foreach(I,V:wirelink=Pixels) {
    local Col=hsv2rgb(randint(360),0.8,0.8)
    V["Red",number]=Col:x()
    V["Green",number]=Col:y()
    V["Blue",number]=Col:z()
}
