@name Laser with Menu
@persist 

#include "lib/button"
#include "lib/variable"
#include "mini_laser"

if( first() ) {
    
    "mroot":n( randint(2e8) )
    holoCreate( "mroot":n() )
    holoScaleUnits( "mroot":n(), vec( 8, 22, 0.001 ) )
    holoColor( "mroot":n(), vec() )
    holoParent( "mroot":n(), entity() )
    
    holoCreate( "mroot":n() + 1 )
    holoScaleUnits( "mroot":n() + 1, vec( 2, 22, -0.1 ) )
    holoPos( "mroot":n() + 1, entity():pos() + vec(-5.8, 0, 0) )
    holoColor( "mroot":n() + 1, vec() )
    holoParent( "mroot":n() + 1, "mroot":n() )
    
    async( 0.25, "menuCreate" )
    
    "plyindex":n( 1 )
    "player":e( owner() )
    
    buttoncallback_bind( "button" )
    function button( Ply:entity, Btn:string ) {
        if( "left":s() == Btn ) {
            "plyindex":n( "plyindex":n() - 1 )
            if( "plyindex":n() <= 0 ) { "plyindex":n( numPlayers() ) }
            "player":e( players()["plyindex":n(), entity] )
            "name":s():htextSetText( "player":e():name() )
        }
        
        if( "right":s() == Btn ) {
            "plyindex":n( "plyindex":n() + 1 )
            if( "plyindex":n() > numPlayers() ) { "plyindex":n( 1 ) }
            "player":e( players()["plyindex":n(), entity] )
            "name":s():htextSetText( "player":e():name() )
        }
        local Plt = holoEntity( "mroot":n() + 1 )
        "name":s():htextSetPosition( Plt:pos() )
        "name":s():htextSetAngle( Plt:angles() + ang(-90,0,0) )
        "name":s():htextSetScale( vec(0.075):setY(0.001) )
        "name":s():htextSetParent( holoEntity( "mroot":n() ) )
        "name":s():htextSetMaterial( "debug/debugdrawflat" )
        "name":s():htextSetColor( teamColor( "player":e():team() ) )
        
        if( "fire":s() == Btn ) {
            
            local X = propSpawn( "models/hunter/blocks/cube025x025x025.mdl", holoEntity(7):pos(), holoEntity(7):angles(), 1 )
            X:setAlpha( 0 )
            X:setMass( 5000 )
            
            X:soundPlay( 1, 0, "acf_other/penetratingshots/00000297.wav" )
            soundPitch( 1, 100 )
            soundVolume( 1, 1000 )
            
            local I = randint(2e8)
            holoCreate(I)
            holoPos( I, X:pos() + (X:right() * 10) )
            holoAlpha( I, 0 )
            holoParent( I, X )
            holoEntity(I):setTrails( 32,32,1, "trails/laser", vec(0,0,255), 255 )
            
            holoCreate(I+1)
            holoPos( I+1, X:pos() + (X:right() * -10) )
            holoAlpha( I+1, 0 )
            holoParent( I+1, X )
            holoEntity(I+1):setTrails( 32,32,1, "trails/laser", vec(0,255,0), 255 )
            
            X:propGravity( 0 )
            X:setAlpha( 0 )
            X:propFreeze( 0 )
            X:applyForce( X:mass() * X:forward() * 5000 )
            X:applyAngForce( ang(0,0,1) * X:mass() * 450 )
            
            async( 7, "propDelete", X )
        }
    }
    
    bindcallback_keydown( "keyboard" )
    function keyboard( Ply:entity, Key:string ) {
        if( Key == "mouse_right" ) {
            holoPos( "mroot":n(), Ply:shootPos() + ( Ply:eye() * 40 ) )
            holoAng( "mroot":n(), ( Ply:shootPos() - holoEntity( "mroot":n() ):pos() ):toAngle() + ang(90,0,0) )
            holoPos( 1, holoEntity("mroot":n()+1):pos() + vec(0,0,2) )
        }
    }
    
    function menuCreate() {
        local Material = array( "models/cheeze/buttons2/left", "models/cheeze/buttons2/fire", "models/cheeze/buttons2/right" )
        local Button = array( "left", "fire", "right" )
        local Position = entity():pos() - vec(0,14,0)
        for( I = 1, 3 ) {
            Button[I, string]:s( buttonCreate() )
            Button[I, string]:s():buttonWhitelist( owner() )
            #Button[I, string]:s():buttonWhitelist( findPlayerByName("Tek") )
            Button[I, string]:s():buttonColorOn( vec(255) )
            Button[I, string]:s():buttonColorOff( vec(150) )
            Button[I, string]:s():buttonSetMaterialOn( Material[I, string] )
            Button[I, string]:s():buttonSetMaterialOff( Material[I, string] )
            Button[I, string]:s():buttonSetPosition( Position + vec(0,7*I,0) )
            Button[I, string]:s():buttonSetScale( vec(0.5) )
            holoParent( Button[I, string]:s():toNumber(), "mroot":n() )
        }
        
        "name":s( htextCreate() )
        "name":s():htextSetText( "Player" )
        "name":s():htextSetPosition( entity():pos() + vec(-6, 0, 0.01) )
        "name":s():htextSetAngle( ang(-90,0,0) )
        "name":s():htextSetScale( vec(0.075):setY(0.001) )
        "name":s():htextSetParent( holoEntity( "mroot":n() ) )
    }
}

if( tickClk() ) {
    laserAim( "player":e() )
}
