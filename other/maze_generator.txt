@name Maze Generator
@inputs DS:wirelink MazeGen Down Left Right Up Mouse1 Mouse2 Shift Space Random
@outputs Clk Percent Resolution Minutes Seconds TotalPercent
@persist Character CurCells Cells Current Finished Game Iterator Maze:array Method Start TempCells Time Visited:array

if(first() | duped())
{
Resolution = 31
}

if(first() | duped() | clk("halt"))
{
runOnTick(1)
Cells = 0
CurCells = 0
Clk = 0
Finished = 1
Game = 0
Start = 0
Method = 0
Percent = 0
Time = 0
TotalPercent = 0
DS:writeCell(1048574, 1)
DS:writeCell(1048572, Resolution)
DS:writeCell(1048573, Resolution)
stoptimer("time")
}

if(clk("time"))
{
Time++
Minutes = floor(Time / 60)
Seconds = Time % 60

timer("time", 1000)
}



if(clk("win"))
{
Resolution += 2
timer("reset", 0)
}

if($Random & Method == 5)
{
if(Iterator > 1)
{
Finished = 0
Current = Maze[Iterator, number]
if(Current - (Resolution * 2) > Resolution + 1 & !Visited[Current - (Resolution * 2), number] | Current + (Resolution * 2) < Resolution ^ 2 - 1 & !Visited[Current + (Resolution * 2), number] | (Current % (Resolution - 1)) + 2 < Resolution - 1 & !Visited[Current + 2, number] | (Current % (Resolution - 1)) - 2 > 0 & !Visited[Current - 2, number])
{
Method = randint(1, 4)
}
else
{
Iterator--
}
}
else
{
Clk = 0
Finished = 1
Game = 1
DS:writeCell(Resolution + 1, 191000)
DS:writeCell(Maze[Maze:count(), number], 900000)
Character = Resolution + 1
Method = 0
timer("time", 1000)
}
}

if($Random & Method == 1)
{
if(Current - (Resolution * 2) > Resolution + 1 & !Visited[Current - (Resolution * 2), number] | (Current % (Resolution - 1)) + 2 < Resolution - 1 & !Visited[Current + 2, number] | (Current % (Resolution - 1)) - 2 > 0 & !Visited[Current - 2, number])
{
if(Current - (Resolution * 2) > Resolution + 1 & !Visited[Current - (Resolution * 2), number])
{
Current -= Resolution * 2
Iterator++
Cells++
CurCells++
Maze[Iterator, number] = Current
Visited[Current, number] = 1
DS:writeCell(Current + Resolution, 0)
Method = randint(1, 4)
}
else
{
Method = randint(1, 4)
}
}
else
{
Method = 5
}
}

if($Random & Method == 2)
{
if(Current + (Resolution * 2) < Resolution ^ 2 - 1 & !Visited[Current + (Resolution * 2), number] | (Current % (Resolution - 1)) + 2 < Resolution - 1 & !Visited[Current + 2, number] | (Current % (Resolution - 1) - 2) > 0 & !Visited[Current - 2, number])
{
if(Current + (Resolution * 2) < Resolution ^ 2 - 1 & !Visited[Current + (Resolution * 2), number])
{
Current += Resolution * 2
Iterator++
Cells++
CurCells++
Maze[Iterator, number] = Current
Visited[Current, number] = 1
DS:writeCell(Current - Resolution, 0)
Method = randint(1, 4)
}
else
{
Method = randint(1, 4)
}
}
else
{
Method = 5
}
}

if($Random & Method == 3)
{
if(Current - (Resolution * 2) > Resolution + 1 & !Visited[Current - (Resolution * 2), number] | Current + (Resolution * 2) < Resolution ^ 2 - 1 & !Visited[Current + (Resolution * 2), number] | (Current % (Resolution - 1)) + 2 < Resolution - 1 & !Visited[Current + 2, number])
{
if((Current % (Resolution - 1)) + 2 < Resolution - 1 & !Visited[Current + 2, number])
{
Current += 2
Iterator++
Cells++
CurCells++
Maze[Iterator, number] = Current
Visited[Current, number] = 1
DS:writeCell(Current - 1, 0)
Method = randint(1, 4)
}
else
{
Method = randint(1, 4)
}
}
else
{
Method = 5
}
}

if($Random & Method == 4)
{
if(Current - (Resolution * 2) > Resolution + 1 & !Visited[Current - (Resolution * 2), number] | Current + (Resolution * 2) < Resolution ^ 2 - 1 & !Visited[Current + (Resolution * 2), number] | (Current % (Resolution - 1)) - 2 > 0 & !Visited[Current - 2, number])
{
if((Current % (Resolution - 1)) - 2 > 0 & !Visited[Current - 2, number])
{
Current -= 2
Iterator++
Cells++
CurCells++
Maze[Iterator, number] = Current
Visited[Current, number] = 1
DS:writeCell(Current + 1, 0)
Method = randint(1, 4)
}
else
{
Method = randint(1, 4)
}
}
else
{
Method = 5
}
}

if(Start & $Random)
{
if(Current < Resolution ^ 2)
{
Current++
CurCells++
if(Current / 2 == floor(Current / 2) | floor(Current / Resolution) % 2 == 0 | Current % Resolution == 0 | Current % Resolution == Resolution)
{
Visited[Current - 1, number] = 1
Cells++
DS:writeCell(Current - 1, 134000)
}
}
else
{
Start = 0
Current = Resolution + 1
TempCells = Cells
CurCells = 0
Visited[Current, number] = 1
Iterator = 0
Method = randint(2, 3)
Percent = 0
}
}

if(~MazeGen & MazeGen | clk("reset"))
{
Cells = 0
CurCells = 0
Current = 0
Finished = 0
Game = 0
Maze = array()
Visited = array()
Clk = 1
TempCells = 0
Start = 1
Time = 0
TotalPercent = 0
DS:writeCell(1048574, 1)
DS:writeCell(1048572, Resolution)
DS:writeCell(1048573, Resolution)
stoptimer("time")
}

Iterator = clamp(Iterator, 1, Iterator)

if(Finished)
{
if(Game & ~Up & Up)
{
if(!DS:readCell(Character - Resolution) | DS:readCell(Character - Resolution) != 134000)
{
DS:writeCell(Character, 0)
DS:writeCell(Resolution + 1, 892000)
if(DS:readCell(Character - Resolution) == 900000)
{
timer("win", 0)
}
else
{
DS:writeCell(Maze[Maze:count(), number], 900000)
Character -= Resolution
DS:writeCell(Character, 191000)
}
}
}
elseif(Game & ~Down & Down)
{
if(!DS:readCell(Character + Resolution) | DS:readCell(Character + Resolution) != 134000)
{
DS:writeCell(Character, 0)
DS:writeCell(Resolution + 1, 892000)
if(DS:readCell(Character + Resolution) == 900000)
{
timer("win", 0)
}
else
{
DS:writeCell(Maze[Maze:count(), number], 900000)
Character += Resolution
DS:writeCell(Character, 191000)
}
}
}
elseif(Game & ~Right & Right)
{
if(!DS:readCell(Character + 1) | DS:readCell(Character + 1) != 134000)
{
DS:writeCell(Character, 0)
DS:writeCell(Resolution + 1, 892000)
if(DS:readCell(Character + 1) == 900000)
{
timer("win", 0)
}
else
{
DS:writeCell(Maze[Maze:count(), number], 900000)
Character += 1
DS:writeCell(Character, 191000)
}
}
}
elseif(Game & ~Left & Left)
{
if(!DS:readCell(Character - 1) | DS:readCell(Character - 1) != 134000)
{
DS:writeCell(Character, 0)
DS:writeCell(Resolution + 1, 892000)
if(DS:readCell(Character - 1) == 900000)
{
timer("win", 0)
}
else
{
DS:writeCell(Maze[Maze:count(), number], 900000)
Character -= 1
DS:writeCell(Character, 191000)
}
}
}
}

if(Finished & ~Mouse1 & Mouse1)
{
if(Shift)
{
Resolution = clamp(Resolution - 10, 9, 511)
}
else
{
Resolution = clamp(Resolution - 2, 9, 511)
}
}
elseif(Finished & ~Mouse2 & Mouse2)
{
if(Shift)
{
Resolution = clamp(Resolution + 10, 9, 511)
}
else
{
Resolution = clamp(Resolution + 2, 9, 511)
}
}
elseif(~Space & Space)
{
timer("halt", 0)
}

Percent = !Finished ? floor((CurCells / ((Resolution ^ 2) - TempCells)) * 100) : 100
TotalPercent = !Finished ? floor((Cells / (Resolution ^ 2)) * 100) : 100

Minutes = floor(Time / 60)
Seconds = Time % 60
