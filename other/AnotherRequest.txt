@name AnotherRequest
@persist E:entity PlyCheck:table
@trigger all

if(first() || duped()){
    runOnTick(1)
    
    E = entity()
    PlyCheck = table()
}

local Plys = players()
local Count = 0

while(Count < Plys:count()){
    local Ply = Plys[Count,entity]
    
    # If hes dead
    if(Ply:health() <= 0 && PlyCheck[Ply:id():toString(),number] == 0){
        
        PlyCheck[Ply:id():toString(),number] = 1
        print("Player " + Ply:name() + " died holding " + Ply:weapon():type() )
        
    }elseif(PlyCheck[Ply:id():toString(),number] == 1 && Ply:health() > 0){
        PlyCheck[Ply:id():toString(),number] = 0 # Clean it
    }
    
    Count++
}
