@name Maze Gen
@inputs Screen:wirelink Memory:wirelink Reset Run
@outputs Percent Time PPS 
@persist Dir Size LIFO:array Width Color Count Current Cell 
@persist X Y Right Left Up Down
@trigger all

if(Reset|first())
    {
        Screen:writeCell(1048572,1)## reset height of digital screento 1 for reset
        Screen:writeCell(1048573,1)## reset width of digital screen to 1 for reset
        Screen:writeCell(1048574,1) ## reset screen pixels
        X=0
        Y=0  
        Count=0 
        Width=32 ## change this to change screen resolution
        Screen:writeCell(1048572,Width)## height
        Screen:writeCell(1048573,Width)## width
        Current=0 
        Time=0
        

    }   

PPS=Current/Time # pixels per second
Size=Width*2 ## size of the shit
Cell=X+Y*(Size/2) ## the cell position 
Color=200 ## color
Percent=round((Current/Width/(Width/4))*100) ## % done
## check if visited

#right
if(Memory:readCell(Cell+2)|X>=Width-2){Right=1}else{Right=0}

#left
if(Memory:readCell(Cell-2)|X<=1){Left=1}else{Left=0}

#up
if(Memory:readCell(Cell-Width*2)|(Y<=1)){Up=1}else{Up=0}

#down
if(Memory:readCell(Cell+Width*2)|(Y>=Width-2)){Down=1}else{Down=0}

timer("run",100)


if(Run|clk("run")|!Run|!clk("run"))
    {
  
    Dir=round(random(4)) ## pick a random direction to move in
    
        # move right
        if(Dir==1&X<=Width-2&!Right)
           {
            ## writes to screen
             Screen:writeCell(Cell,Color)
             Screen:writeCell(Cell+1,Color)
             Screen:writeCell(Cell+2,Color)
            ## write to memory
             Memory:writeCell(Cell,1)
            ## saves the LIFO
             LIFO:setVector2(Count,vec2(X,Y))
            ## increase stuff
             X+=2
             Count++
             Current++
           }
        
         # move left
        if(Dir==2&X>=2&!Left)
           {
            ## writes to screen
             Screen:writeCell(Cell,Color)
             Screen:writeCell(Cell-1,Color)
             Screen:writeCell(Cell-2,Color)
             ## write to memory
             Memory:writeCell(Cell,1)
            ## saves the LIFO
             LIFO:setVector2(Count,vec2(X,Y))
            ## increase stuff
             X-=2
             Count++
             Current++
           }
        
        # move Down
        if(Dir==3&Y<=Width-2&!Down)
           {
            ## writes to screen
             Screen:writeCell(Cell,Color)
             Screen:writeCell(Cell+Width,Color)
             Screen:writeCell(Cell+Width*2,Color)
            ## write to memory
             Memory:writeCell(Cell,1)
            ## saves the LIFO
             LIFO:setVector2(Count,vec2(X,Y))
            ## increase stuff
             Y+=2
             Count++
             Current++
           }
        
        # move Up
        if(Dir==4&Y>=2&!Up)
           {
            ## writes to screen
             Screen:writeCell(Cell,Color)
             Screen:writeCell(Cell-Width,Color)
             Screen:writeCell(Cell-Width*2,Color)
            ## write to memory
             Memory:writeCell(Cell,1)
            ## saves the LIFO
             LIFO:setVector2(Count,vec2(X,Y))
            ## increase stuff
             Y-=2
             Count++
             Current++
           }
        
    }
        
        ## timer 
        if(Percent<100)
            {   timer("time",100)
                    if(clk("time"))              
                        {                        
                        Time+=0.1                        
                        }
            }
        
        
        ## backtrack
        
        if(Right&Left&Up&Down&Count>0)
            {          
            
             ## write to memory
             Memory:writeCell(Cell,1)
                       
                    ## move back to previous cell untill it finds someone that hasn't been visited
                      if(Run|clk("run")|!Run|!clk("run"))
                        {
                           Count--
                           X=LIFO:vector2(Count):x()
                           Y=LIFO:vector2(Count):y()
                        }
                      

        }
