@name sRadioLive
@persist [Owner Entity]:entity
@model models/props_lab/citizenradio.mdl

if(first()) {
    Owner=owner()
    Entity=entity()
    Entity:setMass(1)
    
    # streamDisable3D(0)
    # streamVolume(1,1)
    # streamRadius(1,100)
    
    # Entity:streamStart(1,"http://50.7.173.162:8010/") # audiophile baroque
    # Entity:streamStart(1,"http://radiorivendell.de:80/") # radio rivendell
    # Entity:streamStart(1,"http://205.164.62.15:10052") # love classics
    # Entity:streamStart(1,"http://streaming202.radionomy.com:80/abacusfm-vintage-jazz") # vintage jazz
    # Entity:streamStart(1,"http://91.121.166.38:7016/") # british comedy
    # Entity:streamStart(1,"http://idobiradio.idobi.com") # idobi
    # Entity:streamStart(1,"http://theradio.cc:8000/trcc-stream.mp3") # theradiocc
    # Entity:streamStart(1,"https://www.dropbox.com/s/ah8gpgxho1ot7ys/Eternal%20Spirit.mp3?dl=1")
    # Entity:streamStart(1,"https://www.dropbox.com/s/ccx16iys92sql3v/BadLiZ%20-%20The%20Great%20Strategy%20v2.mp3?dl=1")
    # Entity:streamStart(1,"http://canadiansturf.com/audio/Toby_Fox-Megalovania.mp3")
    # Entity:streamStart(1,"canadiansturf.com/audio/Billy_Joel-Pianoman.mp3")
    # Entity:streamStart(1,"canadiansturf.com/audio/Marty_Robbins-Big_Iron.mp3")
    # Entity:streamStart(1,"http://canadiansturf.com/audio/Ronald_Jenkees-Sidetracked.mp3")
    # Entity:streamStart(1,"https://www.dropbox.com/s/buyn2r16gyjam8u/Grieves%20Sunny%20Side%20of%20Hell%20-%20Bobby%20Hebb%20Sunny%20%281%29.mp3?dl=1")
    # Entity:streamStart(1,"https://api.soundcloud.com/tracks/183130838/stream?client_id=e9c79d9965fc8f6b6469d78bda30b074")
    
    # http://api.mavain.com/getSoundcloud.php?f=https://soundcloud.com/arcien/akira-complex-helios-arcien-remix

}
