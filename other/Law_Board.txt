@name Law Board
@inputs Scr:wirelink
@persist DefaultLaws:array MasterChip
runOnChat(1)
Auth=gTable("SAuthSystem")
Laws=gTable("SLawsSystem")
Descs=gTable("SDescriptionSystem")
Master=gTable("SMasterChip")
if (!Master[1,entity]:isValid()) {
    Master[1,entity]=entity()
    print("Claimed master-chip status!")
}
if (first()) {
    function number isAuth(E:entity) {
        #print(E:name())
        for (I=1, Auth:count()) {
            if (E==Auth[I,entity]) {
                return 1
            }
        }
    return 0
    }
    function deAuth(E:entity) {
        local NewTable=array()
        for (I=1, Auth:count()) {
            if (Auth[I,entity]!=E) {
                NewTable[NewTable:count()+1,entity]=Auth[I,entity]
            }
        }
        Auth:clear()
        for (I=1, NewTable:count()) {
            Auth[I,entity]=NewTable[I,entity]
        }
    }
    function removeLaw(E) {
        local NewTable=array()
        for (I=1, Laws:count()) {
            if (I!=E) {
                NewTable[NewTable:count()+1,string]=Laws[I,string]
            }
        }
        Laws:clear()
        for (I=1, NewTable:count()) {
            Laws[I,string]=NewTable[I,string]
        }
    }
    DefaultLaws=array(
        "Weapons/raiding tools are illegal.",
        "Loud noises, singing, bugbait, and micspam are illegal.",
        "Do not disobey/direspect officers.",
        "No loitering about or entering the PD.",
        "No advertising of any organization.",
        "Officers are exempt from the laws where the !rules aren't broken."
    )
}

Ply=lastSpoke()
if (Master[1,entity]==entity() & (isAuth(Ply) | Ply==owner())) {
    local CMD=Ply:lastSaid()
    if (CMD:sub(1,1)=="-") {
        #print("exec")
        CMD=CMD:sub(2)
        local Args=CMD:explode(" ")
        #print(Args:concat(" "))
        if (Args[1,string]=="auth" & Ply==owner()) {
            local FindName=Args[2,string]:lower()
            local Players=players()
            for (I=1, Players:count()) {
                if (Players[I,entity]:name():lower():matchFirst(FindName)) {
                    print("Authed "+(Players[I,entity]:name())) 
                    Auth[Auth:count()+1,entity]=Players[I,entity]
                    print(Auth:count())
                }
            }
        }
        
        if (Args[1,string]=="deauth" & Ply==owner()) {
            local FindName=Args[2,string]:lower()
            for (I=1, Auth:count()) {
                if (Auth[I,entity]:name():lower():matchFirst(FindName)) {
                    print("Deathorized "+(Auth[I,entity]:name()))
                    deAuth(Auth[I,entity])
                }
            }
        }
        if (Args[1,string]=="desc") {
            Args:removeString(1)
            local Index=Args[1,string]:toNumber()
            Args:removeString(1)
            local NewDesc=Args:concat(" ")
            Descs[Index,string]=NewDesc
        }
            
        if (Args[1,string]=="default" & Ply==owner()) {
            Laws:clear()
            Descs:clear()
            for (I=1, DefaultLaws:count()) {
                Laws[I,string]=DefaultLaws[I,string]
            }
        }
        
        if (Args[1,string]=="add") {
            Args:removeString(1)
            local NewLaw=Args:concat(" ")
            Laws[Laws:count()+1,string]=NewLaw
            print("Added law "+NewLaw+" at "+(Laws:count()))
        }
        if (Args[1,string]=="remove") {
            local RemoveLaw=Args[2,string]:toNumber()
            Descs[RemoveLaw,string]=""
            removeLaw(RemoveLaw)
            print("Removed law #"+RemoveLaw)
        }
        if (Args[1,string]=="mod") {
            Args:removeString(1)
            local Index=Args[1,string]:toNumber()
            Args:removeString(1)
            Laws[Index,string]=Args:concat(" ")
            print("Modified law "+Index)
        }
    }
}
Scr:egpClear()
Scr:egpRoundedBox(1,vec2(256,256),vec2(512,512))
Scr:egpMaterial(1,"skybox/sky_dust_hdrrt.vmt")
Scr:egpAlpha(1,172)
Scr:egpText(2,"LAWS OF THE LAND",vec2(256,10))
Scr:egpAlign(2,1)
Scr:egpSize(2,40)
Scr:egpColor(2,vec(172,0,0))
CurrentDown=20
for (I=1, Laws:count()) {
    local Index=I*2+2
    CurrentDown+=30
    Scr:egpText(Index,I+". "+Laws[I,string],vec2(20,CurrentDown))
    Scr:egpSize(Index,24)
    Scr:egpColor(Index,vec(128,128,128))
    if (Descs[I,string]) {
        Scr:egpText(Index+1,Descs[I,string],vec2(30,CurrentDown+22))
        Scr:egpColor(Index+1,vec(128,128,128))
        CurrentDown+=15
    }
}
local WriterNames=array()
for (I=1, Auth:count()) {
    WriterNames[WriterNames:count()+1,string]=Auth[I,entity]:name()
}
Scr:egpText(egpMaxObjects(),"Writers: "+(WriterNames:concat(", ")),vec2(10,500))
Scr:egpAlign(egpMaxObjects(),0,2)
Scr:egpSize(egpMaxObjects(),27)
