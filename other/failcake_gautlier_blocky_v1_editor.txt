@name Failcake Gautlier Blocky V1 EDITOR
@persist [E O Main MAPPROP]:entity [PROP]:string
@persist MOD_NONE MOD_ELEVATOR MOD_FINISH MOD_NORMAL UP I MOD_SPIN Chat:array ALLOWLOAD Kd
@persist GTABLE:gtable [DATA FINALDATA]:table HoloIndex MaxIndex SELECTED TEM_PROP:entity Bam Inx PROPS:array Saving
@trigger all

function void saveMap(Map:string,Dtb:table){   
fileWrite(">e2shared/"+Map+".txt",vonEncode(Dtb))

for(I = 0,PROPS:count()){
PROPS[I,entity]:propDelete()    
}

Dtb:clear()
Saving = 0
hint("* Map " + Map + " Saved! *",7)    
}


if(first() || duped()){
runOnTick(1)
runOnChat(1)
runOnFile(1)
runOnLast(1)

E = entity()
O = owner()

propSpawnEffect(0)
GTABLE = gTable("GAUNTStream_"+O:id(),1)

PROP = GTABLE["MAIN_PROP",string]

Main = GTABLE["MAIN_E2",entity]

MOD_NORMAL = 0
MOD_NONE = 1
MOD_ELEVATOR = 2
MOD_SPIN = 3
MOD_FINISH = 4

holoCreate(1)
holoModel(1,PROP)
holoAng(1,Main:toWorld(ang(0,0,0)))
holoAlpha(1,0)


HoloIndex = 2
MaxIndex = holoRemainingSpawns()

SELECTED = MOD_NORMAL

E:setColor(255,0,0)

}


if(changed(GTABLE["EDITOR_ENABLED",number]) && GTABLE["EDITOR_ENABLED",number]){
SELECTED = 0 
holoAlpha(2,150)
E:setColor(0,255,0)
holoAlpha(1,150) 

MAPPROP = propSpawn(PROP,Main:toWorld(vec(-48,0,-4)),TEM_PROP:angles(),1)
MAPPROP:setAlpha(150)
PROPS:pushEntity(MAPPROP)

DATA:pushTable(table(SELECTED,MAPPROP:pos(),MAPPROP:angles(),Main:pos()))

}

if(chatClk(O)){

Chat = O:lastSaid():lower():explode(" ")

if(Chat[1,string] == "-save"){
Saving = 1    
saveMap(Chat[2,string],DATA) 
  
}
   
}





if(GTABLE["EDITOR_ENABLED",number] && Saving == 0){
   
for(I = 1,PROPS:count()){
    
if(PROPS[I,entity] == noentity()){
print("* Prop Undoed *")
MAPPROP = PROPS[I-1,entity]
PROPS:remove(I)     
}
    
}  


if(changed(O:keyAttack1()) && O:keyAttack1()){
UP+= 5   
}

if(changed(O:keyAttack2()) && O:keyAttack2()){
UP-= 5     
}

if(O:keyDuck()){
Ang = (holoEntity(1):pos() - O:pos()):toAngle() 
holoAng(1,Ang)    
}

if(changed(O:keyReload()) && O:keyReload()){

if(SELECTED < 4){
SELECTED++
}else{
SELECTED = 0
}
print(SELECTED)
   
}



if(changed(O:keyUse()) && O:keyUse()){

MAPPROP = propSpawn(PROP,holoEntity(1):pos(),holoEntity(1):angles(),1)
MAPPROP:setAlpha(150)
PROPS:pushEntity(MAPPROP)

if(SELECTED == 1){
MAPPROP:setColor(255,0,0)    
}

DATA:pushTable(table(SELECTED,MAPPROP:pos(),MAPPROP:angles(),Main:pos())) 
}


holoPos(1,MAPPROP:toWorld(vec(-MAPPROP:boxSize():x() + 10,0,10 + UP)))

}
 



