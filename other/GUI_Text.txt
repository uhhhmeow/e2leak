#IF YOU FUCKING LEAK MY MYSQL LINK I WILL FIND AND KILL YOU

@name GUI Text
@inputs EGP:wirelink
@outputs 
@persist Text
@persist VoxDecoding VoxOutput:table VoxURL:string VoxClk Char:table Output:table
@persist Decode1:array DecodeCurrent Icons:array
@trigger 
@model models/hunter/plates/plate.mdl

interval(400)

if(first())
{
    EGP:egpClear()
    timer("Text", 1000)
    Text = 1
}

if(clk("Text"))
{
    if(Text == 1)
    {
        EGP:egpClear()
        EGP:egpRoundedBox(1, vec2(575,200),vec2(120,80))
        
        EGP:egpMaterial(1, "weapons/weapon_mad_ak47")
        EGP:egpAlign(1,1,1)
        
        EGP:egpRoundedBox(2, vec2(1,200),vec2(120,80))
        
        EGP:egpMaterial(2, "weapons/weapon_mad_galil")
        EGP:egpAlign(2,1,1)
        
        EGP:egpRoundedBox(4, vec2(285,200), vec2(356, 40))
        EGP:egpColor(4, vec4(255,255,255,100))
        EGP:egpText(3, "WooHoo's Gun Shop!", vec2(285,200))
        EGP:egpAlign(3,1,1)
        EGP:egpSize(3, 40)
        EGP:egpColor(3, vec4(1,1,1,255))
        
        Text = 2
        timer("Text", 10000)
    }
    
    elseif(Text == 2)
    {
        EGP:egpClear()
        EGP:egpRoundedBox(1, vec2(600,200),vec2(120,80))
        
        EGP:egpMaterial(1, "weapons/weapon_mad_glock")
        EGP:egpAlign(1,1,1)
        
        EGP:egpRoundedBox(2, vec2(-40,200),vec2(120,80))
        
        EGP:egpMaterial(2, "weapons/weapon_mad_usp")
        EGP:egpAlign(2,1,1)
        
        EGP:egpRoundedBox(4, vec2(285,200), vec2(560, 40))
        EGP:egpAlign(4,1,1)
        EGP:egpColor(4, vec4(255,255,255,100))
        EGP:egpText(3, "Come buy from WooHoo's Shop!", vec2(280,200))
        EGP:egpAlign(3,1,1)
        EGP:egpSize(3, 40)
        EGP:egpColor(3, vec4(1,1,1,255))
        
        Text = 3
        timer("Text", 10000)
    }
    
    elseif(Text == 3)
    {
        EGP:egpClear()
        EGP:egpRoundedBox(1, vec2(680,200),vec2(120,80))
        
        EGP:egpMaterial(1, "weapons/weapon_mad_p90")
        EGP:egpAlign(1,1,1)
        
        EGP:egpRoundedBox(2, vec2(-100,200),vec2(120,80))
        
        EGP:egpMaterial(2, "weapons/weapon_mad_spas")
        EGP:egpAlign(2,1,1)
        
        EGP:egpRoundedBox(4, vec2(285,200), vec2(650, 40))
        EGP:egpAlign(4,1,1)
        EGP:egpColor(4, vec4(255,255,255,100))
        EGP:egpText(3, "WooHoo's Automatic Shop Over Here!", vec2(285,200))
        EGP:egpAlign(3,1,1)
        EGP:egpSize(3, 40)
        EGP:egpColor(3, vec4(1,1,1,255))
        
        Text = 4 
        timer("Text", 10000)
    }
    
    elseif(Text == 4)
    {
        EGP:egpClear()
        EGP:egpRoundedBox(1, vec2(680,200),vec2(120,80))
        
        EGP:egpMaterial(1, "weapons/weapon_mad_m3")
        EGP:egpAlign(1,1,1)
        
        EGP:egpRoundedBox(2, vec2(-100,200),vec2(120,80))
        
        EGP:egpMaterial(2, "weapons/weapon_mad_ump")
        EGP:egpAlign(2,1,1)
        
        EGP:egpRoundedBox(4, vec2(285,200), vec2(650, 40))
        EGP:egpAlign(4,1,1)
        EGP:egpColor(4, vec4(255,255,255,100))
        EGP:egpText(3, "GUNS GUNS GUNS AND MORE GUNS!!", vec2(285,200))
        EGP:egpAlign(3,1,1)
        EGP:egpSize(3, 40)
        EGP:egpColor(3, vec4(1,1,1,255))
        
        Text = 5 
        timer("Text", 10000)
    }
    elseif(Text == 5)
    {
        EGP:egpClear()
        EGP:egpRoundedBox(1, vec2(680,200),vec2(120,80))
        
        EGP:egpMaterial(1, "weapons/weapon_bd_mp7")
        EGP:egpAlign(1,1,1)
        
        EGP:egpRoundedBox(2, vec2(-100,200),vec2(120,80))
        
        EGP:egpMaterial(2, "weapons/weapon_mad_aug")
        EGP:egpAlign(2,1,1)
        
        EGP:egpRoundedBox(4, vec2(285,200), vec2(650, 40))
        EGP:egpAlign(4,1,1)
        EGP:egpColor(4, vec4(255,255,255,100))
        EGP:egpText(3, "Guns! Come Now! Its easy and secure!", vec2(285,200))
        EGP:egpAlign(3,1,1)
        EGP:egpSize(3, 40)
        EGP:egpColor(3, vec4(1,1,1,255))
        
        Text = 6 
        timer("Text", 10000)
    }
    elseif(Text == 6)
    {
        EGP:egpClear()
        EGP:egpRoundedBox(1, vec2(680,200),vec2(120,80))
        
        EGP:egpMaterial(1, "weapons/weapon_mad_awp")
        EGP:egpAlign(1,1,1)
        
        EGP:egpRoundedBox(2, vec2(-100,200),vec2(120,80))
        
        EGP:egpMaterial(2, "weapons/weapon_mad_scout")
        EGP:egpAlign(2,1,1)
        
        EGP:egpRoundedBox(4, vec2(285,200), vec2(650, 40))
        EGP:egpAlign(4,1,1)
        EGP:egpColor(4, vec4(255,255,255,100))
        EGP:egpText(3, "Best Auto Gun Store In the WORLD!", vec2(285,200))
        EGP:egpAlign(3,1,1)
        EGP:egpSize(3, 40)
        EGP:egpColor(3, vec4(1,1,1,255))
        
        Text = 1
        timer("Text", 10000)
    }
    
}


#VoxCore by Scriptis
#PHP receiver code programmed by Vox the Voice
#[
    VoxClk - Changes to 1 upon Vox's load finishing.
    VoxOutput - Upon VoxClk, filled with the database.
    vox(Index,Value) - Saves a value to the database.
    voxDecode() - Loads the database.
]#
if (first()) {        
    #print(httpUrlEncode("Harry potter is a wizard!!1:"))
        
    function voxDecode() {
        if (VoxDecoding | !httpCanRequest()) { #debounce
            return
        }
        Output:clear()
        Decode1:clear()
        VoxDecoding=1
        httpRequest("http://voxthevoice.com/sshop/setting.php")
        runOnHTTP(1)
    }
    voxDecode()
}

if (httpClk()) {#httpClk
    runOnHTTP(0)
    local Data=httpData():replace("<br>","
")
    Decode1=Data:explode("
")
    #print("begin decode")
    DecodeCurrent=0
    timer("voxdecode",100)
}
if (clk("voxdecode")) {
    local Current=0
    for (I=1, Decode1:count()) {
        Current++
        DecodeCurrent++
        I=DecodeCurrent
        #find the delimiter
        local Decode=Decode1[I,string]:explode("=")
        Output[Decode[1,string],string]=Decode[2,string]:sub(2,Decode[2,string]:length()-1)
        if (Current>15 | DecodeCurrent>=Decode1:count()) {
            timer("voxdecode",1000)
            setName("VOX DECODE STATUS: "+round(DecodeCurrent/Decode1:count()*100)+"%")
            break
        }
    }
    if (Decode1:count()<=DecodeCurrent) {
        stoptimer("voxdecode")
        VoxOutput=Output
        VoxDecoding=0
        VoxClk=1
        setName("VOX DECODE STATUS: COMPLETE")
        timer("novoxclk",10)
    }
}
if (clk("novoxclk")) {
    VoxClk=0
    setName("GUI Text")
    foreach (I,V:string=VoxOutput) {
        if (I:sub(1,6)=="weapon") {
            Icons:pushString(V:explode(";;;")[4,string])
    }
}
    print("Got "+Icons:count()+" Icons!")
    timer("cycle",1000)
    
}
if (clk("cycle")) {
    timer("cycle",1000)
    EGP:egpMaterial(1,Icons[randint(1,Icons:count()),string])
    EGP:egpMaterial(2,Icons[randint(1,Icons:count()),string])
}
