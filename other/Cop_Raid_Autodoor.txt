@name Scriptis' Cop Raid Entrance Autdoor
@persist Timer
@outputs Fade
if (first()) {
    timer("PrimaryLoop",1000)
    function number allowed(E:entity) {
        return E:isAuthorisedPersonnel() & E:team()!=17
    }
    entity():isWeldedTo():createWire(entity(),"Fade","Fade")
}

interval(1000)
findByClass("player")
Ply=findClosest(entity():pos())
#print(_HUD_PRINTCENTER,Ply:name()+"\n"+allowed(Ply))
if (allowed(Ply) & Ply:pos():distance(entity():pos())<100) {
    Timer=8
}

if (clk("PrimaryLoop")) {
    Timer=clamp(Timer-1,0,8)
    if (Timer) {
        soundPlay(Timer,0,"ui/buttonclick.wav")
        soundPitch(Timer,10*Timer+20)
    }
    Fade=Timer>0
    timer("PrimaryLoop",1000)
}
