#[
    Name: Scriptis' HoloNPC Base
    Description: Module for the emulation of NPCs with holograms.
    
    #includes:
        # functions
        bool Success function npc_create(number ID, string Model, table Meta)
            Creates a holoNPC.
            Metadata must include table Anim, vector Pos, angle Ang
            
        bool Success function npc_addState(number ID, string State, bool Value)
            Adds a custom state to an NPC.
            
        bool Success function npc_getState(number ID, string State)
            Returns if an NPC has a custom state.
            
        bool Success function npc_moveTo(number ID, vector Pos)
            Adds state MOVING
            while MOVING, the NPC will follow a plotted path to Pos.
        
        void function npc_setGraph(table Graph)
            Sets the current nodegraph. Format:
            string Key : table NodeGraph
                string Name : table Node*
                    "N" : array Neighbors
                    "Pos" : vector Position
            * Can contain multiple entries; object class.
        
        # internal
        table SNPC_DAT
            Internal table for the module.
]#

@name Scriptis' HoloNPC Base
@persist SNPC_DAT:table

if (first()) {
    SNPC_DAT["Nodegraph",table]=table()
    
    # npc_create(Model,Metadata)
    function number npc_create(ID,Model:string,Metadata:table) {
        SNPC_DAT["Current",number]=SNPC_DAT["Current",number]+1
        SNPC_DAT[ID,table]=table(
            "Model"=Model,
            "Holo"=ID,
            "Status"=table(
                "Walk"=0
            ),
            "Anim"=Metadata["Anim",table],
            "Pos"=Metadata["Pos",vector],
            "Ang"=Metadata["Ang",angle],
            "AnimState"=""
        )
        holoCreate(ID)
        holoModel(ID,Model)
        holoPos(ID,Metadata["Pos",vector])
        holoAng(ID,Metadata["Ang",angle])
    }
    
    function number npc_addState(ID,Metadata:table) {
        
    }
    
    function number npc_moveTo(ID,Pos:vector) {
        
    }
    
    timer("SNPC_LOOP",1)
}

if (clk("SNPC_LOOP")) {
    
    timer("SNPC_LOOP",100)
}

if (first()) {
    npc_create(
        1,
        "models/humans/Group03/male_07.mdl",
        table(
            "Anim"=table(
                "Idle"=0,
                "Walk"=10
            ),
            "Pos"=entity():pos(),
            "Ang"=entity():angles()
        )
    )
}
