@name Vault SFX
@persist Direction

if(first()) {
    soundPlay("Hiss",0,"doors/doormove2.wav")
    soundPitch("Hiss",50)
    Direction=0
    timer("snd_Opening",2000)
}


if(clk("snd_Opening")) {
    soundPlay("Open",0,"doors/door_metal_gate_move1.wav")
    soundPitch("Open",74)
    timer(Direction ? "snd_Stop" : "snd_StopMove",2000)
}

if(clk("snd_Stop")) {
    soundPlay("Stop",0,"doors/door_metal_gate_close1.wav")
    soundPitch("Stop",65)
    timer(Direction ? "snd_MoveBack" : "snd_Opening",1100)
}

if(clk("snd_MoveBack")) {
    soundPlay("MoveBack",0,"doors/garage_move1.wav")
    soundPitch("MoveBack",185)
    timer(Direction ? "snd_StopMove" : "snd_Stop",2000)
}

if(clk("snd_StopMove")) {
    soundStop("MoveBack")
    soundPlay("StopMove",0,"doors/garage_stop1.wav")
    soundPitch("StopMove",80)
}
