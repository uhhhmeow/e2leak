@name FailCake HoloConverter
@persist [E O]:entity [Code E2_STATUS Name]:string Center:vector X Arr:array
@trigger all

if(first() || duped()){
 
    function void generateHeader(File:string){
        Code += "@name " + File + "_Generated\n"
        Code += "@persist E:entity HOLO_INDEX\n"
        Code += "@trigger all\n\n"
    }   
    
    function void onDebug(Deb:string){
        printColor(vec(255,155,0),"[DEBUG] ",vec(255,255,255),Deb)    
    }

    
    function void generateFunctions(){
        Code += "## FUNCTIONS ##\n"
        Code += "if(first() || duped()){\n\n"
        Code += "   function void createHolo(Model:string,Scl:vector,Pos:vector,Ang:angle,Color:vector,Mat:string){\n\n"
            
        Code += "         holoCreate(HOLO_INDEX)\n"
        Code += "         holoModel(HOLO_INDEX,Model)\n"
        Code += "         holoPos(HOLO_INDEX,Pos)\n"
        Code += "         holoAng(HOLO_INDEX,Ang)\n"
        Code += "         holoScale(HOLO_INDEX,Scl)\n"
        Code += "         holoParent(HOLO_INDEX,E)\n"
        Code += "         holoColor(HOLO_INDEX,Color)\n"
        Code += "         holoMaterial(HOLO_INDEX,Mat)\n\n"
        
        Code += "         if(holoEntity(HOLO_INDEX):model() != Model){\n"
        Code += "               holoAlpha(HOLO_INDEX,0)\n"
        Code += "         }\n"
            
        Code += "         HOLO_INDEX+=1\n"
        Code += "   }\n"
        Code += "}\n\n"
    }
    
    function void generateFirst(){
        Code += "if(first() || duped()){\n"
        Code += "   E = entity()\n\n"
        Code += "   ### GENERATED CODE ###\n"
    }
    
}


if(first() || duped()){
    
    runOnChat(1)
    runOnTick(1)
    runOnFile(1)
    
    E = entity()
    O = owner()
    E2_STATUS = "IDLE"

}

if(chatClk(O)){
    local Chat = O:lastSaid():explode(" ")
    
    if(Chat[1,string] == "-save" && Chat[2,string] != ""){
        Code = ""
        Name = Chat[2,string]
        
        onDebug("Generating E2 Headers..")
        generateHeader(Chat[2,string])
        onDebug("Generating E2 Custom Functions..")
        generateFunctions()
            
        E2_STATUS = "GENERATE"
    }
}

### THINK ###
if(changed(E2_STATUS) && E2_STATUS == "GENERATE"){
    
    onDebug("Preparing to gather Props by weld...")
    
    Arr = E:isWeldedTo():getConstraints()
    Arr:unshiftEntity(entity():isWeldedTo())
    
    local I = 0
    
    for(I = 1, Arr:count()) {
        if(Arr[I, entity] == entity()) {
            Arr:removeEntity(I)
            break
        }
    }
    
    # Get Center   
    for(I = 1, Arr:count()) {
        Center += Arr[I, entity]:massCenter()
    }
        
    Center /= Arr:count()
    print(Center)
    generateFirst()
}

if(E2_STATUS == "GENERATE"){
    
    while(perf() && X <= Arr:count()) {
        
        local Pos = "E:toWorld(vec("+round((Arr[X, entity]:pos() - Center):x(), 2) + "," + round((Arr[X, entity]:pos() - Center):y(), 2) + "," + round((Arr[X, entity]:pos() - Center):z(), 2)+"))"
        local Ang = "E:toWorld(ang(" + round((Arr[X, entity]:angles():pitch()), 2) + "," + round((Arr[X, entity]:angles():yaw()), 2) + "," + round((Arr[X, entity]:angles():roll()), 2) + "))"
        local Color = "vec(" + Arr[X, entity]:getColor():x() + "," + Arr[X, entity]:getColor():y() + "," + Arr[X, entity]:getColor():z() + ")"
        Code += "   createHolo(\"" +Arr[X, entity]:model()+ "\",vec(1),"+Pos+","+Ang+","+Color+",\"" +Arr[X, entity]:getMaterial()+ "\")\n"
        
        X++
    }
    
    if(X > Arr:count() && changed(X > Arr:count())) {
        
        Code += "}"
        
        if(fileCanWrite()) {
            
            onDebug("File Saved!")
            fileWrite(">e2shared/Generated/"+Name+".txt",Code)
            selfDestruct()
            
        }
    }

    
}
