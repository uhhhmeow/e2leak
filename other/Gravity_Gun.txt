@name Gravity Gun
@persist Entity:entity
@persist Count Tick
@model models/weapons/w_physics.mdl
interval(1)
if (first()) {
    Entity=entity()
    timer("Gravy",0)
    Table=gTable("Gravity")
    Table["Ent",entity]=Entity
}

Tick+=5

if (clk("Gravy")) {
    Count++
    switch(Count) {
        case 1,
            Entity:soundPlay(1,0,"vo/citadel/al_thegravgun01.wav")
        break
        
        case 2,
            Entity:soundPlay(1,0,"vo/citadel/al_thegravgun03.wav")
        break
        
        case 3,
            Entity:soundPlay(1,0,"vo/citadel/al_thegravgun04.wav")
            Count=0
        break
    }
    timer("Gravy",2000)
}
Pitch=65 #vec(Entity:angVel()):length()

soundPitch(1,Pitch)
soundVolume(1,1)
setName("duh gravety gun")


