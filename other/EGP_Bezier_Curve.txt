@name EGP Bezier Curve
@inputs Scr:wirelink

if(first()) {
    #include "mantle/mantle_core"
    local Bezier=bezier(vec2(0),vec2(512,0),vec2(512),20)
    foreach(I,V:vector2=Bezier) {
        if(I!=Bezier:count()) {
            Scr:egpLine(I,Bezier[I,vector2],Bezier[I+1,vector2])
            Scr:egpColor(I,hsv2rgb((360/Bezier:count())*I,1,1))
            Scr:egpSize(I,5)
        }
    }
}
