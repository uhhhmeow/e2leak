@name E2 Safety
@inputs 
@outputs Entity:entity RandLoc:array P:array
@persist Spawn:vector X Size Radius Player:entity Target:entity
@trigger 
if(first()){
    timer("start",100)
}
interval(50)
runOnChat(1)

S = owner():lastSaid():explode(" ")

if(S[1,string]=="!add" & chatClk(owner()))
{
    hideChat(1)
    TP = findPlayerByName(S[2,string])
    if(TP:isPlayer())
    {
        Target = TP
        P:pushEntity(TP)
    }
}

if(S[1,string]=="!remove"& chatClk(owner()))
{
    hideChat(1)
    TP = findPlayerByName(S[2,string])
    if(TP:isPlayer())
    {
        for(I = 1, P:count())
        {
            if(TP == P[I,entity])
            {
                  P:remove(I)
                  findClearBlackEntityList()
                  holoEntity(1):soundPlay(3,1,"buttons/button10.wav")
            }
        }
    }
}

if(findCanQuery())
{
    findIncludeClass("player")
    findIncludeClass("prop_physics")
    findIncludeEntity(owner())
    findExcludePlayerProps(owner())
    findExcludeEntities(P)
    findInSphere(entity():pos(),Radius)
    Entity = find()
}


if(first())
{
    entity():setAlpha(0)
    Radius = 300
    Spawn = vec(195.397049,-46.316170,-11.980263)      
    Size = round(Radius / 5)
    
    RandLoc = array(vec(3806.301514, -5140.084961, -134.968750))
    
    function makeBubble(Index, Pos:vector, Scale:vector)
    {
        holoCreate(Index,Pos,vec(12.7,12.7,12.7)*Scale,ang(0,0,0),vec(255,220,150),"hqsphere"), holoMaterial(1, "models/props/de_tides/clouds")
        holoCreate(Index+1,Pos,vec(-13.3,-13.3,-13.3)*Scale,ang(0,0,0),vec(255,255,255),"hqsphere"), holoMaterial(2, "sprites/heatwave")
        holoCreate(Index+2,Pos,vec(13.3,13.3,13.3)*Scale,ang(0,0,0),vec(255,255,255),"hqsphere"), holoMaterial(3, "sprites/heatwave")
        
        for(I=Index+3,Index+13)
        {
            holoCreate(I,Pos,(vec(-13.2,-13.2,-13.2)*Scale)+I/5,ang(0,0,0),vec(255,190,0),"hqsphere"), holoAlpha(I,3), holoDisableShading(I,1)
        }
    }
    
    function renderVolume(Index, Pos:vector, Scale:vector)
    {
        holoCreate(Index,Pos,vec(12.7,12.7,12.7)*Scale,ang(0,0,0),vec(255,220,150),"hqsphere"), holoMaterial(1, "models/props/de_tides/clouds")
        holoCreate(Index+1,Pos,vec(-13.3,-13.3,-13.3)*Scale,ang(0,0,0),vec(255,255,255),"hqsphere"), holoMaterial(2, "sprites/heatwave")
        holoCreate(Index+2,Pos,vec(13.3,13.3,13.3)*Scale,ang(0,0,0),vec(255,255,255),"hqsphere"), holoMaterial(3, "sprites/heatwave")
    }
    
    function vector deploy(Ply:entity)
    {
        Ray = rangerOffset(1000,Ply:pos()+vec(0,0,10),vec(0,0,-1))
        if(Ray:hit())
        {
            local G = Ray:position()+vec(0,0,10)
            local N = Ray:hitNormal():toAngle()+ang(90,0,0)
            holoCreate(20,G,vec(1,1,1),N,vec(255,255,255),"models/Items/combine_rifle_ammo01.mdl")
            for(I=21,25)
            {
                holoCreate(I,G-vec(0,0,4),vec(0.8,0.8,1.1),N+ang(180,90*I,0),vec(255,255,255),"models/Items/combine_rifle_cartridge01.mdl")
            }
        }
    }
    timer("init",1000)
}
else 
{
    if(X<Size)
    {
        interval(50)
        X++
        renderVolume(1,entity():pos()+vec(0,0,30),vec(0.1,0.1,0.1)*X)
    }
    else
    {
        makeBubble(1,entity():pos()+vec(0,0,30),vec(0.1,0.1,0.1)*X)
    }
}

if(changed(Entity) & Entity)
{
    Entity:setEntPos(RandLoc[randint(RandLoc:count()),vector])
    holoEntity(1):soundPlay(1,1,"AlyxEMP.Charge")
}
