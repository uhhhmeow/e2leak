@name Generic GunShop V1.1
@inputs [Df,Db,R1,R2,R3]:entity EGP:wirelink
@outputs U1 U2 U3 User:entity Aim:vector2
@persist [Dfh,Dbh]:vector D A D2 A2 Screen  Refresh Hack
interval(100)
Aim = round(EGP:egpCursor(User))
if(first() | dupefinished()){

  Dfh=entity():pos()+vec(0,-2,-15)
  Dbh=entity():pos()+vec(0,-141,0)
  runOnChat(1)
  D=0
  D2=0 
  Screen = 1
  Refresh = 0
  Pause = 0

}
#Start: finding user close to screen
if(findCanQuery()){
   findClearBlackList()
   findByClass("player")
   User = findClosest(entity():pos()) 
}
if(changed(User)&Hack!=1 | !User:isAlive()){ 
  EGP:egpClear()
  Screen = 1
  Refresh = 0
  Pause = 0
  Hack = 0
  soundStop("alarm")
  holoDelete(1) 
}
if(User:pos():distance(entity():pos())>200){
  User = noentity() 
  EGP:egpClear()
  Screen = 1
  Refresh = 0
  Pause = 0
  Hack = 0
  soundStop("alarm")
  holoDelete(1)
  
}

#end: finding user close to screen

#Start: Back door chat control
if(chatClk(owner())&owner():lastSaid()=="!open"){
  Db:soundPlay("opensound",0,"buttons/button5.wav")
  D=1   
}
if(chatClk(owner())&owner():lastSaid()=="!close"){
  Db:soundPlay("opensound",0,"buttons/button6.wav")
  D=0   
}
if(D==1){
  A+=3  
} 
else{
  A-=3 
}
A = clamp(A,0,35)
Db:setPos(Dbh+vec(0,0,A))
#end: Back door chat control


#start: Main menu screen drawing
if(Screen == 1){
  if(Refresh == 0 | first()){
  EGP:egpClear()
  EGP:egpBox(1,vec2(256),vec2(512)) EGP:egpColor(1,vec(0,50,75))
  EGP:egpText(2,"Welcome to Gold's Guns.",vec2(256,256-25)) EGP:egpAlign(2,1,1) EGP:egpSize(2,35)
  EGP:egpText(3,"Press E anywere to begin.",vec2(256,256+25)) EGP:egpAlign(3,1,1) EGP:egpSize(3,30)
  Refresh = 1
  for(I=4,5){
  EGP:egpBox(I,vec2(256,((I-4)*512)),vec2(512,200)) EGP:egpMaterial(I,"gui/gradient_down") EGP:egpAngle(I,(I-4)*-180) EGP:egpColor(I,vec4(0,255,255,100))
}}
if(User:keyUse() && changed(User:keyUse()) && inrange(Aim,vec2(0),vec2(512))){
  EGP:egpClear()
  timer("change",250)
  entity():soundPlay("beeep",0,"buttons/button1.wav")
}}
#end: Main menu screen drawing
if(clk("change")){
  Screen = 2 
  Refresh = 0    
}

#start: Buy menu screen drawing
if(Screen == 2){
  if(Refresh == 0 | first()){
  EGP:egpClear()
  EGP:egpBox(1,vec2(256),vec2(512)) EGP:egpColor(1,vec(0,50,75))
  Refresh = 1
  for(I=2,3){
  EGP:egpBox(I,vec2(256,((I-2)*512)),vec2(512,200)) EGP:egpMaterial(I,"gui/gradient_down") EGP:egpAngle(I,(I-2)*-180) EGP:egpColor(I,vec4(0,255,255,100))
}
  EGP:egpRoundedBox(4,vec2(256,0),vec2(400,100)) EGP:egpColor(4,vec4(0,0,0,200))
  EGP:egpRoundedBox(5,vec2(256),vec2(512,400)) EGP:egpColor(5,vec4(0,0,0,200))
  EGP:egpText(6,"Gold's Gunshop V1",vec2(256,25)) EGP:egpAlign(6,1,1) EGP:egpSize(6,30)
  EGP:egpRoundedBox(7,vec2(125,256-100),vec2(400,75)) EGP:egpColor(7,vec4(100,100,100,50))
  EGP:egpRoundedBox(8,vec2(125,256),vec2(400,75)) EGP:egpColor(8,vec4(100,100,100,50))
  EGP:egpRoundedBox(9,vec2(125,256+100),vec2(400,75)) EGP:egpColor(9,vec4(100,100,100,50))

  EGP:egpText(10,R1:shipmentName(),vec2(150,256-110)) EGP:egpAlign(10,1,1)
  EGP:egpText(11,R2:shipmentName(),vec2(150,256-10)) EGP:egpAlign(11,1,1)
  EGP:egpText(12,R3:shipmentName(),vec2(150,256+90)) EGP:egpAlign(12,1,1)
  

  
  EGP:egpRoundedBox(16,vec2(290,256-100),vec2(50))
  EGP:egpRoundedBox(17,vec2(290,256),vec2(50))
  EGP:egpRoundedBox(18,vec2(290,256+100),vec2(50))
  if(R1){
  EGP:egpColor(16,vec(0,100,0))
  EGP:egpText(13,toString((R1:shipmentPrice()/5)+50),vec2(150,256-90)) EGP:egpAlign(13,1,1)  
  }
  else{EGP:egpColor(16,vec(100,0,0))
  EGP:egpText(13,"Out of Stock",vec2(150,256-90)) EGP:egpAlign(13,1,1) }

  if(R2){
  EGP:egpColor(17,vec(0,100,0)) 
  EGP:egpText(14,toString((R2:shipmentPrice()/5)+50),vec2(150,256+10)) EGP:egpAlign(14,1,1) 
  }
  else{EGP:egpColor(17,vec(100,0,0))
  EGP:egpText(14,"Out of Stock",vec2(150,256+10)) EGP:egpAlign(14,1,1) 
    }

  if(R3){
  EGP:egpColor(18,vec(0,100,0))
  EGP:egpText(15,toString((R3:shipmentPrice()/5)+50),vec2(150,256+110)) EGP:egpAlign(15,1,1)  
  }
  else{EGP:egpColor(18,vec(100,0,0))
  EGP:egpText(15,"Out of Stock",vec2(150,256+110)) EGP:egpAlign(15,1,1)  
    }
  EGP:egpText(19,toString(clamp(R1:shipmentAmount(),0,10)),vec2(290,256-100)) EGP:egpAlign(19,1,1)
  EGP:egpText(20,toString(clamp(R2:shipmentAmount(),0,10)),vec2(290,256)) EGP:egpAlign(20,1,1)
  EGP:egpText(21,toString(clamp(R3:shipmentAmount(),0,10)),vec2(290,256+100)) EGP:egpAlign(21,1,1)
 #hacking system 

}

  if(User:team()==12){EGP:egpRoundedBox(22,vec2(256,512),vec2(200,100)) EGP:egpColor(22,vec4(100,0,0,200))
  EGP:egpText(23,"Inject Lock_hacker.exe",vec2(256,512-25)) EGP:egpAlign(23,1,1)
  if(inrange(Aim,vec2(160,450),vec2(350,512)) & User:keyUse() && changed(User:keyUse())){
  Screen = 4
  Refresh = 0 
  entity():soundPlay("alarm",0,"ambient/alarms/alarm_citizen_loop1.wav")  
}  
}
if(R1 & inrange(Aim,vec2(0,120),vec2(325,200)) & User:keyUse() && changed(User:keyUse())) {moneyRequest(User,(R1:shipmentPrice()/5)+50,15,"G1")}
if(R2 & inrange(Aim,vec2(0,220),vec2(325,300)) & User:keyUse() && changed(User:keyUse())) {moneyRequest(User,(R2:shipmentPrice()/5)+50,15,"G2")}
if(R3 & inrange(Aim,vec2(0,320),vec2(325,400)) & User:keyUse() && changed(User:keyUse())) {moneyRequest(User,(R3:shipmentPrice()/5)+50,15,"G3")}

if(moneyClk("G1")){
  U1 = 1
  Screen = 3
  Refresh = 0 
  timer("end1",4000)
}
if(moneyClk("G2")){
  Screen = 3
  Refresh = 0 
  U2 = 1
  timer("end1",4000) 
}
if(moneyClk("G3")){
  Screen = 3
  Refresh = 0 
  U3 = 1
  timer("end1",4000)  
}
}
#end: Buy menu screen drawing
if(clk("end1")){
 D2=1
 timer("end2",5000)  
}
if(D2==1){
  A2+=3  
} 
else{
  A2-=3 
}
A2 = clamp(A2,0,30)
Df:setPos(Dfh-vec(0,0,A2))

if(clk("end2")){
 D2=0
 U1 = 0  
 U2 = 0  
 U3 = 0  
 Screen = 1
 Refresh = 0 
}

if(Screen == 3){
  if(Refresh == 0 | first()){
  EGP:egpClear()
  EGP:egpBox(1,vec2(256),vec2(512)) EGP:egpColor(1,vec(0,50,75))
  EGP:egpText(2,"Thank you for buying.",vec2(256,256-25)) EGP:egpAlign(2,1,1) EGP:egpSize(2,35)
  EGP:egpText(3,"Please wait for your gun below",vec2(256,256+25)) EGP:egpAlign(3,1,1) EGP:egpSize(3,30)
  Refresh = 1
  for(I=4,5){
  EGP:egpBox(I,vec2(256,((I-4)*512)),vec2(512,200)) EGP:egpMaterial(I,"gui/gradient_down") EGP:egpAngle(I,(I-4)*-180) EGP:egpColor(I,vec4(0,255,255,100))
}}
}

if(Screen == 4){
  if(Refresh == 0 | first()){
  concmd("say /advert [WARNING] Gunshop near spawn is currently being hacked!")
  EGP:egpClear()
  holoCreate(1,entity():pos())
  holoModel(1,"hq_sphere")
  holoScale(1,vec(-30))
  holoColor(1,vec4(255,0,0,100))
  EGP:egpText(1,"Hacking Progress",vec2(256,256-50)) EGP:egpAlign(1,1,1) EGP:egpSize(1,35)
  Refresh = 1}
Hack+=1
EGP:egpBox(2,vec2(57+(Hack/2),256),vec2(Hack,50)) EGP:egpColor(2,vec(255,0,0))
EGP:egpBoxOutline(3,vec2(256),vec2(400,50))
if(Hack>=400){
  Db:soundPlay("opensound",0,"buttons/button5.wav")
  D=1
  Screen = 1
  Refresh = 0 
  holoDeleteAll()
  soundStop("alarm")
  soundPlay("derp",0,"ambient/machines/thumper_shutdown1.wav")
}

}

