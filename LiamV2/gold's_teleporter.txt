@name 
@inputs 
@outputs D T
@persist  Goto:vector R X Y P A:array
@trigger 

R+=2
interval(25)
runOnChat(1)
if(first()) {
 D = 100
P=25   
}

if(T == 1) {
 D-=0.8
 P+=2
soundPitch(2,P)
soundPitch(3,P)
for(I=1,8){
holoCreate(I,Goto+vec(sin(R+(I*45))*D,cos(R+(I*45))*D,0))
holoModel(I,"hq_cylinder")
holoScale(I,vec(0.5,0.5,500))
holoMaterial(I,"Models/effects/comball_sphere")
}

for(A=9,17){
holoCreate(A,owner():pos()+vec(sin(R+(A*45))*D,cos(R+(A*45))*D,0))
holoModel(A,"hq_cylinder")
holoScale(A,vec(0.5,0.5,500))
holoMaterial(A,"Models/effects/comball_sphere")
}
}
if(owner():lastSaid()=="!home" && changed(owner():lastSaid())){
soundStop(1)
Goto = entity():pos()
timer("other",50)
owner():soundPlay(2,0,"ambient/energy/force_field_loop1.wav")
holoEntity(1):soundPlay(3,0,"ambient/energy/force_field_loop1.wav")
T=1
D = 100 
P=25


}
A = owner():lastSaid():explode(" ")
if(A:string(1)==".goto" & chatClk(owner())){
soundStop(1)
Goto = findPlayerByName(A:string(2)):pos()+vec(0,30,0)
timer("other",50)
owner():soundPlay(2,0,"ambient/energy/force_field_loop1.wav")
holoEntity(1):soundPlay(3,0,"ambient/energy/force_field_loop1.wav")
T=1
D = 100 
P=25


}

A = owner():lastSaid():explode(" ")
if(A:string(1)==".on" & chatClk(owner())){
print(_HUD_PRINTCENTER,"-=E2 Protection - Protection is ON=-")
soundPlay(1,0,"NPC_CeilingTurret.Ping")
hideChat(1)
#entity():propNotSolid(1)
entity():setAlpha(0)
}

if(A:string(1)==".off" & chatClk(owner())){
print(_HUD_PRINTCENTER,"-=E2 Protection - Protection is OFF=-")
soundPlay(1,0,"NPC_CeilingTurret.Die")
hideChat(1)
#entity():propNotSolid(0)
entity():setAlpha(100)
}

if(owner():keyUse()&&changed(owner():keyUse())&owner():keySprint()){
timer("other",50)

soundStop(1)
Goto = owner():aimPos()
owner():soundPlay(2,0,"ambient/energy/force_field_loop1.wav")

T=1
D = 100 
P=25


}
if(clk("other")){
holoEntity(1):soundPlay(3,0,"ambient/energy/force_field_loop1.wav")    
}

if(D<0){
 T = 0
 D = 100
P = 25
 
 owner():setEntPos(Goto+vec(0,0,50))
 timer("sound",100)   
}
if(clk("sound")){
    owner():soundPlay(1,0,"ambient/explosions/explode_7.wav"), timer("wait for cooldown",2000) #[lightCreate(1,owner():pos(),vec(0,255,255))]# timer("light",100) soundStop(2) soundStop(3) holoDeleteAll()
    #lightBrightness(1,5000)
    }

if(clk("light")) {
 #lightRemoveAll()
owner():setMaterial("")   
}
if(D<10){
 #owner():applyPlayerForce(vec(0,0,20))
 owner():setMaterial("Models/effects/comball_sphere")   
}

if(clk("wait for cooldown")){
    owner():soundPlay(1,0,"NPC_CeilingTurret.Die")
}
