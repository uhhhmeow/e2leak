@name Guard
@inputs S:wirelink 
@persist Index SpawnPos:vector Move Prop1:entity GAIN Speed COpen Password:string Config:table Player:entity


runOnChat(1)
interval(100)

if(first()){
    
    if(findCanQuery()){
        findClearBlackList()
        findByClass("player")
        Player = findClosest(entity():pos())
    }
    
    #
    
    Config["LazyTimer",number]=15 #<-- In Seconds
    Config["MainText",string]="Type '!List' To Begin!" #<-- What main text will say
    
    timer("lazy",Config["LazyTimer",number]*1000)
    
    
    Password = "V-K"
    
    
    #
    #holoCreate(1)
    #holoPos(1,entity():pos()+vec(0,-60,-11.18396902))
    Speed = 8
    Index = 1
    Prop1=entity():isWeldedTo()
    SpawnPos=Prop1:pos()
    Prop1:setPos(SpawnPos)
    Prop1:propFreeze(1)
    GAIN = 0
    #holoModel(1,"models/Barney.mdl")
    #holoAnim(1,32) #31
    #holoAng(1,ang(0,0,0))
}

if(Index == 1){
    S:egpClear()
    S:egpRoundedBox(1,vec2(256,156),vec2(300,100)),S:egpColor(1,vec(100,100,255))
    S:egpText(2,Config["MainText",string],vec2(256,166)),S:egpAlign(2,1,2),S:egpSize(2,30),S:egpColor(2,vec(255))
    if(Index == 1 & lastSaid() == "!List" & lastSaidWhen() == curtime()) {
    Index = 2
}
}





if(Index == 2){
    S:egpSize(1,vec2(S:egpSize(1):x()+5,126))
    
    if(S:egpSize(1):x()>360){S:egpSize(1,vec2(360,126))
    
    S:egpSetText(2,"Please Enter 'V-K' To Enter ")
    
    if(Index == 2 & Player:lastSaid() == Password & lastSaidWhen() == curtime()) {
    timer("correct login",100)
    }elseif(Index == 2 & Player:lastSaid() != Password & lastSaidWhen() == curtime()) {
    S:egpRemove(2)
    S:egpText(4,"Access Denied! Resseting!",vec2(256,156)),S:egpSize(4,30),S:egpColor(4,vec(255,0,0)),S:egpAlign(4,1,4)
    timer("failed login",100)
    }
}
    
}




if(clk("correct login")){
    stoptimer("lazy")
    S:egpRemove(2)
    S:egpText(3,"Access Granted! Welcome!",vec2(256,156)),S:egpColor(3,vec(0,255,0)),S:egpAlign(3,1,3),S:egpSize(3,30)
    COpen = 0
    Move = 1
}

if(clk("failed login")){
    holoAnim(1,16) # 8
    stoptimer("lazy")
    timer("back to start",3000)
}

if(clk("back to start")){
    Index = 1
    timer("lazy",Config["LazyTimer",number]*1000)
}

if(Move==1){
    if(GAIN<100){
        GAIN+=Speed
        Prop1:setPos(SpawnPos+vec(0,0,GAIN))
    }else{
        Move = 1.5
        timer("Wait",3000)
        soundPlay(2,1.2,"doors/doorstop1.wav")
    }
}elseif(Move==2 & COpen == 0){
    if(GAIN>0){
        GAIN-=Speed
        Prop1:setPos(SpawnPos+vec(0,0,GAIN))
    }else{
        Move = 0
        soundPlay(1,1.2,"doors/doorstop1.wav")
    }
}
if(clk("Wait")){Move=2,soundPlay(1,1.5,"doors/doormove2.wav"),Index = 1}

if(clk("lazy")){
    holoAnim(1,31)
    timer("unlazy",5000)
}

if(clk("unlazy")){
    holoAnim(1,32)
    timer("lazy",Config["LazyTimer",number]*1000)
}
