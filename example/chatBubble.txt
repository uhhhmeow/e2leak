@name chatBubble
@persist Bubble:table

if( first( ) ) {
    local R = players( )
    for( I = 1, R:count( ) ) {
        Bubble[ R[ I, entity ]:steamID64( ), entity ] = R[ I, entity ]
        print( "Added: " + R[ I, entity ]:name( ) )
        local H = R[ I, entity ]:toString( ):toNumber( )
        holoCreate( H )
        holoModel( H, "models/extras/info_speech.mdl" )
        holoScale( H, vec( 1 ) )
        #holoColor( H, teamColor( R[ I, entity ]:team( ) ) )
        holoPos( H, R[ I, entity ]:toWorld( vec( 0, 0, 85 ) ) )
    }
}

if( playerConnectClk( ) ) {
    local R = players( )
    for( I = 1, R:count( ) ) {
        if( !Bubble:exists( R[ I, entity ]:steamID64( ) ) ) {
            Bubble[ R[ I, entity ]:steamID64( ), entity ] = R[ I, entity ]
            print( "Added: " + R[ I, entity ]:name( ) )
            local H = R[ I, entity ]:toString( ):toNumber( )
            holoCreate( H )
            holoModel( H, "models/extras/info_speech.mdl" )
            holoScale( H, vec( 0.5 ) )
            holoPos( H, R[ I, entity ]:toWorld( vec( 0, 0, 85 ) ) )
        }
    }
}

if( playerDisconnectClk( ) ) {
    local R = players( )
    for( I = 1, R:count( ) ) {
        if( !Bubble[ R[ I, entity ]:steamID64( ), entity ]:isValid( ) ) {
            holoDelete( R[ I, entity ]:toString( ):toNumber( ) )
            Bubble:remove( R[ I, entity ]:steamID64( ) )
        }
    }
}

if( clk( ) ) {
    local Z = 105 + ( sin( realtime( ) * 50 ) * 3 )
    foreach( K, E:entity = Bubble ) {
        local I = E:toString( ):toNumber( )
        holoPos( I, E:toWorld( vec( 0, 0, Z ) ) )
        holoAng( I, ( quat( holoEntity( I ):angles( ) ) * quat( ang( 0, 1, 0 ) ) ):toAngle( ) )
        if( E:isTyping( ) ) {
            holoAlpha( I, 255 )
        } else {
            holoAlpha( I, 0 )
        }
    }
}

interval( 60 )
