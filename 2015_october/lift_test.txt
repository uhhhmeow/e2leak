@name lift_test
@inputs 
@outputs 
@persist Entity:entity
@trigger none
@model models/sprops/rectangles/size_5/rect_48x144x3.mdl


if ( first() ) {
    
    print(  )
    
    Entity = entity()

    holoCreate(1, Entity:pos(), vec(11,11,200)/12), holoParent(1, Entity), holoColor(1, vec(255,0,0))
    holoModel(1, "cone"), holoMaterial(1, "models/debug/debugwhite"), holoDisableShading(1, 1)
    
    holoCreate(2, Entity:pos(), vec(11,11,200)/12), holoParent(2, Entity), holoColor(2, vec(0,0,255))
    holoModel(2, "cone"), holoMaterial(2, "models/debug/debugwhite"), holoDisableShading(2, 1)

    holoCreate(3, Entity:pos(), vec(11,11,200)/12), holoParent(3, Entity), holoColor(3, vec(0,255,0))
    holoModel(3, "cone"), holoMaterial(3, "models/debug/debugwhite"), holoDisableShading(3, 1)
    
    
    
    
    holoCreate(4, Entity:toWorld(vec(0,-50,0)), vec(60,60,5)/12), holoParent(4, Entity), holoColor(4, vec(255,255,0))
    holoModel(4, "cone"), holoMaterial(4, "models/debug/debugwhite"), holoDisableShading(4, 1)
    holoAng(4, Entity:toWorld(ang(90,90,0)))
}

#interval(60)
runOnTick(1)

Velocity = Entity:vel()
Direction = Velocity:normalized()
RelativeWind = Direction * -1
Perpendicular = RelativeWind:toAngle():up()
AngleOfAttack = Entity:up():dot(RelativeWind)


holoAng(1, Perpendicular:toAngle() + ang(90,0,0))
holoAng(2, RelativeWind:toAngle() + ang(90,0,0))
holoAng(3, Entity:toWorld( ang(-acos(AngleOfAttack), -90, 0) ) )

CL = 0.04*AngleOfAttack
Area = convertUnit( "in", "m", entity():boxSize():length() ) #convertUnit( "in", "m", 144 ) * convertUnit( "in", "m", 48 )
ASpd = Velocity:dot(Entity:right()) *0.0254 #Velocity:length() * 0.0254

Lift = CL * Area * ((1.225 * ASpd^2) / 2)

Entity:applyForce( Perpendicular * Lift * Entity:mass() ) #* Entity:mass() )


RPM = 1200
Pitch = 3

Thrust = 1.255 * ( (pi() * (0.0254 * 60)^2) / 4) * (RPM * 0.0254 * Pitch * (1/60))^2

Entity:applyOffsetForce( Entity:mass() * Entity:right() * Thrust, holoEntity(4):pos() )
#Entity:applyOffsetForce( Entity:mass() * -Entity:right():setZ(0)*Thrust, holoEntity(4):pos() )

#1.255 *  ( (pi * (0.0254 * d)^2) / 4) * (RPMprop * 0.0254 * pitch * 1/60)^2


#L = Cl * A * .5 * r * V^2 
