@name laser_wep
@inputs 
@outputs 
@persist 
@trigger 


if (first()) {
Mat1 = "sprops/trans/wheels/wheel_d_rim1"
Mat2 = "sprops/trans/wheels/wheel_d_rim2"
Mat3 = ""#models/alyx/emptool_glow"


holoCreate(1)
holoModel(1,"hq_dome")
holoMaterial(1,Mat1)
holoScaleUnits(1, vec(24,24,6))
holoParent(1, entity())

holoClipEnabled(1,1,1)
holoClip(1,1,vec(0,4,0),vec(0,-1,0),0)

holoClipEnabled(1,2,1)
holoClip(1,2,vec(0,-4,0),vec(0,1,0),0)




holoCreate(2)
holoModel(2,"hq_dome")
holoMaterial(2,Mat2)
holoColor(2,vec())
holoScaleUnits(2, vec(24,24,6))
holoParent(2, 1)

holoClipEnabled(2,1,1)
holoClip(2,1,vec(0,4,0),vec(0,1,0),0)

holoClipEnabled(2,2,1)
holoClip(2,2,vec(0,5,0),vec(0,-1,0),0)



holoCreate(3)
holoModel(3,"hq_dome")
holoMaterial(3,Mat2)
holoColor(3,vec())
holoScaleUnits(3, vec(24,24,6))
holoParent(3, 1)

holoClipEnabled(3,1,1)
holoClip(3,1,vec(0,-4,0),vec(0,-1,0),0)

holoClipEnabled(3,2,1)
holoClip(3,2,vec(0,-5,0),vec(0,1,0),0)




holoCreate(4)
holoModel(4,"hq_dome")
holoMaterial(4,Mat2)
holoScaleUnits(4, -vec(24,24,6))
holoParent(4, 1)
holoAng(4,holoEntity(1):toWorld(ang(180,0,0)))
holoColor(4,vec())

holoClipEnabled(4,1,1)
holoClip(4,1,vec(0,5,0),vec(0,-1,0),0)

holoClipEnabled(4,2,1)
holoClip(4,2,vec(0,-5,0),vec(0,1,0),0)






holoCreate(5)
holoModel(5,"hq_dome")
holoMaterial(5,Mat2)
holoColor(5,vec())
holoScaleUnits(5, vec(23,23,5))
holoParent(5, 1)
holoPos(5,holoEntity(1):toWorld(vec(0,0,0.125)))

holoClipEnabled(5,1,1)
holoClip(5,1,vec(0,0,1),vec(0,0,-1),0)



holoCreate(6)
holoModel(6,"hq_dome")
holoMaterial(6,Mat1)
holoScaleUnits(6, vec(23,23,5))
holoParent(6, 5)
holoPos(6, holoEntity(5):pos())

holoClipEnabled(6,1,1)
holoClip(6,1,vec(0,0,1),vec(0,0,1),0)




local N = 7
for (I = 1, 1) {
    N++
    holoCreate(N)
    holoModel(N,"hq_dome")
    holoMaterial(N,Mat3)
    holoScaleUnits(N, -vec(48,48,1))
    holoParent(N, 5)
    holoPos(N,holoEntity(5):toWorld(vec(0,0,0.5)))
    holoColor(N, vec4(0,0,125,125))
    holoAng(N,holoEntity(5):toWorld(ang(180,0,0)))
    
    holoClipEnabled(N,1,1)
    holoClip(N,1,vec(0,-5,0),vec(0,-1,0),0)

    N++
    holoCreate(N)
    holoModel(N,"hq_dome")
    holoMaterial(N,Mat3)
    holoScaleUnits(N, -vec(48,48,1))
    holoParent(N, 5)
    holoPos(N,holoEntity(5):toWorld(vec(0,0,0.5)))
    holoColor(N, vec4(0,0,125,125))
    holoAng(N,holoEntity(5):toWorld(ang(180,0,0)))
    
    holoClipEnabled(N,1,1)
    holoClip(N,1,vec(0,5,0),vec(0,1,0),0)
   
}






for (I = 1, 24) { holoDisableShading(I,1) }

}

interval(60)

S = sin(curtime()*300)*0.5 + 0.5

holoScaleUnits(8, -vec(40,40,1)*S)
holoScaleUnits(9, -vec(40,40,1)*S)
