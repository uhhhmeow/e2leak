@name hexc10
@inputs 
@outputs FindID FR:array Pieces:table
@persist [UI_HexDiameter UI_HexMeshX UI_HexMeshY UI_HoloIndex UI_Cursor] UI_HexColors:array UI_Offset:vector UI_Entity:entity 
@persist [GM_Board GM_Ranks GM_Files GM_Values]:table GM_Index 
@trigger none

if (first()) {

    UI_HexDiameter = 12
    UI_HexColors   = array(vec(50, 50, 50), vec(125, 125, 125), vec(255, 255, 255))
    
    UI_Entity   = entity()
    UI_HexMeshX = UI_HexDiameter*0.5*sqrt(3)
    UI_HexMeshY = UI_HexDiameter*0.5 + UI_HexDiameter*0.25

    GM_Ranks = table(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11)
    GM_Files = table("A", "B", "C", "D", "E", "F", "G", "H", "I", "K", "L")
                     #1   #2   #3   #4   #5   #6   #7   #8   #9   #10  #11

    GM_Values = table(
        "A" = 0,
        "B" = 6,
        "C" = 6 + 7,
        "D" = 6 + 7 + 8,
        "E" = 6 + 7 + 8 + 9,
        "F" = 6 + 7 + 8 + 9 + 10,
        "G" = 6 + 7 + 8 + 9 + 10 + 11,
        "H" = 6 + 7 + 8 + 9 + 10 + 11 + 10,
        "I" = 6 + 7 + 8 + 9 + 10 + 11 + 10 + 9,
        "K" = 6 + 7 + 8 + 9 + 10 + 11 + 10 + 9 + 8,
        "L" = 6 + 7 + 8 + 9 + 10 + 11 + 10 + 9 + 8 + 7
    )
    
    function number getN( RF:string ) {
        return GM_Values[RF[1], number] + RF:right(1):toNumber()
    }
    
    function table:setPiece( RF:string, Piece:string ) {
        This[getN(RF), table]["piece", string] = Piece    
    }
    
    UI_Offset = vec(-UI_HexMeshX*3, UI_HexMeshY*6, 0)
    
    holoCreate(UI_Cursor)
    holoModel(UI_Cursor, "hexagon")
    holoScaleUnits(UI_Cursor, vec(UI_HexDiameter - 1):setZ(1))
    holoAng(UI_Cursor, UI_Entity:angles())
    holoParent(UI_Cursor, UI_Entity)
    holoDisableShading(UI_Cursor, 1) 
    holoMaterial(UI_Cursor, "phoenix_storms/fender_white")
    holoColor(UI_Cursor, vec4(255, 125, 0, 125))

    findClearWhiteList()
    findClearBlackList()
    findIncludeModel( "models/holograms/hexagon.mdl" )
    findIncludePlayerProps( owner() )
    findExcludeEntity( holoEntity(UI_Cursor) )
    
    
    Pieces = table(
        "p" = "Pawn",
        "r" = "Rook",
        "n" = "Knight",
        "b" = "Bishop",
        "q" = "Queen",
        "k" = "King",
        "x" = "Empty"
    )


}

if (UI_HoloIndex < 91) {
    interval(15)

    while (holoCanCreate() & UI_HoloIndex < 91) {

        UI_HoloIndex++
        holoCreate(UI_HoloIndex)
        holoModel(UI_HoloIndex, "hexagon")
        holoScaleUnits(UI_HoloIndex, vec(UI_HexDiameter):setZ(1))
        holoAng(UI_HoloIndex, UI_Entity:angles())
        holoParent(UI_HoloIndex, UI_Entity)
        holoDisableShading(UI_HoloIndex, 1) 
        holoMaterial(UI_HoloIndex, "phoenix_storms/fender_white")
        
        findIncludeEntity( holoEntity(UI_HoloIndex) )
    }  
    
    if (UI_HoloIndex >= 91) {
        local ColorStart = 0
        local ColorInner = 0
        
        for (I = 0, 5) {
            for (J = 0, 5 + I) {
                GM_Index++
                GM_Board[GM_Index, table] = table(
                    "rank" = GM_Ranks[J + 1, number], # integer 1-11
                    "file" = GM_Ranks[I + 1, number], # integer 1-11
                    "cell" = GM_Files[I + 1, string] + GM_Ranks[J + 1, number]
                )
                
                local Rank = GM_Board[GM_Index, table]["rank", number]
                local File = GM_Board[GM_Index, table]["file", number]

                holoPos(GM_Index, UI_Entity:toWorld(UI_Offset + vec(Rank*UI_HexMeshX - File*UI_HexMeshX*0.5, -File*UI_HexMeshY, 0)))

                if (File != GM_Board[GM_Index - 1, table]["file", number]) {
                    if (File < 7) {
                        ColorStart++
                        if (ColorStart > 2) { ColorStart = 0 }    
                    } 
                    else {
                        ColorStart--
                        if (ColorStart < 0) { ColorStart = 2 }
                    }
                    
                    holoColor(GM_Index, UI_HexColors[ColorStart + 1, vector])
                    ColorInner = ColorStart
                }
                else {
                    ColorInner++
                    if (ColorInner > 2) { ColorInner = 0 }
                    holoColor(GM_Index, UI_HexColors[ColorInner + 1, vector])
                }
            }
        }

        for (I = 6, 10) {
            for (J = 0, 15 - I) {
                GM_Index++
                GM_Board[GM_Index, table] = table(
                    "rank" = GM_Ranks[J + 1, number], # integer 1-11
                    "file" = GM_Ranks[I + 1, number], # integer 1-11
                    "cell" = GM_Files[I + 1, string] + GM_Ranks[J + 1, number]
                )

                local Rank = GM_Board[GM_Index, table]["rank", number]
                local File = GM_Board[GM_Index, table]["file", number]
 
                holoPos(GM_Index, UI_Entity:toWorld(UI_Offset + vec(Rank*UI_HexMeshX + (File - 12)*UI_HexMeshX*0.5, -File*UI_HexMeshY, 0)))
               
                if (File != GM_Board[GM_Index - 1, table]["file", number]) {
                    if (File < 7) {
                        ColorStart++
                        if (ColorStart > 2) { ColorStart = 0 }    
                    } 
                    else {
                        ColorStart--
                        if (ColorStart < 0) { ColorStart = 2 }
                    }
                    
                    holoColor(GM_Index, UI_HexColors[ColorStart + 1, vector])
                    ColorInner = ColorStart
                }
                else {
                    ColorInner++
                    if (ColorInner > 2) { ColorInner = 0 }
                    holoColor(GM_Index, UI_HexColors[ColorInner + 1, vector])
                }
            }
        }
        
        
        GM_Board:setPiece( "B1", "P" )
        GM_Board:setPiece( "C2", "P" )
        GM_Board:setPiece( "D3", "P" )
        GM_Board:setPiece( "E4", "P" )
        GM_Board:setPiece( "F5", "P" )
        GM_Board:setPiece( "G4", "P" )
        GM_Board:setPiece( "H3", "P" )
        GM_Board:setPiece( "I2", "P" )
        GM_Board:setPiece( "K1", "P" )
        
    } 
}
else {
    
    interval(60)

    if (findCanQuery()) {
        local Player = owner()
    
        local LAim = UI_Entity:toLocal( Player:eyeTrace():position() ):setZ(0)
        local WAim = UI_Entity:toWorld( LAim )  
        
        findClipToClass("gmod_wire_hologram")
        findInSphere(WAim, UI_HexDiameter/2)
        findSortByDistance(WAim)
        
        FindID = holoIndex(findResult(1))
    }
    
    if (changed(FindID)) { holoPos(UI_Cursor, holoEntity(FindID):toWorld(vec(0, 0, 2))) }
    
}

