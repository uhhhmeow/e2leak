@name step_test_5
@inputs 
@outputs Speed
@persist [POS_Start POS_Height POS_Finish POS_Current]:vector STEP_Stage STEP_Side:string
@persist [POS_Left POS_Right POS_Average]:vector Biped:table Wait Frac MoveSpeed Speed
@persist Entity:entity
@trigger none


if (first()) {
    
    Entity    = entity()

    STEP_Side = "left"
    POS_Start = POS_Height = POS_Finish = POS_Current = Entity:pos()

    Biped["left", vector] = Biped["right", vector] = Entity:pos()
    POS_Average = Entity:pos()
    
    
    holoCreate(hash("left"))
    holoCreate(hash("right"))
    
    holoCreate(1)

    holoEntity(1):soundPlay("test", 0, "vehicles/apc/apc_cruise_loop3.wav")
    soundPitch("test", 100)
    soundVolume("test", 0)

}

interval(60)

MoveDir   = vec(1,0,0):rotate(0, 0, 0)


local Forward = owner():keyPressed("up") - owner():keyPressed("down")

MoveSpeed = MoveSpeed + clamp( Forward*100 - MoveSpeed, -1, 1  )

switch (STEP_Stage) {
    
    case 0,

        local POS_Offset  = STEP_Side == "left" ? vec(0, 30, 0) : vec(0, -30, 0)
        
        POS_Start  = Biped[STEP_Side, vector]
        POS_Finish = toWorld( POS_Offset + MoveDir*MoveSpeed, ang(), POS_Average, MoveDir:toAngle() )

        local Dist = POS_Start:distance(POS_Finish)
        
        POS_Height = POS_Finish + vec(0, 0, Dist/3)
        
        if (Dist > 1) {
            Frac = 0
            STEP_Stage++
        }
        else {
            Wait = curtime()
            STEP_Stage++
            STEP_Stage++   
        }
    
    break    
    
    case 1,
        Speed = min(Speed + 10, 180)
        Frac = min( Frac + abs(sin(Speed)/10), 1 )
        
        Biped[STEP_Side, vector] = bezier(POS_Start, POS_Height, POS_Finish, Frac)
        
        #soundVolume("test", Frac)
        soundVolume("test", 1)
        soundPitch("test", 80 - Frac*35 + MoveSpeed/100)
        
        #Biped[STEP_Side, vector] = mix(POS_Finish, POS_Start, Frac) + vec(0, 0, sin(Frac*180)*25)
        
        if (Frac == 1) {
            Wait = curtime()
            Biped[STEP_Side, vector] = POS_Current = POS_Finish
            STEP_Stage++  
            Speed = 0
            soundVolume("test", 0)
            
            holoEntity(hash(STEP_Side)):soundPlay(STEP_Side + "1", 0, "doors/doormove2.wav")
            soundVolume(STEP_Side + "1", 0.5)
            soundPitch(STEP_Side + "1", randint(100, 120))
            
            
            entity():soundPlay(STEP_Side + "2", 0, "^doors/garage_stop1.wav")
            
        }

    
    break
    
    case 2,
        
        if (min(curtime() - Wait, 1) == 1) {
            STEP_Side = STEP_Side == "left" ? "right" : "left"
            STEP_Stage = 0
        }
    
    break
    
}

POS_Average = (Biped["left", vector] + Biped["right", vector])/2

holoPos(hash(STEP_Side), Biped[STEP_Side, vector])
holoPos(1, POS_Average + vec(0, 0, 50))
holoAng(1, MoveDir:toAngle())
