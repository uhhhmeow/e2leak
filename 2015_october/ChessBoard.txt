@name ChessBoard
@inputs 
@outputs 
@persist BoardEntity:entity BoardString:string HoloID
@trigger none
@model models/sprops/rectangles/size_7/rect_144x144x3.mdl

if (first()) {
    BoardString = "rnbqkbnrpppppppp................................PPPPPPPPRNBQKBNR"
    BoardEntity = entity()
    BoardEntity:setAlpha(0)
    
    function createBoard() {
        local Black = vec(50,50,50)
        local White = vec(255,255,255)
    
        for (I = 1, 64) {
            HoloID++
            
            local X = floor((HoloID - 1) / 8)
            local Y = (HoloID - 1) % 8
            
            holoCreate(HoloID, BoardEntity:toWorld(vec(18*Y - 72 + 9, 18*X - 72 + 9, 0)), vec(17,17,17)/12)
            holoParent(HoloID, BoardEntity)
            holoMaterial(HoloID, "phoenix_storms/fender_white")
            holoModel(HoloID, "plane")
            holoColor(HoloID, (X + Y) % 2 == 0 ? White : Black)
        }
        
        HoloID++
        holoCreate(HoloID)
        holoScaleUnits(HoloID, -vec(144,144,1))
        holoColor(HoloID, Black)
        holoParent(HoloID, BoardEntity)
    }
    
    function createPieces() {
        local Black = vec(50,50,50)
        local White = vec(255,255,255)
        
        local PieceModels = table(
            "r" = "models/props_phx/games/chess/black_rook.mdl",
            "n" = "models/props_phx/games/chess/black_knight.mdl",
            "b" = "models/props_phx/games/chess/black_bishop.mdl",
            "q" = "models/props_phx/games/chess/black_queen.mdl",
            "k" = "models/props_phx/games/chess/black_king.mdl",
            "p" = "models/props_phx/games/chess/black_pawn.mdl",
        
            "R" = "models/props_phx/games/chess/white_rook.mdl",
            "N" = "models/props_phx/games/chess/white_knight.mdl",
            "B" = "models/props_phx/games/chess/white_bishop.mdl",
            "Q" = "models/props_phx/games/chess/white_queen.mdl",
            "K" = "models/props_phx/games/chess/white_king.mdl",
            "P" = "models/props_phx/games/chess/white_pawn.mdl"
        )
        
        for (I = 1, 64) {
            local Piece = BoardString[I]
            if (Piece != ".") {
                HoloID++

                local Team = inrange(toByte(Piece), 96, 122)
                holoCreate(HoloID, 
                    holoEntity(I):toWorld(vec(0,0,0.75)), vec(1, 1, 1)*0.95, 
                    holoEntity(I):toWorld(ang(0, !Team*180 + 90, 0)), Team ? Black : White, PieceModels[Piece, string])
                
                holoMaterial(HoloID, "phoenix_storms/fender_white")
                holoParent(HoloID, BoardEntity)
            }    
        }        
    }
    
    timer("Create", 500)
}

if (clk("Create")) {
    if (!HoloID) {
        createBoard()
        timer("Create", 500)    
    } else {
        createPieces()
    }
}



#[    for (I = 1, 64) {
        #egp
        ScreenID++

        local X = floor((ScreenID - 1) / 8)
        local Y = (ScreenID - 1) % 8

        GUI:egpBox(ScreenID, vec2(64*Y + 32, 64*X + 32), vec2(62, 62))
        GUI:egpColor(ScreenID, (X + Y) % 2 == 0 ? White : Black)
        
        #holos
        if (HoloBoard[ScreenID] != ".") {
            HoloIndex++
            
            local Team = inrange(toByte(HoloBoard[ScreenID]), 96, 122)
            holoCreate(HoloIndex, 
                GUI:egpToWorld(GUI:egpPos(ScreenID)), vec(0.5, 0.5, 0.5), 
                GUIENT:toWorld(ang(0, !Team*180, 0)), Team ? Black : White, PieceModels[HoloBoard[ScreenID], string])
            
            holoMaterial(HoloIndex, "phoenix_storms/fender_white")
            holoParent(HoloIndex, GUIENT)
            
            GUIENT["HoloID" + ScreenID, number] = HoloIndex
        } else {
            GUIENT["HoloID" + ScreenID, number] = 0
        }
    }]#
