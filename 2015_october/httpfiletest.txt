@name httpfiletest
@persist AI:string

if (first()) {
    runOnHTTP(1)    
    
    FEN = "6/P5p/RP4pr/N1P3p1n/Q2P2p2q/BBB1P1p1bbb/K2P2p2k/N1P3p1n/RP4pr/P5p/6/ w"

    AI += "from __future__ import print_function, division\n"
    AI += "from collections import Counter, OrderedDict, namedtuple\n"
    AI += "from itertools import count\n"
    AI += "import zlib, base64, re, sys, urllib\n"
    AI += "exec(zlib.decompress(base64.b64decode(urllib.urlopen('https://dl.dropboxusercontent.com/s/twlm8gvo5qn3znz/ghex_ai_b64.txt').read())))\n"

    AI = httpUrlEncode(AI)
    FEN = httpUrlEncode(FEN)
    
    #-- Single move generation based on current game state
    httpRequest("http://www.rextester.com/rundotnet/api?LanguageChoiceWrapper=5&Program=" + AI + "&Input=" + FEN)
    
    #-- Generate a full list of ai-vs-ai moves
    #httpRequest("http://www.rextester.com/rundotnet/api?LanguageChoiceWrapper=5&Program=" + AI + "&Input=aigame")
}

if (httpClk()) {
    local JSON = httpData()
    local Results = JSON:matchFirst("\"Result\":\"(.*)\",\"Stats\""):explode("\\n"), Results:pop()

    printTable(Results)
}
