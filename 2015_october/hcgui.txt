@name hcgui
@inputs 
@outputs 
@persist GUI:table Settings:table
@trigger none


if (first()) {
    
    Settings = table(
        "string_board"        = "",
        "hexagon_radius"      = 32,
        "hexagon_color_dark"  = vec(),
        "hexagon_color_mid"   = vec(125),
        "hexagon_color_light" = vec(255)
    )

    function setupGUI() {
        GUI = GUI:add(Settings)

        GUI["hexagon_mesh_x", number] = GUI["hexagon_radius", number]*sqrt(3)*0.5
        GUI["hexagon_mesh_y", number] = GUI["hexagon_radius", number]*0.5 + GUI["hexagon_radius", number]*0.25
        GUI["hexagon_offset", vector] = vec(-GUI["hexagon_mesh_x", number]*3, GUI["hexagon_mesh_y", number]*6, 0)
        
        GUI["initialized", number] = 0
        GUI["hexagons", array] = array()
        GUI["pieces", array] = array()
        
        GUI["entity", entity] = entity()
        GUI["owner", entity] = owner()
    }
    
    function number getRank(Index) {
        return toByte("ABCDEFABCDEFGABCDEFGHABCDEFGHIABCDEFGHIJABCDEFGHIJKABCDEFGHIJABCDEFGHIABCDEFGHABCDEFGABCDEF"[Index]) - 64
    }    
    
    function number getFile(Index) {
        return toByte("AAAAAABBBBBBBCCCCCCCCDDDDDDDDDEEEEEEEEEEFFFFFFFFFFFGGGGGGGGGGHHHHHHHHHIIIIIIIIJJJJJJJKKKKKK"[Index]) - 64
    }
    
    function string getNotation(Index) {
        return toChar(getFile(Index) + 64) + getRank(Index)
    }
    
    function number getIndex(Notation:string) {
        local Bit = (toByte(Notation[1]) - 65)*2 + 1 
        return "0006132130405161707885":sub(Bit, Bit + 1):toNumber() + Notation:sub(2):toNumber()
    }
    
    function number getColor(Index, Shift) {
        local File = getFile(Index)
        
        switch (File < 7) {
            case 1, return (Index + (1 - File)^2 - Shift) % 3 + 1, break
            case 0, return (Index - (File - 11)^2 - 1 - Shift) % 3 + 1, break    
        }
    }
         
    function spawnHexagon(Index) {
        local File = getFile(Index)
        local Rank = getRank(Index)
        
        holoCreate(Index)
        holoModel(Index, "hexagon")
        holoParent(Index, GUI["entity", entity])
        holoScaleUnits(Index, vec(GUI["hexagon_radius", number] - 1):setZ(6))
        
        local Pos = vec()
        switch (File <= 6) {
            case 1, Pos = vec(Rank*GUI["hexagon_mesh_x", number] - File*GUI["hexagon_mesh_x", number]*0.5, -File*GUI["hexagon_mesh_y", number], 0), break
            case 0, Pos = vec(Rank*GUI["hexagon_mesh_x", number] + (File - 12)*GUI["hexagon_mesh_x", number]*0.5, -File*GUI["hexagon_mesh_y", number], 0), break
        }
 
        holoPos(Index, GUI["entity", entity]:toWorld(GUI["hexagon_offset", vector] + Pos))
        holoColor(Index, select(getColor(Index, 1), vec(255), vec(125), vec()))
    }

    setupGUI()
    
    interval(60)
}

if (!GUI["initialized", number]) {
    interval(15)
    
    local Count = 91
    while (perf() & holoCanCreate() & GUI["spawn_index", number] < Count) {
        GUI["spawn_index", number] = GUI["spawn_index", number] + 1
        
        spawnHexagon(GUI["spawn_index", number])

        if (GUI["spawn_index", number] >= Count) {
            GUI["initialized", number] = 1
        }
    }
}

