@name surf miner surf
@outputs Dist
@persist RD:ranger Shift Bun Crouch Alt Del Del2 Jump [Fly O Forward]:entity Dist Wep:string ON M1 OnMsg:array Prop:entity
runOnTick(1)
runOnChat(1)
O=owner()
Wep=O:weapon():type()
M1=O:keyAttack1()
Alt=O:keyWalk()
Jump=O:keyJump()
Crouch=O:keyDuck()
Shift=O:keySprint()
if(first()){
    OnMsg[1,string]="Surf Fall Protection: OFF"
    OnMsg[2,string]="Surf Fall Protection: ON"
}
if(Wep=="weapon_physcannon"&changed(M1)&M1){
    ON=!ON
    printColor(vec(0,0,0),OnMsg[ON+1,string])
}
if(O:lastSaid()=="!bunny"&chatClk(owner())){
    Bun=1
}
if(ON){
    RD=ranger(O,-180)
    Dist=RD:distance()
    if(Jump&changed(Alt)&Alt&!Del){
        Fly=propSpawn("models/props_phx/construct/metal_dome360.mdl",O:pos(),ang(),1)
        timer("delete",200)
        Del=1
    }
    if(Jump&changed(Crouch)&Crouch&!Del2){
        Forward=propSpawn("models/props_phx/construct/metal_dome360.mdl",O:pos()-O:eye()*60,O:toWorld(ang(90,0,0)),1)
        Del2=1
        timer("delete2",200)
    }
    if(clk("delete2")){
        stoptimer("delete2")
        Forward:propDelete()
        Del2=0
    }
    if(clk("delete")){
        Fly:propDelete()
        Del=0
        stoptimer("delete")
    }
    if(Bun){
        if(Alt){
            if(Dist<10){
                concmd("+jump")
            }elseif(Dist>15){
               concmd("-jump")
            }
        }
    }
}
if(Dist<100&ON&O:health()>0&O:vel():length()>950&O:aimEntity():owner()!=owner()&RD:entity():owner()!=owner()&!Del){
    Prop=propSpawn("models/props_docks/dockpole02a.mdl",RD:pos(),ang(),1)
}
if(O:vel():length()<40&changed(O:vel())){
    Prop:propDelete()
}

