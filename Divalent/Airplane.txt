@name Airplane
@inputs
@outputs
@persist T Propspeed Vel Timer
@trigger

E = entity():isWeldedTo()
if(first()) {   
    runOnTick(1)
    E:setMass(50)
    entity():setMass(50)
}
entity():setMass(0)
E=entity():isWeldedTo()
E:setMass(50000)
Chair=entity():isWeldedTo()
Driver=Chair:driver()
W=Driver:keyForward()
S=Driver:keyBack()
A=Driver:keyRight()
D=Driver:keyLeft()
Rvel=E:angVel():roll()
Pvel=E:angVel():pitch()
Yvel=E:angVel():yaw()
Pd=E:angles():pitch()/100
R1=E:angles():roll()/100
Range=rangerOffset(100,E:pos()+vec(0,0,-8),-E:up())
Height=Range:distance()
E:applyForce(-E:vel()*E:mass())
E:applyAngForce(ang(Rvel,-Yvel,-Pvel)*E:mass())
E:applyForce(vec(0,0,1)*E:mass()*7.511255)
if(Height<40|!Driver)
{
    E:applyForce(vec(0,0,1)*E:mass()*(28-Height))
    E:applyForce(-vec(0,0,1)*E:mass()*5*(Height-28))
}

E:applyAngForce(ang(R1,0,-Pd)*E:mass()*100)

if (A) {
    E:applyAngForce(ang(0,1,0)*E:mass()*100)
}

if (D) {
    E:applyAngForce(ang(0,-1,0)*E:mass()*100)
}

Forward=E:forward()*E:mass()*150
E:applyForce((Forward+$Forward)*T)
if(W){T+=0.01}
if(S) {E:applyAngForce(ang(0,0,1)*E:mass()*100)}
if(T<0){T=0}
if(T>5){T=5}
Vel=E:vel():length()
Propspeed+=clamp(T*10,0,500)
if(Driver&T<2&Height>40)
{
    E:applyForce(-vec(0,0,1)*E:mass()*100)    
    T=2
}
