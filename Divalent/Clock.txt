@name Clock
@inputs EGP:wirelink 
runOnTick(1)

Hour = time("hour")
Minute = time("min")
Second = time("sec")

if (Hour>=12+4 | Hour<=4) {
    EGP:egpText(1,"PM",vec2(256,310))
    EGP:egpFont(1,"Times New Roman",40)
    EGP:egpAlign(1,1,1)
} else {
    EGP:egpText(1,"AM",vec2(256,310))
    EGP:egpFont(1,"Times New Roman",40)
    EGP:egpAlign(1,1,1)
}

MYHour = Hour-4
MYSecond = Second-2
EGP:egpText(2,MYHour+":"+Minute+":"+Second,vec2(256,256))
EGP:egpFont(2,"Times New Roman",100)
EGP:egpAlign(2,1,1)

if (changed(owner():keyWalk()) & owner():keyWalk()){
    EGP:egpClear()
}
