@name Lizard
@inputs [Pod Cam]:wirelink 
@outputs 
@persist T 
@trigger 
interval(60)

E = entity()
Links = 5
Length = 0.2
Thick = 0.1
Bend = 4

if (first() | dupefinished()) {
    #Head
    for (I = 10,13) {
        holoCreate(I)
        holoParent(I, I > 10 ? holoEntity(I-1) : entity())
        holoPos(I,holoEntity(I):parent():toWorld(vec(5,0,0)))
        holoScale(I,vec(0.1))
    }
    
    #Tail
    for (I2 = 1,Links) {
        holoCreate(I2)   
        holoModel(I2,"models/props_c17/oildrum001.mdl")
        holoScale(I2,vec(Thick,Thick,Length))
        
        if (holoEntity(I2)) {
            T++
        } else {break}
    }
    
    holoCreate(14)
    holoModel(14,"hq_rcylinder_thick")
    holoPos(14,E:toWorld(vec(-10,0,0)))
    holoAng(14,E:toWorld(ang(0,90,90)))
    holoScale(14,vec(1,1,2.2))
    holoParent(14,E)
    
    holoCreate(15)
    holoModel(15,"models/holograms/hq_cubinder.mdl")
    holoPos(15,holoEntity(13):toWorld(vec(0,0,0)))
    holoAng(15,E:toWorld(ang(0,90,90)))
    holoScale(15,vec(0.4,0.4,0.4))
    holoParent(15,13)
    
    holoCreate(0)
    holoPos(0,E:toWorld(vec(-30,0,40)))
    holoAng(0,E:toWorld(ang(0,-90,0)))
    holoAlpha(0,0)
    holoParent(0,E)
    
    Offset = holoEntity(0):pos()
    Self = holoEntity(0)
    Cam["Parent", entity] = Self
    Cam["Position", vector] = Offset
}

for (I2 = 1,Links) {
    if (I2 == 1) {
        holoPos(I2,entity():pos()-entity():forward()*24)    
        
        Ang = (holoEntity(I2+1):pos()-entity():pos()):toAngle()+ang(90,0,0)
        holoAng(I2,Ang)
    } elseif(I2 == Links) {
        holoPos(I2,holoEntity(I2-1):pos()+holoEntity(I2-1):up()*Length*34)
        Ang = (holoEntity(I2-1):pos()-holoEntity(I2):pos()):toAngle()+ang(90,0,0)
        holoAng(I2,Ang)    
    } else {
        holoPos(I2,holoEntity(I2-1):pos()+holoEntity(I2-1):up()*Length*34)
        Ang = (holoEntity(I2+1):pos()-holoEntity(I2-1):pos()):toAngle()+ang(90,0,0)
        holoAng(I2,Ang)
    }
}

local Seat = Pod["Entity",entity]
local Driver = Seat:driver()
local Active = Driver:isPlayer()
local W = Driver:keyForward()
local A = Driver:keyLeft()
local S = Driver:keyBack()
local D = Driver:keyRight()
local Space = Driver:keyJump()
local Shift = Driver:keySprint()
local M1 = Driver:keyAttack1()
local M2 = Driver:keyAttack2()
Cam["Activated",number] = Active

rangerFilter(E)
Ranger = rangerOffset(9999,E:pos(),Driver:eye())

TargetPos = Ranger:pos()
TargetAng = angnorm((TargetPos - holoEntity(10):pos()):toAngle() - entity():angles())/4

if (Active) {
    holoAng(10,entity():toWorld(TargetAng))
    holoAng(11,holoEntity(10):toWorld(TargetAng))
    holoAng(12,holoEntity(11):toWorld(TargetAng))
    holoAng(13,holoEntity(12):toWorld(TargetAng))
}
