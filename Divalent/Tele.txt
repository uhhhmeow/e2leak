@name Tele
@inputs TV:entity S:wirelink 
@outputs User:entity  
@persist A State Current [BGChange SDChange]:array
@trigger 
interval(50)

findIncludeClass("player")
User = findClosest(entity():pos())

Cursor = S:egpCursor(User)

BGChange = array(
    "console/title_fullmoon",
    "vgui/appchooser/background_ep2",
    "vgui/appchooser/background_episodic",
    "vgui/appchooser/background_hl2"
    )
SDChange = array(
    "ambient/music/dustmusic1.wav",
    "basscannon/bass4.mp3",
    "basscannon/bass2.mp3"
    )
    
Seconds = 4000

if (first()) {
    Current = 1
    holoCreate(1)
    holoPos(1,entity():toWorld(vec(-18.1,0,34.46)))
    holoScale(1,vec(1,6,0.1))
    holoClipEnabled(1,1,1)
    holoClip(1,holoEntity(1):pos()+vec(6,0,0),vec(-1,0,0),1)
    
    holoCreate(2)
    holoPos(2,entity():toWorld(vec(-18.1,0,32.46)))
    holoScale(2,vec(1,6,0.1))
    holoColor(2,vec(0,0,0))
    
    holoCreate(3)
    holoModel(3,"models/hunter/plates/plate05x075.mdl")
    holoPos(3,entity():toWorld(vec(-1.8,23.8134,23.1)))
    holoAng(3,entity():angles()+ang(0,90,90))
    
    holoCreate(4)
    holoModel(4,"models/hunter/plates/plate05x075.mdl")
    holoPos(4,entity():toWorld(vec(-1.8,-23.659,23.1)))
    holoAng(4,entity():angles()+ang(0,90,90))
    
    holoCreate(5)
    holoModel(5,"models/cheeze/wires/speaker.mdl")
    holoPos(5,entity():toWorld(vec(-1.9,23.659,25)))
    holoAng(5,entity():angles()+ang(0,-90,90))
    holoParent(5,3)
    
    holoCreate(6)
    holoModel(6,"models/cheeze/wires/speaker.mdl")
    holoPos(6,entity():toWorld(vec(-1.9,23.659,10)))
    holoAng(6,entity():angles()+ang(0,-90,90))
    holoParent(6,3)
    
    holoCreate(7)
    holoModel(7,"models/cheeze/wires/speaker.mdl")
    holoPos(7,entity():toWorld(vec(-1.9,-23.659,25)))
    holoAng(7,entity():angles()+ang(0,-90,90))
    holoParent(7,4)
    
    holoCreate(8)
    holoModel(8,"models/cheeze/wires/speaker.mdl")
    holoPos(8,entity():toWorld(vec(-1.9,-23.659,10)))
    holoAng(8,entity():angles()+ang(0,-90,90))
    holoParent(8,4)
    
    holoCreate(10)
    holoModel(10,"hq_stube_thin")
    holoPos(10,entity():toWorld(vec(-3.2,24,17)))
    holoAng(10,ang(0,90,90))
    holoScale(10,vec(2,3,0.08))
    holoColor(10,vec(0,0,0))
    holoParent(10,3)
    
    holoCreate(11)
    holoModel(11,"hq_stube_thin")
    holoPos(11,entity():toWorld(vec(-3.2,-24,17)))
    holoAng(11,ang(0,90,90))
    holoScale(11,vec(2,3,0.08))
    holoColor(11,vec(0,0,0))
    holoParent(11,4)
    
    holoMaterial(1,"models/props/CS_militia/roofbeams01")
    holoMaterial(3,"models/props/CS_militia/roofbeams01")
    holoMaterial(4,"models/props/CS_militia/roofbeams01")
    
    S:egpClear()
    S:egpBox(1,vec2(256,256),vec2(512,512))
}

S:egpMaterial(1,BGChange[Current,string])

timer("Changer",Seconds)
if(clk("Changer")){Current+=1}
if(Current>=BGChange:count()+1){Current=1}

if(changed(inrange(Cursor, S:egpPos(1)-S:egpSize(1)/2, S:egpPos(1)+S:egpSize(1)/2) & User:keyUse())){
    Open = 1
}

R = owner():keyWalk()

if(changed(R) & R) {if(State == 0) {State = 1} else {State = 0}}

if(State == 1) {
    if(A < 0.38) {
        A += 0.01
        TV:setPos(TV:pos()+vec(0,0,1))
        holoPos(1,holoEntity(1):pos()+vec(0.4,0,0))
        holoAng(3,holoEntity(3):angles()+ang(0,-4.74,0))
        holoAng(4,holoEntity(4):angles()+ang(0,4.74,0))
    }
} else {
    if(A > 0) {
        A -= 0.01
        TV:setPos(TV:pos()+vec(0,0,-1))
        holoPos(1,holoEntity(1):pos()+vec(-0.4,0,0))
        holoAng(3,holoEntity(3):angles()+ang(0,4.74,0))
        holoAng(4,holoEntity(4):angles()+ang(0,-4.74,0))
    }
}

if (changed(State == 1) & State == 1) {
    soundPlay(1,0,SDChange[Current,string])
} elseif (State == 0) { soundStop(1) }

if(S:entity():pos():distance(User:pos()) > 100) {S}

S:egpCircle(100, vec2(0,0), vec2(2,2))
S:egpColor(100,vec4(255,255,255,0))
S:egpParentToCursor(100)
