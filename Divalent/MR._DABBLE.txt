@name MR. DABBLE
@inputs D:entity 
@outputs User:entity 
@persist A [CornerPos1 CornerPos2]:vector
@trigger 
interval(50)

if (first()) {
    Scale = D:boxSize() + vec(0,0,40)
    
    Holo = holoCreate(1)
    
    holoScaleUnits(1,Scale)
    holoPos(1,D:toWorld(vec(0,0,20)))
    holoAng(1,D:angles())
    holoColor(1,vec(255,255,255),255)
    holoParent(1,entity())
    
    CornerPos1 = Scale / 2
    CornerPos2 = -CornerPos1
    
    CornerPos1 = Holo:toWorld(CornerPos1)
    CornerPos2 = Holo:toWorld(CornerPos2)
    
    CornerScale = vec() + 1
    
    holoCreate(2)
    holoPos(2,CornerPos1)
    holoScaleUnits(2,CornerScale)
    holoParent(2,Holo)
    
    holoCreate(3)
    holoPos(3,CornerPos2)
    holoScaleUnits(3,CornerScale)
    holoParent(3,Holo)
    
    for (I = 1,3) {holoColor(I,vec(255),255)}
}
