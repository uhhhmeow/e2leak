@name Panel Door
@inputs Input 
@outputs 
@persist Use Rate GAIN Open [E1 E2 E3 E4 E]:entity All Dist 
@trigger 

#runOnTick(1)
interval(50)

if(first()){
    E=entity()
    GAIN=11.7
    E1=propSpawn("models/hunter/plates/plate05x2.mdl",E:toWorld(vec(0,11,25)),1)
    E2=propSpawn("models/hunter/plates/plate05x2.mdl",E:toWorld(vec(0,11,25)),1)
    E3=propSpawn("models/hunter/plates/plate05x2.mdl",E:toWorld(vec(0,11,25)),1)
    E4=propSpawn("models/hunter/plates/plate05x2.mdl",E:toWorld(vec(0,11,25)),1)
    
    E1:setAng(E:toWorld(ang(0,90,90)))
    E2:setAng(E:toWorld(ang(0,90,90)))
    E3:setAng(E:toWorld(ang(0,90,90)))
    E4:setAng(E:toWorld(ang(0,90,90)))
    Rate=100*(100/1000)
}

Use = owner():keyUse()
All = 1
Dist = 0
if (Input){
    Open=1
} else {
    if (All){
        for (I=1,players():count()){
            TP=players()[I,entity]
            Open=0
            if (TP:shootPos():distance(entity():pos())<Dist){
                Open=1
                break
            }
        }
    }
}
GAIN+=clamp((Open ? 95 : 0)-GAIN,-Rate,Rate)

E1:setPos(E:toWorld(vec(0,GAIN,11.4)))
E2:setPos(E:toWorld(vec(0,GAIN,35)))
E3:setPos(E:toWorld(vec(0,GAIN,58.7)))
E4:setPos(E:toWorld(vec(0,GAIN,82.5)))

E1:setAng(E:toWorld(ang(90,0,0)))
E2:setAng(E:toWorld(ang(90,0,0)))
E3:setAng(E:toWorld(ang(90,0,0)))
E4:setAng(E:toWorld(ang(90,0,0)))

if (!E1:isFrozen()&!E2:isFrozen()&!E3:isFrozen()&!E4:isFrozen()) {E1:propFreeze(1) E2:propFreeze(1) E3:propFreeze(1) E4:propFreeze(1)}

if (changed(Open)&!(first()|dupefinished())){
    if (Open){
        entity():soundPlay(1,2,"doors/doormove2.wav")
    }
    if (!Open){
        entity():soundPlay(1,2,"doors/doormove3.wav")
    }
}
