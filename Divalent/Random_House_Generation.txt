@name Random House Generation
@inputs 
@outputs 
@persist M I 
@trigger 

E = entity()

if (first()) {M = 3}

if (I < M & holoCanCreate() & propCanCreate()) {
    interval(15)
    
    I++
    
    Rng = rangerOffset(1010,entity():toWorld(vec(randint(-500,500)*1,randint(-500,500)*1,1000)),vec(0,0,-1))
    
    holoCreate(I,Rng:pos()+holoEntity(I):boxSize())
    holoAng(I,Rng:hitNormal():toAngle()+ang(90,0,0))
} else {interval(100)}
