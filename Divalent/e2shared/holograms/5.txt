@name 5
@inputs
@outputs
@persist I M E:entity
interval(100)

if (dupefinished()) {reset()}

if (first()) {
    M = 1
    E = entity():isWeldedTo()
}

if (I < M & holoCanCreate()) {
    I++

    holoCreate(I)
    holoParent(I,E)

    holoModel(1,"models/sprops/rectangles/size_3_5/rect_30x96x3.mdl")
    holoPos(1,E:toWorld(vec(0,0,0)))
    holoAng(1,E:toWorld(ang(0,90,0)))
    

}