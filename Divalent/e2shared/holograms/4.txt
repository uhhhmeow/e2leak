@name 4
@inputs
@outputs
@persist I M E:entity
interval(100)

if (dupefinished()) {reset()}

if (first()) {
    M = 2
    E = entity():isWeldedTo()
}

if (I < M & holoCanCreate()) {
    I++

    holoCreate(I)
    holoParent(I,E)

    holoModel(1,"models/props_c17/oildrum001.mdl")
    holoPos(1,E:toWorld(vec(53.9839,-17.4722,-44.668)))
    holoAng(1,E:toWorld(ang(-0.0352,114.8876,0)))
    

    holoModel(2,"models/props_c17/oildrum001_explosive.mdl")
    holoPos(2,E:toWorld(vec(10.8193,-29.7593,-35.709)))
    holoAng(2,E:toWorld(ang(-0.0316,82.8554,0)))
    

}