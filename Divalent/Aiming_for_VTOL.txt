@name Aiming for VTOL
@inputs 
@outputs 
@persist Ang 
@trigger 
runOnTick(1)
runOnChat(1)

if (first()) {
    holoCreate(4)
    holoModel(4,"hqtorus2")
    holoScaleUnits(4,vec(40,40,4))
    holoColor(4,vec(35,35,35))
    holoCreate(5)
    holoModel(5,"hqcylinder2")
    holoScaleUnits(5,vec(2,4,35))
    holoColor(5,vec(35,35,35))
    holoCreate(6)
    holoModel(6,"hqcylinder2")
    holoScaleUnits(6,vec(2,4,35))
    holoColor(6,vec(35,35,35))
    holoAng(5,ang(90,90,0))
    holoAng(6,ang(90,0,0))
    holoPos(4,entity():pos()+vec(0,0,0.5))
    holoParent(6,4)
    holoParent(5,4)
}

    Owner = owner()
    E = Owner:keyUse()
    StartPos = Owner:aimPos()
    CurPos = StartPos + vec(0,0,5000)
    Speed = Move = Size = 0
    
if (E) {
    P = Owner:aimPos()-CurPos
    if(P:setZ(0):length()>5 | Speed){Speed+=0.3}
    CurPos+=( P:length()>(P:normalized()*Speed):length() ? P:normalized():setZ(0)*Speed : P:normalized() )
    Move = 1
}

if (chatClk(owner())) {
    M = lastSaid():explode(" ")
    if (M:string(1) == "give") {
        T = findPlayerByName(M:string(2))
        if (T) {
            print("Gave the artillery to: " + T:name())
            Owner = T
        } else {
            print("No player named " + M:string(2) + " found.")
    }
}
}
   if (owner():lastSaid() == "off"){
    holoDeleteAll()
}
   else{ if(owner():lastSaid() == "aim"){
    holoCreate(4)
    holoModel(4,"hqtorus2")
    holoScaleUnits(4,vec(40,40,4))
    holoColor(4,vec(35,35,35))
    holoCreate(5)
    holoModel(5,"hqcylinder2")
    holoScaleUnits(5,vec(2,4,35))
    holoColor(5,vec(35,35,35))
    holoCreate(6)
    holoModel(6,"hqcylinder2")
    holoScaleUnits(6,vec(2,4,35))
    holoColor(6,vec(35,35,35))
    holoAng(5,ang(90,90,0))
    holoAng(6,ang(90,0,0))
    holoPos(4,entity():pos()+vec(0,0,0.5))
    holoParent(6,4)
    holoParent(5,4)
    }
}
Ang++
if (Ang>360) {Ang = 0}
holoAng(4,ang(0,Ang,0))
holoPos(4,Owner:aimPos()+Owner:aimNormal()*2)

