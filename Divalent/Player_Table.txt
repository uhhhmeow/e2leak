@name Player Table
@outputs [Fade]:number
@persist [Table]:table [Clk,NoFilter]:number
runOnChat(1)
interval(250)

Clk = !Clk

if ( chatClk(owner()) & owner():lastSaid():sub(1,1) == "%" ) {
    hideChat(1)
    Said = owner():lastSaid()
    Command = Said:sub(2,Said:explode(" "):string(1):length())
    Argument = Said:sub(Command:length()+3,Said:length())
    if ( Command == "add" ) {
        Target = Argument ? findPlayerByName(Argument) : owner():aimEntity()
        if ( Target:isPlayer() ) {
            printColor(vec(255),"Added: ",vec(0,255,255),+Target:name():lower()+"",vec(255),"!")
            Table[teamName(Target:team()):lower(),entity] = Target
        }
    }
    elseif( Command == "remove" ) {
        print(Table:count())
        foreach ( K , V:entity = Table ) {
            print(V:name())
            if ( V:name():lower():find(Argument) ) {
                printColor(vec(255),"Removed: ",vec(255,0,0),+V:name():lower()+"",vec(255),"!")
                Table:remove(K)
                break
            }
        }
    }
    elseif( Command == "clear" ) {
        Table = table()
        print("Whitelist cleared!")
    }
    elseif( Command == "addjob" ) {
        Ply = players()
        for ( I = 1 , Ply:count() ) {
            Target = Ply[I,entity]
            if ( teamName(Target:team()):lower() == Argument ) {
                Table[teamName(Target:team()):lower(),entity] = Target
                printColor(vec(255),"Added: ",vec(0,255,255),+Target:name():lower()+"",vec(255),"!")
            }
        }
    }
    elseif( Command == "togglefilter" ) {
        NoFilter = !NoFilter
        printColor(vec(255),"Filter ",vec(0,0,255),NoFilter ? "off" : "on",vec(255),"!")
    }
}
elseif ( !Clk ) {
    foreach( K , V:entity = Table ) {
        if ( teamName(V:team()):lower() != K ) {
            Table:remove(K)
        }
    }
}
