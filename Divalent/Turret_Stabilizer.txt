@name Hovering With applyForce
@inputs Pod:wirelink 
@outputs 
@persist E:entity Force:vector Gyro:angle 
@trigger 
runOnTick(1)

if(first()){
    E = entity():isWeldedTo()
    EE = entity():isConstrainedTo()
}

Force=((-E:vel()*E:mass())*vec(1,1,1))+vec(0,0,9)
E:applyForce(Force)

P = E:angles():pitch()
Y = 0
R = E:angles():roll()

Gyro = angnorm(ang(0,0,0) - ang(P,Y,R))*E:mass()*5000

E:applyAngForce(Gyro + $Gyro*50)
E:applyAngForce(-E:angVel()*E:mass())
