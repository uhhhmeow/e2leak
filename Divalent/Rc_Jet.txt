@name Rc Jet
@inputs 
@outputs 
@persist Y R
@trigger 
@model models/hunter/plates/plate025x075.mdl
#runOnTick(1)

if (first()){
    entity():setColor(vec(), 0)
E=entity()
Size=2
function cReate(Index, Model:string, Position:vector, Angle:angle, Scale:vector)
    {
        holoCreate(Index)
        holoModel(Index,Model)
        holoPos(Index,Position)
        holoAng(Index,Angle)
        holoScale(Index,Scale)
        holoShadow(Index,1)
        holoParent(Index,E)
    }
cReate(1,"hq_sphere",E:toWorld(vec(0,20,0)/Size),E:toWorld(ang(0,0,-90)),vec(4.5,3,27)/Size) #Nose Cone
cReate(2,"hq_tube_thin",E:toWorld(vec(0,25,5)/Size),E:toWorld(ang(0,0,0)),vec(3,10,2)/Size)#Cockpit
cReate(3,"hq_dome",E:toWorld(vec(0,25,10)/Size),E:toWorld(ang(0,0,0)),vec(3,10,4)/Size)#WindSheild
cReate(4,"hq_cubinder",E:toWorld(vec(0,18,0)/Size),E:toWorld(ang(0,0,-90)),vec(3,3,15)/Size)#BodyCylinder
cReate(5,"hq_stube",E:toWorld(vec(25,-60,-20)/Size),E:toWorld(ang(100,0,-90)),vec(1.5,2,15)/Size)#AirIntakeRight
cReate(6,"hq_stube",E:toWorld(vec(-25,-60,-20)/Size),E:toWorld(ang(-100,0,-90)),vec(1.5,2,15)/Size)#AirIntakeLeft
cReate(7,"hq_rcube",E:toWorld(vec(0,-100,-5)/Size),E:toWorld(ang(0,0,0)),vec(9,17,2)/Size)#Big Body Thingy
cReate(8,"hq_stube",E:toWorld(vec(25,-205,-5.2)/Size),E:toWorld(ang(0,0,90)),vec(2.5,1.5,1.5)/Size)#Right Thruster
cReate(9,"hq_stube",E:toWorld(vec(-25,-205,-5.2)/Size),E:toWorld(ang(0,0,90)),vec(2.5,1.5,1.5)/Size)#Left Thruster
cReate(10,"hq_cylinder",E:toWorld(vec(14,0,-5)/Size),E:toWorld(ang(0,15,90)),vec(5,2,8)/Size)#Right Thruster Cover
cReate(11,"hq_cylinder",E:toWorld(vec(-14,0,-5)/Size),E:toWorld(ang(0,-15,90)),vec(5,2,8)/Size)#Left Thruster Cover
cReate(12,"cube",E:toWorld(vec(115,-30,-10)/Size),E:toWorld(ang(0,-20,0)),vec(20,1,0.2)/Size)#Right Wing Leading Edge
cReate(13,"cube",E:toWorld(vec(-115,-30,-10)/Size),E:toWorld(ang(0,20,0)),vec(20,1,0.2)/Size)#Left Wing Leading Edge
cReate(14,"cube",E:toWorld(vec(225,-90,-10)/Size),E:toWorld(ang(0,90,0)),vec(4,1,0.2)/Size)#Right Wing Leading Edge Side
cReate(15,"cube",E:toWorld(vec(-225,-90,-10)/Size),E:toWorld(ang(0,90,0)),vec(4,1,0.2)/Size)#Left Wing Leading Edge Side
cReate(16,"cube",E:toWorld(vec(215,-128,-10)/Size),E:toWorld(ang(0,-120,0)),vec(3.2,1,0.2)/Size)#Right Wing Trailing Edge Side
cReate(17,"cube",E:toWorld(vec(-215,-128,-10)/Size),E:toWorld(ang(0,120,0)),vec(3.2,1,0.2)/Size)#Left Wing Trailing Edge Side
cReate(18,"cube",E:toWorld(vec(120,-150,-10)/Size),E:toWorld(ang(0,5,0)),vec(15,1,0.2)/Size)#Right Wing Trailing Edge
cReate(19,"cube",E:toWorld(vec(-120,-150,-10)/Size),E:toWorld(ang(0,-5,0)),vec(15,1,0.2)/Size)#Left Wing Trailing Edge
cReate(20,"cube",E:toWorld(vec(80,-60,-10)/Size),E:toWorld(ang(0,-20,0)),vec(24,7,0.2)/Size)#Right Wing Body 1
cReate(21,"cube",E:toWorld(vec(-80,-60,-10)/Size),E:toWorld(ang(0,20,0)),vec(24,7,0.2)/Size)#Left Wing Body 1
cReate(22,"cube",E:toWorld(vec(80,-120,-10)/Size),E:toWorld(ang(0,5,0)),vec(22,5,0.2)/Size)#Right Wing Body 2
cReate(23,"cube",E:toWorld(vec(-80,-120,-10)/Size),E:toWorld(ang(0,-5,0)),vec(22,5,0.2)/Size)#Left Wing Body 2
cReate(24,"cube",E:toWorld(vec(89,-110,-10)/Size),E:toWorld(ang(0,0,0)),vec(22,2,0.2)/Size)#Right Wing Body 3
cReate(25,"cube",E:toWorld(vec(-89,-110,-10)/Size),E:toWorld(ang(0,0,0)),vec(22,2,0.2)/Size)#Left Wing Body 3
cReate(26,"cube",E:toWorld(vec(60,-195,-10)/Size),E:toWorld(ang(0,-50,0)),vec(9,4,0.2)/Size)#Right Tail Wing Body 1
cReate(27,"cube",E:toWorld(vec(-60,-195,-10)/Size),E:toWorld(ang(0,50,0)),vec(9,4,0.2)/Size)#Left Tail Wing Body 1
cReate(28,"hq_sphere",E:toWorld(vec(50,-210,-10)/Size),E:toWorld(ang(0,0,0)),vec(1.5,7,2)/Size)#Right Tail Connector
cReate(29,"hq_sphere",E:toWorld(vec(-50,-210,-10)/Size),E:toWorld(ang(0,0,0)),vec(1.5,7,2)/Size)#Left Tail Connector
cReate(30,"cube",E:toWorld(vec(98,-234,-10)/Size),E:toWorld(ang(0,0,0)),vec(2.5,2.2,0.2)/Size)#Right Tail Wing Body 2
cReate(31,"cube",E:toWorld(vec(-98,-234,-10)/Size),E:toWorld(ang(0,0,0)),vec(2.5,2.2,0.2)/Size)#Left Tail Wing Body 2
cReate(32,"cube",E:toWorld(vec(94,-244,-10)/Size),E:toWorld(ang(0,9,0)),vec(3,1,0.2)/Size)#Right Tail Wing Body 3
cReate(33,"cube",E:toWorld(vec(-94,-244,-10)/Size),E:toWorld(ang(0,-9,0)),vec(3,1,0.2)/Size)#Left Tail Wing Body 3
cReate(34,"cube",E:toWorld(vec(70,-150,15)/Size),E:toWorld(ang(35,0,20)),vec(0.2,2,7)/Size)#Right Tail Rudder Body 1
cReate(35,"cube",E:toWorld(vec(-70,-150,15)/Size),E:toWorld(ang(-35,0,20)),vec(0.2,2,7)/Size)#Left Tail Rudder Body 1
cReate(36,"cube",E:toWorld(vec(70,-190,15)/Size),E:toWorld(ang(35,0,-20)),vec(0.2,2,7)/Size)#Right Tail Rudder Body 2
cReate(37,"cube",E:toWorld(vec(-70,-190,15)/Size),E:toWorld(ang(-35,0,-20)),vec(0.2,2,7)/Size)#Left Tail Rudder Body 2
cReate(38,"cube",E:toWorld(vec(70,-170,16)/Size),E:toWorld(ang(35,0,0)),vec(0.2,3,7)/Size)#Right Tail Rudder Body 3
cReate(39,"cube",E:toWorld(vec(-70,-170,16)/Size),E:toWorld(ang(-35,0,0)),vec(0.2,3,7)/Size)#Left Tail Rudder Body 3
#cReate(40,"hq_cylinder",E:toWorld(vec(0,20,-30)/Size),E:toWorld(ang(0,0,20)),vec(0.5,0.5,2)/Size)#Front Landing Gear
#cReate(41,"hq_cylinder",E:toWorld(vec(5,25,-40)/Size),E:toWorld(ang(90,0,0)),vec(0.5,0.5,1)/Size)#Right Front Landing Gear Connector 1
#cReate(42,"hq_cylinder",E:toWorld(vec(-5,25,-40)/Size),E:toWorld(ang(90,0,0)),vec(0.5,0.5,1)/Size)#Left Front Landing Gear Connector 1
#cReate(43,"hq_cylinder",E:toWorld(vec(10,25,-46)/Size),E:toWorld(ang(0,0,0)),vec(0.2,0.2,1.5)/Size)#Right Front Landing Gear Connector 2
#cReate(44,"hq_cylinder",E:toWorld(vec(-10,25,-46)/Size),E:toWorld(ang(0,0,0)),vec(0.2,0.2,1.5)/Size)#Left Front Landing Gear Connector 2
#cReate(45,"hq_cylinder",E:toWorld(vec(0,25,-55)/Size),E:toWorld(ang(90,0,0)),vec(0.2,0.2,1.9)/Size)#Front Landing Gear Connector 3
#cReate(46,"models/XQM/airplanewheel1.mdl",E:toWorld(vec(0,25,-55)/Size),E:toWorld(ang(0,0,20)),vec(1,1,1)/Size)#Front Landing Wheel
#cReate(47,"hq_cylinder",E:toWorld(vec(30,-120,-30)/Size),E:toWorld(ang(-30,0,0)),vec(0.5,0.5,3)/Size)#Rear Right Landing Gear 1
#cReate(48,"hq_cylinder",E:toWorld(vec(-30,-120,-30)/Size),E:toWorld(ang(30,0,0)),vec(0.5,0.5,3)/Size)#Rear Left Landing Gear 1
#cReate(49,"hq_cylinder",E:toWorld(vec(42,-120,-48)/Size),E:toWorld(ang(-40,0,0)),vec(0.3,0.3,1.2)/Size)#Rear Right Landing Gear 2
#cReate(50,"hq_cylinder",E:toWorld(vec(-42,-120,-48)/Size),E:toWorld(ang(40,0,0)),vec(0.3,0.3,1.2)/Size)#Rear Left Landing Gear 2
#cReate(51,"models/XQM/airplanewheel1.mdl",E:toWorld(vec(50,-120,-55)/Size),E:toWorld(ang(0,0,0)),vec(1,1,1)/Size)#Right Rear Landing Gear Wheel
#cReate(52,"models/XQM/airplanewheel1.mdl",E:toWorld(vec(-50,-120,-55)/Size),E:toWorld(ang(0,0,0)),vec(1,1,1)/Size)#Left Rear Landing Gear WHeel
#cReate(53,"models/props_phx/amraam.mdl",E:toWorld(vec(220,-100,-30)/Size),E:toWorld(ang(0,90,0)),vec(1,1,1)/Size)#Rightile
#cReate(54,"models/props_phx/amraam.mdl",E:toWorld(vec(-220,-100,-30)/Size),E:toWorld(ang(0,90,0)),vec(1,1,1)/Size)#Leftile
#cReate(55,"cube",E:toWorld(vec(220,-100,-15)/Size),E:toWorld(ang(0,90,0)),vec(2,0.2,1)/Size)#Right Missile Connector
#cReate(56,"cube",E:toWorld(vec(-220,-100,-15)/Size),E:toWorld(ang(0,90,0)),vec(2,0.2,1)/Size)#Left Missile Connector       
cReate(57,"hq_torus",E:toWorld(vec(0,-160,7)/Size),E:toWorld(ang(0,0,0)),vec(3,3,3)/Size)#Body Work
cReate(58,"cube",E:toWorld(vec(0,-160,7)/Size),E:toWorld(ang(0,0,0)),vec(1.5,0.4,0.35)/Size)#Body Work 2
cReate(59,"cube",E:toWorld(vec(0,-170,7)/Size),E:toWorld(ang(0,90,0)),vec(1.5,0.4,0.35)/Size)#Body Work 3
cReate(60,"models/sprops/misc/alphanum/alphanum_star_3.mdl",E:toWorld(vec(-180,-88.5,-8.6)/Size),E:toWorld(ang(0,0,-90)),vec(2.5,0.5,2.5)/Size)
cReate(61,"models/sprops/misc/alphanum/alphanum_star_3.mdl",E:toWorld(vec(180,-88.5,-8.6)/Size),E:toWorld(ang(0,0,-90)),vec(2.5,0.5,2.5)/Size)
cReate(62,"cube",E:toWorld(vec(-180,-90,-9)/Size),E:toWorld(ang(0,0,90)),vec(5.4,0.1,0.8)/Size)
cReate(63,"cube",E:toWorld(vec(180,-90,-9)/Size),E:toWorld(ang(0,0,90)),vec(5.4,0.1,0.8)/Size)
cReate(64,"hq_cylinder",E:toWorld(vec(-180,-90,-9.2)/Size),E:toWorld(ang(0,0,0)),vec(2.8,2.8,0.2)/Size)
cReate(65,"hq_cylinder",E:toWorld(vec(180,-90,-9.2)/Size),E:toWorld(ang(0,0,0)),vec(2.8,2.8,0.2)/Size)
cReate(66,"cube",E:toWorld(vec(-180,-90,-9.2)/Size),E:toWorld(ang(0,0,90)),vec(5.8,0.1,1.2)/Size)
cReate(67,"cube",E:toWorld(vec(180,-90,-9.2)/Size),E:toWorld(ang(0,0,90)),vec(5.8,0.1,1.2)/Size)

    holoClipEnabled(57,1)
    holoClip(57,vec(0,0,0),vec(0,1,0),0)
    holoMaterial(58,"models/debug/debugwhite")
    holoMaterial(60,"models/debug/debugwhite")
    holoMaterial(61,"models/debug/debugwhite")
    holoColor(66,vec(13,44,136))
    holoColor(67,vec(13,44,136))
    holoColor(64,vec(13,44,136))
    holoColor(65,vec(13,44,136))
    holoColor(57,vec(13,44,136))
    
    for(I=1,39)
    {    
        holoColor(I,vec(0,120,31))
    }
    holoColor(4,vec(0,67,25))
    holoColor(1,vec(0,67,25))
    holoColor(5,vec(0,0,0))
    holoColor(6,vec(0,0,0))
    holoColor(8,vec(0,0,0))
    holoColor(9,vec(0,0,0))
    for(I=36,39)
    {
        holoColor(I,vec(0,67,25))   
    }        
    holoMaterial(3,"phoenix_storms/glass")
}

