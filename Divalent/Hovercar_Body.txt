@name Hovercar Body
@inputs 
@outputs 
@persist I M 
@trigger 
interval(100)

E = entity():isWeldedTo()

if (first() | dupefinished()) {M = 30}

if (I < M & holoCanCreate()) {
    I++
    
    holoCreate(I)
    holoMaterial(I,"phoenix_storms/fender_white")
    holoPos(I,E:toWorld(vec(0,0,0)))
    holoAng(I,E:toWorld(ang(0,0,0)))
    holoParent(I,E)
    
    holoModel(1,"models/holograms/hq_rcylinder_thick.mdl")
    holoScale(1,vec(16,5,3))
    holoColor(1,vec(0,250,255))
    
    holoClipEnabled(1,1,1)
    holoClip(1,vec(10,0,0),vec(1,0,0),0)
    
    ClipAng = holoEntity(1):angles()+ang(170,0,0)
    holoClipEnabled(1,2,1)
    holoClip(1,2,vec(0,23,0),ClipAng:forward(),0)
    
    ClipAng2 = holoEntity(1):angles()-ang(170,0,0)
    holoClipEnabled(1,3,1)
    holoClip(1,3,vec(0,-23,0),-ClipAng2:forward(),0)
    
    holoModel(2,"models/holograms/hq_rcylinder_thick.mdl")
    holoScale(2,vec(16,5,3))
    holoColor(2,vec(255,255,255))
    
    holoClipEnabled(2,1,1)
    holoClip(2,vec(10,0,0),vec(1,0,0),0)
    
    ClipAng3 = holoEntity(2):angles()+ang(170,0,0)
    holoClipEnabled(2,2,1)
    holoClip(2,2,vec(0,23,0),-ClipAng3:forward(),0)
    
    holoModel(3,"models/holograms/hq_rcylinder_thick.mdl")
    holoScale(3,vec(16,5,3))
    holoColor(3,vec(255,255,255))
    
    holoClipEnabled(3,1,1)
    holoClip(3,vec(10,0,0),vec(1,0,0),0)
    
    ClipAng4 = holoEntity(3):angles()-ang(170,0,0)
    holoClipEnabled(3,2,1)
    holoClip(3,2,vec(0,-23,0),ClipAng4:forward(),0)
    
    holoModel(4,"models/holograms/hq_rcylinder_thick.mdl")
    holoScale(4,vec(16,5,3))
    holoColor(4,vec(0,250,255))
    
    holoClipEnabled(4,1,1)
    holoClip(4,vec(10,0,0),vec(-1,0,0),0)
    
    holoClipEnabled(4,2,1)
    holoClip(4,2,vec(-52,0,0),vec(1,0,0),0)
    
    holoClipEnabled(4,3,1)
    holoClip(4,3,vec(0,0,-15.15),vec(0,0,-1),0)
    
    holoModel(5,"models/holograms/hq_rcylinder_thick.mdl")
    holoScale(5,vec(16,5,3))
    holoColor(5,vec(255,255,255))
    
    holoClipEnabled(5,1,1)
    holoClip(5,vec(10,0,0),vec(-1,0,0),0)
    
    holoClipEnabled(5,2,1)
    holoClip(5,2,vec(-52,0,0),vec(1,0,0),0)
    
    holoClipEnabled(5,3,1)
    holoClip(5,3,vec(0,20,0),vec(0,1,0),0)
    
    holoClipEnabled(5,4,1)
    holoClip(5,4,vec(0,0,-15.15),vec(0,0,1),0)
    
    holoModel(6,"models/holograms/hq_rcylinder_thick.mdl")
    holoScale(6,vec(16,5,3))
    holoColor(6,vec(255,255,255))
    
    holoClipEnabled(6,1,1)
    holoClip(6,vec(10,0,0),vec(-1,0,0),0)
    
    holoClipEnabled(6,2,1)
    holoClip(6,2,vec(-52,0,0),vec(1,0,0),0)
    
    holoClipEnabled(6,3,1)
    holoClip(6,3,vec(0,-20,0),vec(0,-1,0),0)
    
    holoClipEnabled(6,4,1)
    holoClip(6,4,vec(0,0,-15.15),vec(0,0,1),0)
    
    holoModel(7,"models/holograms/hq_rcylinder_thick.mdl")
    holoScale(7,vec(16,5,3))
    holoColor(7,vec(0,250,255))
    
    holoClipEnabled(7,1,1)
    holoClip(7,vec(-52,0,0),vec(-1,0,0),0)
    
    ClipAng5 = holoEntity(7):angles()+ang(170,0,0)
    holoClipEnabled(7,2,1)
    holoClip(7,2,vec(0,23,0),ClipAng5:forward(),0)
    
    ClipAng6 = holoEntity(7):angles()-ang(170,0,0)
    holoClipEnabled(7,3,1)
    holoClip(7,3,vec(0,-23,0),-ClipAng6:forward(),0)
    
    holoModel(8,"models/holograms/hq_rcylinder_thick.mdl")
    holoScale(8,vec(16,5,3))
    holoColor(8,vec(255,255,255))
    
    holoClipEnabled(8,1,1)
    holoClip(8,vec(-52,0,0),vec(-1,0,0),0)
    
    ClipAng7 = holoEntity(8):angles()+ang(170,0,0)
    holoClipEnabled(8,2,1)
    holoClip(8,2,vec(0,23,0),-ClipAng7:forward(),0)
    
    holoModel(9,"models/holograms/hq_rcylinder_thick.mdl")
    holoScale(9,vec(16,5,3))
    holoColor(9,vec(255,255,255))
    
    holoClipEnabled(9,1,1)
    holoClip(9,vec(-52,0,0),vec(-1,0,0),0)
    
    ClipAng8 = holoEntity(9):angles()-ang(170,0,0)
    holoClipEnabled(9,2,1)
    holoClip(9,2,vec(0,-23,0),ClipAng8:forward(),0)
    
    holoModel(10,"models/holograms/hq_rcylinder_thick.mdl")
    holoScale(10,vec(-16,-5,-3))
    holoColor(10,vec(71,71,71))
    
    holoClipEnabled(10,1,1)
    holoClip(10,vec(10,0,0),vec(1,0,0),0)
    
    ClipAng = holoEntity(10):angles()+ang(170,0,0)
    holoClipEnabled(10,2,1)
    holoClip(10,2,vec(0,23,0),ClipAng:forward(),0)
    
    ClipAng2 = holoEntity(10):angles()-ang(170,0,0)
    holoClipEnabled(10,3,1)
    holoClip(10,3,vec(0,-23,0),-ClipAng2:forward(),0)
    
    holoModel(11,"models/holograms/hq_rcylinder_thick.mdl")
    holoScale(11,vec(-16,-5,-3))
    holoColor(11,vec(71,71,71))
    
    holoClipEnabled(11,1,1)
    holoClip(11,vec(10,0,0),vec(1,0,0),0)
    
    ClipAng3 = holoEntity(11):angles()+ang(170,0,0)
    holoClipEnabled(11,2,1)
    holoClip(11,2,vec(0,23,0),-ClipAng3:forward(),0)
    
    holoModel(12,"models/holograms/hq_rcylinder_thick.mdl")
    holoScale(12,vec(-16,-5,-3))
    holoColor(12,vec(71,71,71))
    
    holoClipEnabled(12,1,1)
    holoClip(12,vec(10,0,0),vec(1,0,0),0)
    
    ClipAng4 = holoEntity(12):angles()-ang(170,0,0)
    holoClipEnabled(12,2,1)
    holoClip(12,2,vec(0,-23,0),ClipAng4:forward(),0)
    
    holoModel(13,"models/holograms/hq_rcylinder_thick.mdl")
    holoScale(13,vec(-16,-5,-3))
    holoColor(13,vec(71,71,71))
    
    holoClipEnabled(13,1,1)
    holoClip(13,vec(10,0,0),vec(-1,0,0),0)
    
    holoClipEnabled(13,2,1)
    holoClip(13,2,vec(-52,0,0),vec(1,0,0),0)
    
    holoClipEnabled(13,3,1)
    holoClip(13,3,vec(0,0,-15.15),vec(0,0,-1),0)
    
    holoModel(14,"models/holograms/hq_rcylinder_thick.mdl")
    holoScale(14,vec(-16,-5,-3))
    holoColor(14,vec(71,71,71))
    
    holoClipEnabled(14,1,1)
    holoClip(14,vec(10,0,0),vec(-1,0,0),0)
    
    holoClipEnabled(14,2,1)
    holoClip(14,2,vec(-52,0,0),vec(1,0,0),0)
    
    holoClipEnabled(14,3,1)
    holoClip(5,3,vec(0,20,0),vec(0,1,0),0)
    
    holoClipEnabled(14,4,1)
    holoClip(14,4,vec(0,0,-15.15),vec(0,0,1),0)
    
    holoModel(15,"models/holograms/hq_rcylinder_thick.mdl")
    holoScale(15,vec(-16,-5,-3))
    holoColor(15,vec(71,71,71))
    
    holoClipEnabled(15,1,1)
    holoClip(15,vec(10,0,0),vec(-1,0,0),0)
    
    holoClipEnabled(15,2,1)
    holoClip(15,2,vec(-52,0,0),vec(1,0,0),0)
    
    holoClipEnabled(15,3,1)
    holoClip(15,3,vec(0,-20,0),vec(0,-1,0),0)
    
    holoClipEnabled(15,4,1)
    holoClip(15,4,vec(0,0,-15.15),vec(0,0,1),0)
    
    holoModel(16,"models/holograms/hq_rcylinder_thick.mdl")
    holoScale(16,vec(-16,-5,-3))
    holoColor(16,vec(71,71,71))
    
    holoClipEnabled(16,1,1)
    holoClip(16,vec(-52,0,0),vec(-1,0,0),0)
    
    ClipAng5 = holoEntity(16):angles()+ang(170,0,0)
    holoClipEnabled(16,2,1)
    holoClip(16,2,vec(0,23,0),ClipAng5:forward(),0)
    
    ClipAng6 = holoEntity(16):angles()-ang(170,0,0)
    holoClipEnabled(16,3,1)
    holoClip(16,3,vec(0,-23,0),-ClipAng6:forward(),0)
    
    holoModel(17,"models/holograms/hq_rcylinder_thick.mdl")
    holoScale(17,vec(-16,-5,-3))
    holoColor(17,vec(71,71,71))
    
    holoClipEnabled(17,1,1)
    holoClip(17,vec(-52,0,0),vec(-1,0,0),0)
    
    ClipAng7 = holoEntity(17):angles()+ang(170,0,0)
    holoClipEnabled(17,2,1)
    holoClip(17,2,vec(0,23,0),-ClipAng7:forward(),0)
    
    holoModel(18,"models/holograms/hq_rcylinder_thick.mdl")
    holoScale(18,vec(-16,-5,-3))
    holoColor(18,vec(71,71,71))
    
    holoClipEnabled(18,1,1)
    holoClip(18,vec(-52,0,0),vec(-1,0,0),0)
    
    ClipAng8 = holoEntity(18):angles()-ang(170,0,0)
    holoClipEnabled(18,2,1)
    holoClip(18,2,vec(0,-23,0),ClipAng8:forward(),0)
    
    holoPos(19,E:toWorld(vec(60,30,-6)))
    holoAng(19,E:toWorld(ang(0,0,90)))
    holoModel(19,"models/holograms/hq_rcylinder_thick.mdl")
    holoScale(19,vec(5,4,1.5))
    holoColor(19,vec(0,250,255))
    
    holoPos(20,E:toWorld(vec(60,-30,-6)))
    holoAng(20,E:toWorld(ang(0,0,90)))
    holoModel(20,"models/holograms/hq_rcylinder_thick.mdl")
    holoScale(20,vec(5,4,1.5))
    holoColor(20,vec(0,250,255))
    
    holoPos(21,E:toWorld(vec(-70,30,-6)))
    holoAng(21,E:toWorld(ang(0,0,90)))
    holoModel(21,"models/holograms/hq_rcylinder_thick.mdl")
    holoScale(21,vec(4,4,1.5))
    holoColor(21,vec(0,250,255))
    
    holoPos(22,E:toWorld(vec(-70,-30,-6)))
    holoAng(22,E:toWorld(ang(0,0,90)))
    holoModel(22,"models/holograms/hq_rcylinder_thick.mdl")
    holoScale(22,vec(4,4,1.5))
    holoColor(22,vec(0,250,255))
    
    holoPos(23,E:toWorld(vec(60,30,-6)))
    holoAng(23,E:toWorld(ang(0,0,90)))
    holoModel(23,"models/holograms/hq_rcylinder_thick.mdl")
    holoScale(23,vec(-5,-4,-1.5))
    holoColor(23,vec(71,71,71))
    
    holoPos(24,E:toWorld(vec(60,-30,-6)))
    holoAng(24,E:toWorld(ang(0,0,90)))
    holoModel(24,"models/holograms/hq_rcylinder_thick.mdl")
    holoScale(24,vec(-5,-4,-1.5))
    holoColor(24,vec(71,71,71))
    
    holoPos(25,E:toWorld(vec(-70,30,-6)))
    holoAng(25,E:toWorld(ang(0,0,90)))
    holoModel(25,"models/holograms/hq_rcylinder_thick.mdl")
    holoScale(25,vec(-4,-4,-1.5))
    holoColor(25,vec(71,71,71))
    
    holoPos(26,E:toWorld(vec(-70,-30,-6)))
    holoAng(26,E:toWorld(ang(0,0,90)))
    holoModel(26,"models/holograms/hq_rcylinder_thick.mdl")
    holoScale(26,vec(-4,-4,-1.5))
    holoColor(26,vec(71,71,71))
    
    for (C = 19,26) {
        holoClipEnabled(C,1,1)
        holoClip(C,vec(0,-6,0),vec(0,1,0),0)
    }
    
    holoPos(27,E:toWorld(vec(-20,0,10)))
    holoAng(27,E:toWorld(ang(0,0,90)))
    holoModel(27,"models/holograms/hq_rcylinder_thick.mdl")
    holoScale(27,vec(8,4,3.5))
    holoAlpha(27,200)
    holoColor(27,vec(71,71,71))
    
    holoClipEnabled(27,1,1)
    holoClip(27,vec(0,3,0),vec(0,1,0),0)
    
    holoPos(28,E:toWorld(vec(-20,0,10)))
    holoAng(28,E:toWorld(ang(0,0,90)))
    holoModel(28,"models/holograms/hq_rcylinder_thick.mdl")
    holoScale(28,vec(-8,-4,-3.5))
    holoAlpha(28,180)
    holoColor(28,vec(71,71,71))
    
    holoClipEnabled(28,1,1)
    holoClip(28,vec(0,3,0),vec(0,1,0),0)
    
    holoPos(29,E:toWorld(vec(-20,0,10)))
    holoAng(29,E:toWorld(ang(0,0,90)))
    holoModel(29,"models/holograms/hq_rcylinder_thick.mdl")
    holoScale(29,vec(-8,-4,-3.5))
    holoColor(29,vec(71,71,71))
    
    holoClipEnabled(29,1,1)
    holoClip(29,vec(0,3,0),vec(0,-1,0),0)
    
    holoPos(30,E:toWorld(vec(10,0,0)))
    holoAng(30,E:toWorld(ang(0,90,110)))
    holoModel(30,"models/holograms/hq_rcylinder_thick.mdl")
    holoScale(30,vec(5,4,1))
    holoColor(30,vec(71,71,71))
    
    holoClipEnabled(30,1,1)
    holoClip(30,vec(0,17,0),vec(0,-1,0),0)
    
    holoClipEnabled(30,2,1)
    holoClip(30,2,vec(-22,0,0),vec(1,0,0),0)
    
    holoClipEnabled(30,3,1)
    holoClip(30,3,vec(22,0,0),vec(-1,0,0),0)
    
    holoClipEnabled(30,4,1)
    holoClip(30,4,vec(0,-1,0),vec(0,1,0),0)
}
