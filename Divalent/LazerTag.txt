@name LazerTag
@inputs 
@outputs 
@persist 
@trigger 
interval(100)

O=owner()

if(first()){
    holoCreate(1)
    holoParentAttachment(1,O,"mouth")
    holoPos(1,O:attachmentPos("mouth"))
    holoAng(1,O:attachmentAng("mouth")+ang(0,90,0))
    holoPos(1,entity():pos()+vec(0,0,25))
    holoParent(1,entity())
}
