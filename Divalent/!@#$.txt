@name !@#$
@inputs 
@outputs 
@persist 
@trigger 

Prop = entity():isWeldedTo()

if (first() & fileCanWrite()) {
    local T = table(
        "mdl" = Prop:model(),
        "pos" = Prop:pos(),
        "ang" = Prop:angles(),
        "color" = Prop:getColor4()
    )

    local Encode = vonEncode(T)
    
    fileWrite(">e2shared/Holograms/6.txt",Encode)
}
