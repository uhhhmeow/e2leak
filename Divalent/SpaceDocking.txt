@name SpaceDocking
@inputs 
@outputs BA:array 
@persist R:ranger [H Ship]:entity 
@trigger 
interval(100)

E = entity():isWeldedTo()

if (first()) {
    H = holoCreate(1)
    holoAlpha(1,100)
    holoScale(1,vec(4,2,1))
    HS=H:boxSize()/12
    holoPos(1,entity():isWeldedTo():toWorld(vec(0,0,-40)))
}
if(dupefinished()){
    reset()
}
#models/hunter/tubes/tube4x2x2.mdl
R=rangerOffsetHull(2.5,H:pos(),-E:up(),HS)
if(R:entity():model()=="models/hunter/plates/plate05x1.mdl"){
    BA:pushEntity(R:entity())
    Ship=BA[1,entity]
    Ship:setPos(holoEntity(1):pos())
    Ship:setAng(holoEntity(1):angles()+ang(0,90,0))
    Ship:propFreeze(1)
}
if(!Ship){
    BA:clear()
}
