@name BunkerBase
@inputs Door:entity 
@outputs 
@persist A P State [CornerPos1 CornerPos2]:vector 
@trigger 
interval(100)

if (first() | dupefinished()) {
    entity():setColor(vec4(0))
    Scale = vec(5,5,5) * 20
    
    holoCreate(5)
    holoScaleUnits(5,Scale)
    holoPos(5,entity():toWorld(vec(-20,0,-23)))
    holoAng(5,entity():angles())
    holoColor(5,vec(255,255,255),0)
    holoParent(5,entity())
    
    CornerPos1 = Scale / 2
    CornerPos2 = -CornerPos1
    
    CornerPos1 = holoEntity(5):toWorld(CornerPos1)
    CornerPos2 = holoEntity(5):toWorld(CornerPos2)
    
    CornerScale = vec() + 1
    
    holoCreate(6)
    holoPos(6,CornerPos1)
    holoScaleUnits(6,CornerScale)
    holoParent(6,entity())
    
    holoCreate(7)
    holoPos(7,CornerPos2)
    holoScaleUnits(7,CornerScale)
    holoParent(7,entity())
    
    holoCreate(1)
    holoModel(1,"models/props_c17/oildrum001.mdl")
    holoPos(1,Door:toWorld(vec(-24.4,-48,2)))
    holoAng(1,Door:toWorld(ang(90,0,0)))
    holoScale(1,vec(0.1,0.1,1.08))
    holoParent(1,Door)
    
    holoCreate(2)
    holoModel(2,"models/props_c17/oildrum001.mdl")
    holoPos(2,Door:toWorld(vec(-24.4,48,2)))
    holoAng(2,Door:toWorld(ang(90,0,0)))
    holoScale(2,vec(0.1,0.1,1.08))
    holoParent(2,Door)
    
    holoCreate(3)
    holoMaterial(3,"models/props_canal/canal_bridge_railing_01c")
    holoPos(3,Door:toWorld(vec(0,-23.725000381469,1.85)))
    holoAng(3,Door:toWorld(ang(90,0,0)))
    holoScale(3,vec(0.1,3.96,3.96))
    holoParent(3,1)
    
    holoCreate(4)
    holoMaterial(4,"models/props_canal/canal_bridge_railing_01c")
    holoPos(4,Door:toWorld(vec(0,23.725000381469,1.85)))
    holoAng(4,Door:toWorld(ang(90,0,0)))
    holoScale(4,vec(0.1,3.96,3.96))
    holoParent(4,2)
}

findInBox(CornerPos1,CornerPos2)
findIncludeClass("player")
User = findClosest(holoEntity(5):pos())

if (User:isPlayer()) {State = 1} else {State = 0}

if (State == 1) {
    if (A < 200) {
        A += 4
        holoAng(1,holoEntity(1):toWorld(ang(0,2,0)))
        holoAng(2,holoEntity(2):toWorld(ang(0,-2,0)))
} elseif (A == 200) {User:plySetPos(vec(-4650.562,-2111.437,-139.187))}
} else {
    if (A > 0) {
        A -= 4
        holoAng(1,holoEntity(1):toWorld(ang(0,-2,0)))
        holoAng(2,holoEntity(2):toWorld(ang(0,2,0)))
    }
}
