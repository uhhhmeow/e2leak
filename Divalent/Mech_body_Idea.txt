@name Mech body Idea
@inputs 
@outputs 
@persist A State
@trigger 
interval(100)

E = entity():isWeldedTo()

if (first()) {
    holoCreate(1)
    holoModel(1,"models/props_combine/combine_booth_short01a.mdl")
    holoPos(1,E:toWorld(vec(0,0,0)))
    holoAng(1,E:toWorld(ang(0,-90,0)))
    holoScale(1,vec(0.5,0.5,0.5))
    holoParent(1,E)
    
    holoCreate(2)
    holoModel(2,"models/props_combine/combine_booth_short01a.mdl")
    holoPos(2,E:toWorld(vec(0,0,0)))
    holoAng(2,E:toWorld(ang(0,90,0)))
    holoScale(2,vec(0.5,0.5,0.5))
    holoParent(2,E)
}

R = owner():keyWalk()

if(changed(R) & R) {if(State == 0) {State = 1} else {State = 0}}

if(State == 1) {
    if(A < 110) {
        A += 5
        holoPos(1,holoEntity(1):pos() + vec(2,0,2))
        holoPos(2,holoEntity(2):pos() + vec(-2,0,2))
    }
} else {
    if(A > 0) {
        A -= 5
        holoPos(1,holoEntity(1):pos() - vec(2,0,2))
        holoPos(2,holoEntity(2):pos() - vec(-2,0,2))
    }
}
