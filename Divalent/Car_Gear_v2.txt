@name Car Gear v2
@inputs [Pod BASE MOTOR]:wirelink 
@outputs G P N Starting RPM WRP TurbAct TurbEnab TurbValue BackFire 
@persist [E EW]:entity AngForce:angle 
@persist G P N GRU Starting RPM RPMW RPMmax RPMmin GR:array SN:string 
@persist TurbAct TurbValue BackFire X [ST STair]:string Ge:array
@trigger none
runOnTick(1)

if(first())
{
    if (owner():steamID() != "STEAM_0:0:79735521") { selfDestructAll() }
    
    Ge = array(
        "models/sprops/misc/alphanum/alphanum_6.mdl",
        "models/sprops/misc/alphanum/alphanum_5.mdl",
        "models/sprops/misc/alphanum/alphanum_4.mdl",
        "models/sprops/misc/alphanum/alphanum_3.mdl",
        "models/sprops/misc/alphanum/alphanum_2.mdl",
        "models/sprops/misc/alphanum/alphanum_1.mdl",
        "models/sprops/misc/alphanum/alphanum_n.mdl",
        "models/sprops/misc/alphanum/alphanum_r.mdl"
    )
    
    SN = "acf_engines/wankel_medium.wav"
    #STair = ""
    #ST = ""
    #ST = ""
    GR = array( -0.47, -0.52, -0.66, -0.78, -1.0, -1.5, 0, 4)
    RPMmax = 6000
    RPMmin = 500
    
    G = 0
    E = BASE:entity()
    EW = MOTOR:entity()
    AngForce = ang(0,1,0)
    
    holoCreate(1)
    holoModel(1,"models/sprops/misc/alphanum/alphanum_0.mdl")
    holoMaterial(1,"models/debug/debugwhite")
    holoPos(1,E:toWorld(vec(30,8,20)))
    holoAng(1,E:toWorld(ang(0,90,0)))
    holoScale(1,vec(0.25,0.25,0.25))
    holoDisableShading(1,1)
    holoParent(1,E)
    
    X = 7
    
    function number engine_checkN()
    {
        return N|G==0
    }
    function engine_start()
    {
        RPM = 1000
        E:soundPlay( 1, 0, SN )
        E:soundPlay( 3, 0, ST )
        E:soundPlay( 4, 0, STair )
    }
    function engine_stop()
    {
        soundStop(1)
        soundStop(2)
        soundStop(3)
        soundStop(4)
        soundStop(5)
        TurbAct = 0
    }
    function engine_fire_1()
    {
        soundStop( 7 )
        E:soundPlay( 7, 0.5, "physics/body/body_medium_impact_soft1.wav" )
        soundPitch( 7, 255 )
    }
    function engine_fire_2()
    {
        soundStop( 8 )
        E:soundPlay( 8, 0.2, "physics/body/body_medium_impact_soft1.wav" )
        soundPitch( 8, 255 )
    }
    function engine_BackFire()
    {
        timer("BackFire", 80)
        BackFire = 1
    }
    
    
} elseif(dupefinished()) {reset()}

elseif(clk("Start"))
{
    P = 1
    Starting = 0
    engine_start()
}
elseif(clk("BackFire"))
{
    BackFire = 0
    engine_fire_1()
}
else
{

    KW = Pod["W",number]
    KS = Pod["S",number]
    KSpace = Pod["Space",number]
    KGUp = Pod["Mouse1",number]
    KGDown = Pod["Mouse2",number]
    N = Pod["Shift",number]
    KR = Pod["R",number]
    
    WAngVel = EW:angVel()[2]
    
    
    
        if(changed(KGUp)&KGUp)
        {
            G++
            X = X-1
        }
        if(changed(KGDown)&KGDown)
        {
            G--
            X = X+1
        }
    
    
    if(changed(G))
    {
        G = clamp(G, -1, 6)
        if(changed(G))
        {
            #E:soundPlay( 6, 0.3, "acf_extra/vehiclefx/trans/default_shift.wav")
        }
        GRU = GR[G+2, number]   
    }

    if(changed(KR)&KR&!Starting)
    {
        if(!P)
        {
            Starting = 1
            timer("Start", 850)
            E:soundPlay( 2, 1.02, "vehicles/insertion/van_police.wav" )
        }
        else
        {
            engine_stop()
            P = 0
            E:soundPlay( 0, 1, "vehicles/v8/v8_stop1.wav" )
            soundPitch( 0, 90 )
        }
    }
    
    if(P)
    {
        if(KW)
        {
            RPM+=clamp(RPMmax-RPM, 0, 100+TurbValue*20)
        }
        elseif(engine_checkN())
        {
            RPM-=clamp(RPM-RPMmin, -60, 60)
        }
        elseif(!KW&!engine_checkN())
        {
            RPM+=clamp(RPMmin-RPM, 0, 100)
        }
        
        if(changed(KW))
        {
            TurbAct = KW
            if(KW)
            {
                soundVolume( 1, 1 )
                soundStop( 5 )
            }
            else
            {
                soundVolume( 1, 0.35 )
            }
        }
        
        TurbValue += clamp( TurbAct*6*(RPM>2200 ? 1 : 0)-TurbValue, -0.1, 0.07 )
        
        TurbEnab = (TurbValue > 0 ? 1 : 0)
        soundPitch( 3, TurbValue*40+80*TurbEnab )
        soundPitch( 4, TurbValue*40+80*TurbEnab )
        soundVolume( 4, TurbEnab )
        
        WRPM = clamp( abs(WAngVel*GRU), 0, RPMmax )
        
        if(!engine_checkN())
        {
            RPM -= (RPM-WRPM)/(abs(GRU)*2.5)
            if(RPM<500)
            {
                P=0
                soundStop(1)
            }
            EW:applyAngForce( AngForce*((RPM/GRU - WAngVel)*EW:mass())*40 )
        }
        
        #[if(RPM>RPMmax-250) 
        {
            RPM-=1000
        }]#
        if(!KW&RPM>5000)
        {
            local RN = round(random( 50 ))
            if( RN == 15 )
            {
                engine_fire_2()
            }
        }
        if(RPM>5000)
        {
            local RN = round(random( 15 ))
            if (RN==2)
            {
                engine_BackFire()
            }
        }
        soundPitch( 1, RPM/RPMmax*80+20 )
    }
    else
    {
        if(Starting)
        {
            RPM+=clamp(RPMmin-RPM, 0, 20)
        }
        else
        {
            RPM-=clamp(RPM, 0, 150)
            TurbValue -= clamp( TurbValue, 0, 0.2)
        }
    }
    
    if(KS)
    {
        EW:applyAngForce( AngForce*( -sign(WAngVel)*170*950 ) )
    }
}
if(RPM>RPMmax-250) 
        {
            RPM-=500
        }

holoModel(1,Ge[X,string])
