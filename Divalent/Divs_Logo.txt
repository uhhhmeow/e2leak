@name Divs Logo
@inputs S:wirelink 
@outputs 
@persist 
@trigger 
interval(100)

if (first()) {    
    S:egpClear()
    
    PosX = 256
    PosY = 256
    
    S:egpBox(1,vec2(PosX,PosY),vec2(65,45))
    
    S:egpBox(2,vec2(PosX,PosY),vec2(60,40))
    S:egpColor(2,vec(0,0,0))
    
    S:egpBox(3,vec2(PosX,PosY),vec2(50,30))
    
    S:egpText(4,"Div",vec2(PosX,PosY))
    S:egpFont(4,"Times New Roman",30)
    S:egpColor(4,vec(0,0,0))
    S:egpAlign(4,1,1)
}
