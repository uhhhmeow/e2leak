@name Door example
@inputs EGP:wirelink 
@outputs 
@persist P:array Target:entity Z 
@trigger 
interval(300)
runOnChat(1)

if (first() | dupefinished()) {EGP:egpClear()}

S = owner():lastSaid():explode(" ")

if (S[1,string] == "!add" & chatClk(owner())) {
    hideChat(1)
    TP = findPlayerByName(S[2,string])
    
    if (TP:isPlayer()) {
        Target = TP
        P:pushEntity(TP)
        EGP:egpText(0,TP:name(),vec2(256,256))
    }
   
}

if (S[1,string] == "!remove"& chatClk(owner())) {
    hideChat(1)
    TP = findPlayerByName(S[2,string])
    
    if (TP:isPlayer()) {
        while (Z < 20) {
            if (TP == P[Z,entity]) {P:remove(Z)}
            Z++
        }
    }
    Z = 0
}

if (S[1,string] == "!list" & chatClk(owner())) {hideChat(1) print(P)}
