@name [TOX TEC] Swingarms
@inputs [Left_Wheel Right_Wheel Base]:wirelink 
interval(90)

Material = "WTP/metal_2"
Color = vec(70)

if (changed(Base) & Base) {
    B=Base:entity()
}

if (first()) {
    holoCreate(10)
    holoParent(10,B)
    holoScale(10,vec(0.2))
    holoColor(10,vec4(0))
    
    holoCreate(11)
    holoParent(11,B)
    holoScale(11,vec(0.2))
    holoColor(11,vec4(0))
    
    holoCreate(20)
    holoPos(20,holoEntity(10):toWorld(vec(0,0,0)))  
    holoAng(20,holoEntity(10):toWorld(ang(90,0,0)))  
    holoModel(20,"models/gibs/scanner_gib01.mdl")
    holoParent(20,holoEntity(10))
    holoMaterial(20,Material)
    holoColor(20,Color)
    
    holoCreate(21)
    holoPos(21,holoEntity(11):toWorld(vec(0,0,0)))  
    holoAng(21,holoEntity(11):toWorld(ang(90,0,0)))  
    holoModel(21,"models/gibs/scanner_gib01.mdl")
    holoParent(21,holoEntity(11))
    holoMaterial(21,Material)
    holoColor(21,Color)
}

if (changed(Left_Wheel) & Left_Wheel) { 
    W1 = Left_Wheel:entity()
    
    W1_PosX = B:toLocal( W1:pos() ):x()
    W1_PosY = B:toLocal( W1:pos() ):y()
    
    holoCreate(1)
    holoPos(1,B:toWorld(vec(W1_PosX-10,W1_PosY-33,0)))  
    holoParent(1,B)
    holoScale(1,vec(0.2))
    holoAng(1,B:toWorld(ang(0)))
    holoColor(1,vec4(0))
    
    holoCreate(2)
    holoPos(2,W1:toWorld(vec(0,0,-10)))  
    holoParent(2,W1)
    holoScale(2,vec(0.2))
    holoAng(2,B:toWorld(ang(0)))
    holoColor(2,vec4(0))
}

if (changed(Right_Wheel) & Right_Wheel) { 
    W2 = Right_Wheel:entity()
    
    W2_PosX = B:toLocal(W2:pos()):x()
    W2_PosY = B:toLocal(W2:pos()):y()
    
    holoCreate(3)
    holoPos(3,B:toWorld(vec(W2_PosX-10,W2_PosY+33,0)))  
    holoParent(3,B)
    holoScale(3,vec(0.2))
    holoAng(3,B:toWorld(ang(0)))
    holoColor(3,vec4(0))
    
    holoCreate(4)
    holoPos(4,W2:toWorld(vec(0,0,-10)))  
    holoParent(4,W2)
    holoScale(4,vec(0.2))
    holoAng(4,B:toWorld(ang(0)))
    holoColor(4,vec4(0))
}

Ele1 = holoEntity(1):elevation(holoEntity(2):pos())
Bear1 = holoEntity(1):bearing(holoEntity(2):pos())
Pos1 = holoEntity(1):toLocal(holoEntity(2):pos())/2
Len1 = holoEntity(1):toLocal(holoEntity(2):pos()):length()
Len2 = holoEntity(3):toLocal(holoEntity(4):pos()):length()

holoAng(10,holoEntity(1):toWorld(ang(90-Ele1,-Bear1,0)))
holoPos(10,holoEntity(1):toWorld(Pos1))

Ele2 = holoEntity(3):elevation(holoEntity(4):pos())
Bear2 = holoEntity(3):bearing(holoEntity(4):pos())
Pos2 = holoEntity(3):toLocal(holoEntity(4):pos())/2

holoAng(11,holoEntity(3):toWorld(ang(90-Ele2,-Bear2,0)))
holoPos(11,holoEntity(3):toWorld(Pos2))
holoScaleUnits(20,vec(Len1,20,10))
holoScaleUnits(21,vec(Len2,20,10))
