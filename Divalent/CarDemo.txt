@name CarDemo
@inputs Pod:wirelink 
@outputs 
@persist 
@trigger 
runOnTick(1)

E = entity():isWeldedTo()

if (first()) {
 
}

local Seat = Pod["Entity",entity]
local Driver = Seat:driver()
local W = Driver:keyForward()
local A = Driver:keyLeft()
local S = Driver:keyBack()
local D = Driver:keyRight()
local R = Driver:keyReload()
local Space = Driver:keyJump()
local Shift = Driver:keySprint()
local Alt = Driver:keyWalk()
local Active = Driver:isPlayer()


if (W) {
    E:applyForce(E:right()*E:mass()*50)
} elseif (S) {
    E:applyForce(-E:right()*E:mass()*50)
}

if (A) {
    E:applyAngForce(ang(0,E:mass()*3000,0))
} elseif (D) {
    E:applyAngForce(ang(0,E:mass()*-3000,0))
}

if (Space) {
    E:propFreeze(1)
} else {E:propFreeze(0)}

if (Shift & W) {
    E:applyForce(E:right()*E:mass()*90)
}

if (Shift & S) {
    E:applyForce(-E:right()*E:mass()*90)
}
