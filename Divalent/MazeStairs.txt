@name MazeStairs
@inputs Prop:entity Pos:entity 
@outputs 
@persist State [CornerPos1 CornerPos2]:vector 
@trigger 
interval(100)

if (first()) {
    Scale = vec(2,2,4) * 12
    
    Door = holoCreate(1)
    
    holoScaleUnits(1,Scale)
    holoPos(1,Pos:toWorld(vec(0,0,40)))
    holoAng(1,Pos:angles())
    holoColor(1,vec(255,255,255),255)
    holoParent(1,entity())
    
    CornerPos1 = Scale / 2
    CornerPos2 = -CornerPos1
    
    CornerPos1 = Door:toWorld(CornerPos1)
    CornerPos2 = Door:toWorld(CornerPos2)
    
    CornerScale = vec() + 1
    
    holoCreate(2)
    holoPos(2,CornerPos1)
    holoScaleUnits(2,CornerScale)
    holoParent(2, Door)
    
    holoCreate(3)
    holoPos(3,CornerPos2)
    holoScaleUnits(3,CornerScale)
    holoParent(3, Door)
    
    for (I = 1,3) {holoColor(I,vec(),0)}
}

findInBox(CornerPos1,CornerPos2)
findIncludeClass("player")
User = findClosest(holoEntity(1):pos())

R = User

if(changed(R) & R) {if(State == 0) {State = 1} else {State = 0}}

if(changed(State == 1) & State == 1) {
    Prop:setPos(Prop:toWorld(vec(48,0,0)))
    timer("wait",5000)
}

if (clk("wait")) {
    Prop:setPos(Prop:toWorld(vec(-48,0,0)))
}
