@name RC Engine
@inputs W A S D [LF_W RF_W LFM_W RFM_W LBM_W RBM_W LB_W RB_W]:wirelink SoundProp:entity
@outputs 
@persist [LFW RFW LBW RBW]:entity
@trigger 
interval(16)

Torque = 10
TTorque = 40

LFW = LF_W:entity()
RFW = RF_W:entity()
LBW = LB_W:entity()
RBW = RB_W:entity()

LFMW = LFM_W:entity()
RFMW = RFM_W:entity()
LBMW = LBM_W:entity()
RBMW = RBM_W:entity()

if (W) {
    LFW:applyTorque(vec(0,Torque,0)*LFW:mass())
    RFW:applyTorque(vec(0,-Torque,0)*RFW:mass())
    LBW:applyTorque(vec(0,Torque,0)*LBW:mass())
    RBW:applyTorque(vec(0,-Torque,0)*RBW:mass())
    LFMW:applyTorque(vec(0,Torque,0)*LFW:mass())
    RFMW:applyTorque(vec(0,-Torque,0)*RFW:mass())
    LBMW:applyTorque(vec(0,Torque,0)*LBW:mass())
    RBMW:applyTorque(vec(0,-Torque,0)*RBW:mass())
} else {
    LFW:applyTorque(vec(0,0,0))
    RFW:applyTorque(vec(0,0,0))
    LBW:applyTorque(vec(0,0,0))
    RBW:applyTorque(vec(0,0,0))
    LFMW:applyTorque(vec(0,0,0))
    RFMW:applyTorque(vec(0,0,0))
    LBMW:applyTorque(vec(0,0,0))
    RBMW:applyTorque(vec(0,0,0))
}

if (S) {
    LFW:applyTorque(vec(0,-Torque,0)*LFW:mass())
    RFW:applyTorque(vec(0,Torque,0)*RFW:mass())
    LBW:applyTorque(vec(0,-Torque,0)*LBW:mass())
    RBW:applyTorque(vec(0,Torque,0)*RBW:mass())
    LFMW:applyTorque(vec(0,-Torque,0)*LFW:mass())
    RFMW:applyTorque(vec(0,Torque,0)*RFW:mass())
    LBMW:applyTorque(vec(0,-Torque,0)*LBW:mass())
    RBMW:applyTorque(vec(0,Torque,0)*RBW:mass())
} else {
    LFW:applyTorque(vec(0,0,0))
    RFW:applyTorque(vec(0,0,0))
    LBW:applyTorque(vec(0,0,0))
    RBW:applyTorque(vec(0,0,0))
    LFMW:applyTorque(vec(0,0,0))
    RFMW:applyTorque(vec(0,0,0))
    LBMW:applyTorque(vec(0,0,0))
    RBMW:applyTorque(vec(0,0,0))
}

if (D) {
    LFW:applyTorque(vec(0,TTorque*2,0)*LFW:mass())
    RFW:applyTorque(vec(0,TTorque*2,0)*RFW:mass())
    LBW:applyTorque(vec(0,TTorque*2,0)*LBW:mass())
    RBW:applyTorque(vec(0,TTorque*2,0)*RBW:mass())
    LFMW:applyTorque(vec(0,TTorque*2,0)*LFW:mass())
    RFMW:applyTorque(vec(0,TTorque*2,0)*RFW:mass())
    LBMW:applyTorque(vec(0,TTorque*2,0)*LBW:mass())
    RBMW:applyTorque(vec(0,TTorque*2,0)*RBW:mass())
} else {
    LFW:applyTorque(vec(0,0,0))
    RFW:applyTorque(vec(0,0,0))
    LBW:applyTorque(vec(0,0,0))
    RBW:applyTorque(vec(0,0,0))
    LFMW:applyTorque(vec(0,0,0))
    RFMW:applyTorque(vec(0,0,0))
    LBMW:applyTorque(vec(0,0,0))
    RBMW:applyTorque(vec(0,0,0))
}

if (A) {
    LFW:applyTorque(vec(0,-TTorque*2,0)*LFW:mass())
    RFW:applyTorque(vec(0,-TTorque*2,0)*RFW:mass())
    LBW:applyTorque(vec(0,-TTorque*2,0)*LBW:mass())
    RBW:applyTorque(vec(0,-TTorque*2,0)*RBW:mass())
    LFMW:applyTorque(vec(0,-TTorque*2,0)*LFW:mass())
    RFMW:applyTorque(vec(0,-TTorque*2,0)*RFW:mass())
    LBMW:applyTorque(vec(0,-TTorque*2,0)*LBW:mass())
    RBMW:applyTorque(vec(0,-TTorque*2,0)*RBW:mass())
} else {
    LFW:applyTorque(vec(0,0,0))
    RFW:applyTorque(vec(0,0,0))
    LBW:applyTorque(vec(0,0,0))
    RBW:applyTorque(vec(0,0,0))
    LFMW:applyTorque(vec(0,0,0))
    RFMW:applyTorque(vec(0,0,0))
    LBMW:applyTorque(vec(0,0,0))
    RBMW:applyTorque(vec(0,0,0))
}

if(first()){
    #SoundProp:soundPlay(1,0,"acf_extra/vehiclefx/engines/flat4/flat4.wav")
}
soundPitch(1,10+SoundProp:vel():length()/10)
