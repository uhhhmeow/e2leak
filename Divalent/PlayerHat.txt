@name PlayerHat
@inputs 
@outputs Player:entity 
@persist 
@trigger 
runOnTick(1)

if (first()) {
findByClass("Player")
Player = findClosest(entity():pos())
}
Player:teleport(owner():pos() + vec(0,0,80))

if (owner():keyDuck()){
Player:teleport(owner():pos() + vec(0,0,50))
}
