@name furtureistic engine
@inputs RPM 
interval(20)

if(first()){
    holoCreate(1)
    holoScale(1,vec(0.5,0.5,0.5))
    holoAng(1,entity():toWorld(ang(90,-90,180)))
    holoModel(1,"hq_torus")
    holoParent(1,entity())
    
    holoCreate(2)
    holoScale(2,vec(0.5,0.5,0.5))
    holoPos(2,entity():toWorld(vec(0,0,0)))
    holoAng(2,entity():toWorld(ang(90,-90,180)))
    holoModel(2,"hq_torus")
    holoParent(2,entity())
    
    holoCreate(3)
    holoScale(3,vec(0.5,0.5,0.5))
    holoPos(3,entity():toWorld(vec(0,0,0)))
    holoAng(3,entity():toWorld(ang(90,-90,180)))
    holoModel(3,"hq_torus")
    holoParent(3,entity())
    
    holoCreate(4)
    holoScale(4,vec(0.5,0.5,0.5))
    holoPos(4,entity():toWorld(vec(0,0,0)))
    holoAng(4,entity():toWorld(ang(90,-90,180)))
    holoModel(4,"hq_torus")
    holoParent(4,entity())
    
    holoCreate(5)
    holoScale(5,vec(0.5,0.5,0.5))
    holoPos(5,entity():toWorld(vec(0,0,0)))
    holoAng(5,entity():toWorld(ang(90,-90,180)))
    holoModel(5,"hq_torus")
    holoParent(5,entity())
    
    holoCreate(6)
    holoScale(6,vec(0.5,0.5,0.5))
    holoPos(6,entity():toWorld(vec(0,0,0)))
    holoAng(6,entity():toWorld(ang(90,-90,180)))
    holoModel(6,"hq_torus")
    holoParent(6,entity())
    
    holoCreate(7)
    holoScale(7,vec(0.5,0.5,0.5))
    holoPos(7,entity():toWorld(vec(0,0,0)))
    holoAng(7,entity():toWorld(ang(90,-90,180)))
    holoModel(7,"hq_torus")
    holoParent(7,entity())
    
    holoCreate(8)
    holoScale(8,vec(0.5,0.5,0.5))
    holoPos(8,entity():toWorld(vec(0,0,0)))
    holoAng(8,entity():toWorld(ang(90,-90,180)))
    holoModel(8,"hq_torus")
    holoParent(8,entity())
    
    holoCreate(9)
    holoScale(9,vec(0.5,0.5,0.5))
    holoPos(9,entity():toWorld(vec(0,0,0)))
    holoAng(9,entity():toWorld(ang(90,-90,180)))
    holoModel(9,"hq_torus")
    holoParent(9,entity())
}

R = round(RPM)
A = sin(curtime() * 100) * 30

holoPos(1,entity():toWorld(vec(0,0,-A)))
holoPos(2,entity():toWorld(vec(0,10,A)))
holoPos(3,entity():toWorld(vec(0,-10,A)))
holoPos(4,entity():toWorld(vec(0,20,-A)))
holoPos(5,entity():toWorld(vec(0,-20,-A)))
holoPos(6,entity():toWorld(vec(0,30,A)))
holoPos(7,entity():toWorld(vec(0,-30,A)))
holoPos(8,entity():toWorld(vec(0,40,-A)))
holoPos(9,entity():toWorld(vec(0,-40,-A)))
