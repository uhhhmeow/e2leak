@name BestVTOL
@model models/xqm/cylinderx2big.mdl
@persist [P2H_Mod P2H_Color P2H_Mat P2H_Pos P2H_Ang]:array P2H_Int P2H_Offset P2H_Ready
if(first())
{
    P2H_Int = 0
    P2H_Offset = 0
    runOnTick(1)
    P2H_Mod = array("models/hunter/tubes/tube1x1x3.mdl","models/sprops/geometry/hhex_96.mdl","models/hunter/plates/plate025x05.mdl","models/sprops/misc/tubes/size_60/tube_60x6.mdl","models/xqm/cylinderx2big.mdl","models/hunter/plates/plate025x025.mdl","models/sprops/rectangles/size_5/rect_48x72x3.mdl","models/sprops/rectangles/size_3/rect_24x48x3.mdl","models/hunter/plates/plate075x2.mdl","models/hunter/plates/plate075x3.mdl","models/hunter/plates/plate075x1.mdl","models/sprops/geometry/qring_24.mdl","models/sprops/misc/tubes/size_60/tube_60x6.mdl","models/sprops/geometry/qring_24.mdl","models/sprops/geometry/hhex_96.mdl","models/sprops/misc/tubes/size_60/tube_60x6.mdl","models/hunter/plates/plate025x025.mdl","models/sprops/rectangles/size_5/rect_48x72x3.mdl","models/xqm/cylinderx1big.mdl","models/sprops/misc/domes/size_3/dome_36x48.mdl","models/xqm/cylinderx2big.mdl","models/hunter/plates/plate025x05.mdl","models/sprops/rectangles/size_3/rect_24x48x3.mdl")
    P2H_Color = array(vec4(255,255,255,255),vec4(255,255,255,255),vec4(255,255,255,255),vec4(255,255,255,255),vec4(255,255,255,255),vec4(255,255,255,255),vec4(255,255,255,255),vec4(255,255,255,255),vec4(255,255,255,255),vec4(255,255,255,255),vec4(255,255,255,255),vec4(255,255,255,255),vec4(255,255,255,255),vec4(255,255,255,255),vec4(255,255,255,255),vec4(255,255,255,255),vec4(255,255,255,255),vec4(255,255,255,255),vec4(255,255,255,255),vec4(255,255,255,255),vec4(255,255,255,255),vec4(255,255,255,255),vec4(255,255,255,255))
    P2H_Mat = array(" "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," "," ")
    P2H_Pos = array(vec(-74.533,0.9859,-6.2505),vec(-3.4251,-26.6729,28.2519),vec(-3.0643,-28.1321,-4.9225),vec(-3.0416,-67.8981,-5.0367),vec(0.0023,-13.4237,-7.3902),vec(-2.9835,-102.0545,-3.7685),vec(-3.4208,-86.1225,19.8996),vec(-3.4029,-49.3884,45.0707),vec(-94.3495,6.8927,15.2291),vec(-112.0727,6.8685,10.6075),vec(-150.9153,6.8848,48.4507),vec(-171.1767,-15.089,48.4208),vec(-204.6901,0.9363,48.4346),vec(-172.0665,17.1468,48.4138),vec(-3.4455,29.4784,27.6934),vec(-3.9312,68.81,-5.6606),vec(-3.5656,104.7464,-3.9197),vec(-3.4457,88.9315,19.3225),vec(69.5275,0.7266,-5.6572),vec(81.45,0.7019,-5.6569),vec(0.2948,13.6906,-7.8158),vec(-3.9068,28.5666,-6.7476),vec(-3.4577,52.2116,44.4751))
    P2H_Ang = array(ang(89.9999,-0.0003,0),ang(0,179.9999,-33.7499),ang(0,-0.0001,0),ang(0,179.9999,-89.9997),ang(0,0,0.0001),ang(0,-0.6824,-0.0001),ang(44.9999,-90,0),ang(-0.0001,-179.9996,0),ang(0.0001,90.0004,45),ang(0.0051,90.3169,28.4853),ang(0.0019,90.0045,0.0036),ang(0.0001,135,-89.9999),ang(0,179.9999,90.0002),ang(-0.0001,-135.0001,90.0001),ang(0,0,-33.7497),ang(0,-0.0001,89.9999),ang(0,179.9999,0.0002),ang(45.0001,89.9999,0.0001),ang(0,-0.0001,-0.0001),ang(89.9999,-0.0011,0),ang(-0.1292,0.2122,-0.0001),ang(0,-180,0.0001),ang(0,-179.9997,0.0002))
}

if(!P2H_Ready)
{
    while(holoCanCreate() & perf() & P2H_Int < P2H_Mod:count())
    {
        P2H_Int++
        local P2H_TempInt = P2H_Int+P2H_Offset
        holoCreate(P2H_TempInt,entity():toWorld(P2H_Pos[P2H_Int,vector]),vec(1),entity():toWorld(P2H_Ang[P2H_Int,angle]))
        holoColor(P2H_TempInt,P2H_Color[P2H_Int,vector4])
        holoModel(P2H_TempInt,P2H_Mod[P2H_Int,string])
        holoMaterial(P2H_TempInt,P2H_Mat[P2H_Int,string])
        holoParent(P2H_TempInt,entity())
    }
    P2H_Ready = P2H_Int >= P2H_Mod:count()
}
