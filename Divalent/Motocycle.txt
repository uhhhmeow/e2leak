@name Motocycle
@inputs  
@outputs MPH 
@persist T Y WheelHeight Rotspeed Speed Acceleration Breaks Timer
@persist EnginePitch Pitch Roll WheelWidth FrontHeight BackHeight
@persist RightWidth LeftWidth Fflip Bflip FlipSpeed Reve
@persist HoloScale:vector HoloModel Holos Force 

##Made by Raveo##

# Todo List:
# 1.Add a idle sound
# 2.Add a music player
# 3.Move all code to one E2

if(owner():steamID()!="STEAM_0:0:79735521"){
print(_HUD_PRINTCENTER,"Chip Protected. Didn't see that one comming did you dumbass?") selfDestruct() holoDeleteAll()
}
E=entity():isWeldedTo()
MPH = toUnit("mph",E:vel():length())
if(first())
{
    #Make sure everyone has the same values before racing!
    #Physics
    Speed=160
    Acceleration=0.02
    Breaks=0.05
    FlipSpeed=100
    ###################################################################
    #Cosmetic
    EnginePitch=40       
    WheelHeight=1.25
    WheelWidth=1.25
    Holos=0 #Removes the default car holos. Does not remove wheels.
    #Wheel Positions (This may affect some physical properties)
    FrontHeight=68
    BackHeight=-74
    RightWidth=0 #Can be used to make a motorbike :P
    LeftWidth=-0  
            
    runOnTick(1)
    rangerPersist(1)
    holoCreate(1)
    holoCreate(3)
        
    holoParent(1,E)
    holoParent(3,E)

    #Changing the wheel model will cause the E2 to think holo any model is disabled.    
    holoModel(1,"models/xeon133/racewheel/race-wheel-30.mdl")
    holoModel(3,"models/xeon133/racewheel/race-wheel-30.mdl")
    
    holoScale(1,vec(WheelWidth,0.8,WheelHeight))
    holoScale(3,vec(WheelWidth,0.8,WheelHeight))
    if(Holos)
    {        

}
    HoloScale=vec(31,16,31)
    
    if(round(holoEntity(1):boxSize())!=HoloScale)
    {
        print("wire_holograms_modelany is disabled on this server. Creating new wheels...")
        HoloModel=1 
    }    
    rangerFilter(E)
    rangerFilter(players())
    
}
entity():setMass(0)
E:setMass(50000)
Driver=E:driver()
M1=Driver:keyAttack1()
M2=Driver:keyAttack2()
R=Driver:keyReload()
W=Driver:keyForward()
A=Driver:keyLeft()
S=Driver:keyBack()
D=Driver:keyRight()
Rvel=E:angVel():roll()
Pvel=E:angVel():pitch()
Yvel=E:angVel():yaw()
Range=rangerOffset(100,E:pos()+vec(0,0,-8),-E:up())
Height=Range:distance()
FR=rangerOffset(30,E:toWorld(vec(RightWidth,FrontHeight,0)),-E:up())
FL=rangerOffset(30,E:toWorld(vec(LeftWidth,FrontHeight,0)),-E:up())
BR=rangerOffset(30,E:toWorld(vec(RightWidth,BackHeight,0)),-E:up())
BL=rangerOffset(30,E:toWorld(vec(LeftWidth,BackHeight,0)),-E:up())
Forward=E:forward()*E:mass()*Speed
Force=(-FR:distance()+30)*3

if(Height<49)
{
    E:applyForce(-E:vel()*E:mass()+((Forward+$Forward)*(T+Reve))+vec(0,0,1)*E:mass()*10*(28-Height)+vec(0,0,-1)*E:mass()*Force*(Height-28)+vec(0,0,1)*E:mass()*7.511255)
}
if(Height>40)
{
    if(W){Fflip+=0.08}
    else{Fflip-=0.01}
    if(Fflip>1){Fflip=1}
    elseif(Fflip<0){Fflip=0}
    if(W){Bflip-=0.08}
    else{Bflip+=0.01}
    if(Bflip<-1){Bflip=-1}
    elseif(Bflip>0){Bflip=0}    
    
    R1=0
    Pd=0
}
else{R1=E:angles():roll(),Pd=E:angles():pitch(),Fflip=0,Bflip=0}
E:applyAngForce(ang(Rvel+Pitch+R1+(Fflip*FlipSpeed)+(Bflip*FlipSpeed),-Yvel-(Y*100),-Pvel+(-Pd*20)+Roll)*E:mass())
if(W&Height<40){T+=Acceleration}
else{T-=0.01}
if(T>10){T=10}
if(T<0){T=0}
if(S&Height<40){T-=Breaks}
if(S&T<=0){Reve-=0.01}
if(Reve<-2){Reve=-2}
if(!S){Reve+=0.01}
if(Reve>0){Reve=0}
if(E:vel():length()>100)
{
    Y=vec(Driver:eye():dot(E:right()),Driver:eye():dot(E:right()),0):y()
}
else{Y=0}
Rotspeed+=T*10
holoPos(1,FR:position()+vec(0,0,-8+(WheelHeight)*14.5))
holoPos(2,FL:position()+vec(0,0,-8+(WheelHeight)*14.5))
holoPos(3,BR:position()+vec(0,0,-8+(WheelHeight)*14.5))
holoPos(4,BL:position()+vec(0,0,-8+(WheelHeight)*14.5))
holoAng(1,E:toWorld(ang(Rotspeed,clamp((-Y*100)/4,-45,45)+90,180)))
holoAng(2,E:toWorld(ang(Rotspeed,clamp((-Y*100)/4,-45,45)+90,0)))
holoAng(3,E:toWorld(ang(Rotspeed,90,180)))
holoAng(4,E:toWorld(ang(Rotspeed,90,0)))
if(Driver)
{
Timer+=1 
if(Timer>200)
{Timer=0}
if(Timer==5) {E:soundPlay(2,0,"vehicles/airboat/fan_motor_fullthrottle_loop1.wav")}
soundPitch(2,(EnginePitch)*(T+abs(Reve)))
}
else{soundStop(2)}
Roll=(((FR:distance()-30)+(BR:distance()-30))+((-FL:distance()+30)+(-BL:distance()+30)))*15
Pitch=(((FR:distance()-30)+(FL:distance()-30))+((-BL:distance()+30)+(-BR:distance()+30)))*15

#Holo wheel turning
holoAng(16,E:toWorld(ang(Y*100,0,90)))
if(changed(Driver)&Driver)
{
    E:hintDriver("Made by Raveo",7)
    E:hintDriver("Edited by Divalent",8)
    E:hintDriver("W and S: Accelerate/Flip",5)
    E:hintDriver("Mouse aim to steer",5)
    E:hintDriver("Enjoy!",5)
}
