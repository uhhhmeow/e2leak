@name Basic
@inputs W S
@outputs 
@persist 
@trigger 
@model models/sprops/cylinders/size_3/cylinder_6x30.mdl
runOnTick(1)

E = entity()

if (W) {E:applyTorque(vec(0,0,80000))}

if (S) {E:applyTorque(vec(0,0,-80000))}
