@name Pod Rocket
@inputs Fire Reload
@outputs M:array
@persist Ammo Reloading LockFire E:entity SoundInt
@model models/hunter/blocks/cube025x1x025.mdl
if(first())
{
    function reload()
    {
        Reloading = 1
        
        soundPlay("reload", 0.52, "weapons/rpg/shotdown.wav")
        
        timer("reload", 200 + 150 * (16 - Ammo)) #Reloading when you're nearly full means lesser reload times, but it's better after shooting 2 rounds.
    }
    
    E = entity()
    E:setColor(vec4())
    
    holoCreate(1, E:toWorld(vec(-6, 0, 6)), vec(1, 1, 4), E:toWorld(ang(0, 0, 90)), vec(120))
    holoModel(1, "hq_cylinder")
    holoParent(1, E)
    holoMaterial(1, "models/shiny")
    
    holoCreate(2, E:toWorld(vec(-6, -1, 6)), vec(0.8, 0.8, 4), E:toWorld(ang(0, 0, 90)), vec(80))
    holoModel(2, "hq_cylinder")
    holoParent(2, E)
    holoMaterial(2, "models/shiny")
    
    Ammo = 16
    CanFire = 1
    
    timer("main", 0)
    
    propSpawnUndo(1)
}

if (~Fire & Fire) {
    timer("fire listen", 0)
}

if (clk("fire listen")) {
    if (Fire) { timer("fire listen", 50) }
    
    if (!LockFire) {
        Click = Reloading | !propCanCreate()
        soundPlay(SoundInt % 25, Click ? 0.038 : 0.8, Click ? "weapons/pistol/pistol_empty.wav" : "/mvm/giant_demoman/giant_demoman_grenade_shoot.wav")
        SoundInt++
        holoPos(2,holoEntity(2):toWorld(vec(0,0,1)))
        timer("shot",100)
        
        LockFire = 1
        
        timer("Unlock", 500)
        
        if (!Reloading) {
            if (!M:count()) { timer("missile", 0) }
            
            local TE = propSpawn("models/props_phx/ww2bomb.mdl", E:toWorld(vec(-6, 20, 6)), E:toWorld(ang(0, 90, 0)):forward():toAngle(), 1)
            TE:setColor(vec4())
            TE:setTrails(0, 30, 0.5, "trails/smoke", vec(255), 150)
            M:pushEntity(TE)
            
            Ammo--
            
            if (Ammo <= 0) {
                reload()
            }
        }
    }
}

if (clk("shot")) { holoPos(2,holoEntity(2):toWorld(vec(0,0,-1))) }

if (clk("missile")) {
    #Missiles
    if (M:count() > 0) { foreach(I, Mi:entity = M) {
            if (Mi:isValid()) {
                local RD = rangerOffset(550, Mi:pos(), Mi:forward())
                
                if (RD:hit()) {
                    Mi:setPos(RD:pos())
                    Mi:propBreak()
                    M:remove(I)
                } else {
                    Mi:setAng(clamp(Mi:toWorld(ang(1/24, 0, 0)), ang(-90, -360, 0), ang(90, 360, 0)))
                    Mi:setPos(Mi:toWorld(vec(500, 0, 0)))
                }
            } else {
                M:remove(I)
            }
        }
    timer("missile", 50)
    }
}

if (clk("Unlock")) {
    LockFire = 0
}

if (~Reload & Reload) {
    if (Ammo < 16) {
        reload()
    }
}

if (clk("reload")) {
    Ammo = 15
    Reloading = 0
    soundPlay("reload_finished", 0.988, "buttons/button4.wav")
}
