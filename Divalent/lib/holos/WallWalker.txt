@name Spider
@inputs [Cam Pod]:wirelink
@outputs PointyPos:vector Damping TransferCounter State MRNorm:vector
@trigger 
@persist [Ps]:array TMax DMax HoverHeight
@model models/hunter/plates/plate025x025.mdl
#include "lib/holos/wallwalkerholos"

runOnTick(1)

if(duped()){
print(_HUD_PRINTCENTER,"Chip Protected. Didn't see that one comming did you dumbass?") selfDestruct() holoDeleteAll()
}

entity():setColor(vec(),0)

local Seat = Pod["Entity",entity]
local Driver = Seat:driver()
local W = Driver:keyForward()
local A = Driver:keyLeft()
local S = Driver:keyBack()
local D = Driver:keyRight()
local R = Driver:keyReload()
local Alt = Driver:keyWalk()
local Mouse1 = Driver:keyAttack1()
local Mouse2 = Driver:keyAttack2()
local Shift = Driver:keySprint()
local Active = Driver:isPlayer()
local On = Active
local HoloEnd = entity()

if (first()) {    
    entity():setMass(50000)
    Ps = entity():getConstraints()
    
    # Ranger setup
    rangerPersist(1)
    rangerHitWater(0)
    rangerDefaultZero(0)
    rangerFilter(Ps)
    rangerFilter(players())
    rangerHitEntities(0)
    
    TransferCounter = -1
    State = 0
    TMax = 30
    DMax = 60
    HoverHeight = 5
    
    holoCreate(100)
    holoPos(100,entity():toWorld(vec(-20,0,20)))
    holoParent(100,entity())
    
} elseif (duped() | dupefinished()) {
    # Reset is duplicated
    reset()
} elseif (tickClk()) {

Cam["Position", vector] = holoEntity(100):pos()
Cam["Parent", entity] = holoEntity(100)
Cam["Activated", number] = Active

if ( TransferCounter == -1 ){

        DownRange = rangerOffset(DMax ,entity():pos(), entity():up() * -1 )
        ForRange = rangerOffset( DMax * 0.5, entity():pos(), entity():forward() )
        Bv = ( entity():forward() * 0.4) + (entity():up() * 0.6 )
        Bv *= -1
        BackRange = rangerOffset( DMax * 3, entity():pos(),  Bv  )

        if ( ForRange:hit() ){
            
            #state = 0
            
            MRNorm = ForRange:hitNormal()
            PointyPos = ForRange:position()
            State = 1
            
            TransferCounter = TMax
            
        }else{
        
            if ( DownRange:hit() ){
                
                #state = 1
                MRNorm = DownRange:hitNormal()
                PointyPos = DownRange:position()                
                State = 0
                
            }else{
            
                if ( BackRange:hit() ){
                    
                    #state = 2
                    MRNorm = BackRange:hitNormal()
                    PointyPos = BackRange:position()        
                    State = 2            
                    
                    TransferCounter = TMax 
                                      
                }else{
                    
                    MRNorm = vec(0,0,1)
                    PointyPos = DownRange:position()
                    State = 3

                }#Not backwards hit

            }#Not Forwards hit

        }#Not Down hit

    }#TransferCounter = -1 
    
    if (TransferCounter > -1){
     TransferCounter -= 1
    }
    
    
    Ang = MRNorm:cross(entity():forward() ):normalized():cross(MRNorm)
    
    # Quat calculations
    TarQ=quat(Ang,MRNorm)
    CurQ=quat(entity())
    Q=TarQ/CurQ
    Ve=entity():toLocal(rotationVector(Q)+entity():pos())
    
    #Stabilises
    entity():applyTorque((150*Ve -12*entity():angVelVector())*entity():inertia())
    
    #Applies a force when the user's pressing keys
    entity():applyAngForce( ang( 0,  120 * A * entity():mass() , 0 ) )
    entity():applyAngForce( -ang( 0,  120 * D * entity():mass() , 0 ) )

    Dist = PointyPos - entity():pos()
    
    XYZDist = sqrt( Dist:x() * Dist:x() + Dist:y() * Dist:y() + Dist:z() * Dist:z()  )
    entity():applyForce( -entity():vel() * entity():mass() )

    Mul = 10
    if ( XYZDist < HoverHeight ) { Mul *= -1 }
        
    OutForce = entity():mass() * entity():up() * -1 * Mul     
    
    OutForce += entity():mass() * entity():forward() * W * 100
    
    OutForce += entity():mass() * entity():forward() * Shift * 200
    
    OutForce += entity():mass() * -entity():forward() * S * 100
             
    entity():applyForce(  OutForce )
}
