@name Random Generation2
@persist M I Props:entity 
runOnChat(1)

if (chatClk(owner()) & owner():lastSaid():lower():find("!r")) {
    hideChat(1)
    reset()
}

if (first()) {M = 50}

if (I < M & holoCanCreate() & propCanCreate()) {
    interval(15)
    
    I++
    
    Rng = rangerOffset(1010,entity():toWorld(vec(randint(-500,500)*1,randint(-500,500)*1,1000)),vec(0,0,-1))
    
    holoCreate(I,Rng:pos()+holoEntity(I):boxSize())
    holoAng(I,Rng:hitNormal():toAngle()+ang(90,0,0))
    
    Props = propSpawn("models/sprops/cylinders/size_5/cylinder_12x96.mdl",holoEntity(I):pos() + vec(0,0,96/2),1)
} else {interval(100)}

if (changed(owner():keyPressed("z")) & owner():keyPressed("z")) {
    propDeleteAll()
    holoDeleteAll()
    entity():propDelete()
}

