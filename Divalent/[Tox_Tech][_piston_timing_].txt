@name [Tox Tech][ piston timing ]
@inputs [Piston_1 Piston_2 Piston_3 Piston_4 Piston_5 Piston_6 Piston_7 Piston_8 Piston_9 Piston_10 Piston_11 Piston_12 Crank Base]:wirelink Throttle Active
@persist Torque:number Max_rpm:number T1 T2 T3 T4 T5 T6
@outputs RPM
interval(16)

if(first()){
Max_rpm = 2000
Torque = 1000
} 
C = Crank:entity()
B = Base:entity()
P1= Piston_1:entity()
P2= Piston_2:entity()
P3= Piston_3:entity()
P4= Piston_4:entity()
P5= Piston_5:entity()
P6= Piston_6:entity()
P7= Piston_7:entity()
P8= Piston_8:entity()
P9= Piston_9:entity()
P10= Piston_10:entity()
P11= Piston_11:entity()
P12= Piston_12:entity()

Rev = Throttle * Active

Bearing=C:bearing(B:pos()+vec(0,0,12):rotate(B:angles()))

RPM = abs(C:angVel():yaw())/6

if(RPM>=Max_rpm){Rev=0}

T1 = sin(Bearing)*Rev
T2 = sin(Bearing + 120)*Rev
T3 = sin(Bearing + 240)*Rev

P1:applyForce(-P1:up()*P1:mass()*Torque*T1)

P2:applyForce(-P2:up()*P2:mass()*Torque*T2)

P3:applyForce(-P3:up()*P3:mass()*Torque*T3)

P4:applyForce(-P4:up()*P4:mass()*Torque*T1)

P5:applyForce(-P5:up()*P5:mass()*Torque*T2)

P6:applyForce(-P6:up()*P6:mass()*Torque*T3)

P7:applyForce(-P7:up()*P7:mass()*Torque*T1)

P8:applyForce(-P8:up()*P8:mass()*Torque*T2)

P9:applyForce(-P9:up()*P9:mass()*Torque*T3)

P10:applyForce(-P10:up()*P10:mass()*Torque*T1)

P11:applyForce(-P11:up()*P11:mass()*Torque*T2)

P12:applyForce(-P12:up()*P12:mass()*Torque*T3)
