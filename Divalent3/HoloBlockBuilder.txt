@name HoloBlockBuilder
@inputs 
@outputs 
@persist 
@trigger 
runOnTick(1)

if (first()) {
    
}

local Start = owner():keyWalk()
local Stop = owner():keyPressed("PAD_0")
local Left = owner():keyPressed("left")
local Down = owner():keyPressed("down")
local Up = owner():keyPressed("up")
local Right = owner():keyPressed("right")
local CornerPos1 = holoEntity(1):toWorld(vec(1,1,1) * 12 / 2)
local CornerPos1 = holoEntity(1):toWorld(-vec(1,1,1) * 12 / 2)

if (changed(Start) & Start) {
    holoCreate(1)    
    holoPos(1,entity():toWorld(vec(0,0,40)))
    holoColor(1,vec(255,255,255),255)
    holoParent(1,entity())
}

if (owner():aimEntity() == holoEntity(1) & owner():keyUse()) {
    holoPos(1,holoEntity(1):pos()+vec(0,0,1)*12)
}

if (changed(Stop) & Stop) {
    holoDeleteAll()
}
