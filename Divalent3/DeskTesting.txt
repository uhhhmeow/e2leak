@name DeskTesting
@inputs Egp:entity 
@outputs 
@persist A State 
@trigger 
interval(100)

if (first()) {
    entity():setAlpha(0)
    
    holoCreate(2)
    holoModel(2,"models/props_c17/FurnitureDrawer001a_Chunk02.mdl")
    holoPos(2,entity():toWorld(vec(-10.5,-10,25)))
    holoAng(2,ang(0,90,0))
    holoColor(2,vec(155,92,60))
    holoScale(2,vec(1,1,0.7))
    holoParent(2,1)
    holoClipEnabled(2,1,1)
    holoClip(2,vec(11.2,0,0),vec(-1,0,0),0)
    
    holoCreate(1)
    holoModel(1,"models/props_combine/breendesk.mdl")
    holoPos(1,entity():toWorld(vec(-32,-20,0)))
    holoAng(1,ang(0,-90,0))
    holoParent(1,2)
    holoClipEnabled(1,1,1)
    holoClip(1,vec(0,0,19.18),vec(0,0,1),0)
    
    holoClipEnabled(1,2,1)
    holoClip(1,2,vec(0,0,23.5),vec(0,0,-1),0)
    
    holoClipEnabled(1,3,1)
    holoClip(1,3,vec(0,21.2,0),vec(0,1,0),0)
    
    holoClipEnabled(1,4,1)
    holoClip(1,4,vec(-21,0,0),vec(-1,0,0),0)
    
    holoCreate(3)
    holoPos(3,entity():toWorld(vec(0.6,1.3,21.3)))
    holoScale(3,vec(1.8,0.05,0.34))
    holoColor(3,vec(0,0,0))
    holoParent(3,2)
    
    Egp:setAng(ang(0,90,0))
}
Egp:setPos(holoEntity(2):pos()+vec(11,0,-6))

R = owner():keyWalk()

if(changed(R) & R) {if(State == 0) {State = 1} else {State = 0}}

if(State == 1) {
    if(A < 0.38) {
        A += 0.01
        holoPos(2,holoEntity(2):pos()+vec(0,0.6,0))
    }
} else {
    if(A > 0) {
        A -= 0.01
        holoPos(2,holoEntity(2):pos()+vec(0,-0.6,0))
    }
}
