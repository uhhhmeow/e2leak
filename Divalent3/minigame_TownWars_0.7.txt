@name minigame_TownWars 0.7
@inputs [Egp Egp2]:wirelink [User]:entity
@persist [Data Towns TempTable TempTable2 UnderAttack]:table [TempTown DefPrices Namen]:array
@persist [Ply CreateEntity]:entity [Stadt GameName]:string Index IndexS Tick MTick IT IM IE DrawTowns Temp Time
@persist Away AFKtimer Fade DrawInfo InitTime
@trigger User

#This game is coded by LuckyGuy STEAM_0:1:15459836 bjoern@bjoern-kart.de

if(first()|duped()){
    function number egpClick(Player:entity,ID){
        local Pos=Egp:egpPos(ID)
        local Half=Egp:egpSize(ID)/2
        return inrange(Egp:egpCursor(Player),Pos-Half,Pos+Half)
    }
}
if(changed(Egp:entity()) & Egp:entity()){
    CreateEntity=Egp:entity()
    IM=1,IT=1,IE=1
    Egp2:egpClear()
    Egp2:egpBox(300,vec2(256,256),vec2(512,512)),Egp2:egpColor(300,vec())

    #Default Towns =  
    Towns["Berlin",array]=array(900,100,10,2,0,1,1,vec2(250,100),"Bot1",0)
    TempTable["Berlin",number]=1
    Towns["Bayern",array]=array(900,10,50,50,5,1,1,vec2(100,300),"Bot1",0)
    TempTable["Bayern",number]=1
    Data["Bot1",table]=TempTable:clone()
    TempTable:clear()
    Towns["Amsterdamm",array]=array(900,10,50,20,1,4,1,vec2(350,400),"Bot2",0)
    TempTable["Amsterdamm",number]=1
    Data["Bot2",table]=TempTable:clone()
    TempTable:clear()

    DefPrices   =array(0,100,0,0,500,1000,5000)
    Namen       =array("Money: ","Mine Field: ","Defending Soldiers: ","Soldiers: ","Unit Factory Lvl: ","Export Company Lvl: ","Infrastructure: ")

    InitTime=curtime()
    runOnFile(1)
    runOnLast(1)
    Index = -1
    Egp:egpClear()
    Egp:egpText(1,"Town Wars Setup",vec2(20,0)),Egp:egpSize(1,60)
    Egp:egpBox(2,vec2(256,110),vec2(400,50))
    Egp:egpText(3,"Start New Game",vec2(80,80)),Egp:egpSize(3,45),Egp:egpColor(3,vec())
    Egp:egpBox(4,vec2(256,310),vec2(400,50))
    Egp:egpText(5,"Load Save Game",vec2(75,280)),Egp:egpSize(5,45),Egp:egpColor(5,vec())
}

if(Index==-1){
    if(~User&User==owner() & IndexS==0){
        if(egpClick(User,2)){
            Egp:egpClear()
            Egp:egpRoundedBox(1,vec2(256,256),vec2(450,150)),Egp:egpColor(1,vec(77,77,77))
            Egp:egpRoundedBoxOutline(2,vec2(256,256),vec2(450,150))
            Egp:egpRoundedBox(3,vec2(256,146),vec2(450,70)),Egp:egpColor(3,vec(200,200,200))
            Egp:egpTextLayout(4,"Please type in chat a save game name",vec2(55,110),vec2(450,70)),Egp:egpSize(4,30),Egp:egpColor(4,vec())
            IndexS=1,runOnChat(1)
        }elseif(egpClick(User,4)){
            Egp:egpClear()
            Egp:egpRoundedBox(1,vec2(256,256),vec2(450,150)),Egp:egpColor(1,vec(77,77,77))
            Egp:egpRoundedBoxOutline(2,vec2(256,256),vec2(450,150))
            Egp:egpRoundedBox(3,vec2(256,146),vec2(450,70)),Egp:egpColor(3,vec(200,200,200))
            Egp:egpTextLayout(4,"Please type in chat a save game name",vec2(55,110),vec2(450,70)),Egp:egpSize(4,30),Egp:egpColor(4,vec())
            IndexS=2,runOnChat(1)
        }
    }
    if(chatClk(owner())){
        if(IndexS==1){
            local Text=owner():lastSaid()
            if(Text:matchFirst("/")|Text:matchFirst(":")|Text:matchFirst("*")|Text:matchFirst("?")|Text:matchFirst("<")|Text:matchFirst(">")|Text:matchFirst("|")){
                Egp:egpSetText(4,"Error: File name can not contain any of these symbols: \ / : * ? < > |"),Egp:egpColor(4,vec(100,0,0))
            }else{
                GameName = Text+".txt",runOnChat(0),Index=0,IndexS=0,timer("GameTick",60000)
                printColor(vec(200,200,200),"[",vec(0,200,0),"LuckyGuys Town Wars",vec(200,200,200),"]",vec(0,200,250)," Save Game Name set to ",vec(200,200,200),GameName,vec(0,200,250)," try to remember that :P")
            }
        }elseif(IndexS==2){
            local Text=owner():lastSaid()
            if(Text:matchFirst("/")|Text:matchFirst(":")|Text:matchFirst("*")|Text:matchFirst("?")|Text:matchFirst("<")|Text:matchFirst(">")|Text:matchFirst("|")){
                Egp:egpSetText(4,"Error: File name can not contain any of these symbols: \ / : * ? < > |"),Egp:egpColor(4,vec(100,0,0))
            }else{
                Egp:egpSetText(4,"Loading: "+GameName+".txt"),Egp:egpColor(4,vec(200,100,0))
                GameName = Text+".txt",timer("TimeOut",2000)
                fileLoad(GameName),runOnChat(0),IndexS=0
            }
        }
    }
    if(clk("TimeOut")){
        Egp:egpSetText(4,"Error: "+GameName+".txt Not found!"),Egp:egpColor(4,vec(150,0,0))
        timer("Reset",4000)
    }
    if(clk("Reset")){
        Egp:egpClear()
        Egp:egpText(1,"Town Wars Setup",vec2(20,0)),Egp:egpSize(1,60)
        Egp:egpBox(2,vec2(256,110),vec2(400,50))
        Egp:egpText(3,"Start New Game",vec2(60,65)),Egp:egpSize(3,45),Egp:egpColor(3,vec())
        Egp:egpBox(4,vec2(256,310),vec2(400,50))
        Egp:egpText(5,"Load Save Game",vec2(55,265)),Egp:egpSize(5,45),Egp:egpColor(5,vec())
    }
    if(fileClk(GameName)){
        Data=vonDecodeTable(fileRead()):clone()
        Towns=Data["Towns",table]:clone()
        Data:remove("Towns")
        timer("GameTick",60000)
        Index=0
    }
}


if(last()){
    Data["Towns",table]=Towns
    fileWrite(GameName,vonEncode(Data))
    local Sec=int(Time - InitTime)
    local Min=int(Sec/60)
    local Hrs=int(Min/60)
    printColor(vec(200,200,200),"[",vec(0,200,0),"LuckyGuys Town Wars",vec(200,200,200),"]",vec(0,200,0)," Saved Game: ",vec(200,200,200),GameName,vec(180,140,0)," after "+Hrs+":"+(Min-Hrs*60)+":"+(Sec-Min*60)+" playing time.")
}

Time=curtime()

if(Index==0){
    Egp:egpClear()
    Egp:egpText(1,"Town Wars",vec2(20,0)),Egp:egpSize(1,100)
    Egp:egpBox(2,vec2(256,256),vec2(400,100))
    Egp:egpText(3,"Join / Login",vec2(60,215)),Egp:egpSize(3,80),Egp:egpColor(3,vec())
    Egp:egpText(4,"by LuckyGuy Entertainments.",vec2(5,470)),Egp:egpSize(4,40),Egp:egpColor(4,vec(0,70,0))
    Index=1
}elseif(Index==1){
    if(~User&User){
        Ply=User
        local SteamID=User:steamID()
        if(Data:exists(SteamID)){
            TempTable:clear()
            Egp:egpClear()
            Egp:egpBox(1,vec2(256,30),vec2(512,60)),Egp:egpColor(1,vec(90,90,90))
            Egp:egpText(2,"Select a town",vec2(100,0)),Egp:egpSize(2,50)
            TempTable=Data[SteamID,table]:clone()
            local ID=0
            local Count=TempTable:count()
            local Size=452/Count
            foreach(K,V:number=TempTable){
                ID++
                Egp:egpBox(2+ID,vec2(256,60+(ID*Size)-Size/2),vec2(512,Size))
                Egp:egpColor(2+ID,vec(70,70,70))
                Egp:egpTextLayout(2+ID+Count,K:left(20),vec2(0,30+(ID*Size)-Size/2),vec2(512,80))
                Egp:egpSize(2+ID+Count,50),Egp:egpAlign(2+ID+Count,1)
            }
            Index=1.5
        }else{
            Egp:egpClear()
            Egp:egpTextLayout(1,"Please type a name for your town into chat now.",vec2(5,5),vec2(512,512)),Egp:egpSize(1,100),Egp:egpAlign(1,1)
            Index=2,runOnChat(1)
        }
    }
}elseif(Index==1.5){
    if(AFKtimer<Time){if(Ply:pos():distance(CreateEntity:pos())>190){Index=0}else{AFKtimer=Time+2}}
    if(~User&User==Ply){
        local Keys=TempTable:keys()
        for(A=1,Keys:count()){
            if(egpClick(User,2+A)){
                Stadt=Keys[A,string],TempTown=Towns[Stadt,array]:clone(),DrawInfo=1
                break
            }
        }
    }
}elseif(Index==2){
    if(AFKtimer<Time){if(Ply:pos():distance(CreateEntity:pos())>190){Index=0,runOnChat(0)}else{AFKtimer=Time+3}}
    if(chatClk(Ply)){
        Stadt=Ply:lastSaid()
        if(Towns:exists(Stadt)){
            Egp:egpSetText(1,"This town exists already. Try again...")
        }elseif(Stadt==" "|Stadt:matchFirst("   ")){
            Egp:egpSetText(1,"Invalid Town name. Try again...")
        }elseif(Stadt:length()>15){
            Egp:egpSetText(1,"Town name is limited to 15 chars.")
        }else{
            #ARRAY SYNTAX:
            #1 Geld
            #2 Minen
            #3 Stationierte soldaten
            #4 Soldaten
            #5 Soldaten fabrik
            #6 Export fabrik
            #7 Infrastruktur
            #8 Position
            #9 Owner steam ID
            #10 attackierende soldaten zahl
            #11 position
            #12 ziel pos
            #13 Ziel stadt name

            TempTown=array(900,0,0,0,0,0,1,vec2(random(100,412),random(100,412)),Ply:steamID(),0,vec2(),vec2(),"")
            Towns[Stadt,array]=TempTown:clone()
            TempTable:clear()
            TempTable[Stadt,number]=1
            Data[Ply:steamID(),table]=TempTable:clone()
            Egp:egpClear()
            Egp2:egpClear()
            Egp2:egpBox(300,vec2(256,256),vec2(512,512)),Egp2:egpColor(300,vec())
            Index=3,DrawInfo=1,MTick=Time
            runOnChat(0)
        }
    }
}elseif(Index==3){
    if(AFKtimer<Time){
        if(Ply:pos():distance(CreateEntity:pos())>180){
            Towns[Stadt,array]=TempTown:clone()
            Index=0,Ply=noentity(),TempTown:clear()
        }else{
            AFKtimer=Time+3
        }
    }

    if(~User&User==Ply){
        if(egpClick(User,50)){
            if(IndexS==2){
                if(DefPrices[IndexS,number]<=TempTown[1,number]){
                    if(TempTown[1,number]>=10000){
                        TempTown[1,number]=TempTown[1,number]-DefPrices[IndexS,number]*100
                        TempTown[IndexS,number]=TempTown[IndexS,number]+100
                    }elseif(TempTown[1,number]>=1000){
                        TempTown[1,number]=TempTown[1,number]-DefPrices[IndexS,number]*10
                        TempTown[IndexS,number]=TempTown[IndexS,number]+10
                    }else{
                        TempTown[1,number]=TempTown[1,number]-DefPrices[IndexS,number]
                        TempTown[IndexS,number]=TempTown[IndexS,number]+1
                    }
                    Egp:egpSetText(12,Namen[1,string]+TempTown[1,number])
                    Egp:egpSetText(11+IndexS,Namen[IndexS,string]+TempTown[IndexS,number])
                    Egp:egpSetText(4,"You have "+TempTown[IndexS,number]+" mines around you")
                    soundPlay(1,0.8,"ambient/levels/labs/coinslot1.wav")
                    if(Data[Ply:steamID(),table]:exists(Stadt)){
                        Towns[Stadt,array]=TempTown:clone()
                    }
                }else{
                    soundPlay(1,0.7,"buttons/button19.wav")
                }
            }elseif(IndexS==3){
                if(TempTown[3,number]>0){
                    Egp:egpSetText(4,"Type in chat how many you want to move")
                    runOnChat(1)
                }
            }elseif(IndexS==4){
                if(TempTown[4,number]>0){
                    Egp:egpSetText(4,"Type in chat how many you command to defend")
                    runOnChat(1)
                }
            }elseif(IndexS==5){
                if(DefPrices[IndexS,number]*TempTown[IndexS,number]<=TempTown[1,number]){
                    TempTown[1,number]=TempTown[1,number]-DefPrices[IndexS,number]*TempTown[IndexS,number]
                    TempTown[IndexS,number]=TempTown[IndexS,number]+1
                    Egp:egpSetText(12,Namen[1,string]+TempTown[1,number])
                    Egp:egpSetText(11+IndexS,Namen[IndexS,string]+TempTown[IndexS,number])
                    Egp:egpSetText(4,"You produce "+(TempTown[IndexS,number]*TempTown[7,number])+"/m    Next upgrade costs: "+(TempTown[IndexS,number]*DefPrices[IndexS,number])+"$")
                    soundPlay(1,0.8,"ambient/levels/labs/coinslot1.wav")
                    if(Data[Ply:steamID(),table]:exists(Stadt)){
                        Towns[Stadt,array]=TempTown:clone()
                    }
                }else{
                    soundPlay(1,0.7,"buttons/button19.wav")
                }
            }elseif(IndexS==6){
                if(DefPrices[IndexS,number]*TempTown[IndexS,number]<=TempTown[1,number]){
                    TempTown[1,number]=TempTown[1,number]-DefPrices[IndexS,number]*TempTown[IndexS,number]
                    TempTown[IndexS,number]=TempTown[IndexS,number]+1
                    Egp:egpSetText(12,Namen[1,string]+TempTown[1,number])
                    Egp:egpSetText(11+IndexS,Namen[IndexS,string]+TempTown[IndexS,number])
                    Egp:egpSetText(4,"Creates money for you Next upgrade costs: "+(TempTown[6,number]*DefPrices[6,number])+"$")
                    soundPlay(1,0.8,"ambient/levels/labs/coinslot1.wav")
                    if(Data[Ply:steamID(),table]:exists(Stadt)){
                        Towns[Stadt,array]=TempTown:clone()
                    }
                }else{
                    soundPlay(1,0.7,"buttons/button19.wav")
                }
            }elseif(IndexS==7){
                if(DefPrices[IndexS,number]*TempTown[IndexS,number]*2<=TempTown[1,number]){
                    TempTown[1,number]=TempTown[1,number]-DefPrices[IndexS,number]*TempTown[IndexS,number]*2
                    TempTown[IndexS,number]=TempTown[IndexS,number]+1
                    Egp:egpSetText(12,Namen[1,string]+TempTown[1,number])
                    Egp:egpSetText(11+IndexS,Namen[IndexS,string]+TempTown[IndexS,number])
                    Egp:egpSetText(4,"Increases all productions Next upgrade costs: "+(TempTown[7,number]*2*DefPrices[7,number])+"$")
                    soundPlay(1,0.8,"ambient/levels/labs/coinslot1.wav")
                    if(Data[Ply:steamID(),table]:exists(Stadt)){
                        Towns[Stadt,array]=TempTown:clone()
                    }
                }else{
                    soundPlay(1,0.7,"buttons/button19.wav")
                }
            }
        }elseif(egpClick(User,52)){
            if(IndexS==4){
                if(TempTown[10,number]>0){
                    UnderAttack[TempTown[13,string],number]=0
                    TempTown[13,string]=Stadt
                    TempTown[12,vector2]=TempTown[8,vector2]
                    if(Data[Ply:steamID(),table]:exists(Stadt)){
                        Towns[Stadt,array]=TempTown:clone()
                    }
                    Egp:egpSetText(4,"Your army will retreat to their home")
                }elseif(TempTown[4,number]>0){
                    if(TempTown[10,number]<=0){
                        Egp:egpSetText(4,"Type in chat how many you want to send")
                        runOnChat(1)
                        IndexS=4.5
                    }else{
                        Egp:egpSetText(4,"You can start only 1 attack run at a time :(")
                        soundPlay(1,0.7,"buttons/button19.wav")
                    }
                }else{
                    Egp:egpSetText(4,"You have no units to attack with :(")
                    soundPlay(1,0.7,"buttons/button19.wav")
                }
            }
        }elseif(egpClick(User,19)){
            Towns[Stadt,array]=TempTown:clone()
            Index=0,Ply=noentity(),TempTown:clear()
        }elseif(egpClick(User,24)){
            IndexS=5,runOnChat(1)
            printColor(vec(200,200,200),"[",vec(0,200,0),"LuckyGuys Town Wars",vec(200,200,200),"]",vec(255,0,0)," Cheat ",vec(200,200,200),"type in chat a number between 1-7 and another to set a value.",vec(100,100,255)," example to cheat money: 1 99999999999")
            if(Ply!=owner()){
                 printColor(vec(200,200,200),"[",vec(0,200,0),"LuckyGuys Town Wars",vec(200,200,200),"]",vec(255,255,255)," if you want "+Ply:name()+" to cheat tell him that command.")
            }
        }elseif(egpClick(User,21)){
            if(UnderAttack[Stadt,number]==1){
                Egp:egpText(23,"Your under attack",vec2(320,435)),Egp:egpSize(23,25),Egp:egpFont(23,"Coolvetica")
            }else{
                Index=5
                Egp:egpBox(100,vec2(256,300),vec2(400,200)),Egp:egpColor(100,vec(100,100,130))
                Egp:egpText(101,"Are You Sure ?",vec2(90,250)),Egp:egpSize(101,60)
                Egp:egpBox(102,vec2(126,350),vec2(100,50)),Egp:egpColor(102,vec(0,200,0))
                Egp:egpText(103,"Yes",vec2(100,325)),Egp:egpSize(103,50)
                Egp:egpBox(104,vec2(326,350),vec2(100,50)),Egp:egpColor(104,vec(200,0,0))
                Egp:egpText(105,"No",vec2(300,325)),Egp:egpSize(105,50)
            }
        }else{
            for(A=1,7){
                if(egpClick(User,4+A)){
                    Egp:egpRemove(50),Egp:egpRemove(51),Egp:egpRemove(52),Egp:egpRemove(53)
                    if(A==1){
                        Egp:egpSetText(4,"Income: $"+(TempTown[6,number]*400*TempTown[7,number])+" /m")
                    }elseif(A==2){
                        Egp:egpSetText(4,"You have "+TempTown[2,number]+" mines around you")
                        Egp:egpBox(50,vec2(460,80+A*45),vec2(100,40)),Egp:egpColor(50,vec(0,120,0))
                        Egp:egpText(51,"BUY",vec2(420,60+A*45)),Egp:egpSize(51,40)
                    }elseif(A==3){
                        Egp:egpSetText(4,"There are "+TempTown[3,number]+" soldiers defending")
                        Egp:egpBox(50,vec2(460,80+A*45),vec2(100,40)),Egp:egpColor(50,vec(0,0,120))
                        Egp:egpText(51,"MOVE",vec2(415,60+A*45)),Egp:egpSize(51,40)
                    }elseif(A==4){
                        Egp:egpSetText(4,TempTown[4,number]+" Soldiers waiting for commands")
                        Egp:egpBox(50,vec2(460,80+A*45),vec2(115,40)),Egp:egpColor(50,vec(120,100,0))
                        Egp:egpText(51,"Defend",vec2(405,60+A*45)),Egp:egpSize(51,40),Egp:egpFont(51,"Coolvetica")
                        if(TempTown[10,number]<=0){
                            Egp:egpBox(52,vec2(340,80+A*45),vec2(110,40)),Egp:egpColor(52,vec(150,0,0))
                            Egp:egpText(53,"Attack",vec2(285,60+A*45)),Egp:egpSize(53,40),Egp:egpFont(53,"Coolvetica")
                        }else{
                            Egp:egpBox(52,vec2(340,80+A*45),vec2(110,40)),Egp:egpColor(52,vec(0,150,0))
                            Egp:egpText(53,"Retreat",vec2(285,60+A*45)),Egp:egpSize(53,40),Egp:egpFont(53,"Coolvetica")
                        }
                    }elseif(A==5){
                        Egp:egpSetText(4,"You produce "+(TempTown[5,number]*TempTown[7,number])+"/m    Next upgrade costs: "+(TempTown[5,number]*DefPrices[5,number])+"$")
                        Egp:egpBox(50,vec2(440,80+A*45),vec2(150,40)),Egp:egpColor(50,vec(0,0,120))
                        Egp:egpText(51,"Upgrade",vec2(365,60+A*45)),Egp:egpSize(51,40)
                    }elseif(A==6){
                        Egp:egpSetText(4,"Creates money for you Next upgrade costs: "+(TempTown[6,number]*DefPrices[6,number])+"$")
                        Egp:egpBox(50,vec2(440,80+A*45),vec2(150,40)),Egp:egpColor(50,vec(0,0,120))
                        Egp:egpText(51,"Upgrade",vec2(365,60+A*45)),Egp:egpSize(51,40)
                    }elseif(A==7){
                        Egp:egpSetText(4,"Increases all productions Next upgrade costs: "+(TempTown[7,number]*2*DefPrices[7,number])+"$")
                        Egp:egpBox(50,vec2(440,80+A*45),vec2(150,40)),Egp:egpColor(50,vec(0,0,120))
                        Egp:egpText(51,"Upgrade",vec2(365,60+A*45)),Egp:egpSize(51,40)
                    }
                    IndexS=A,runOnChat(0)
                    break
                }
            }
        }
    }

    if(chatClk(Ply)){
        if(IndexS==3){
            local N=clamp(Ply:lastSaid():toNumber(),0,TempTown[3,number])
            TempTown[3,number]=TempTown[3,number]-N
            TempTown[4,number]=TempTown[4,number]+N
            Egp:egpSetText(4,"There are "+TempTown[3,number]+" soldiers defending")
            Egp:egpSetText(14,Namen[IndexS,string]+TempTown[IndexS,number])
            Egp:egpSetText(15,Namen[4,string]+TempTown[4,number])
            runOnChat(0)
            if(Data[Ply:steamID(),table]:exists(Stadt)){
                Towns[Stadt,array]=TempTown:clone()
            }
        }elseif(IndexS==4){
            local N=clamp(Ply:lastSaid():toNumber(),0,TempTown[4,number])
            TempTown[3,number]=TempTown[3,number]+N
            TempTown[4,number]=TempTown[4,number]-N
            Egp:egpSetText(4,TempTown[4,number]+" Soldiers waiting for commands")
            Egp:egpSetText(15,Namen[IndexS,string]+TempTown[IndexS,number])
            Egp:egpSetText(14,Namen[3,string]+TempTown[3,number])
            runOnChat(0)
            if(Data[Ply:steamID(),table]:exists(Stadt)){
                Towns[Stadt,array]=TempTown:clone()
            }
        }elseif(IndexS==4.5){
            local N=clamp(Ply:lastSaid():toNumber(),0,TempTown[4,number])
            TempTown[4,number]=TempTown[4,number]-N
            Temp=N
            Egp:egpClear()
            Egp:egpBox(1,vec2(256,25),vec2(512,50)),Egp:egpColor(1,vec(150,150,150))
            Egp:egpText(2,"Attack /Send troops to...",vec2(15,0)),Egp:egpSize(2,40)
            if(Data[Ply:steamID(),table]:exists(Stadt)){
                Towns[Stadt,array]=TempTown:clone()
            }
            Index=4,DrawTowns=1
            runOnChat(0)
        }elseif(IndexS==5){
            local Com=Ply:lastSaid():explode(" ")
            local N=clamp(Com[1,string]:toNumber(),1,7)
            TempTown[N,number]=Com[2,string]:toNumber()
            runOnChat(0)
        }
    }
}elseif(Index==4){
    if(~User&User==Ply){
        local ID=0
        foreach(K,V:array=Towns){
            ID++
            if(egpClick(Ply,2+ID)&K!=Stadt){
                TempTown[10,number]=Temp
                TempTown[11,vector2]=TempTown[8,vector2]
                TempTown[12,vector2]=V[8,vector2]
                TempTown[13,string]=K
                UnderAttack[K,number]=1
                Towns[Stadt,array]=TempTown:clone()
                Egp:egpClear()
                DrawInfo=1
                break
            }
        }
    }
}elseif(Index==5){
    if(~User&User==Ply){
        if(egpClick(Ply,102)){
            Towns:remove(Stadt)
            TempTable:remove(Stadt)
            if(TempTable:count()<=0){
                Data:remove(Ply:steamID())
            }else{
                Data[Ply:steamID(),table]=TempTable:clone()
            }
            Egp2:egpClear()
            Egp2:egpBox(300,vec2(256,256),vec2(512,512)),Egp2:egpColor(300,vec())
            Index=0,Ply=noentity(),TempTown:clear(),MTick=Time
        }elseif(egpClick(Ply,104)){
            Egp:egpRemove(100),Egp:egpRemove(101),Egp:egpRemove(102),Egp:egpRemove(103),Egp:egpRemove(104),Egp:egpRemove(105)
            Index=3
        }
    }
}

if(DrawInfo){
    Egp:egpBox(1,vec2(256,256),vec2(512,512)),Egp:egpColor(1,vec(50,180,20))
    Egp:egpRoundedBox(2,vec2(256,50),vec2(500,100)),Egp:egpColor(2,vec())
    Egp:egpRoundedBoxOutline(3,vec2(256,50),vec2(500,100))
    Egp:egpTextLayout(4,"The Citizens of "+Stadt+" greet you",vec2(10,5),vec2(500,100)),Egp:egpSize(4,40),Egp:egpAlign(4,1)
    for(Z=DrawInfo,7){
        if( maxquota()-opcounter() < 50 ){
            DrawInfo=Z
            interval(100)
            exit()
        }
        Egp:egpBox(4+Z,vec2(256,80+Z*45),vec2(512,40)),Egp:egpColor(4+Z,vec())
        Egp:egpText(11+Z,Namen[Z,string]+TempTown[Z,number],vec2(5,60+Z*45)),Egp:egpSize(11+Z,35)
    }
    Egp:egpBox(19,vec2(120,487),vec2(200,50)),Egp:egpColor(19,vec(100,0,0))
    Egp:egpText(20,"Exit",vec2(70,465)),Egp:egpSize(20,45),Egp:egpFont(20,"Coolvetica")
    Egp:egpBox(21,vec2(420,487),vec2(150,30)),Egp:egpColor(21,vec(200,0,0))
    Egp:egpText(22,"Burn Town",vec2(370,475)),Egp:egpSize(22,25),Egp:egpFont(22,"Coolvetica")
    #Egp:egpBoxOutline(24,vec2(270,475),vec2(20,10))
    DrawInfo=0,Index=3
}

if(DrawTowns){
    local KA=Towns:keys()
    for(F=DrawTowns,Towns:count()){
        if( maxquota()-opcounter() < 50 ){
            DrawTowns=F
            interval(100)
            exit()
        }

        local X=int((F-1)/10)
        Egp:egpBox(2+F,vec2(128+X*256,50+F*40-X*400),vec2(250,40))
        if(Towns[KA[F,string],array][9,string]==Ply:steamID()){Egp:egpColor(2+F,vec(5,100,5))}else{Egp:egpColor(2+F,vec(100,20,20))}
        Egp:egpTextLayout(2+F+Towns:count(),KA[F,string],vec2(250*X,30+F*40-X*400),vec2(250,40)),Egp:egpSize(2+F+Towns:count(),40),Egp:egpAlign(2+F+Towns:count(),1)
    }
    DrawTowns=0
}


if(Tick<Time){
    local K=Towns:keys()

    for(B=IT,K:count()){
        if( maxquota()-opcounter() < 50 ){
            IT=B
            interval(100)
            exit()
        }
        local Town=Towns[K[B,string],array]:clone()
        Town[1,number]=Town[1,number]+Town[6,number]*400*Town[7,number]
        Town[4,number]=Town[4,number]+Town[5,number]*Town[7,number]
        Towns[K[B,string],array]=Town:clone()
        if(Index==3){
            if(Ply:steamID()==Town[9,string]&Stadt==K[B,string]){
                TempTown=Town:clone()
                Egp:egpSetText(12,Namen[1,string]+TempTown[1,number])
                Egp:egpSetText(15,Namen[4,string]+TempTown[4,number])
                if(IndexS==4){
                    Egp:egpSetText(4,TempTown[4,number]+" Soldiers waiting for commands")
                }
            }
        }
    }
    Tick=Time+60,IT=1
}


if(MTick<Time){
    local KE=Towns:keys()
    for(D=IM,KE:count()){
        if( maxquota()-opcounter() < 90 ){
            IM=D
            interval(100)
            exit()
        }
        local TownA = Towns[KE[D,string],array]:clone()
        Egp2:egpCircle(D,TownA[8,vector2],vec2(10,10))
        Egp2:egpText(D+50,KE[D,string],TownA[8,vector2]-vec2(KE[D,string]:length(),25)),Egp2:egpAlign(D+50,1)
        local Units=TownA[10,number]
        local Total=Units
        if(Units>0){
            local Pos=TownA[11,vector2]
            if(Pos:distance(TownA[12,vector2])>5){
                Pos = Pos + (TownA[12,vector2] - Pos):normalized()*3.5
                TownA[11,vector2]=Pos
                Egp2:egpBox(D+100,Pos,vec2(35,35)),Egp2:egpMaterial(D+100,"vgui/gfx/vgui/guerilla")
                Egp2:egpText(D+150,Units:toString(),Pos-vec2(10,25)),Egp2:egpColor(D+150,vec(0,160,0))
                if(Index==3){
                    if(Ply:steamID()==TownA[9,string]&KE[D,string]==Stadt){
                        TempTown=TownA:clone()
                    }
                }
                Towns[KE[D,string],array]=TownA:clone()
            }else{
                local TownB = Towns[TownA[13,string],array]:clone()
                Fade=Time+30
                Egp2:egpRemove(D+100),Egp2:egpRemove(D+150),Egp2:egpRemove(201),Egp2:egpRemove(202),Egp2:egpRemove(203),Egp2:egpRemove(204),Egp2:egpRemove(205),Egp2:egpRemove(206),Egp2:egpRemove(207),Egp2:egpRemove(208)
                local Pos = clamp(TownB[8,vector2],vec2(125,75),vec2(387,437))
                if(TownB[9,string]!=TownA[9,string]){
                    Egp2:egpBox(201,Pos ,vec2(250,150)),Egp2:egpColor(201,vec(100,100,100))
                    Egp2:egpText(202,"Battle Details",Pos-vec2(60,70))
                    Egp2:egpText(203,KE[D,string]+" vs "+TownA[13,string],Pos-vec2(90,50))
                    local Death=int(clamp(random(TownB[2,number]/3,TownB[2,number]),0,Units))
                    Egp2:egpText(204,Death+"/ "+Total+" died by mines.",Pos-vec2(110,30))
                    Units=Units-Death
                    TownB[2,number]=clamp(TownB[2,number]-Death,0,TownB[2,number])
                    if(Units>0){
                        Death=int(clamp(TownB[3,number]*1.25,0,Units))
                        Egp2:egpText(205,Death+"/ "+Total+" died by defenders.",Pos-vec2(110,10))
                        Units=Units-Death
                        TownB[3,number]=int(clamp(TownB[3,number]-(Death/1.25),0,TownB[3,number]))
                    }
                    if(Units>0){
                        Death=int(clamp(TownB[4,number]/2,0,Units))
                        Egp2:egpText(206,Death+"/ "+Total+" died by normals.",Pos-vec2(110,-10))
                        Units=Units-Death
                        TownB[4,number]=clamp(TownB[4,number]-Death*2,0,TownB[4,number])
                    }
                    if(Units>0){
                        Egp2:egpText(207,Units+"/ "+Total+" survived.",Pos-vec2(110,-30))
                        Egp2:egpText(208,"TOWN CAPTURED",TownA[11,vector2]-vec2(70,-50))
                        if(Index==3){
                            if(Ply:steamID()==TownB[9,string]&Stadt==TownA[13,string]){
                                Index=0
                            }
                        }
                        TempTable2=Data[TownB[9,string],table]:clone()
                        TempTable2:remove(TownA[13,string])
                        if(TempTable2:count()<=0){
                            Data:remove(TownB[9,string])
                        }else{
                            Data[TownB[9,string],table]=TempTable2:clone()
                        }
                        TempTable2:clear()
                        TempTable2=Data[TownA[9,string],table]:clone()
                        TempTable2[TownA[13,string],number]=1
                        Data[TownA[9,string],table]=TempTable2:clone()
                        TempTable2:clear()
                        TownB[4,number]=Units
                        TownB[9,string]=TownA[9,string]
                        Towns[TownA[13,string],array]=TownB:clone()
                        TownA[10,number]=0,Units=0,TownA[13,string]=""
                        Towns[KE[D,string],array]=TownA:clone()
                        if(Index==3){
                            if(Ply:steamID()==TownA[9,string]&Stadt==KE[D,string]){
                                TempTown=TownA:clone()
                            }
                        }
                    }else{
                        Egp2:egpText(207,"All "+Total+" attackers died.",Pos-vec2(100,-30))
                        Egp2:egpText(208,"ATTACK DEFENDED",Pos-vec2(70,-50))
                        TownA[10,number]=0,Units=0
                        TownA[11,vector2]=TownA[8,vector2]
                        Towns[KE[D,string],array]=TownA:clone()
                        Towns[TownA[13,string],array]=TownB:clone()
                        if(Index==3){
                            if(Ply:steamID()==TownB[9,string]&Stadt==TownA[13,string]){
                                TempTown=TownB:clone()
                                Egp:egpSetText(12,Namen[1,string]+TempTown[1,number])
                                Egp:egpSetText(13,Namen[2,string]+TempTown[2,number])
                                Egp:egpSetText(14,Namen[3,string]+TempTown[3,number])
                                Egp:egpSetText(15,Namen[4,string]+TempTown[4,number])
                                if(IndexS==2){
                                    Egp:egpSetText(4,"You have "+TempTown[2,number]+" mines around you")
                                }elseif(IndexS==4){
                                    Egp:egpSetText(4,TempTown[4,number]+" Soldiers waiting for commands")
                                }
                            }
                        }
                        UnderAttack[TownA[13,string],number]=0
                        TownA[13,string]=""
                    }
                }elseif(Units){
                    Egp2:egpBox(201,Pos,vec2(200,70)),Egp2:egpColor(201,vec(100,100,100))
                    Egp2:egpText(202,KE[D,string]+" -> "+TownA[13,string],Pos-vec2(90,30))
                    Egp2:egpText(203,Units+" Units arrived",Pos-vec2(75,10))
                    TownB[4,number]=TownB[4,number]+Units
                    TownA[10,number]=0,TownB[10,number]=0,Units=0
                    Towns[KE[D,string],array]=TownA:clone()
                    Towns[TownA[13,string],array]=TownB:clone()
                    UnderAttack[TownA[13,string],number]=0
                    if(Index==3){
                        if(Ply:steamID()==TownB[9,string]&Stadt==TownA[13,string]){
                            TempTown=TownB:clone()
                            Egp:egpSetText(15,Namen[4,string]+TempTown[4,number])
                            if(IndexS==4){
                                Egp:egpSetText(4,TempTown[4,number]+" Soldiers waiting for commands")
                            }
                        }
                    }
                }
            }
        }
        if(Fade&Fade<Time){
            Egp2:egpRemove(201),Egp2:egpRemove(202),Egp2:egpRemove(203),Egp2:egpRemove(204),Egp2:egpRemove(205),Egp2:egpRemove(206),Egp2:egpRemove(207),Egp2:egpRemove(208)
            Fade=0
        }
    }
    MTick=Time+1,IM=1
}

interval(1000)
