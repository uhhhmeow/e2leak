@name Under Toilet
@inputs Pod:wirelink  
@persist Door:entity DoorNormalPlace DoorOpenPlace Tog 
entity():setColor(vec(),0)
local Seat = Pod["Entity",entity]
local Driver = Seat:driver()
local R = Driver:keyReload()
local Shift = Driver:keySprint()
local Active = Driver:isPlayer()

runOnTick(1)
if(first()) {
    Door = entity():isWeldedTo()
    
    DoorNormalPlace = Door:pos():z()
    DoorOpenPlace = DoorNormalPlace + 90
}

if(changed(Driver:keyZoom()) & Driver:keyZoom() | owner():keyWalk()) {
    if(Tog == 1) {
        Tog = 0
    } else {
        Tog = 1
    }
}

if(Tog) {
    if(Door:pos():z() < DoorOpenPlace) {
        Door:setPos(Door:pos() + vec(0, 0, 1))
    }
} else {
    if(Door:pos():z() > DoorNormalPlace) {
        Door:setPos(Door:pos() - vec(0, 0, 1))
    }
}
