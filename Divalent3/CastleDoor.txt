@name CastleDoor
@persist Gates:array A State 
interval(100)

if(first()) {
    findByModel("models/props_manor/gothic_gate_01b.mdl")
    findIncludePlayerProps(owner())
    find()
    Gates = findToArray()
    
    foreach(ID, Gate:entity=Gates) {
        holoCreate(ID,Gate:pos())
        holoAng(ID,Gate:angles())
        holoModel(ID,Gate:model())
        holoClipEnabled(ID,1,1)
        holoClip(ID,Gate:pos()+vec(0,0,-36),vec(0,0,-1),1)
        holoParent(ID,Gate)
        Gate:setAlpha(0)
    }
}

R = owner():keyWalk()

if (changed(R) & R) {if(State == 0) {State = 1} else {State = 0}}

if(State == 1) {
    if(A < 200) {
        A += 5
        foreach(ID, Gate:entity=Gates) {
            Gate:setPos(Gate:pos() + vec(0,0,5))
        }
    }
} else {
    if(A > 0) {
        A -= 5
        
        foreach(ID, Gate:entity=Gates) {
            Gate:setPos(Gate:pos() - vec(0,0,5))
        }
    }
}
