@name LittleMan
@inputs [Pod Cam]:wirelink 
@outputs Gyro:angle 
@persist Timer
@trigger 
@model models/props_junk/garbage_plasticbottle003a.mdl
interval(10)

E = entity()

if (first()) {
    entity():setColor(vec(),0)
    
    #Head
    holoCreate(1)
    holoModel(1,"hq_sphere")
    holoScale(1,vec(0.4,0.4,0.4))
    holoPos(1,E:toWorld(vec(0,0,5.5)))
    holoParent(1,E)
    
    #Neck
    holoCreate(2)
    holoModel(2,"hq_cylinder")
    holoScale(2,vec(0.1,0.1,0.2))
    holoPos(2,E:toWorld(vec(0,0,3)))
    holoParent(2,E)
    
    #Body
    holoCreate(3)
    holoModel(3,"hq_rcylinder_thick")
    holoScale(3,vec(0.3,0.5,0.7))
    holoPos(3,E:toWorld(vec(0,0,-2)))
    holoParent(3,E)
    
    #Left Sholder
    holoCreate(12)
    holoModel(12,"cube")
    holoScale(12,vec(0.1,0.1,0.1))
    holoPos(12,E:toWorld(vec(0,-1.6,0.5)))
    holoParent(12,E)
    
    #Right Sholder
    holoCreate(13)
    holoModel(13,"cube")
    holoScale(13,vec(0.1,0.1,0.1))
    holoPos(13,E:toWorld(vec(0,1.6,0.5)))
    holoParent(13,E)
    
    #Right Arm
    holoCreate(4)
    holoModel(4,"hq_cylinder")
    holoScale(4,vec(0.1,0.1,0.5))
    holoPos(4,E:toWorld(vec(-0.5,3.5,-2)))
    holoAng(4,ang(15,0,35))
    holoParent(4,13)
    
    #Left Arm
    holoCreate(5)
    holoModel(5,"hq_cylinder")
    holoScale(5,vec(0.1,0.1,0.5))
    holoPos(5,E:toWorld(vec(-0.5,-3.5,-2)))
    holoAng(5,ang(15,0,-35))
    holoParent(5,12)
    
    #Right Hip
    holoCreate(10)
    holoModel(10,"cube")
    holoScale(10,vec(0.1,0.1,0.1))
    holoPos(10,E:toWorld(vec(0,1.2,-5)))
    holoParent(10,E)
    
    #Left Hip
    holoCreate(11)
    holoModel(11,"cube")
    holoScale(11,vec(0.1,0.1,0.1))
    holoPos(11,E:toWorld(vec(0,-1.2,-5)))
    holoParent(11,E)
    
    #Right Leg
    holoCreate(6)
    holoModel(6,"hq_cylinder")
    holoScale(6,vec(0.1,0.1,0.5))
    holoPos(6,E:toWorld(vec(0,1.2,-8)))
    holoParent(6,10)
    
    #Left Leg
    holoCreate(7)
    holoModel(7,"hq_cylinder")
    holoScale(7,vec(0.1,0.1,0.5))
    holoPos(7,E:toWorld(vec(0,-1.2,-8)))
    holoParent(7,11)
    
    #Right Shoe
    holoCreate(8)
    holoModel(8,"models/props_junk/Shoe001a.mdl")
    holoScale(8,vec(0.3,0.3,0.3))
    holoPos(8,E:toWorld(vec(-1,1.4,-10)))
    holoAng(8,ang(0,180,0))
    holoParent(8,6)
    
    #Left Shoe
    holoCreate(9)
    holoModel(9,"models/props_junk/Shoe001a.mdl")
    holoScale(9,vec(0.3,0.3,0.3))
    holoPos(9,E:toWorld(vec(-1,-1,-10)))
    holoAng(9,ang(0,180,0))
    holoParent(9,7)
    
    holoCreate(14)
    holoPos(14,E:toWorld(vec(14,0,10)))
    holoParent(14,E)
    holoAlpha(14,0)
}

#Leg Movement
if(E:vel():length() > 40){
    Timer += E:vel():length()/5
    holoAng(11,(holoEntity(11):angles():rotateAroundAxis(E:right(),sin(Timer)*7)))
    holoAng(10,(holoEntity(10):angles():rotateAroundAxis(E:right(),-sin(Timer)*7)))
} else {
    holoAng(11,ang(E:angles():roll(),E:angles():yaw(),E:angles():pitch()))
    holoAng(10,ang(E:angles():roll(),E:angles():yaw(),E:angles():pitch()))
}
#Arm Movement
if(E:vel():length() > 40){
    Timer += E:vel():length()/5
    holoAng(13,(holoEntity(11):angles():rotateAroundAxis(E:right(),sin(Timer)*7)))
    holoAng(12,(holoEntity(10):angles():rotateAroundAxis(E:right(),-sin(Timer)*7)))
} else {
    holoAng(13,ang(E:angles():roll(),E:angles():yaw(),E:angles():pitch()))
    holoAng(12,ang(E:angles():roll(),E:angles():yaw(),E:angles():pitch()))
}

local Seat = Pod["Entity",entity]
local Driver = Seat:driver()
local W = Driver:keyForward()
local A = Driver:keyLeft()
local S = Driver:keyBack()
local D = Driver:keyRight()
local Mouse1 = Driver:keyAttack1()
local Mouse2 = Driver:keyAttack2()
local Space = Driver:keyJump()
local Shift = Driver:keySprint()
local Active = Driver:isPlayer()
Cam["Activated", number] = Active
Cam["Parent", entity] = holoEntity(14)
Cam["Position", vector] = holoEntity(14):pos()
Cam["Angle", angle] = holoEntity(14):angles()+ang(0,180,0)
