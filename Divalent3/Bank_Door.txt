@name Bank Door
@inputs Prop:entity 
@outputs User:entity 
@persist 
@trigger 
interval(80)

findByClass("Player")
User=findClosest(entity():pos())
E=entity():isWeldedTo()
Use=User:aimEntity() == Prop & User:keyUse()

if (changed(Use) & Use) {
    Prop:propNotSolid(1)
    timer("open",5000)
}

if (clk("open")) {
    Prop:propNotSolid(0)
}
