@name Turret Animation
@inputs 
@outputs 
@persist
@trigger

if (first()) {
    E = entity()
    
    holoCreate(1)
    holoModel(1,"models/buildables/sentry3_heavy.mdl")
    holoPos(1,E:toWorld(vec(0,0,0)))
    holoAng(1,E:toWorld(ang(0,0,0)))
}
 
holoAnim(1,0)
