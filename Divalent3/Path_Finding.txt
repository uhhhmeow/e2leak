@name Path Finding
@inputs 
@outputs I 
@persist 
@trigger 
runOnTick(1)
runOnChat(1)

if (chatClk(owner()) & owner():lastSaid():lower():find("!add")) {
    hideChat(1)
    holoCreate(I)
    holoPos(I,owner():aimPos())
}
