@name WaterLand
@inputs
@outputs
@persist I M E:entity
interval(100)

if (dupefinished()) {reset()}

if (first()) {
    M = 90
    E = entity()
}

if (I < M & holoCanCreate()) {
    I++

    holoCreate(I)
    holoParent(I,E)

    holoModel(1,"models/sprops/rectangles/size_1_5/rect_6x6x3.mdl")
    holoPos(1,E:toWorld(vec(16.5169,57.013,8.7188)))
    holoAng(1,E:toWorld(ang(90,-180,180)))
    holoMaterial(1,"models/props_canal/canal_bridge_railing_01c")

    holoModel(2,"models/sprops/trans/lights/light_b2.mdl")
    holoPos(2,E:toWorld(vec(-26.9821,-59.8464,4.9365)))
    holoAng(2,E:toWorld(ang(0,-90,90)))
    holoColor(2,vec(72,72,72))

    holoModel(3,"models/sprops/cuboids/height06/size_6/cube_36x108x6.mdl")
    holoPos(3,E:toWorld(vec(0.0169,0.0129,-3.2812)))
    holoAng(3,E:toWorld(ang(0,90,0)))
    holoMaterial(3,"models/props_canal/canal_bridge_railing_01c")

    holoModel(4,"models/sprops/rectangles/size_1/rect_3x42x3.mdl")
    holoPos(4,E:toWorld(vec(0.0169,-58.0891,0.667)))
    holoAng(4,E:toWorld(ang(0,-180,-71.543)))
    holoMaterial(4,"models/props_canal/canal_bridge_railing_01c")

    holoModel(5,"models/sprops/rectangles/size_1_5/rect_6x12x3.mdl")
    holoPos(5,E:toWorld(vec(-26.9821,-58.256,4.9365)))
    holoAng(5,E:toWorld(ang(71.5637,-90,-180)))
    holoMaterial(5,"models/props_canal/canal_bridge_railing_01c")

    holoModel(6,"models/sprops/rectangles_thin/size_2_5/rect_18x18x1_5.mdl")
    holoPos(6,E:toWorld(vec(-9.8034,-21.8733,13.4229)))
    holoAng(6,E:toWorld(ang(75.0146,90,0)))
    holoMaterial(6,"models/props_c17/furniturefabric001a")

    holoModel(7,"models/sprops/trans/stwheels/stwheel_6.mdl")
    holoPos(7,E:toWorld(vec(-9.8034,4.8704,22.3701)))
    holoAng(7,E:toWorld(ang(-25,-90,0)))
    

    holoModel(8,"models/sprops/rectangles/size_2/rect_12x42x3.mdl")
    holoPos(8,E:toWorld(vec(-23.9821,0.0129,-1.7812)))
    holoAng(8,E:toWorld(ang(0,-90,0)))
    holoMaterial(8,"models/props_canal/canal_bridge_railing_01c")

    holoModel(9,"models/sprops/rectangles/size_1_5/rect_6x36x3.mdl")
    holoPos(9,E:toWorld(vec(-26.9821,-39.7814,9.6035)))
    holoAng(9,E:toWorld(ang(0,90,0)))
    holoMaterial(9,"models/props_canal/canal_bridge_railing_01c")

    holoModel(10,"models/sprops/rectangles/size_1_5/rect_6x12x3.mdl")
    holoPos(10,E:toWorld(vec(-26.9821,-21.3073,4.9365)))
    holoAng(10,E:toWorld(ang(71.5667,90,180)))
    holoMaterial(10,"models/props_canal/canal_bridge_railing_01c")

    holoModel(11,"models/sprops/rectangles/size_2/rect_12x36x3.mdl")
    holoPos(11,E:toWorld(vec(-23.9821,39.8089,9.6025)))
    holoAng(11,E:toWorld(ang(0,90,0)))
    holoMaterial(11,"models/props_canal/canal_bridge_railing_01c")

    holoModel(12,"models/sprops/rectangles/size_1_5/rect_6x12x3.mdl")
    holoPos(12,E:toWorld(vec(-23.9821,57.3353,7.7822)))
    holoAng(12,E:toWorld(ang(-0.0009,179.9972,71.5458)))
    holoMaterial(12,"models/props_canal/canal_bridge_railing_01c")

    holoModel(13,"models/sprops/rectangles/size_1_5/rect_6x12x3.mdl")
    holoPos(13,E:toWorld(vec(-26.9821,21.3333,4.9365)))
    holoAng(13,E:toWorld(ang(71.5667,-89.9999,-180)))
    holoMaterial(13,"models/props_canal/canal_bridge_railing_01c")

    holoModel(14,"models/sprops/rectangles/size_1_5/rect_6x6x3.mdl")
    holoPos(14,E:toWorld(vec(-16.4831,57.013,8.7188)))
    holoAng(14,E:toWorld(ang(90,-0.0001,180)))
    holoMaterial(14,"models/props_canal/canal_bridge_railing_01c")

    holoModel(15,"models/sprops/rectangles/size_1_5/rect_6x42x3.mdl")
    holoPos(15,E:toWorld(vec(0.016,-36.1042,14.3672)))
    holoAng(15,E:toWorld(ang(-0.0162,-179.9974,65.4372)))
    holoMaterial(15,"models/props_canal/canal_bridge_railing_01c")

    holoModel(16,"models/sprops/triangles/right/size_0_5/rtri_6x12.mdl")
    holoPos(16,E:toWorld(vec(-16.4831,57.013,-0.2812)))
    holoAng(16,E:toWorld(ang(90,-90,180)))
    holoMaterial(16,"models/props_canal/canal_bridge_railing_01c")

    holoModel(17,"models/sprops/rectangles/size_2_5/rect_18x30x3.mdl")
    holoPos(17,E:toWorld(vec(-22.4831,-38.987,8.7188)))
    holoAng(17,E:toWorld(ang(0,90,-90)))
    holoMaterial(17,"models/props_canal/canal_bridge_railing_01c")

    holoModel(18,"models/sprops/triangles/right/size_0/rtri_3x6.mdl")
    holoPos(18,E:toWorld(vec(-22.4831,19.513,14.7188)))
    holoAng(18,E:toWorld(ang(-90,-90,180)))
    holoMaterial(18,"models/props_canal/canal_bridge_railing_01c")

    holoModel(19,"models/sprops/triangles/right/size_0_5/rtri_6x12.mdl")
    holoPos(19,E:toWorld(vec(-22.4831,21.013,5.7188)))
    holoAng(19,E:toWorld(ang(90,-90,180)))
    holoMaterial(19,"models/props_canal/canal_bridge_railing_01c")

    holoModel(20,"models/sprops/rectangles/size_2/rect_12x42x3.mdl")
    holoPos(20,E:toWorld(vec(0.0169,21.3333,4.9365)))
    holoAng(20,E:toWorld(ang(0,0,-108.4326)))
    holoMaterial(20,"models/props_canal/canal_bridge_railing_01c")

    holoModel(21,"models/sprops/rectangles/size_2_5/rect_18x18x3.mdl")
    holoPos(21,E:toWorld(vec(-9.8034,-12.8171,3.4238)))
    holoAng(21,E:toWorld(ang(0,-180,0)))
    holoMaterial(21,"models/props_c17/furniturefabric001a")

    holoModel(22,"models/sprops/triangles/right/size_0/rtri_3x6.mdl")
    holoPos(22,E:toWorld(vec(-16.4831,22.513,2.7188)))
    holoAng(22,E:toWorld(ang(-90,-90,180)))
    holoMaterial(22,"models/props_canal/canal_bridge_railing_01c")

    holoModel(23,"models/sprops/rectangles/size_2/rect_12x30x3.mdl")
    holoPos(23,E:toWorld(vec(-16.4831,39.013,5.7188)))
    holoAng(23,E:toWorld(ang(0,90,-90)))
    holoMaterial(23,"models/props_canal/canal_bridge_railing_01c")

    holoModel(24,"models/sprops/rectangles_thin/size_2/rect_12x42x1_5.mdl")
    holoPos(24,E:toWorld(vec(0.015,-28.5939,12.2617)))
    holoAng(24,E:toWorld(ang(0,-180,0)))
    holoMaterial(24,"models/props_building_details/courtyard_template001c_bars")

    holoModel(25,"models/sprops/rectangles/size_1/rect_3x42x3.mdl")
    holoPos(25,E:toWorld(vec(0.0169,16.9603,32.7109)))
    holoAng(25,E:toWorld(ang(0,180,-20.0046)))
    holoMaterial(25,"models/props_canal/canal_bridge_railing_01c")

    holoModel(26,"models/sprops/trans/lights/light_b3.mdl")
    holoPos(26,E:toWorld(vec(-23.9821,53.3084,17.1025)))
    holoAng(26,E:toWorld(ang(0,90,0)))
    holoColor(26,vec(72,72,72))

    holoModel(27,"models/sprops/rectangles/size_2/rect_12x42x3.mdl")
    holoPos(27,E:toWorld(vec(-22.4831,-2.987,5.7188)))
    holoAng(27,E:toWorld(ang(0,90,-90)))
    holoMaterial(27,"models/props_canal/canal_bridge_railing_01c")

    holoModel(28,"models/sprops/rectangles/size_1_5/rect_6x48x3.mdl")
    holoPos(28,E:toWorld(vec(0.0169,22.513,14.7188)))
    holoAng(28,E:toWorld(ang(0,180,-90)))
    holoMaterial(28,"models/props_canal/canal_bridge_railing_01c")

    holoModel(29,"models/sprops/prisms/tri/size_2/tprism_6x12.mdl")
    holoPos(29,E:toWorld(vec(-14.9831,-56.987,-3.2812)))
    holoAng(29,E:toWorld(ang(90,90,180)))
    holoMaterial(29,"models/props_canal/canal_bridge_railing_01c")

    holoModel(30,"models/sprops/rectangles/size_1/rect_3x42x3.mdl")
    holoPos(30,E:toWorld(vec(0.015,-34.9929,10.7617)))
    holoAng(30,E:toWorld(ang(0,0,0)))
    

    holoModel(31,"models/sprops/rectangles_thin/size_2_5/rect_18x42x1_5.mdl")
    holoPos(31,E:toWorld(vec(0.0169,19.9436,24.7725)))
    holoAng(31,E:toWorld(ang(0,-180,-110.0056)))
    holoMaterial(31,"models/props_combine/citadel_cable_b")

    holoModel(32,"models/sprops/trans/misc/gauge_1.mdl")
    holoPos(32,E:toWorld(vec(-14.9831,20.263,14.7188)))
    holoAng(32,E:toWorld(ang(-15,-90,0)))
    holoColor(32,vec(72,72,72))

    holoModel(33,"models/sprops/rectangles/size_1/rect_3x18x3.mdl")
    holoPos(33,E:toWorld(vec(-22.4831,19.5252,25.6631)))
    holoAng(33,E:toWorld(ang(70,90,0)))
    holoMaterial(33,"models/props_canal/canal_bridge_railing_01c")

    holoModel(34,"models/sprops/triangles/right/size_0/rtri_3x6.mdl")
    holoPos(34,E:toWorld(vec(-22.4831,-22.487,14.7188)))
    holoAng(34,E:toWorld(ang(-90,90,180)))
    holoMaterial(34,"models/props_canal/canal_bridge_railing_01c")

    holoModel(35,"models/sprops/rectangles/size_1/rect_3x12x3.mdl")
    holoPos(35,E:toWorld(vec(-22.4831,21.3333,4.9365)))
    holoAng(35,E:toWorld(ang(-71.5678,89.9905,-179.9906)))
    holoMaterial(35,"models/props_canal/canal_bridge_railing_01c")

    holoModel(36,"models/sprops/rectangles/size_2_5/rect_18x42x3.mdl")
    holoPos(36,E:toWorld(vec(0.0169,-55.4064,8.7061)))
    holoAng(36,E:toWorld(ang(0,0,-108.4131)))
    holoMaterial(36,"models/props_canal/canal_bridge_railing_01c")

    holoModel(37,"models/sprops/cylinders/size_1/cylinder_1_5x6.mdl")
    holoPos(37,E:toWorld(vec(-23.9821,52.5584,14.1025)))
    holoAng(37,E:toWorld(ang(0,90,0)))
    holoMaterial(37,"models/props_canal/metalwall005b")

    holoModel(38,"models/sprops/triangles/right/size_0_5/rtri_6x18.mdl")
    holoPos(38,E:toWorld(vec(-22.4831,-56.987,8.7188)))
    holoAng(38,E:toWorld(ang(-90,-90,180)))
    holoMaterial(38,"models/props_canal/canal_bridge_railing_01c")

    holoModel(39,"models/sprops/triangles/right/size_0_5/rtri_6x6.mdl")
    holoPos(39,E:toWorld(vec(-22.4831,-56.987,-3.2812)))
    holoAng(39,E:toWorld(ang(90,90,180)))
    holoMaterial(39,"models/props_canal/canal_bridge_railing_01c")

    holoModel(40,"models/sprops/rectangles_thin/size_0/rect_1_5x6x1_5.mdl")
    holoPos(40,E:toWorld(vec(-23.9821,55.2049,17.1025)))
    holoAng(40,E:toWorld(ang(0,0,0)))
    holoMaterial(40,"models/props_canal/metalwall005b")

    holoModel(41,"models/sprops/rectangles/size_1_5/rect_6x36x3.mdl")
    holoPos(41,E:toWorld(vec(-20.984,-35.9869,-1.7812)))
    holoAng(41,E:toWorld(ang(0,90,0)))
    holoMaterial(41,"models/props_canal/canal_bridge_railing_01c")

    holoModel(42,"models/sprops/rectangles/size_2_5/rect_18x42x3.mdl")
    holoPos(42,E:toWorld(vec(0.0169,-44.987,16.2188)))
    holoAng(42,E:toWorld(ang(0,180,0)))
    holoMaterial(42,"models/props_canal/canal_bridge_railing_01c")

    holoModel(43,"models/sprops/triangles/right/size_0_5/rtri_6x36.mdl")
    holoPos(43,E:toWorld(vec(-16.4831,42.013,14.7188)))
    holoAng(43,E:toWorld(ang(0,90,0)))
    holoMaterial(43,"models/props_canal/canal_bridge_railing_01c")

    holoModel(44,"models/sprops/rectangles/size_1/rect_3x42x3.mdl")
    holoPos(44,E:toWorld(vec(0.0169,22.513,10.2188)))
    holoAng(44,E:toWorld(ang(0,-180,0)))
    holoMaterial(44,"models/props_canal/canal_bridge_railing_01c")

    holoModel(45,"models/sprops/rectangles/size_2/rect_12x42x3.mdl")
    holoPos(45,E:toWorld(vec(0.015,-34.9929,3.2617)))
    holoAng(45,E:toWorld(ang(0,0,-90)))
    

    holoModel(46,"models/sprops/rectangles_thin/size_2/rect_12x42x1_5.mdl")
    holoPos(46,E:toWorld(vec(0.015,-23.3439,5.5117)))
    holoAng(46,E:toWorld(ang(0,180,90)))
    holoMaterial(46,"models/props_building_details/courtyard_template001c_bars")

    holoModel(47,"models/sprops/rectangles/size_2/rect_12x42x3.mdl")
    holoPos(47,E:toWorld(vec(24.0169,0.0129,-1.7812)))
    holoAng(47,E:toWorld(ang(0,-90,0)))
    holoMaterial(47,"models/props_canal/canal_bridge_railing_01c")

    holoModel(48,"models/sprops/rectangles/size_1_5/rect_6x36x3.mdl")
    holoPos(48,E:toWorld(vec(27.0169,-39.7814,9.6035)))
    holoAng(48,E:toWorld(ang(0,90,0)))
    holoMaterial(48,"models/props_canal/canal_bridge_railing_01c")

    holoModel(49,"models/sprops/rectangles/size_1_5/rect_6x12x3.mdl")
    holoPos(49,E:toWorld(vec(27.0169,-21.3073,4.9365)))
    holoAng(49,E:toWorld(ang(71.5667,90,-180)))
    holoMaterial(49,"models/props_canal/canal_bridge_railing_01c")

    holoModel(50,"models/sprops/rectangles_thin/size_1/rect_3x30x1_5.mdl")
    holoPos(50,E:toWorld(vec(0.0169,24.5052,16.1152)))
    holoAng(50,E:toWorld(ang(0,180,-80.5078)))
    holoMaterial(50,"models/props_canal/canal_bridge_railing_01c")

    holoModel(51,"models/sprops/triangles/right/size_0_5/rtri_6x12.mdl")
    holoPos(51,E:toWorld(vec(22.5169,21.013,5.7188)))
    holoAng(51,E:toWorld(ang(90,-90,180)))
    holoMaterial(51,"models/props_canal/canal_bridge_railing_01c")

    holoModel(52,"models/sprops/rectangles/size_2/rect_12x36x3.mdl")
    holoPos(52,E:toWorld(vec(24.0169,39.8089,9.6025)))
    holoAng(52,E:toWorld(ang(0,90,0)))
    holoMaterial(52,"models/props_canal/canal_bridge_railing_01c")

    holoModel(53,"models/sprops/rectangles/size_1_5/rect_6x12x3.mdl")
    holoPos(53,E:toWorld(vec(27.0169,21.3333,4.9365)))
    holoAng(53,E:toWorld(ang(71.5667,-90,-180)))
    holoMaterial(53,"models/props_canal/canal_bridge_railing_01c")

    holoModel(54,"models/sprops/rectangles/size_2_5/rect_18x30x3.mdl")
    holoPos(54,E:toWorld(vec(22.5169,-38.987,8.7188)))
    holoAng(54,E:toWorld(ang(0,90,90)))
    holoMaterial(54,"models/props_canal/canal_bridge_railing_01c")

    holoModel(55,"models/sprops/triangles/right/size_0_5/rtri_6x18.mdl")
    holoPos(55,E:toWorld(vec(22.5169,-56.987,8.7188)))
    holoAng(55,E:toWorld(ang(-90,-90,180)))
    holoMaterial(55,"models/props_canal/canal_bridge_railing_01c")

    holoModel(56,"models/sprops/rectangles/size_1_5/rect_6x12x3.mdl")
    holoPos(56,E:toWorld(vec(27.0169,-58.256,4.9365)))
    holoAng(56,E:toWorld(ang(71.5637,-90,-180)))
    holoMaterial(56,"models/props_canal/canal_bridge_railing_01c")

    holoModel(57,"models/sprops/geometry/t_qdisc_12.mdl")
    holoPos(57,E:toWorld(vec(0.0169,-62.987,-1.7812)))
    holoAng(57,E:toWorld(ang(0,165,0)))
    holoMaterial(57,"models/props_c17/metalladder001")

    holoModel(58,"models/sprops/geometry/t_qdisc_12.mdl")
    holoPos(58,E:toWorld(vec(-3.8805,-62.987,-8.5312)))
    holoAng(58,E:toWorld(ang(56.7741,28.1868,-155.8539)))
    holoMaterial(58,"models/props_c17/metalladder001")

    holoModel(59,"models/sprops/rectangles/size_1/rect_3x18x3.mdl")
    holoPos(59,E:toWorld(vec(22.5169,19.5252,25.6631)))
    holoAng(59,E:toWorld(ang(70,90,0)))
    holoMaterial(59,"models/props_canal/canal_bridge_railing_01c")

    holoModel(60,"models/sprops/rectangles/size_2/rect_12x42x3.mdl")
    holoPos(60,E:toWorld(vec(22.5169,-2.987,5.7188)))
    holoAng(60,E:toWorld(ang(0,90,90)))
    holoMaterial(60,"models/props_canal/canal_bridge_railing_01c")

    holoModel(61,"models/sprops/rectangles_thin/size_2_5/rect_18x18x1_5.mdl")
    holoPos(61,E:toWorld(vec(9.8382,-21.8733,13.4229)))
    holoAng(61,E:toWorld(ang(75.0146,90,0)))
    holoMaterial(61,"models/props_c17/furniturefabric001a")

    holoModel(62,"models/sprops/trans/lights/light_b2.mdl")
    holoPos(62,E:toWorld(vec(27.0169,-59.8464,4.9365)))
    holoAng(62,E:toWorld(ang(0,-90,-90)))
    holoColor(62,vec(72,72,72))

    holoModel(63,"models/sprops/rectangles/size_1_5/rect_6x12x3.mdl")
    holoPos(63,E:toWorld(vec(24.0169,57.3353,7.7822)))
    holoAng(63,E:toWorld(ang(-0.0009,0.0027,-71.5458)))
    holoMaterial(63,"models/props_canal/canal_bridge_railing_01c")

    holoModel(64,"models/sprops/geometry/t_qdisc_12.mdl")
    holoPos(64,E:toWorld(vec(3.9144,-62.987,-8.5312)))
    holoAng(64,E:toWorld(ang(-56.7741,28.1868,155.8539)))
    holoMaterial(64,"models/props_c17/metalladder001")

    holoModel(65,"models/sprops/triangles/right/size_0/rtri_3x6.mdl")
    holoPos(65,E:toWorld(vec(16.5169,22.513,2.7188)))
    holoAng(65,E:toWorld(ang(-90,-90,180)))
    holoMaterial(65,"models/props_canal/canal_bridge_railing_01c")

    holoModel(66,"models/sprops/cylinders/size_3/cylinder_6x3.mdl")
    holoPos(66,E:toWorld(vec(0.0169,-62.987,-6.2812)))
    holoAng(66,E:toWorld(ang(90,90,180)))
    holoMaterial(66,"models/props_canal/metalwall005b")

    holoModel(67,"models/sprops/trans/wheel_c/t_wheel30.mdl")
    holoPos(67,E:toWorld(vec(0.0169,42.8763,18.3828)))
    holoAng(67,E:toWorld(ang(0,-180,99.4712)))
    

    holoModel(68,"models/sprops/trans/lights/light_b3.mdl")
    holoPos(68,E:toWorld(vec(24.0169,53.3084,17.1025)))
    holoAng(68,E:toWorld(ang(0,90,0)))
    holoColor(68,vec(72,72,72))

    holoModel(69,"models/sprops/rectangles_thin/size_2/rect_12x24x1_5.mdl")
    holoPos(69,E:toWorld(vec(0.0169,30.6536,8.6543)))
    holoAng(69,E:toWorld(ang(0,0,0)))
    

    holoModel(70,"models/sprops/trans/misc/gauge_1.mdl")
    holoPos(70,E:toWorld(vec(-4.4821,20.263,14.7188)))
    holoAng(70,E:toWorld(ang(-15,-90,0)))
    holoColor(70,vec(72,72,72))

    holoModel(71,"models/sprops/rectangles/size_2/rect_12x30x3.mdl")
    holoPos(71,E:toWorld(vec(0.0169,55.9881,1.0225)))
    holoAng(71,E:toWorld(ang(0,180,-63.4377)))
    holoMaterial(71,"models/props_canal/canal_bridge_railing_01c")

    holoModel(72,"models/sprops/rectangles/size_1_5/rect_6x36x3.mdl")
    holoPos(72,E:toWorld(vec(21.0169,-35.9869,-1.7812)))
    holoAng(72,E:toWorld(ang(0,90,0)))
    holoMaterial(72,"models/props_canal/canal_bridge_railing_01c")

    holoModel(73,"models/sprops/triangles/right/size_0_5/rtri_6x36.mdl")
    holoPos(73,E:toWorld(vec(16.5169,42.013,14.7188)))
    holoAng(73,E:toWorld(ang(0,90,0)))
    holoMaterial(73,"models/props_canal/canal_bridge_railing_01c")

    holoModel(74,"models/sprops/triangles/right/size_0/rtri_3x6.mdl")
    holoPos(74,E:toWorld(vec(22.5169,19.513,14.7188)))
    holoAng(74,E:toWorld(ang(-90,-90,180)))
    holoMaterial(74,"models/props_canal/canal_bridge_railing_01c")

    holoModel(75,"models/sprops/rectangles/size_2_5/rect_18x18x3.mdl")
    holoPos(75,E:toWorld(vec(9.8382,-12.8171,3.4238)))
    holoAng(75,E:toWorld(ang(0,0,0)))
    holoMaterial(75,"models/props_c17/furniturefabric001a")

    holoModel(76,"models/sprops/rectangles/size_1/rect_3x30x3.mdl")
    holoPos(76,E:toWorld(vec(0.0169,53.3421,-4.2686)))
    holoAng(76,E:toWorld(ang(0,180,26.5603)))
    holoMaterial(76,"models/props_canal/canal_bridge_railing_01c")

    holoModel(77,"models/sprops/triangles/right/size_0/rtri_3x6.mdl")
    holoPos(77,E:toWorld(vec(22.5169,-22.487,14.7188)))
    holoAng(77,E:toWorld(ang(-90,90,180)))
    holoMaterial(77,"models/props_canal/canal_bridge_railing_01c")

    holoModel(78,"models/sprops/cylinders/size_1/cylinder_1_5x18.mdl")
    holoPos(78,E:toWorld(vec(-9.8034,14.2147,18.0127)))
    holoAng(78,E:toWorld(ang(-64.9988,90,0)))
    holoMaterial(78,"models/props_canal/canal_bridge_railing_01c")

    holoModel(79,"models/sprops/cuboids/height06/size_1/cube_6x18x6.mdl")
    holoPos(79,E:toWorld(vec(0.0169,-56.987,-3.2812)))
    holoAng(79,E:toWorld(ang(0,-180,0)))
    holoMaterial(79,"models/props_canal/canal_bridge_railing_01c")

    holoModel(80,"models/bull/gates/capacitor_nano.mdl")
    holoPos(80,E:toWorld(vec(-8.0348,20.9818,16.2939)))
    holoAng(80,E:toWorld(ang(70,-90,0)))
    

    holoModel(81,"models/sprops/trans/misc/gauge_1.mdl")
    holoPos(81,E:toWorld(vec(-8.9831,20.263,11.7188)))
    holoAng(81,E:toWorld(ang(-15,-90,0)))
    holoColor(81,vec(72,72,72))

    holoModel(82,"models/sprops/rectangles/size_3_5/rect_30x36x3.mdl")
    holoPos(82,E:toWorld(vec(0.0169,42.0115,13.2012)))
    holoAng(82,E:toWorld(ang(9.4712,89.9998,0)))
    holoMaterial(82,"models/props_canal/canal_bridge_railing_01c")

    holoModel(83,"models/sprops/rectangles_thin/size_0/rect_1_5x6x1_5.mdl")
    holoPos(83,E:toWorld(vec(24.0169,55.2049,17.1025)))
    holoAng(83,E:toWorld(ang(0,-180,0)))
    holoMaterial(83,"models/props_canal/metalwall005b")

    holoModel(84,"models/sprops/prisms/tri/size_2/tprism_6x12.mdl")
    holoPos(84,E:toWorld(vec(15.0179,-56.987,-3.2812)))
    holoAng(84,E:toWorld(ang(90,90,180)))
    holoMaterial(84,"models/props_canal/canal_bridge_railing_01c")

    holoModel(85,"models/sprops/rectangles/size_1_5/rect_6x30x3.mdl")
    holoPos(85,E:toWorld(vec(0.0169,58.513,8.7188)))
    holoAng(85,E:toWorld(ang(0,0,-90)))
    holoMaterial(85,"models/props_canal/canal_bridge_railing_01c")

    holoModel(86,"models/sprops/rectangles/size_2/rect_12x30x3.mdl")
    holoPos(86,E:toWorld(vec(16.5169,39.013,5.7188)))
    holoAng(86,E:toWorld(ang(0,90,90)))
    holoMaterial(86,"models/props_canal/canal_bridge_railing_01c")

    holoModel(87,"models/sprops/triangles/right/size_0_5/rtri_6x12.mdl")
    holoPos(87,E:toWorld(vec(16.5169,57.013,-0.2812)))
    holoAng(87,E:toWorld(ang(90,-90,180)))
    holoMaterial(87,"models/props_canal/canal_bridge_railing_01c")

    holoModel(88,"models/sprops/triangles/right/size_0_5/rtri_6x6.mdl")
    holoPos(88,E:toWorld(vec(22.5169,-56.987,-3.2812)))
    holoAng(88,E:toWorld(ang(90,90,180)))
    holoMaterial(88,"models/props_canal/canal_bridge_railing_01c")

    holoModel(89,"models/sprops/cylinders/size_1/cylinder_1_5x6.mdl")
    holoPos(89,E:toWorld(vec(24.0169,52.5584,14.1025)))
    holoAng(89,E:toWorld(ang(0,90,0)))
    holoMaterial(89,"models/props_canal/metalwall005b")

    holoModel(90,"models/sprops/rectangles/size_1/rect_3x12x3.mdl")
    holoPos(90,E:toWorld(vec(22.5169,21.3333,4.9365)))
    holoAng(90,E:toWorld(ang(-71.5678,90.0095,179.9906)))
    holoMaterial(90,"models/props_canal/canal_bridge_railing_01c")

}