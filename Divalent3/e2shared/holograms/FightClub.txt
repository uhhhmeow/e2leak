@name FightClub
@inputs 
@outputs 
@persist [Prop1 Prop2 LastAlive T1 T2]:entity JoinClub1 JoinClub2 
@persist Health Armor 
interval(100)
runOnLast(1)

E = entity()

if (last()) {propDeleteAll()}

if (first()) {
    local P = propSpawn("models/hunter/plates/plate7.mdl",vec(-2079.6499,-1760.0964,-288.2798),ang(90,90,180),1)  P:setMaterial("phoenix_storms/car_tire") P:setColor(vec4(255,255,255,255))
    local P = propSpawn("models/hunter/plates/plate7.mdl",vec(-2079.6499,-1760.0964,-301.7798),ang(90,90,180),1)  P:setMaterial("phoenix_storms/car_tire") P:setColor(vec4(255,255,255,255))
    local P = propSpawn("models/hunter/plates/plate7.mdl",vec(-2079.6499,-1760.0964,-315.2798),ang(90,90,180),1)  P:setMaterial("phoenix_storms/car_tire") P:setColor(vec4(255,255,255,255))
    local P = propSpawn("models/hunter/plates/plate7.mdl",vec(-2079.6499,-1760.0964,-369.2798),ang(90,90,180),1)  P:setMaterial("phoenix_storms/car_tire") P:setColor(vec4(255,255,255,255))
    local P = propSpawn("models/hunter/plates/plate7.mdl",vec(-2079.6499,-1760.0964,-355.7798),ang(90,90,180),1)  P:setMaterial("phoenix_storms/car_tire") P:setColor(vec4(255,255,255,255))
    local P = propSpawn("models/hunter/plates/plate7.mdl",vec(-2079.6499,-1760.0964,-342.2798),ang(90,90,180),1)  P:setMaterial("phoenix_storms/car_tire") P:setColor(vec4(255,255,255,255))
    local P = propSpawn("models/hunter/plates/plate7.mdl",vec(-2079.6499,-1760.0964,-328.7798),ang(90,90,180),1)  P:setMaterial("phoenix_storms/car_tire") P:setColor(vec4(255,255,255,255))
    local P = propSpawn("models/hunter/plates/plate7.mdl",vec(-1892.8499,-1573.2964,-369.2798),ang(90,-180,180),1)  P:setMaterial("phoenix_storms/car_tire") P:setColor(vec4(255,255,255,255))
    local P = propSpawn("models/hunter/plates/plate7.mdl",vec(-1892.8499,-1573.2964,-355.7798),ang(90,-180,180),1)  P:setMaterial("phoenix_storms/car_tire") P:setColor(vec4(255,255,255,255))
    local P = propSpawn("models/hunter/plates/plate7.mdl",vec(-1892.8499,-1573.2964,-342.2798),ang(90,-180,180),1)  P:setMaterial("phoenix_storms/car_tire") P:setColor(vec4(255,255,255,255))
    local P = propSpawn("models/hunter/plates/plate7.mdl",vec(-1892.8499,-1573.2964,-288.2798),ang(90,-180,180),1)  P:setMaterial("phoenix_storms/car_tire") P:setColor(vec4(255,255,255,255))
    local P = propSpawn("models/hunter/plates/plate7.mdl",vec(-1892.8499,-1573.2964,-301.7798),ang(90,-180,180),1)  P:setMaterial("phoenix_storms/car_tire") P:setColor(vec4(255,255,255,255))
    local P = propSpawn("models/hunter/plates/plate7.mdl",vec(-1892.8499,-1573.2964,-315.2798),ang(90,-180,180),1)  P:setMaterial("phoenix_storms/car_tire") P:setColor(vec4(255,255,255,255))
    local P = propSpawn("models/hunter/plates/plate7.mdl",vec(-1892.8499,-1573.2964,-328.7798),ang(90,-180,180),1)  P:setMaterial("phoenix_storms/car_tire") P:setColor(vec4(255,255,255,255))
    local P = propSpawn("models/hunter/plates/plate7.mdl",vec(-2079.6499,-1384.9963,-289.7798),ang(0,90,0),1)  P:setMaterial("phoenix_storms/car_tire") P:setColor(vec4(255,255,255,255))
    local P = propSpawn("models/hunter/plates/plate7.mdl",vec(-2079.6499,-1384.9963,-303.2798),ang(0,90,0),1)  P:setMaterial("phoenix_storms/car_tire") P:setColor(vec4(255,255,255,255))
    local P = propSpawn("models/hunter/plates/plate7.mdl",vec(-2079.6499,-1384.9963,-316.7798),ang(0,90,0),1)  P:setMaterial("phoenix_storms/car_tire") P:setColor(vec4(255,255,255,255))
    local P = propSpawn("models/hunter/plates/plate7.mdl",vec(-2079.6499,-1384.9963,-370.7799),ang(0,90,0),1)  P:setMaterial("phoenix_storms/car_tire") P:setColor(vec4(255,255,255,255))
    local P = propSpawn("models/hunter/plates/plate7.mdl",vec(-2079.6499,-1384.9963,-357.2799),ang(0,90,0),1)  P:setMaterial("phoenix_storms/car_tire") P:setColor(vec4(255,255,255,255))
    local P = propSpawn("models/hunter/plates/plate7.mdl",vec(-2079.6499,-1384.9963,-343.7799),ang(0,90,0),1)  P:setMaterial("phoenix_storms/car_tire") P:setColor(vec4(255,255,255,255))
    local P = propSpawn("models/hunter/plates/plate7.mdl",vec(-2079.6499,-1384.9963,-330.2798),ang(0,90,0),1)  P:setMaterial("phoenix_storms/car_tire") P:setColor(vec4(255,255,255,255))
    local P = propSpawn("models/hunter/plates/plate7.mdl",vec(-2267.95,-1573.2964,-370.7798),ang(0,-180,0),1)  P:setMaterial("phoenix_storms/car_tire") P:setColor(vec4(255,255,255,255))
    local P = propSpawn("models/hunter/plates/plate7.mdl",vec(-2267.95,-1573.2964,-357.2798),ang(0,-180,0),1)  P:setMaterial("phoenix_storms/car_tire") P:setColor(vec4(255,255,255,255))
    local P = propSpawn("models/hunter/plates/plate7.mdl",vec(-2267.95,-1573.2964,-343.7798),ang(0,-180,0),1)  P:setMaterial("phoenix_storms/car_tire") P:setColor(vec4(255,255,255,255))
    local P = propSpawn("models/hunter/plates/plate7.mdl",vec(-2267.95,-1573.2964,-289.7798),ang(0,-180,0),1)  P:setMaterial("phoenix_storms/car_tire") P:setColor(vec4(255,255,255,255))
    local P = propSpawn("models/hunter/plates/plate7.mdl",vec(-2267.95,-1573.2964,-303.2798),ang(0,-180,0),1)  P:setMaterial("phoenix_storms/car_tire") P:setColor(vec4(255,255,255,255))
    local P = propSpawn("models/hunter/plates/plate7.mdl",vec(-2267.95,-1573.2964,-316.7798),ang(0,-180,0),1)  P:setMaterial("phoenix_storms/car_tire") P:setColor(vec4(255,255,255,255))
    local P = propSpawn("models/hunter/plates/plate7.mdl",vec(-2267.95,-1573.2964,-330.2798),ang(0,180,0),1)  P:setMaterial("phoenix_storms/car_tire") P:setColor(vec4(255,255,255,255))
    local P = propSpawn("models/hunter/blocks/cube05x2x05.mdl",vec(-2257.5874,-1751.2339,-328.7798),ang(0,180,90),1)  P:setMaterial("phoenix_storms/Indenttiles_1-2") P:setColor(vec4(255,255,255,255))
    local P = propSpawn("models/hunter/blocks/cube05x2x05.mdl",vec(-1901.7124,-1751.2339,-328.7799),ang(0,180,90),1)  P:setMaterial("phoenix_storms/Indenttiles_1-2") P:setColor(vec4(255,255,255,255))
    local P = propSpawn("models/hunter/blocks/cube05x2x05.mdl",vec(-2257.5874,-1395.3589,-328.7798),ang(0,180,90),1)  P:setMaterial("phoenix_storms/Indenttiles_1-2") P:setColor(vec4(255,255,255,255))
    local P = propSpawn("models/hunter/blocks/cube05x2x05.mdl",vec(-1901.7124,-1395.3589,-328.7799),ang(0,-180,90),1)  P:setMaterial("phoenix_storms/Indenttiles_1-2") P:setColor(vec4(255,255,255,255))
    local P = propSpawn("models/hunter/blocks/cube8x8x1.mdl",vec(-2079.6499,-1573.2964,-399.9549),ang(0,90,0),1)  P:setMaterial("sprops/textures/sprops_metal6") P:setColor(vec4(182,182,182,255))
    
    JoinClub1 = 1
    JoinClub2 = 1
    
    concmd("say /advert Fight club is now open! First two people to type either '!join 1' or '!join 2' get in. (Don't join with weapons)")
}

runOnChat(1)

if (chatClk()) {
    LS = lastSaid():lower()
    LE = lastSpoke()
    
    if (LS == "!join 1" & JoinClub1 == 1) {
        hideChat(1)
        JoinClub1 = 0
        
        T1 = findPlayerByName(LE:name())
        
        Health = LE:health()
        Armor = LE:armor()
        
        LE:plySetPos(vec(-2221.969,-1430.960,-375.937))
        LE:plyGod(0)
        LE:plyNoclip(0)
        LE:plyStripWeapons()
        LE:plyGive("weapon_fists")
        LE:plySetHealth(100)
        LE:plySetArmor(0)
        Prop1 = propSpawn("models/hunter/blocks/cube1x2x1.mdl",vec(-2221.969,-1430.960,-305.937),ang(0,0,90),1)
        Prop1:setColor(vec4(0))
    } 
    
    if (LS == "!join 2" & JoinClub2 == 1) {
        hideChat(1)
        JoinClub2 = 0
        
        T2 = findPlayerByName(LE:name())
        
        Health = LE:health()
        Armor = LE:armor()
        
        LE:plySetPos(vec(-1937.250,-1715.625,-375.937))
        LE:plyGod(0)
        LE:plyNoclip(0)
        LE:plyStripWeapons()
        LE:plyGive("weapon_fists")
        LE:plySetHealth(100)
        LE:plySetArmor(0)
        Prop2 = propSpawn("models/hunter/blocks/cube1x2x1.mdl",vec(-1937.250,-1715.625,-305.937),ang(0,0,90),1)
        Prop2:setColor(vec4(0))
    }
}

if (JoinClub1 == 0 & JoinClub2 == 0) {
    if (changed(!T1:isAlive()) & !T1:isAlive()) {
        T2:plySetPos(entity():pos())
        timer("Reset",2000)
    } elseif (changed(!T2:isAlive()) & !T2:isAlive()) {
        T1:plySetPos(entity():pos())
        timer("Reset",2000)
    }
    timer("Start",5000)
}

if (clk("Start")) {Prop1:propDelete() Prop2:propDelete()}
if (clk("Reset")) {reset()}
