@name 2
@inputs
@outputs
@persist I M E:entity
interval(100)

if (dupefinished()) {reset()}

if (first()) {
    M = 3
    E = entity()
}

if (I < M & holoCanCreate()) {
    I++

    holoCreate(I)
    holoParent(I,E)

    holoModel(1,"models/props_c17/furniturecouch002a.mdl")
    holoPos(1,E:toWorld(vec(9.2222,-17.0106,56.5719)))
    holoAng(1,E:toWorld(ang(-1.3562,137.2249,-0.0126)))
    

    holoModel(2,"models/props_c17/furnitureboiler001a.mdl")
    holoPos(2,E:toWorld(vec(-60.3271,-22.2936,55.9449)))
    holoAng(2,E:toWorld(ang(0.0819,114.7883,-0.0084)))
    

    holoModel(3,"models/props_c17/furniturechair001a.mdl")
    holoPos(3,E:toWorld(vec(-7.0511,59.2435,27.3372)))
    holoAng(3,E:toWorld(ang(-0.0068,152.334,-0.0164)))
    

}