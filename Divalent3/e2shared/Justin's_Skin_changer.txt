@name Skin changer

It = owner()
interval(1)
runOnChat(1)
hideChat(0)
if (It:lastSaid() == "!solid") {
   It:setMaterial("models/debug/debugwhite")
}
if (It:lastSaid() == "!ghost") {
   It:setMaterial("models/effects/vol_light001")
}
if (It:lastSaid() == "!warning") {
   It:setMaterial("phoenix_storms/stripes")
}
if (It:lastSaid() == "!gel") {
   It:setMaterial("models/props_combine/com_shield001a")
}
if (It:lastSaid() == "!water") {
   It:setMaterial("models/shadertest/shader3")
}
if (It:lastSaid() == "!flesh") {
   It:setMaterial("models/flesh")
}
if (It:lastSaid() == "!space") {
   It:setMaterial("models/props_lab/warp_sheet")
}
if (It:lastSaid() == "!electro") {
   It:setMaterial("models/effects/comball_sphere")
}
if (It:lastSaid() == "!brick") {
   It:setMaterial("brick/brick_model")
}
if (It:lastSaid() == "!sun") {
   It:setMaterial("models/effects/comball_tape")
}
if (It:lastSaid() == "!brown") {
   It:setColor(127,95,0)
}
if (It:lastSaid() == "!orange") {
   It:setColor(255,191,0)
}
if (It:lastSaid() == "!blue") {
   It:setColor(0,255,255)
}
if (It:lastSaid() == "!green") {
   It:setColor(0,255,25)
}
if (It:lastSaid() == "!red") {
   It:setColor(255,0,0)
}
if (It:lastSaid() == "!yellow") {
   It:setColor(255,255,0)
}
if (It:lastSaid() == "!purple") {
   It:setColor(216,0,255)
}
if (It:lastSaid() == "!normal") {
   It:setMaterial("models/")
   It:weapon():setMaterial("models/")
   It:setColor(255,255,255)
}
if (It:lastSaid() == "!wire") {
   It:setMaterial("models/vortigaunt/pupil")
}
if (It:lastSaid() == "!gross") {
   It:setMaterial("models/props_lab/xencrystal_sheet")
}
if (It:lastSaid() == "!gold") {
   It:setMaterial("models/player/shared/gold_player")
}
if (It:lastSaid() == "!silver") {
   It:setMaterial("models/player/shared/ice_player")
}
if (It:lastSaid() == "!satan") {
   It:setMaterial("models/XQM/LightlinesRed_tool")
}
if (It:lastSaid() == "!money") {
   It:setMaterial("models/props/cs_assault/dollar")
}
if (It:lastSaid() == "!robot") {
   It:setMaterial("phoenix_storms/FuturisticTrackRamp_1-2")
}
if (It:lastSaid() == "!fence") {
   It:setMaterial("models/props_interiors/metalfence007a")
}
if (It:lastSaid() == "!wood") {
   It:setMaterial("phoenix_storms/fender_wood")
}
if (It:lastSaid() == "!half") {
   It:setMaterial("phoenix_storms/heli")
}
if (It:lastSaid() == "!cone") {
   It:setMaterial("models/props_junk/TrafficCone001a")
}
if (It:lastSaid() == "!rt") {
   It:setMaterial("models/rendertarget")
}
