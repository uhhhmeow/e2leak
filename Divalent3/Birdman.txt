@name Birdman
@inputs 
@outputs 
@persist 
@trigger 
interval(100)

if (first()) {
    holoCreate(1)
    holoModel(1,"models/crow.mdl")
    holoScale(1,vec(5,5,5))
    holoAnim(1,12)
    holoParent(1,entity())
    
    holoClipEnabled(1,1,1)
    holoClip(1,vec(0,0,19),vec(0,0,1),0)
    
    holoClipEnabled(1,2,1)
    holoClip(1,2,vec(-30,0,0),vec(1,0,0),0)
    
    holoClipEnabled(1,3,1)
    holoClip(1,3,vec(15,0,0),vec(-1,0,0),0)
    
    holoClipEnabled(1,4,1)
    holoClip(1,4,vec(0,0,38),vec(0,0,-1),0)
}

holoPos(1,owner():toWorld(vec(14,0,24)))
holoAng(1,owner():toWorld(ang(-40,0,0)))
