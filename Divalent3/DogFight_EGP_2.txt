@name DogFight EGP 2
@inputs EGP:wirelink
@outputs 
@persist 
@trigger 

#[
if (first()) {
    EGP:egpClear()
    EGP:egpBox(1,vec2(306,166),vec2(30,300))
    EGP:egpMaterial(1,"gui/center_gradient")
    EGP:egpColor(1,vec(100,100,100))
    EGP:egpAngle(1,90)
    
    EGP:egpText(2,"Shark Tooth",vec2(216,126))
    EGP:egpSize(2,35)
    EGP:egpColor(2,vec(0,0,0))
    
    EGP:egpText(5,"To go back to the choosing menu hit backspace.",vec2(158,386))
    EGP:egpSize(5,15)
    EGP:egpColor(5,vec(0,0,0))
}
]#
#[
if (first()) {
    EGP:egpClear()
    EGP:egpBox(1,vec2(306,166),vec2(30,300))
    EGP:egpMaterial(1,"gui/center_gradient")
    EGP:egpColor(1,vec(100,100,100))
    EGP:egpAngle(1,90)
    
    EGP:egpText(2,"The Majestic Beaver",vec2(156,126))
    EGP:egpSize(2,35)
    EGP:egpColor(2,vec(0,0,0))
    
    EGP:egpText(5,"To go back to the choosing menu hit backspace.",vec2(158,386))
    EGP:egpSize(5,15)
    EGP:egpColor(5,vec(0,0,0))
}
]#

if (first()) {
    EGP:egpClear()
    EGP:egpBox(1,vec2(306,166),vec2(30,300))
    EGP:egpMaterial(1,"gui/center_gradient")
    EGP:egpColor(1,vec(100,100,100))
    EGP:egpAngle(1,90)
    
    EGP:egpText(2,"The Stinger",vec2(226,126))
    EGP:egpSize(2,35)
    EGP:egpColor(2,vec(0,0,0))
    
    EGP:egpText(5,"To go back to the choosing menu hit backspace.",vec2(158,386))
    EGP:egpSize(5,15)
    EGP:egpColor(5,vec(0,0,0))
}
