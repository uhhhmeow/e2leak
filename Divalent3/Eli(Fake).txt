@name Eli(Fake)
@inputs [DoorL DoorR StandPos Emitter Eli]:entity S:wirelink 
@outputs [User EliUser]:entity 
@persist A A2 State Active1 Active2 Active3 [CornerPos1 CornerPos2]:vector 
@trigger 
interval(100)
runOnChat(1)

findInSphere(Emitter:pos(),30)
User = find()

#Walking = 8

if (first() | dupefinished()) {
    holoCreate(1)
    holoModel(1,"models/Humans/Group01/male_07.mdl")
    holoPos(1,StandPos:pos()+vec(40,-40,2))
    holoAng(1,StandPos:angles()+ang(0,90,0))
    holoScale(1,vec(0.9,0.9,0.9))
    holoAnim(1,2)
    
    Scale = vec(6,8,6) * 12
    
    Door = holoCreate(2)
    
    holoScaleUnits(2,Scale)
    holoPos(2,Eli:toWorld(vec(0,0,40)))
    holoAng(2,Eli:angles())
    holoColor(2,vec(255,255,255),255)
    holoParent(2,entity())
    
    CornerPos1 = Scale / 2
    CornerPos2 = -CornerPos1
    
    CornerPos1 = Door:toWorld(CornerPos1)
    CornerPos2 = Door:toWorld(CornerPos2)
    
    CornerScale = vec() + 1
    
    holoCreate(3)
    holoPos(3,CornerPos1)
    holoScaleUnits(3,CornerScale)
    holoParent(3, Door)
    
    holoCreate(4)
    holoPos(4,CornerPos2)
    holoScaleUnits(4,CornerScale)
    holoParent(4, Door)
    
    for (I = 2,4) {holoColor(I,vec(),0)}
    
    holoCreate(5)
    holoModel(5,"models/props_c17/FurnitureChair001a.mdl")
    holoPos(5,StandPos:pos()+vec(-50,10,20))
    holoAng(5,StandPos:angles()+ang(0,110,0))
    
    S:egpClear()
    
    S:egpText(5,"Room #1",vec2(156,350))
    S:egpAlign(5,1,1)
    S:egpSize(5,20)
    
    S:egpRoundedBox(2,vec2(156,380),vec2(62,22))
    S:egpColor(2,vec(0,255,0))
    
    S:egpText(6,"Room #2",vec2(256,350))
    S:egpAlign(6,1,1)
    S:egpSize(6,20)
    
    S:egpRoundedBox(3,vec2(256,380),vec2(62,22))
    S:egpColor(3,vec(0,255,0))
    
    S:egpText(7,"Room #3",vec2(356,350))
    S:egpAlign(7,1,1)
    S:egpSize(7,20)
    
    S:egpRoundedBox(4,vec2(356,380),vec2(62,22))
    S:egpColor(4,vec(0,255,0))
    
    S:egpText(666,"Don't use. No refunds!",vec2(256,212))
    S:egpAlign(666,1,1)
    S:egpSize(666,30)
    S:egpColor(666,vec(0,155,255))
    
    Active1 = 1
    Active2 = 1
    Active3 = 1
}

if (User:isPlayer()) {
    holoPos(1,StandPos:pos()+vec(40,-40,2)) 
    holoAnim(1,2)
    holoAng(1,StandPos:angles()+ang(0,90,0))
} else {
    holoPos(1,holoEntity(5):pos()+vec(18,-5,-18))
    holoAnim(1,115)
    holoAng(1,holoEntity(5):angles())
}

if (findCanQuery()) {
    findInBox(CornerPos1,CornerPos2)
    findIncludeClass("player")
    EliUser = findClosest(holoEntity(2):pos())
}
#[
R = owner():keyWalk()

if(changed(R) & R) {if(State == 0) {State = 1} else {State = 0}}

if(State == 1) {
    if(A < 0.38) {
        A += 0.01
        DoorL:setPos(DoorL:pos()+vec(0,-1.25,0))
        DoorR:setPos(DoorR:pos()+vec(0,1.25,0))
    }
} elseif(A > 0) {
    A -= 0.01
    DoorL:setPos(DoorL:pos()+vec(0,1.25,0))
    DoorR:setPos(DoorR:pos()+vec(0,-1.25,0))
}

if (changed(State == 1) & State == 1) {
    DoorL:soundPlay(1,0,"doors/doormove2.wav")
    DoorR:soundPlay(2,0,"doors/doormove2.wav")
    soundVolume(1,0.3)
    soundVolume(2,0.3)
}

if (changed(State == 0) & State == 0) {
    DoorL:soundPlay(1,0,"doors/doormove3.wav")
    DoorR:soundPlay(2,0,"doors/doormove3.wav")
    soundVolume(1,0.3)
    soundVolume(2,0.3)
}
]#
if (chatClk(owner()) & owner():lastSaid():lower():find("vacate room 1")) {
    Active1 = 1
    S:egpColor(2,vec(0,255,0))
}
if (chatClk(owner()) & owner():lastSaid():lower():find("vacate room 2")) {
    Active2 = 1
    S:egpColor(3,vec(0,255,0))
}
if (chatClk(owner()) & owner():lastSaid():lower():find("vacate room 3")) {
    Active3 = 1
    S:egpColor(4,vec(0,255,0))
}

if (Active1 == 1) {
    if (chatClk(User) & User:lastSaid():lower():find("buy room 1")) {
        moneyRequest(User,20000,"Rent Room #1",20)
    }

    if (moneyClk("Rent Room #1")) {
        S:egpColor(2,vec(255,0,0))
        Active1 = 0
    }
}

if (Active2 == 1) {
    if (chatClk(User) & User:lastSaid():lower():find("buy room 2")) {
        moneyRequest(User,20000,"Rent Room #2",20)
    }

    if (moneyClk("Rent Room #2")) {
        S:egpColor(3,vec(255,0,0))
        Active2 = 0
    }
}

if (Active3 == 1) {
    if (chatClk(User) & User:lastSaid():lower():find("buy room 3")) {
        moneyRequest(User,20000,"Rent Room #3",20)
    }

    if (moneyClk("Rent Room #3")) {
        S:egpColor(4,vec(255,0,0))
        Active3 = 0
    }
}

if(EliUser:isPlayer()) {A2 = 1}
else {A2 = 0}

if(changed(A2) & !first()) {
    if(A2 == 1) {
        Eli:soundPlay("Eli",0,"ambient/music/cubanmusic1.wav")
        soundVolume("Eli",0.2)
    } elseif (A2 == 0) {
        soundStop("Eli")
    }
}
