@name HeliChair
@inputs 
@outputs 
@persist 
@trigger 
interval(100)

E = entity():isWeldedTo()

if (first()) {
    holoCreate(1)
    holoModel(1,"models/props_c17/TrapPropeller_Engine.mdl")
    holoPos(1,E:pos()+vec(0,28,10))
    holoAng(1,ang(0,90,0))
    holoParent(1,E)
    
    holoCreate(2)
    holoPos(2,E:pos()+vec(2,22,10))
    holoAng(2,E:angles()+ang(0,0,0))
    holoScale(2,vec(0.1,0.1,0.1))
    holoParent(2,E)
    
    holoCreate(3)
    holoModel(3,"models/props_c17/signpole001.mdl")
    holoPos(3,E:pos()+vec(2,22,26))
    holoAng(3,E:angles()+ang(0,0,0))
    holoScale(3,vec(0.6,0.6,0.2))
    holoParent(3,E)
    
    holoCreate(4)
    holoModel(4,"models/props_c17/TrapPropeller_Blade.mdl")
    holoPos(4,E:pos()+vec(2,22,44))
    holoAng(4,E:angles()+ang(0,0,0))
    holoParent(4,2)
    
    holoCreate(5)
    holoModel(5,"models/props_c17/TrapPropeller_Blade.mdl")
    holoPos(5,E:pos()+vec(2,22,44))
    holoAng(5,E:angles()+ang(0,90,0))
    holoParent(5,2)
    
    holoCreate(6)
    holoModel(6,"models/props_c17/TrapPropeller_Blade.mdl")
    holoPos(6,E:pos()+vec(2,22,44))
    holoAng(6,E:angles()+ang(0,180,0))
    holoParent(6,2)
    
    holoCreate(7)
    holoModel(7,"models/props_c17/TrapPropeller_Blade.mdl")
    holoPos(7,E:pos()+vec(2,22,44))
    holoAng(7,E:angles()+ang(0,-90,0))
    holoParent(7,2)
}

holoAng(2,holoEntity(2):angles()+ang(0,100,0))
