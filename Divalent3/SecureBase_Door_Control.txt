@name SecureBase Door Control
@inputs [Door1 Door2 Plate]:entity 
@outputs 
@persist State A 
@trigger 
interval(50)

if (first()) {
    State = 0
}

if (changed(owner():keyWalk()) & owner():keyWalk()) {
    State = 1
}

if (changed(owner():keySprint()) & owner():keySprint()) {
    State = 2
}
if (changed(owner():keyJump()) & owner():keyJump()) {
    State = 3
}

if (State == 1) {
    if(A < 450) {
        A += 5
        Door1:setAng(Door1:angles() + ang(0,2,0))
    }
}

if (State == 2) {
    if(A < 450) {
        A += 5
        Door2:setAng(Door2:angles() + ang(0,2,0))
    }
}

if (changed(State == 3) & State == 3) {
    holoCreate(1)
    holoModel(1,"cplane")
    holoAng(1,Plate:angles())
    holoScale(1,Plate:boxSize()/14)
    holoParent(1,Plate)
    
    holoCreate(2)
    holoModel(2,"cplane")
    holoAng(2,Plate:angles() + ang(180,0,0))
    holoScale(2,Plate:boxSize()/14)
    holoParent(2,1)
    
    for (I = 1,2) {
        holoDisableShading(I,1)
        holoColor(I,vec(255,0,0))
        holoAlpha(I,100)
    }
}

if (State == 3) {
    holoPos(1,Plate:pos() + vec(0,0,48+sin(curtime() * 34) * 38))
}
