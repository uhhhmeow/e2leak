@name SPACCCEEEEE
@inputs Pod:wirelink 
@persist State CPit TarPos:vector TarAng:angle 
@outputs Speed Pitch
@model models/hunter/plates/plate1x2.mdl

interval(50)

local Prop = entity()
local Seat = Pod["Entity",entity]
local Driver = Seat:driver()
local W = Driver:keyForward()
local A = Driver:keyLeft()
local S = Driver:keyBack()
local D = Driver:keyRight()
local R = Driver:keyReload()
local P1 = Driver:keyPressed("1")
local P2 = Driver:keyPressed("2")
local P3 = Driver:keyPressed("3")
local Mouse5 = Driver:keyPressed("mouse_5")
local Mouse4 = Driver:keyPressed("mouse_4")
local Mouse1 = Driver:keyAttack1()
local Mouse2 = Driver:keyAttack2()
local Space = Driver:keyJump()
local Shift = Driver:keySprint()
local Alt = Driver:keyWalk()
local Active = Driver:isPlayer()

if(first() | dupefinished()) {
    holoCreate(1)
    holoModel(1,"models/props_combine/tprotato1.mdl")
    holoAng(1, Prop:toWorld(ang(75,90,180)))
    holoParent(1,Prop)
 
    holoCreate(3)
    holoModel(3,"models/props_combine/tprotato1_chunk01.mdl")
    holoPos(3,Prop:toWorld(vec(-14,-70,0)))
    holoAng(3, Prop:toWorld(ang(-90,-90,180)))
    holoParent(3,Prop)
    
    holoCreate(4)
    holoPos(4, Prop:toWorld(vec(0,50,20)))
    holoParent(4, Prop)
    holoAlpha(4, 0)
    
    holoCreate(2)
    holoModel(2,"models/props_combine/tprotato2.mdl")
    holoPos(2,Prop:toWorld(vec(0,0,24)))
    holoAng(2, Prop:toWorld(ang(-90,0,90)))
    holoParent(2,4)

    Prop:setMass(50000)
    
    Prop:setColor(vec(),0)
    
    TarAng = Prop:angles()
    TarPos = Prop:pos()
    
    # Convert two vectors to an angle with ang
    function angle ang(V1:vector,V2:vector) {
        return (V2-V1):toAngle()
    }
    
    function number ang(V1:vector2,V2:vector2) {
        return (V2-V1):toAngle()
    }
    
    # Determine path of least resistance to reach another angle
    function number swivel(A:number,B:number) {
        local C1=(B+(360-A))%360
        local C2=C1-360
        return abs(C1)<abs(C2) ? C1 : C2
    }
    
    function angle swivel(A:angle,B:angle) {
        return ang(
            swivel(A:pitch(),B:pitch()),
            swivel(A:yaw(),B:yaw()),
            swivel(A:roll(),B:roll())
        )
    }
}

if (Prop:isPlayerHolding()) {TarPos = Prop:pos() TarAng = Prop:angles()}

if(changed(R) & R & (CPit == -45 | CPit == 0)) {
    if(State == 0) {
        State = 1
    } else {State = 0}
}

if(changed(Active) & Active & (CPit == -45 | CPit == 0)) {
    if(State == 0) {
        State = 1
    } else {State = 0}
}

if(changed(!Active) & !Active & (CPit == +45 | CPit == 0)) {
    if(State == 0) {
        State = 1
    } else {State = 0}
}

if(State == 1) {
    if(CPit > -45) {
        CPit -= 1
        holoAng(4,Prop:toWorld(ang(0,0,CPit)))
    }
} else {
    if(CPit < 0) {
        CPit += 1
        holoAng(4,Prop:toWorld(ang(0,0,CPit)))
    }
}

Prop:applyAngForce((swivel(Prop:angles(),TarAng) * 5 - Prop:angVel()) * Prop:mass()*5)
Prop:applyForce(((TarPos - Prop:pos()) * 5 - Prop:vel()) * Prop:mass())

if (W) { TarPos += Prop:right()*10 }
if (W & Shift) { TarPos += Prop:right()*20 }
if (S) { TarPos += -Prop:right()*10 }
if (S & Shift) { TarPos += -Prop:right()*20 }
if (Space) { TarPos += Prop:up()*8 }
if (Alt) { TarPos += -Prop:up()*8 }
if (A) { TarAng += ang(0,1,0)*5 }
if (D) { TarAng += -ang(0,1,0)*5 }

if (changed(Active) & Active) {Prop:soundPlay(1,0,"ambient/atmosphere/drone1lp.wav")}
if (!Active) {soundStop(1)}

#This is the speed you need to be to reach the top pitch
TopSpeed = 200

Speed = Prop:velL():length()
Speed = clamp(Speed, 0, TopSpeed)

#IdlePitch is the pitch when not moving
IdlePitch = 100
TopPitch = 255

Pitch = round((TopPitch - IdlePitch) * Speed / TopSpeed + IdlePitch)
Pitch = clamp(Pitch, IdlePitch, TopPitch)

#soundPitch(1,Pitch)
soundPitch(1,255)


####################
## Thanks to Kron ##
# for helping with #
##   holoAngles   ##
####################
