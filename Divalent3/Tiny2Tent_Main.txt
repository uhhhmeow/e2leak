@name Tiny2Tent Main
@inputs Prop:entity [Tp Prop2]:entity 
@outputs User:entity 
@persist A [CornerPos1 CornerPos2]:vector 
@trigger 
interval(100)

if(first()) {
    
    Scale = vec(2,2,3) * 12
    
    Door = holoCreate(1)
    
    holoScaleUnits(1,Scale)
    holoPos(1,Prop:toWorld(vec(5,0,40)))
    holoColor(1,vec(255,255,255),255)
    holoParent(1,entity())
    
    CornerPos1 = Scale / 2
    CornerPos2 = -CornerPos1
    
    CornerPos1 = Door:toWorld(CornerPos1)
    CornerPos2 = Door:toWorld(CornerPos2)
    
    CornerScale = vec() + 1
    
    holoCreate(2)
    holoPos(2,CornerPos1)
    holoScaleUnits(2,CornerScale)
    holoParent(2,Door)
    
    holoCreate(3)
    holoPos(3,CornerPos2)
    holoScaleUnits(3,CornerScale)
    holoParent(3,Door)
    
    for (I = 1,3) {holoColor(I,vec(),0)}
}

findInBox(CornerPos1,CornerPos2)
findIncludeClass("player")
User = findClosest(holoEntity(1):pos())

if(User:isPlayer()) {A = 1}
else {A = 0}

if(changed(A) & !first()) {
    if(A == 1) {
        printColor(vec(147,65,0),User:name(),vec(0,45,45)," has stepped infront of the bookcase.")
    }
}

if (User:aimEntity() == Prop2 & User:keyUse()) {
    User:teleport(Tp:pos())
}
