@name hover thing
@inputs Thrusters:array Base:entity 
@persist D1 D2 D3 D4
@persist [T1 T2 T3 T4]:entity
@persist [W1 W2 W3 W4]:wirelink
@persist [R1 R2 R3 R4]:ranger

#credit to doomtaters n stuff# fuck you
if( first() )
{
    runOnTick(1)
    
    T1 = Thrusters[1,entity]
    T2 = Thrusters[2,entity]
    T3 = Thrusters[3,entity]
    T4 = Thrusters[4,entity]
    
    W1 = T1:wirelink()
    W2 = T2:wirelink()
    W3 = T3:wirelink()
    W4 = T4:wirelink()
    
}
Base:setMass(1000)

rangerFilter(T1)
rangerFilter(entity():isWeldedTo())
R1 = rangerOffset(1e+6,T1:pos(),vec(0,0,-1))
D1 = R1:distance()
rangerFilter(T2)
rangerFilter(entity():isWeldedTo())
R2 = rangerOffset(1e+6,T2:pos(),vec(0,0,-1))
D2 = R2:distance()
rangerFilter(T3)
rangerFilter(entity():isWeldedTo())
R3 = rangerOffset(1e+6,T3:pos(),vec(0,0,-1))
D3 = R3:distance()
rangerFilter(T4)
rangerFilter(entity():isWeldedTo())
R4 = rangerOffset(1e+6,T4:pos(),vec(0,0,-1))
D4 = R4:distance()

W1["A",normal] = clamp(50-R1:distance()-$D1*5,0,10000)*150
W2["A",normal] = clamp(50-R2:distance()-$D2*5,0,10000)*150
W3["A",normal] = clamp(50-R3:distance()-$D3*5,0,10000)*150
W4["A",normal] = clamp(50-R4:distance()-$D4*5,0,10000)*150

RE = entity():isWeldedTo()

RE:applyForce( -RE:vel() * RE:mass() * 0.01 )
RE:applyAngForce( -RE:angVel() * RE:mass() )
