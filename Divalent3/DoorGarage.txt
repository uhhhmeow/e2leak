@name DoorGarage
@inputs Door:entity
@outputs 
@persist A State 
@trigger 
runOnTick(1)

if (first()) {
    
}

R = owner():keyWalk()

if(changed(R) & R) {if(State == 0) {State = 1} else {State = 0}}

if(State == 1) {
    if(A < 200) {
        A += 5
        Door:setPos(Door:pos() + vec(0,-1.2,1.2))
        Door:setAng(Door:angles() + ang(2.2,0,0))
    }
} else {
    if(A > 0) {
        A -= 5
        Door:setPos(Door:pos() - vec(0,-1.2,1.2))
        Door:setAng(Door:angles() + ang(-2.2,0,0))
    }
}
