@name Water Gate
@inputs Toggle
@persist Door:entity DoorNormalPlace DoorOpenPlace Toggle 
@outputs DoorNormalPlace DoorOpenPlace Pos
runOnTick(1)

Pos = Door:pos():z()

if(first()) {
    entity():setColor(vec(255,255,255),0)
    #entity():propNotSolid(1)
    Door = entity():isWeldedTo()
    DoorNormalPlace = Door:pos():z()
    DoorOpenPlace = DoorNormalPlace - 335
}

if(Toggle) {
    if(Door:pos():z() > DoorOpenPlace) {
        Door:setPos(Door:pos() - vec(0,0,1))
        Door:soundPlay(1,0,"doors/drawbridge_move1.wav")
    }
} else {
    if(Door:pos():z() < DoorNormalPlace) {
        Door:setPos(Door:pos() + vec(0,0,1))
        Door:soundPlay(1,0,"doors/drawbridge_move1.wav")
    }
}
