@name Christmas
@inputs S:wirelink 
@outputs Plys:array 
@persist 
@trigger 
interval(1000)

findByClass("player")
Plys = findToArray()

if (first()) { 
    S:egpBox(1,vec2(256,256),vec2(512,512))
    S:egpMaterial(1,"console/background_xmas2011")

    S:egpText(2,"Happy",vec2(15,100))
    S:egpSize(2,50)
    S:egpColor(2,vec(255,0,0))


    S:egpText(3,"Holidays",vec2(15,145))
    S:egpSize(3,45)
    S:egpColor(3,vec(0,255,0))

    S:egpText(4,"From:",vec2(325,100))
    S:egpSize(4,50)
    S:egpColor(4,vec(0,255,0))

    S:egpText(5,"The Admins",vec2(325,140))
    S:egpSize(5,35)
    S:egpColor(5,vec(255,0,0))
}

if (owner():keyWalk()) {
for(I=1,Plys:count()){
    holoCreate(I,Plys[I,entity]:pos()+vec(0,10,0))
    holoEntity(I):soundPlay(I,0,"ui/holiday/gamestartup_saxxy.mp3")
    holoAlpha(I,0)
    holoParent(I,Plys[I,entity]) 
    }
}
