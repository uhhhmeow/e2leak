@name BailNPC
@inputs EGP:wirelink 
@outputs User:entity 
@persist Prop:entity 
@trigger 
interval(100)
runOnChat(1)

Cursor = EGP:egpCursor(User)

if (first()) {
    runOnLast(1)
    
    holoCreate(1)
    holoModel(1,"models/Combine_Super_Soldier.mdl")
    holoAnim(1,134)
    holoPos(1,entity():toWorld(vec(0,0,0)))
    holoAng(1,ang(0,135,0))
    holoParent(1,entity())
    
    #Prop = propSpawn("models/hunter/blocks/cube05x105x05.mdl",1)
    #Prop:setPos(entity():pos() + vec(0,0,40))
    #Prop:setAng(ang(0,0,90))
    #Prop:setColor(vec(255,255,255),0)
    
    function array findArrestedPlayers() {
        local Players = players()
        
        local ArrestedPlayers = array()
        
        for (I = 1, Players:count()) {
            Player = Players[I,entity]
            
            Alive = Player:isAlive()
            
            Weapon = Player:weapon()            
            
            if (Alive & !Weapon) {
                ArrestedPlayers:pushEntity(Player)
            }
        }
        return ArrestedPlayers
    }
    
    ArrestedPlayers = findArrestedPlayers()
    
    printTable(ArrestedPlayers)
    
    EGP:egpClear()
    
    EGP:egpBox(1,vec2(306,312),vec2(156,312))
    EGP:egpMaterial(1,"vgui/button_central_disabled")
    
    EGP:egpText(7,"Choose a player",vec2(256/1.8,52))
    EGP:egpColor(7,vec(107,45,45))
    EGP:egpSize(7,50)
    
    EGP:egpText(8,"to bail out.",vec2(196,92))
    EGP:egpSize(8,50)
}

foreach (I,Ply:entity = ArrestedPlayers) {
    EGP:egpBox(1 + I,vec2(306,212 + ((I - 1) * 50)),vec2(116,42))
    EGP:egpMaterial(1 + I,"vgui/button_central_hover")
    
    EGP:egpText(100 + I,Ply:name(),vec2(306,195 + ((I - 1) * 50)))
    EGP:egpAlign(100 + I,1)
    EGP:egpSize(100 + I,30)
}

findByClass("Player")
User = findClosest(entity():pos())

#if (moneyClk()) {
#    concmd("say /unarrest "+Target:name())
#    print("You unarrested "+Target:name(),".")
#}

#if (last()) {Prop:propDelete()}

EGP:egpCircle(0,vec2(0,0),vec2(2,2))
EGP:egpColor(0,vec4(255,255,255,0))
EGP:egpParentToCursor(0)
