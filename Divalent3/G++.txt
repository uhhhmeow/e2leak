@name G++
@inputs EGP:wirelink Key 
@outputs [Cursor]:vector2 Lines:array Input:string AddL OnLine XX

if (first())
{
    
    EGP:egpClear()
    
    EGP:egpBox(2, vec2(256, 256), vec2(512, 512))
    
    EGP:egpText(1000, "C/C++ Editor", vec2(0, 0))
    EGP:egpFont(1000, "Lucida Console")
    
    EGP:egpColor(2, vec(22, 36, 48))
    OnLine = 0
    
    function number isalpha(N:number)
    {
        return (N >= 97 & N <= 122)
    }
    
    function drawLines()
    {
        X = 0
        for (I = 1, 24)
        {
            Line = Lines[I, string]

            X++
            M = 100 + (I % 24)
            
            EGP:egpText(M, Line, vec2(0, 14 * X) + vec2(0, 14))
            EGP:egpFont(M, "Lucida Console")
        }
    }
    
    function number l()
    {
        return 100 + (OnLine % 24)
    }
}

if (Key & ~Key)
{
    if (Key == 19)
    {
        Cursor += vec2(-1, 0)
    }
    elseif (Key == 20)
    {
        Cursor += vec2(1, 0)
    }
    elseif (Key == 17)
    {
        Cursor += vec2(0, -1)
    }
    elseif (Key == 18)
    {
        Cursor += vec2(0, 1)
    }
    else
    {
        
        AddL
        

        if (Key != 127 & Key != 154 & Key != 13)
        {
            Input+=toChar(Key)
            AddL+=4
            Cursor += vec2(1, 0)
        }
        
        if (Key == 127)
        {
            Input = Input:left(Input:length() - 1)
            Cursor -= vec2(1, 0)
            
            if (Input:length() == 0)
            {
                if (OnLine > 0)
                {
                    EGP:egpText(l() + 1, Input + "", vec2(0, OnLine) * 14 + vec2(0, 28))
                    Input = Lines[OnLine + 1, string] = ""
                    
                    OnLine--
                    Input = Lines[OnLine + 1, string]
                    Cursor = vec2(Input:length() + 1, OnLine)
                }
            }
        }
            
        if (Key == 13)
        {
            Cursor = vec2(0, Cursor:y() + 1)
            OnLine += 1
            Lines[OnLine, string] = Input
            
            Input = ""
            drawLines()
        }
    }

    
    #X = max(0, Cursor:x())
    #Y = max(0, Cursor:y())
    #Cursor = vec2(X, Y)
    EGP:egpText(l() + 1, Input, vec2(0, OnLine) * 14 + vec2(0, 28))
    EGP:egpText(3, "|", vec2(Cursor:x() * 0.85, Cursor:y()) * 14 + vec2(0, 28))
    
    EGP:egpFont(l() + 1, "Lucida Console")
    EGP:egpFont(3, "Lucida Console")
}


XX = l()
