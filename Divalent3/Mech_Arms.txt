@name Mech Arms
@inputs [Pod G]:wirelink 
@outputs 
@persist State 
@trigger 
runOnTick(1)

E = entity():isWeldedTo()

if (first()) {
    holoCreate(1)
    holoPos(1,E:toWorld(vec(-15,35,0)))
    holoModel(1,"models/weapons/w_mach_m249para.mdl")
    holoAng(1,E:toWorld(ang(0,180,0)))
    
    holoCreate(2)
    holoPos(2,E:toWorld(vec(-15,-35,0)))
    holoAng(2,E:toWorld(ang(0,180,0)))
    
    G:entity():propFreeze(1)
    G:entity():setMass(50000)
}

local Seat = Pod["Entity",entity]
local Driver = Seat:driver()
local M1 = Driver:keyAttack1()
local M2 = Driver:keyAttack2()
local R = Driver:keyReload()
local HoloEnt = holoEntity(2)
G:entity():setPos(HoloEnt:pos()+vec(0,0,0))
G:entity():setAng(HoloEnt:toWorld(ang(90,0,0)))

if (M1) {
    holoAng(2,Driver:eyeAngles())
}

if(changed(R) & R) {if(State == 0) {State = 1} else {State = 0}}

if (State == 1) {
    G["Grab",number] = 1
}

if (State == 0) {
    G["Grab",number] = 0
}

if (M2) {
    holoAng(1,Driver:eyeAngles())
}
