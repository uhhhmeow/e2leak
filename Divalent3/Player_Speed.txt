@name Player Speed
@inputs EGP:wirelink
@outputs 
@persist 
@trigger 
runOnTick(1)

MPH = toUnit("mph",entity():isWeldedTo():vel():length())

if (first()) {
    EGP:egpClear()
    EGP:egpColor(1,vec(0,255,255))
}
EGP:egpText(1,"MPH: "+round(MPH),vec2(775,872))
