@name FileWriteFunction2
@inputs
@outputs
@persist Ready ArrayAdd Int [Name ModelStr ColorStr MatStr PosStr AngStrText]:string 
@persist [Ents Model Color Material Pos Ang]:array Base:entity 
@trigger 
interval(100)
runOnChat(1)

if (owner():weapon():type() == "weapon_physcannon") {
    if (changed(owner():keyUse())||changed(owner():keyReload())) {
        AE = owner():aimEntity()
        
        if (owner():keyUse()) {
            if (Ents:count() > 0) {
                for (I = 1,Ents:count()) {
                    AAE = Ents[I,entity]
                    
                    if (AE == AAE) {
                        ArrayAdd = 0
                        AAE:setColor(Color[I,vector4])
                        Ents:remove(I)
                        Model:remove(I)
                        Color:remove(I)
                        Material:remove(I)
                        Pos:remove(I)
                        Ang:remove(I)
                        break
                    } else {ArrayAdd = 1}
                }
            } else {ArrayAdd = 1}
            
            if (ArrayAdd) {
                if (Base:isValid()) {
                    if (AE:type() == "prop_physics") {
                        Ents:pushEntity(AE)
                        Model:pushString(AE:model())
                        Color:pushVector4(AE:getColor4())
                        
                        if (AE:getMaterial():length() < 2) {Material:pushString(" ")}
                        else {Material:pushString(AE:getMaterial())}
                        
                        Pos:pushVector(Base:toLocal(AE:pos()))
                        Ang:pushAngle(Base:toLocal(AE:angles()))
                        AE:setColor(0,255,0,200)
                    }
                }
            }
        }
        
        if (owner():keyReload()) {
            if (AE != Base) {
                if (!AE:isPlayer() & !AE:isWeapon() & !AE:isNPC()) {
                    Base:setColor(255,255,255,255)
                    Base = AE
                    Base:setColor(255,0,0,200)
                }
            } else {
                Base:setColor(255,255,255,255)
                Base = noentity()
            }
        }
    }
}

if (changed(owner():keyWalk()) & owner():keyWalk()) {hint(Ents:count():toString(),1)}

if (chatClk(owner())) {
    CR = owner():lastSaid():explode(" ")
    if (CR[1,string]=="!s") {
        hideChat(1)
        
        if (!CR[2,string]:lower():find(".txt")) {
            Name = CR[2,string]+".txt"}
            else {Name = CR[2,string]}
            
            Ready = 1
            print("Saving.")
    }
}

Text = "@name "+Name:replace(".txt","")+"
@model "+Base:model()+"
@persist 
interval(100)

if (first() | dupefinished()) {
    
}
"

if (Ready == 1 & fileCanWrite()) {
    fileWrite("AAAA.txt",Text)
    
    for (I = 1,Ents:count()) {
        AE = Ents[I,entity]
        AE:setColor(Color[I,vector4])
    }
    
    Base:setColor(255,255,255,255)
    selfDestruct()
}
