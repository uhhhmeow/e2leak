@name Simple Pod Protection
@inputs 
@outputs 
@persist [E POD]:entity
@trigger 

if(first()){
    runOnTick(1)
    E=entity()
    E:setAlpha(0)
    POD=E:isWeldedTo()
}

if(POD:driver()!=owner()&POD:driver():isValid()){
    POD:printColorDriver(vec(255,0,0),"STAY OUTTA MY SEAT")
    POD:soundPlay(1,1,"/npc/turret_floor/alert.wav",0.5) POD:killPod()
}
