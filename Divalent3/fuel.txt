@name fuel
@inputs Fuel Capacity Speed RPM
@outputs 
@persist [Tach Speedo Fuel_Gauge O E]:entity Idx Cid:array Speedometer Tachometer FuelGauge
@model 

if( duped() ){ reset() }

if( first() )
{
    runOnTick(1)
    #runOnChat(1)
    #runOnLast(1)
    
    O = owner()
    E = entity():isWeldedTo() ?: entity()
    
    function number getID( N )
    {
        Idx+=N
        return Idx
    }
    function number clipIt( N:number, [V1 V2]:vector )
    {
        Cid[N,number] = Cid[N,number] + 1
        holoClipEnabled( N, Cid[N,number], 1 )
        holoClip( N, Cid[N,number], V1, V2, 0 )
    }
    #clipIt( getID(0), vec(), vec(0,0,1) )
    
    Scale = 1/2
    Small_Scale = 0.75
    
    Small_Scale *= Scale
    
    Skins = vec(5,4,6)
    
    Sides = vec(12,12,12)
    Offset = Scale*vec(0,0,20)
    
    Tach = holoCreate( getID(1), E:toWorld(Offset+vec(Scale*0.3,-6*Scale-(6-1.5)*Small_Scale,-6*Scale+6*Small_Scale)), Small_Scale*vec(12,12,12) / 12, E:toWorld(ang(0,0,0)), vec()+255 )
    holoModel( getID(0), "models/sprops/trans/misc/gauge_3.mdl" )
    holoSkin( getID(0), Skins[1] )
    holoParent( getID(0), E )
    
        Tmp = holoCreate( getID(1), Tach:toWorld(vec(Scale*0.8,0,0)), Small_Scale*vec(1,1,0.35) / 12, Tach:toWorld(ang(90,0,0)), vec()+255 )
        holoModel( getID(0), "hq_rcylinder" )
        holoParent( getID(0), Tach )
        Tachometer = getID(0)
        
        holoCreate( getID(1), Tmp:toWorld(Small_Scale*vec(0,-2.25,0)), Small_Scale*vec(0.6,0.2,4.75) / 12, Tmp:toWorld(ang(0,0,90)), vec()+255 )
        holoModel( getID(0), "prism" )
        holoDisableShading( getID(0), 1 )
        holoParent( getID(0), Tmp )
    
    Speedo = holoCreate( getID(1), E:toWorld(Offset+vec(0,0,0)), Scale*vec(12,12,12) / 12, E:toWorld(ang(0,0,0)), vec()+255 )
    holoModel( getID(0), "models/sprops/trans/misc/gauge_3.mdl" )
    holoSkin( getID(0), Skins[2] )
    holoParent( getID(0), E )
    
        Tmp = holoCreate( getID(1), Speedo:toWorld(Scale*vec(1,0,0)), Scale*vec(1,1,0.35) / 12, Speedo:toWorld(ang(90,0,0)), vec()+255 )
        holoModel( getID(0), "hq_rcylinder" )
        holoParent( getID(0), Speedo )
        Speedometer = getID(0)
        
        holoCreate( getID(1), Tmp:toWorld(Scale*vec(0,-2.25,0)), Scale*vec(0.6,0.2,4.75) / 12, Tmp:toWorld(ang(0,0,90)), vec()+255 )
        holoModel( getID(0), "prism" )
        holoDisableShading( getID(0), 1 )
        holoParent( getID(0), Tmp )
    
    Fuel_Gauge = holoCreate( getID(1), E:toWorld(Offset+vec(Scale*0.3,6*Scale+(6-1.5)*Small_Scale,-6*Scale+6*Small_Scale)), Small_Scale*vec(12,12,12) / 12, E:toWorld(ang(0,0,0)), vec()+255 )
    holoModel( getID(0), "models/sprops/trans/misc/gauge_3.mdl" )
    holoSkin( getID(0), Skins[3] )
    holoParent( getID(0), E )
    
        Tmp = holoCreate( getID(1), Fuel_Gauge:toWorld(vec(Scale*0.8,0,0)), Small_Scale*vec(1,1,0.35) / 12, Fuel_Gauge:toWorld(ang(90,0,0)), vec()+255 )
        holoModel( getID(0), "hq_rcylinder" )
        holoParent( getID(0), Fuel_Gauge )
        FuelGauge = getID(0)
        
        holoCreate( getID(1), Tmp:toWorld(Small_Scale*vec(0,-2.25,0)), Small_Scale*vec(0.6,0.2,4.75) / 12, Tmp:toWorld(ang(0,0,90)), vec()+255 )
        holoModel( getID(0), "prism" )
        holoDisableShading( getID(0), 1 )
        holoParent( getID(0), Tmp )

    local Down = Scale*0.5
    
#[    holoCreate( getID(1), E:toWorld(Offset+Scale*vec(0.15+1,0,-5.5)), Scale*vec(25-0.2+1,35,3) / 12, E:toWorld(ang(90+6,0,0)), vec()+255 )
    holoModel( getID(0), "hq_tube_thin" )
    holoMaterial( getID(0), "WTP/carbonfibre_2" )
    clipIt( getID(0), vec(Down,0,0), vec(-1,0,0):rotate(ang(-6,0,0)) )
    holoParent( getID(0), E )
    
    holoCreate( getID(1), E:toWorld(Offset+Scale*vec(0.4,0,-5.5)), Scale*vec(25+1,35,1) / 12, E:toWorld(ang(90,0,0)), vec()+255 )
    holoModel( getID(0), "hq_cylinder" )
    holoMaterial( getID(0), "WTP/carbonfibre_2" )
    clipIt( getID(0), vec(Down,0,0), vec(-1,0,0):rotate(ang(0,0,0)) )
    holoParent( getID(0), E )
    
    holoCreate( getID(1), E:toWorld(Offset+Scale*vec(0.4-0.55,0,-5.5)), Scale*vec(25+1,35,30) / 12, E:toWorld(ang(-90,0,0)), vec()+255 )
    holoModel( getID(0), "hq_dome" )
    holoMaterial( getID(0), "WTP/carbonfibre_2" )
    clipIt( getID(0), vec(-Down,0,0), vec(1,0,0):rotate(ang(0,0,0)) )
    holoParent( getID(0), E )
    
    holoCreate( getID(1), E:toWorld(Offset+Scale*vec(0.4,0,-6.5+0.4)), Scale*vec(30+1.1,35+0.35,0.25) / 12, E:toWorld(ang(0,0,0)), vec()+255 )
    holoModel( getID(0), "hq_cylinder" )
    holoMaterial( getID(0), "WTP/carbonfibre_2" )
    clipIt( getID(0), Scale*vec(2.2,0,0), vec(-1,0,0):rotate(ang(0,0,0)) )
    holoParent( getID(0), E )
    
    holoCreate( getID(1), E:toWorld(Offset+Scale*vec(0.4,0,-6.5+0.4)), -Scale*vec(30+1.1,35+0.35,0.25) / 12, E:toWorld(ang(0,0,0)), vec()+255 )
    holoModel( getID(0), "hq_cylinder" )
    holoMaterial( getID(0), "WTP/carbonfibre_2" )
    clipIt( getID(0), Scale*vec(2.2,0,0), vec(-1,0,0):rotate(ang(0,0,0)) )
    holoParent( getID(0), E )
]#
}

if( ~Speed )
{
    holoAng( Speedometer, Speedo:toWorld( ang( 90, 0, 0 ):rotateAroundAxis( vec( 1, 0, 0 ), 90 - ( 317.5 / 180 ) * Speed ) ) )
}
if( ~RPM )
{
    holoAng( Tachometer, Tach:toWorld( ang( 90, 0, 0 ):rotateAroundAxis( vec( 1, 0, 0 ), 90 - ( 317.5 / 9000 ) * min( 9000, RPM ) ) ) )
}
if( ~Fuel )
{
    holoAng( FuelGauge, Fuel_Gauge:toWorld( ang( 90, 0, 0 ):rotateAroundAxis( vec( 1, 0, 0 ), 50 - ( 280 / Capacity ) * Fuel ) ) )
}
