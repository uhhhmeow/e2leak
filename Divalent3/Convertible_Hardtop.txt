@name Convertible Hardtop
@inputs Chassis:entity 
@outputs 
@persist 
@trigger 
interval(100)

if (first() | dupefinished()) {
    holoCreate(1)
    holoPos(1,Chassis:toWorld(vec(0,0,40)))
    holoAng(1,Chassis:toWorld(ang(0,0,0)))
    holoScale(1,vec(6,3,0.1))
    holoParent(1,0)
}
