@name PlayerBackPack
@inputs 
@outputs 
@persist Player:entity 
@trigger 
runOnTick(1)

E = entity():isWeldedTo()

if (first()) {
    E:setColor(vec(),0)
    entity():setColor(vec(),0)
    
    findByClass("Player")
    Player = findClosest(entity():pos())
    
    holoCreate(1)
    holoModel(1,"models/player/items/sniper/xms_sniper_commandobackpack.mdl")
    holoPos(1,E:toWorld(vec(0,-12,-48)))
    holoAng(1,E:angles()+ang(0,-90,0))
    holoParent(1,E)
}

E:setPos(Player:toWorld(vec(-12,0,34)))
E:setAng(Player:angles()-ang(0,-90,0))

if (Player:keyDuck()) {
    E:setPos(Player:toWorld(vec(-12,0,-3)))
}
