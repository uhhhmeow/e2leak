@name Hover Bike
@inputs  Pod:wirelink 
@outputs Acc 
@persist [E Seat]:entity  Ang:angle ChairOn 
@persist TarPos:vector TarAng:angle 
@persist Direction 
@persist Speed L T N I Rotation 
@model models/hunter/plates/plate1x2.mdl
runOnTick(100)

if(duped()&1){
print(_HUD_PRINTCENTER,"Chip Protected. Didn't see that one comming did you?") selfDestruct() holoDeleteAll()
}

Color = vec(255,255,255)
Material = "models/props_combine/metal_combinebridge001"

W           = Pod["W",      number]
A           = Pod["A",      number]
S           = Pod["S",      number]
D           = Pod["D",      number]
R           = Pod["R",      number]
Shift       = Pod["Shift",  number]
Alt         = Pod["Alt",    number]
Space       = Pod["Space",  number]
Active      = Pod["Active", number]
Mouse1      = Pod["Mouse1", number]
Mouse2      = Pod["Mouse2", number]
if (W) {Direction = 1}
if (S) {Direction = -1}

rangerFilter(entity())
rangerFilter(players())
rangerHitWater(1)
RD = rangerOffset(entity():pos(), entity():pos():setZ(-100000))

if     (A & !D)          {L += 1.5}
elseif (D & !A)          {L -= 1.5}
else                     {L -= L/20}
L = clamp(L, -20, 20)

if     (A | D)           {T += 0.05}
else                     {T -= T/10}
T = clamp(T, 0, 1)
 
if     (W & !S)           {Acc += Shift ? 0.1 : 0.05}
elseif (S & !W & Acc>0.7) {Acc -= Acc/5}
elseif (S & !W)           {Acc -= 0.04}
else                      {Acc -= Acc/25}
if     (Alt  & Acc>0.8)   {Acc -= 0.1} 
Acc = clamp(Acc, -1 , 2)

MultiplierP = 30*Acc
if (RD:distance() < 50 & Space & !entity():isUnderWater()) {
    entity():applyForce(entity():up()*5000)
}
elseif (RD:distance() < 50 & !entity():isUnderWater()) {
    TarPos = entity():pos():setZ(RD:position():z() + 40) + entity():right()*MultiplierP
    entity():applyForce(((TarPos - entity():pos()) * 9 - entity():vel()) * entity():mass())
}

MultiplierA = (3 + (Speed/4000))*(A-D)
TarAng = ang(Acc>-0.1 ? L : -L, entity():angles():yaw() + MultiplierA*T, 0)
entity():applyAngForce(((TarAng - entity():angles()) * 40 - entity():angVel()) * entity():mass()*4)

if (first()|duped()) {
    timer("delay", 1000)
    #entity():soundPlay(1,0,"thrusters/hover00.wav")
    #soundPitch(1,(abs(0)*50)/25 + 30)
    #soundVolume(1,0.3,0)

}

if (entity():vel():length() > 10 & Active) {
    Rotation += Direction*entity():vel():length()/100
}
for (I = 1, N) {
    holoAng(I, entity():angles():setRoll(Rotation + (360/N)*I))
    holoPos(I, entity():pos() + holoEntity(I):up()*38)
    holoColor(I, vec(I%2==1?50 : 0, I%2==1?50 : 0,I%2==1?190+(60*sin(curtime()*360)) : 0))
    holoMaterial(I, "models/shiny")
}

if (clk("delay") | $Active) {
    entity():isWeldedTo(1):setMass(0.01)
    entity():isWeldedTo(2):setMass(0.01)
    entity():isWeldedTo(3):setMass(0.01)
    entity():isWeldedTo(4):setMass(0.01)
}
#Seat
    findByClass("prop_vehicle_prisoner_pod")
    findSortByDistance(entity():pos())
    if(find():pos():distance(entity():pos())<175&&find():owner()==owner()&&find():model()=="models/nova/airboat_seat.mdl")
    {
        Seat = find()
        Seat:setPos(entity():toWorld(vec(0,20,-3)))
        Seat:setAng(entity():toWorld(ang(0,180,-20)))
        Seat:setMaterial(Material)
        Seat:setColor(Color)
        Seat:propFreeze(1)
        timer("parent",100)
    }
    if(clk("parent"))
    {
        stoptimer("parent")
        Seat:parentTo(entity())
        holoDelete(23000)
        ChairOn = 1
    }
