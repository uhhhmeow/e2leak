@name Overhead Radio
@persist [HelpStrings Musics]:array Vec:vector ChatSymbol:string
@model models/hunter/plates/plate1x1.mdl
interval(100)
 
# declaring terms
Epos=entity():pos()
Vec=owner():pos()+vec(0,0,-60+owner():height())
#

# putting E2 above owner's head
entity():setPos(Vec)
entity():setAng(ang(0,0,0))
entity():setColor(hsv2rgb((curtime() * 70) % 360, 1, 1))
#

# creates a trail and holograms, setting music list
if (first())
{
    
    ChatSymbol = "."
    
    owner():setTrails(40,4,4,"trails/lol",vec(255,0,135),255)

    HelpStrings = array()
    HelpStrings:pushString(".help - Brings up these hints")
    HelpStrings:pushString(".music N - Plays music number N")
    HelpStrings:pushString(".music stop - Stops playing")
    HelpStrings:pushString(".pitch N - Sets music pitch to N")

    Musics = array()
    Musics:pushString("music/hl2_song12_long.mp3")
    Musics:pushString("air-raid.wav")
    Musics:pushString("annoying.mp3")
    Musics:pushString("itsfriday.mp3")
    Musics:pushString("music/carousel_of_curses.wav")
    Musics:pushString("music/hl2_song23_suitsong3.mp3")
    Musics:pushString("player/brutal_legend_taunt.wav")
    
    for (I=1,HelpStrings:count())
    {
            hint(HelpStrings[I,string],5)
    }
}
#                     (startsize,endsize,length,material,color,alpha)
#                     (#,position,size,angles,color,model)

# chat control
runOnChat(1)

if (chatClk(owner()))
{

    LS = owner():lastSaid():lower()
    LSE = LS:sub(2,LS:length()):explode(" ")
    LS0 = LS:index(1)
    LS1 = LSE:string(1)
    LS2 = LSE:string(2)
    LS3 = LSE:string(3)
    LS4 = LSE:string(4)

    if (LS0==ChatSymbol)
    {
        hideChat(1)
        if (LS1=="help")
        {
            for (I=1,HelpStrings:count())
            {
                hint(HelpStrings[I,string],5)
            }
        }
        if (LS1=="music")
        {
            if (LS2=="stop")
            {
                soundStop(1,1)
                hint("Music stopped..",5)
            }
            else
            {
                N = LS2:toNumber()
                if (inrange(N,1,Musics:count()))
                {
                    soundPlay(1,300,Musics[N,string])
                    hint("Now playing.. '"+Musics[N,string]+"'",5)
                }
            }
        }
        elseif (LS1=="pitch")
        {
            N = LS2:toNumber()
            if (N>0)
            { 
                soundPitch(1,N)
                hint("soundPitch set to '"+N+"'",5)
            }
            else
            {
                soundPitch(1,100)
                hint("soundPitch has been reset",5)
            }
        }

    }

}
