@name Holo-Whip
@persist T 
interval(60)

Kinks = 8
KLength = 0.2
Thick = 0.1
Bend = 4

if (first()) {
    for (I2 = 1,Kinks) {
        holoCreate(I2)   
        holoModel(I2,"models/props_c17/oildrum001.mdl")
        holoScale(I2,vec(Thick,Thick,KLength))
        
        if (holoEntity(I2)) {
            T++
        } else {break}
    }
}

for (I2 = 1,Kinks) {
    if (I2 == 1) {
        holoPos(I2,entity():pos()-entity():forward()*6)    
        
        Ang = (holoEntity(I2+1):pos()-entity():pos()):toAngle()+ang(90,0,0)
        holoAng(I2,Ang)
    } elseif(I2 == Kinks) {
        holoPos(I2,holoEntity(I2-1):pos()+holoEntity(I2-1):up()*KLength*34)
        Ang = (holoEntity(I2-1):pos()-holoEntity(I2):pos()):toAngle()+ang(90,0,0)
        holoAng(I2,Ang)    
    } else {
        holoPos(I2,holoEntity(I2-1):pos()+holoEntity(I2-1):up()*KLength*34)
        Ang = (holoEntity(I2+1):pos()-holoEntity(I2-1):pos()):toAngle()+ang(90,0,0)
        holoAng(I2,Ang)
    }
}
