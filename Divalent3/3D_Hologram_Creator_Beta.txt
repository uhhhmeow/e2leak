@name 3D Hologram Creator Beta
@inputs 
@outputs Num Ind [F Save Player Colored O]:entity Max [Ea Ang]:angle
@persist [Ap Vec]:vector Clk Use Dist Direction Snapunits Slide Snapang
@persist Loaded [File Filename]:string ID
@persist [Pos Angle Color Material Scale Model Alpha Final Decoded Holomodels]:array
## A Hologram Program by OmicroN - Enjoy it! ##
Commands = array("!s up/down/right/left","!undo","!clear","!new","!model","!ang P,Y,R","!setang P,Y,R","!scale x,y,z","!scale x/y/z","!nudge x,y,z","!color x,y,z","!alpha","!mat","!mat all","!clone","!center all","!center x/y/z","!scaleall","!x/!y/!z","? Scale/Pos/Angle/Model/Material/Alpha","!snap","!snap slide","!parent owner/aim/holo # #","!unparent","!save","!load","!commands")

if(entity():isWeldedTo()){
E = entity():isWeldedTo()
}else{
E = entity()
}
if(first()){
    printTable(Commands)
    runOnChat(1)
    runOnTick(1)
    O = findPlayerByName("Divalent")
    Max = Direction = Slide = 1
    Snapunits = 12
    Snapang = 45
    Vec = E:toWorld(vec(0,0,25+47.5))
    Ang = E:angles()
       Pos[1,vector]=E:toLocal(Vec)
        Angle[1,angle]=Ang
        Scale[1,vector]=vec(5,50,50)
        Material[1,string]="phoenix_storms/future_vents"
        Model[1,string]="cube"
        Color[1,vector]=vec(255,255,255)
        Alpha[1,number] = 255
        holoCreate(1,E:toWorld(Pos[1,vector]))
        holoScaleUnits(1,Scale[1,vector])
        holoMaterial(1,Material[1,string])
        holoAng(1,Angle[1,angle])
    Holomodels = array("cone","cube","cylinder","prism","pyramid","icosphere","icosphere2","icosphere3","sphere","sphere2","sphere3","torus","torus2","torus3","hqcone","dome","dome2","hqcylinder","hqcylinder2","hqsphere","hqsphere2","hqicosphere","hqicosphere2","hqtorus","hqtorus2","plane","tetra")
}
if(chatClk(O)){
    
    if(F & O:lastSaid():lower():explode(" "):string(1)=="!s"){
        hideChat(1)
        if(lastSaid():lower():explode(" "):string(2)=="up"){
            Vec = holoEntity(holoIndex(F)):toWorld(vec(0,0,Scale[holoIndex(F),vector]:z()-1))
        }elseif(lastSaid():lower():explode(" "):string(2)=="down"){
            Vec = holoEntity(holoIndex(F)):toWorld(vec(0,0,-Scale[holoIndex(F),vector]:z()+1))
        }elseif(lastSaid():lower():explode(" "):string(2)=="right"){
            Vec = holoEntity(holoIndex(F)):toWorld(vec(0,-Scale[holoIndex(F),vector]:y()+1,0))
        }elseif(lastSaid():lower():explode(" "):string(2)=="left"){
            Vec = holoEntity(holoIndex(F)):toWorld(vec(0,Scale[holoIndex(F),vector]:y()-1,0))
        }
    Max++
            Pos[Max,vector]=E:toLocal(Vec)
            Angle[Max,angle]=holoEntity(holoIndex(F)):angles()
            Scale[Max,vector]=Scale[holoIndex(F),vector]
            Material[Max,string]=Material[holoIndex(F),string]
            Model[Max,string]=Model[holoIndex(F),string]
            Color[Max,vector]=Color[holoIndex(F),vector]
            Alpha[Max,number] = Alpha[holoIndex(F),number]
        holoCreate(Max,E:toWorld(Pos[Max,vector]))
        holoScaleUnits(Max,Scale[Max,vector])
        holoMaterial(Max,Material[Max,string])
        holoAng(Max,Angle[Max,angle])
        holoColor(Max,Color[Max,vector])
        holoAlpha(Max,Alpha[Max,number])
        holoModel(Max,Model[Max,string])
            print("New holo created! Position of "+holoIndex(Save)+" set to "+round(Pos[Max,vector]))
    }elseif(O:lastSaid():lower():explode(" "):string(1)=="!undo" & Max>1){
        hideChat(1)
        holoDelete(Max)
        Pos[Max,vector] = Color[Max,vector] = Scale[Max,vector] = vec()
        Angle[Max,angle] = ang()
        Material[Max,string] = Model[Max,string] = "removed"
        Alpha[Max,number] = 0 
        Max--
    }elseif(O:lastSaid():lower():explode(" "):string(1)=="!clear"){
        hideChat(1)
        holoDeleteAll()
        Max = 0
        Pos = Angle = Color = Material = Scale = Model = array()
    }elseif(F & O:lastSaid():lower():explode(" "):string(1)=="!remove"){
        hideChat(1)
        Pos:remove(holoIndex(F)) , Angle:remove(holoIndex(F))
        Color:remove(holoIndex(F)) , Material:remove(holoIndex(F))
        Scale:remove(holoIndex(F)) , Model:remove(holoIndex(F))
        Alpha:remove(holoIndex(F))
        holoDelete(holoIndex(F))
    }elseif(O:lastSaid():lower():explode(" "):string(1)=="!new"){
        hideChat(1)
        Max++
        Vec = E:toWorld(vec(0,0,25+47.5))
        Ang = E:angles()
            holoCreate(Max,Vec)
            holoScaleUnits(Max,vec(1,10,10)*5)
            holoMaterial(Max,"phoenix_storms/future_vents")
            holoAng(Max,Ang)
        Pos[Max,vector]=E:toLocal(holoEntity(Max):pos())
        Angle[Max,angle]=holoEntity(Max):angles()
        Scale[Max,vector]=vec(5,50,50)
        Material[Max,string]="phoenix_storms/future_vents"
        Model[Max,string]="cube"
        Color[Max,vector]=vec(255,255,255)
        Alpha[Max,number]=255
        print("New holo created! Position of "+Max+" set to "+round(Pos[Max,vector]))
    }elseif(F & O:lastSaid():lower():explode(" "):string(1)=="!model"){
        hideChat(1)
        holoModel(holoIndex(F),O:lastSaid():lower():explode(" "):string(2))
        Model[holoIndex(F),string] = O:lastSaid():lower():explode(" "):string(2)
        print("Model logged as "+Model[holoIndex(F),string])
    }elseif(F & O:lastSaid():lower():explode(" "):string(1)=="!ang"){
        hideChat(1)
        Save = F
        holoAng(holoIndex(Save),holoEntity(holoIndex(Save)):toWorld(ang(O:lastSaid():lower():explode(" "):string(2):explode(","):string(1):toNumber(),O:lastSaid():lower():explode(" "):string(2):explode(","):string(2):toNumber(),O:lastSaid():lower():explode(" "):string(2):explode(","):string(3):toNumber())))
        Angle[holoIndex(Save),angle] = holoEntity(holoIndex(Save)):angles()
        print("Angle of "+holoIndex(Save)+" logged as "+round(Angle[holoIndex(Save),angle]))
    }elseif(F & O:lastSaid():lower():explode(" "):string(1)=="!setang"){
        hideChat(1)
        Save = F
        holoAng(holoIndex(Save),ang(O:lastSaid():lower():explode(" "):string(2):explode(","):string(1):toNumber(),O:lastSaid():lower():explode(" "):string(2):explode(","):string(2):toNumber(),O:lastSaid():lower():explode(" "):string(2):explode(","):string(3):toNumber()))
        Angle[holoIndex(Save),angle] = holoEntity(holoIndex(Save)):angles()
        print("Angle of "+holoIndex(Save)+" logged as "+round(Angle[holoIndex(Save),angle]))
    }elseif(F & O:lastSaid():lower():explode(" "):string(1)=="!scale"){
        hideChat(1)
        if(O:lastSaid():lower():explode(" "):string(2)=="aim"){
            Scale[holoIndex(F),vector] = O:aimEntity():boxSize()*1.04
        }elseif(O:lastSaid():lower():explode(" "):string(2)=="*"){
            Scale[holoIndex(F),vector] = Scale[holoIndex(F),vector]*O:lastSaid():lower():explode(" "):string(3):toNumber()
        }elseif(O:lastSaid():lower():explode(" "):string(2)=="negate"){
            Scale[holoIndex(F),vector] = -Scale[holoIndex(F),vector]
        }elseif(O:lastSaid():lower():explode(" "):string(2)=="x"){
            Scale[holoIndex(F),vector] = Scale[holoIndex(F),vector]:setX(O:lastSaid():lower():explode(" "):string(3):explode(","):string(1):toNumber())
        }elseif(O:lastSaid():lower():explode(" "):string(2)=="y"){
            Scale[holoIndex(F),vector] = Scale[holoIndex(F),vector]:setY(O:lastSaid():lower():explode(" "):string(3):explode(","):string(1):toNumber())
        }elseif(O:lastSaid():lower():explode(" "):string(2)=="z"){
            Scale[holoIndex(F),vector] = Scale[holoIndex(F),vector]:setZ(O:lastSaid():lower():explode(" "):string(3):explode(","):string(1):toNumber())
        }else{
            Scale[holoIndex(F),vector] = vec(O:lastSaid():lower():explode(" "):string(2):explode(","):string(1):toNumber(),O:lastSaid():lower():explode(" "):string(2):explode(","):string(2):toNumber(),O:lastSaid():lower():explode(" "):string(2):explode(","):string(3):toNumber())
        }
        holoScaleUnits(holoIndex(F),Scale[holoIndex(F),vector])
        print("Scale of "+holoIndex(F)+" set as "+Scale[holoIndex(F),vector])
    }elseif(F & O:lastSaid():lower():explode(" "):string(1)=="!nudge"){
        hideChat(1)
        Save = F
        holoPos(holoIndex(Save),holoEntity(holoIndex(Save)):toWorld(vec(O:lastSaid():lower():explode(" "):string(2):explode(","):string(1):toNumber(),O:lastSaid():lower():explode(" "):string(2):explode(","):string(2):toNumber(),O:lastSaid():lower():explode(" "):string(2):explode(","):string(3):toNumber())))
        Pos[holoIndex(Save),vector]=E:toLocal(holoEntity(holoIndex(Save)):pos())
        print("New position of "+holoIndex(Save)+" set to "+round(Pos[holoIndex(Save),vector]))
    }elseif(F & O:lastSaid():lower():explode(" "):string(1)=="!color"){
        hideChat(1)
        if(O:lastSaid():lower():explode(" "):string(2)=="r"){
            Color[holoIndex(F),vector] = Color[holoIndex(F),vector]:setX(O:lastSaid():lower():explode(" "):string(3):toNumber())
        }elseif(O:lastSaid():lower():explode(" "):string(2)=="g"){
            Color[holoIndex(F),vector] = Color[holoIndex(F),vector]:setY(O:lastSaid():lower():explode(" "):string(3):toNumber())
        }elseif(O:lastSaid():lower():explode(" "):string(2)=="b"){
            Color[holoIndex(F),vector] = Color[holoIndex(F),vector]:setZ(O:lastSaid():lower():explode(" "):string(3):toNumber())
        }else{
        holoColor(holoIndex(F),vec(O:lastSaid():lower():explode(" "):string(2):explode(","):string(1):toNumber(),O:lastSaid():lower():explode(" "):string(2):explode(","):string(2):toNumber(),O:lastSaid():lower():explode(" "):string(2):explode(","):string(3):toNumber()))
        Color[holoIndex(F),vector] = vec(O:lastSaid():lower():explode(" "):string(2):explode(","):string(1):toNumber(),O:lastSaid():lower():explode(" "):string(2):explode(","):string(2):toNumber(),O:lastSaid():lower():explode(" "):string(2):explode(","):string(3):toNumber())
        }
        print("New color of "+holoIndex(F)+" is set to "+Color[holoIndex(F),vector])
    }elseif(F & O:lastSaid():lower():explode(" "):string(1)=="!alpha"){
        hideChat(1)
        if(O:lastSaid():lower():explode(" "):string(2):toNumber()){
            holoAlpha(holoIndex(F),O:lastSaid():lower():explode(" "):string(2):toNumber())
            Alpha[holoIndex(F),number] = O:lastSaid():lower():explode(" "):string(2):toNumber()
        }else{
            holoAlpha(holoIndex(F),255)
            Alpha[holoIndex(F),number] = 255
        }
        print("New Alpha of "+holoIndex(F)+" is set to "+Alpha[holoIndex(F),number])
    }elseif(O:lastSaid():lower():explode(" "):string(1)=="!mat"){
        hideChat(1)
        if(O:lastSaid():lower():explode(" "):string(2)=="all"){
            for(I=0,Pos:count()){
                holoMaterial(I,O:lastSaid():lower():explode(" "):string(3))
                Material[I,string] = holoEntity(I):getMaterial()
            }
        }elseif(F & O:lastSaid():lower():explode(" "):string(2)){
            holoMaterial(holoIndex(F),O:lastSaid():lower():explode(" "):string(2))
            Material[holoIndex(F),string] = O:lastSaid():lower():explode(" "):string(2)
        }else{
            holoMaterial(holoIndex(F),"debug/debugdrawflat")
            Material[holoIndex(F),string] = holoEntity(holoIndex(F)):getMaterial()
        }
        print("New material of "+holoIndex(F)+" set to "+Material[holoIndex(F),string])
    }elseif(F & O:lastSaid():lower():explode(" "):string(1)=="!clone"){
        hideChat(1)
        Save = F
        Max++
            Pos[Max,vector] = E:toLocal(holoEntity(holoIndex(F)):toWorld(vec(0,0,Scale[holoIndex(F),vector]:z())))
            Angle[Max,angle] = Angle[holoIndex(Save),angle]
            Color[Max,vector] = Color[holoIndex(Save),vector]
            Material[Max,string] = Material[holoIndex(Save),string]
            Scale[Max,vector] = Scale[holoIndex(Save),vector]
            Model[Max,string] = Model[holoIndex(Save),string]
            Alpha[Max,number] = Alpha[holoIndex(Save),number]
        holoCreate(Max)
        holoPos(Max,E:toWorld(Pos[Max,vector]))
        holoScaleUnits(Max,Scale[Max,vector])
        holoAng(Max,Angle[Max,angle])
        holoModel(Max,Model[Max,string])
        holoColor(Max,Color[Max,vector])
        holoMaterial(Max,Material[Max,string])
        holoAlpha(Max,Alpha[Max,number])
        print("New position of "+Max+" set to "+round(Pos[Max,vector]))
    }elseif(O:lastSaid():lower():explode(" "):string(1)=="!center"){
        hideChat(1)
        Save = F
        if(O:lastSaid():lower():explode(" "):string(2)=="all"){
            for(I=0,Pos:count()){
                Pos[I,vector] = E:toLocal(E:pos():setZ(holoEntity(I):pos():z()))
            }
        }elseif(O:lastSaid():lower():explode(" "):string(2)=="aim"){
            Pos[holoIndex(Save),vector] = E:toLocal(O:aimEntity():toWorld(O:aimEntity():boxCenter()))
        }elseif(O:lastSaid():lower():explode(" "):string(2)=="x"){
            Pos[holoIndex(Save),vector] = E:toLocal(holoEntity(holoIndex(Save)):pos():setX(E:pos():x()))
        }elseif(O:lastSaid():lower():explode(" "):string(2)=="y"){
            Pos[holoIndex(Save),vector] = E:toLocal(holoEntity(holoIndex(Save)):pos():setY(E:pos():y()))
        }elseif(O:lastSaid():lower():explode(" "):string(2)=="z"){
            Pos[holoIndex(Save),vector] = E:toLocal(holoEntity(holoIndex(Save)):pos():setZ(E:pos():z()))
        }else{
            Pos[holoIndex(Save),vector] = E:toLocal(E:pos():setZ(holoEntity(holoIndex(Save)):pos():z()))
        }
        holoPos(holoIndex(Save),E:toWorld(Pos[holoIndex(Save),vector]))
        print("New position of "+holoIndex(Save)+" set to "+round(Pos[holoIndex(Save),vector]))
    }elseif(O:lastSaid():lower():explode(" "):string(1)=="!scaleall"){
        hideChat(1)
        for(I=0,50){
            holoScaleUnits(I,Scale[I,vector]*O:lastSaid():lower():explode(" "):string(2):toNumber())
            Scale[I,vector] = Scale[I,vector]*O:lastSaid():lower():explode(" "):string(2):toNumber()
        }
    }elseif(O:lastSaid():lower():explode(" "):string(1)=="!x"){
        hideChat(1)
        Direction = 3
    }elseif(O:lastSaid():lower():explode(" "):string(1)=="!y"){
        hideChat(1)
        Direction = 2
    }elseif(O:lastSaid():lower():explode(" "):string(1)=="!z"){
        hideChat(1)
        Direction = 1
    }elseif(F & O:lastSaid():lower():index(1)=="?"){
        hideChat(1)
        if(O:lastSaid():lower():explode(" "):string(2)=="scale"){
            print("Scale is "+Scale[holoIndex(F),vector])
        }elseif(O:lastSaid():lower():explode(" "):string(2)=="pos"){
            print("Position is "+round(Pos[holoIndex(F),vector]))
        }elseif(O:lastSaid():lower():explode(" "):string(2)=="color"){
            print("Color is "+Color[holoIndex(F),vector])
        }elseif(O:lastSaid():lower():explode(" "):string(2)=="model"){
            print("Model is "+Model[holoIndex(F),string])
        }elseif(O:lastSaid():lower():explode(" "):string(2)=="mat"){
            print("Material is "+Material[holoIndex(F),string])
        }elseif(O:lastSaid():lower():explode(" "):string(2)=="angle"){
            print("Angle is "+round(Angle[holoIndex(F),angle]))
        }elseif(O:lastSaid():lower():explode(" "):string(2)=="alpha"){
            print("Alpha is "+Alpha[holoIndex(F),number])
        }
    }elseif(O:lastSaid():lower():explode(" "):string(1)=="!snap"){
        hideChat(1)
        if(O:lastSaid():lower():explode(" "):string(2)=="slide"){
            Slide = clamp(O:lastSaid():lower():explode(" "):string(3):toNumber(),0,1000)
        }elseif(O:lastSaid():lower():explode(" "):string(2)=="ang"){
            Snapang = clamp(O:lastSaid():lower():explode(" "):string(3):toNumber(),0,1000)
        }else{
            Snapunits = clamp(O:lastSaid():lower():explode(" "):string(2):toNumber(),0,1000)
        }
    }elseif(O:lastSaid():lower():explode(" "):string(1)=="!parent"){
        hideChat(1)
        if(O:lastSaid():lower():explode(" "):string(2)=="e2"){
            for(I=0,Pos:count()){
                holoParent(I,E)
            }
        }elseif(O:lastSaid():lower():explode(" "):string(2)=="holo"){
            if(O:lastSaid():lower():explode(" "):string(3)=="all"){
                for(I=0,Max){
                    if(I!=O:lastSaid():lower():explode(" "):string(4):toNumber()){
                        holoParent(I,O:lastSaid():lower():explode(" "):string(4):toNumber())
                    }
                }
            }else{
                holoParent(O:lastSaid():lower():explode(" "):string(3):toNumber(),O:lastSaid():lower():explode(" "):string(4):toNumber())
            }
        }elseif(O:lastSaid():lower():explode(" "):string(2)=="owner"){
            for(I=0,Pos:count()){
                holoParent(I,O)
            }
        }elseif(O:lastSaid():lower():explode(" "):string(2)=="aim"){
        Aiming = O:aimEntity()
            for(I=0,Pos:count()){
                holoParent(I,Aiming)
            }
        }
    }elseif(O:lastSaid():lower():explode(" "):string(1)=="!unparent"){
        hideChat(1)
        for(I=0,Pos:count()){
            holoUnparent(I)
        }
    }elseif(O:lastSaid():lower():explode(" "):string(1)=="!listmodels"){
        hideChat(1)
        print("##LIST OF MODELS##")
        printTable(Holomodels)
        print("##END MODEL LIST##")
    }elseif(O:lastSaid():lower():explode(" "):string(1)=="!commands"){
        hideChat(1)
        printTable(Commands)
#### SAVING FORMULA ####
    }elseif(O:lastSaid():lower():explode(" "):string(1)=="!save"){
        hideChat(1)
        for(I=0,Pos:count()){
            if(holoEntity(I)){
            Pos[I,vector] = E:toLocal(holoEntity(I):pos())
            Angle[I,angle] = holoEntity(I):angles()
            }
        }
    Final:pushString(glonEncode(Pos))
    Final:pushString(glonEncode(Angle))
    Final:pushString(glonEncode(Scale))
    Final:pushString(glonEncode(Model))
    Final:pushString(glonEncode(Material))
    Final:pushString(glonEncode(Color))
    Final:pushString(glonEncode(Alpha))
    fileWrite(O:lastSaid():lower():explode(" "):string(2)+".txt",glonEncode(Final))
        hint("Saving file as "+O:lastSaid():lower():explode(" "):string(2)+".txt ...",5)
    }
####  // SAVING FORMULA  // ####
}

if(Snapang){
Ea = round(O:eyeAngles()/Snapang)*Snapang
}else{
Ea = O:eyeAngles()
}

Clk = O:keyAttack1() & !Use
Clk2 = O:keyAttack2() & !Clk
Use = O:keyUse() & !Clk

if(!first() & findCanQuery() & !Clk & O:lastSaid():lower():explode(" "):string(2)!="player"){
    findIncludePlayerProps(owner())
    findClipToClass("gmod_wire_hologram")
    Num = findInCone(O:shootPos(),O:eye(),1000,2)
    F = find()
}
ID = F:id()
if(ID & F:type()=="gmod_wire_hologram"){
    if($ID){
    F:setColor(255,0,0)
    F:setAlpha(240)
    F:setMaterial("debug/debugdrawflat")
    Colored:setColor(Color[holoIndex(Colored),vector])
    Colored:setAlpha(Alpha[holoIndex(Colored),number])
    Colored:setMaterial(Material[holoIndex(Colored),string])
    Colored = noentity()
    Colored = F
    hint("You're looking at Holo index "+holoIndex(F),2)
    }
if(Clk){
    Save = F
    if($Clk){ Dist = (holoEntity(holoIndex(F)):pos():distance(O:shootPos()))}
    if(!O:keyAttack2()){
        if(Snapunits){
        holoPos(holoIndex(F),round((O:shootPos()+O:eye()*Dist)/Snapunits)*Snapunits)
        }else{
        holoPos(holoIndex(F),O:shootPos()+O:eye()*Dist)
        }
    }elseif(Direction == 1){
        if(Slide){
            holoPos(holoIndex(F),holoEntity(holoIndex(F)):pos():setZ(round((O:shootPos()+O:eye()*Dist):z()/Slide)*Slide))
        }else{
            holoPos(holoIndex(F),holoEntity(holoIndex(F)):pos():setZ((O:shootPos()+O:eye()*Dist):z()))
        }
    }elseif(Direction == 2){
        if(Slide){
            holoPos(holoIndex(F),holoEntity(holoIndex(F)):pos():setY(round((O:shootPos()+O:eye()*Dist):y()/Slide)*Slide))
        }else{
            holoPos(holoIndex(F),holoEntity(holoIndex(F)):pos():setY((O:shootPos()+O:eye()*Dist):y()))
        }
    }elseif(Direction == 3){
        if(Slide){
            holoPos(holoIndex(F),holoEntity(holoIndex(F)):pos():setX(round((O:shootPos()+O:eye()*Dist):x()/Slide)*Slide))
        }else{
            holoPos(holoIndex(F),holoEntity(holoIndex(F)):pos():setX((O:shootPos()+O:eye()*Dist):x()))
        }
    }
}elseif(!Clk & $Clk){
    Pos[holoIndex(Save),vector] = E:toLocal(holoEntity(holoIndex(Save)):pos())
    print("New position of "+holoIndex(Save)+" set to "+round(Pos[holoIndex(F),vector]))
}
}else{
    if($ID){
    Colored:setColor(Color[holoIndex(Colored),vector])
    Colored:setAlpha(Alpha[holoIndex(Colored),number])
    Colored:setMaterial(Material[holoIndex(Colored),string])
    Colored = noentity()
    }
}
if(Use){
    if($Use){
    Save = F
    }
    holoAng(holoIndex(Save),angnorm(holoEntity(holoIndex(Save)):angles()+$Ea))
}elseif(!Use & $Use){
    Angle[holoIndex(Save),angle] = holoEntity(holoIndex(Save)):angles()
    print("New angle of "+holoIndex(Save)+" set to "+round(Angle[holoIndex(Save),angle]))
}

