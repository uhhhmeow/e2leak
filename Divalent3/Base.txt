@name Base
@persist [E P Dri]:entity Active_i PWrk VSV Ign VelLL Mul Ntrpl HoverHeight FlyAng SpeedMultiplier YawSpeed
@model models/hunter/plates/plate1x2.mdl
runOnTick(1)

entity():setColor(vec(), 0)

if(duped() | dupefinished())
{
    reset()
}

if(first())
{
    Ntrpl = 90/(500/33) #If your hovercraft is spazzing out, try lowing that 90 in the interval (Line 4) and this line here. Warning: Decreasing the interval will increase the ops usage.
    
    E = entity()
    P = entity():isConstrainedTo()
    
    E:setMass(5000)
    E:propFreeze(0)
    
    lightCreate(1)
    lightColor(1,vec(0,155,155))
    lightParent(1,E)
    lightToggle(1,0)
    
    #rangerHitWater(1)
    rangerHitEntities(0)
    rangerPersist(1)
	
    #Settings:
    HoverHeight = 30 #Self-explanitory
    FlyAng = -35 #Angle at which you want to fly towards when you hold Space
    SpeedMultiplier = 0.5 #Mess with this number if you wanna fly fast
    YawSpeed = 10 #Speed at which the hovercraft turns left and right
    
holoCreate(1)
holoModel(1,"models/props_combine/bustedarm.mdl")
holoMaterial(1,"models/props_combine/metal_combinebridge001")
holoAng(1,ang(-30,-67,50))
holoPos(1,entity():toWorld(vec(-16,-16,-40)))
holoParent(1,entity())
holoClipEnabled(1,1)
holoClip(1,vec(0,0,35),vec(0,0,-1),0)

holoCreate(2)
holoModel(2,"models/props_combine/combine_smallmonitor001.mdl")
holoAng(2,ang(-35,90,0))
holoPos(2,entity():toWorld(vec(0,-10,-5)))
holoParent(2,entity())

holoCreate(3)
holoModel(3,"models/props_lights/light_fluorescent_basement.mdl")
holoAng(3,ang(0,0,28))
holoPos(3,entity():toWorld(vec(0.2,5,6.8)))
holoScale(3,vec(0.8,0.95,1))
holoParent(3,entity())

holoCreate(4)
holoModel(4,"models/props_lights/light_fluorescent_basement.mdl")
holoAng(4,ang(0,0.5,3))
holoPos(4,entity():toWorld(vec(-1,58,6.2)))
holoScale(4,vec(0.2,0.4,1))
holoParent(4,entity())
} 

if(PWrk)
{
    Dri = P:driver()
    Act = P:driver():isValid()
    
    if(Act)
    {
        W = Dri:keyForward()
        A = Dri:keyLeft()
        S = Dri:keyBack()
        D = Dri:keyRight()
        Shi = Dri:keySprint()
        Alt = Dri:keyWalk()
        Spc = Dri:keyJump()
        R = Dri:keyReload()
        M1 = Dri:keyAttack1()
        M2 = Dri:keyAttack2()
        
        if(changed(R) & R)
        {
            Ign = !Ign            
            lightToggle(1,Ign ? 1 : 0)
            soundStop("fail")
            soundPlay("ignv",Ign ? 0.874 : 1.13, Ign ? "hl1/fvox/activated.wav" : "hl1/fvox/deactivated.wav")
        } else {
    }
}
    
    Mul += Ign ? 0.0125 : -0.0125
    Mul = clamp(Mul,0,1)
    
    if(changed(P))
    {
        PWrk = P:isValid()
    }
    
    VelLL = E:vel():length()
    DV = $VelLL
}else
{
    findByClass("prop_vehicle_prisoner_pod")
    findSortByDistance(E:pos())
    if(find():model() == "models/nova/airboat_seat.mdl" & find():owner() == owner())
    {
        P = find()
        P:setPos(E:toWorld(vec(0,20,-3))) #Mess with that vector to change the offset position of the chair
        P:setAng(E:toWorld(ang(0,180,-20))) #If you DO want your chair to face backwards or something, go ahead and mess with this
        
        timer("parent",20)
    }
    if(clk("parent"))
    {
        stoptimer("parent")
        
        P:parentTo(E)
        
        rangerFilter(array(E,P))
        
        PWrk = 1
    }
}

if(Ign)
{
    VelL = E:velL()
    VSV = clamp(-E:velL():y()/7.5,-1500,1500)
    
    GR = rangerOffset(HoverHeight, E:pos() + (E:right() * clamp(VSV,0,1500)), vec(0,0,-1))
    FR = rangerOffset(25 + clamp(VSV,0,1500), mix(E:pos(), GR:pos(), 0.5), E:right())
    
    applyForce(((vec(0,0,(HoverHeight - GR:distance()) * GR:hit()))*0.75 + (E:right() * (W-S) * SpeedMultiplier * (10 * 1 + Spc*3 +  Shi*4 + FR:hit() * 3)) - E:toWorldAxis(VelL * vec(0.025,0.006125,0.045))) * E:mass() * Ntrpl)
    applyAngForce(((ang(E:angVel():yaw() / 6 - (2 * !GR:hit()), (A-D) * YawSpeed, ((Spc * FlyAng * (W-S)) + clamp((E:angles():roll() - 35) * FR:hit() * W,-90,0))  * (W|S))  - E:angles()*ang(1,0,1))*5 - E:angVel()/ang(2.5,2.5,1.5)) * E:mass() * Ntrpl)
    applyAngForce(((ang(E:angVel():yaw() / 6 - (2 * !GR:hit()), (A-D) * YawSpeed, ((Shi * -FlyAng * (W-S)) + clamp((E:angles():roll() - 35) * FR:hit() * W,-90,0))  * (W|S))  - E:angles()*ang(1,0,1))*5 - E:angVel()/ang(2.5,2.5,1.5)) * E:mass() * Ntrpl)
}
    if(DV < -10000) #Mess with the number if you wanna change the resistence to crashing
    {
        Ign = 0
        soundStop("ignv")
        soundPlay("crash",0.8,"ambient/explosions/explode_3.wav")
    }

