@name Airship
@inputs Pod:wirelink 
@outputs Gyro:angle 
@persist 
@trigger 
runOnTick(1)

if (first()) {
    function entity:goto(Vector:vector)
    {
        This:setMass(10)
        This:applyForce(((Vector-This:pos())*10-This:vel())*This:mass())   
    }
    E = entity():isWeldedTo()
    
    holoCreate(1)
    holoModel(1,"hq_hdome")
    holoPos(1,E:toWorld(vec(350,0,220)))
    holoAng(1,E:angles()+ang(90,0,0))
    holoScale(1,vec(29.965,29.965,50))
    holoParent(1,E)
    
    holoCreate(2)
    holoModel(2,"hq_tube")
    holoPos(2,E:toWorld(vec(50,0,220)))
    holoAng(2,E:angles()+ang(90,0,0))
    holoScale(2,vec(30,30,100))
    holoParent(2,E)
    
    holoCreate(3)
    holoModel(3,"hq_hdome")
    holoPos(3,E:toWorld(vec(-250,0,220)))
    holoAng(3,E:angles()+ang(-90,0,0))
    holoScale(3,vec(29.965,29.965,100))
    holoParent(3,E)
    
    holoCreate(4)
    holoModel(4,"models/hunter/geometric/hex025x1.mdl")
    holoPos(4,E:toWorld(vec(-545,-11,220)))
    holoAng(4,E:angles()+ang(90,90,0))
    holoScale(4,vec(5,5,5))
    holoParent(4,E)
    
    holoCreate(5)
    holoModel(5,"models/hunter/geometric/hex025x1.mdl")
    holoPos(5,E:toWorld(vec(-545,-11,220)))
    holoAng(5,E:angles()+ang(0,90,0))
    holoScale(5,vec(5,5,5))
    holoParent(5,E)
    
for (I=4,5) {
    holoMaterial(I,"models/debug/debugwhite")
    }
}

local Seat = Pod["Entity",entity]
local Driver = Seat:driver()
local W = Driver:keyForward()
local A = Driver:keyLeft()
local S = Driver:keyBack()
local D = Driver:keyRight()
local R = Driver:keyReload()
local Mouse1 = Driver:keyAttack1()
local Mouse2 = Driver:keyAttack2()
local Shift = Driver:keySprint()
local Space = Driver:keyJump()
local Active = Driver:isPlayer()
local On = Active

if (Space) {
    Seat:applyForce(vec(0,0,100))
}
if (W) {
    Seat:applyForce(vec(0,0,0))
}

