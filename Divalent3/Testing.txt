@name Testing
@inputs [Pod Cam]:wirelink 
@outputs 
@persist Rotation AbleJump 
@trigger 
interval(16)

E = entity():isWeldedTo()
Height = 60

local Seat = Pod["Entity",entity]
local Driver = Seat:driver()
local Active = Driver:isPlayer()
local W = Driver:keyForward()
local A = Driver:keyLeft()
local S = Driver:keyBack()
local D = Driver:keyRight()
local Space = Driver:keyJump()
local Shift = Driver:keySprint()
local M1 = Driver:keyAttack1()
local M2 = Driver:keyAttack2()
Cam["Activated", number] = Active

if (first() | dupefinished()) {
    holoCreate(1)
    holoModel(1,"models/xeon133/offroad/off-road-30.mdl")
    holoAng(1,E:toWorld(ang(0,90,0)))
    holoParent(1,E)
    
    holoCreate(2)
    holoPos(2,E:toWorld(vec(0,0,0)))
    holoAng(2,E:toWorld(ang(0,180,0)))
    holoAlpha(2,0)
    holoParent(2,E)
    
    holoCreate(3)
    holoPos(3,holoEntity(2):toWorld(vec(-30,0,40)))
    holoAlpha(3,0)
    holoParent(3,2)
    
    Offset = holoEntity(3):pos()
    Self = holoEntity(3)
    Cam["Parent", entity] = Self
    Cam["Position", vector] = Offset
}

rangerFilter(E)
rangerFilter(players())
Rng = rangerOffset(Height,E:pos(),-E:up())

rangerFilter(E)
Ranger = rangerOffset(999999,entity():pos(),Driver:eye())

holoPos(1,Rng:pos() + vec(0,0,17.030203))

Yaw = (Ranger:position() - entity():pos()):toAngle():yaw()

ToR = (toUnit("mph",(entity():velL():y())))
Rotation = Rotation + ToR

if ((W-S)) {holoAng(1,entity():toWorld(ang(0,0,-Rotation)))}

if (Rng:hit()) {
    Diff = Height - Rng:distance()
    E:applyForce((vec(0,0,Diff * 25) - E:vel()) * E:mass() + E:forward()*(S-W) * E:mass() * 150 * (2 + Shift))
    
    AbleJump = 1
}

if (AbleJump == 1 & changed(Space) & Space) {
    Diff = 0
    E:applyForce(E:up() * E:mass()*210)
    
    AbleJump = 0
} 

E:applyAngForce((E:toLocal(ang((S-W)*6 * (1 + Shift),Yaw + 180,0))*200 - E:angVel()*20) * shiftL(ang(E:inertia())))

