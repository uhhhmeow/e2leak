@name KronsChairThing
@inputs Pod:wirelink 
@outputs 
@persist T S 
@trigger 
#runOnTick(1)
interval(100)

if (first()) {
    holoCreate(1)
    holoModel(1,"models/hunter/misc/lift2x2.mdl")
    holoPos(1,entity():isWeldedTo():toWorld(vec(0,-20,6.4)))
    holoScale(1,vec(0.25,0.5,0.45))
    holoAng(1,ang(-90,0,0))
    holoParent(1,entity():isWeldedTo())
    
    holoCreate(2)
    holoModel(2,"models/hunter/misc/shell2x2a.mdl")
    holoPos(2,entity():isWeldedTo():toWorld(vec(0,1,18)))
    holoAng(2,ang(0,90,0))
    holoScale(2,vec(0.45,0.45,0.45))
    holoParent(2,entity():isWeldedTo())
    
    holoCreate(3)
    holoPos(3,entity():isWeldedTo():toWorld(vec(0,19.74,17.65)))
    holoScale(3,vec(0.5,3,0.1))
    holoAng(3,ang(0,0,0))
    holoParent(3,entity():isWeldedTo())
    
    holoCreate(4)
    holoPos(4,entity():isWeldedTo():toWorld(vec(0,-16.96,17.65)))
    holoScale(4,vec(0.5,3,0.1))
    holoAng(4,ang(0,0,0))
    holoParent(4,entity():isWeldedTo())
}
