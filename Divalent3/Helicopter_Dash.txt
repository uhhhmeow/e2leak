@name Helicopter Dash
@inputs [BladeInd]:entity 
@outputs 
@persist 
@trigger 

if (first() | dupefinished()) {
    holoCreate(1)
    holoModel(1,"models/wingf0x/isasocket.mdl")
    holoPos(1,BladeInd:toWorld(vec(0,0,1.4)))
    holoAng(1,BladeInd:toWorld(ang(0,0,0)))
    holoScale(1,vec(1,0.4,0.6))
    holoParent(1,BladeInd)
    
    holoCreate(2)
    holoModel(2,"models/wingf0x/isasocket.mdl")
    holoPos(2,BladeInd:toWorld(vec(0,0,1.3)))
    holoAng(2,BladeInd:toWorld(ang(0,90,0)))
    holoScale(2,vec(1,0.4,0.6))
    holoParent(2,BladeInd)
}
