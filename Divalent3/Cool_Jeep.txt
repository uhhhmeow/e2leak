@name Cool Jeep
@inputs 
@outputs 
@persist 
@trigger 
interval(100)

if (owner():eyeTrace():position():distance(entity():pos()) > 5) {
    entity():propNotSolid(1)
} else {
    entity():propNotSolid(0)
}

E = entity():isWeldedTo()

if (first()) {
    holoCreate(1)
    holoPos(1,E:toWorld(vec(0,20,30)))
    holoAng(1,E:toWorld(ang(0,0,-2.4)))
    holoScale(1,vec(3,5,1.6))
    holoColor(1,vec(0,0,0))
    holoParent(1,E)
}
