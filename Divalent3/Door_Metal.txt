@name Door Metal
@inputs [D1 D2 Base]:entity 
@outputs User:entity 
@persist A State O [CornerPos1 CornerPos2]:vector 
@trigger 
runOnTick(1)

if (first() | dupefinished()) {
    entity():setAlpha(0)
    
    holoCreate(1)
    holoPos(1,D1:toWorld(vec(24,0,0)))
    
    holoCreate(2)
    holoPos(2,D2:toWorld(vec(-24,0,0)))
    
    D1:parentTo(holoEntity(1))
    D2:parentTo(holoEntity(2))
    
    Scale = vec(5,8,5) * 12
    
    Door = holoCreate(3)
    
    holoScaleUnits(3,Scale)
    holoPos(3,Base:toWorld(vec(0,0,50)))
    holoAng(3,Base:angles())
    holoColor(3,vec(255,255,255),255)
    holoParent(3,entity())
    
    CornerPos1 = Scale / 2
    CornerPos2 = -CornerPos1
    
    CornerPos1 = Door:toWorld(CornerPos1)
    CornerPos2 = Door:toWorld(CornerPos2)
    
    CornerScale = vec() + 1
    
    holoCreate(4)
    holoPos(4,CornerPos1)
    holoScaleUnits(4,CornerScale)
    holoParent(4, Door)
    
    holoCreate(5)
    holoPos(5,CornerPos2)
    holoScaleUnits(5,CornerScale)
    holoParent(5, Door)
    
    for (I = 1,5) {holoColor(I,vec(),0)}
}

findInBox(CornerPos1,CornerPos2)
findIncludeClass("player")
User = findClosest(holoEntity(1):pos())

if(User:isPlayer()) {State = 1}
else {State = 0}

if(State == 1) {
    if(A < 200) {
        A += 2
        holoAng(1,holoEntity(1):angles() + ang(0,-1,0))
        holoAng(2,holoEntity(2):angles() + ang(0,1,0))
    }
} else {
    if(A > 0) {
        A -= 2
        holoAng(1,holoEntity(1):angles() + ang(0,1,0))
        holoAng(2,holoEntity(2):angles() + ang(0,-1,0))
    }
}

if (changed(State == 1) & State == 1) {
    soundPlay(1,0,"doors/generic_door_open.wav")
    soundStop(2)
}

if (changed(State == 0) & State == 0) {
    soundStop(1)
}

if (State == 0 & changed(D1:angles() <= ang(0,-91,0))) {
    soundPlay(2,0,"doors/generic_door_close.wav")
}
