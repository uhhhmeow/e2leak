@name Teleport
@inputs [Tp Prop]:entity 
@outputs User:entity 
@persist 
@trigger 
runOnTick(1)

findByClass("Player")
User=findClosest(Prop:pos())

if (User:aimEntity() == Prop & User:keyUse()) {
    User:teleport(Tp:pos())
}
