@name KeyHole
@inputs [Key Stand]:entity 
@outputs Fade In:entity 
@persist A [CornerPos1 CornerPos2]:vector 
@trigger 
#interval(100)
runOnTick(1)

findInBox(CornerPos1,CornerPos2)
findIncludeModel("models/hunter/plates/plate.mdl")
In = findClosest(holoEntity(1):pos())

if (first()) {
    Scale = vec(0.5,0.5,0.5) * 12
    
    Door = holoCreate(1)
    
    holoScaleUnits(1,Scale)
    holoPos(1,entity():toWorld(vec(0,0,2)))
    holoAng(1,entity():angles())
    holoColor(1,vec(255,255,255),255)
    holoParent(1,entity())
    
    CornerPos1 = Scale / 2
    CornerPos2 = -CornerPos1
    
    CornerPos1 = Door:toWorld(CornerPos1)
    CornerPos2 = Door:toWorld(CornerPos2)
    
    CornerScale = vec() + 1
    
    holoCreate(2)
    holoPos(2,CornerPos1)
    holoScaleUnits(2,CornerScale)
    holoParent(2, Door)
    
    holoCreate(3)
    holoPos(3,CornerPos2)
    holoScaleUnits(3,CornerScale)
    holoParent(3, Door)
    
    for (I = 1,3) {holoColor(I,vec(),0)}
}

if(In:isValid()) {A = 1}
else {A = 0}

if(changed(A) & !first()) {
    if(A == 1) {
        Fade = 1
        timer("wait",5000)
        timer("tp",2500)
    }
}
if (clk("wait")) {
    Fade = 0
}
if (clk("tp")) {
    Key:teleport(Stand:pos()+vec(0,0,2))
}

findInSphere(owner():pos(),60)
Find = find()

if (Find == Key & owner():keyWalk()) {
    Attach = 1
}

if (Attach == 1) {
    Key:setPos(owner():attachmentPos("chest"))
    Key:setAng(owner():attachmentAng("chest"))
} #[elseif (Attach == 0) {
    Key:setPos(owner():pos()+vec(0,0,68))
    Key:setAng(owner():angles())
    Key:propFreeze(0)
}
