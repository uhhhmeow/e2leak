#[
Scriptis' ApplyForceLib
    Entity:move(Vector)
    Entity:rotate(Angle)
    Works best on an interval(100)
]#
@persist Lib_Force_DeltaTable:table
@outputs Lib_Force_Ent:entity Lib_Force_Force:vector
if (first()) {
    function number entity:move(Vec:vector) {
        local CurPos=This:pos()
        local ThisID="v"+toString(This:id())
        local TargetForce=Vec-CurPos
        local LastPos=Lib_Force_DeltaTable[ThisID,vector]
        if (LastPos:length()==0) {LastPos=CurPos}
        local Delta=LastPos-CurPos
        Lib_Force_DeltaTable[ThisID,vector]=TargetForce
        This:applyForce(((Delta+CurPos)-This:vel())*This:mass()*(TargetForce:length()>0.1))
        Lib_Force_Ent=This
        Lib_Force_Force=((Delta+CurPos)-This:vel())*This:mass()*(TargetForce:length()>0.1)
        return 1
    }
    function number entity:rot(Ang:angle) {
        local Ang=(Ang-This:angles())
        #local Ang=ang(Ang:pitch()%180,Ang:yaw()%180,Ang:roll()%180)
        local ThisID="a"+This:id()
        local LastAng=Lib_Force_DeltaTable[ThisID,angle]
        if (LastAng:pitch()+LastAng:yaw()+LastAng:roll()==0) {LastAng=Ang}
        Lib_Force_DeltaTable[ThisID,angle]=Ang
        local Delta=LastAng-Ang
        This:applyAngForce(((Delta+Ang-This:angVel()))*This:mass())
        return 1
    }
}
