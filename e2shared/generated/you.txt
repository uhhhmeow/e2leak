@name you_Generated
@persist E:entity HOLO_INDEX
@trigger all

## FUNCTIONS ##
if(first() || duped()){

   function void createHolo(Model:string,Scl:vector,Pos:vector,Ang:angle,Color:vector,Mat:string){

         holoCreate(HOLO_INDEX)
         holoModel(HOLO_INDEX,Model)
         holoPos(HOLO_INDEX,Pos)
         holoAng(HOLO_INDEX,Ang)
         holoScale(HOLO_INDEX,Scl)
         holoParent(HOLO_INDEX,E)
         holoColor(HOLO_INDEX,Color)
         holoMaterial(HOLO_INDEX,Mat)

         if(holoEntity(HOLO_INDEX):model() != Model){
               holoAlpha(HOLO_INDEX,0)
         }
         HOLO_INDEX+=1
   }
}

if(first() || duped()){
   E = entity()

   ### GENERATED CODE ###
   createHolo("models/props_interiors/furniture_couch01a.mdl",vec(1),E:toWorld(vec(-10.68,-34.21,-21.13)),E:toWorld(ang(0.83,-134.34,-0.02)),vec(255,255,255),"")
   createHolo("models/props_lab/blastdoor001c.mdl",vec(1),E:toWorld(vec(-34.03,9.13,-42.64)),E:toWorld(ang(-0.14,-43,0)),vec(255,255,255),"")
   createHolo("models/props_lab/blastdoor001c.mdl",vec(1),E:toWorld(vec(31.05,-6.06,-42.55)),E:toWorld(ang(0,179.96,0)),vec(255,255,255),"")
   createHolo("models/combine_helicopter/helicopter_bomb01.mdl",vec(1),E:toWorld(vec(11.62,28.86,6.5)),E:toWorld(ang(-1.08,-112.51,3.46)),vec(255,255,255),"")
}
