@name lamp_Generated
@persist E:entity HOLO_INDEX
@trigger all

## FUNCTIONS ##
if(first() || duped()){

   function void createHolo(Model:string,Scl:vector,Pos:vector,Ang:angle,Color:vector,Mat:string){

         holoCreate(HOLO_INDEX)
         holoModel(HOLO_INDEX,Model)
         holoPos(HOLO_INDEX,Pos)
         holoAng(HOLO_INDEX,Ang)
         holoScale(HOLO_INDEX,Scl)
         holoParent(HOLO_INDEX,E)
         holoColor(HOLO_INDEX,Color)
         holoMaterial(HOLO_INDEX,Mat)

         if(holoEntity(HOLO_INDEX):model() != Model){
               holoAlpha(HOLO_INDEX,0)
         }
         HOLO_INDEX+=1
   }
}

if(first() || duped()){
   E = entity()

   ### GENERATED CODE ###
   createHolo("",vec(1),E:toWorld(vec(nan,nan,nan)),E:toWorld(ang(0,0,0)),vec(0,0,0),"")
}