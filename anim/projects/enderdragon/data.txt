@name anim/projects/enderdragon/data
@persist [HN CN] [HT CT]:table


#----------------------
#-- GTable Setup
local ID = "enderdragon"

XGT = gTable("_holocore", 0)

XGT[ID, table] = table(
    "Clips" = table(),
    "Holos" = table()
)

HT = XGT[ID, table]["Holos", table]
CT = XGT[ID, table]["Clips", table]

local Vec = vec()
local Ang = ang()

local MAT_A = "phoenix_storms/construct/concrete_barrier00"
local PSPRITE = "sprites/soul"

local BLACK = vec4(30,30,30,255)
local GRAY = vec4(150,150,150,255)
local WHITE = vec4(200,200,200,255)
local RED = vec4(255,0,0,255)
local PURPLE_A = vec4(224,121,250,255)
local PURPLE_B = vec4(204,0,250,255)

#----------------------
#--Bones
local Skeleton = 0

local Scale = vec(12,12,12)/2
local Color = vec4(255,255,255,255)#Skeleton*255)
local Material = ""
local Model = ""

local Vec = vec()
local Ang = ang()


#----------------------
#-- Body Joints
HN++,HT[HN,table]=table(1,999,1,vec(0,0,0),ang(),vec(),"sphere","debug/debugdrawflat",RED)
HN++,HT[HN,table]=table(2,2,1,vec(45,0,0),ang(),vec(),"sphere","debug/debugdrawflat",RED)
HN++,HT[HN,table]=table(3,3,1,vec(-45,0,0),ang(),vec(),"sphere","debug/debugdrawflat",RED)

#----------------------
#-- Neck Joints
HN++,HT[HN,table]=table(4,2,1,vec(28.5+5,0,0),ang(),vec(24,23,23),"",MAT_A,BLACK)
HN++,HT[HN,table]=table(5,4,1,vec(23,0,0),ang(),vec(24,23.5,23.5),"",MAT_A,BLACK)
HN++,HT[HN,table]=table(6,5,1,vec(23,0,0),ang(),vec(24,23,23),"",MAT_A,BLACK)
HN++,HT[HN,table]=table(7,6,1,vec(23,0,0),ang(),vec(24,23.5,23.5),"",MAT_A,BLACK)
HN++,HT[HN,table]=table(8,7,1,vec(23,0,0),ang(),vec(24,23,23),"",MAT_A,BLACK)

HN++,HT[HN,table]=table(9,8,1,vec(28,0,0),ang(),vec(),"sphere","debug/debugdrawflat",RED)#-headjoint

#----------------------
#-- Tail Joints
HN++,HT[HN,table]=table(10,3,1,vec(-36,0,0),ang(),vec(24,23,23),"",MAT_A,BLACK)
HN++,HT[HN,table]=table(11,10,1,vec(-23,0,0),ang(),vec(24,23.5,23.5),"",MAT_A,BLACK)
HN++,HT[HN,table]=table(12,11,1,vec(-23,0,0),ang(),vec(24,23,23),"",MAT_A,BLACK)
HN++,HT[HN,table]=table(13,12,1,vec(-23,0,0),ang(),vec(24,23.5,23.5),"",MAT_A,BLACK)
HN++,HT[HN,table]=table(14,13,1,vec(-23,0,0),ang(),vec(24,23,23),"",MAT_A,BLACK)
HN++,HT[HN,table]=table(15,14,1,vec(-23,0,0),ang(),vec(24,23.5,23.5),"",MAT_A,BLACK)
HN++,HT[HN,table]=table(16,15,1,vec(-23,0,0),ang(),vec(24,23,23),"",MAT_A,BLACK)
HN++,HT[HN,table]=table(17,16,1,vec(-23,0,0),ang(),vec(24,23.5,23.5),"",MAT_A,BLACK)
HN++,HT[HN,table]=table(18,17,1,vec(-23,0,0),ang(),vec(24,23,23),"",MAT_A,BLACK)
HN++,HT[HN,table]=table(19,18,1,vec(-23,0,0),ang(),vec(24,23.5,23.5),"",MAT_A,BLACK)
HN++,HT[HN,table]=table(20,19,1,vec(-23,0,0),ang(),vec(24,23,23),"",MAT_A,BLACK)
HN++,HT[HN,table]=table(21,20,1,vec(-23,0,0),ang(),vec(24,23.5,23.5),"",MAT_A,BLACK)


#----------------------
#-- Neck Spikes
HN++,HT[HN,table]=table(22,4,1,vec(0,0,16),ang(),vec(15,4,9),"",MAT_A,GRAY)
HN++,HT[HN,table]=table(23,5,1,vec(0,0,16),ang(),vec(15,4,9),"",MAT_A,GRAY)
HN++,HT[HN,table]=table(24,6,1,vec(0,0,16),ang(),vec(15,4,9),"",MAT_A,GRAY)
HN++,HT[HN,table]=table(25,7,1,vec(0,0,16),ang(),vec(15,4,9),"",MAT_A,GRAY)
HN++,HT[HN,table]=table(26,8,1,vec(0,0,16),ang(),vec(15,4,9),"",MAT_A,GRAY)


#----------------------
#-- Tail Spikes
HN++,HT[HN,table]=table(27,10,1,vec(0,0,16),ang(),vec(15,4,9),"",MAT_A,GRAY)
HN++,HT[HN,table]=table(28,11,1,vec(0,0,16),ang(),vec(15,4,9),"",MAT_A,GRAY)
HN++,HT[HN,table]=table(29,12,1,vec(0,0,16),ang(),vec(15,4,9),"",MAT_A,GRAY)
HN++,HT[HN,table]=table(30,13,1,vec(0,0,16),ang(),vec(15,4,9),"",MAT_A,GRAY)
HN++,HT[HN,table]=table(31,14,1,vec(0,0,16),ang(),vec(15,4,9),"",MAT_A,GRAY)
HN++,HT[HN,table]=table(32,15,1,vec(0,0,16),ang(),vec(15,4,9),"",MAT_A,GRAY)
HN++,HT[HN,table]=table(33,16,1,vec(0,0,16),ang(),vec(15,4,9),"",MAT_A,GRAY)
HN++,HT[HN,table]=table(34,17,1,vec(0,0,16),ang(),vec(15,4,9),"",MAT_A,GRAY)
HN++,HT[HN,table]=table(35,18,1,vec(0,0,16),ang(),vec(15,4,9),"",MAT_A,GRAY)
HN++,HT[HN,table]=table(36,19,1,vec(0,0,16),ang(),vec(15,4,9),"",MAT_A,GRAY)
HN++,HT[HN,table]=table(37,20,1,vec(0,0,16),ang(),vec(15,4,9),"",MAT_A,GRAY)
HN++,HT[HN,table]=table(38,21,1,vec(0,0,16),ang(),vec(15,4,9),"",MAT_A,GRAY)


#----------------------
#-- Spine Spikes
HN++,HT[HN,table]=table(39,2,1,vec(0,0,4.5),ang(),vec(48,54,55),"",MAT_A,BLACK)
HN++,HT[HN,table]=table(40,1,1,vec(0,0,4.5),ang(),vec(48,53.5,54.5),"",MAT_A,BLACK)
HN++,HT[HN,table]=table(41,3,1,vec(0,0,4.5),ang(),vec(48,54,55),"",MAT_A,BLACK)
HN++,HT[HN,table]=table(42,2,1,vec(0,0,36),ang(),vec(26,6,15),"",MAT_A,GRAY)
HN++,HT[HN,table]=table(43,1,1,vec(0,0,36),ang(),vec(26,6,15),"",MAT_A,GRAY)
HN++,HT[HN,table]=table(44,3,1,vec(0,0,36),ang(),vec(26,6,15),"",MAT_A,GRAY)


#----------------------
#-- Head Visuals
HN++,HT[HN,table]=table(45,9,1,vec(0,0,4.5),ang(),vec(36,36,36),"",MAT_A,BLACK)
HN++,HT[HN,table]=table(46,9,1,vec(0,-9,26),ang(),vec(18,5,12),"",MAT_A,GRAY)
HN++,HT[HN,table]=table(47,9,1,vec(0,9,26),ang(),vec(18,5,12),"",MAT_A,GRAY)

HN++,HT[HN,table]=table(48,9,1,vec(18.2,-10,11.5),ang(90,0,0),vec(4,6,12),"plane","debug/debugdrawflat",vec4(224,121,250,255))
HN++,HT[HN,table]=table(49,48,1,vec(1,4,0),ang(0,0,0),vec(2,2,12),"plane","debug/debugdrawflat",vec4(224,121,250,255))
HN++,HT[HN,table]=table(50,48,1,vec(1,2,0.2),ang(0,0,0),vec(2,2,12),"plane","debug/debugdrawflat",vec4(204,0,250,255))
HN++,HT[HN,table]=table(51,48,1,vec(0,0,0.2),ang(0,0,0),vec(4,2,12),"plane","debug/debugdrawflat",vec4(204,0,250,255))

HN++,HT[HN,table]=table(52,9,1,vec(18.2,10,11.5),ang(90,0,0),vec(4,6,12),"plane","debug/debugdrawflat",vec4(224,121,250,255))
HN++,HT[HN,table]=table(53,52,1,vec(1,-4,0),ang(0,0,0),vec(2,2,12),"plane","debug/debugdrawflat",vec4(224,121,250,255))
HN++,HT[HN,table]=table(54,52,1,vec(1,-2,0.2),ang(0,0,0),vec(2,2,12),"plane","debug/debugdrawflat",vec4(204,0,250,255))
HN++,HT[HN,table]=table(55,52,1,vec(0,0,0.2),ang(0,0,0),vec(4,2,12),"plane","debug/debugdrawflat",vec4(204,0,250,255))

HN++,HT[HN,table]=table(56,9,1,vec(36.5,0,0.5),ang(),vec(37,28,12),"",MAT_A,BLACK)
HN++,HT[HN,table]=table(57,56,1,vec(9.8,-9,8.25),ang(),vec(9,4,5),"",MAT_A,BLACK)
HN++,HT[HN,table]=table(58,56,1,vec(9.8,9,8.25),ang(),vec(9,4,5),"",MAT_A,BLACK)
HN++,HT[HN,table]=table(59,9,1,vec(18,0,-4.5),ang(),vec(32,20,2.5),"hexagon","debug/debugdrawflat",vec4(224,121,250,255))
HN++,HT[HN,table]=table(60,9,1,vec(18,0,-4.5),ang(),vec(18,14,2.7),"octagon","debug/debugdrawflat",vec4(204,0,250,255))


#----------------------
#-- Jaw Visuals
HN++,HT[HN,table]=table(61,9,1,vec(18,0,-4.5),ang(25,0,0),vec(),"sphere","debug/debugdrawflat",RED)
HN++,HT[HN,table]=table(62,61,1,vec(18.5,0,-4.5),ang(),vec(37,28,7),"",MAT_A,BLACK)
HN++,HT[HN,table]=table(63,61,1,vec(0,0,-1.9),ang(),vec(39,20,2.5),"hexagon","debug/debugdrawflat",vec4(224,121,250,255))
HN++,HT[HN,table]=table(64,61,1,vec(0,0,-1.9),ang(),vec(24,14,2.7),"octagon","debug/debugdrawflat",vec4(204,0,250,255))


#----------------------
#-- Right Wing
HN++,HT[HN,table]=table(65,2,1,vec(6,-27,25),ang(),vec(),"sphere","debug/debugdrawflat",RED)
HN++,HT[HN,table]=table(66,65,1,vec(0,-128,0),ang(),vec(),"sphere","debug/debugdrawflat",RED)
HN++,HT[HN,table]=table(67,65,1,vec(0,-64,0),ang(),vec(18,128,18),"",MAT_A,GRAY)
HN++,HT[HN,table]=table(68,66,1,vec(0,-64,0),ang(),vec(12,128,12),"",MAT_A,GRAY)

HN++,HT[HN,table]=table(69,65,1,vec(-18,-62,0),ang(),vec(24,132,1.5),"",MAT_A,BLACK)
HN++,HT[HN,table]=table(70,65,1,vec(-66,-98,0),ang(),vec(72,60,1.5),"",MAT_A,BLACK)
HN++,HT[HN,table]=table(71,65,0,vec(-102,0.8,0),ang(0,90,0),vec(2.9,3.05,0.5),"models/hunter/misc/platehole1x1d.mdl",MAT_A,BLACK)
HN++,HT[HN,table]=table(72,65,0,vec(-131.6,-68,0),ang(0,90,0),vec(2.53,1.25,0.5),"models/hunter/misc/platehole1x1d.mdl",MAT_A,BLACK)
HN++,HT[HN,table]=table(73,65,1,vec(0,-109,0):rotate(0,-29,0),ang(0,-119,90),vec(12,3,112),"prism",MAT_A,WHITE)
HN++,HT[HN,table]=table(74,65,1,vec(-66,-125,0),ang(0,90,-90),vec(6,2.5,132),"right_prism",MAT_A,WHITE)

HN++,HT[HN,table]=table(75,66,1,vec(-54,-40,0),ang(),vec(96,80,1.5),"",MAT_A,BLACK)
HN++,HT[HN,table]=table(76,66,0,vec(-131.6,-60,0),ang(180,90,0),vec(2.53,1.25,0.5),"models/hunter/misc/platehole1x1d.mdl",MAT_A,BLACK)
HN++,HT[HN,table]=table(77,66,0,vec(-53.25,-103.725,0),ang(180,90,0),vec(0.5,1,0.5),"models/hunter/misc/platehole4x4d.mdl",MAT_A,BLACK)
HN++,HT[HN,table]=table(78,66,1,vec(-66,-3,0),ang(0,-90,90),vec(6,2.5,132),"right_prism",MAT_A,WHITE)
HN++,HT[HN,table]=table(79,66,1,vec(-9,-65,0):rotate(0,-43,0),ang(0,127,-90),vec(12,3,128),"prism",MAT_A,WHITE)


#----------------------
#-- Left Wing
HN++,HT[HN,table]=table(80,2,1,vec(6,27,25),ang(180,180,0),vec(),"sphere","debug/debugdrawflat",RED)
HN++,HT[HN,table]=table(81,80,1,vec(0,-128,0),ang(),vec(),"sphere","debug/debugdrawflat",RED)
HN++,HT[HN,table]=table(82,80,1,vec(0,-64,0),ang(),vec(18,128,18),"",MAT_A,GRAY)
HN++,HT[HN,table]=table(83,81,1,vec(0,-64,0),ang(),vec(12,128,12),"",MAT_A,GRAY)

HN++,HT[HN,table]=table(84,80,1,vec(-18,-62,0),ang(),vec(24,132,1.5),"",MAT_A,BLACK)
HN++,HT[HN,table]=table(85,80,1,vec(-66,-98,0),ang(),vec(72,60,1.5),"",MAT_A,BLACK)
HN++,HT[HN,table]=table(86,80,0,vec(-102,0.8,0),ang(0,90,0),vec(2.9,3.05,0.5),"models/hunter/misc/platehole1x1d.mdl",MAT_A,BLACK)
HN++,HT[HN,table]=table(87,80,0,vec(-131.6,-68,0),ang(0,90,0),vec(2.53,1.25,0.5),"models/hunter/misc/platehole1x1d.mdl",MAT_A,BLACK)
HN++,HT[HN,table]=table(88,80,1,vec(0,-109,0):rotate(0,-29,0),ang(0,-119,90),vec(12,3,112),"prism",MAT_A,WHITE)
HN++,HT[HN,table]=table(89,80,1,vec(-66,-125,0),ang(0,90,-90),vec(6,2.5,132),"right_prism",MAT_A,WHITE)

HN++,HT[HN,table]=table(90,81,1,vec(-54,-40,0),ang(),vec(96,80,1.5),"",MAT_A,BLACK)
HN++,HT[HN,table]=table(91,81,0,vec(-131.6,-60,0),ang(180,90,0),vec(2.53,1.25,0.5),"models/hunter/misc/platehole1x1d.mdl",MAT_A,BLACK)
HN++,HT[HN,table]=table(92,81,0,vec(-53.25,-103.725,0),ang(180,90,0),vec(0.5,1,0.5),"models/hunter/misc/platehole4x4d.mdl",MAT_A,BLACK)
HN++,HT[HN,table]=table(93,81,1,vec(-66,-3,0),ang(0,-90,90),vec(6,2.5,132),"right_prism",MAT_A,WHITE)
HN++,HT[HN,table]=table(94,81,1,vec(-9,-65,0):rotate(0,-43,0),ang(0,127,-90),vec(12,3,128),"prism",MAT_A,WHITE)


#----------------------
#-- RIGHTFRONTLEG
HN++,HT[HN,table]=table(95,2,1,vec(6,-27,-20),ang(165,0,0),vec(),"sphere","debug/debugdrawflat",RED)
HN++,HT[HN,table]=table(96,95,1,vec(45,0,0),ang(-15,0,0),vec(),"sphere","debug/debugdrawflat",RED)
HN++,HT[HN,table]=table(97,96,1,vec(50,0,0),ang(-90,0,0),vec(),"sphere","debug/debugdrawflat",RED)
HN++,HT[HN,table]=table(98,95,1,vec(17,0,0),ang(),vec(56,20,20),"",MAT_A,BLACK)
HN++,HT[HN,table]=table(99,96,1,vec(25,0,0),ang(),vec(52,15,15),"",MAT_A,BLACK)
HN++,HT[HN,table]=table(100,97,1,vec(10,0,-6),ang(),vec(36,15.5,10),"",MAT_A,BLACK)

#----------------------
#-- LEFTFRONTLEG
HN++,HT[HN,table]=table(101,2,1,vec(6,27,-20),ang(165,0,0),vec(),"sphere","debug/debugdrawflat",RED)
HN++,HT[HN,table]=table(102,101,1,vec(45,0,0),ang(-15,0,0),vec(),"sphere","debug/debugdrawflat",RED)
HN++,HT[HN,table]=table(103,102,1,vec(50,0,0),ang(-90,0,0),vec(),"sphere","debug/debugdrawflat",RED)
HN++,HT[HN,table]=table(104,101,1,vec(17,0,0),ang(),vec(56,20,20),"",MAT_A,BLACK)
HN++,HT[HN,table]=table(105,102,1,vec(25,0,0),ang(),vec(52,15,15),"",MAT_A,BLACK)
HN++,HT[HN,table]=table(106,103,1,vec(10,0,-6),ang(),vec(36,15.5,10),"",MAT_A,BLACK)

#----------------------
#-- RIGHTREARLEG
HN++,HT[HN,table]=table(107,3,1,vec(6,-39,-12),ang(152,0,0),vec(),"sphere","debug/debugdrawflat",RED)
HN++,HT[HN,table]=table(108,107,1,vec(54,0,0),ang(25,0,0),vec(),"sphere","debug/debugdrawflat",RED)
HN++,HT[HN,table]=table(109,108,1,vec(72,0,0),ang(-90,0,0),vec(),"sphere","debug/debugdrawflat",RED)
HN++,HT[HN,table]=table(110,107,1,vec(20,0,0),ang(),vec(69,32,36),"",MAT_A,BLACK)
HN++,HT[HN,table]=table(111,108,1,vec(38,0,0),ang(),vec(76,22,27),"",MAT_A,BLACK)
HN++,HT[HN,table]=table(112,109,1,vec(10,0,-7),ang(),vec(54,23,16),"",MAT_A,BLACK)

#----------------------
#-- LEFTREARLEG
HN++,HT[HN,table]=table(113,3,1,vec(6,39,-12),ang(152,0,0),vec(),"sphere","debug/debugdrawflat",RED)
HN++,HT[HN,table]=table(114,113,1,vec(54,0,0),ang(25,0,0),vec(),"sphere","debug/debugdrawflat",RED)
HN++,HT[HN,table]=table(115,114,1,vec(72,0,0),ang(-90,0,0),vec(),"sphere","debug/debugdrawflat",RED)
HN++,HT[HN,table]=table(116,113,1,vec(20,0,0),ang(),vec(69,32,36),"",MAT_A,BLACK)
HN++,HT[HN,table]=table(117,114,1,vec(38,0,0),ang(),vec(76,22,27),"",MAT_A,BLACK)
HN++,HT[HN,table]=table(118,115,1,vec(10,0,-7),ang(),vec(54,23,16),"",MAT_A,BLACK)


selfDestruct()
