#@name monopoly
@inputs EGP:wirelink
@outputs 
@persist EGPN
@trigger none

if (!->EGP) { exit() }

Colors = table(
    "board" = vec(205, 230, 208),
    "baltic" = vec(149, 84, 54),
    "oriental" = vec(170, 224, 250),
    "charles" = vec(217, 58, 150),
    "james" = vec(247, 148, 29),
    "kentucky" = vec(237, 27, 36),
    "atlantic" = vec(254, 242, 0),
    "pacific" = vec(31, 178, 90),
    "boardwalk" = vec(0, 114, 187)
)

EGP:egpClear()

#EGPN++
#EGP:egpBox(EGPN, vec2(256, 256), vec2(384, 384))
#EGP:egpMaterial(EGPN, "gui/monopoly")

EGPN++
EGP:egpBox(EGPN, vec2(256, 256), vec2(384, 384))
EGP:egpColor(EGPN, Colors["board", vector])
EGP:egpMaterial(EGPN, "")

local Array = array()
local Start = vec2(64, 64)

for (N = 1, 12) {
    if (N <= 6) {
        Array:pushVector2(Start), Start += vec2(384, 0)
        Array:pushVector2(Start), Start += vec2(0, (N == 1 | N == 6) ? 48 :  32)
        Array:pushVector2(Start), Start -= vec2(384, 0)
        Array:pushVector2(Start), if (N < 6) { Start += vec2(0, 32) } else { Start -= vec2(0, 384) }
    }
    else {
        Array:pushVector2(Start), Start += vec2((N == 7 | N == 12) ? 48 :  32, 0)
        Array:pushVector2(Start), Start += vec2(0, 384)
        Array:pushVector2(Start), if (N < 12) { Start += vec2(32, 0) }
        Array:pushVector2(Start), Start -= vec2(0, 384)
    }
}

EGPN++
EGP:egpLineStrip(EGPN, Array)
EGP:egpColor(EGPN, vec(0, 0, 0))

EGPN++
EGP:egpBoxOutline(EGPN, vec2(256, 256), vec2(384 - 74, 384 - 72))
EGP:egpColor(EGPN, vec())

EGPN++
EGP:egpBox(EGPN, vec2(257, 257), vec2(384 - 97, 384 - 97))
EGP:egpColor(EGPN, Colors["board", vector])


#-- mediteranean/baltic
EGPN++
EGP:egpBox(EGPN, vec2(385, 384+22), vec2(31, 10))
EGP:egpColor(EGPN, Colors["baltic", vector])

EGPN++
EGP:egpBox(EGPN, vec2(385 - 64, 384+22), vec2(31, 10))
EGP:egpColor(EGPN, Colors["baltic", vector])

#-- oriental/vermont/connecticut/
EGPN++
EGP:egpBox(EGPN, vec2(385 - 32*5, 384+22), vec2(31, 10))
EGP:egpColor(EGPN, Colors["oriental", vector])

EGPN++
EGP:egpBox(EGPN, vec2(385 - 32*7, 384+22), vec2(31, 10))
EGP:egpColor(EGPN, Colors["oriental", vector])

EGPN++
EGP:egpBox(EGPN, vec2(385 - 32*8, 384+22), vec2(31, 10))
EGP:egpColor(EGPN, Colors["oriental", vector])

#-- charles
EGPN++
EGP:egpBox(EGPN, vec2(129-22, 129 + 32*8), vec2(10, 31))
EGP:egpColor(EGPN, Colors["charles", vector])

EGPN++
EGP:egpBox(EGPN, vec2(129-22, 129 + 32*6), vec2(10, 31))
EGP:egpColor(EGPN, Colors["charles", vector])

EGPN++
EGP:egpBox(EGPN, vec2(129-22, 129 + 32*5), vec2(10, 31))
EGP:egpColor(EGPN, Colors["charles", vector])

#-- james
EGPN++
EGP:egpBox(EGPN, vec2(129-22, 129 + 32*3), vec2(10, 31))
EGP:egpColor(EGPN, Colors["james", vector])

EGPN++
EGP:egpBox(EGPN, vec2(129-22, 129 + 32*1), vec2(10, 31))
EGP:egpColor(EGPN, Colors["james", vector])

EGPN++
EGP:egpBox(EGPN, vec2(129-22, 129 + 32*0), vec2(10, 31))
EGP:egpColor(EGPN, Colors["james", vector])

#-- kentucky
EGPN++
EGP:egpBox(EGPN, vec2(385 - 32*5, 128-21), vec2(31, 11))
EGP:egpColor(EGPN, Colors["kentucky", vector])

EGPN++
EGP:egpBox(EGPN, vec2(385 - 32*6, 128-21), vec2(31, 11))
EGP:egpColor(EGPN, Colors["kentucky", vector])

EGPN++
EGP:egpBox(EGPN, vec2(385 - 32*8, 128-21), vec2(31, 11))
EGP:egpColor(EGPN, Colors["kentucky", vector])

#-- atlantic
EGPN++
EGP:egpBox(EGPN, vec2(385 - 32*3, 128-21), vec2(31, 11))
EGP:egpColor(EGPN, Colors["atlantic", vector])

EGPN++
EGP:egpBox(EGPN, vec2(385 - 32*2, 128-21), vec2(31, 11))
EGP:egpColor(EGPN, Colors["atlantic", vector])

EGPN++
EGP:egpBox(EGPN, vec2(385 - 32*0, 128-21), vec2(31, 11))
EGP:egpColor(EGPN, Colors["atlantic", vector])

#-- pacific
EGPN++
EGP:egpBox(EGPN, vec2(384+22, 129 + 32*3), vec2(9, 31))
EGP:egpColor(EGPN, Colors["pacific", vector])

EGPN++
EGP:egpBox(EGPN, vec2(384+22, 129 + 32*1), vec2(9, 31))
EGP:egpColor(EGPN, Colors["pacific", vector])

EGPN++
EGP:egpBox(EGPN, vec2(384+22, 129 + 32*0), vec2(9, 31))
EGP:egpColor(EGPN, Colors["pacific", vector])
