@name holograms/mechs/gits/haw206/data
@persist [HN CN] [HT CT]:table


#----------------------
#--GTableSetup
local ID = "HAW206_V1"

XGT = gTable("_holocore",0)

XGT[ID,table] = table(
    "Clips" = table(),
    "Holos" = table()
)

HT = XGT[ID,table]["Holos",table]
CT = XGT[ID,table]["Clips",table]


#----------------------
#--Bones
local Skeleton = 0

local Scale = vec(12,12,12)
local Color = vec4(255,255,255,Skeleton*255)
local Material = ""
local Model = ""

local Vec = vec()
local Ang = ang()
local Def = vec(1,1,1)


#----------------------
#--Body
HN++,HT[HN,table] = table(1,0,1,Vec,Ang,Scale,Model,Material,Color)


if(Skeleton){

#----------------------
#--Bones

#----------------------
#--FrontLeftLeg
HN++,HT[HN,table] = table(2,1,1,vec(26.5926,28.6354,-10),Ang,Scale,Model,Material,Color)
HN++,HT[HN,table] = table(3,2,1,vec(0,0,51),Ang,Scale,Model,Material,Color)
HN++,HT[HN,table] = table(4,3,1,vec(0,0,35),Ang,Scale,Model,Material,Color)
HN++,HT[HN,table] = table(5,4,1,vec(0,0,94),Ang,Scale,Model,Material,Color)


#----------------------
#--FrontRighttLeg
HN++,HT[HN,table] = table(6,1,1,vec(26.5926,-28.6354,-10),Ang,Scale,Model,Material,Color)
HN++,HT[HN,table] = table(7,6,1,vec(0,0,51),Ang,Scale,Model,Material,Color)
HN++,HT[HN,table] = table(8,7,1,vec(0,0,35),Ang,Scale,Model,Material,Color)
HN++,HT[HN,table] = table(9,8,1,vec(0,0,94),Ang,Scale,Model,Material,Color)


#----------------------
#--RearLeftLeg
HN++,HT[HN,table] = table(10,1,1,vec(-26.5926,28.6354,-10),Ang,Scale,Model,Material,Color)
HN++,HT[HN,table] = table(11,10,1,vec(0,0,51),Ang,Scale,Model,Material,Color)
HN++,HT[HN,table] = table(12,11,1,vec(0,0,35),Ang,Scale,Model,Material,Color)
HN++,HT[HN,table] = table(13,12,1,vec(0,0,94),Ang,Scale,Model,Material,Color)


#----------------------
#--RearRighttLeg
HN++,HT[HN,table] = table(14,1,1,vec(-26.5926,-28.6354,-10),Ang,Scale,Model,Material,Color)
HN++,HT[HN,table] = table(15,14,1,vec(0,0,51),Ang,Scale,Model,Material,Color)
HN++,HT[HN,table] = table(16,15,1,vec(0,0,35),Ang,Scale,Model,Material,Color)
HN++,HT[HN,table] = table(17,16,1,vec(0,0,94),Ang,Scale,Model,Material,Color)


#----------------------
#--Torso
HN++,HT[HN,table] = table(18,1,1,vec(0,0,5),Ang,Scale,Model,Material,Color)


#----------------------
#--Tail
HN++,HT[HN,table] = table(19,18,1,vec(-43,0,30),ang(10,0,0),Scale,Model,Material,Color)
HN++,HT[HN,table] = table(20,19,1,vec(-48,0,0),ang(10,0,0),Scale,Model,Material,Color)
HN++,HT[HN,table] = table(21,20,1,vec(-48,0,5),ang(-10,0,0),Scale,Model,Material,Color)

HN++,HT[HN,table] = table(716,21,1,vec(-24,0,57.5),ang(0,0,90),Scale,Model,Material,Color)#Yaw
HN++,HT[HN,table] = table(721,21,1,vec(-24,0,90),ang(-10,0,0),Scale,Model,Material,Color)#Pitch


#----------------------
#--LeftArm
HN++,HT[HN,table] = table(800,18,1,vec(75,35,6),ang(0,0,-45),Scale,Model,Material,Color)
HN++,HT[HN,table] = table(801,800,1,vec(8,18,0),ang(25,0,0),Scale,Model,Material,Color)

#-Claw
HN++,HT[HN,table] = table(804,801,1,vec(65,0,-5),Ang,Scale,Model,Material,Color)
HN++,HT[HN,table] = table(805,801,1,vec(65,0,-5):rotate(0,0,-120),ang(0,0,-120),Scale,Model,Material,Color)
HN++,HT[HN,table] = table(806,801,1,vec(65,0,-5):rotate(0,0,-240),ang(0,0,-240),Scale,Model,Material,Color)


#----------------------
#--RightArm
HN++,HT[HN,table] = table(900,18,1,vec(75,-35,6),ang(0,0,45),Scale,Model,Material,Color)
HN++,HT[HN,table] = table(901,900,1,vec(8,-18,0),ang(25,0,0),Scale,Model,Material,Color)

#-Claw
HN++,HT[HN,table] = table(904,901,1,vec(65,0,-5),Ang,Scale,Model,Material,Color)
HN++,HT[HN,table] = table(905,901,1,vec(65,0,-5):rotate(0,0,-120),ang(0,0,-120),Scale,Model,Material,Color)
HN++,HT[HN,table] = table(906,901,1,vec(65,0,-5):rotate(0,0,-240),ang(0,0,-240),Scale,Model,Material,Color)




#----------------------
#--Links

local Scale = Scale-1
local Color = Color/vec4(4,4,4,1)


#----------------------
#--FrontLeftLeg
HN++,HT[HN,table] = table(1000,2,1,Vec:setZ(51/2),Ang,Scale:setZ(51),Model,Material,Color)
HN++,HT[HN,table] = table(1001,3,1,Vec:setZ(35/2),Ang,Scale:setZ(35),Model,Material,Color)
HN++,HT[HN,table] = table(1002,4,1,Vec:setZ(94/2),Ang,Scale:setZ(94),Model,Material,Color)


#----------------------
#--FrontRightLeg
HN++,HT[HN,table] = table(1003,6,1,Vec:setZ(51/2),Ang,Scale:setZ(51),Model,Material,Color)
HN++,HT[HN,table] = table(1004,7,1,Vec:setZ(35/2),Ang,Scale:setZ(35),Model,Material,Color)
HN++,HT[HN,table] = table(1005,8,1,Vec:setZ(94/2),Ang,Scale:setZ(94),Model,Material,Color)


#----------------------
#--RearLeftLeg
HN++,HT[HN,table] = table(1006,10,1,Vec:setZ(51/2),Ang,Scale:setZ(51),Model,Material,Color)
HN++,HT[HN,table] = table(1007,11,1,Vec:setZ(35/2),Ang,Scale:setZ(35),Model,Material,Color)
HN++,HT[HN,table] = table(1008,12,1,Vec:setZ(94/2),Ang,Scale:setZ(94),Model,Material,Color)


#----------------------
#--RearRightLeg
HN++,HT[HN,table] = table(1009,14,1,Vec:setZ(51/2),Ang,Scale:setZ(51),Model,Material,Color)
HN++,HT[HN,table] = table(1010,15,1,Vec:setZ(35/2),Ang,Scale:setZ(35),Model,Material,Color)
HN++,HT[HN,table] = table(1011,16,1,Vec:setZ(94/2),Ang,Scale:setZ(94),Model,Material,Color)


}
else{
    

#----------------------
#--
local BMat1 = "models/props_debris/plasterwall040c"
local BCol1 = vec4(63,70,56,255)
local BCol2 = vec4(54,57,51,255)
local BCol3 = vec4(36,36,36,255)
local BCol4 = vec4(135,135,135,255)

local EyeM = "models/props_debris/plasterwall040c"
local EyeC1 = vec4(255,255,254,255)
local EyeC2 = vec4(15,15,15,255)

local GlowM = "models/shiny"
local GlowC = vec4(165,0,0,255)


#----------------------
#--Torso
HN++,HT[HN,table] = table(18,1,0,vec(0,0,5),Ang,vec(1.25),"models/Combine_Helicopter/helicopter_bomb01.mdl",BMat1,BCol3)


#----------------------
#--Tail
HN++,HT[HN,table] = table(19,18,1,vec(-43,0,30),ang(10,0,0),Scale,Model,Material,Color)
HN++,HT[HN,table] = table(20,19,1,vec(-48,0,0),ang(10,0,0),Scale,Model,Material,Color)
HN++,HT[HN,table] = table(21,20,1,vec(-48,0,5),ang(-10,0,0),Scale,Model,Material,Color)


#----------------------
#--Body
HN++,HT[HN,table] = table(100,1,0,vec(0,0,-9.5),ang(90,0,0),vec(0.85,0.925,0.925),"models/props_vehicles/apc_tire001.mdl",BMat1,BCol1)

HN++,HT[HN,table] = table(101,1,0,vec(26.5926*0.5,28.6354*0.5,-11),ang(90,45,0),vec(1.35,1.35,1.125),"models/props_junk/terracotta01.mdl",BMat1,BCol3)
HN++,HT[HN,table] = table(102,1,0,vec(26.5926*0.5,-28.6354*0.5,-11),ang(90,-45,0),vec(1.35,1.35,1.125),"models/props_junk/terracotta01.mdl",BMat1,BCol3)
HN++,HT[HN,table] = table(103,1,0,vec(-26.5926*0.5,28.6354*0.5,-8.5),ang(-90,-45,0),vec(1.35,1.35,1.125),"models/props_junk/terracotta01.mdl",BMat1,BCol3)
HN++,HT[HN,table] = table(104,1,0,vec(-26.5926*0.5,-28.6354*0.5,-8.5),ang(-90,45,0),vec(1.35,1.35,1.125),"models/props_junk/terracotta01.mdl",BMat1,BCol3)

#-BottomEye
HN++,HT[HN,table] = table(105,1,0,vec(0,0,-16),ang(45,0,0),vec(1,1,1)*1.1,"models/sprops/geometry/sphere_24.mdl",EyeM,EyeC1)
HN++,HT[HN,table] = table(106,105,1,vec(2,0,2),ang(90,0,0),vec(2,2,24),"cylinder",EyeM,EyeC2)
HN++,HT[HN,table] = table(107,105,1,vec(2,0,2):rotate(0,0,120),ang(90,0,0),vec(2,2,24),"cylinder",EyeM,EyeC2)
HN++,HT[HN,table] = table(108,105,1,vec(2,0,2):rotate(0,0,240),ang(90,0,0),vec(2,2,24),"cylinder",EyeM,EyeC2)


#----------------------
#--FrontLeftLeg
HN++,HT[HN,table] = table(2,1,0,vec(26.5926,28.6354,-10),Ang,vec(0.7),"models/Combine_Helicopter/helicopter_bomb01.mdl",BMat1,BCol3)
HN++,HT[HN,table] = table(3,2,0,vec(0,0,51),Ang,vec(0.7),"models/Combine_Helicopter/helicopter_bomb01.mdl",BMat1,BCol3)
HN++,HT[HN,table] = table(4,3,0,vec(0,0,35),Ang,vec(0.7),"models/Combine_Helicopter/helicopter_bomb01.mdl",BMat1,BCol3)
HN++,HT[HN,table] = table(5,4,0,vec(0,0,94),Ang,vec(0.7),"models/Combine_Helicopter/helicopter_bomb01.mdl",BMat1,BCol3)

HN++,HT[HN,table] = table(201,2,0,vec(0,0,22),ang(180,0,0),vec(2.75,2.75,3.25),"models/sprops/misc/fittings/cred_12_9_tall.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(202,2,0,vec(0,0,44.5),Ang,vec(2.75,2.75,1),"models/sprops/misc/fittings/cred_12_9_short.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(204,3,0,vec(0,0,15),ang(180,0,0),vec(3.25,3.25,2.5),"models/sprops/misc/fittings/cred_12_9_tall.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(205,3,0,vec(0,0,33),Ang,vec(3.25,3.25,1),"models/sprops/misc/fittings/cred_12_9_short.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(207,4,0,vec(0,0,10),ang(180,0,0),vec(2.5,2.5,1),"models/sprops/misc/fittings/cred_12_9_short.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(208,4,0,vec(0,0,16),Ang,vec(2.5,2.5,1),"models/sprops/misc/fittings/cred_12_9_short.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(209,4,0,vec(0,0,24),ang(180,0,0),vec(2.5,2.5,2),"models/sprops/misc/fittings/cred_12_9_short.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(210,4,0,vec(0,0,60),Ang,vec(2.5,2.5,5),"models/sprops/misc/fittings/cred_12_9_tall.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(212,5,0,vec(0,-13,-6),ang(-90,0,90),vec(1),"models/sprops/misc/fittings/bend_long_90_6.mdl",BMat1,BCol3)
HN++,HT[HN,table] = table(213,5,0,vec(0,-13,-6):rotate(0,90,0),ang(-90,90,90),vec(1),"models/sprops/misc/fittings/bend_long_90_6.mdl",BMat1,BCol3)
HN++,HT[HN,table] = table(214,5,0,vec(0,-13,-6):rotate(0,180,0),ang(-90,180,90),vec(1),"models/sprops/misc/fittings/bend_long_90_6.mdl",BMat1,BCol3)
HN++,HT[HN,table] = table(215,5,0,vec(0,-13,-6):rotate(0,-90,0),ang(-90,-90,90),vec(1),"models/sprops/misc/fittings/bend_long_90_6.mdl",BMat1,BCol3)


#----------------------
#--FrontRighttLeg
HN++,HT[HN,table] = table(6,1,0,vec(26.5926,-28.6354,-10),Ang,vec(0.7),"models/Combine_Helicopter/helicopter_bomb01.mdl",BMat1,BCol3)
HN++,HT[HN,table] = table(7,6,0,vec(0,0,51),Ang,vec(0.7),"models/Combine_Helicopter/helicopter_bomb01.mdl",BMat1,BCol3)
HN++,HT[HN,table] = table(8,7,0,vec(0,0,35),Ang,vec(0.7),"models/Combine_Helicopter/helicopter_bomb01.mdl",BMat1,BCol3)
HN++,HT[HN,table] = table(9,8,0,vec(0,0,94),Ang,vec(0.7),"models/Combine_Helicopter/helicopter_bomb01.mdl",BMat1,BCol3)

HN++,HT[HN,table] = table(301,6,0,vec(0,0,22),ang(180,0,0),vec(2.75,2.75,3.25),"models/sprops/misc/fittings/cred_12_9_tall.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(302,6,0,vec(0,0,44.5),Ang,vec(2.75,2.75,1),"models/sprops/misc/fittings/cred_12_9_short.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(304,7,0,vec(0,0,15),ang(180,0,0),vec(3.25,3.25,2.5),"models/sprops/misc/fittings/cred_12_9_tall.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(305,7,0,vec(0,0,33),Ang,vec(3.25,3.25,1),"models/sprops/misc/fittings/cred_12_9_short.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(307,8,0,vec(0,0,10),ang(180,0,0),vec(2.5,2.5,1),"models/sprops/misc/fittings/cred_12_9_short.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(308,8,0,vec(0,0,16),Ang,vec(2.5,2.5,1),"models/sprops/misc/fittings/cred_12_9_short.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(309,8,0,vec(0,0,24),ang(180,0,0),vec(2.5,2.5,2),"models/sprops/misc/fittings/cred_12_9_short.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(310,8,0,vec(0,0,60),Ang,vec(2.5,2.5,5),"models/sprops/misc/fittings/cred_12_9_tall.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(312,9,0,vec(0,-13,-6),ang(-90,0,90),vec(1),"models/sprops/misc/fittings/bend_long_90_6.mdl",BMat1,BCol3)
HN++,HT[HN,table] = table(313,9,0,vec(0,-13,-6):rotate(0,90,0),ang(-90,90,90),vec(1),"models/sprops/misc/fittings/bend_long_90_6.mdl",BMat1,BCol3)
HN++,HT[HN,table] = table(314,9,0,vec(0,-13,-6):rotate(0,180,0),ang(-90,180,90),vec(1),"models/sprops/misc/fittings/bend_long_90_6.mdl",BMat1,BCol3)
HN++,HT[HN,table] = table(315,9,0,vec(0,-13,-6):rotate(0,-90,0),ang(-90,-90,90),vec(1),"models/sprops/misc/fittings/bend_long_90_6.mdl",BMat1,BCol3)


#----------------------
#--RearLeftLeg
HN++,HT[HN,table] = table(10,1,0,vec(-26.5926,28.6354,-10),Ang,vec(0.7),"models/Combine_Helicopter/helicopter_bomb01.mdl",BMat1,BCol3)
HN++,HT[HN,table] = table(11,10,0,vec(0,0,51),Ang,vec(0.7),"models/Combine_Helicopter/helicopter_bomb01.mdl",BMat1,BCol3)
HN++,HT[HN,table] = table(12,11,0,vec(0,0,35),Ang,vec(0.7),"models/Combine_Helicopter/helicopter_bomb01.mdl",BMat1,BCol3)
HN++,HT[HN,table] = table(13,12,0,vec(0,0,94),Ang,vec(0.7),"models/Combine_Helicopter/helicopter_bomb01.mdl",BMat1,BCol3)

HN++,HT[HN,table] = table(401,10,0,vec(0,0,22),ang(180,0,0),vec(2.75,2.75,3.25),"models/sprops/misc/fittings/cred_12_9_tall.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(402,10,0,vec(0,0,44.5),Ang,vec(2.75,2.75,1),"models/sprops/misc/fittings/cred_12_9_short.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(404,11,0,vec(0,0,15),ang(180,0,0),vec(3.25,3.25,2.5),"models/sprops/misc/fittings/cred_12_9_tall.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(405,11,0,vec(0,0,33),Ang,vec(3.25,3.25,1),"models/sprops/misc/fittings/cred_12_9_short.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(407,12,0,vec(0,0,10),ang(180,0,0),vec(2.5,2.5,1),"models/sprops/misc/fittings/cred_12_9_short.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(408,12,0,vec(0,0,16),Ang,vec(2.5,2.5,1),"models/sprops/misc/fittings/cred_12_9_short.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(409,12,0,vec(0,0,24),ang(180,0,0),vec(2.5,2.5,2),"models/sprops/misc/fittings/cred_12_9_short.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(410,12,0,vec(0,0,60),Ang,vec(2.5,2.5,5),"models/sprops/misc/fittings/cred_12_9_tall.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(412,13,0,vec(0,-13,-6),ang(-90,0,90),vec(1),"models/sprops/misc/fittings/bend_long_90_6.mdl",BMat1,BCol3)
HN++,HT[HN,table] = table(413,13,0,vec(0,-13,-6):rotate(0,90,0),ang(-90,90,90),vec(1),"models/sprops/misc/fittings/bend_long_90_6.mdl",BMat1,BCol3)
HN++,HT[HN,table] = table(414,13,0,vec(0,-13,-6):rotate(0,180,0),ang(-90,180,90),vec(1),"models/sprops/misc/fittings/bend_long_90_6.mdl",BMat1,BCol3)
HN++,HT[HN,table] = table(415,13,0,vec(0,-13,-6):rotate(0,-90,0),ang(-90,-90,90),vec(1),"models/sprops/misc/fittings/bend_long_90_6.mdl",BMat1,BCol3)


#----------------------
#--RearLeftLeg
HN++,HT[HN,table] = table(14,1,0,vec(-26.5926,-28.6354,-10),Ang,vec(0.7),"models/Combine_Helicopter/helicopter_bomb01.mdl",BMat1,BCol3)
HN++,HT[HN,table] = table(15,14,0,vec(0,0,51),Ang,vec(0.7),"models/Combine_Helicopter/helicopter_bomb01.mdl",BMat1,BCol3)
HN++,HT[HN,table] = table(16,15,0,vec(0,0,35),Ang,vec(0.7),"models/Combine_Helicopter/helicopter_bomb01.mdl",BMat1,BCol3)
HN++,HT[HN,table] = table(17,16,0,vec(0,0,94),Ang,vec(0.7),"models/Combine_Helicopter/helicopter_bomb01.mdl",BMat1,BCol3)

HN++,HT[HN,table] = table(501,14,0,vec(0,0,22),ang(180,0,0),vec(2.75,2.75,3.25),"models/sprops/misc/fittings/cred_12_9_tall.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(502,14,0,vec(0,0,44.5),Ang,vec(2.75,2.75,1),"models/sprops/misc/fittings/cred_12_9_short.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(504,15,0,vec(0,0,15),ang(180,0,0),vec(3.25,3.25,2.5),"models/sprops/misc/fittings/cred_12_9_tall.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(505,15,0,vec(0,0,33),Ang,vec(3.25,3.25,1),"models/sprops/misc/fittings/cred_12_9_short.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(507,16,0,vec(0,0,10),ang(180,0,0),vec(2.5,2.5,1),"models/sprops/misc/fittings/cred_12_9_short.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(508,16,0,vec(0,0,16),Ang,vec(2.5,2.5,1),"models/sprops/misc/fittings/cred_12_9_short.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(509,16,0,vec(0,0,24),ang(180,0,0),vec(2.5,2.5,2),"models/sprops/misc/fittings/cred_12_9_short.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(510,16,0,vec(0,0,60),Ang,vec(2.5,2.5,5),"models/sprops/misc/fittings/cred_12_9_tall.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(512,17,0,vec(0,-13,-6),ang(-90,0,90),vec(1),"models/sprops/misc/fittings/bend_long_90_6.mdl",BMat1,BCol3)
HN++,HT[HN,table] = table(513,17,0,vec(0,-13,-6):rotate(0,90,0),ang(-90,90,90),vec(1),"models/sprops/misc/fittings/bend_long_90_6.mdl",BMat1,BCol3)
HN++,HT[HN,table] = table(514,17,0,vec(0,-13,-6):rotate(0,180,0),ang(-90,180,90),vec(1),"models/sprops/misc/fittings/bend_long_90_6.mdl",BMat1,BCol3)
HN++,HT[HN,table] = table(515,17,0,vec(0,-13,-6):rotate(0,-90,0),ang(-90,-90,90),vec(1),"models/sprops/misc/fittings/bend_long_90_6.mdl",BMat1,BCol3)


#----------------------
#--Torso
HN++,HT[HN,table] = table(600,18,0,vec(0,0,10),ang(180,0,0),vec(5,5,2),"models/sprops/misc/fittings/cred_12_9_short.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(601,18,0,vec(77.9427,0,43.4636),ang(0,90,11.7),Def,"models/sprops/rectangles/size_54/rect_54x78x3.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(602,18,0,vec(77.6789,-24.5332,42.3254),ang(-12.0022,-174.2535,-45.5565),Def,"models/sprops/rectangles/size_4/rect_36x54x3.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(603,18,0,vec(77.6789,24.5333,42.3254),ang(-12.0022,174.2536,45.5565),Def,"models/sprops/rectangles/size_4/rect_36x54x3.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(604,18,0,vec(4.2308,-32.4952,47.893),ang(0,-173.6,-45),Def,"models/sprops/rectangles/size_4/rect_36x96x3.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(605,18,0,vec(4.2308,32.4952,47.893),ang(0,173.6,45),Def,"models/sprops/rectangles/size_4/rect_36x96x3.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(606,18,0,vec(3.7934,0,51.3381),ang(2.9,0,0),Def,"models/sprops/rectangles/size_78/rect_78x96x3.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(607,18,0,vec(84.3063,23.5345,1.6726),ang(-11.9971,174.2542,114.928),Def,"models/sprops/rectangles/size_2/rect_12x24x3.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(608,18,0,vec(84.3063,-23.5345,1.6726),ang(-11.9971,-174.2542,-114.928),Def,"models/sprops/rectangles/size_2/rect_12x24x3.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(609,18,0,vec(73.8431,32.2121,20.7572),ang(-12.0022,174.2535,114.9307),Def,"models/sprops/rectangles/size_3/rect_24x54x3.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(610,18,0,vec(73.8431,-32.212,20.7572),ang(-12.0022,-174.2536,-114.9307),Def,"models/sprops/rectangles/size_3/rect_24x54x3.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(611,18,0,vec(58.0288,26.1786,7.2849),ang(-11.9971,174.2542,-155.072),Def,"models/sprops/triangles/right/size_1/rtri_12x30.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(612,18,0,vec(58.0287,-26.1785,7.2849),ang(-11.9971,-174.2542,155.072),Def,"models/sprops/triangles/right/size_1/rtri_12x30.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(613,18,0,vec(5.051,39.8072,25.9839),ang(0,173.6005,114.9994),Def,"models/sprops/rectangles/size_3/rect_24x96x3.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(614,18,0,vec(5.051,-39.8071,25.9839),ang(0,-173.6005,-114.9994),Def,"models/sprops/rectangles/size_3/rect_24x96x3.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(615,18,0,vec(97.6183,0,16.4138),ang(0,90,101.6949),vec(1,0.97,1),"models/sprops/rectangles/size_5/rect_48x66x3.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(616,18,0,vec(81.8103,7.5704,-2.4927),ang(-1.178,-95.4956,-11.9997),Def,"models/sprops/rectangles/size_3/rect_24x30x3.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(617,18,0,vec(81.8103,-7.5704,-2.4927),ang(-1.178,95.4956,11.9997),Def,"models/sprops/rectangles/size_3/rect_24x30x3.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(618,18,0,vec(53.0889,7.5999,11.6022),ang(-7.4103,-101.7554,-31.7889),Def,"models/sprops/rectangles/size_4/rect_36x42x3.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(619,18,0,vec(53.0889,-7.5998,11.6022),ang(-7.4103,101.7554,31.7889),Def,"models/sprops/rectangles/size_4/rect_36x42x3.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(620,18,0,vec(1.9621,12.2681,15.9742),ang(0,173.6004,0),Def,"models/sprops/rectangles/size_5/rect_48x96x3.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(621,18,0,vec(1.9621,-12.268,15.9742),ang(0,-173.6004,0),Def,"models/sprops/rectangles/size_5/rect_48x96x3.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(622,18,0,vec(-40.7211,0,34.2685),ang(0,90,-90),Def,"models/sprops/rectangles/size_4/rect_36x96x3.mdl",BMat1,BCol1)

CN++,CT[CN,table] = table(601,1,vec(12.359779395538,-1.296449747428,12.693293438792),vec(-0.695771,0.072981,-0.714546))
CN++,CT[CN,table] = table(601,2,vec(-12.359782675216,-1.2964482597879,12.693295719572),vec(0.695771,0.072981,-0.714546))
CN++,CT[CN,table] = table(602,1,vec(0.016940578238352,-1.8883164679313,1.928766845287),vec(-0.006276,0.699560,-0.714546))
CN++,CT[CN,table] = table(603,1,vec(0.016940971194051,1.8883602695481,1.9288115851957),vec(-0.006276,-0.699560,-0.714546))
CN++,CT[CN,table] = table(604,1,vec(-0.24884738085463,-3.4758508064141,3.5152907315054),vec(0.050274,0.702217,-0.710185))
CN++,CT[CN,table] = table(605,1,vec(-0.24884738085463,3.4758508064141,3.5152907315054),vec(0.050274,-0.702217,-0.710185))
CN++,CT[CN,table] = table(606,1,vec(0.94319547301891,15.43438091113,15.598706980057),vec(-0.042942,-0.702704,-0.710185))
CN++,CT[CN,table] = table(606,2,vec(0.94319689950492,-15.434404254041,15.598730571495),vec(-0.042942,0.702704,-0.710185))

CN++,CT[CN,table] = table(615,1,ang(4.010,-134.225,0.000),-32.021306189244)
CN++,CT[CN,table] = table(615,2,ang(5.098,156.157,0.000),-28.994442100353)
CN++,CT[CN,table] = table(615,3,ang(5.096,23.844,0.000),-28.994907493694)
CN++,CT[CN,table] = table(615,4,ang(4.009,-45.774,0.000),-32.02243530582)
CN++,CT[CN,table] = table(622,1,ang(-4.521,134.821,0.000),-37.54334815075)
CN++,CT[CN,table] = table(622,2,ang(-4.521,45.179,0.000),-37.543141100104)
CN++,CT[CN,table] = table(622,3,ang(-5.798,-25.137,0.000),-45.509251861578)
CN++,CT[CN,table] = table(622,4,ang(-5.798,-154.863,0.000),-45.508824601261)

#-TopEye
HN++,HT[HN,table] = table(623,18,1,vec(75.9848,0,43.8691),ang(78.305+90,180,0),vec(30,30,11),"cylinder",BMat1,BCol2)
HN++,HT[HN,table] = table(624,18,0,vec(76.796,0,47.7859),ang(-45,0,0),vec(1,1,1)*1.1,"models/sprops/geometry/sphere_24.mdl",EyeM,EyeC1)
HN++,HT[HN,table] = table(625,624,1,vec(2,0,2),ang(90,0,0),vec(2,2,24),"cylinder",EyeM,EyeC2)
HN++,HT[HN,table] = table(626,624,1,vec(2,0,2):rotate(0,0,120),ang(90,0,0),vec(2,2,24),"cylinder",EyeM,EyeC2)
HN++,HT[HN,table] = table(627,624,1,vec(2,0,2):rotate(0,0,240),ang(90,0,0),vec(2,2,24),"cylinder",EyeM,EyeC2)

#-Hatch
HN++,HT[HN,table] = table(628,18,0,vec(15,0,46),ang(0,77,0),vec(0.35,0.35,0.5),"models/props_wasteland/laundry_washer001a.mdl",BMat1,BCol2)
HN++,HT[HN,table] = table(629,18,0,vec(0,0,54),ang(0,90,90),vec(1,2,4),"models/sprops/geometry/hhex_48.mdl",BMat1,BCol2)

#-Glowies
HN++,HT[HN,table] = table(630,18,1,vec(101.4462,6.0003,34.0092),ang(101.942,0,0),vec(4,4,5),"rcylinder_thin",GlowM,GlowC)
HN++,HT[HN,table] = table(631,18,1,vec(101.4462,-6.0003,34.0092),ang(101.942,0,0),vec(4,4,5),"rcylinder_thin",GlowM,GlowC)
HN++,HT[HN,table] = table(632,18,1,vec(100.2052,0.0002,28.1391),ang(101.942,0,0),vec(4,4,5),"rcylinder_thin",GlowM,GlowC)
HN++,HT[HN,table] = table(633,18,1,vec(99.5823,24.0001,25.204),ang(101.942,0,0),vec(4,4,5),"rcylinder_thin",GlowM,GlowC)
HN++,HT[HN,table] = table(634,18,1,vec(99.5823,-24.0001,25.204),ang(101.942,0,0),vec(4,4,5),"rcylinder_thin",GlowM,GlowC)

#-ArmSockets
HN++,HT[HN,table] = table(635,18,0,vec(74.3801,23,12.1572),ang(-11.9971,174.2542,114.928),vec(1.35,1.35,1),"models/props_junk/terracotta01.mdl",BMat1,BCol2)
HN++,HT[HN,table] = table(636,18,0,vec(74.3801,-23,12.1572),ang(-11.9971,-174.2542,-114.928),vec(1.35,1.35,1),"models/props_junk/terracotta01.mdl",BMat1,BCol2)
HN++,HT[HN,table] = table(637,18,0,vec(70,0,5),ang(16,0,180),vec(1.35,1.35,1),"models/props_junk/terracotta01.mdl",BMat1,BCol2)

#-Details
HN++,HT[HN,table] = table(638,18,0,vec(0.9671,48.5162,35.73),ang(0,-7,0),vec(0.5,0.35,1),"models/mechanics/robotics/g1.mdl",BMat1,BCol2)
HN++,HT[HN,table] = table(639,638,0,vec(0,2,0),ang(90,0,0),vec(1,1,3),"models/props_junk/PopCan01a.mdl",BMat1,BCol3)#flappy
HN++,HT[HN,table] = table(640,639,0,vec(25,0,0),ang(90,0,0),vec(0.7,0.5,4),"models/sprops/geometry/hhex_30.mdl",BMat1,BCol2)

HN++,HT[HN,table] = table(641,18,0,vec(0.9671,-48.5162,35.73),ang(0,187,0),vec(0.5,0.35,1),"models/mechanics/robotics/g1.mdl",BMat1,BCol2)
HN++,HT[HN,table] = table(642,641,0,vec(0,2,0),ang(90,0,0),vec(1,1,3),"models/props_junk/PopCan01a.mdl",BMat1,BCol3)#flappy
HN++,HT[HN,table] = table(643,642,0,vec(25,0,0),ang(90,0,0),vec(0.7,0.5,4),"models/sprops/geometry/hhex_30.mdl",BMat1,BCol2)

HN++,HT[HN,table] = table(644,18,0,vec(-35,27.5,55),ang(90,0,0),vec(3,1,1),"models/sprops/trans/exhaust/tip_3.mdl",BMat1,BCol2)
HN++,HT[HN,table] = table(645,18,0,vec(-35,-27.5,55),ang(90,0,0),vec(3,1,1),"models/sprops/trans/exhaust/tip_3.mdl",BMat1,BCol2)
HN++,HT[HN,table] = table(646,18,0,vec(48,-23,55),ang(90,0,0),vec(5,1,1),"models/sprops/trans/exhaust/tip_3.mdl",BMat1,BCol2)

HN++,HT[HN,table] = table(647,18,1,vec(94.5,8,-4.9),ang(78.305,-180,0),vec(8,8,8),"cylinder",EyeM,EyeC1)
HN++,HT[HN,table] = table(648,18,1,vec(94.5,-8,-4.9),ang(78.305,-180,0),vec(8,8,8),"cylinder",EyeM,EyeC1)
HN++,HT[HN,table] = table(649,0,0,vec(82,0,1),ang(0,-90,-115),Def,"models/props_c17/playground_teetertoter_stan.mdl",BMat1,BCol4)

#-MisslePods
HN++,HT[HN,table] = table(650,18,1,vec(25,0,48),ang(0,0,90),vec(6,6,90),"cylinder",BMat1,BCol3)
HN++,HT[HN,table] = table(651,650,0,vec(1,0.5,-48),ang(180,0,0),vec(0.25,0.75,0.5),"models/missiles/minipod.mdl",BMat1,BCol2)
HN++,HT[HN,table] = table(652,650,0,vec(1,0.5,48),ang(0,180,0),vec(0.25,0.75,0.5),"models/missiles/minipod.mdl",BMat1,BCol2)


#----------------------
#--Tail
HN++,HT[HN,table] = table(700,19,0,vec(-24,0,20),ang(0,90,0),vec(1,11,0.5),"models/sprops/geometry/hhex_60.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(701,19,0,vec(-24,27,-6),ang(0,0,180),vec(1.1,2,3),"models/sprops/geometry/hhex_30.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(702,19,0,vec(-24,-27,-6),ang(0,0,180),vec(1.1,2,3),"models/sprops/geometry/hhex_30.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(703,19,0,vec(-24,30.75,-15),ang(90,0,0),vec(1,1,2),"models/props_junk/PopCan01a.mdl",BMat1,BCol3)#flappy
HN++,HT[HN,table] = table(704,703,0,vec(19,0,0),ang(90,0,0),vec(0.5,0.5,3),"models/sprops/geometry/hhex_30.mdl",BMat1,BCol2)
HN++,HT[HN,table] = table(705,19,0,vec(-24,-30.75,-15),ang(90,0,0),vec(1,1,2),"models/props_junk/PopCan01a.mdl",BMat1,BCol3)#flappy
HN++,HT[HN,table] = table(706,705,0,vec(19,0,0),ang(90,0,0),vec(0.5,0.5,3),"models/sprops/geometry/hhex_30.mdl",BMat1,BCol2)
HN++,HT[HN,table] = table(707,19,0,vec(-24,0,-5),ang(0,0,90),vec(1.65)*2,"models/maxofs2d/hover_classic.mdl",BMat1,BCol3)

HN++,HT[HN,table] = table(708,20,0,vec(-24,0,20),ang(0,90,0),vec(1,11,0.5),"models/sprops/geometry/hhex_60.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(709,20,0,vec(-24,27,-6),ang(0,0,180),vec(1.1,2,3),"models/sprops/geometry/hhex_30.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(710,20,0,vec(-24,-27,-6),ang(0,0,180),vec(1.1,2,3),"models/sprops/geometry/hhex_30.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(711,20,0,vec(-24,30.75,-15),ang(90,0,0),vec(1,1,2),"models/props_junk/PopCan01a.mdl",BMat1,BCol3)#flappy
HN++,HT[HN,table] = table(712,711,0,vec(19,0,0),ang(90,0,0),vec(0.5,0.5,3),"models/sprops/geometry/hhex_30.mdl",BMat1,BCol2)
HN++,HT[HN,table] = table(713,20,0,vec(-24,-30.75,-15),ang(90,0,0),vec(1,1,2),"models/props_junk/PopCan01a.mdl",BMat1,BCol3)#flappy
HN++,HT[HN,table] = table(714,713,0,vec(19,0,0),ang(90,0,0),vec(0.5,0.5,3),"models/sprops/geometry/hhex_30.mdl",BMat1,BCol2)
HN++,HT[HN,table] = table(715,20,0,vec(-24,0,-5),ang(0,0,90),vec(1.65)*2,"models/maxofs2d/hover_classic.mdl",BMat1,BCol3)

HN++,HT[HN,table] = table(716,21,0,vec(-24,0,57.5),ang(0,0,90),vec(6,1,6),"models/sprops/misc/tubes/size_1/tube_12x60.mdl",BMat1,BCol1)#Yaw
HN++,HT[HN,table] = table(717,716,0,vec(0,-40,0),ang(0,0,90),vec(6,6,4),"models/sprops/misc/fittings/cred_12_9_short.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(718,716,0,vec(0,35,4.243*6-1),ang(45,0,0),vec(6,1,6),"models/sprops/misc/tubes_thin/size_1/t_q_tube_12x12.mdl",BMat1,BCol3)
HN++,HT[HN,table] = table(719,716,0,vec(0,35,-4.243*6+1),ang(-135,0,0),vec(6,1,6),"models/sprops/misc/tubes_thin/size_1/t_q_tube_12x12.mdl",BMat1,BCol3)
HN++,HT[HN,table] = table(720,21,0,vec(-24,0,-5),ang(0,0,90),vec(1.65)*2,"models/maxofs2d/hover_classic.mdl",BMat1,BCol3)

HN++,HT[HN,table] = table(721,21,0,vec(-24,0,90),ang(-10,0,0),vec(2),"models/Combine_Helicopter/helicopter_bomb01.mdl",BMat1,BCol2)#Pitch
HN++,HT[HN,table] = table(722,721,0,vec(100,0,10),ang(-90,0,0),vec(2,2,1),"models/props_phx/misc/potato_launcher.mdl",BMat1,BCol3)
HN++,HT[HN,table] = table(723,721,0,vec(40,0,10),ang(90,0,0),vec(2,2,1),"models/props_phx/misc/potato_launcher_chamber.mdl",BMat1,BCol3)


#----------------------
#--LeftArm
HN++,HT[HN,table] = table(800,18,0,vec(75,35,6),ang(0,0,-45),vec(0.7),"models/Combine_Helicopter/helicopter_bomb01.mdl",BMat1,BCol3)
HN++,HT[HN,table] = table(801,800,0,vec(8,18,0),ang(25,0,0),vec(0.7),"models/Combine_Helicopter/helicopter_bomb01.mdl",BMat1,BCol3)
HN++,HT[HN,table] = table(802,801,0,vec(10,0,0),ang(-90,0,0),vec(2.5,2.5,2),"models/sprops/misc/fittings/cred_12_9_short.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(803,801,0,vec(40,0,0),ang(90,0,0),vec(2.5,2.5,4),"models/sprops/misc/fittings/cred_12_6_tall.mdl",BMat1,BCol1)

#-Claw
HN++,HT[HN,table] = table(804,801,0,vec(65,0,-5),Ang,vec(0.135),"models/Combine_Helicopter/helicopter_bomb01.mdl",BMat1,BCol3)
HN++,HT[HN,table] = table(805,801,0,vec(65,0,-5):rotate(0,0,-120),ang(0,0,-120),vec(0.135),"models/Combine_Helicopter/helicopter_bomb01.mdl",BMat1,BCol3)
HN++,HT[HN,table] = table(806,801,0,vec(65,0,-5):rotate(0,0,-240),ang(0,0,-240),vec(0.135),"models/Combine_Helicopter/helicopter_bomb01.mdl",BMat1,BCol3)

HN++,HT[HN,table] = table(807,804,0,vec(12,0,-6),ang(90,-90,180),vec(0.45,0.5,0.5),"models/mechanics/robotics/claw.mdl",BMat1,BCol3)
HN++,HT[HN,table] = table(808,805,0,vec(12,0,-6),ang(90,-90,180),vec(0.45,0.5,0.5),"models/mechanics/robotics/claw.mdl",BMat1,BCol3)
HN++,HT[HN,table] = table(809,806,0,vec(12,0,-6),ang(90,-90,180),vec(0.45,0.5,0.5),"models/mechanics/robotics/claw.mdl",BMat1,BCol3)

#-Cannon
HN++,HT[HN,table] = table(810,801,0,vec(73,0,-3):rotate(0,0,-62.5),ang(0,0,-62.5),vec(0.35,0.65,0.65),"models/mortar/mortar_60mm.mdl","",BCol2)
HN++,HT[HN,table] = table(811,801,0,vec(73,0,-3):rotate(0,0,-120-62.5),ang(0,0,-120-62.5),vec(0.35,0.65,0.65),"models/mortar/mortar_60mm.mdl","",BCol2)
HN++,HT[HN,table] = table(812,801,0,vec(73,0,-3):rotate(0,0,-240-62.5),ang(0,0,-240-62.5),vec((0.35,0.65,0.65),"models/mortar/mortar_60mm.mdl","",BCol2)


#----------------------
#--RightArm
HN++,HT[HN,table] = table(900,18,0,vec(75,-35,6),ang(0,0,45),vec(0.7),"models/Combine_Helicopter/helicopter_bomb01.mdl",BMat1,BCol3)
HN++,HT[HN,table] = table(901,900,0,vec(8,-18,0),ang(25,0,0),vec(0.7),"models/Combine_Helicopter/helicopter_bomb01.mdl",BMat1,BCol3)
HN++,HT[HN,table] = table(902,901,0,vec(10,0,0),ang(-90,0,0),vec(2.5,2.5,2),"models/sprops/misc/fittings/cred_12_9_short.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(903,901,0,vec(40,0,0),ang(90,0,0),vec(2.5,2.5,4),"models/sprops/misc/fittings/cred_12_6_tall.mdl",BMat1,BCol1)

#-Claw
HN++,HT[HN,table] = table(904,901,0,vec(65,0,-5),Ang,vec(0.135),"models/Combine_Helicopter/helicopter_bomb01.mdl",BMat1,BCol3)
HN++,HT[HN,table] = table(905,901,0,vec(65,0,-5):rotate(0,0,-120),ang(0,0,-120),vec(0.135),"models/Combine_Helicopter/helicopter_bomb01.mdl",BMat1,BCol3)
HN++,HT[HN,table] = table(906,901,0,vec(65,0,-5):rotate(0,0,-240),ang(0,0,-240),vec(0.135),"models/Combine_Helicopter/helicopter_bomb01.mdl",BMat1,BCol3)

HN++,HT[HN,table] = table(907,904,0,vec(12,0,-6),ang(90,-90,180),vec(0.45,0.5,0.5),"models/mechanics/robotics/claw.mdl",BMat1,BCol3)
HN++,HT[HN,table] = table(908,905,0,vec(12,0,-6),ang(90,-90,180),vec(0.45,0.5,0.5),"models/mechanics/robotics/claw.mdl",BMat1,BCol3)
HN++,HT[HN,table] = table(909,906,0,vec(12,0,-6),ang(90,-90,180),vec(0.45,0.5,0.5),"models/mechanics/robotics/claw.mdl",BMat1,BCol3)

#-Cannon
HN++,HT[HN,table] = table(910,901,0,vec(73,0,-3):rotate(0,0,-62.5),ang(0,0,-62.5),vec(0.35,0.65,0.65),"models/mortar/mortar_60mm.mdl","",BCol2)
HN++,HT[HN,table] = table(911,901,0,vec(73,0,-3):rotate(0,0,-120-62.5),ang(0,0,-120-62.5),vec(0.35,0.65,0.65),"models/mortar/mortar_60mm.mdl","",BCol2)
HN++,HT[HN,table] = table(912,901,0,vec(73,0,-3):rotate(0,0,-240-62.5),ang(0,0,-240-62.5),vec(0.35,0.65,0.65),"models/mortar/mortar_60mm.mdl","",BCol2)


#----------------------
#--MiddleArm
HN++,HT[HN,table] = table(913,18,0,vec(65,0,-7),ang(10,0,0),vec(0.65),"models/Combine_Helicopter/helicopter_bomb01.mdl",BMat1,BCol3)
HN++,HT[HN,table] = table(914,913,1,vec(12,0,-9),Ang,vec(24,6,6),"",BMat1,BCol2)
HN++,HT[HN,table] = table(915,913,1,vec(28.5,0,-9),Ang,vec(9,6,6),"",BMat1,BCol4)


}

selfDestruct()

