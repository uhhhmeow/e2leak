@name holograms/mechs/alchemist/data
@persist [HN CN] [HT CT]:table


#----------------------
#-- GTable Setup
local ID = "alchemist"

XGT = gTable("_holocore", 0)

XGT[ID, table] = table(
    "Clips" = table(),
    "Holos" = table()
)

HT = XGT[ID, table]["Holos", table]
CT = XGT[ID, table]["Clips", table]


#----------------------
#-- Bones
local Skeleton = 0

local Scale = vec(12,12,12)/2
local Color = vec4(255,255,255,Skeleton*255)
local Material = ""
local Model = ""

local Vec = vec()
local Ang = ang()

local Default = vec(1,1,1)

local GlassM1 = "models/shiny"
local GlassC1 = vec4(0,255,0,20)

local BodyS1 = 0
local BodyM1 = BodyM2 = BodyM3 = ""
local BodyC1 = BodyC2 = BodyC3 = vec4(255,255,255,255)

#-Bones (Torso)
HN++,HT[HN,table] = table(1,999,1,vec(0,0,0),ang(0,0,0),Scale,Model,Material,Color)

#-Bones (Right Leg)
HN++,HT[HN,table] = table(2,1,1,vec(2,-17,-20),ang(0,0,0),Scale,Model,Material,Color)
HN++,HT[HN,table] = table(3,2,1,vec(0,-37.5,-15),ang(0,0,0),Scale,Model,Material,Color)
HN++,HT[HN,table] = table(4,3,1,vec(0,0,-55),ang(0,0,0),Scale,Model,Material,Color)
HN++,HT[HN,table] = table(5,4,1,vec(0,0,-35),ang(0,0,0),Scale,Model,Material,Color)
HN++,HT[HN,table] = table(6,5,1,vec(0,0,30),ang(0,0,0),Scale,Model,Material,Color)

#-Bones (Left Leg)
HN++,HT[HN,table] = table(7,1,1,vec(2,17,-20),ang(0,0,0),Scale,Model,Material,Color)
HN++,HT[HN,table] = table(8,7,1,vec(0,37.5,-15),ang(0,0,0),Scale,Model,Material,Color)
HN++,HT[HN,table] = table(9,8,1,vec(0,0,-55),ang(0,0,0),Scale,Model,Material,Color)
HN++,HT[HN,table] = table(10,9,1,vec(0,0,-35),ang(0,0,0),Scale,Model,Material,Color)
HN++,HT[HN,table] = table(11,10,1,vec(0,0,30),ang(0,0,0),Scale,Model,Material,Color)

#-Bones (Body)
HN++,HT[HN,table] = table(12,1,1,vec(0,0,13),ang(0,0,0),Scale,Model,Material,Color)
HN++,HT[HN,table] = table(13,12,1,vec(9,0,39.25),ang(0,0,0),Scale,Model,Material,Color)

#-Bones(Right Arm)
HN++,HT[HN,table] = table(14,12,1,vec(-40,-30,65),ang(0,-15,-75),Scale,Model,Material,Color)
HN++,HT[HN,table] = table(15,14,1,vec(0,0,-60),ang(0,0,30),Scale,Model,Material,Color)
HN++,HT[HN,table] = table(16,15,1,vec(0,0,-40),ang(0,0,30),Scale,Model,Material,Color)
HN++,HT[HN,table] = table(17,16,1,vec(0,0,-60),ang(0,0,0),Scale,Model,Material,Color)

#-Bones (Left Arm)
HN++,HT[HN,table] = table(18,12,1,vec(-40,30,65),ang(0,15,75),Scale,Model,Material,Color)
HN++,HT[HN,table] = table(19,18,1,vec(0,0,-60),ang(0,0,-30),Scale,Model,Material,Color)
HN++,HT[HN,table] = table(20,19,1,vec(0,0,-40),ang(0,0,-30),Scale,Model,Material,Color)
HN++,HT[HN,table] = table(21,20,1,vec(0,0,-60),ang(0,0,0),Scale,Model,Material,Color)

#-Bones (Weapons)
HN++,HT[HN,table] = table(22,12,1,vec(30,-58,11),ang(0,0,0),Scale,Model,Material,Color) #Right
HN++,HT[HN,table] = table(23,12,1,vec(30,58,11),ang(0,0,0),Scale,Model,Material,Color) #Left

if (!Skeleton)    {

#-Visuals (Torso, Bone: 1)
HN++,HT[HN,table] = table(24,1,0,vec(0,0,-11.5),ang(0,0,0),vec(1.5,1.5,0.9),"models/Mechanics/gears/gear16x24.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(25,1,1,vec(0,0,-0.5),ang(0,7,0),vec(64,64,22),"models/holograms/rcylinder.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(26,1,0,vec(0,0,0),ang(180,0,0),vec(3,3,1),"models/props_trainstation/trainstation_ornament002.mdl",BodyM1,BodyC1)

#-Visuals (Neck, Bone: 12)
HN++,HT[HN,table] = table(27,12,0,vec(0,0,-2.5),ang(0,12,90),vec(3,2,3),"models/props_wasteland/gear02.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(28,12,0,vec(0,0,6),ang(0,0,0),vec(0.75,0.75,0.75),"models/props_lab/teleplatform.mdl",BodyM2,BodyC2)
HN++,HT[HN,table] = table(29,12,0,vec(10,15,40),ang(45,180,90),vec(1,1,1),"models/props_lab/teleportring.mdl",BodyM2,BodyC2)
HN++,HT[HN,table] = table(30,12,0,vec(10,-23,40),ang(45,180,90),vec(1,1,1),"models/props_lab/teleportring.mdl",BodyM2,BodyC2)

#-Visuals (Head, Bone: 13)
HN++,HT[HN,table] = table(31,13,0,vec(0,-20,0),ang(0,90,0),vec(0.75,1.25,1.25),"models/props_vehicles/tire001b_truck.mdl",BodyM2,BodyC2)
HN++,HT[HN,table] = table(32,13,0,vec(0,20,0),ang(0,90,0),vec(0.75,1.25,1.25),"models/props_vehicles/tire001b_truck.mdl",BodyM2,BodyC2)
HN++,HT[HN,table] = table(33,13,0,vec(0,0,0),ang(0,0,0),vec(1.1,1.1,1.1),"models/sprops/geometry/sphere_60.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(34,13,0,vec(0,0,0),ang(0,0,0),-vec(1.1,1.1,1.1),"models/sprops/geometry/sphere_60.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(35,13,1,vec(30,0,0),ang(90,0,0),vec(32,32,3),"hq_tube_thin",BodyM1,BodyC1)
HN++,HT[HN,table] = table(36,13,1,vec(30,0,0),ang(90,0,0),vec(31,31,1),"hq_cylinder",GlassM1,GlassC1)
HN++,HT[HN,table] = table(37,13,1,vec(0,-24,0),ang(0,0,90),vec(40,40,0.1),"hq_cylinder",GlassM1,GlassC1)
HN++,HT[HN,table] = table(38,13,1,vec(0,24,0),ang(0,0,90),vec(40,40,0.1),"hq_cylinder",GlassM1,GlassC1)

CN++,CT[CN,table] = table(33, 1, vec(0,20,0), vec(0,-1,0))
CN++,CT[CN,table] = table(33, 2, vec(0,-20,0), vec(0,1,0))
CN++,CT[CN,table] = table(33, 3, vec(29,0,0), vec(-1,0,0))
CN++,CT[CN,table] = table(34, 1, vec(0,20,0), vec(0,-1,0))
CN++,CT[CN,table] = table(34, 2, vec(0,-20,0), vec(0,1,0))
CN++,CT[CN,table] = table(34, 3, vec(29,0,0), vec(-1,0,0))


#-Visuals (Right Leg, Bones: 2, 3, 4, 5, 6, )
HN++,HT[HN,table] = table(39,2,0,vec(0,-2,-10),ang(-90,-90,0),vec(1.5,1,1),"models/props_wasteland/light_spotlight01_lamp.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(40,2,0,vec(0,-11,-15),ang(-90,180,0),vec(2,4.5,2),"models/props_wasteland/gear02.mdl",BodyM1,BodyC1)

HN++,HT[HN,table] = table(41,3,0,vec(0,-6,0),ang(0,0,-90),vec(2,2,0.4),"models/props_junk/MetalBucket01a.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(42,3,0,vec(0,6,0),ang(0,0,90),vec(2,2,0.4),"models/props_junk/MetalBucket01a.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(43,3,0,vec(0,-1,0),ang(0,0,0),vec(1.75,1.75,1.75),"models/props_wasteland/gear01.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(44,3,0,vec(0,6,-32),ang(0,90,90),vec(2,3,3),"models/props_c17/utilityconnecter005.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(45,3,0,vec(0,-6,-32),ang(0,90,90),vec(2,3,3),"models/props_c17/utilityconnecter005.mdl",BodyM1,BodyC1)

HN++,HT[HN,table] = table(46,4,0,vec(0,5.5,0),ang(0,0,90),vec(0.5,0.5,0.25),"models/props_c17/oildrum001.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(47,4,0,vec(-10,4,0),ang(-16,0,0),vec(0.7,0.7,1.85),"models/props_junk/propane_tank001a.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(48,4,0,vec(-10,-4,0),ang(-16,0,0),vec(0.7,0.7,1.85),"models/props_junk/propane_tank001a.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(49,4,0,vec(-10,-1.25,5),ang(32,0,90),vec(5,1,1),"models/sprops/misc/alphanum/alphanum_rbracket.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(50,4,0,vec(-5,-1.25,-10),ang(-72,0,90),vec(5,1,1),"models/sprops/misc/alphanum/alphanum_rbracket.mdl",BodyM1,BodyC1)

HN++,HT[HN,table] = table(51,5,0,vec(0,-4.5,0),ang(0,0,-90),vec(1.5,1.5,0.375),"models/props_junk/MetalBucket01a.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(52,5,0,vec(0,4.5,0),ang(0,0,90),vec(1.5,1.5,0.375),"models/props_junk/MetalBucket01a.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(53,5,0,vec(0,-0.75,0),ang(0,0,0),vec(1.3,1.3,1.3),"models/props_wasteland/gear01.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(54,5,0,vec(0,4.5,16),ang(0,90,90),vec(2,1.5,1.5),"models/props_c17/utilityconnecter005.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(55,5,0,vec(0,-4.5,16),ang(0,90,90),vec(2,1.5,1.5),"models/props_c17/utilityconnecter005.mdl",BodyM1,BodyC1)

HN++,HT[HN,table] = table(56,6,0,vec(2,14,-1),ang(0,0,0),vec(0.2,0.2,0.2),"models/props_wasteland/laundry_washer003.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(57,6,0,vec(2,-14,-1),ang(0,180,0),vec(0.2,0.2,0.2),"models/props_wasteland/laundry_washer003.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(58,6,0,vec(16,0,-1),ang(0,-90,0),vec(0.2,0.2,0.2),"models/props_wasteland/laundry_washer003.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(59,6,0,vec(-12,0,-1),ang(0,90,0),vec(0.2,0.2,0.2),"models/props_wasteland/laundry_washer003.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(60,56,0,vec(0,12,-4),ang(0,90,0),vec(0.35,0.75,0.25),"models/props_radiostation/radio_antenna01_pylon.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(61,57,0,vec(0,12,-4),ang(0,90,0),vec(0.35,0.75,0.25),"models/props_radiostation/radio_antenna01_pylon.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(62,58,0,vec(0,12,-4),ang(0,90,0),vec(0.35,0.75,0.25),"models/props_radiostation/radio_antenna01_pylon.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(63,59,0,vec(0,12,-4),ang(0,90,0),vec(0.35,0.75,0.25),"models/props_radiostation/radio_antenna01_pylon.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(64,6,0,vec(-3,0,4),ang(90,0,0),vec(0.5,1.5,1.5),"models/props_wasteland/light_spotlight01_lamp.mdl",BodyM1,BodyC1)


#-Visuals (Left Leg, Bones: 7, 8, 9, 10, 11, )
HN++,HT[HN,table] = table(65,7,0,vec(0,2,-10),ang(-90,90,0),vec(1.5,1,1),"models/props_wasteland/light_spotlight01_lamp.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(66,7,0,vec(0,11,-15),ang(-90,0,0),vec(2,4.5,2),"models/props_wasteland/gear02.mdl",BodyM1,BodyC1)

HN++,HT[HN,table] = table(67,8,0,vec(0,-6,0),ang(0,0,-90),vec(2,2,0.4),"models/props_junk/MetalBucket01a.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(68,8,0,vec(0,6,0),ang(0,0,90),vec(2,2,0.4),"models/props_junk/MetalBucket01a.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(69,8,0,vec(0,-1,0),ang(0,0,0),vec(1.75,1.75,1.75),"models/props_wasteland/gear01.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(70,8,0,vec(0,6,-32),ang(0,90,90),vec(2,3,3),"models/props_c17/utilityconnecter005.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(71,8,0,vec(0,-6,-32),ang(0,90,90),vec(2,3,3),"models/props_c17/utilityconnecter005.mdl",BodyM1,BodyC1)

HN++,HT[HN,table] = table(72,9,0,vec(0,5.5,0),ang(0,0,90),vec(0.5,0.5,0.25),"models/props_c17/oildrum001.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(73,9,0,vec(-10,4,0),ang(-16,0,0),vec(0.7,0.7,1.85),"models/props_junk/propane_tank001a.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(74,9,0,vec(-10,-4,0),ang(-16,0,0),vec(0.7,0.7,1.85),"models/props_junk/propane_tank001a.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(75,9,0,vec(-10,-1.25,5),ang(32,0,90),vec(5,1,1),"models/sprops/misc/alphanum/alphanum_rbracket.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(76,9,0,vec(-5,-1.25,-10),ang(-72,0,90),vec(5,1,1),"models/sprops/misc/alphanum/alphanum_rbracket.mdl",BodyM1,BodyC1)

HN++,HT[HN,table] = table(77,10,0,vec(0,-4.5,0),ang(0,0,-90),vec(1.5,1.5,0.375),"models/props_junk/MetalBucket01a.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(78,10,0,vec(0,4.5,0),ang(0,0,90),vec(1.5,1.5,0.375),"models/props_junk/MetalBucket01a.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(79,10,0,vec(0,-0.75,0),ang(0,0,0),vec(1.3,1.3,1.3),"models/props_wasteland/gear01.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(80,10,0,vec(0,4.5,16),ang(0,90,90),vec(2,1.5,1.5),"models/props_c17/utilityconnecter005.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(81,10,0,vec(0,-4.5,16),ang(0,90,90),vec(2,1.5,1.5),"models/props_c17/utilityconnecter005.mdl",BodyM1,BodyC1)

HN++,HT[HN,table] = table(82,11,0,vec(2,14,-1),ang(0,0,0),vec(0.2,0.2,0.2),"models/props_wasteland/laundry_washer003.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(83,11,0,vec(2,-14,-1),ang(0,180,0),vec(0.2,0.2,0.2),"models/props_wasteland/laundry_washer003.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(84,11,0,vec(16,0,-1),ang(0,-90,0),vec(0.2,0.2,0.2),"models/props_wasteland/laundry_washer003.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(85,11,0,vec(-12,0,-1),ang(0,90,0),vec(0.2,0.2,0.2),"models/props_wasteland/laundry_washer003.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(86,82,0,vec(0,12,-4),ang(0,90,0),vec(0.35,0.75,0.25),"models/props_radiostation/radio_antenna01_pylon.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(87,83,0,vec(0,12,-4),ang(0,90,0),vec(0.35,0.75,0.25),"models/props_radiostation/radio_antenna01_pylon.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(88,84,0,vec(0,12,-4),ang(0,90,0),vec(0.35,0.75,0.25),"models/props_radiostation/radio_antenna01_pylon.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(89,85,0,vec(0,12,-4),ang(0,90,0),vec(0.35,0.75,0.25),"models/props_radiostation/radio_antenna01_pylon.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(90,11,0,vec(-3,0,4),ang(90,0,0),vec(0.5,1.5,1.5),"models/props_wasteland/light_spotlight01_lamp.mdl",BodyM1,BodyC1)


#- Visuals (Right Arm, Bones: 14,15,16,17)
HN++,HT[HN,table] = table(91,14,0,vec(0,-6,0),ang(0,0,-90),vec(2,2,0.4),"models/props_junk/MetalBucket01a.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(92,14,0,vec(0,6,0),ang(0,0,90),vec(2,2,0.4),"models/props_junk/MetalBucket01a.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(93,14,0,vec(0,-1,0),ang(0,0,0),vec(1.75,1.75,1.75),"models/props_wasteland/gear01.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(94,14,0,vec(0,6,-32),ang(0,90,90),vec(2,3,3),"models/props_c17/utilityconnecter005.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(95,14,0,vec(0,-6,-32),ang(0,90,90),vec(2,3,3),"models/props_c17/utilityconnecter005.mdl",BodyM1,BodyC1)

HN++,HT[HN,table] = table(96,15,0,vec(0,8.5,0),ang(0,0,90),vec(0.65,0.65,0.35),"models/props_c17/oildrum001.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(97,15,0,vec(-10,4,-10),ang(-16,0,0),vec(0.7,0.7,1.85),"models/props_junk/propane_tank001a.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(98,15,0,vec(-10,-4,-10),ang(-16,0,0),vec(0.7,0.7,1.85),"models/props_junk/propane_tank001a.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(99,15,0,vec(-10,-1.25,5),ang(32,0,90),vec(5,1,1),"models/sprops/misc/alphanum/alphanum_rbracket.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(100,15,0,vec(-5,-1.25,-10),ang(-72,0,90),vec(5,1,1),"models/sprops/misc/alphanum/alphanum_rbracket.mdl",BodyM1,BodyC1)

HN++,HT[HN,table] = table(101,16,0,vec(0,-4.5,0),ang(0,0,-90),vec(1.5,1.5,0.375),"models/props_junk/MetalBucket01a.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(102,16,0,vec(0,4.5,0),ang(0,0,90),vec(1.5,1.5,0.375),"models/props_junk/MetalBucket01a.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(103,16,0,vec(0,-0.75,0),ang(0,0,0),vec(1.3,1.3,1.3),"models/props_wasteland/gear01.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(104,16,0,vec(0,4.5,-32),ang(0,90,90),vec(2,4,1.5),"models/props_c17/utilityconnecter005.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(105,16,0,vec(0,-4.5,-32),ang(0,90,90),vec(2,4,1.5),"models/props_c17/utilityconnecter005.mdl",BodyM1,BodyC1)

HN++,HT[HN,table] = table(106,17,0,vec(0,0,0),ang(0,0,90),vec(2.5,2.5,2.5),"models/props_junk/sawblade001a.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(107,17,0,vec(0,-1,0),ang(0,0,0),vec(1.5,1.5,1.5),"models/props_wasteland/gear01.mdl",BodyM1,BodyC1)

#- Visuals (Left Arm, Bones: 18,19,20,21)
HN++,HT[HN,table] = table(108,18,0,vec(0,-6,0),ang(0,0,-90),vec(2,2,0.4),"models/props_junk/MetalBucket01a.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(109,18,0,vec(0,6,0),ang(0,0,90),vec(2,2,0.4),"models/props_junk/MetalBucket01a.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(110,18,0,vec(0,-1,0),ang(0,0,0),vec(1.75,1.75,1.75),"models/props_wasteland/gear01.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(111,18,0,vec(0,6,-32),ang(0,90,90),vec(2,3,3),"models/props_c17/utilityconnecter005.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(112,18,0,vec(0,-6,-32),ang(0,90,90),vec(2,3,3),"models/props_c17/utilityconnecter005.mdl",BodyM1,BodyC1)

HN++,HT[HN,table] = table(113,19,0,vec(0,8.5,0),ang(0,0,90),vec(0.65,0.65,0.35),"models/props_c17/oildrum001.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(114,19,0,vec(-10,4,-10),ang(-16,0,0),vec(0.7,0.7,1.85),"models/props_junk/propane_tank001a.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(115,19,0,vec(-10,-4,-10),ang(-16,0,0),vec(0.7,0.7,1.85),"models/props_junk/propane_tank001a.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(116,19,0,vec(-10,-1.25,5),ang(32,0,90),vec(5,1,1),"models/sprops/misc/alphanum/alphanum_rbracket.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(117,19,0,vec(-5,-1.25,-10),ang(-72,0,90),vec(5,1,1),"models/sprops/misc/alphanum/alphanum_rbracket.mdl",BodyM1,BodyC1)

HN++,HT[HN,table] = table(118,20,0,vec(0,-4.5,0),ang(0,0,-90),vec(1.5,1.5,0.375),"models/props_junk/MetalBucket01a.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(119,20,0,vec(0,4.5,0),ang(0,0,90),vec(1.5,1.5,0.375),"models/props_junk/MetalBucket01a.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(120,20,0,vec(0,-0.75,0),ang(0,0,0),vec(1.3,1.3,1.3),"models/props_wasteland/gear01.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(121,20,0,vec(0,4.5,-32),ang(0,90,90),vec(2,4,1.5),"models/props_c17/utilityconnecter005.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(122,20,0,vec(0,-4.5,-32),ang(0,90,90),vec(2,4,1.5),"models/props_c17/utilityconnecter005.mdl",BodyM1,BodyC1)

HN++,HT[HN,table] = table(123,21,0,vec(0,0,0),ang(0,0,90),vec(2.5,2.5,2.5),"models/props_junk/sawblade001a.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(124,21,0,vec(0,-1,0),ang(0,0,0),vec(1.5,1.5,1.5),"models/props_wasteland/gear01.mdl",BodyM1,BodyC1)


#-Visuals (Gatling Gun, Bone: 14)
#[HN++,HT[HN,table] = table(125,22,0,vec(20,0,-8),ang(90,-90,0),vec(0.5,0.5,0.5),"models/props_mining/elevator_winch_cog.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(126,22,0,vec(20,0,9.5),ang(90,-90,0),vec(0.5,0.5,0.5),"models/props_mining/elevator_winch_cog.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(127,126,1,vec(0,20,2),ang(0,0,90),vec(1.5,1.5,50),"hq_tube_thin",BodyM1,BodyC1)
HN++,HT[HN,table] = table(128,126,1,vec(0,20,2):rotate(72,0,0),ang(0,0,90),vec(1.5,1.5,50),"hq_tube_thin",BodyM1,BodyC1)
HN++,HT[HN,table] = table(129,126,1,vec(0,20,2):rotate(144,0,0),ang(0,0,90),vec(1.5,1.5,50),"hq_tube_thin",BodyM1,BodyC1)
HN++,HT[HN,table] = table(130,126,1,vec(0,20,2):rotate(216,0,0),ang(0,0,90),vec(1.5,1.5,50),"hq_tube_thin",BodyM1,BodyC1)
HN++,HT[HN,table] = table(131,126,1,vec(0,20,2):rotate(288,0,0),ang(0,0,90),vec(1.5,1.5,50),"hq_tube_thin",BodyM1,BodyC1)
HN++,HT[HN,table] = table(132,126,0,vec(0,35,0),ang(0,0,90),vec(0.125,0.125,0.2),"models/props_wasteland/laundry_basket001.mdl",BodyM1,BodyC1)

HN++,HT[HN,table] = table(133,22,0,vec(-30,0,9),ang(90,0,0),vec(0.55,0.55,0.75),"models/props_c17/canister_propane01a.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(134,22,0,vec(1,-1,-14),ang(180,0,0),vec(1.25,1,1),"models/gibs/airboat_broken_engine.mdl",BodyM1,BodyC1)
HN++,HT[HN,table] = table(135,22,0,vec(0,20,0),ang(180,0,90),vec(2.25,5,2.5),"models/props_wasteland/light_spotlight02_base.mdl",BodyM1,BodyC1)]#

}


selfDestruct()

