@name holograms/creatures/widow/data
@persist [HN CN] [HT CT]:table

#----------------------
#--GTableSetup
local ID = "BlackWidow_V1"

XGT = gTable("_holocore",0)

XGT[ID,table] = table(
    "Clips" = table(),
    "Holos" = table()
)

HT = XGT[ID,table]["Holos",table]
CT = XGT[ID,table]["Clips",table]


#----------------------
#--Bones
local Skeleton = 0

local Scale = vec(12,12,12)/2
local Color = vec4(255,255,255,Skeleton*255)
local Material = ""
local Model = ""

local Vec = vec()
local Ang = ang()


#----------------------
#--Body
HN++,HT[HN,table] = table(1,0,1,Vec,Ang,Scale,Model,Material,Color)


#----------------------
#--Rear
HN++,HT[HN,table] = table(2,1,1,vec(-15,0,0),Ang,Scale,Model,Material,Color)


#----------------------
#--Fangs
HN++,HT[HN,table] = table(3,1,1,vec(15,2,-2),ang(60,10,-10),Scale,Model,Material,Color)#Left
HN++,HT[HN,table] = table(4,1,1,vec(15,-2,-2),ang(60,-10,10),Scale,Model,Material,Color)#Right


#----------------------
#--Spinner
HN++,HT[HN,table] = table(5,2,1,vec(-50,0,10),Ang,Scale,Model,Material,Color)


#----------------------
#--FrontLegs
HN++,HT[HN,table] = table(6,1,1,vec(10,10,0),Ang,Scale,Model,Material,Color)#Left
HN++,HT[HN,table] = table(7,6,1,Vec,Ang,Scale,Model,Material,Color)
HN++,HT[HN,table] = table(8,7,1,vec(0,0,75),Ang,Scale,Model,Material,Color)
HN++,HT[HN,table] = table(9,8,1,vec(0,0,50),Ang,Scale,Model,Material,Color)

HN++,HT[HN,table] = table(10,1,1,vec(10,-10,0),Ang,Scale,Model,Material,Color)#Right
HN++,HT[HN,table] = table(11,10,1,Vec,Ang,Scale,Model,Material,Color)
HN++,HT[HN,table] = table(12,11,1,vec(0,0,75),Ang,Scale,Model,Material,Color)
HN++,HT[HN,table] = table(13,12,1,vec(0,0,50),Ang,Scale,Model,Material,Color)


#----------------------
#--MiddleLegs
HN++,HT[HN,table] = table(14,1,1,vec(5,13,0),Ang,Scale,Model,Material,Color)#LeftF
HN++,HT[HN,table] = table(15,14,1,Vec,Ang,Scale,Model,Material,Color)
HN++,HT[HN,table] = table(16,15,1,vec(0,0,60),Ang,Scale,Model,Material,Color)
HN++,HT[HN,table] = table(17,16,1,vec(0,0,35),Ang,Scale,Model,Material,Color)

HN++,HT[HN,table] = table(18,1,1,vec(0,13,0),Ang,Scale,Model,Material,Color)#LeftR
HN++,HT[HN,table] = table(19,18,1,Vec,Ang,Scale,Model,Material,Color)
HN++,HT[HN,table] = table(20,19,1,vec(0,0,60),Ang,Scale,Model,Material,Color)
HN++,HT[HN,table] = table(21,20,1,vec(0,0,35),Ang,Scale,Model,Material,Color)

HN++,HT[HN,table] = table(22,1,1,vec(5,-13,0),Ang,Scale,Model,Material,Color)#RightF
HN++,HT[HN,table] = table(23,22,1,Vec,Ang,Scale,Model,Material,Color)
HN++,HT[HN,table] = table(24,23,1,vec(0,0,60),Ang,Scale,Model,Material,Color)
HN++,HT[HN,table] = table(25,24,1,vec(0,0,35),Ang,Scale,Model,Material,Color)

HN++,HT[HN,table] = table(26,1,1,vec(0,-13,0),Ang,Scale,Model,Material,Color)#RightR
HN++,HT[HN,table] = table(27,26,1,Vec,Ang,Scale,Model,Material,Color)
HN++,HT[HN,table] = table(28,27,1,vec(0,0,60),Ang,Scale,Model,Material,Color)
HN++,HT[HN,table] = table(29,28,1,vec(0,0,35),Ang,Scale,Model,Material,Color)


#----------------------
#--RearLegs
HN++,HT[HN,table] = table(30,1,1,vec(-5,12,0),Ang,Scale,Model,Material,Color)#Left
HN++,HT[HN,table] = table(31,30,1,Vec,Ang,Scale,Model,Material,Color)
HN++,HT[HN,table] = table(32,31,1,vec(0,0,60),Ang,Scale,Model,Material,Color)
HN++,HT[HN,table] = table(33,32,1,vec(0,0,35),Ang,Scale,Model,Material,Color)

HN++,HT[HN,table] = table(34,1,1,vec(-5,-12,0),Ang,Scale,Model,Material,Color)#Right
HN++,HT[HN,table] = table(35,34,1,Vec,Ang,Scale,Model,Material,Color)
HN++,HT[HN,table] = table(36,35,1,vec(0,0,60),Ang,Scale,Model,Material,Color)
HN++,HT[HN,table] = table(37,36,1,vec(0,0,35),Ang,Scale,Model,Material,Color)


if(Skeleton){


local Color = vec4(35,35,35,255)
local Scale = Scale-1


#----------------------
#--FrontLegs
HN++,HT[HN,table] = table(38,7,1,vec(0,0,37.5),Ang,Scale:setZ(75),Model,Material,Color)#Left
HN++,HT[HN,table] = table(39,8,1,vec(0,0,25),Ang,Scale:setZ(50),Model,Material,Color)
HN++,HT[HN,table] = table(40,9,1,vec(0,0,37.5),Ang,Scale:setZ(75),Model,Material,Color)

HN++,HT[HN,table] = table(41,11,1,vec(0,0,37.5),Ang,Scale:setZ(75),Model,Material,Color)#Left
HN++,HT[HN,table] = table(42,12,1,vec(0,0,25),Ang,Scale:setZ(50),Model,Material,Color)
HN++,HT[HN,table] = table(43,13,1,vec(0,0,37.5),Ang,Scale:setZ(75),Model,Material,Color)


#----------------------
#--MiddleLegs
HN++,HT[HN,table] = table(44,15,1,vec(0,0,30),Ang,Scale:setZ(60),Model,Material,Color)#LeftF
HN++,HT[HN,table] = table(45,16,1,vec(0,0,17.5),Ang,Scale:setZ(35),Model,Material,Color)
HN++,HT[HN,table] = table(46,17,1,vec(0,0,30),Ang,Scale:setZ(60),Model,Material,Color)

HN++,HT[HN,table] = table(47,19,1,vec(0,0,30),Ang,Scale:setZ(60),Model,Material,Color)#LeftR
HN++,HT[HN,table] = table(48,20,1,vec(0,0,17.5),Ang,Scale:setZ(35),Model,Material,Color)
HN++,HT[HN,table] = table(49,21,1,vec(0,0,30),Ang,Scale:setZ(60),Model,Material,Color)

HN++,HT[HN,table] = table(50,23,1,vec(0,0,30),Ang,Scale:setZ(60),Model,Material,Color)#RightF
HN++,HT[HN,table] = table(51,24,1,vec(0,0,17.5),Ang,Scale:setZ(35),Model,Material,Color)
HN++,HT[HN,table] = table(52,25,1,vec(0,0,30),Ang,Scale:setZ(60),Model,Material,Color)

HN++,HT[HN,table] = table(53,27,1,vec(0,0,30),Ang,Scale:setZ(60),Model,Material,Color)#RightR
HN++,HT[HN,table] = table(54,28,1,vec(0,0,17.5),Ang,Scale:setZ(35),Model,Material,Color)
HN++,HT[HN,table] = table(55,29,1,vec(0,0,30),Ang,Scale:setZ(60),Model,Material,Color)


#----------------------
#--Rear
HN++,HT[HN,table] = table(56,31,1,vec(0,0,30),Ang,Scale:setZ(60),Model,Material,Color)#Left
HN++,HT[HN,table] = table(57,32,1,vec(0,0,17.5),Ang,Scale:setZ(35),Model,Material,Color)
HN++,HT[HN,table] = table(58,33,1,vec(0,0,30),Ang,Scale:setZ(60),Model,Material,Color)

HN++,HT[HN,table] = table(59,35,1,vec(0,0,30),Ang,Scale:setZ(60),Model,Material,Color)#Right
HN++,HT[HN,table] = table(60,36,1,vec(0,0,17.5),Ang,Scale:setZ(35),Model,Material,Color)
HN++,HT[HN,table] = table(61,37,1,vec(0,0,30),Ang,Scale:setZ(60),Model,Material,Color)

}
else{


local BMat1 = "sprops/textures/sprops_metal1"
local BCol1 = vec4(25,25,25,255)

local BMat2 = "sprops/textures/sprops_metal1"
local BCol2 = vec4(200,0,0,255)


#----------------------
#--Back
HN++,HT[HN,table] = table(62,2,0,vec(-24,0,0):rotate(24,0,0),ang(24,0,0),vec(1.25,1,1),"models/sprops/geometry/sphere_48.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(63,62,0,Vec,Ang,vec(1.25,1,1)*1.01,"models/sprops/geometry/sphere_48.mdl",BMat2,BCol2)
HN++,HT[HN,table] = table(64,62,0,Vec,Ang,vec(1.25,1,1)*1.01,"models/sprops/geometry/sphere_48.mdl",BMat2,BCol2)

CN++,CT[CN,table] = table(63,1,vec(-5,0,0),vec(0.5,1,0))
CN++,CT[CN,table] = table(63,2,vec(-5,0,0),vec(0.5,-1,0))
CN++,CT[CN,table] = table(63,3,vec(0,0,20),vec(0,0,1))

CN++,CT[CN,table] = table(64,1,vec(5,0,0),vec(-0.5,-1,0))
CN++,CT[CN,table] = table(64,2,vec(5,0,0),vec(-0.5,1,0))
CN++,CT[CN,table] = table(64,3,vec(0,0,20),vec(0,0,1))


#----------------------
#--Front
HN++,HT[HN,table] = table(65,1,0,Vec,Ang,vec(1.25,1,0.75),"models/sprops/geometry/sphere_30.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(66,1,0,Vec,Ang,vec(1.25,1,0.5),"models/sprops/geometry/sphere_30.mdl",BMat1,BCol1)

CN++,CT[CN,table] = table(65,1,vec(0,0,0.5),vec(0,0,-1))
CN++,CT[CN,table] = table(66,1,vec(0,0,-0.5),vec(0,0,1))

local EyeMat = "models/shiny"
local EyeCol = vec4(50,0,0,255)

HN++,HT[HN,table] = table(67,1,1,vec(15,0,4):rotate(0,5,0),Ang,vec(2,2,2),"hq_sphere",EyeMat,EyeCol)
HN++,HT[HN,table] = table(68,1,1,vec(15,0,4):rotate(0,-5,0),Ang,vec(2,2,2),"hq_sphere",EyeMat,EyeCol)
HN++,HT[HN,table] = table(69,1,1,vec(14,0,4.5):rotate(0,13,0),Ang,vec(2,2,2)/1.25,"hq_sphere",EyeMat,EyeCol)
HN++,HT[HN,table] = table(70,1,1,vec(14,0,4.5):rotate(0,-13,0),Ang,vec(2,2,2)/1.25,"hq_sphere",EyeMat,EyeCol)

HN++,HT[HN,table] = table(71,1,1,vec(17.5,0,2):rotate(0,4,0),Ang,vec(2,2,2)/1.5,"hq_sphere",EyeMat,EyeCol)
HN++,HT[HN,table] = table(72,1,1,vec(17.5,0,2):rotate(0,-4,0),Ang,vec(2,2,2)/1.5,"hq_sphere",EyeMat,EyeCol)
HN++,HT[HN,table] = table(73,1,1,vec(17,0,2.5):rotate(0,12,0),Ang,vec(2,2,2)/1.75,"hq_sphere",EyeMat,EyeCol)
HN++,HT[HN,table] = table(74,1,1,vec(17,0,2.5):rotate(0,-12,0),Ang,vec(2,2,2)/1.75,"hq_sphere",EyeMat,EyeCol)


#----------------------
#--Fangs
HN++,HT[HN,table] = table(75,3,0,vec(6,0,1),Ang,vec(1.25,1,1),"models/gibs/antlion_gib_small_1.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(76,4,0,vec(6,0,1),Ang,vec(1.25,1,1),"models/gibs/antlion_gib_small_1.mdl",BMat1,BCol1)


#----------------------
#--FrontLegs
HN++,HT[HN,table] = table(77,7,1,vec(0,0,37.5),ang(),vec(5,5,75),"models/sprops/misc/fittings/cred_12_6_tall.mdl",BMat1,BCol1)#Left
HN++,HT[HN,table] = table(78,8,1,vec(0,0,25),ang(),vec(3,3,50),"models/sprops/misc/fittings/cred_12_6_tall.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(79,9,1,vec(0,0,0),ang(),vec(1.5,1.5,75),"models/sprops/misc/cones/size_1/cone_12x12.mdl",BMat1,BCol1)

HN++,HT[HN,table] = table(80,11,1,vec(0,0,37.5),ang(),vec(5,5,75),"models/sprops/misc/fittings/cred_12_6_tall.mdl",BMat1,BCol1)#Right
HN++,HT[HN,table] = table(81,12,1,vec(0,0,25),ang(),vec(3,3,50),"models/sprops/misc/fittings/cred_12_6_tall.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(82,13,1,vec(0,0,0),ang(),vec(1.5,1.5,75),"models/sprops/misc/cones/size_1/cone_12x12.mdl",BMat1,BCol1)


#----------------------
#--MiddleLegs
HN++,HT[HN,table] = table(83,15,1,vec(0,0,30),Ang,vec(5,5,60),"models/sprops/misc/fittings/cred_12_6_tall.mdl",BMat1,BCol1)#LeftF
HN++,HT[HN,table] = table(84,16,1,vec(0,0,17.5),Ang,vec(3,3,35),"models/sprops/misc/fittings/cred_12_6_tall.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(85,17,1,Vec,Ang,vec(1.5,1.5,60),"models/sprops/misc/cones/size_1/cone_12x12.mdl",BMat1,BCol1)

HN++,HT[HN,table] = table(86,19,1,vec(0,0,30),Ang,vec(5,5,60),"models/sprops/misc/fittings/cred_12_6_tall.mdl",BMat1,BCol1)#LeftR
HN++,HT[HN,table] = table(87,20,1,vec(0,0,17.5),Ang,vec(3,3,35),"models/sprops/misc/fittings/cred_12_6_tall.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(88,21,1,Vec,Ang,vec(1.5,1.5,60),"models/sprops/misc/cones/size_1/cone_12x12.mdl",BMat1,BCol1)

HN++,HT[HN,table] = table(89,23,1,vec(0,0,30),Ang,vec(5,5,60),"models/sprops/misc/fittings/cred_12_6_tall.mdl",BMat1,BCol1)#RightF
HN++,HT[HN,table] = table(90,24,1,vec(0,0,17.5),Ang,vec(3,3,35),"models/sprops/misc/fittings/cred_12_6_tall.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(91,25,1,Vec,Ang,vec(1.5,1.5,60),"models/sprops/misc/cones/size_1/cone_12x12.mdl",BMat1,BCol1)

HN++,HT[HN,table] = table(92,27,1,vec(0,0,30),Ang,vec(5,5,60),"models/sprops/misc/fittings/cred_12_6_tall.mdl",BMat1,BCol1)#RightR
HN++,HT[HN,table] = table(93,28,1,vec(0,0,17.5),Ang,vec(3,3,35),"models/sprops/misc/fittings/cred_12_6_tall.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(94,29,1,Vec,Ang,vec(1.5,1.5,60),"models/sprops/misc/cones/size_1/cone_12x12.mdl",BMat1,BCol1)


#----------------------
#--RearLegs
HN++,HT[HN,table] = table(95,31,1,vec(0,0,30),Ang,vec(5,5,60),"models/sprops/misc/fittings/cred_12_6_tall.mdl",BMat1,BCol1)#LeftF
HN++,HT[HN,table] = table(96,32,1,vec(0,0,17.5),Ang,vec(3,3,35),"models/sprops/misc/fittings/cred_12_6_tall.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(97,33,1,Vec,Ang,vec(1.5,1.5,60),"models/sprops/misc/cones/size_1/cone_12x12.mdl",BMat1,BCol1)

HN++,HT[HN,table] = table(98,35,1,vec(0,0,30),Ang,vec(5,5,60),"models/sprops/misc/fittings/cred_12_6_tall.mdl",BMat1,BCol1)#LeftR
HN++,HT[HN,table] = table(99,36,1,vec(0,0,17.5),Ang,vec(3,3,35),"models/sprops/misc/fittings/cred_12_6_tall.mdl",BMat1,BCol1)
HN++,HT[HN,table] = table(100,37,1,Vec,Ang,vec(1.5,1.5,60),"models/sprops/misc/cones/size_1/cone_12x12.mdl",BMat1,BCol1)
}

selfDestruct()
