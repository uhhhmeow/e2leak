@name holograms/gundam/masamune/data
@persist [HN CN] [HT CT]:table


#----------------------
#-- GTable Setup
local ID = "masamune"

XGT = gTable("_holocore", 0)

XGT[ID, table] = table(
    "Clips" = table(),
    "Holos" = table()
)

HT = XGT[ID, table]["Holos", table]
CT = XGT[ID, table]["Clips", table]


#----------------------
#-- Bones
local Skeleton = 0

local Scale = vec(12,12,12)/5
local Color = vec4(255,255,255,Skeleton*255)
local Material = ""
local Model = ""

local Vec = vec()
local Ang = ang()


#----------------------
#-- Hip
HN++,HT[HN,table] = table(0,999,1,vec(0,0,0),ang(0,0,0),Scale,Model,Material,Color)
HN++,HT[HN,table] = table(1,1,1,vec(0,0,0),ang(0,0,0),Scale,Model,Material,Color)


#----------------------
#-- Torso
HN++,HT[HN,table] = table(2,1,1,vec(0,0,10-12),ang(0,0,0),Scale,Model,Material,Color)


#----------------------
#-- Chest
HN++,HT[HN,table] = table(3,2,1,vec(0,0,15),ang(0,0,0),Scale,Model,Material,Color)


#----------------------
#-- Left Arm
HN++,HT[HN,table] = table(4,3,1,vec(-2.5,13,3.5),ang(0,0,10),Scale,Model,Material,Color)
HN++,HT[HN,table] = table(5,4,1,vec(0,0,-11),ang(0,0,-10),Scale,Model,Material,Color)
HN++,HT[HN,table] = table(6,5,1,vec(0,0,-12),ang(0,0,-10),Scale,Model,Material,Color)


#----------------------
#-- Right Arm
HN++,HT[HN,table] = table(7,3,1,vec(-2.5,-13,3.5),ang(0,0,-10),Scale,Model,Material,Color)
HN++,HT[HN,table] = table(8,7,1,vec(0,0,-11),ang(0,0,10),Scale,Model,Material,Color)
HN++,HT[HN,table] = table(9,8,1,vec(0,0,-12),ang(0,0,10),Scale,Model,Material,Color)


#----------------------
#-- Head
HN++,HT[HN,table] = table(10,3,1,vec(0,0,10.5),ang(10,0,0),Scale,Model,Material,Color)


#----------------------
#-- Left Leg
HN++,HT[HN,table] = table(11,1,1,vec(-2,5,8-12),ang(180,0,-6),Scale+1,Model,Material,Color)
HN++,HT[HN,table] = table(12,11,1,vec(0,0,15),Ang,Scale,Model,Material,Color)
HN++,HT[HN,table] = table(13,12,1,vec(0,0,14),ang(180,0,-6),Scale+1,Model,Material,Color)


#----------------------
#-- Right Leg
HN++,HT[HN,table] = table(14,1,1,vec(-2,-5,8-12),ang(180,0,6),Scale+1,Model,Material,Color)
HN++,HT[HN,table] = table(15,14,1,vec(0,0,15),Ang,Scale,Model,Material,Color)
HN++,HT[HN,table] = table(16,15,1,vec(0,0,14),ang(180,0,6),Scale+1,Model,Material,Color)


#----------------------
#-- Skeleton
if (Skeleton) {
    
    
    
}

#----------------------
#-- Visuals
else {
    
local Mat1 = "models/props_debris/plasterwall040c"
local Mat2 = "models/props_debris/plasterwall040c"

local Hue = 220

local White = vec4(255,255,255,255)
local Black = vec4(50,50,50,255) #vec4(hsv2rgb(Hue,0.15,0.2),255)
local Gray = vec4(75,75,75,255) #vec4(hsv2rgb(Hue,0.3,0.3),255)
local Highlight = vec4(100,100,100,255) #vec4(hsv2rgb(Hue,0.4,0.4),255)

local Blade = vec4(255,255,255,255)


#----------------------
#-- Torso
HN++,HT[HN,table] = table(200,2,0,vec(0,0,3),ang(0,0,90),vec(1,1,0.75),"rcylinder_thick",Mat1,Black)
HN++,HT[HN,table] = table(201,2,0,vec(0,0,5+1),ang(0,180,0),vec(0.25,0.25,0.125),"models/props_interiors/Furniture_Couch02a.mdl",Mat1,Gray)
HN++,HT[HN,table] = table(202,2,0,vec(0,0,7.5+1),ang(0,180,0),vec(0.25,0.25,0.15),"models/props_interiors/Furniture_Couch02a.mdl",Mat1,Gray)


#----------------------
#-- Chest
HN++,HT[HN,table] = table(300,3,0,vec(-2,0,0),ang(0,0,90),vec(1.25,1.25,0.75),"rcylinder_thick",Mat1,Black)
HN++,HT[HN,table] = table(301,3,0,vec(-2,0,0),ang(90,0,90),vec(1.5,1,0.6),"hexagon",Mat1,Black)
HN++,HT[HN,table] = table(302,3,0,vec(3.5,0,1),ang(45,0,180),vec(1,1,1.1),"models/Items/combine_rifle_cartridge01.mdl",Mat1,Gray)
HN++,HT[HN,table] = table(303,3,0,vec(3.5,0,1),ang(45,0,180),vec(0.85,2.5,0.85),"models/Items/combine_rifle_cartridge01.mdl",Mat2,Highlight)
HN++,HT[HN,table] = table(304,3,0,vec(4.5,0,1),ang(45,0,180),vec(0.6,4,0.6),"models/Items/combine_rifle_cartridge01.mdl",Mat1,Black)
HN++,HT[HN,table] = table(305,3,0,vec(-1,8,3),ang(55,0,180),vec(1,2,1),"models/Items/combine_rifle_cartridge01.mdl",Mat2,Highlight)
HN++,HT[HN,table] = table(306,3,0,vec(-1,-8,3),ang(55,0,180),vec(1,2,1),"models/Items/combine_rifle_cartridge01.mdl",Mat2,Highlight)
HN++,HT[HN,table] = table(307,3,0,vec(-5,0,3),ang(45,180,180),vec(0.9,10,0.9),"models/Items/combine_rifle_cartridge01.mdl",Mat1,Black)
HN++,HT[HN,table] = table(308,3,0,vec(-2.5,0,2),ang(0,0,90),vec(0.65,0.65,1.65),"rcylinder_thick",Mat1,Gray)
HN++,HT[HN,table] = table(309,3,0,vec(-7,0,1),ang(135,0,0),vec(1,3,1.75),"models/Items/combine_rifle_cartridge01.mdl",Mat2,Highlight)


#----------------------
#-- Left Arm
HN++,HT[HN,table] = table(401,4,0,vec(0,0,0),ang(90,0,90),vec(0.3,0.3,0.3),"models/Combine_Helicopter/helicopter_bomb01.mdl",Mat1,Gray)
HN++,HT[HN,table] = table(402,4,0,vec(0,0,0),ang(90,0,90),vec(0.31,0.31,0.31),"models/Combine_Helicopter/helicopter_bomb01.mdl",Mat2,Highlight)
HN++,HT[HN,table] = table(403,4,0,vec(0,0,-7),ang(0,0,0),vec(0.2,0.2,0.2),"models/props_junk/TrashBin01a.mdl",Mat1,Black)
HN++,HT[HN,table] = table(404,4,0,vec(0,-2,-5),ang(90,90,0),vec(1.5,0.8,0.5),"models/props_wasteland/panel_leverHandle001a.mdl",Mat2,Highlight)

HN++,HT[HN,table] = table(405,5,0,vec(0,0,-5),ang(0,90,0),vec(0.15,0.15,0.3),"models/props_junk/TrashBin01a.mdl",Mat1,Black)
HN++,HT[HN,table] = table(406,5,0,vec(0,1,-7),ang(0,90,0),vec(1,0.5,0.75),"models/props_wasteland/panel_leverHandle001a.mdl",Mat2,Highlight)
HN++,HT[HN,table] = table(407,5,0,vec(0,2.5,-4),ang(115,90,0),vec(2.25,1.75,1.75),"models/weapons/ar2_grenade.mdl",Mat1,Gray)

CN++,CT[CN,table] = table(402,1,vec(3,0,0),vec(-1,0,-0))
CN++,CT[CN,table] = table(402,2,vec(-3,0,0),vec(1,0,-0))
CN++,CT[CN,table] = table(402,3,vec(0,0,-2),vec(0,0,1))

HN++,HT[HN,table] = table(412,6,0,vec(4,-0.5,0),ang(200,90,0),vec(0.3),"models/weapons/c_models/c_fists_of_steel/c_fists_of_steel.mdl",Mat2,Highlight)

CN++,CT[CN,table] = table(412,1,vec(0,0,0),vec(0,1,0))


#----------------------
#-- Left Sword
#[HN++,HT[HN,table] = table(408,6,0,vec(-3,0,19),ang(0,0,0),vec(sqrt(2),0.15,4),"models/sprops/misc/alphanum/alphanum_rpar.mdl",Mat1,Black)
HN++,HT[HN,table] = table(409,408,0,vec(-1,0,0),ang(0,0,0),vec(sqrt(2),0.075,3.99),"models/sprops/misc/alphanum/alphanum_rpar.mdl",Mat1,Blade)
HN++,HT[HN,table] = table(410,408,0,vec(1,0,-20),ang(80,0,0),vec(0.125,0.125,0.6),"rcylinder",Mat1,Gray)
HN++,HT[HN,table] = table(411,410,0,vec(0,0,2),ang(0,0,0),vec(0.15,0.15,0.4),"rcylinder_thin",Mat1,Black)
]#

#----------------------
#-- Right Arm
HN++,HT[HN,table] = table(501,7,0,vec(0,0,0),ang(90,0,90),vec(0.3,0.3,0.3),"models/Combine_Helicopter/helicopter_bomb01.mdl",Mat1,Gray)
HN++,HT[HN,table] = table(502,7,0,vec(0,0,0),ang(90,0,90),vec(0.31,0.31,0.31),"models/Combine_Helicopter/helicopter_bomb01.mdl",Mat2,Highlight)
HN++,HT[HN,table] = table(503,7,0,vec(0,0,-7),ang(0,0,0),vec(0.2,0.2,0.2),"models/props_junk/TrashBin01a.mdl",Mat1,Black)
HN++,HT[HN,table] = table(504,7,0,vec(0,2,-5),ang(90,-90,0),vec(1.5,0.8,0.5),"models/props_wasteland/panel_leverHandle001a.mdl",Mat2,Highlight)

HN++,HT[HN,table] = table(505,8,0,vec(0,0,-5),ang(0,-90,0),vec(0.15,0.15,0.3),"models/props_junk/TrashBin01a.mdl",Mat1,Black)
HN++,HT[HN,table] = table(506,8,0,vec(0,-1,-7),ang(0,-90,0),vec(1,0.5,0.75),"models/props_wasteland/panel_leverHandle001a.mdl",Mat2,Highlight)
HN++,HT[HN,table] = table(507,8,0,vec(0,-2.5,-4),ang(115,-90,0),vec(2.25,1.75,1.75),"models/weapons/ar2_grenade.mdl",Mat1,Gray)

CN++,CT[CN,table] = table(502,1,vec(3,0,0),vec(-1,0,-0))
CN++,CT[CN,table] = table(502,2,vec(-3,0,0),vec(1,0,-0))
CN++,CT[CN,table] = table(502,3,vec(0,0,2),vec(0,0,-1))

HN++,HT[HN,table] = table(512,9,0,vec(4,0.5,0),ang(200,-90,0),vec(0.3),"models/weapons/c_models/c_fists_of_steel/c_fists_of_steel.mdl",Mat2,Highlight)

CN++,CT[CN,table] = table(512,1,vec(0,0,0),vec(0,-1,0))


#----------------------
#-- Right Sword
#[HN++,HT[HN,table] = table(508,9,0,vec(-3,0,19),ang(0,0,0),vec(sqrt(2),0.15,4),"models/sprops/misc/alphanum/alphanum_rpar.mdl",Mat1,Black)
HN++,HT[HN,table] = table(509,508,0,vec(-1,0,0),ang(0,0,0),vec(sqrt(2),0.075,3.99),"models/sprops/misc/alphanum/alphanum_rpar.mdl",Mat1,Blade)
HN++,HT[HN,table] = table(510,508,0,vec(1,0,-20),ang(80,0,0),vec(0.125,0.125,0.6),"rcylinder",Mat1,Gray)
HN++,HT[HN,table] = table(511,510,0,vec(0,0,2),ang(0,0,0),vec(0.15,0.15,0.4),"rcylinder_thin",Mat1,Black)
]#

#----------------------
#-- Head
HN++,HT[HN,table] = table(600,10,0,vec(0,0,0.35),ang(0,0,0),vec(1,1,0.35),"models/sprops/misc/cones/size_2/cone_24x12.mdl",Mat1,Black)
HN++,HT[HN,table] = table(601,10,0,vec(0,0,0),ang(40,0,0),vec(1,0.8,1),"models/Gibs/HGIBS.mdl",Mat1,Gray)
HN++,HT[HN,table] = table(602,10,0,vec(-1,0,0),ang(40,0,0),vec(1,1.05,1),"models/Gibs/HGIBS.mdl",Mat2,Black)


#----------------------
#-- Jetpack
HN++,HT[HN,table] = table(700,3,0,vec(-12-0,0,5),ang(0,0,90),vec(0.25,0.25,1),"rcylinder",Mat1,Black)

HN++,HT[HN,table] = table(701,700,0,vec(0.65,-6,-4),ang(0,0,0),vec(0.125,0.25,0.25),"models/mechanics/solid_steel/box_beam_4.mdl",Mat2,Highlight)
HN++,HT[HN,table] = table(702,700,0,vec(-0.65,-6*0.75,-4),ang(0,0,0),vec(0.125,0.25*0.75,0.25),"models/mechanics/solid_steel/box_beam_4.mdl",Mat2,Highlight)
HN++,HT[HN,table] = table(703,700,0,vec(0.65,-6,4),ang(0,0,0),vec(0.125,0.25,0.25),"models/mechanics/solid_steel/box_beam_4.mdl",Mat2,Highlight)
HN++,HT[HN,table] = table(704,700,0,vec(-0.65,-6*0.75,4),ang(0,0,0),vec(0.125,0.25*0.75,0.25),"models/mechanics/solid_steel/box_beam_4.mdl",Mat2,Highlight)


#----------------------
#-- Sashimono
HN++,HT[HN,table] = table(725,3,0,vec(-12.25,0,0),ang(-17,0,0),vec(),"",Mat1,Black)
HN++,HT[HN,table] = table(726,725,0,vec(0,0,16),ang(0,0,0),vec(0.075,0.075,3),"cylinder",Mat1,Black)
HN++,HT[HN,table] = table(727,726,0,vec(-6,0,22-16),ang(0,0,0),vec(1,0.025,2),"",Mat1,vec4(255,255,255,255))
HN++,HT[HN,table] = table(728,726,0,vec(-6-0.5,0,22+6-16),ang(0,0,90),vec(0.66,0.66,0.035),"cylinder",Mat1,vec4(200,0,0,255))
HN++,HT[HN,table] = table(729,726,0,vec(-0.5,0,31-16),ang(0,0,0),vec(0.5,0.25,1),"models/sprops/misc/alphanum/alphanum_colon.mdl",Mat1,Black)
HN++,HT[HN,table] = table(730,726,0,vec(-0.5,0,27.75-16),ang(0,0,0),vec(0.5,0.25,1),"models/sprops/misc/alphanum/alphanum_colon.mdl",Mat1,Black)
HN++,HT[HN,table] = table(731,726,0,vec(-0.5,0,17.75-16),ang(0,0,0),vec(0.5,0.25,1),"models/sprops/misc/alphanum/alphanum_colon.mdl",Mat1,Black)
HN++,HT[HN,table] = table(732,726,0,vec(-0.5,0,16.3-16),ang(0,0,0),vec(0.075,0.06,0.2),"",Mat1,Black)


#----------------------
#-- Hip
HN++,HT[HN,table] = table(800,1,0,vec(0,0,8-10),ang(110,0,180),vec(1.5,2,1),"models/Items/combine_rifle_cartridge01.mdl",Mat2,Highlight)
HN++,HT[HN,table] = table(801,1,0,vec(-2,0,10-10),ang(110,180,180),vec(1.5,1,1),"models/Items/combine_rifle_cartridge01.mdl",Mat1,Black)


#----------------------
#-- Left Leg
HN++,HT[HN,table] = table(900,11,0,vec(0,0,0),ang(90,0,90),vec(0.25,0.25,0.25),"models/Combine_Helicopter/helicopter_bomb01.mdl",Mat1,Black)
HN++,HT[HN,table] = table(901,11,0,vec(0,0,0),ang(35,0,0),vec(1,2,1),"models/Items/combine_rifle_cartridge01.mdl",Mat2,Highlight)
HN++,HT[HN,table] = table(902,11,0,vec(0,0,8),ang(180,0,0),vec(0.35,0.3,0.4),"models/props_junk/TrashBin01a.mdl",Mat1,Black)
HN++,HT[HN,table] = table(904,11,0,vec(-3,0,3),ang(10,0,0),vec(0.2,0.175,0.15),"models/props_interiors/Furniture_Couch02a.mdl",Mat2,Highlight)
HN++,HT[HN,table] = table(905,11,0,vec(-2.5,0,7),ang(10,0,0),vec(0.2-0.0125,0.175-0.0125,0.15),"models/props_interiors/Furniture_Couch02a.mdl",Mat2,Highlight)
HN++,HT[HN,table] = table(906,11,0,vec(-2,0,11),ang(10,0,0),vec(0.2-0.0125*2,0.175-0.0125*2,0.15),"models/props_interiors/Furniture_Couch02a.mdl",Mat2,Highlight)

HN++,HT[HN,table] = table(907,12,0,vec(0,0,8),ang(180,0,0),vec(0.275,0.225,0.35),"models/props_junk/TrashBin01a.mdl",Mat1,Black)
HN++,HT[HN,table] = table(908,12,0,vec(0,0,12.5),ang(0,180,180),vec(1.5,0.75,1.25),"models/props_wasteland/panel_leverHandle001a.mdl",Mat2,Highlight)
HN++,HT[HN,table] = table(909,12,0,vec(0,0,7),ang(0,0,0),vec(0.55,0.25,1),"",Mat2,Highlight)

HN++,HT[HN,table] = table(910,13,0,vec(2,0,-3),ang(0,0,0),vec(0.165,0.075,0.125),"models/XQM/coastertrain1seat.mdl",Mat2,Highlight)
HN++,HT[HN,table] = table(911,13,0,vec(2,0,-3.5),ang(0,0,0),vec(0.165,0.07,0.125),"models/XQM/coastertrain1seat.mdl",Mat1,Black)


#----------------------
#-- Right Leg
HN++,HT[HN,table] = table(1000,14,0,vec(0,0,0),ang(90,0,90),vec(0.25,0.25,0.25),"models/Combine_Helicopter/helicopter_bomb01.mdl",Mat1,Black)
HN++,HT[HN,table] = table(1001,14,0,vec(0,0,0),ang(35,0,0),vec(1,2,1),"models/Items/combine_rifle_cartridge01.mdl",Mat2,Highlight)
HN++,HT[HN,table] = table(1002,14,0,vec(0,0,8),ang(180,0,0),vec(0.35,0.3,0.4),"models/props_junk/TrashBin01a.mdl",Mat1,Black)
HN++,HT[HN,table] = table(1004,14,0,vec(-3,0,3),ang(10,0,0),vec(0.2,0.175,0.15),"models/props_interiors/Furniture_Couch02a.mdl",Mat2,Highlight)
HN++,HT[HN,table] = table(1005,14,0,vec(-2.5,0,7),ang(10,0,0),vec(0.2-0.0125,0.175-0.0125,0.15),"models/props_interiors/Furniture_Couch02a.mdl",Mat2,Highlight)
HN++,HT[HN,table] = table(1006,14,0,vec(-2,0,11),ang(10,0,0),vec(0.2-0.0125*2,0.175-0.0125*2,0.15),"models/props_interiors/Furniture_Couch02a.mdl",Mat2,Highlight)

HN++,HT[HN,table] = table(1007,15,0,vec(0,0,8),ang(180,0,0),vec(0.275,0.225,0.35),"models/props_junk/TrashBin01a.mdl",Mat1,Black)
HN++,HT[HN,table] = table(1008,15,0,vec(0,0,12.5),ang(0,180,180),vec(1.5,0.75,1.25),"models/props_wasteland/panel_leverHandle001a.mdl",Mat2,Highlight)
HN++,HT[HN,table] = table(1009,15,0,vec(0,0,7),ang(0,0,0),vec(0.55,0.25,1),"",Mat2,Highlight)

HN++,HT[HN,table] = table(1010,16,0,vec(2,0,-3),ang(0,0,0),vec(0.165,0.075,0.125),"models/XQM/coastertrain1seat.mdl",Mat2,Highlight)
HN++,HT[HN,table] = table(1011,16,0,vec(2,0,-3.5),ang(0,0,0),vec(0.165,0.07,0.125),"models/XQM/coastertrain1seat.mdl",Mat1,Black)


#----------------------
#-- Daisho
HN++,HT[HN,table] = table(1100,1,1,vec(4,9.5,-3),ang(0,0,6),Scale,Model,Material,Color)
HN++,HT[HN,table] = table(1101,1100,0,vec(-2.5,-0.5,0.6),ang(0,0,-90),vec(0.15,0.5,0.15),"models/props_lab/monitor01a.mdl",Mat1,Black)
HN++,HT[HN,table] = table(1102,1100,0,vec(3,0,3),ang(0,-90,90.15),vec(1.5,1.05,1.0125),"models/weapons/c_models/c_shogun_katana/c_shogun_katana.mdl",Mat1,Black)
HN++,HT[HN,table] = table(1103,1100,0,vec(2,0,-2.5),ang(0,-90,90.15),vec(1.5,1.05,1.0125)*0.75,"models/weapons/c_models/c_shogun_katana/c_shogun_katana.mdl",Mat1,Black)

HN++,HT[HN,table] = table(1104,1100,0,vec(3,0,3),ang(0,-90,90),vec(1),"models/weapons/c_models/c_shogun_katana/c_shogun_katana.mdl","",vec4(255,255,255,255)) #- katana
HN++,HT[HN,table] = table(1105,1104,0,vec(),ang(),vec(1),"models/weapons/c_models/c_shogun_katana/c_shogun_katana.mdl",Mat1,Black)
HN++,HT[HN,table] = table(1106,1100,0,vec(2,0,-2.5),ang(0,-90,90),vec(0.75),"models/weapons/c_models/c_shogun_katana/c_shogun_katana.mdl","",vec4(255,255,255,255)) #- wakizashi
HN++,HT[HN,table] = table(1107,1106,0,vec(),ang(),vec(0.75),"models/weapons/c_models/c_shogun_katana/c_shogun_katana.mdl",Mat1,Black)

CN++,CT[CN,table] = table(1102,1,vec(0,0,5),vec(0,0,1))
CN++,CT[CN,table] = table(1103,1,vec(0,0,5),vec(0,0,1))
CN++,CT[CN,table] = table(1104,1,vec(0,0,3.75),vec(0,0,1))
CN++,CT[CN,table] = table(1105,1,vec(0,0,3.75),vec(0,0,-1))
CN++,CT[CN,table] = table(1106,1,vec(0,0,3.75)*0.75,vec(0,0,1))
CN++,CT[CN,table] = table(1107,1,vec(0,0,3.75)*0.75,vec(0,0,-1))


}

selfDestruct()
