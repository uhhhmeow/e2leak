@name games/chess/chess/game
@inputs 
@outputs
@trigger none
@model models/hunter/plates/plate2x2.mdl

if (first()) {

    #include "games/chess/chess/lib"

    Chess = game_from_fen(FEN_NewGame)

}

elseif (UI_LoadStatus == "create_game") {

    create_game()

}

elseif (UI_LoadStatus == "play_game") {

    play_game()

}
