@name keyf_test_7
@inputs 
@outputs Output Ease AnimStep
@persist DeltaTime CurTime
@persist Frames:array [NumKeyFrames NumTotalFrames NumTweenFrames] [AnimDuration AnimFPS AnimStep] [AnimNextKey AnimPrevKey AnimLastKey] [AnimFrame AnimFrac]
@trigger none


if (first()) {

    function dt( Interval ) {
        interval(Interval)

        DeltaTime = min(curtime() - CurTime,  Interval/1000)
        CurTime   = curtime()
    }
    
    Frames = array(
        0 = 0,
        1 = 90,
        2 = -90,
        3 = 0
    )
    
    AnimDuration = 1
    AnimFPS      = 60 #NOT WORKING PROPERLY
    
    NumKeyFrames   = Frames:count()
    NumTotalFrames = AnimFPS*AnimDuration
    NumTweenFrames = NumTotalFrames/NumKeyFrames

    holoCreate(1)
    holoModel(1, "models/sprops/misc/origin.mdl")    
    
    function number linear( Y0, Y1, T ) {
        return Y0 + T * (Y1 - Y0)
    }

    function number cosine( Y0, Y1, T ) {
        return linear( Y0, Y1, -cosr( pi() * T ) / 2 + 0.5 )
    }
    
    function number accelerate( Y0, Y1, T ) {
        return linear( Y0, Y1, T^2 )
    }
    
    function number decelerate( Y0, Y1, T ) {
        return linear( Y0, Y1, 1 - ( 1 - T )^2 )
    }
    
}

dt(60)

AnimStep = min(AnimStep + (AnimFPS*AnimDuration)*DeltaTime, NumTotalFrames)

AnimLastKey = AnimNextKey
AnimNextKey = floor(AnimStep/NumTweenFrames) % NumTweenFrames + 1
AnimPrevKey = AnimNextKey - 1

if (AnimLastKey != AnimNextKey) {
    AnimFrame = 0
}

AnimFrame = min(AnimFrame + NumTweenFrames*DeltaTime, NumTweenFrames) #HOW TO INCREMENT PROPERLY?
AnimFrac  = AnimFrame/NumTweenFrames

Output = (Frames[AnimNextKey, number] - Frames[AnimPrevKey, number])*AnimFrac + Frames[AnimPrevKey, number]

holoAng(1, ang(Output, 0, 0))

