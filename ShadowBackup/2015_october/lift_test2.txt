@name lift_test2
@inputs [LW RW LE RE]:entity
@outputs 
@persist Surfaces:table  Base:entity
@persist [CONSTANT_CONVERSION_MS CONSTANT_AIR_DENSITY]
@trigger none



if ( first() ) {
    CONSTANT_CONVERSION_MS = 0.0254
    CONSTANT_AIR_DENSITY = 1.255
    
    function createSurface( Entity:entity, Mass, Coeff ) {
        Entity:setMass(Mass)
        local Data = table(
            "LCoeff" = Coeff,
            "WArea"  = convertUnit( "in", "m", Entity:boxSize():length() ),
            "WMass"  = Entity:mass(),
            "WEnt"   = Entity
        )
        Surfaces[Surfaces:count() + 1 + "", table] = Data
    }
    
    function table:simulate() {
        local Entity = This["WEnt", entity]

        local Velocity     = Entity:vel()
        local RelativeWind = -Velocity:normalized()
        local AirSpeed     = Velocity:dot(Entity:right()) * CONSTANT_CONVERSION_MS

        local LiftForce = This["LCoeff", number]*Entity:up():dot(RelativeWind) * This["WArea", number] * ((CONSTANT_AIR_DENSITY * AirSpeed^2) / 2)

        Entity:applyForce( RelativeWind:toAngle():up()*LiftForce*This["WMass", number] )
    }
    
    entity():isWeldedTo():setMass(1000)
    Base = entity():isWeldedTo()
    
    createSurface( LW, 500, 0.04 )
    createSurface( RW, 500, 0.04 )
    createSurface( LE, 450, 0.4 )
    createSurface( RE, 450, 0.4 )
    
    runOnTick(1)
}

foreach (K, Surface:table = Surfaces) { Surface:simulate() }


Base:applyOffsetForce( -Base:forward() * Base:mass() * 20, Base:toWorld(vec(-119.977, -0.023, -1.463)) )

#[Velocity = Entity:vel()
Direction = Velocity:normalized()
RelativeWind = Direction * -1
Perpendicular = RelativeWind:toAngle():up()
AngleOfAttack = Entity:up():dot(RelativeWind)


holoAng(1, Perpendicular:toAngle() + ang(90,0,0))
holoAng(2, RelativeWind:toAngle() + ang(90,0,0))
holoAng(3, Entity:toWorld( ang(-acos(AngleOfAttack), -90, 0) ) )

CL = 0.04*AngleOfAttack
Area = convertUnit( "in", "m", entity():boxSize():length() ) #convertUnit( "in", "m", 144 ) * convertUnit( "in", "m", 48 )
ASpd = Velocity:dot(Entity:right()) *0.0254 #Velocity:length() * 0.0254

Lift = CL * Area * ((1.225 * ASpd^2) / 2)

Entity:applyForce( Perpendicular * Lift * Entity:mass() ) #* Entity:mass() )]#
