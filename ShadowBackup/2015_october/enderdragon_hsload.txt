@name HSYS/FIGURES/MINECRAFT/ENDERDRAGON_HSLOAD
@inputs 
@outputs PLAYER:entity STRENGTH
@persist [NV1 NV2 NV3 NV4]:vector GRD CEIL TDELAY TRATE TARGET:vector TT [SHOULDER HEAD TAIL]:entity SPEED HANG:angle
#@model models/sprops/cuboids/height36/size_2/cube_48x120x36.mdl

if (first())    {
    #-Initialize
    #include "HSYS/FIGURES/MINECRAFT/ENDERDRAGON_HSINJ"

    DELAY = TABLE:count()
    interval(DELAY), print("Spawning " + DELAY + " holograms at " + min(round(1000/DELAY),DELAY) + " holograms per second.")
    runOnLast(1)

    #-Hologram Setup
    FACTOR = 0.5
    SHADING = 1
    STATUS = "spawning"
    MODE = "load"
    
    OWNER = owner()
    ENTITY = entity()#, ENTITY:setAlpha(0)
    
    rangerFilter(players())
    rangerPersist(1)
} elseif (MODE == "load") {
    if (STATUS == "spawning")   {
        interval(DELAY)
        if (HINDEX < TABLE:count() & holoCanCreate()) {
            HINDEX++
    
            #PAR = holoEntity(TABLE[HINDEX, array][1, number]) ?: entity(TABLE[HINDEX, array][1, number])
            PAR = TABLE[HINDEX, array][1, number] == SEED ? ENTITY : holoEntity(TABLE[HINDEX, array][1, number])
    
            holoCreate(HINDEX)
            holoParent(HINDEX, PAR)
            holoPos(HINDEX, PAR:toWorld(FACTOR * TABLE[HINDEX, array][3, vector]))
            holoAng(HINDEX, PAR:toWorld(TABLE[HINDEX, array][4, angle]))
            holoModel(HINDEX, TABLE[HINDEX, array][6, string]:lower())
            holoMaterial(HINDEX, TABLE[HINDEX, array][7, string])
            holoColor(HINDEX, TABLE[HINDEX, array][8, vector])
    
            if (TABLE[HINDEX, array][2, number] == 0)    { holoScale(HINDEX, FACTOR * TABLE[HINDEX, array][5, vector]) }
            if (TABLE[HINDEX, array][2, number] == 1)    { holoScaleUnits(HINDEX, FACTOR * TABLE[HINDEX, array][5, vector]) }
            if (SHADING == 1) { holoDisableShading(HINDEX, 1) }
    
            setName("HoloLoader -- Spawning " + toString((HINDEX / TABLE:count()) * 100) + "% complete.")
            if (HINDEX == TABLE:count())   { 
                STATUS = CLIP:count() > 0 ? "clipping" : "finished"
                print("Spawning of " + TABLE:count() + " holograms finished.") 
            }
        }
    } elseif (STATUS == "clipping") {
        interval(DELAY)
        if (CINDEX < CLIP:count()) {
            CINDEX++
            
            CLIPHOL = CLIP[CINDEX, array][1, number]
            CLIPNUM = CLIP[CINDEX, array][2, number]
            CLIPPOS = CLIP[CINDEX, array][3, vector]
            CLIPDIR = CLIP[CINDEX, array][4, vector]
    
            holoClipEnabled(CLIPHOL, CLIPNUM, 1)
            holoClip(CLIPHOL, CLIPNUM, CLIPPOS*FACTOR, CLIPDIR, 0)
            
            if (CINDEX == CLIP:count())   { STATUS = "finished", print(CLIP:count() + " holograms clipped.") }
        }
    } elseif (STATUS == "finished")   {
        MODE = "run", print("Script is now live!"), interval(90)
        
        holoCreate(500)
        holoScaleUnits(500, vec(500))
        holoModel(500, "hq_sphere")
        #holoAlpha(500, 150)
        holoColor(500, vec(0,255,0))

        NV1 = vec(-28.5,0,0)*FACTOR
        NV2 = vec(-23,0,0)*FACTOR
        NV3 = vec(-36,0,0)*FACTOR
        NV4 = vec(-47,0,0)*FACTOR

        function rv(Index, Offset:vector)  {
            holoPos(Index, holoEntity(Index-1):toWorld(Offset))
            holoAng(Index, (holoEntity(Index-1):pos() - holoEntity(Index+1):pos()):toAngle():setRoll(HANG:roll()))
        }
        
        rangerHitEntities(0)
        GRD = rangerOffset(32768, vec(0,0,2000), vec(0,0,-1)):position():z() + 1200*FACTOR
        CEIL = rangerOffset(32768,vec(0,0,2000), vec(0,0,1)):position():z() - 1200*FACTOR
        
        TARGET = randvec(-32768,32768):setZ(random(GRD,CEIL))  
        TDELAY = random(50,500)
        TRATE = TDELAY*0.1
        
        SPEED = 60 * FACTOR
        SHOULDER = holoEntity(7)
        HEAD = holoEntity(1)
        TAIL = holoEntity(20)
        
        foreach (K, V:entity = players()) {
            if (V != owner()) {
                holoVisible(500, V, 0)
            }
        }

        particleGravity(vec())
        for (I = 1, 21)   { holoUnparent(I) }
    }
} elseif (MODE == "run")    {
    if (clk())    {
        interval(90)

        TIME = curtime()*250

        HPOS = HEAD:pos()
        HANG = HEAD:angles()
        YAW = ang(0, 0, clamp($HANG:yaw()*-20, -40, 40))

        #-Head  
        holoAng(1, slerp(quat(HEAD), quat((TARGET-HPOS):toAngle() + YAW) , 0.1):toAngle())
        holoPos(1, HPOS + HEAD:forward()*SPEED)
        #holoPos(1, ENTITY:pos())
        #holoAng(1, ENTITY:angles())
        
        #-Wings
        WSIN = sin(TIME)*45
        WCOS = ang(0, 0, cos(TIME)*65)

        holoAng(61, HEAD:toWorld(ang(max(0, sin(TIME/2)*35), 0, 0)))
        holoAng(65, SHOULDER:toWorld(ang(0, 0, WSIN+10)))
        holoAng(80, SHOULDER:toWorld(ang(180, 180, -WSIN-10)))
        holoAng(66, holoEntity(65):toWorld(-WCOS))
        holoAng(81, holoEntity(80):toWorld(WCOS))
        
        #-Spine
        rv(2, NV1)
        rv(3, NV2), rv(4, NV2), rv(5, NV2), rv(6, NV2)
        rv(7, NV3), rv(8, NV4), rv(9, NV4), rv(10, NV3)
        rv(11, NV2), rv(12, NV2), rv(13, NV2), rv(14, NV2), rv(15, NV2)
        rv(16, NV2), rv(17, NV2), rv(18, NV2), rv(19, NV2), rv(20, NV2)
        
        holoPos(21, TAIL:toWorld(NV2))
        holoAng(21, (TAIL:pos() - holoEntity(21):toWorld(NV2)):toAngle():setRoll(HANG:roll()))
    
#[        if (findCanQuery())   {
            findInCone(HPOS, HEAD:forward() , 32768, 30)
            findClipToClass("player")
            findSortByDistance(HPOS)
            
            PLAYER = find()
            
            holoPos(500, PLAYER:pos())
        }]#

        if (TT < TDELAY)   {
            TT += TRATE
            if (TT >= TDELAY) {
                TARGET = randvec(-32768,32768):setZ(random(GRD,CEIL))
            
#[                if (PLAYER) { 
                    TARGET = PLAYER:pos() + vec(0,0,50)
                    STRENGTH = 0.75
                } else  {
                    TARGET = randvec(-32768,32768):setZ(random(GRD,CEIL))
                    STRENGTH = 0.1
                }]#

                TDELAY = random(250,1000)
                TRATE = TDELAY*0.05
                TT = 0
            }
        }

        if (particleCanCreate())    {
            PCOLOR = randint(1,2) == 2 ? PURPLE_A : PURPLE_B
            PSIZE = (random(10,30) + sin(TIME)*5)*FACTOR
            particle(random(0.5,1.5), PSIZE, PSIZE*3, PSPRITE, PCOLOR, SHOULDER:pos() + randvec(-200,200)*FACTOR, randvec(-65,65)*FACTOR)
        }
    }
}
