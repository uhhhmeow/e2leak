@name step_test_3
@inputs 
@outputs 
@persist [CurTime DeltaTime] Feet:table Entity:entity MoveDir:vector
@trigger none



if (first()) {
    
    
    function dt(Interval) {
        interval(Interval)
        
        DeltaTime = curtime() - CurTime
        CurTime   = curtime() 
    }    

    function addFoot(ID:string,LPos:vector,StepDelay,StepDist) {
        local Hash = hash(ID)
        local WPos = Entity:toWorld(LPos)

        Feet[ID, table] = table(
            "lpos"  = LPos,
            "delay" = StepDelay,
            "dist"  = StepDist,
            
            "prev" = WPos,
            "next" = WPos,
            "midl" = WPos,
            "step" = WPos,
            
            "frac"  = 0,
            "stage" = 0,
            "hash"  = Hash
        )
        
        holoCreate(Hash, WPos, vec(3/12))
    }
    
    function table:updateFoot() {
        switch (This["stage", number]) {
            
            case 0, 
                
                local StepDir = MoveDir:normalized()
                local StepVec = StepDir*This["dist", number]
                
                This["prev", vector] = This["step", vector]
                This["next", vector] = This["step", vector] + StepVec
                This["midl", vector] = mix(This["prev", vector], This["next", vector], 0.5) + vec(0, 0, This["prev", vector]:distance(This["next", vector])/2)
                
                This["rate", number]  = This["dist", number]
                This["stage", number] = 1
            
            break
            
            
            case 1, 
            
                This["frac", number] = min(This["frac", number] + This["rate", number]*DeltaTime, This["dist", number])
                This["step", vector] = bezier(This["prev", vector], This["midl", vector], This["next", vector], This["frac", number]/This["dist", number])
                
                holoPos(This["hash", number], This["step", vector])
                
                if (This["frac", number] == This["dist", number]) {
                    This["frac", number]  = CurTime
                    This["stage", number] = 2    
                }
            
            break
            
            
            case 2, 
            
                if (min(CurTime - This["frac", number], This["delay", number]) == This["delay", number]) {
                    This["frac", number]  = 0
                    This["stage", number] = 0
                }
            
            break   
             
        }

    }

    Entity = holoCreate(1)
    
    addFoot("1", vec(0,30,0), 1, 50)
    addFoot("2", vec(0,-30,0), 1, 50)
       
}

dt(60)

MoveDir = vec(1,0,0)

Feet["1", table]:updateFoot()
Feet["2", table]:updateFoot()
