@name 2015_october/teddiursa_data

#- function holo(Num,Parent,ScaleType,Pos:vector,Ang:angle,Scale:vector,[Model Mat]:string,Color:vector4)    { }
#- function clip(Num,ClipNum,[Pos Dir]:vector)   { }

#include "2015_october/hc_lib"

local CellShading = 1

local BodyM1 = "sprops/textures/sprops_metal5"
local BodyC1 = vec4(214, 129, 45, 255)
local BodyC2 = vec4(255, 245, 245, 255)
local BodyC3 = vec4(221, 225, 168, 255)
local BodyC4 = vec4(50, 50, 50, 255)

#-Bones
local ShowBones = 0

holo(1, 0, 1, vec(0,0,8), ang(), vec(2,2,2), "models/sprops/misc/origin.mdl", "", vec4(255,255,255,255*ShowBones))      #-Chest
holo(2, 1, 1, vec(-4,0,-4.25), ang(), vec(2,2,2), "models/sprops/misc/origin.mdl", "", vec4(255,255,255,255*ShowBones))  #-Tail
holo(3, 1, 1, vec(-0.5,0,2.5), ang(), vec(2,2,2), "models/sprops/misc/origin.mdl", "", vec4(255,255,255,255*ShowBones))  #-Neck
holo(4, 3, 1, vec(-1,6,9), ang(), vec(2,2,2), "models/sprops/misc/origin.mdl", "", vec4(255,255,255,255*ShowBones))      #-Left Ear
holo(5, 3, 1, vec(-1,-6,9), ang(), vec(2,2,2), "models/sprops/misc/origin.mdl", "", vec4(255,255,255,255*ShowBones))     #-Right Ear
holo(6, 1, 1, vec(0,2.5,1.5), ang(), vec(2,2,2), "models/sprops/misc/origin.mdl", "", vec4(255,255,255,255*ShowBones))     #-Left Arm
holo(7, 1, 1, vec(0,-2.5,1.5), ang(), vec(2,2,2), "models/sprops/misc/origin.mdl", "", vec4(255,255,255,255*ShowBones))    #-Right Arm
holo(8, 1, 1, vec(0,2,-6.25), ang(), vec(2,2,2), "models/sprops/misc/origin.mdl", "", vec4(255,255,255,255*ShowBones))   #-Left Foot
holo(9, 1, 1, vec(0,-2,-6.25), ang(), vec(2,2,2), "models/sprops/misc/origin.mdl", "", vec4(255,255,255,255*ShowBones))  #-Right Foot

#-Head
holo(10, 3, 1, vec(0,0,5.75), ang(), vec(14.25,15,11.5), "hq_sphere", BodyM1, BodyC1)
holo(11, 3, 1, vec(0,0,5.75), ang(), vec(14.25,15,11.5), "hq_sphere", BodyM1, BodyC3)
holo(12, 3, 1, vec(0,0,5.75), ang(), vec(14.25,15,11.5), "hq_sphere", BodyM1, BodyC3)
holo(13, 3, 1, vec(0,0,5.75), ang(), vec(14.25,15,11.5), "hq_sphere", BodyM1, BodyC1)
holo(14, 10, 1, vec(7,0,-0.75), ang(), vec(0.75,2.25,1), "sphere3", BodyM1, BodyC4)  #-Nose

clip(10, 1, vec(5.5,0,2), vec(-1,0,-0.7))
clip(10, 2, vec(6,0,-1.5), vec(-1,0,0.6))
clip(11, 1, vec(6,0,-1.5), vec(1,0,-0.6))
clip(12, 1, vec(5.5,0,2), vec(1,0,0.7))
clip(12, 2, vec(5.65,3,2), vec(-1,-0.15,-0.7))
clip(13, 1, vec(5.65,3,2), vec(1,0.15,0.7))

#-Eyes
holo(16, 10, 1, vec(5.5,2.25,0), ang(-17,22,0), vec(3,3,3), "hq_sphere", BodyM1, BodyC4)   #-Right Eye
holo(17, 16, 1, vec(0,0,0), ang(), vec(3), "hq_sphere", BodyM1, vec4(255,255,255,255))     #-Right Eye
holo(18, 10, 1, vec(5.5,-2.25,0), ang(-17,-22,0), vec(3,3,3), "hq_sphere", BodyM1, BodyC4) #-Left Eye
holo(19, 18, 1, vec(0,0,0), ang(), vec(3), "hq_sphere", BodyM1, vec4(255,255,255,255))     #-Left Eye

clip(16, 1, vec(1.425,0,0), vec(-1,0,0))
clip(17, 1, vec(1.425,0,0), vec(1,0,0))
clip(18, 1, vec(1.425,0,0), vec(-1,0,0))
clip(19, 1, vec(1.425,0,0), vec(1,0,0))

#-Ears
holo(20, 4, 1, vec(0,1,2.75), ang(90,0,0), vec(6,6,10), "torus3", BodyM1, BodyC1)
holo(21, 4, 1, vec(0,1.25,2.5), ang(90,0,0), vec(4.5,4.5,10), "torus3", BodyM1, BodyC1)
holo(22, 4, 1, vec(0,1.5,2.25), ang(90,0,0), vec(3.5,3.5,10), "torus3", BodyM1, BodyC1)
holo(23, 4, 1, vec(-1.2,1,2.75), ang(-90,0,0), vec(6.25,6.25,1.5), "sphere3", BodyM1, BodyC1)
holo(24, 4, 1, vec(0,1.5,2.25), ang(90,0,0), vec(3.5,3.5,10), "cplane", BodyM1, BodyC4)

holo(25, 5, 1, vec(0,-1,2.75), ang(90,0,0), vec(6,6,10), "torus3", BodyM1, BodyC1)
holo(26, 5, 1, vec(0,-1.25,2.5), ang(90,0,0), vec(4.5,4.5,10), "torus3", BodyM1, BodyC1)
holo(27, 5, 1, vec(0,-1.5,2.25), ang(90,0,0), vec(3.5,3.5,10), "torus3", BodyM1, BodyC1)
holo(28, 5, 1, vec(-1.2,-1,2.75), ang(-90,0,0), vec(6.25,6.25,1.5), "sphere3", BodyM1, BodyC1)
holo(29, 5, 1, vec(0,-1.5,2.25), ang(90,0,0), vec(3.5,3.5,10), "cplane", BodyM1, BodyC4)

#-Mouth
holo(30, 10, 1, vec(4.625,0,-3), ang(140,0,0), vec(2,2,2), "hq_tube_thin", BodyM1, BodyC4)

#-Body
holo(31, 1, 1, vec(0,0,-5), ang(), vec(8,10,18), "sphere3", BodyM1, BodyC1)
holo(32, 1, 1, vec(0,0,-3.333), ang(), vec(7.9,9.9,6), "sphere3", BodyM1, BodyC1)
holo(33, 2, 1, vec(-1.75,0,0), ang(), vec(4.5,4.5,4.5), "sphere3", BodyM1, BodyC1)

clip(31, 1, vec(0,0,2), vec(0,0,1))

#-Arms
holo(34, 6, 1, vec(0,-3.5,0), ang(0,0,90), vec(4,2.5,18), "cone", BodyM1, BodyC1)
holo(35, 6, 1, vec(0,5.5,0), ang(0,0,90), vec(4,2.5,2.5), "sphere3", BodyM1, BodyC1)
holo(36, 35, 1, vec(0,0,-1.25), ang(0,0,0), vec(1.25,1,1.5), "icosphere", BodyM1, BodyC2)
holo(37, 35, 1, vec(0,0,-1.25):rotate(-60,0,0), ang(0,-60,0), vec(1.25,1,1.65), "icosphere", BodyM1, BodyC2)
holo(38, 35, 1, vec(0,0,-1.25):rotate(60,0,0), ang(0,60,0), vec(1.25,1,1.65), "icosphere", BodyM1, BodyC2)

clip(34, 1, vec(0,0,-3), vec(0,0,-1))

holo(39, 7, 1, vec(0,3.5,0), ang(0,180,90), vec(4,2.5,18), "cone", BodyM1, BodyC1)
holo(40, 7, 1, vec(0,-5.5,0), ang(0,180,90), vec(4,2.5,2.5), "sphere3", BodyM1, BodyC1)
holo(41, 40, 1, vec(0,0,-1.25), ang(0,0,0), vec(1.25,1,1.5), "icosphere", BodyM1, BodyC2)
holo(42, 40, 1, vec(0,0,-1.25):rotate(-60,0,0), ang(0,-60,0), vec(1.25,1,1.65), "icosphere", BodyM1, BodyC2)
holo(43, 40, 1, vec(0,0,-1.25):rotate(60,0,0), ang(0,60,0), vec(1.25,1,1.65), "icosphere", BodyM1, BodyC2)

clip(39, 1, vec(0,0,-3), vec(0,0,-1))

#-Legs
holo(44, 8, 1, vec(2,0.5,-1), ang(0,0,0), vec(1.5,3.5,3), "dome", BodyM1, BodyC1)
holo(45, 8, 1, vec(2,0.5,-1), ang(0,0,0), vec(8.5,3.5,3), "dome", BodyM1, BodyC1)
holo(46, 45, 1, vec(0,-0.7,0.1), ang(0,-10,0), vec(3,1.25,1.5), "dome", BodyM1, BodyC2)
holo(47, 45, 1, vec(0,0.7,0.1), ang(0,10,0), vec(3,1.25,1.5), "dome", BodyM1, BodyC2)

clip(45, 1, vec(0,0,0), vec(-1,0,0))

holo(48, 9, 1, vec(2,-0.5,-1), ang(0,0,0), vec(1.5,3.5,3), "dome", BodyM1, BodyC1)
holo(49, 9, 1, vec(2,-0.5,-1), ang(0,0,0), vec(8.5,3.5,3), "dome", BodyM1, BodyC1)
holo(50, 49, 1, vec(0,-0.7,0.1), ang(0,-10,0), vec(3,1.25,1.5), "dome", BodyM1, BodyC2)
holo(51, 49, 1, vec(0,0.7,0.1), ang(0,10,0), vec(3,1.25,1.5), "dome", BodyM1, BodyC2)

clip(49, 1, vec(0,0,0), vec(-1,0,0))

holo(52, 8, 1, vec(0,0,1), ang(0,0,0), vec(2,2,3), "sphere3", BodyM1, BodyC1)
holo(53, 9, 1, vec(0,0,1), ang(0,0,0), vec(2,2,3), "sphere3", BodyM1, BodyC1)



#-Cell Shading
if (CellShading)
{

local S = HoloCount + 1

#-Head
holo(S+1, 10, 1, vec(), ang(), vec(14.25,15,11.5)*-1.01, "hq_sphere", BodyM1, BodyC4)
holo(S+2, 20, 1, vec(), ang(), vec(6,6,10)*-1.015, "torus3", BodyM1, BodyC4)
holo(S+3, 25, 1, vec(), ang(), vec(6,6,10)*-1.015, "torus3", BodyM1, BodyC4)
holo(S+4, 23, 1, vec(), ang(), vec(6.25,6.25,1.5)*-1.015, "sphere3", BodyM1, BodyC4)
holo(S+5, 28, 1, vec(), ang(), vec(6.25,6.25,1.5)*-1.015, "sphere3", BodyM1, BodyC4)

clip(S+2, 1, vec(0,0,-1.2)*1.015, vec(0,0,1))
clip(S+3, 1, vec(0,0,-1.2)*1.015, vec(0,0,1))
clip(S+4, 1, vec(), vec(0,0,1))
clip(S+5, 1, vec(), vec(0,0,1))

#-Body
holo(S+6, 31, 1, vec(), ang(), vec(8,10,18)*-1.01, "sphere3", BodyM1, BodyC4)
holo(S+7, 32, 1, vec(), ang(), vec(7.9,9.9,6)*-1.01, "sphere3", BodyM1, BodyC4)
holo(S+8, 33, 1, vec(), ang(), vec(4.5,4.5,4.5)*-1.015, "sphere3", BodyM1, BodyC4)

clip(S+6, 1, vec(0,0,2)*1.01, vec(0,0,1))

#-Arms
holo(S+9, 34, 1, vec(), ang(0,0,180), vec(4+0.1,2.5+0.1,18)*-1.015, "cone", BodyM1, BodyC4)
holo(S+10, 35, 1, vec(), ang(), vec(4+0.1,2.5+0.1,2.5+0.1)*-1.015, "sphere3", BodyM1, BodyC4)
holo(S+11, 36, 1, vec(), ang(), vec(1.25,1,1.5)*-1.1, "icosphere", BodyM1, BodyC4)
holo(S+12, 37, 1, vec(), ang(), vec(1.25,1,1.65)*-1.1, "icosphere", BodyM1, BodyC4)
holo(S+13, 38, 1, vec(), ang(), vec(1.25,1,1.65)*-1.1, "icosphere", BodyM1, BodyC4)

clip(S+9, 1, vec(0,0,3)*1.015, vec(0,0,1))
clip(S+9, 2, vec(0,0,9.1), vec(0,0,-1))
clip(S+10, 1, vec(), vec(0,0,-1))

holo(S+14, 39, 1, vec(), ang(0,0,180), vec(4+0.1,2.5+0.1,18)*-1.015, "cone", BodyM1, BodyC4)
holo(S+15, 40, 1, vec(), ang(), vec(4+0.1,2.5+0.1,2.5+0.1)*-1.015, "sphere3", BodyM1, BodyC4)
holo(S+16, 41, 1, vec(), ang(), vec(1.25,1,1.5)*-1.1, "icosphere", BodyM1, BodyC4)
holo(S+17, 42, 1, vec(), ang(), vec(1.25,1,1.65)*-1.1, "icosphere", BodyM1, BodyC4)
holo(S+18, 43, 1, vec(), ang(), vec(1.25,1,1.65)*-1.1, "icosphere", BodyM1, BodyC4)

clip(S+14, 1, vec(0,0,3)*1.015, vec(0,0,1))
clip(S+14, 2, vec(0,0,9.1), vec(0,0,-1))
clip(S+15, 1, vec(), vec(0,0,-1))

#-Feet
holo(S+19, 44, 1, vec(), ang(180,0,0), vec(1.5,3.5,3)*-1.025, "dome", BodyM1, BodyC4)
holo(S+20, 45, 1, vec(), ang(180,0,0), vec(8.5,3.5,3)*-1.025, "dome", BodyM1, BodyC4)
holo(S+21, 46, 1, vec(), ang(180,0,0), vec(3,1.25,1.5)*-1.05, "dome", BodyM1, BodyC4)
holo(S+22, 47, 1, vec(), ang(180,0,0), vec(3,1.25,1.5)*-1.05, "dome", BodyM1, BodyC4)

clip(S+19, 1, vec(0,0,0), vec(-1,0,0))
clip(S+20, 1, vec(0,0,0), vec(1,0,0))

holo(S+23, 48, 1, vec(), ang(180,0,0), vec(1.5,3.5,3)*-1.025, "dome", BodyM1, BodyC4)
holo(S+24, 49, 1, vec(), ang(180,0,0), vec(8.5,3.5,3)*-1.025, "dome", BodyM1, BodyC4)
holo(S+25, 50, 1, vec(), ang(180,0,0), vec(3,1.25,1.5)*-1.05, "dome", BodyM1, BodyC4)
holo(S+26, 51, 1, vec(), ang(180,0,0), vec(3,1.25,1.5)*-1.05, "dome", BodyM1, BodyC4)

clip(S+23, 1, vec(0,0,0), vec(-1,0,0))
clip(S+24, 1, vec(0,0,0), vec(1,0,0))

}
