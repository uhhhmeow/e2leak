@name trt
@inputs 
@outputs 
@persist New:table D
@trigger 

if (first()) {

function table new_game() {
    return table(
        array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
        array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
        array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
        array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
        array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
        array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
        array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
        array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
        array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
        array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
        array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
        array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
        array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
        array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
        array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
        array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
}

function print_game( G:table ) {
    local Colors = array(
        0 = vec(155, 155, 155),
        1 = vec(255, 255, 255),
        2 = vec(255, 255,   0)
    )
    
    printColor(vec(255, 255, 255), "      Tetris")
    for (R = 1, 16) {
        local A = G[R, array]
        printColor(
            Colors[A[1, number], vector],  (A[1, number] > 0)  + " ",
            Colors[A[2, number], vector],  (A[2, number] > 0)  + " ",
            Colors[A[3, number], vector],  (A[3, number] > 0)  + " ",
            Colors[A[4, number], vector],  (A[4, number] > 0)  + " ",
            Colors[A[5, number], vector],  (A[5, number] > 0)  + " ",
            Colors[A[6, number], vector],  (A[6, number] > 0)  + " ",
            Colors[A[7, number], vector],  (A[7, number] > 0)  + " ",
            Colors[A[8, number], vector],  (A[8, number] > 0)  + " ",
            Colors[A[9, number], vector],  (A[9, number] > 0)  + " ",
            Colors[A[10, number], vector], (A[10, number] > 0) + " "
        ) 
    }
}

function table tetromino(Color, TopLeftRow, TopLeftCol) {
    return table(
        "shape" = table(
            array(Color, Color),
            array(Color, Color)),

        "position" = vec2(TopLeftRow, TopLeftCol)
    )    
}

function insert_tetromino( G:table, T:table ) {
    local Pos   = T["position", vector2]
    local Shape = T["shape", table]
    
    for (R = 1, Shape:count()) {
        local S = Shape[R, array]
        for (C = 1, S:count()) {
            G[R + Pos[1], array][C + Pos[2], number] = S[C, number]
        } 
    }
}

function remove_tetromino( G:table, T:table ) {
    local Pos   = T["position", vector2]
    local Shape = T["shape", table]
    
    for (R = 1, Shape:count()) {
        local S = Shape[R, array]
        for (C = 1, S:count()) {
            G[R + Pos[1], array][C + Pos[2], number] = 0
        } 
    }   
}

New = new_game()

}

interval(1000)

D++
insert_tetromino(New, tetromino(2, D, 4))
#print_game(New)
remove_tetromino(New, tetromino(2, D, 4))

#[insert_tetromino(New, tetromino(2, 1, 4))
print_game(New)
remove_tetromino(New, tetromino(2, 1, 4))

insert_tetromino(New, tetromino(2, 2, 4))
print_game(New)
remove_tetromino(New, tetromino(2, 2, 4))

insert_tetromino(New, tetromino(2, 3, 4))
print_game(New)
remove_tetromino(New, tetromino(2, 3, 4))]#
