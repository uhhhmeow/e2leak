@name click_interface
@inputs 
@outputs ClickStage:string
@persist CurrentPlayer:entity ClickStage:string ClickMove:vector2
@trigger none

if (first()) {

    function switch_player( Player:entity ) {
        runOnKeys( CurrentPlayer, 0 )
        runOnKeys( Player, 1 )
    
        CurrentPlayer = Player
    }

    
    switch_player( owner() )
    
    ClickStage = "SelectCellA"

}

if (keyClk()) {
    local Key = keyClkPressed()
    local Down = CurrentPlayer:keyPressed(Key)
    
    if (Key == "mouse_right") {
        # get pseudo legal list
        if (Down & ClickStage == "SelectCellA") {
            ClickMove = ClickMove:setX( 1 )
            ClickStage = "ValidateMoveList"
            
            timer("ValidateMoveList", 0)
        }
        
        # reset colors
        if (!Down & ClickStage == "ValidateMoveList") {
            stoptimer("ValidateMoveList")
            ClickStage = "SelectCellA"                
        }
        
        # queue move
        if (ClickStage == "SelectCellB") {
            ClickMove = ClickMove:setY( 2 )
            ClickStage = "SelectCellA"
            
            print("done")
        }
    }
}

if (clk("ValidateMoveList")) {
    timer("ValidateMoveList", 15)
    
    local R = randint(1, 100)
    if (R > 95) {
        stoptimer("ValidateMoveList")
        ClickStage = "SelectCellB" #set colors
        
        soundPlay("done", 0, "garrysmod/balloon_pop_cute.wav")
    }
}
