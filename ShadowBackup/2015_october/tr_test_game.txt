@name tr_test_game
@inputs 
@outputs 
@persist PlayerYaw PlayerPos:vector PlayerDir:vector CurTime DeltaTime
@persist [PlaneEnd PlaneStart]:vector PlaneDelay
@trigger none


if (first()) {
    Playeryaw = 0
    PlayerDir = vec(1, 0, 0):rotate(0, PlayerYaw, 0)
    PlayerPos = entity():pos()

    holoCreate(1)
    holoPos(1, PlayerPos)
    holoAng(1, PlayerDir:toAngle())
    holoScale(1, vec(3, 1, 1))
    holoEntity(1):setTrails(5,5,5,"trails/smoke",vec(0,0,255),255)
    
    
    holoCreate(2, vec(), vec(1.25))
    holoColor(2, vec(255,0,0))
    holoCreate(3, vec(), vec(1.25))
    holoColor(3, vec(255,255,0))
        
    function setPlayerAngle( Direction ) {
        local Direction = sign(Direction)
        
        PlayerYaw = PlayerYaw + Direction*90
        PlayerDir = vec(1, 0, 0):rotate(0, PlayerYaw, 0)
        
        PlaneEnd = PlaneStart = PlayerPos
        PlaneDelay = curtime()
    }
    
    function setPlayerPos() {
        PlayerPos  = PlayerPos + PlayerDir*100*DeltaTime
        PlaneStart = PlayerPos
        
        if (curtime() - PlaneDelay > 5) {
            PlaneEnd = PlaneEnd + PlayerDir*100*DeltaTime   
        }

        holoPos(2, PlaneStart)
        holoPos(3, PlaneEnd)
    }
    
    function dt( Interval ) {
        interval(Interval)
        
        DeltaTime = curtime() - CurTime
        CurTime   = curtime()    
    }
    
    

    CurTime = curtime()
    
    
    setPlayerAngle(0)
}

dt(90)


setPlayerPos()

holoAng(1, PlayerDir:toAngle())
holoPos(1, PlayerPos)



#[if (curtime() - CurrentTime > Delay) {
    CurrentTime = curtime()
    Delay = random(0.5, 2)
    setPlayerAngle( random(-100, 100) )
}]#
