@name c4
@inputs 
@outputs 
@persist 
@trigger 

Material = "phoenix_storms/fender_white"
Color    = vec(41, 128, 185)
Color2   = vec(52, 152, 219)
Entity = entity()
Index  = 0


for (Height = 1, 8) {
    if (Height <= 6) {
        for (Length = 1, 7) {
            Index++
            
            holoCreate(Index, Entity:toWorld(vec(0, Length*54 - 4*54, Height*54)/4))
            holoAng(Index, Entity:toWorld(ang(90, 0, 0)))
            holoScale(Index, vec(1/4,1/4,1/4*6))
            holoModel(Index, "models/hunter/misc/platehole1x1a.mdl")
            holoParent(Index, Entity)
            holoColor(Index, Color)
            holoMaterial(Index, Material)
        }
    }
    
    if (Height <= 7) {
        Index++
        holoCreate(Index, Entity:toWorld(vec(0, 0, Height*54 - 27)/4))
        holoScale(Index, vec(18, 7*54 + 7, 7)/4/12)
        holoParent(Index, Entity) 
        holoColor(Index, Color)
        holoMaterial(Index, Material)
    }

    Index++
    holoCreate(Index, Entity:toWorld(vec(0, Height*54 - 4.5*54, 4*54 - 27)/4))
    holoScale(Index, vec(18, 7, 6*54)/4/12)
    holoParent(Index, Entity)
    holoColor(Index, Color)
    holoMaterial(Index, Material)
}
