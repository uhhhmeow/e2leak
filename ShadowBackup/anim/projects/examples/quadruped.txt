@name anim/projects/examples/quadruped
@inputs Vehicle:entity Camera:wirelink
@outputs Momentum
@persist [DestHeight JumpHeight MaxSpeed Acceleration BreakFactor]
@persist [CollisionRadius FallDampeningFactor] MoveDir:vector
@persist [Position]:vector [Angle]:angle [Speed Gravity MaxGravity] [Base Player]:entity
@trigger none

if (first()) {

    #--------------
    #-- Settings
    
    #include "anim/libraries/animate"
    
    DestHeight          = 50
    JumpHeight          = 50
    MaxSpeed            = 45
    MaxGravity          = 10
    Acceleration        = MaxSpeed
    BreakFactor         = 0.8
    CollisionRadius     = 32
    FallDampeningFactor = 0.7
    
    #--------------
    #-- Setup
    
    runOnLast(1)
    rangerHitEntities(1)
    rangerFilter(Vehicle)
    rangerPersist(1)
    
    holoCreate(1)
    holoModel(1, "models/sprops/misc/origin.mdl")
    
    Base = holoEntity(1)
    
    if (Vehicle["Position", vector]) {
        Position = Vehicle["Position", vector]
        holoPos(1, Position)
    }
    else {
        Position = entity():pos() + vec(0, 0, DestHeight)
        holoPos(1, Position)
    }
    
    function setup() {
        Camera["Parent", entity]   = Base
        Camera["Position", vector] = vec(8, 0, 38)
        Camera["Distance", number] = 0
        
        rangerFilter(Vehicle)
    }
    
    function number lerp(A, B, X) {
        return A + (B - A) * X
    }
    
    setup()
    
    holoCreate(10,Base:pos())
    holoParent(10,1)
            
    addFootController("fl",Base,vec(33,15,0),0,3,30,15)
    addFootController("fr",Base,vec(33,-15,0),45,3,30,15)
    addFootController("rl",Base,vec(-33,15,0),180,3,30,15)
    addFootController("rr",Base,vec(-33,-15,0),225,3,30,15)

}

interval(90)


#--------------
#- Vehicle
Player = Vehicle:driver()

if (!Vehicle:parent()) {
    Vehicle:propFreeze(1)
    
    local Pos = holoEntity(10):toWorld(vec(0, 0, 0))
    local Ang = holoEntity(10):toWorld(ang(0, -90, 0))

    Vehicle:setPos(Pos)
    Vehicle:setAng(Ang)
    
    if ((Vehicle:pos() - Pos):length() < 0.1 & vec(Vehicle:angles() - Ang):length() < 0.1) {
        Vehicle:parentTo(holoEntity(10))
        
        setup()
    }
}
if (last()) { 
    Vehicle["Position", vector] = Position
    Vehicle:deparent()
}


if (Player) {

    #--------------
    #- Camera

    Camera["Activated", number] = 1
    if (Player:keyReload()) { Vehicle:ejectPod() }
    
    #--------------
    #- Directions
    
    local Trace = rangerOffsetHull(DestHeight*2+Speed, Position, vec(0, 0, -1), vec(24))
    
    local AimDir  = Vehicle:toLocalAxis(Player:eye()):setZ(0):normalized()
    local SideDir = AimDir:cross(vec(0, 0, 1))
    local Forward = Player:keyForward() - Player:keyBack()
    local Strafe  = Player:keyRight() - Player:keyLeft()
    
    if (Forward | Strafe) { Speed = min(Speed + Acceleration, MaxSpeed + Player:keySprint()*10) } else { Speed *= BreakFactor }

    local MoveDir = (AimDir*Forward + SideDir*Strafe):normalized()
    
    #--------------
    #- Gravity

    if (Trace:distance() <= DestHeight + 2) {
        Gravity *= FallDampeningFactor
        
        local Diff = Trace:distance() - DestHeight
        Position += Diff*vec(0, 0, -1)
    }
    elseif (Trace:distance() <= DestHeight + 20) {
        if (MoveDir:length() > 0) {
            local SlopeDot = MoveDir:dot(Trace:hitNormal())
            
            if (SlopeDot > 0.1) {
                local Diff = Trace:distance() - DestHeight
                Position += Diff*vec(0, 0, -1)
            }
            
        }
    }
    else { 
        Gravity = min(Gravity - 3, Gravity)
    }
    
    #--------------
    #- Movement

    Position += MoveDir*Speed + vec(0, 0, Gravity)
    
    if (MoveDir:length() > 0) {
        Angle = slerp(quat(MoveDir:toAngle()), quat(Angle), 0.5):toAngle()

        local Trace = rangerOffsetHull(CollisionRadius, Position, MoveDir, vec(CollisionRadius))
        
        local Dist = Trace:distance()
        if (Dist <= CollisionRadius) {
            local Normal = Trace:hitNormal()
            local Diff = -(Dist - CollisionRadius)
            local Approach = MoveDir * Speed
            
            local Reflection = -2 * (Approach:dot(Normal)) * Normal - Approach
            
            Position += Reflection:normalized() * Diff
        }
        
        #Anim += Speed*pi()/3
    } else {
        #Anim *= BreakFactor
    }

    holoPos(1, mix(Position, Base:pos(), 0.5))
    holoAng(1, Angle)

    holoPos(10, Base:toWorld(vec(0, 0, -35 + abs(cos(FrameTime/2)*FootControllers["fl", table]["stepH", number]))))
    holoAng(10, Base:toWorld(ang(5+sin(-FrameTime)*FootControllers["fl", table]["stepH", number], 0, -Base:toLocalAxis(MoveDir*Speed):y())))
    
    updateFootControllers( Speed*1.25*MoveDir:length(), 1 )

}
else {
    Camera["Activated", number] = 0
}

