@name anim/projects/examples/skeleton_dragon/init
@inputs 
@outputs
@persist 
@trigger none
@model models/maxofs2d/cube_tool.mdl

#----------------------
#-- Setup
if (first()) {
    #include "anim/libraries/holocore"
    
    ID = "Skeleton_Dragon"
    
    Chip = entity()
    ScaleFactor = 1
    ToggleColMat = 1
    ToggleShading = 0
} 


#----------------------
#-- Load the hologram and clip data arrays.
elseif (CoreStatus == "InitSpawn") {
    loadContraption()
} 


#----------------------
#-- This is like if (first()) { }, code here is run only once.
elseif (CoreStatus == "InitPostSpawn") {
    CoreStatus = "RunThisCode"  
    
    Chip:setAlpha(0)
} 


#----------------------
#-- This is where executing code goes
elseif (CoreStatus == "RunThisCode") {
    if (owner():keyPressed("up")) { reset() }
}

