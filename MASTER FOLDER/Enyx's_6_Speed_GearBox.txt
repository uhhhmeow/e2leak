@name Enyx's 6 Speed GearBox
@inputs GearBox:entity GearUp GearDown
@outputs Gear Clutch
@persist Ready GearChangeTimer MaxGear Gear TargGear HoloGridDisplay ShiftSounds [GearUpPattern GearDownPattern]:array

if (first()|dupefinished()){

#INSTRUCTIONS
#-First, choose the settings you wish to use with the e2.
#-Wire Gear and Clutch of your acf gearbox / e2 engine / prop engine to the Gear output on the chip, for realistic shifting.
#-Refresh the E2, parent the E2 to the base of the car, and you're all done!

#SETTINGS    
GearChangeTimer = 150 #THE TIME TO TRANSITION BETWEEN EACH STEP. MAKE THIS LESS FOR A FASTER CHANGE
HoloGridDisplay = 0 #DISPLAY THE GRID OF THE PATTERN SYSTEM
ShiftSounds = 1 #DISABLE OR ENABLE THE GEAR SHIFT SOUNDS

#------------DO NOT CHANGE ANYTHING UNDER HERE------------#
MaxGear = 6
Gear = 1
Ready = 1  

for (I=1,9){holoCreate(I), holoModel(I,"models/sprops/geometry/t_fdisc_12.mdl"), 
holoMaterial(I,"phoenix_storms/fender_white"), holoScale(I,vec(0.1,0.1,0.1)), holoAng(I,ang(0,0,90)), 
holoParent(I,entity())}

holoPos(2,entity():pos()+entity():up()*5+entity():forward()*2+entity():right()*2) holoPos(4,entity():pos()+entity():up()*5+entity():forward()*2)  
holoPos(6,entity():pos()+entity():up()*5+entity():forward()*2+entity():right()*-2) holoPos(7,entity():pos()+entity():up()*5+entity():forward()*0+entity():right()*2)
holoPos(8,entity():pos()+entity():up()*5+entity():forward()*0) holoPos(9,entity():pos()+entity():up()*5+entity():forward()*0+entity():right()*-2)  
holoPos(1,entity():pos()+entity():up()*5+entity():forward()*-2+entity():right()*2) holoPos(3,entity():pos()+entity():up()*5+entity():forward()*-2)  
holoPos(5,entity():pos()+entity():up()*5+entity():forward()*-2+entity():right()*-2)

holoColor(Gear,vec(255,0,0))

holoCreate(10) holoCreate(11) holoCreate(12) holoCreate(13) holoCreate(14) holoCreate(15) holoCreate(16)
holoModel(10,"models/sprops/cylinders/size_1/cylinder_1_5x12.mdl") holoModel(11,"models/sprops/cylinders/size_2/cylinder_3x3.mdl") holoModel(12,"hq_icosphere")
holoModel(13, "models/sprops/misc/fittings/cred_3_1_5_tall.mdl") holoModel(14,"models/sprops/rectangles_thin/size_1/rect_3x3x1_5.mdl")
holoModel(15,"models/sprops/rectangles_thin/size_1/rect_3x3x1_5.mdl") holoModel(16,"models/sprops/misc/alphanum/alphanum_1.mdl")
holoMaterial(10,"sprops/textures/sprops_metal5") holoMaterial(11,"phoenix_storms/fender_chrome") holoMaterial(13,"sprops/textures/sprops_metal5") holoMaterial(14,"sprops/textures/sprops_metal5")
holoMaterial(15,"phoenix_storms/fender_white") holoMaterial(16,"phoenix_storms/fender_white")
holoColor(10,vec(40,40,40)) holoColor(11,vec(40)) holoColor(12,vec(50)) holoColor(13,vec(45)) holoColor(14,vec(60)) holoColor(15,vec(25))
holoScale(11,vec(0.65)) holoScale(12,vec(0.5,0.5,0.15)) holoScale(15,vec(0.3,0.5,0.05)) holoScale(16,vec(0.04,0.01,0.04))
holoParent(11,holoEntity(10))
holoParent(10,entity()) holoParent(12,entity()) holoParent(13, holoEntity(10)) holoParent(14, entity()) holoParent(15, entity()) holoParent(16, entity())
holoAng(10,holoEntity(10):angles()-ang(0,holoEntity(10):angles():yaw(),0)) holoAng(11,holoEntity(10):angles()+ang(0,-48,0)) holoAng(13,holoEntity(10):angles()+ang(180,0,0))
holoAng(15,holoEntity(15):angles()+ang(30,0,0)) holoAng(16,entity():angles()+ang(0,-90,60))
holoPos(11,holoEntity(10):pos()+holoEntity(10):up()*-6.5) holoPos(13,holoEntity(13):pos()+holoEntity(13):up()*1) holoPos(15,holoEntity(15):pos()+entity():forward()*2.5+entity():up()*0.5)
holoPos(16,holoEntity(15):pos()+holoEntity(16):right()*-0.07)
holoBodygroup(16,0,1)
holoClipEnabled(10,1,1) holoClipEnabled(13,1,1)
holoClip(10,1,vec(0,0,0),vec(0,0,-1),0) holoClip(13,1,vec(0,0,-1),vec(0,0,1),0)

TargGear = 1

GearUpPattern=array(7,2)
GearDownPattern=array(0,0)

holoAng(10,(holoEntity(10):pos()-holoEntity(TargGear):pos()):toAngle()+ang(90,0,0))

if (HoloGridDisplay==1) {for (I=1,9) {holoAlpha(I,255)}} else {for (I=1,9) {holoAlpha(I,0)}}

entity():setAlpha(0)
}

function gearSound() {
if (ShiftSounds==1){
GearBox:soundPlay(1,0,"acf_extra/vehiclefx/trans/default_shift.wav")    
    }
}

function blockSound() {
if (ShiftSounds==1){
entity():soundPlay(2,0,"acf_extra/vehiclefx/trans/default_shift.wav")   
soundVolume(2,20)
soundPitch(2,40)
    }
}

function changeColor() { if (HoloGridDisplay==1) {for (I=1,9) {holoColor(I,vec(255,255,255))}, holoColor(TargGear,vec(255,0,0))}}

function holoAngC() {holoAng(10,((holoEntity(10):pos()-holoEntity(TargGear):pos()):toAngle()+ang(90,0,0)))}

function display() {holoModel(16,"models/sprops/misc/alphanum/alphanum_"+Gear+".mdl")}

#GEAR UP AND DOWN PATTERNS FOR THE ACTION TO FOLLOW
if (changed(Gear)&Gear==1) {GearUpPattern=array(7,2)}
if (changed(Gear)&Gear==2) {GearUpPattern=array(7,8,3), GearDownPattern=array(7,1)}
if (changed(Gear)&Gear==3) {GearUpPattern=array(8,4), GearDownPattern=array(8,7,2)}
if (changed(Gear)&Gear==4) {GearUpPattern=array(8,9,5), GearDownPattern=array(8,3)}
if (changed(Gear)&Gear==5) {GearUpPattern=array(9,6), GearDownPattern=array(9,8,4)}
if (changed(Gear)&Gear==6) {GearDownPattern=array(9,5)}

#GEAR UP FUNCTIONS-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#
if (Ready & (changed(GearUp)&GearUp)&Gear<MaxGear) {timer("1",GearChangeTimer), Clutch Ready=0}
if (clk("1")) {TargGear=GearUpPattern[1,number], changeColor(), timer("2",GearChangeTimer), blockSound(), holoAngC()}
if (clk("2")) {TargGear=GearUpPattern[2,number], changeColor(), holoAngC()}
if ((clk("2")) & GearUpPattern[3,number]==0) {Gear+=1, gearSound(), !Clutch, display(), Ready=1}
elseif ((clk("2")) & GearUpPattern[3,number]>0) {timer("3",GearChangeTimer)}
if (clk("3")) {TargGear=GearUpPattern[3,number], changeColor(), Gear+=1, gearSound(), holoAngC(), !Clutch, display(), Ready=1}
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#

#GEAR DOWN FUNCTIONS-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
if (Ready & (changed(GearDown)&GearDown)&Gear>1) {timer("4",GearChangeTimer), Clutch, Ready=0}
if (clk("4")) {TargGear=GearDownPattern[1,number], changeColor(), timer("5",GearChangeTimer), blockSound(), holoAngC()}
if (clk("5")) {TargGear=GearDownPattern[2,number], changeColor(), holoAngC()}
if ((clk("5")) & GearDownPattern[3,number]==0) {Gear-=1, gearSound(), !Clutch, display(), Ready=1}
elseif ((clk("5")) & GearDownPattern[3,number]>0) {timer("6",GearChangeTimer)}
if (clk("6")) {TargGear=GearDownPattern[3,number], changeColor(), Gear-=1, gearSound(), holoAngC(), !Clutch, display(), Ready=1}
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#
