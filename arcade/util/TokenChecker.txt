@name TokenChecker
@inputs Scr:wirelink
interval(1000)
#include "Arcade/lib/token"
findByClass("player")
local User=findClosest(Scr:entity():pos())
Scr:egpBox(1,vec2(256,256),vec2(512,512))
Scr:egpMaterial(1,"gui/gradient_down")
Scr:egpColor(1,vec(0,128,128))
Scr:egpText(2,User:name()+"'s Account",vec2(256,30))
Scr:egpAlign(2,1,0)
Scr:egpSize(2,40)
Scr:egpText(3,""+(User:getVar("Tokens"))+" tokens",vec2(256,256-25))
Scr:egpAlign(3,1,1)
Scr:egpSize(3,50)
Scr:egpText(4,""+(User:getVar("Tickets"))+" tickets",vec2(256,256+25))
Scr:egpAlign(4,1,1)
Scr:egpSize(4,50)
