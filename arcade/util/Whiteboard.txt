@name Whiteboard
@inputs Scr:wirelink
@persist Current Strings:table Drawing Edit:string
runOnChat(1)
if (first() | changed(Scr)) {Scr:egpClear() timer("redraw",10000)}
if (chatClk(owner()) & owner():lastSaid():sub(1,1)=="-") {
    local Text=owner():lastSaid():sub(2)
    local Args=Text:explode(" ")
    if (Args[1,string]=="add") {
        Current=Strings:count()+1
        Args:removeString(1)
        local Out=Args:concat(" ")
        Strings[Current,string]=Out
        print("Text "+Current+" is "+Out)
        Scr:egpText(Current,Out,Scr:egpCursor(owner()))
        Scr:egpAlign(Current,1,0)
        Drawing=1
        Edit=Out
        interval(100)
    }
    if (Args[1,string]=="color") {
        Scr:egpColor(Current,vec(Args[2,string]:toNumber(),Args[3,string]:toNumber(),Args[4,string]:toNumber()))
    }
    if (Args[1,string]=="size") {
        Scr:egpSize(Current,Args[2,string]:toNumber())
    }
    if (Args[1,string]=="edit") {
        Current=Args[2,string]:toNumber()
        Drawing=1
    }
    if (Args[1,string]=="remove") {
        Scr:egpRemove(Args[2,string]:toNumber())
    }    
    if (Args[1,string]=="box") {
        Current=Strings:count()+1
        local Size=Args[3,string]:toNumber()
        Scr:egpBox(Current,Scr:egpCursor(owner()),vec2(Size,Size/2))
        Scr:egpMaterial(Current,Args[2,string])
        print("Box "+Current+" with texture "+Args[3,string])
        Drawing=1
    }
    if (Args[1,string]=="mat") {
        Scr:egpMaterial(Args[2,string]:toNumber(),Args[3,string])
    }
}
if (Drawing) {
    interval(100)
    Scr:egpPos(Current,Scr:egpCursor(owner()))
    if (owner():keyUse()) {
        Drawing=0
        Strings[Current,array]=array(Edit,Scr:egpPos(Current),Scr:egpSize(Current),Current)
    }
}
