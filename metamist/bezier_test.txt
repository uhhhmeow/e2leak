@name Bezier Test
@inputs [Start,End]:vector Go Chair:entity
@persist HoloID X Station HeightOffset NodeOffset MoveInterval [V1,V2,V3,Attr]:vector

interval(100)
if(first() | duped()) {
    HeightOffset = 250
    MoveInterval = 10
    NodeOffset = 0.2
    
    #holoCreate(0)
    #holoScale(0, vec(2,2,2))
    #holoPos(0, Start) 
    Chair:setPos(Start + vec(0,0,10))
    Station=1
}

Attr = vec((Start:x() + End:x()) / 2, (Start:y() + End:y()) / 2, (Start:z() + End:z()) / 2 + HeightOffset)

if(Go & ~Go) {
    X=Station
    #if(Station) {
    #    Chair:setAng(ang(0,Chair:bearing(Start),0))
    #}else{
    #    Chair:setAng(ang(0,Chair:bearing(End),0))
    #}
    
    timer("Update", MoveInterval)   
}

if(clk("Update")) {
    V1 = mix(Start, Attr, X)
    V2 = mix(Attr, End, X)
    V3 = mix(V1, V2, X)
    #holoPos(0, V3)
    Chair:setPos(V3 + vec(0,0,10))
    
    
    if(Station) {
        Chair:setAng(ang(0,-(atan2(comp(End:y() - Start:y(), End:x() - Start:x())) * 180 / pi()),0))
        X-=0.01
    }else{
        Chair:setAng(ang(0,(atan2(comp(End:y() - Start:y(), End:x() - Start:x())) * 180 / pi()),0))
        X+=0.01
    }
      
    if(X<=1 & X>=0) {
        timer("Update", MoveInterval)
    }else{
        Station = !Station
    }

}

HoloID=1
for(T=0, 1, NodeOffset) {
    V1 = mix(Start, Attr, T)
    V2 = mix(Attr, End, T)
    V3 = mix(V1, V2, T)
        
    holoCreate(HoloID)
    holoPos(HoloID, V3)
    holoColor(HoloID, vec(T*255, T*255, T*255), 255)
    
    HoloID++
}
