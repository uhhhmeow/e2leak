@name 
@inputs 
@outputs 
@persist Use Props:array E:entity K Dupe Pos:vector T
@trigger 

interval(100)
if(first()) {
    runOnLast(1)
    
    E = entity()
}
Use = owner():keyUse()

if(Use & $Use) {
    for(I = 1, Props:count()) {
        Props[I, entity]:setColor(vec(255,255,255), 255)
    }
    Props:clear()
    
    Props:pushEntity(owner():aimEntity())
    #owner():aimEntity():setColor(vec(0, 255, 0), 100)
    
    M = owner():aimEntity():getConstraints()
    
    for(I = 1, M:count()) {
        Props:pushEntity(M[I, entity])
        #M[I, entity]:setColor(vec(0, 255, 0), 100)
    }

    rangerFilter(Props)
    Ranger = rangerOffset(1000000000, Props[1, entity]:pos(), vec(0,0,-1))
    Pos = Ranger:pos()
    
    Dupe = 1
    K = 1
    T = 0
}

if(Dupe) {
    print(Dupe + " , " + K + " , " + Props:count() + " , " + T)
    if(T >= 4) {
        T = 0
        if(K <= Props:count()) {
            S = propSpawn(Props[K,entity]:model(), E:pos() + (Props[K, entity]:pos() - Pos), 1)
            S:setAng(Props[K, entity]:angles())
            S:setMaterial(Props[K, entity]:getMaterial())
            S:setColor(Props[K, entity]:getColor())
            
            K++
        }
        else {
            Dupe = 0
        }
    }
    T++
}

if(last()) {
    for(I = 1, Props:count()) {
        Props[I, entity]:setColor(vec(255,255,255), 255)
    }
}
