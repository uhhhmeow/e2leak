@name 
@inputs 
@outputs 
@persist Links Tbl:table
@trigger 
@model models/hunter/blocks/cube05x05x05.mdl
interval(60)
if (first()) {
    
    holoCreate( 1 )
    
    function angle lerp( A:angle, B:angle, X ) {
        local DirA = A:forward()
        local DirB = B:forward()
        
        return ( DirA + ( DirB - DirA ) * X ):toAngle()
    }
    
    function vector lerp( A:vector, B:vector, X ) {
        return A + ( B - A ) * X
    }
    
    function table:newLink(Idx, Parent, Offset:vector, AngOffset:angle) {
        Links++
        This[Links, table] = table(
            "Idx"  = Idx,
            "Parent" = holoEntity(Parent) ?: entity(),
            "Offset" = Offset,
            "AngOffset" = AngOffset,
            "Pos" = holoEntity(Idx):pos()
        )
        holoUnparent(Idx)
    } 
    
    function table:doLink( Speed )   {
        local Count = This:count()
        for (I = 1, Count) {
            local T = This[I, table]
            local Id = T["Idx", number]
            holoPos(Id, T["Parent", entity]:toWorld(T["Offset", vector]))
            
            local Next = holoEntity( I < Count ? Id+1 : Id )
            local Ang = (T["Parent", entity]:pos() - Next:pos()):toAngle()
            
            local NewAng = Ang#:setRoll(T["Parent", entity]:toWorld(T["AngOffset", angle]):roll())
            
            holoAng(Id, NewAng) #:setRoll(Roll)
        }
    }
    
    for (I = 2, 31) {
        holoCreate( I )
        holoPos( I, holoEntity(I - 1):toWorld( vec( -10, 0, 0 )) )
        holoAng( I, holoEntity(I - 1):toWorld( ang( 0, 0, 0 ) ) )
        Tbl:newLink( I, I - 1, vec(-6,0,0), ang( 0,0,0 ) )
    }
    holoAng(1, entity():angles())
    holoParent(1, entity())
}


Tbl:doLink( 1 )
