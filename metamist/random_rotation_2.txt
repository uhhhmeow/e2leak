@name 
@inputs 
@outputs 
@persist E:entity A:table Num [Data1 Data2]:table Ang:array Speed
@trigger 

interval(100)
if(first())
{
    E = entity()
    A = table()
    Num = 10
    Speed = 10
    holoCreate(2)
    holoModel(2, "hq_icosphere")
    holoScale(2, vec(6,6,6))
    holoMaterial(2, "models/props_combine/com_shield001a")
    holoColor(2, vec(0,255,0))
    holoParent(2, E)
    
    holoCreate(3)
    holoModel(3, "hq_icosphere")
    holoScale(3, -vec(7,7,7))
    holoColor(3, vec(0,255,0))
    holoParent(3, E)
    
    A:pushTable(table())
    A:pushTable(table())
    Data1 = A[1,table]
    Data2 = A[2,table]
    
    for(I = 1, Num) {
        holoCreate(I+3)
        #holoModel(I+3, "hq_torus_thin")
        #holoScale(I+3, vec(4+I/Num*25,4+I/Num*25,4+I/Num*10))
        holoMaterial(I+3, "models/XQM/LightLinesRed")
        Data1:pushArray(array())
        Data2:pushArray(array())
    }
}

for(I=1,Num){
    R1 = Data1[I,array]
    R1x= Data2[I,array]
    
    # Ring1- Pitch Value
    if(R1[1,normal] != R1x[1,normal] & R1[1,normal] < R1x[1,normal]){
        R1[1,normal] = R1[1,normal] + 1
    }elseif(R1[1,normal] != R1x[1,normal] & R1[1,normal] > R1x[1,normal]){
        R1[1,normal] = R1[1,normal] - 1
    }
    
    if (R1[1,normal] == R1x[1,normal]) {
        R1x[1,normal] = randint(0,255)
    }
    # Ring1- Yaw Value
    if(R1[2,normal] != R1x[2,normal] & R1[2,normal] < R1x[2,normal]){
        R1[2,normal] = R1[2,normal] + 1
    }elseif(R1[1,normal] != R1x[1,normal] & R1[2,normal] > R1x[2,normal]){
        R1[2,normal] = R1[2,normal] - 1
    }
    
    if (R1[2,normal] == R1x[2,normal]) {
        R1x[2,normal] = randint(0,255)
    }
    # Ring1- Roll Value
    if(R1[3,normal] != R1x[3,normal] & R1[3,normal] < R1x[3,normal]){
        R1[3,normal] = R1[3,normal] + 1
    }elseif(R1[3,normal] != R1x[3,normal] & R1[3,normal] > R1x[3,normal]){
        R1[3,normal] = R1[3,normal] - 1
    }
    
    if (R1[3,normal] == R1x[3,normal]) {
        R1x[3,normal] = randint(0,255)
    }
    
    Ang[I,vector] = vec(50,0,0):rotate(R1[1,normal] * Speed,R1[2,normal] * Speed,R1[3,normal] * Speed)
    
    #holoAng(I+3,Ang[I,angle]*4)
    holoPos(I+3, E:pos() + Ang[I,vector])
}
