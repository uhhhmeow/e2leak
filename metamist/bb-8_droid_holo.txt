@name 
@inputs 
@outputs 
@persist Position:vector
@persist [Radius CollisionRadius FallDampeningFactor]
@trigger 

interval(60)

if (first()) {
    Radius              = 50
    Height              = 45
    JumpHeight          = 50
    MaxSpeed            = 20
    MaxGravity          = 10
    Acceleration        = 5
    BreakFactor         = 0.5
    CollisionRadius     = Radius
    FallDampeningFactor = 0.7
    
    holoCreate(1)
    holoModel(1, "icosphere3")
    holoScaleUnits(1, vec(Radius) * 2)
    
    Position = entity():toWorld(vec(0, 0, Radius))
}


local MoveDir = (owner():pos() - holoEntity(1):pos()):setZ(0):normalized()
local Speed = 15

local Trace = rangerOffsetHull(CollisionRadius, Position, MoveDir, vec(CollisionRadius))

Position += MoveDir * Speed

local SpeedDiff = Speed
local RollDir = MoveDir

local Dist = Trace:distance()
if (Trace:hit() & Dist < CollisionRadius) {
    local Normal = Trace:hitNormal()
    local Diff = -(Dist - CollisionRadius)
    local Approach = MoveDir * Speed
    
    local Reflection = -2 * (Approach:dot(Normal)) * Normal - Approach
    local ResolveDir = Reflection:normalized()
    Position += ResolveDir * Diff
    
    RollDir = (MoveDir + ResolveDir):normalized()
    SpeedDiff = (MoveDir * Speed + ResolveDir * Diff):length()
}

local Circumference = 2 * pi() * Radius
local Fract = SpeedDiff / Circumference
local DeltaAng = 360 * Fract

local Perp = vec(0, 0, 1):cross(RollDir)

holoPos(1, Position)

local Ang = holoEntity(1):angles():rotateAroundAxis(Perp, DeltaAng)
holoAng(1, Ang)

