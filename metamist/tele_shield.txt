@name 
@inputs 
@outputs 
@persist E:entity Targets:array
@persist Distance Scale
@persist ExcludeList:table T:entity
@model models/props_borealis/bluebarrel001.mdl 

interval(100)
if(first()) {
    E = entity()
    Distance = 300
    Scale = Distance / 8  #6.85
    ExcludeList[0, string] = "Erza"
    ExcludeList[1, string] = ""
    #models/error/new_light1
    
    findIncludeClass("player")
    for(I = 0, ExcludeList:count()) {
        findExcludePlayer(ExcludeList[I, string])
    }
    
    holoCreate(-1)
    holoScaleUnits(-1, vec(0.06,0.06,0.06) * Scale)
    holoModel(-1, "models/bynari/desperation.mdl")
    holoAlpha(-1, 255)
    holoMaterial(-1, "models/props_combine/masterinterface01c")
    holoAng(-1, ang(0,0,0))
    holoParent(-1, E)
    
    holoCreate(-3)
    holoScaleUnits(-3, vec(0.065,0.065,0.065) * Scale)
    holoModel(-3, "models/bynari/desperation.mdl")
    holoAlpha(-3, 255)
    holoMaterial(-3, "models/props_combine/stasisfield_beam")
    holoAng(-3, ang(0,0,0))
    holoParent(-3, E)
    
    holoCreate(-2)
    holoScaleUnits(-2, -vec(0.058,0.058,0.058) * Scale)
    holoModel(-2, "models/bynari/desperation.mdl")
    holoAlpha(-2, 10)
    holoAng(-2, ang(0,0,0))
    holoMaterial(-2, "models/props_combine/com_shield001a")
    holoParent(-2, E)
}

findInSphere(E:pos(), Distance)
Targets = findToArray()

for(I = 0, Targets:count())
{
    T = Targets[I, entity]
    if(T:type() != "") {
        Dir = T:pos()-E:pos()
        NPos = E:pos() + (-Dir:normalized() * (Distance + 10))
        T:teleport(NPos)
    }
}


if(owner():keyUse()) {
    print("end")
}
