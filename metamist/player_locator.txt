@name 
@inputs 
@outputs 
@persist Players:array
@trigger 

interval(100)
Players = players()
Pos = owner():shootPos()

for(I = 0, Players:count()) {
    Dir = (Players[I,entity]:pos()-Pos):normalized()
    holoCreate(I)
    holoPos(I, Pos + Dir*30)
    holoModel(I, "hq_torus")
    holoScale(I, vec(0.5,0.5,0.5))
    holoAng(I, (Players[I,entity]:pos() - Pos):toAngle() + ang(90,0,0))
    if(Players[I,entity] == owner()) {
        holoAlpha(I, 0)
    } else{
        holoAlpha(I, 100)
    }
}
