@name IK Example
@inputs
@outputs
@persist [E Ent]:entity TL TL2 SL RDDist
@persist BaseID Joints:table Holo 

interval(100)
if(first()){
    runOnLast(1)
    
    E = entity()
    
    TL = 30 #Tigh Lenght
    TL2 = 20
    SL = 40 #Shin Lenght
    RDDist = 1000
    
    
    Holo = 0
    Joints = table()
    
    Ent = propSpawn("models/hunter/misc/sphere025x025.mdl",E:toWorld(vec(40,0,10)),1)   
    
    function angle entity:ta(P, Y, R) { return This:toWorld(ang(P, Y, R)) }
    function vector entity:tv(X, Y, Z) { return This:toWorld(vec(X, Y, Z)) }
    
    function void hCreate() { holoCreate(Holo) }
    function void hParent(ID) { holoParent(Holo, ID) }
    function void hParent(ID:entity) { holoParent(Holo, ID) }
    function void hAng(Ang:angle) { holoAng(Holo, Ang) }
    function void hPos(Pos:vector) { holoPos(Holo, Pos) }
    function void hScale(Vec:vector) { holoScaleUnits(Holo, Vec) }
    function entity hEnt(ID) { return holoEntity(ID) }
    function void hFinish() { Holo++ }
    
    function number hID() { return Holo }
     
    function void setJoint(Str:string, HoloID) { Joints[Str, number] = HoloID }
    function number getJoint(Str:string) { return Joints[Str, number] }
    
    BaseID = hID()
    hCreate()
    hPos(E:tv(0, 0, 60))
    hParent(E)
    hFinish()
    
    for (I = 1, 1) {
        local YOffset = 20 * (I == 2 ? -1 : 1)
        
        J1 = hID()
        hCreate()
        hParent(BaseID)
        hPos(hEnt(BaseID):tv(0,YOffset,0))
        hAng(hEnt(BaseID):ta(0,0,0))
        hFinish()
        
        hCreate()
        hParent(J1)
        hPos(hEnt(J1):tv(0,0,TL / 2))
        hAng(hEnt(J1):ta(90,0,0))
        hScale(vec(TL, 4, 4))
        hFinish()
        
        J2 = hID()
        hCreate()
        hParent(J1)
        hPos(hEnt(J1):tv(0,0,TL))
        hAng(hEnt(J1):ta(0,0,0))
        hFinish()
        
        hCreate()
        hParent(J2)
        hPos(hEnt(J2):tv(0,0,TL2 / 2))
        hAng(hEnt(J2):ta(90,0,0))
        hScale(vec(TL2, 4, 4))
        hFinish()
        
        J3 = hID()
        hCreate()
        hParent(J2)
        hPos(hEnt(J2):tv(0,0,TL2))
        hAng(hEnt(J2):ta(0,0,0))
        hFinish()
        
        hCreate()
        hParent(J3)
        hPos(hEnt(J3):tv(0,0,SL / 2))
        hAng(hEnt(J3):ta(90,0,0))
        hScale(vec(SL, 4, 4))
        hFinish()
        
        setJoint("Leg " + I + ".1", J1)
        setJoint("Leg " + I + ".1.Length", TL)
        setJoint("Leg " + I + ".1.Rest", TL2 + SL)
        
        setJoint("Leg " + I + ".2", J2)
        setJoint("Leg " + I + ".2.Length", TL2)
        setJoint("Leg " + I + ".2.Rest", SL)
        
        setJoint("Leg " + I + ".3", J3)
        setJoint("Leg " + I + ".3.Length", SL)
        setJoint("Leg " + I + ".3.Rest", 0)
    }
    
    #[holo(Holo,E:tv(0,0,50),E:ta(0,0,0),vec(10,10,10),vec(100,100,100),E,1)
    holoExtra(Holo,"cube","Phoenix_storms/mat/mat_phx_metallic",255,0) Holo++
    
    holo(Holo,he(1):tv(0,0,-TL/2),he(1):ta(90,0,0),vec(TL,4,4),vec(100,100,100),he(1),1)
    holoExtra(Holo,"cube","Phoenix_storms/mat/mat_phx_metallic",255,0) Holo++
    
    holo(Holo,he(1):tv(0,0,-TL),he(1):ta(0,0,0),vec(10,10,10),vec(100,100,100),he(1),1)
    holoExtra(Holo,"cube","Phoenix_storms/mat/mat_phx_metallic",255,0) Holo++
    
    holo(Holo,he(3):tv(0,0,SL/2),he(3):ta(90,0,0),vec(SL,4,4),vec(100,100,100),he(3),1)
    holoExtra(Holo,"cube","Phoenix_storms/mat/mat_phx_metallic",255,0) Holo++               
    ]#
    rangerPersist(1)
    rangerFilter(entity())
    rangerFilter(Ent)
}


for (I = 1, 1) {
    local Side = (I == 1 ? -1 : 1)
    
    local Joint1 = getJoint("Leg " + I + ".1")
    local Joint2 = getJoint("Leg " + I + ".2")
    local Joint3 = getJoint("Leg " + I + ".3")
    
    
    
    local RDFoot = rangerOffset(RDDist, hEnt(Joint1):pos() + vec(0,0,100), vec(0,0,-1))
    
    local PrefDest = Ent:pos()
    local A = (PrefDest - RDFoot:pos())
    local A2 = A:normalized() * A:length()
    local DestDot = A2:dot(hEnt(BaseID):forward())
    
    
    local IsBehind = (PrefDest - hEnt(BaseID):pos()):normalized():dot(hEnt(BaseID):forward())
    local IsOnSide = ((PrefDest - RDFoot:pos()):normalized() * (PrefDest - RDFoot:pos()):length()):dot(hEnt(BaseID):right()) * Side >= 0
    #print(((PrefDest - RDFoot:pos()):normalized() * (PrefDest - RDFoot:pos()):length()):dot(hEnt(BaseID):right()) * Side)
    
    local DestPos = RDFoot:pos() + hEnt(BaseID):forward() * DestDot
    local Strafe = clamp(DestPos:distance(PrefDest:setZ(DestPos:z())) / 1.1, -25, 25) * Side
    
    if (!IsOnSide) {
        Strafe *= -1
        Strafe = clamp(Strafe, -15, 15)
    } else {
        holoCreate(100, DestPos + hEnt(BaseID):right() * Strafe / 1.3)
        holoColor(100, vec(0,255,0))
    }
    #DestPos = PrefDest
    #holoCreate(101, PrefDest)
    
    #print(DestDot)
    
    Hypelength = (hEnt(Joint1):pos() - DestPos):length()
    HypeAngle = (hEnt(Joint1):pos() - DestPos):toAngle()
    
    if (IsBehind < 0) { HypeAngle = HypeAngle:setPitch(-HypeAngle:pitch() - 180) }
    
    
    
    HypeL = clamp(Hypelength, TL, TL + SL)
    
    HipAng = HypeAngle:pitch()
    holoAng(Joint1, hEnt(BaseID):toLocal(hEnt(BaseID):ta(HipAng - 90,E:angles():yaw() + 180,HypeAngle:roll())))
    
    ThighHypeLength = clamp((DestPos - hEnt(Joint2):pos()):length(), TL2, TL2 + SL)
    ThighHypeAng = (DestPos - hEnt(Joint2):pos()):normalized()
    
    Knee1Ang = -acos((TL2^2 + ThighHypeLength^2 - SL^2) / (2 * TL2 * ThighHypeLength))
    holoAng(Joint2,hEnt(Joint1):ta(Knee1Ang,0,0))
    
    Knee2Ang = -acos((TL2^2 + SL^2 - ThighHypeLength^2) / (2 * TL2 * SL))
    holoAng(Joint3,hEnt(Joint2):ta(Knee2Ang + 180,0,0))
    
    
}

if (last()) { Ent:propDelete() }
