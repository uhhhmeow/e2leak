@name 
@inputs 
@outputs 
@persist 
@trigger 
interval(100)

holoCreate(1)
holoScale(1, vec(1,1,1) * 0.5)
holoModel(1, "hq_icosphere")

#holoTextCreate(1)
#holoText(1, owner():eyeAngles() + " , " + (holoEntity(1):pos() - owner():shootPos()):toAngle())

A = owner():eyeAngles():yaw()
B = (holoEntity(1):pos() - owner():shootPos()):toAngle():yaw()
A2 = owner():eyeAngles():pitch()
B2 = (holoEntity(1):pos() - owner():shootPos()):toAngle():pitch()
Range = 14
#[if((360-(A-B)%360<Range | (A-B)%360<Range) & (360-(A2-B2)%360<Range | (A2-B2)%360<Range)) {
    holoColor(1, vec(0,255,0))
} else{
    holoColor(1, vec(255,0,0))
}]#

Des=holoEntity(1):pos()
Pro=owner():shootPos()
Pro+=Pro:distance(Des)*owner():eye()
if(Pro:distance(Des)<=6 * 0.5) {
    holoColor(1, vec(0,255,0))
}
else {
    holoColor(1, vec(255,0,0))
}
