@inputs Pod:wirelink Cam:wirelink
@persist Dir:vector E:entity Active BaseAngles:vector Normal:vector OwnerEye:angle Position:vector ViewAngle:angle Angle:angle
@persist Scale Speed Pitch Joint:table Anim AnimNum Stage Owner:entity Jumping Aim:ranger
@outputs ViewAngle:angle Position:vector E:entity

function void jump(){
    if(E:pos():distance(holoEntity(Joint["aim",number]):pos())>10){
        Dir = (holoEntity(Joint["aim",number]):pos() - E:pos()):normalized()
        holoPos(1,E:pos()+Dir*5)    
    }
    else{
        Jumping = 0
        holoUnparent(Joint["aim",number])    
    }
    
}

function void moveStage1(){
    if(holoEntity(Joint["FR",number]):elevation(E:pos()+E:forward())>35 || holoEntity(Joint["FL",number]):elevation(E:pos()+E:forward())<26){
        AnimNum = 2
        Stage = 1
        E:soundPlay(curtime(),1,"weapons/crossbow/hit1.wav")
        soundPitch(curtime(),200)
        soundVolume(curtime(),0.05)
    }
    else{
        holoAng(Joint["LL",number],holoEntity(Joint["LL",number]):angles():rotateAroundAxis(E:up(),-1.25*Speed))
        if(Stage==1){
            holoAng(Joint["LL",number]+3,holoEntity(Joint["LL",number]+3):angles():rotateAroundAxis(
                    holoEntity(Joint["LL",number]+3):right(),1.25*Speed))
            if(holoEntity(Joint["LL",number]+3):elevation(holoEntity(Joint["LL",number]+2):pos())>20){
                holoAng(Joint["LL",number]+3,holoEntity(Joint["LL",number]):toWorld(ang(-60,0,0)))
                Stage=2
            }
        }
        else{
            if(holoEntity(Joint["LL",number]+3):elevation(holoEntity(Joint["LL",number]+2):pos())>0){
                holoAng(Joint["LL",number]+3,holoEntity(Joint["LL",number]+3):angles():rotateAroundAxis(
                        holoEntity(Joint["LL",number]+3):right(),-1.5*Speed))
            }
        }
        holoAng(Joint["FR",number],holoEntity(Joint["FR",number]):angles():rotateAroundAxis(E:right(),-0.5*Speed))
        if(holoEntity(Joint["FR",number]+3):elevation(holoEntity(Joint["FR",number]+2):pos())<40){
            holoAng(Joint["FR",number]+3,holoEntity(Joint["FR",number]+3):angles():rotateAroundAxis(E:right(),2*Speed))
        } 
        if(Anim){ 
            holoAng(Joint["FL",number],holoEntity(Joint["FL",number]):angles():rotateAroundAxis(E:right(),+0.5*Speed))
            if(holoEntity(Joint["FL",number]+3):elevation(holoEntity(Joint["FL",number]+2):pos())>0){
                holoAng(Joint["FL",number]+3,holoEntity(Joint["FL",number]+3):angles():rotateAroundAxis(E:right(),-1.25*Speed))
            }

            holoAng(Joint["RL",number],holoEntity(Joint["RL",number]):angles():rotateAroundAxis(E:up(),-1.25*Speed))
            if(holoEntity(Joint["RL",number]+3):elevation(holoEntity(Joint["RL",number]+2):pos())>0){
                    holoAng(Joint["RL",number]+3,holoEntity(Joint["RL",number]+3):angles():rotateAroundAxis(
                            holoEntity(Joint["RL",number]+3):right(),1.5*Speed))
            }
        }
    }
}
function void moveStage2(){
    if(holoEntity(Joint["FR",number]):elevation(E:pos()+E:forward())<26 || holoEntity(Joint["FL",number]):elevation(E:pos()+E:forward())>35){
        AnimNum = 1  
        Stage = 1
        E:soundPlay(curtime(),1,"weapons/crossbow/hit1.wav")
        soundPitch(curtime(),200)
        soundVolume(curtime(),0.05)
    }
    else{
        Anim = 1
        holoAng(Joint["LL",number],holoEntity(Joint["LL",number]):angles():rotateAroundAxis(E:up(),1.25*Speed))
        if(holoEntity(Joint["LL",number]+3):elevation(holoEntity(Joint["LL",number]+2):pos())>0){
                holoAng(Joint["LL",number]+3,holoEntity(Joint["LL",number]+3):angles():rotateAroundAxis(
                        holoEntity(Joint["LL",number]+3):right(),1.5*Speed))
        }
        holoAng(Joint["RL",number],holoEntity(Joint["RL",number]):angles():rotateAroundAxis(E:up(),1.25*Speed))
        if(Stage==1){
            holoAng(Joint["RL",number]+3,holoEntity(Joint["RL",number]+3):angles():rotateAroundAxis(
                    holoEntity(Joint["RL",number]+3):right(),1.25*Speed))
            if(holoEntity(Joint["RL",number]+3):elevation(holoEntity(Joint["RL",number]+2):pos())>20){
                holoAng(Joint["RL",number]+3,holoEntity(Joint["RL",number]):toWorld(ang(-60,0,0)))
                Stage=2
            }
        }
        else{
            if(holoEntity(Joint["RL",number]+3):elevation(holoEntity(Joint["RL",number]+2):pos())>0){
                holoAng(Joint["RL",number]+3,holoEntity(Joint["RL",number]+3):angles():rotateAroundAxis(
                        holoEntity(Joint["RL",number]+3):right(),-1.5*Speed))
            }
        }
        holoAng(Joint["FR",number],holoEntity(Joint["FR",number]):angles():rotateAroundAxis(E:right(),+0.5*Speed))
        if(holoEntity(Joint["FR",number]+3):elevation(holoEntity(Joint["FR",number]+2):pos())>0){
            holoAng(Joint["FR",number]+3,holoEntity(Joint["FR",number]+3):angles():rotateAroundAxis(E:right(),-1.25*Speed))
        }
        holoAng(Joint["FL",number],holoEntity(Joint["FL",number]):angles():rotateAroundAxis(E:right(),-0.5*Speed))
        if(holoEntity(Joint["FL",number]+3):elevation(holoEntity(Joint["FL",number]+2):pos())<40){
            holoAng(Joint["FL",number]+3,holoEntity(Joint["FL",number]+3):angles():rotateAroundAxis(E:right(),2*Speed))
        }
    } 
}



if(tickClk()){
    
    Active = Pod["Active",number]
    Driver = Pod["Entity",entity]:driver()
    if($Active & Active){
        BaseAngles = owner():eye()
        holoVisible(31,players(),0)
        holoVisible(31,Driver,1)
    }
    elseif(Active){
        OldNormal = E:up()
        W = Pod["W",number]
        A = Pod["A",number]
        S = Pod["S",number]
        D = Pod["D",number]
        Space = Pod["Space",number]
        Shift = Pod["Shift",number]
        Alt = Pod["Alt",number]
        Rinput = Pod["R",number]
        Mouse1 = Pod["Mouse1",number]
        Mouse2 = Pod["Mouse2",number]
        Active = Pod["Active",number]
        Dir = vec(0,0,0)
        
        Cam["Position", vector] = holoEntity(1):toWorld(vec(0,0,0))
        Cam["Angle", angle] = ViewAngle
        
        if(Alt){Mult=5}
        elseif(Rinput){Mult=1}
        else{ Mult=2 }
        
        Speed = Mult*2.25
        if(W){
            Dir+=E:forward()*Mult
            if(AnimNum==1){
                moveStage1()
            }
            elseif(AnimNum==2){
                moveStage2()
            }
        }
        if(A){Dir-=E:right()*Mult}
        if(S){Dir-=E:forward()*Mult}
        if(D){Dir+=E:right()*Mult}
        if(Space){
            if(!Jumping && Aim:hit()){
                if(!Aim:hitWorld() && Aim:entity()!=noentity()){
                    holoParent(1,Aim:entity())
                    holoPos(Joint["aim",number],Aim:pos())
                    holoParent(Joint["aim",number],Aim:entity())
                }
                else{
                    holoUnparent(1)    
                }
                
                Jumping = 1
            }
        }
        if(Shift){Dir-=vec(0,0,1)*Mult}
        if(Jumping){
            jump()
            exit()    
        }
        
        
        rangerFilter(entity())
        rangerFilter(entity():getConstraints())
        RaX0 = rangerOffset(10+20*Scale,E:pos()+Dir,-E:up())
        RaX1 = rangerOffset(10+20*Scale,E:pos()+Dir,E:up())
        RaX2 = rangerOffset(10+20*Scale,E:pos()+Dir,E:forward())
        RaX3 = rangerOffset(10+20*Scale,E:pos()+Dir,-E:up()+E:forward())
        RaX4 = rangerOffset(10+20*Scale,E:pos()+Dir,E:up()+E:forward())
         

        
        
        Rangers = array()
        Rangers:pushRanger(RaX0)
        Rangers:pushRanger(RaX1)
        Rangers:pushRanger(RaX2)
        Rangers:pushRanger(RaX3)
        Rangers:pushRanger(RaX4)
        
        Ranger = RaX0
        foreach(I, R:ranger=Rangers){
            if(R:hit() & R:distance()<Ranger:distance()){
                Ranger = R
            }
        }
        
        if(!Ranger:hit()){
            RaX0 = rangerOffset(10+20*Scale,E:pos()+Dir-E:up()-E:up()*(5),-E:forward())
            RaX1 = rangerOffset(10+20*Scale,E:pos()+Dir-E:up()-E:up()*(5),E:forward())
            RaX2 = rangerOffset(10+20*Scale,E:pos()+Dir-E:up()-E:up()*(5),E:right())
            RaX3 = rangerOffset(10+20*Scale,E:pos()+Dir-E:up()-E:up()*(5),-E:right())
            
            
            Rangers = array()
            Rangers:pushRanger(RaX0)
            Rangers:pushRanger(RaX1)
            Rangers:pushRanger(RaX2)
            Rangers:pushRanger(RaX3)
            
            Ranger = RaX0
            foreach(I, R:ranger=Rangers){
                if(R:hit() & R:distance()<Ranger:distance()){
                    Ranger = R
                }
            }
        }
        
        
        if(Ranger:hit()){
            if(!Ranger:hitWorld() && Ranger:entity()!=noentity()){
                holoParent(1,Ranger:entity())    
            }
            else{
                holoUnparent(1)    
            }
            Normal = Ranger:hitNormal()
            HitPos = Ranger:pos()+Normal*5
        }
        if(!Ranger:hit()){
            holoUnparent(1)
            HitPos = E:pos()-vec(0,0,5)
            Normal = vec(0,0,1)
        }
        
        L = acos(OldNormal:dot(Normal))
        Angle = holoEntity(1):angles():rotateAroundAxis(OldNormal:cross(Normal),L)
        Yaw = OwnerEye:yaw()-Driver:eyeAngles():yaw()
        Angle = Angle:rotateAroundAxis(Normal,-Yaw)
        OwnerEye = Driver:eyeAngles()
        holoAng(1,Angle)
        holoPos(holoIndex(E),HitPos)
        ViewAngle = Angle:rotateAroundAxis(E:right(),-OwnerEye:pitch())
        Position = HitPos+Normal*10-ViewAngle:forward()*10
        
        Aim = rangerOffset(200,Position,ViewAngle:forward())
        
        if(Aim:hit())
        {
            holoColor(Joint["aim",number],vec(100,255,100))
        }
        else{
            holoColor(Joint["aim",number],vec(255,100,100))
        }
        
        holoPos(Joint["aim",number],Aim:pos())
        
        
        if(Mouse2){
            #holoEntity(1):simulateUse(Aim:entity(),owner())
        }
    }
    Cam["Activated", normal] = Active
}
elseif(first() || duped()){
    
    Anim = 0
    Stage = 1
    AnimNum = 1
    Jumping = 0
    Speed = 0.5 # initial value, changes later
    
    ## FR - Front Right
    ## RL - Right Leg
    ## FL - Front Left
    ## LL - Left Leg
    
    Joint = table()
    #if(owner():steamID()!="STEAM_0:1:22668309"){selfDestructAll()}
    Scale = 2
    OwnerEye = owner():eyeAngles()
    Normal = vec(0,0,1)
    runOnTick(1)
    E = entity()
    Pitch = 0
    Holo = 1
    Pos =  vec(0,0,0)
    holoCreate(Holo,vec(0,0,0),vec(10,10,1)*0.1)
    holoAng(Holo,ang(0,0,0))
    holoAlpha(Holo,0) Holo++
    E = holoEntity(1)

    holoCreate(Holo,Pos+E:up()*0.5-E:forward()*2,vec(0.6,0.1,0.6),ang(0,90,100))
    holoModel(Holo,"prism")
    holoParent(Holo,Holo-1) Holo++
    holoCreate(Holo,Pos+E:right()*3,vec(1,0.25,0.075),ang(10,-10,20))
    holoParent(Holo,Holo-1) Holo++
    holoCreate(Holo,Pos-E:right()*3,vec(1,0.25,0.075),ang(10,10,-20))
    holoParent(Holo,Holo-1) Holo++
    
    holoCreate(Holo,Pos+E:right()*5+E:forward()*2-E:up()*1.5,vec(1,0.25,0.075),ang(15,-15,25))
    holoParent(Holo,1) Holo++ ## Front right
    Joint["FR",number] = Holo
    holoCreate(Holo,Pos+E:right()*2-E:forward()*0.5-E:up()*1,vec(0.15,0.15,0.15),ang(10,0,0))
    holoParent(Holo,Holo-1) Holo++ #body joint right
    holoCreate(Holo,Pos+E:right()*2+E:forward()*3-E:up()*1.5,vec(0.6,0.075,0.1),ang(10,0,0))
    holoParent(Holo,Holo-1) Holo++
    holoCreate(Holo,Pos+E:right()*2+E:forward()*8.5+E:up()*0.2,vec(0.6,0.075,0.1),ang(-40,0,0))
    holoParent(Holo,Holo-1) Holo++
    holoCreate(Holo,Pos+E:right()*2+E:forward()*11.20+E:up()*2.5,vec(0.125,0.125,0.125),ang(-40,0,0))
    holoParent(Holo,Holo-1) Holo++
    holoCreate(Holo,Pos+E:right()*2+E:forward()*14+E:up()*0.2,vec(0.6,0.075,0.1),ang(40,0,0))
    holoParent(Holo,Holo-1) Holo++
    holoCreate(Holo,Pos+E:right()*2+E:forward()*16.5-E:up()*4,vec(0.45,0.075,0.1),ang(80,0,0))
    holoParent(Holo,Holo-1) Holo++
    
    
    
    Joint["RL",number] = Holo
    holoCreate(Holo,Pos+E:right()*2+E:forward()*3-E:up()*1.2,vec(0.125,0.125,0.125),ang(10,0,0))
    holoParent(Holo,1) Holo++
    holoCreate(Holo,Pos+E:right()*2+E:forward()*4.75-E:up()*1.5,vec(0.25,0.075,0.1),ang(10,0,0))
    holoParent(Holo,Holo-1) Holo++
    holoCreate(Holo,Pos+E:right()*2+E:forward()*8.5+E:up()*0.2,vec(0.6,0.075,0.1),ang(-40,0,0))
    holoParent(Holo,Holo-1) Holo++
    holoCreate(Holo,Pos+E:right()*2+E:forward()*11.20+E:up()*2.5,vec(0.125,0.125,0.125),ang(-40,0,0))
    holoParent(Holo,Holo-1) Holo++
    holoCreate(Holo,Pos+E:right()*2+E:forward()*14+E:up()*0.2,vec(0.6,0.075,0.1),ang(40,0,0))
    holoParent(Holo,Holo-1) Holo++
    holoCreate(Holo,Pos+E:right()*2+E:forward()*16.5-E:up()*4,vec(0.45,0.075,0.1),ang(80,0,0))
    holoParent(Holo,Holo-1) Holo++
    holoPos(Joint["RL",number],Pos+E:right()*2-E:forward()*4)
    holoAng(Joint["RL",number],ang(10,-110,0))
    
    
    Joint["LL",number] = Holo
    holoCreate(Holo,Pos+E:right()*2+E:forward()*3-E:up()*1.2,vec(0.125,0.125,0.125),ang(10,0,0))
    holoParent(Holo,1) Holo++
    holoCreate(Holo,Pos+E:right()*2+E:forward()*4.75-E:up()*1.5,vec(0.25,0.075,0.1),ang(10,0,0))
    holoParent(Holo,Holo-1) Holo++
    holoCreate(Holo,Pos+E:right()*2+E:forward()*8.5+E:up()*0.2,vec(0.6,0.075,0.1),ang(-40,0,0))
    holoParent(Holo,Holo-1) Holo++
    holoCreate(Holo,Pos+E:right()*2+E:forward()*11.20+E:up()*2.5,vec(0.125,0.125,0.125),ang(-40,0,0))
    holoParent(Holo,Holo-1) Holo++
    holoCreate(Holo,Pos+E:right()*2+E:forward()*14+E:up()*0.2,vec(0.6,0.075,0.1),ang(40,0,0))
    holoParent(Holo,Holo-1) Holo++
    holoCreate(Holo,Pos+E:right()*2+E:forward()*16.5-E:up()*4,vec(0.45,0.075,0.1),ang(80,0,0))
    holoParent(Holo,Holo-1) Holo++
    holoPos(Joint["LL",number],Pos-E:right()*2-E:forward()*4)
    holoAng(Joint["LL",number],ang(10,110,0))
    
    
    
    holoCreate(Holo,Pos-E:right()*5+E:forward()*2-E:up()*1.5,vec(1,0.25,0.075),ang(15,15,-25))
    holoParent(Holo,1) Holo++ ## Front left
    Joint["FL",number] = Holo
    holoCreate(Holo,Pos-E:right()*2-E:forward()*0.5-E:up()*1,vec(0.15,0.15,0.15),ang(10,0,0))
    holoParent(Holo,Holo-1) Holo++ #body joint left
    holoCreate(Holo,Pos-E:right()*2+E:forward()*3-E:up()*1.5,vec(0.6,0.075,0.1),ang(10,0,0))
    holoParent(Holo,Holo-1) Holo++
    holoCreate(Holo,Pos-E:right()*2+E:forward()*8.5+E:up()*0.2,vec(0.6,0.075,0.1),ang(-40,0,0))
    holoParent(Holo,Holo-1) Holo++
    holoCreate(Holo,Pos-E:right()*2+E:forward()*11.20+E:up()*2.5,vec(0.125,0.125,0.125),ang(-40,0,0))
    holoParent(Holo,Holo-1) Holo++
    holoCreate(Holo,Pos-E:right()*2+E:forward()*14+E:up()*0.2,vec(0.6,0.075,0.1),ang(40,0,0))
    holoParent(Holo,Holo-1) Holo++
    holoCreate(Holo,Pos-E:right()*2+E:forward()*16.5-E:up()*4,vec(0.45,0.075,0.1),ang(80,0,0))
    holoParent(Holo,Holo-1) Holo++
    
    for(I=1,Holo){
        #Cmats/Station_Metal_B
        holoMaterial(I,"models/props_combine/Combine_Citadel001")  
        #holoColor(I,vec(200,200,200))  
    }
    
    
    Joint["aim",number] = Holo
    holoCreate(Holo,vec(0,0,0),vec(1,1,1)*0.1)
    holoModel(Holo,"icosphere2")
    holoColor(Holo,vec(255,0,0))
    print(Holo)
    holoMaterial(Holo,"models/debug/debugwhite") Holo++
    
    
    holoPos(1,entity():pos())
    
}


