@name Fanorona
@inputs 
@outputs 
@persist E:entity FinishedLoading
@persist BoardWidth BoardHeight Spacing ScaleMult BoardEdgeSpacing BorderThickness
@persist SlotSizeW SlotSizeH SlotThickness
@persist ChipSizeW ChipSizeH ChipThickness
@persist [SlotColor LinkColor MovePointerColor]:vector4
@persist [Neighbours XNeighbours]:array
@persist [BoardOrigin BoardMin BoardMax BoardSize]:vector CenterBoard
@persist SpinnerOrigin:vector
@persist Board:table Chips:table MovePointers:table Spinner:table Players:table
@persist Turn
@persist ID
@trigger 

interval(60)

if (first()) {
    E = entity()
    
    BoardWidth  = 9
    BoardHeight = 5
    
    SlotColor         = vec4(100, 100, 100, 255)
    LinkColor         = vec4(80, 80, 80, 255)
    MovePointerColor  = vec4(20, 20, 20, 255)
    
    SlotSizeW = 16
    SlotSizeH = 16
    SlotThickness = 1
    Spacing = 5

    ChipSizeW = 8
    ChipSizeH = 8
    ChipThickness = 1
    
    BoardOrigin = vec(0, 0, 10)
    CenterBoard = 1
    
    BorderThickness = 4
    BoardEdgeSpacing = 5
    
    ScaleMult = 1.083

    Turn = -1
    
    local SpacingX = ((BoardWidth-1) * Spacing)
    local SpacingY = ((BoardHeight-1) * Spacing)
    BoardMin = vec(0, 0, 0 )
    BoardMax = vec(BorderThickness * 2 + BoardEdgeSpacing * 2 + BoardWidth * SlotSizeW + SpacingX,
                        BorderThickness * 2 + BoardEdgeSpacing * 2 + BoardHeight * SlotSizeH + SpacingY, 0 )
    BoardSize = BoardMax - BoardMin
    
    if (CenterBoard) {
        BoardOrigin = BoardOrigin + vec(-BoardSize:x() / 2, -BoardSize:y() / 2, 0)
    }
    
    BoardMin = BoardMin + BoardOrigin
    BoardMax = BoardMax + BoardOrigin
    
    SpinnerOrigin = BoardMin + vec(-30, BoardSize:y() / 2, 0)
    
    local Neighbours = array(
        vec2(-1, 0),
        vec2(0, -1),
        vec2(1, 0),
        vec2(0, 1)
    )
    
    local XNeighbours = array(
        vec2(-1, -1),
        vec2(1, -1),
        vec2(1, 1),
        vec2(-1, 1)
    )

    function table getBoardSlot(X, Y) {
        return Board[X + Y * BoardWidth, table]
    }
    
    function setBoardHoloID(X, Y, ID) {
        Board[X + Y * BoardWidth, table]["Holo", number] = ID
    }

    function vector getBoardSlotPos(X, Y) {
        local SpacingX = (X * Spacing)
        local SpacingY = (Y * Spacing)
        return E:toWorld(BoardOrigin +
                    vec(BorderThickness + BoardEdgeSpacing + (SlotSizeW / 2) + X * SlotSizeW + SpacingX,
                        BorderThickness + BoardEdgeSpacing + (SlotSizeH / 2) + Y * SlotSizeH + SpacingY, 0))
    }
    
    function number linkBoardSlots(X, Y, X2, Y2, OneWay) {
        local Slot1 = getBoardSlot(X, Y)
        if (!Slot1) { return 0 }
        
        for (I = 1, Slot1["Links", array]:count()) {
            local V = Slot1["Links", array][I, vector2]
            if (V:x() == X2 && V:y() == Y2) {
                return 0
            }
        }
        
        Slot1["Links", array]:pushVector2(vec2(X2, Y2))
        
        if (!OneWay) {
            linkBoardSlots(X2, Y2, X, Y, 1)
        }
        
        return 1
    }
    
    function vector2 getBoardChipPos(X, X) {
        local Pos = getBoardSlotPos(X, Y)
        return Pos + E:up() * (SlotThickness / 2 + ChipThickness / 2)
    }
    
    function table getChip(X, Y)
        for (I = 1, Chips:count()) {
            local V = Chips[I, table]
            if (V["Slot", vector2]:x() == X && V["Slot", vector2]:y() == Y) { return V }
        }
    }
    
    function table getChipCount() {
        local Tbl = table
        for (I = 1, Players:count()) {
            Tbl[I, number] = 0
        }
        
        for (I = 1, Chips:count()) {
            local V = Chips[I, table]
            Tbl[V["Player", number], number] = V["Player", number] + 1
        }
        
        return Tbl
    }
    
    function removeChip(X, Y)
        for (I = 1, Chips:count()) {
            local V = Chips[I, table]
            
            if (V["Slot", vector2]:x() == X && V["Slot", vector2]:y() == Y) {
                local Ent = holoEntity(V["Holo", number])
                V["CaptureAnimTime", number] = 0
                V["CaptureRandAng", angle] = ang(random(0, 45), random(0, 90), random(0, 45))
                ChipsCaptured:pushTable(V)
                
                Chips:remove(I)
                
                SelectedChip = -1
                
                return
            }
        }
    }
    
    function number isLinked(X, Y, X2, Y2) {
        local Slot = getBoardSlot(X, Y)
        if (!Slot || !Slot["Links", table]) { return 0 }
        
        for (I = 1, Slot["Links", table]:count()) {
            local V = Slot["Links", table][I, vector2]
            if (V:x() == X2 && V:y() == Y2) { return 1 }
        }
        
        return 0
    }
    
    function resetMovePointers() {
        for (I = 1, MovePointers:count()) {
            local Index = MovePointers[I, number]
            local Ent = holoEntity(Index)
            if (Ent:isValid()) {
                holoPos(Index, E:pos())
                local Col = MovePointerColor
                holoColor(Index, vec4(Col:x(), Col:y(), Col:z(), 0))
            }
        }
    }
    
    function updateMovePointers(X, Y) {
        resetMovePointers()
        
        local Slot = getBoardSlot(X, Y)
        for (I = 1, Slot["Links", table]) {
            local Index = MovePointers[I, number]
            local V = Slot["Links", table][I, vector2]
            
            local C = getChip(V:x(), V:y())
            if (C) { continue } # Won't work?
            local CanMove = 1
            
            if (AvailableMoves) then
                canMove = false
                for k2, v2 in pairs( availableMoves ) do
                    if v.x == v2.x and v.y == v2.y then
                        canMove = true
                    end
                end
            end
            
            if (CanMove) {
                holoPos(Index, getBoardChipPos(V:x(), V:y()))
                holoColor(Index, MovePointerColor)
            }
        end
    end
    
    local function moveChip( x, y, destX, destY )
        local destChip = getChip( destX, destY )
        if destChip then return false end
        
        if not isLinked( x, y, destX, destY ) then return false end
        
        local chip = getChip( x, y )
        if chip then
            local canMove = true
                                    
            if availableMoves then
                canMove = false
                for k2, v2 in pairs( availableMoves ) do
                    if v2.x == destX and v2.y == destY then
                        canMove = true
                    end
                end
            end
            
            if canMove then
                movingChip = chip
                movingChip.dest = { x = destX, y = destY }
                movingChip.from = { x = x, y = y }
                
                playPlayerSound( nil, "preMove" )
                
                resetMovePointers()
                moveTime = 0
            end
        end
    end
    
    local function takeChips( x, y, dirX, dirY, ply )
        local didTake = false
        local takeCount = 0
        
        local nextX = x + dirX
        local nextY = y + dirY
        local nextChip = getChip( nextX, nextY )
        if nextChip then
            local amt = math.max( boardWidth, boardHeight )
            
            local takeX = x
            local takeY = y
            
            for i = 1, amt do
                takeX = takeX + dirX
                takeY = takeY + dirY
                
                if takeX < 0 or takeY < 0 or takeX > boardWidth - 1 or takeY > boardHeight - 1 then
                    break
                end
                local chip = getChip( takeX, takeY )
                
                if not chip then break end
                
                if chip.player ~= ply then
                    takeCount = takeCount + 1
                    didTake = true
                    removeChip( takeX, takeY )
                else
                    break
                end
            end
        end
                    
        if didTake then
            onChipCaptured()
        end
        
        return didTake, takeCount
    end
    
    local function aimsOnSphere( origin, dir, pos, radius )
        local dist = origin:getDistance( pos )
        local newPos = origin + dir * dist
        
        return newPos:getDistance( pos ) <= radius
    end
    
    local function checkAvailableMoves( x, y, ply )
        local chip = getChip( x, y )
        local available = false
        local moves = {}
        
        local slot = getBoardSlot( chip.slot.x, chip.slot.y )
        if slot and slot.links then
            for i, v in ipairs( slot.links ) do
                local dirX = v.x - x
                local dirY = v.y - y
                
                local newX = v.x + dirX
                local newY = v.y + dirY
                
                local chip = getChip( v.x, v.y )
                if newX >= 0 and newY >= 0 and newX <= boardWidth - 1 or newY <= boardHeight - 1 then
                    local nextChip = getChip( newX, newY )
                    if not chip and nextChip and nextChip.player ~= ply then
                        available = true
                        moves[ #moves + 1 ] = { x = v.x, y = v.y }
                    end
                end
                
                local backChip = getChip( x - dirX, y - dirY )
                if not chip and backChip and backChip.player ~= ply then
                    available = true
                    moves[ #moves + 1 ] = { x = v.x, y = v.y }
                end
            end
        end
        
        return available, moves
    end
    
    local function setTurn( t )
        turn = t
        mustMoveChip = nil            
        selectedChip = nil
        availableMoves = nil
        
        currStreak = 0
        
        onTurnChanged( t )
    end
    
    local function resetChips()
        for i, v in ipairs( chipsCaptured ) do
            v.captureAnimTime = nil
            v.captureRandAng = nil
            
            chips[ #chips + 1 ] = v
        end
        selectedChip = nil
        movingChip = nil
        
        chipsCaptured = {}
    
        local i = 1
        for x = 0, boardWidth - 1 do
            for y = 0, boardHeight - 1 do
                local chip = chips[ i ]
                if chip then
                    local ent = hEnt( chip.holo )
                    
                    local ply = 1
                    
                    if y > (boardHeight - 1) / 2 then
                        ply = 2
                    elseif y == (boardHeight - 1) / 2 then
                        if x > (boardWidth - 1) / 2 then
                            if x % 2 == 0 then ply = 1 else ply = 2 end
                        elseif x < (boardWidth - 1) / 2 then
                            if (x+1) % 2 == 0 then ply = 1 else ply = 2 end
                        else ply = 3 end
                    end
                    
                    if ply == 3 then
                        continue
                    end
                    chip.player = ply
                    chip.slot = { x = x, y = y }
                    
                    ent:setPos( getBoardChipPos( x, y ) )
                    ent:setAngles( Angle( 0, 0, 90 ) )
                    ent:setColor( players[ ply ].color )
                    
                    i = i + 1
                end
            end
        end
    end
    
    holoCreate(1)
    holoModel(1, "models/sprops/cuboids/height06/size_1/cube_6x6x6.mdl")
    holoPos(1, E:pos() + BoardMin + BoardSize / 2 - vec(0, 0, SlotThickness))
    holoMaterial(1, "models/debug/debugwhite")
    holoColor(1, vec(50, 50, 50))
    holoScaleUnits(1, vec(BoardSize:x() * ScaleMult, BoardSize:y() * ScaleMult, 2 * ScaleMult))
    holoDisableShading(1, 1)
    holoParent(1, E)
    
    # Borders
    holoCreate(2)
    holoModel(2, "models/sprops/cuboids/height06/size_1/cube_6x6x6.mdl")
    holoPos(2, E:pos() + BoardMin + vec(BoardSize:x() / 2, BorderThickness / 2, 0))
    holoMaterial(2, "models/debug/debugwhite")
    holoColor(2, vec(20, 20, 20))
    holoScaleUnits(2, vec(BoardSize:x() * ScaleMult, BorderThickness * ScaleMult, 2 * ScaleMult))
    holoDisableShading(2, 1)
    holoParent(2, E)
    
    holoCreate(3)
    holoModel(3, "models/sprops/cuboids/height06/size_1/cube_6x6x6.mdl")
    holoPos(3, E:pos() + BoardMin + vec(BoardSize:x() / 2, BoardSize:y() - (BorderThickness / 2), 0))
    holoMaterial(3, "models/debug/debugwhite")
    holoColor(3, vec(20, 20, 20))
    holoScaleUnits(3, vec(BoardSize:x() * ScaleMult, BorderThickness * ScaleMult, 2 * ScaleMult))
    holoDisableShading(3, 1)
    holoParent(3, E)
    
    holoCreate(4)
    holoModel(4, "models/sprops/cuboids/height06/size_1/cube_6x6x6.mdl")
    holoPos(4, E:pos() + BoardMin + vec(BorderThickness / 2, BoardSize:y() / 2, 0))
    holoMaterial(4, "models/debug/debugwhite" )
    holoColor(4, vec(20, 20, 20))
    holoScaleUnits(4, vec(BorderThickness * ScaleMult, BoardSize:y() * ScaleMult, 2 * ScaleMult))
    holoDisableShading(4, 1)
    holoParent(4, E)
    
    holoCreate(5)
    holoModel(5, "models/sprops/cuboids/height06/size_1/cube_6x6x6.mdl")
    holoPos(5, E:pos() + BoardMin + vec(BoardSize:x() - (BorderThickness / 2), BoardSize:y() / 2, 0))
    holoMaterial(5, "models/debug/debugwhite" )
    holoColor(5, vec(20, 20, 20))
    holoScaleUnits(5, vec(BorderThickness * ScaleMult, BoardSize:y() * ScaleMult, 2 * ScaleMult))
    holoDisableShading(5, 1)
    holoParent(5, E)
    
    for (Y = 0, BoardHeight - 1) {
        for (X = 0, BoardWidth - 1) {
            Board[X + Y * BoardWidth, table] = table()
        }
    }
    
    
    timer("delay", 1000)
}

if (clk("delay")) {
    ID = 5
    for (Y = 0, BoardHeight - 1) {
        for (X = 0, BoardWidth - 1) {
            local Slot = getBoardSlot(X, Y)
            
            local SpacingX = (X * Spacing)
            local SpacingY = (X * Spacing)
            
            ID++
            
            holoCreate(ID)
            holoModel(ID, "models/sprops/geometry/fdisc_12.mdl")
            holoPos(ID, getBoardSlotPos(X, Y))
            
            holoAng(ID, ang(0, 0, 90))
            holoMaterial(ID, "models/debug/debugwhite" )
            holoColor(ID, SlotColor )
            holoScaleUnits(ID, vec(SlotSizeW * 1.035, SlotThickness * 1.2, SlotSizeH * 1.035))
            holoDisableShading(ID, 1)
            holoParent(ID, E)
    
            setBoardHoloID(X, Y, ID)
            
            for (I = 1, Neighbours:count()) {
                local N = Neighbours[I, vector2]
                
                local NewX = X + N:x()
                local NewY = Y + N:y()
                
                if (NewX < 0 || NewY < 0 || NewX >= BoardWidth || NewY >= BoardHeight) {
                    continue
                }
                
                local DidLink = linkBoardSlots(X, Y, NewX, NewY, 0)
                if (DidLink) {
                    
                }
            }
            
            if ((X + Y) % 2 == 0) {
                for (I = 1, XNeighbours:count()) {
                    local N = XNeighbours[I, vector2]
                
                    local NewX = X + N:x()
                    local NewY = Y + N:y()
                    
                    if (NewX < 0 || NewY < 0 || NewX >= BoardWidth || NewY >= BoardHeight) {
                        continue
                    }
                    
                    local DidLink = linkBoardSlots(X, Y, NewX, NewY, 0)
                    if (DidLink) {
                        
                    }
                }
            }
        }
    }
    
    timer("delay2", 1000)
}
elseif (clk("delay2")) {
    # Create X and Y connections
    for (X = 0, BoardWidth - 1) {
        local P1 = getBoardSlotPos(X, 0)
        local P2 = getBoardSlotPos(X, BoardHeight - 1)
        
        ID++
        
        holoCreate(ID)
        holoModel(ID, "models/sprops/cuboids/height06/size_1/cube_6x6x6.mdl")
        holoMaterial(ID, "models/debug/debugwhite")
        holoPos(ID, (P1 + P2) * 0.5)
        holoColor(ID, LinkColor )
        holoScaleUnits(ID, vec(4, P1:distance(P2) * ScaleMult, SlotThickness * 0.5))
        holoDisableShading(ID, 1)
        holoParent(ID, E)
    }
    
    for (Y = 0, BoardHeight - 1) {
        local P1 = getBoardSlotPos(0, Y )
        local P2 = getBoardSlotPos(BoardWidth - 1, Y)
        
        ID++
        
        holoCreate(ID)
        holoModel(ID, "models/sprops/cuboids/height06/size_1/cube_6x6x6.mdl")
        holoMaterial(ID, "models/debug/debugwhite")
        holoPos(ID, (P1 + P2) * 0.5)
        holoColor(ID, LinkColor)
        holoScaleUnits(ID, vec(P1:distance(P2) * ScaleMult, 4, SlotThickness * 0.5))
        holoDisableShading(ID, 1 )
        holoParent(ID, E )
    }
    
    # Create diagonal connections
    for (X = ceil(-BoardHeight / 2), (BoardWidth - 1) / 2) {
        local P2x = (X * 2) + BoardHeight - 1
        local Remainder = 0
        local Remainder2 = 0
        
        if (P2x > BoardWidth - 1) {
            Remainder = (BoardWidth - 1) - P2x
            P2x = BoardWidth - 1
        }
        
        if (X < 0) {
            Remainder2 = -X * 2
            X = 0
        }
        
        local P1 = getBoardSlotPos(P2x, -Remainder)
        local P2 = getBoardSlotPos(X * 2, BoardHeight - 1 - Remainder2)
        
        local P3 = getBoardSlotPos(X * 2, Remainder2)
        local P4 = getBoardSlotPos(P2x, BoardHeight - 1 + Remainder)
        
        if (P1 != P2) {
            ID++
            
            holoCreate(ID)
            holoModel(ID, "models/sprops/cuboids/height06/size_1/cube_6x6x6.mdl")
            holoMaterial(ID, "models/debug/debugwhite")
            holoPos(ID, (P1 + P2) * 0.5)
            holoColor(ID, LinkColor)
            holoScaleUnits(ID, vec(P1:distance(P2) * ScaleMult, 4, SlotThickness * 0.5))
            holoAng(ID, (P2 - P1):normalized():toAngle())
            holoDisableShading(ID, 1)
            holoParent(ID, E)
        }
        
        if (P3 != P4) {
            ID++
            
            holoCreate(ID)
            holoModel(ID, "models/sprops/cuboids/height06/size_1/cube_6x6x6.mdl")
            holoMaterial(ID, "models/debug/debugwhite")
            holoPos(ID, (P3 + P4) * 0.5)
            holoColor(ID, LinkColor)
            holoScaleUnits(ID, vec(P3:distance(P4) * ScaleMult, 4, SlotThickness * 0.5))
            holoAng(ID, (P4 - P3):normalized():toAngle())
            holoDisableShading(ID, 1)
            holoParent(ID, E)
        }
    }
    
    local I = 0
    for (X = 0, BoardWidth - 1) {
        for (Y = 0, BoardHeight - 1) {
            I++
            if (I == BoardWidth * BoardHeight) { continue }
            ID++
            
            local Tbl = table()
            Tbl["Holo", number] = ID
            Chips:pushTable(Tbl)
            
            holoCreate(ID)
            holoModel(ID, "models/sprops/geometry/fdisc_12.mdl")
            holoMaterial(ID, "models/debug/debugwhite")
            holoAng(ID, ang(0, 0, 90))
            holoScaleUnits(ID, vec(ChipSizeW * 1.035, ChipThickness * 1.2, ChipSizeH * 1.035))
            holoDisableShading(ID, 1)
            holoParent(ID, E)
        }
    }
    
    timer("delay3", 1000)
}
elseif (clk("delay3")) {
    for (I = 1, 8) {
        ID++
        MovePointers:pushNumber(ID)
        
        holoCreate(ID)
        holoMaterial(ID, "models/debug/debugwhite")
        holoModel(ID, "models/sprops/misc/pyramids/rsize_1/rpyr_12x6.mdl")
        holoColor(ID, vec(0, 0, 0))
        holoDisableShading(ID, 1)
        holoParent(ID, E)
    }
    
    
    ID++
    holoCreate(ID)
    holoPos(ID, E:pos() + BoardOrigin + vec(-25, BoardSize:y() / 2, 0))
    holoModel(ID, "models/sprops/geometry/hhex_18.mdl")
    holoAng(ID, ang(0, -90, 90))
    holoMaterial(ID, "models/debug/debugwhite")
    holoColor(ID, vec(20, 20, 20))
    holoScaleUnits(ID, vec(120, 2 * 1.158, 60))
    holoParent(ID, E)
    
    ID++
    Spinner[1, number] = ID
    holoCreate(ID)
    holoPos(ID, E:pos() + SpinnerOrigin + vec(0, 0, 15 / 2))
    holoModel(ID, "models/sprops/geometry/hdisc_12.mdl")
    holoAng(ID, ang(0, 0, 0))
    holoMaterial(ID, "models/debug/debugwhite")
    #holoColor(ID, players[1].color)
    holoDisableShading(ID, 1)
    holoScaleUnits(ID, vec(29 * ScaleMult, 30 * ScaleMult, 15 * ScaleMult))
    holoParent(ID, E)
    
    ID++
    Spinner[2, number] = ID
    holoCreate(ID)
    holoPos(ID, E:pos() + SpinnerOrigin + vec(0, 0, -15 / 2))
    holoModel(ID, "models/sprops/geometry/hdisc_12.mdl")
    holoAng(ID, ang(0, 0, 180))
    holoMaterial(ID, "models/debug/debugwhite")
    #holoColor(ID, players[2].color)
    holoDisableShading(ID, 1)
    holoScaleUnits(ID, vec(29 * ScaleMult, 30 * ScaleMult, 15 * ScaleMult))
    holoParent(ID, E)
    
    
    ID++
    holoCreate(ID)
    holoPos(ID, E:pos() + SpinnerOrigin + vec(0, 15, 0))
    holoModel(ID, "models/sprops/geometry/fdisc_12.mdl")
    holoAng(ID, ang(0, 0, 180))
    holoMaterial(ID, "models/debug/debugwhite")
    holoColor(ID, vec(20, 20, 20))
    holoDisableShading(ID, 1 )
    holoScaleUnits(ID, vec(30 * ScaleMult, 2 * ScaleMult, 30 * ScaleMult))
    holoParent(ID, E)
    
    ID++
    holoCreate(ID)
    holoPos(ID, E:pos() + SpinnerOrigin + vec(0, -15, 0))
    holoModel(ID, "models/sprops/geometry/fdisc_12.mdl")
    holoAng(ID, ang(0, 0, 180))
    holoMaterial(ID, "models/debug/debugwhite")
    holoColor(ID, vec(20, 20, 20))
    holoDisableShading(ID, 1)
    holoScaleUnits(ID, vec(30 * ScaleMult, 2 * ScaleMult, 30 * ScaleMult))
    holoParent(ID, E)
    
    FinishedLoading = 1
    
    resetChips()
}
else {
    if (FinishedLoading) {
        
        
        
    }
}
