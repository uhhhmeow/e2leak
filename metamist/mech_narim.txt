@name Mechs/New
@inputs 
@outputs 
@persist [Model Mech]:table Position:array [Self Temp]:entity
@persist Base Generating Loop Index
@model models/hunter/blocks/cube1x1x05.mdl

if (first())
{
    runOnTick(1)
    
    Model = table()
    Mech  = table()
    
    Mech["Name",table] = table()
    
    Self = entity()
    
    Holo  = 1    
    Loop  = 1    
    Index = 1
    
    Generating = 1
    
    function void initModel(Array:array)
    {
        Model:pushArray(Array)
        Holo++
    }
    
    function void createModel(Array:array)
    {
        local Entity = holoEntity(Array[6,number])
        local Vector = vec()
        local Angle  = ang()
        local Alpha  = 255
        
        switch (Array[2,number])
        {
            case 1, Material = "Phoenix_storms/mat/mat_phx_metallic" Colour = vec(50,50,50), break
            case 2, Material = "Phoenix_storms/fender_chrome" Colour = vec(255,255,255), break
            case 3, Colour   = vec(255,0,0), break 
        }
        
        switch (Array[8,string])
        {
            case "vec", Vector = Array[3,vector], Angle = Entity:toWorld(Array[4,angle]), break
            case "+vec", Vector = Entity:pos() + Array[3,vector], Angle = Entity:toWorld(Array[4,angle]), break
            case "ang", Vector = Entity:toWorld(Array[3,vector]), Angle = Array[4,angle], break
            case "vec + ang", Vector = Array[3,vector], Angle = Array[4,angle], break
            case "+vec + ang", Vector = Entity:pos() + Array[3,vector], Angle = Array[4,angle], break
            case "colour", Colour = Array[9,vector]
            case "", Vector = Entity:toWorld(Array[3,vector]), Angle = Entity:toWorld(Array[4,angle]), break
        } 
        
        holoCreate(Array[1,number],Vector,vec(),Angle,Colour)    
        holoScaleUnits(Array[1,number],Array[5,vector])
        holoParent(Array[1,number],Entity)
        holoModel(Array[1,number],Array[7,string])           
        holoMaterial(Array[1,number],Material)  
        holoAlpha(Array[1,number],Alpha) 
    }
    
    for (I = 1, 4)
    {
        holoCreate(I)
        holoAlpha(I, 150)
        
        holoCreate(I + 4)
        holoAlpha(I + 4, 100)
        holoColor(I + 4, vec(0,0,255))
        
        holoCreate(I + 8)
        holoAlpha(I + 8, 100)
        holoColor(I + 8, vec(255,0,0))             
        
        Holo+=3
        
        Position:pushVector(Self:pos())
    }
    
    Temp = holoCreate(Holo,Self:toWorld(vec(0,0,200)))
    Holo++
    
    Base = Holo
    #initModel(array(Holo,1,Self:toWorld(vec(0,0,0)),ang(0,0,0),vec(11,11,27.5),noentity(),"models/mechanics/wheels/wheel_speed_72.mdl","vec"))
    initModel(array(Holo,1,Self:toWorld(vec(0,0,0)),ang(0,0,0),vec(75,75,75),noentity(),"hq_icosphere","vec"))
    
    #initModel(array(Holo,3,Self:toWorld(vec(0,0,0)),ang(0,0,0),vec(5,5,5),noentity(),"hq_icosphere","vec"))
    
    for (I = 1, 4)
    {        
        initModel(array(Holo,1,ang(0,135 - 90 * I,0):forward() * 25,ang(90,135 - 90 * I,0),vec(8.75,10,10),Base,"models/props_interiors/Furniture_Couch02a.mdl","+vec"))
        
        # Joints
        local Part_1 = Holo 
        Mech["Name",table]["Leg " + I + ".1",number] = Holo
        initModel(array(Holo,1,ang(0,135 - 90 * I,0):forward() * 35.355,ang(0,0,0),vec(25,25,25),Base,"hq_icosphere","+vec + ang"))
        #initModel(array(Holo,3,ang(0,135 - 90 * I,0):forward() * 35.355,ang(0,0,0),vec(5,5,5),Base,"","+vec + ang"))  
               
        local Part_2 = Holo 
        Mech["Name",table]["Leg " + I + ".2",number] = Holo
        initModel(array(Holo,3,vec(-75,0,0),ang(0,0,0),vec(5,5,5),Part_1,"","ang"))      
         
        local Part_3 = Holo 
        Mech["Name",table]["Leg " + I + ".3",number] = Holo
        initModel(array(Holo,3,vec(0,-75,0),ang(0,0,0),vec(5,5,5),Part_2,"","ang"))
        
        initModel(array(Holo,3,vec(0,75,0),ang(0,0,0),vec(5,5,5),Part_3,""))
        
        ##[ Legs
        initModel(array(Holo,1,vec(-37.5,0,0),ang(0,-90,0),vec(75,100,100),Part_1,"models/props_c17/TrapPropeller_Lever.mdl"))
   
        initModel(array(Holo,1,vec(-37.5,0,0),ang(0,90,0),vec(75,100,100),Part_1,"models/props_c17/TrapPropeller_Lever.mdl")) 
 
        initModel(array(Holo,2,vec(-50,0,0),ang(90,0,0),vec(8,8,6.67),Part_1,"models/props_c17/oildrum001.mdl")) 
 
        initModel(array(Holo,1,vec(-75,0,0),ang(0,90,0),vec(6.25,7.5,6.25),Part_1,"models/NatesWheel/nateswheelwide.mdl")) 

        initModel(array(Holo,2,vec(-75,1,0),ang(0,90,0),vec(4,8,4),Part_1,"models/XQM/airplanewheel1big.mdl")) 

        initModel(array(Holo,1,vec(0,-37.5,0),ang(0,0,0),vec(75,100,100),Part_2,"models/props_c17/TrapPropeller_Lever.mdl")) 

        initModel(array(Holo,1,vec(0,-37.5,0),ang(0,180,0),vec(75,100,100),Part_2,"models/props_c17/TrapPropeller_Lever.mdl")) 

        initModel(array(Holo,2,vec(0,-50,0),ang(0,0,-90),vec(8,8,6.67),Part_2,"models/props_c17/oildrum001.mdl")) 

        initModel(array(Holo,1,vec(0,-75,0),ang(0,0,0),vec(6.25,7.5,6.25),Part_2,"models/NatesWheel/nateswheelwide.mdl")) 

        initModel(array(Holo,2,vec(1,-75,0),ang(0,0,0),vec(4,8,4),Part_2,"models/XQM/airplanewheel1big.mdl")) 

        initModel(array(Holo,1,vec(0,30,0),ang(0,0,180),vec(50,100,100),Part_3,"models/props_c17/TrapPropeller_Lever.mdl")) 
        
        initModel(array(Holo,2,vec(0,46.7,0),ang(0,0,90),vec(8,8,6.67),Part_3,"models/props_c17/oildrum001.mdl")) 
        
        initModel(array(Holo,1,vec(0,46.7,0),ang(0,90,90),vec(25,25,30),Part_3,"models/props_lab/tpplug.mdl")) 
        #]#
        
        #initModel(array(Holo,1,vec(-37.5,0,0),ang(90,0,0),vec(15,15,15),Part_1,"models/props_c17/canister01a.mdl"))
    }
}

if (Generating)
{            
    if (Loop > Model:count())
    {                                      
        Generating = 0 
        
        #holoParent(Base,Self) 
        #Self:parentTo(holoEntity(Base))
            
        print(Model:count()) 
    }
            
    while (Loop <= Model:count() & holoCanCreate() & perf())
    {
        createModel(Model[Loop,array])
        Loop++
    }
}
else
{
    
    ID = holoIndex(Temp)
    #holoPos(ID,holoEntity(ID):toWorld(vec(2,0,0)))
    #holoAng(ID,holoEntity(ID):toWorld(ang(0,0.75,0)))
    
    for (I = 1, 4){ 
        
        local Vector = (holoEntity(I + 4):pos() - holoEntity(I + 8):pos())
        Position[I,vector] = Vector:normalized() * min(Vector:length(), 5) * (holoEntity(I + 8):pos():distance(holoEntity(I + 4):pos()) / 30)
        
        holoPos(I + 8, holoEntity(I + 8):pos() + Position[I,vector])
        
        local Height = holoEntity(I + 4):pos():distance(holoEntity(I + 8):pos())        
        local Height = (-(Height ^ 2) + 75 * Height) / 40
                      
        local HL = (holoEntity(Mech["Name",table]["Leg " + I + ".1",number]):pos() - (holoEntity(I + 8):pos() + clamp(Height,0,25))):length()
        local HA = (holoEntity(Mech["Name",table]["Leg " + I + ".1",number]):pos() - (holoEntity(I + 8):pos() + clamp(Height,0,25))):toAngle()
        
        local Temp_Angle = (HL - 75) / 2            
        local Angle_One  = acos(Temp_Angle / 75)
        local Angle_Two  = 180 - Angle_One
            
        holoAng(Mech["Name",table]["Leg " + I + ".1",number],holoEntity(Base):toLocal(holoEntity(Base):toWorld(ang(Angle_One + HA:pitch(),HA:yaw(),0))))
            
        holoAng(Mech["Name",table]["Leg " + I + ".2",number],holoEntity(Mech["Name",table]["Leg " + I + ".1",number]):toWorld(ang(0,90,Angle_Two)))
            
        holoAng(Mech["Name",table]["Leg " + I + ".3",number],holoEntity(Mech["Name",table]["Leg " + I + ".2",number]):toWorld(ang(0,0,Angle_Two)))
        
        
        local Ranger = rangerOffset(1000, Temp:toWorld(ang(0,135 - 90 * I,0):forward() * 175), vec(0,0,-1))
        
        holoPos(I, Ranger:pos())
        
        if (Index == 1 & I == 1)
        {
            if (holoEntity(I + 4):pos():distance(Ranger:pos()) > 75)
            {
                holoPos(I + 4, Ranger:pos())
                timer("Index", 50)
            }
        }
        elseif (Index == 2 & I == 3)
        {
            if (holoEntity(I + 4):pos():distance(Ranger:pos()) > 75)
            {
                holoPos(I + 4, Ranger:pos())
                timer("Index", 50)
            }
        }
        elseif (Index == 3 & I == 2)
        {
            if (holoEntity(I + 4):pos():distance(Ranger:pos()) > 75)
            {
                holoPos(I + 4, Ranger:pos())
                timer("Index", 50)
            }
        }
        elseif (Index == 4 & I == 4)
        {
            if (holoEntity(I + 4):pos():distance(Ranger:pos()) > 75)
            {
                holoPos(I + 4, Ranger:pos())
                timer("Index", 50)
            }
        }
    }
    
    local Toe_One   = holoEntity(9):pos()
    local Toe_Two   = holoEntity(10):pos()
    local Toe_Three = holoEntity(11):pos()
    local Toe_Four  = holoEntity(12):pos()
            
    local Base_Vector = (Toe_One + Toe_Two + Toe_Four + Toe_Three) / 4
        
    holoPos(Base,Base_Vector:setZ(Base_Vector:z() + 40))
        
    local Pitch_Front = ((Toe_One + Toe_Two) / 2)
    local Pitch_Back  = ((Toe_Four + Toe_Three) / 2)   
        
    local Pitch = (Pitch_Front - Pitch_Back):toAngle()
        
    local Roll_Right = ((Toe_Two + Toe_Three) / 2)
    local Roll_Left  = ((Toe_One + Toe_Four) / 2)
        
    local Roll = (Roll_Right - Roll_Left):toAngle()
        
    holoAng(Base,ang(Pitch:pitch(),Pitch:yaw(),Roll:pitch()))  
    
    if (clk("Index"))
    {
        if (Index == 4)
        {
            Index = 1
        }
        else
        {
            Index++
        }
    }
    
    if (owner():keyUse()) {
        holoPos(ID, Temp:pos() + (owner():pos() - Temp:pos()):normalized() * 14)
    }
}
