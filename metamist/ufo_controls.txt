@name 
@inputs [Pod Cam]:wirelink
@outputs TractorBeam
@persist E:entity Pos:vector Direction:vector OldMouse:angle CamAng:angle CamPos:vector CamDir:vector CamDirUp:vector CamDirRight:vector
@persist Forward Strafe Vert MaxSpeed R
@trigger 
@model models/hunter/tubes/circle4x4.mdl 

if(first()) {
    function vector smoothStep (Min:vector, Max:vector, X) {
        T = clamp(X, 0, 1)
        Num = (T^3) * (T * (T * 6 - 15) + 10)
        return Num * (Max - Min) + Min
    }
    
    function angle smoothStep (Min:angle, Max:angle, X) {
        T = clamp(X, 0, 1)
        Num = (T^3) * (T * (T * 6 - 15) + 10)
        return Num * (Max - Min) + Min
    }
    
    runOnTick(1)
    
    E = entity()
    Pos = E:pos()
    MaxSpeed = 50
    
    E:setMass(2500)
    
    holoCreate(0)
    holoPos(0, E:toWorld(vec(0,0,107.5)))
    holoAng(0, E:angles())
    holoAlpha(0, 0)
    holoParent(0, E)
    
    holoCreate(-1)
    holoModel(-1, "models/Slyfo/biodome2.mdl")
    holoScale(-1, vec(0.6, 0.6, 0.6))
    holoPos(-1, holoEntity(0):toWorld(vec(0,0,-40)))
    holoAng(-1, holoEntity(0):angles())
    holoClipEnabled(-1, 1)
    holoMaterial(-1, "models/shiny")
    holoColor(-1, vec(70, 0, 0))
    holoClip(-1, vec(0,0,0), vec(0, -1, 0), 0)
    holoParent(-1, holoEntity(0))
    
    holoCreate(-2)
    holoModel(-2, "models/Slyfo/biodome2.mdl")
    holoScale(-2, vec(0.6, 0.6, 0.6))
    holoPos(-2, holoEntity(0):toWorld(vec(0,0,-40)))
    holoAng(-2, holoEntity(0):toWorld(ang(0,180,0)))
    holoClipEnabled(-2, 1)
    holoMaterial(-2, "models/shiny")
    holoColor(-2, vec(70, 0, 0))
    holoClip(-2, vec(0,0,0), vec(0, -1, 0), 0)
    holoParent(-2, holoEntity(0))
    
    holoCreate(-3)
    holoModel(-3, "hq_tube_thin")
    holoMaterial(-3, "models/gibs/metalgibs/metal_gibs")
    holoScale(-3, vec(45,45,2))
    holoPos(-3, holoEntity(0):toWorld(vec(0,0,-120)))
    holoAng(-3, holoEntity(0):angles())
    holoParent(-3, holoEntity(0))
    
    holoCreate(-4)
    holoModel(-4, "models/mechanics/wheels/wheel_spike_48.mdl")
    holoMaterial(-4, "models/gibs/metalgibs/metal_gibs")
    holoScale(-4, vec(12,12,3))
    holoPos(-4, holoEntity(0):toWorld(vec(0,0,-127.5)))
    holoAng(-4, holoEntity(0):toWorld(ang(0,0,0)))
    holoParent(-4, holoEntity(0))
    
    
    
    holoCreate(-5)
    holoModel(-5, "models/boba_fett/catwalk_build/gate_platform.mdl")
    holoMaterial(-5, "models/gibs/metalgibs/metal_gibs")
    holoScale(-5, vec(0.5,0.5,1))
    holoPos(-5, holoEntity(0):toWorld(vec(0,0,-120)))
    holoAng(-5, holoEntity(0):toWorld(ang(90, 0, 0)))
    holoClipEnabled(-5, 1)
    holoClip(-5, vec(0,0,0), vec(-1, 0, 0), 0)
    holoParent(-5, holoEntity(0))
    
    holoCreate(-6)
    holoModel(-6, "models/boba_fett/catwalk_build/gate_platform.mdl")
    holoMaterial(-6, "models/gibs/metalgibs/metal_gibs")
    holoScale(-6, vec(0.5,0.5,1))
    holoPos(-6, holoEntity(0):toWorld(vec(0,0,-120)))
    holoAng(-6, holoEntity(0):toWorld(ang(-90,0,0)))
    holoClipEnabled(-6, 1)
    holoClip(-6, vec(0,0,0), vec(1, 0, 0), 0)
    holoParent(-6, holoEntity(0))
    
    holoCreate(-7)
    holoModel(-7, "models/boba_fett/catwalk_build/gate_platform.mdl")
    holoMaterial(-7, "models/gibs/metalgibs/metal_gibs")
    holoScale(-7, vec(0.5,0.5,1))
    holoPos(-7, holoEntity(0):toWorld(vec(0,0,-120)))
    holoAng(-7, holoEntity(0):toWorld(ang(90, 0, 90)))
    holoClipEnabled(-7, 1)
    holoClip(-7, vec(0,0,0), vec(-1, 0, 0), 0)
    holoParent(-7, holoEntity(0))
    
    holoCreate(-8)
    holoModel(-8, "models/boba_fett/catwalk_build/gate_platform.mdl")
    holoMaterial(-8, "models/gibs/metalgibs/metal_gibs")
    holoScale(-8, vec(0.5,0.5,1))
    holoPos(-8, holoEntity(0):toWorld(vec(0,0,-120)))
    holoAng(-8, holoEntity(0):toWorld(ang(-90,0,-90)))
    holoClipEnabled(-8, 1)
    holoClip(-8, vec(0,0,0), vec(1, 0, 0), 0)
    holoParent(-8, holoEntity(0))
    
    holoCreate(-9)
    holoModel(-9, "models/hunter/tubes/tube4x4x1to2x2.mdl")
    holoMaterial(-9, "models/gibs/metalgibs/metal_gibs")
    holoScale(-9, vec(2,2,1.5))
    holoPos(-9, holoEntity(0):toWorld(vec(0,0,-220)))
    holoAng(-9, holoEntity(0):toWorld(ang(180,0,0)))
    holoParent(-9, holoEntity(0))
    
    holoCreate(-10)
    holoModel(-10, "models/xeon133/offroad/off-road-80.mdl")
    holoMaterial(-10, "models/gibs/metalgibs/metal_gibs")
    holoScale(-10, vec(6,1,6))
    holoPos(-10, holoEntity(0):toWorld(vec(0,0,-140)))
    holoAng(-10, holoEntity(0):toWorld(ang(90,0,0)))
    holoParent(-10, holoEntity(0))
    
    #holoCreate(-11)
    #holoModel(-11, "models/xeon133/offroad/off-road-80.mdl")
    #holoMaterial(-11, "models/gibs/metalgibs/metal_gibs")
    #holoScale(-11, vec(10,1,10))
    #holoPos(-11, Prop:toWorld(vec(0,0,-140)))
    #holoAng(-11, Prop:toWorld(ang(90,0,0)))
    #holoParent(-11, Prop)
    
    for(I = 0, 8) {
        holoCreate(-11 - I)
        holoModel(-11 - I, "models/boba_fett/p90/attachments/kull-zapper.mdl")
        #holoMaterial(-11, "models/gibs/metalgibs/metal_gibs")
        holoColor(-11 - I, vec(230,230,230))
        holoScale(-11 - I, vec(40,40,45))
        holoPos(-11 - I, holoEntity(0):toWorld(vec(sin((I/8)*360)*220,cos((I/8)*360)*220,-165)))
        Yaw = (I % 4 == 1 ? 90 : (I % 4 == 2 ? 0 : (I % 4 == 3 ? -90 : 180))) + I * (360 / 8)
        holoAng(-11 - I, holoEntity(0):toWorld(ang(200,90 + Yaw,0)))
        holoClipEnabled(-11 - I, 1)
        holoClip(-11 - I, vec(0,0,0) + vec(1, 0, 0) * 70, vec(-1, 0, 0), 0)
        holoParent(-11 - I, holoEntity(0))
    }
    
    
    holoCreate(-20)
    holoModel(-20, "models/XQM/Rails/trackball_1.mdl")
    holoMaterial(-20, "models/gibs/metalgibs/metal_gibs")
    holoScale(-20, vec(19,19,2))
    holoPos(-20, holoEntity(0):toWorld(vec(0,0,-95)))
    holoAng(-20, holoEntity(0):toWorld(ang(0,0,0)))
    holoParent(-20, holoEntity(0))
    
    holoCreate(-21)
    holoModel(-21, "models/boba_fett/props/sodan_ring.mdl")
    holoMaterial(-21, "models/gibs/metalgibs/metal_gibs")
    holoScale(-21, vec(1.5,1.5,1))
    holoPos(-21, holoEntity(0):toWorld(vec(0,0,-95)))
    holoAng(-21, holoEntity(0):toWorld(ang(0,0,0)))
    holoParent(-21, holoEntity(0))
    
    holoCreate(-22)
    holoModel(-22, "models/boba_fett/rings/ring_platform.mdl")
    holoMaterial(-22, "models/gibs/metalgibs/metal_gibs")
    holoScale(-22, vec(1.5,1.5,0.2))
    holoPos(-22, holoEntity(0):toWorld(vec(0,0,-90)))
    holoAng(-22, holoEntity(0):toWorld(ang(0,0,0)))
    holoParent(-22, holoEntity(0))
    
    holoCreate(-23)
    holoModel(-23, "models/madman07/ancient_rings/ring.mdl")
    holoMaterial(-23, "models/gibs/metalgibs/metal_gibs")
    holoScale(-23, vec(3,3,1))
    holoPos(-23, holoEntity(0):toWorld(vec(0,0,-170)))
    holoAng(-23, holoEntity(0):toWorld(ang(0,0,0)))
    holoParent(-23, holoEntity(0))
    
    for(I = 0, 3) {
        holoCreate(1+I)
        holoMaterial(1+I, "spacebuild/fusion4")
        if(I == 3) {
            holoModel(1+I, "hq_hdome")
            holoScale(1+I, vec(8.8,8.8,0.5))
            holoAng(1+I, holoEntity(0):toWorld(ang(0,0,0)))
        } else {
            holoModel(1+I, "models/props_vehicles/tire001c_car.mdl")
            holoScale(1+I, vec(7.4 - (I * 2.2), 2, 7.4 - (I * 2.2)))
            holoAng(1+I, holoEntity(0):toWorld(ang(90,0,0)))
        }
        holoColor(1+I, vec(255,0,0))
        holoPos(1+I, holoEntity(0):toWorld(vec(0,0,-200 + I * 5)))
        holoParent(1+I, holoEntity(0))
    }
}

if(duped() || dupefinished()) { reset() }

Active = Pod["Active", normal]

if(Active) {
    W = Pod["W", normal]
    A = Pod["A", normal]
    S = Pod["S", normal]
    D = Pod["D", normal]
    Alt = Pod["Alt", normal]
    Space = Pod["Space", normal]
    R = Pod["R", normal]
    
    Driver = Pod["Entity", entity]:driver()
    
    CamAng += ((E:angles() - Driver:eyeAngles()) - OldMouse)
    CamDir      = vec(1, 0, 0):rotate(-CamAng)
    CamDirRight = vec(0, 1, 0):rotate(-CamAng)
    CamDirUp    = vec(0, 0, 1):rotate(-CamAng)
    
    if(W) {
        if(Forward < MaxSpeed) { Forward += 2 }
        if(Forward > MaxSpeed) { Forward -= 2 }
    }
    elseif(S) {
        if(Forward > -MaxSpeed) { Forward -= 2 }
        if(Forward < -MaxSpeed) { Forward += 2 }
    }
    
    if(A) {
        if(Strafe < MaxSpeed) { Strafe += 2 }
        if(Strafe > MaxSpeed) { Strafe -= 2 }
    }
    elseif(D) {
        if(Strafe > -MaxSpeed) { Strafe -= 2 }
        if(Strafe < -MaxSpeed) { Strafe += 2 }
    }
    
    if(Space) {
        if(Vert < MaxSpeed) { Vert += 2 }
        if(Vert > MaxSpeed) { Vert -= 2 }
    }
    elseif(Alt) {
        if(Vert > -MaxSpeed) { Vert -= 2 }
        if(Vert < -MaxSpeed) { Vert += 2 }
    }
    
    
    if(R & $R) { TractorBeam = !TractorBeam }
    
    Cam["Activated", normal] = Active
    CamPos = smoothStep(CamPos, E:pos() - (CamDir) * 1000, 0.3)
    Cam["Position", vector] = CamPos
    
    Cam["Angle", angle] = -CamAng
    OldMouse = E:angles() - Driver:eyeAngles()
} else { if(TractorBeam == 1) { TractorBeam = 0 } }


if(!W & !S) {
    Forward -= Forward / 2
}
if(!A & !D) {
    Strafe -= Strafe / 2
}
if(!Space & !Alt) {
    Vert -= Vert / 2
}

holoAng(1, holoEntity(0):toWorld(ang(90,curtime()*-400,0)))
holoAng(2, holoEntity(0):toWorld(ang(90,curtime()*400,0)))
holoAng(3, holoEntity(0):toWorld(ang(90,curtime()*-400,0)))
holoAng(4, holoEntity(0):toWorld(ang(0,curtime()*400,0)))

if(E:isPlayerHolding()) { Pos = E:pos() }

Pos += (CamDir * Forward) + (CamDirRight * Strafe) + (CamDirUp * (Vert / 2))

Vel = (-E:vel() * E:mass()) / 10
#E:applyForce(((-E:pos() + Pos) - E:vel():setX(E:vel():x() / 10):setY(E:vel():y() / 10)) * E:mass())
E:applyForce(((-E:pos() + Pos) * 2 - E:vel()) * E:mass())

#Pos += (owner():pos()-E:pos()):normalized()

TarAng = ang(0, E:angles():yaw() + 15, 0)
E:applyAngForce(((TarAng - E:angles()) * 2 - E:angVel()) * E:mass()*120)

