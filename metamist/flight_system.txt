@name 
@inputs Pod:wirelink Cam:wirelink
@outputs 
@persist E:entity Pos:vector TarAng:angle Forward Strafe Vert MaxSpeed SpeedInterval
@persist CamAng:angle [CamDirUp CamDirRight CamDir CamPos]:vector OldMouse:angle
@trigger 

interval(50)

if(first()) {
    function vector smoothStep (Min:vector, Max:vector, X) {
        T = clamp(X, 0, 1)
        Num = (T^3) * (T * (T * 6 - 15) + 10)
        return Num * (Max - Min) + Min
    }
    
    function angle smoothStep (Min:angle, Max:angle, X) {
        T = clamp(X, 0, 1)
        Num = (T^3) * (T * (T * 6 - 15) + 10)
        return Num * (Max - Min) + Min
    }
    
    E = entity()
    E:setMass(2500)
    
    MaxSpeed = 180
    SpeedInterval = 8
    Pos = E:pos()
}

if(E:isPlayerHolding()) { Pos = E:pos() }

Active = Pod["Active", normal]

if(Active) {
    W = Pod["W", normal]
    A = Pod["A", normal]
    S = Pod["S", normal]
    D = Pod["D", normal]
    Alt = Pod["Alt", normal]
    Space = Pod["Space", normal]
    R = Pod["R", normal]
    
    Driver = Pod["Entity", entity]:driver()
    
    CamAng += ((E:angles() - Driver:eyeAngles()) - OldMouse)
    CamDir      = vec(1, 0, 0):rotate(-CamAng)
    CamDirRight = vec(0, 1, 0):rotate(-CamAng)
    CamDirUp    = vec(0, 0, 1):rotate(-CamAng)
    
    if(W) {
        if(Forward < MaxSpeed) { Forward += SpeedInterval }
        if(Forward > MaxSpeed) { Forward -= SpeedInterval }
    }
    elseif(S) {
        if(Forward > -MaxSpeed) { Forward -= SpeedInterval }
        if(Forward < -MaxSpeed) { Forward += SpeedInterval }
    }
    
    if(A) {
        if(Strafe < MaxSpeed) { Strafe += SpeedInterval }
        if(Strafe > MaxSpeed) { Strafe -= SpeedInterval }
    }
    elseif(D) {
        if(Strafe > -MaxSpeed) { Strafe -= SpeedInterval }
        if(Strafe < -MaxSpeed) { Strafe += SpeedInterval }
    }
    
    if(Space) {
        if(Vert < MaxSpeed) { Vert += SpeedInterval }
        if(Vert > MaxSpeed) { Vert -= SpeedInterval }
    }
    elseif(Alt) {
        if(Vert > -MaxSpeed) { Vert -= SpeedInterval }
        if(Vert < -MaxSpeed) { Vert += SpeedInterval }
    }
    
    
    #if(R & $R) { TractorBeam = !TractorBeam }
    
    Cam["Activated", normal] = Active
    CamPos = smoothStep(CamPos, E:pos() - (CamDir) * 1000, 0.4)
    Cam["Position", vector] = CamPos
    
    Cam["Angle", angle] = -CamAng
    OldMouse = E:angles() - Driver:eyeAngles()
}
if(!W & !S) {
    Forward -= Forward / 2
}
if(!A & !D) {
    Strafe -= Strafe / 2
}
if(!Space & !Alt) {
    Vert -= Vert / 2
}

Pos += (CamDir * Forward) + (CamDirRight * Strafe) + (CamDirUp * Vert)
E:applyForce(((Pos - E:pos()) * 2 - E:vel()) * E:mass())

TarAng = ang(0,E:angles():yaw() + 20,0)
E:applyAngForce(((TarAng - E:angles()) * 140 - E:angVel() * 120) * E:mass())
