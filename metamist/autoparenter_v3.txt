###########################################################################################################
###########################  AutoParenter created by override[GER]  #######################################
###########################################################################################################
@name AutoParenter_v3
@inputs 
@outputs 
@persist [EParent EProp]:entity [Ents CEnts Colors]:array [State]
@trigger 
interval(100)

STATE_NOTHING       = 0
STATE_SELECT        = 1
STATE_RESETCOLORS   = 2


if ( first() )
{
    runOnChat(1)
    runOnLast(1)
    
    EParent = entity():isWeldedTo()
    Ents = entity():getConstraints()
    
    # Filter entities
    for (IE = 1, Ents:count())
    {
        Remove = 0
        Class = Ents[IE, entity]:type()
        if  (Class == "prop_vehicle_prisoner_pod") { Remove = 1 }
        #elseif      (Class == "gmod_wire_expression2") { Remove = 1 }
        
        if (Remove)
        {
            Ents:remove(IE)
            IE -= 1
        }
    }
    
    # Get entity colors
    CEnts = Ents:clone()
    for (IE = 1, CEnts:count())
    {
        Colors:pushVector4( CEnts[IE, entity]:getColor4() )
    }
    
    # Set entity colors for selection
    for (IE = 1, CEnts:count())
    {
        CEnts[IE, entity]:setColor( vec4(255, 0, 0, 235) )
    }
    
    # Show Options
    print("Press E on a prop to deselect/reselect it.\nCommands:\n\
           .parent      = parent props\
           .unparent    = unparent props\
           .stop        = stop process", "")

    State = STATE_SELECT
}
elseif ((State == STATE_SELECT) && (owner():keyUse()))
{
    EProp = owner():aimEntity()
    if (EProp == EParent) { print("Cant remove parent prop!", "") exit() }
    
    for (IE = 1, Ents:count())
    {
        if (Ents[IE, entity] == EProp)
        {
            CEnts[IE, entity]:setColor( Colors[IE, vector4] )
            
            #Ents[IE, entity] = noentity() # Doesn't work, needs the dirty workaround below...
            Ents:remove(IE)
            Ents:insertEntity(IE, noentity())
            
            exit()
        }
    }
    
    for (IE = 1, CEnts:count())
    {
        if (CEnts[IE, entity] == EProp)
        {
            EProp:setColor( vec4(255, 0, 0, 235) )
            Ents[IE, entity] = EProp
        }
    }
}
elseif (chatClk( owner() ))
{
    LastSaid = owner():lastSaid():lower()
    if (LastSaid == ".parent")
    {
        ParentedProps = 0
        print("Parenting started on", EParent)
        
        for (IE = 1, Ents:count())
        {
            if (Ents[IE, entity] != noentity())
            {
                if (Ents[IE, entity] != EParent)
                {
                    Ents[IE, entity]:deparent()
                    Ents[IE, entity]:parentTo(EParent)
                    ParentedProps += 1
                }
            }
        }
        
        print("Successfully parented props:", ParentedProps)
        State = STATE_RESETCOLORS
    }
    elseif (LastSaid == ".unparent")
    {
        ParentedProps = 0
        print("Unparenting started...", "")
        
        for (IE = 1, Ents:count())
        {
            if (Ents[IE, entity] != noentity())
            {
                if (Ents[IE, entity]:parent() != noentity())
                {
                    Ents[IE,entity]:propFreeze(1)
                    Ents[IE, entity]:deparent()
                    ParentedProps += 1
                }
            }
        }
        
        print("Successfully unparented props:", ParentedProps)
        State = STATE_RESETCOLORS
    }
    elseif (LastSaid == ".stop")
    {
        State = STATE_RESETCOLORS
    }
    
    if (State == STATE_RESETCOLORS)
    {
        hideChat(1)
    }
}
elseif ((State == STATE_RESETCOLORS) || (last()))
{
    # Reset entity colors
    for (IE = 1, CEnts:count())
    {
        CEnts[IE, entity]:setColor( Colors[IE, vector4] )
    }
    
    selfDestruct()
    State = STATE_NOTHING
}


