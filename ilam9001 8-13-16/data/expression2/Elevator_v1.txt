@name Elevator
@inputs Ent:entity Prop:entity
@persist Pos:vector Ang:angle X Y Z Diff Ply:entity
@trigger all

if (first()|duped()) {
    runOnTick(1)
    runOnChat(1)
    Prop:setMass(50000) #<- I can't remember but I think the prop was supposed to weigh 50k. If it freaks out or doesn't work because of this, just remove this line.
    X = Y = 0
}

if (chatClk()) {
    if (lastSpoke():pos():distance(Prop:massCenter()) < 50) {
        Msg = lastSaid():lower():explode(" ")
        if (Msg:string(1) == ".floor") {
            X = Msg:string(2):toNumber()
            if (X<0) {X = 0} #<- Also added this to prevent if from trying to dig into the ground if some noob tries to go to a negative floor
        }
    }
}

Y = X * 105

if (Z != Y) {
    Diff = sign(round(Y-Z))
    Z+=Diff*1.5
}

if (tickClk()) {
    Pos = (Ent:massCenter():setZ(Z+10) - Prop:massCenter()) * 2000
    Ang = Prop:angles() * -10000
    Prop:applyForce(Pos+$Pos*10)
    Prop:applyAngForce(Ang*3+$Ang*30)
}
