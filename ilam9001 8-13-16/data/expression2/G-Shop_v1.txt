@name G-Shop v1
@inputs EGP:wirelink User:entity Ranger1:entity Ranger2:entity Ranger3:entity Ranger4:entity 
@outputs User1 User2 User3 User4
@persist Color:vector LoadingAngle Version:string
@trigger 
interval(100)






if(first()) {

Version = "V1"

}


if(first()) {
#Functions

function start() {
    EGP:egpClear()
    EGP:egpBox(1,vec2(256,256),vec2(512,512))
    EGP:egpAlpha(1,100)
    EGP:egpMaterial(1,"gui/gradient_down")   
    EGP:egpBox(4,vec2(256,256),vec2(512,100))
    EGP:egpAlpha(4,100)
    EGP:egpColor(1,vec(75,75,75))
    EGP:egpColor(4,vec(54,56,56))
    EGP:egpText(5,"G-Shop " + Version + "",vec2(170,236))
    EGP:egpSize(5,40)
    EGP:egpText(6,"Press [USE] anywhere to begin",vec2(140,280))
}
function load() {
    LoadingAngle = LoadingAngle + 5 
    EGP:egpRemove(4)
    EGP:egpRemove(5)
    EGP:egpRemove(6)
    EGP:egpBox(2,vec2(256,256),vec2(256,256))  
    EGP:egpAngle(2,vec2(256,256),vec2(0,0),LoadingAngle) 
    EGP:egpMaterial(2,"gui/gmod_logo")
}

function mainmenu() {
   EGP:egpClear()
   EGP:egpBox(8,vec2(256,256),vec2(512,512))
   EGP:egpColor(8,vec(90,90,90))
   EGP:egpBox(7,vec2(256,300),vec2(512,450)) 
   EGP:egpColor(7,vec(30,30,30))
   EGP:egpAlpha(8,100)
   EGP:egpRoundedBox(9,vec2(70,70),vec2(80,30))
   EGP:egpColor(9,vec(30,30,30)) 
   EGP:egpRoundedBox(10,vec2(200,70),vec2(80,30))
   EGP:egpColor(10,vec(30,30,30))
   EGP:egpRoundedBox(11,vec2(330,70),vec2(80,30))
   EGP:egpColor(11,vec(30,30,30))
   EGP:egpRoundedBox(12,vec2(460,70),vec2(80,30))
   EGP:egpColor(12,vec(30,30,30))
   EGP:egpText(13,"Menu",vec2(230,5))
   EGP:egpSize(13,30)
   EGP:egpText(14,"Guns",vec2(50,57))
   EGP:egpText(15,"Info",vec2(185,57))
   EGP:egpText(16,"Donate",vec2(305,57))
   EGP:egpText(17,"Stats",vec2(440,57))
}

function guns() {
    
    EGP:egpRemove(31)
    EGP:egpRemove(32)
    EGP:egpRemove(33)
    EGP:egpRemove(34)
    EGP:egpRemove(35)
    EGP:egpRemove(36)
    EGP:egpRemove(37)   
    EGP:egpRemove(36)
    EGP:egpRemove(37)
    EGP:egpRemove(38)
    EGP:egpRemove(39)
    EGP:egpRemove(40)     
EGP:egpRoundedBox(31,vec2(256,480),vec2(200,50))
EGP:egpAlpha(31,100)
EGP:egpText(32,"Next page ->",vec2(210,470))
EGP:egpColor(31,vec(75,75,75))

if(Ranger1:shipmentName() == "M3 Super 90") {  
    EGP:egpBox(18,vec2(100,130),vec2(170,100))      
    EGP:egpMaterial(18,"vgui/gfx/vgui/m3")
    EGP:egpRoundedBox(19,vec2(100,200),vec2(170,50))
    EGP:egpAlpha(19,100)
    EGP:egpColor(19,vec(125,125,125))
    EGP:egpText(20,"Buy M3 Super 90",vec2(35,190))
}

elseif(Ranger2:shipmentName() == "M3 Super 90") {  
    EGP:egpBox(18,vec2(100,130),vec2(170,100))      
    EGP:egpMaterial(18,"vgui/gfx/vgui/m3")
    EGP:egpRoundedBox(19,vec2(100,200),vec2(170,50))
    EGP:egpAlpha(19,100)
    EGP:egpColor(19,vec(125,125,125))
    EGP:egpText(20,"Buy M3 Super 90",vec2(35,190))
}

elseif(Ranger3:shipmentName() == "M3 Super 90") {  
    EGP:egpBox(18,vec2(100,130),vec2(170,100))      
    EGP:egpMaterial(18,"vgui/gfx/vgui/m3")
    EGP:egpRoundedBox(19,vec2(100,200),vec2(170,50))
    EGP:egpAlpha(19,100)
    EGP:egpColor(19,vec(125,125,125))
    EGP:egpText(20,"Buy M3 Super 90",vec2(35,190))
}

elseif(Ranger4:shipmentName() == "M3 Super 90") {  
    EGP:egpBox(18,vec2(100,130),vec2(170,100))      
    EGP:egpMaterial(18,"vgui/gfx/vgui/m3")
    EGP:egpRoundedBox(19,vec2(100,200),vec2(170,50))
    EGP:egpAlpha(19,100)
    EGP:egpColor(19,vec(125,125,125))
    EGP:egpText(20,"Buy M3 Super 90",vec2(35,190))
}
  
elseif(Ranger1:shipmentName() == "AK-47") {
    
    EGP:egpBox(21,vec2(300,130),vec2(170,100))      
    EGP:egpMaterial(21,"vgui/gfx/vgui/ak47")
    EGP:egpRoundedBox(22,vec2(300,200),vec2(170,50))
    EGP:egpAlpha(22,100)
    EGP:egpColor(22,vec(125,125,125))
    EGP:egpText(23,"Buy AK-47",vec2(260,190))      
}
elseif(Ranger2:shipmentName() == "AK-47") {
    
    EGP:egpBox(21,vec2(300,130),vec2(170,100))      
    EGP:egpMaterial(21,"vgui/gfx/vgui/ak47")
    EGP:egpRoundedBox(22,vec2(300,200),vec2(170,50))
    EGP:egpAlpha(22,100)
    EGP:egpColor(22,vec(125,125,125))
    EGP:egpText(23,"Buy AK-47",vec2(260,190))      
}
elseif(Ranger3:shipmentName() == "AK-47") {
    
    EGP:egpBox(21,vec2(300,130),vec2(170,100))      
    EGP:egpMaterial(21,"vgui/gfx/vgui/ak47")
    EGP:egpRoundedBox(22,vec2(300,200),vec2(170,50))
    EGP:egpAlpha(22,100)
    EGP:egpColor(22,vec(125,125,125))
    EGP:egpText(23,"Buy AK-47",vec2(260,190))      
}
elseif(Ranger4:shipmentName() == "AK-47") {
    
    EGP:egpBox(21,vec2(300,130),vec2(170,100))      
    EGP:egpMaterial(21,"vgui/gfx/vgui/ak47")
    EGP:egpRoundedBox(22,vec2(300,200),vec2(170,50))
    EGP:egpAlpha(22,100)
    EGP:egpColor(22,vec(125,125,125))
    EGP:egpText(23,"Buy AK-47",vec2(260,190))      
}
elseif(Ranger1:shipmentName() == "Desert Eagle") {
    
    EGP:egpBox(24,vec2(100,330),vec2(170,100))      
    EGP:egpMaterial(24,"vgui/gfx/vgui/deserteagle")
    EGP:egpRoundedBox(25,vec2(100,420),vec2(170,50))
    EGP:egpAlpha(25,100)
    EGP:egpColor(25,vec(125,125,125))
    EGP:egpText(26,"Buy Deagle",vec2(55,410))      
}

elseif(Ranger2:shipmentName() == "Desert Eagle") {
    
    EGP:egpBox(24,vec2(100,330),vec2(170,100))      
    EGP:egpMaterial(24,"vgui/gfx/vgui/deserteagle")
    EGP:egpRoundedBox(25,vec2(100,420),vec2(170,50))
    EGP:egpAlpha(25,100)
    EGP:egpColor(25,vec(125,125,125))
    EGP:egpText(26,"Buy Deagle",vec2(55,410))      
}

elseif(Ranger3:shipmentName() == "Desert Eagle") {
    
    EGP:egpBox(24,vec2(100,330),vec2(170,100))      
    EGP:egpMaterial(24,"vgui/gfx/vgui/deserteagle")
    EGP:egpRoundedBox(25,vec2(100,420),vec2(170,50))
    EGP:egpAlpha(25,100)
    EGP:egpColor(25,vec(125,125,125))
    EGP:egpText(26,"Buy Deagle",vec2(55,410))      
}

elseif(Ranger4:shipmentName() == "Desert Eagle") {
    
    EGP:egpBox(24,vec2(100,330),vec2(170,100))      
    EGP:egpMaterial(24,"vgui/gfx/vgui/deserteagle")
    EGP:egpRoundedBox(25,vec2(100,420),vec2(170,50))
    EGP:egpAlpha(25,100)
    EGP:egpColor(25,vec(125,125,125))
    EGP:egpText(26,"Buy Deagle",vec2(55,410))      
}

elseif(Ranger1:shipmentName() == "Glock 20") {
    
    EGP:egpBox(27,vec2(300,330),vec2(170,100))      
    EGP:egpMaterial(27,"vgui/gfx/vgui/glock18")
    EGP:egpRoundedBox(28,vec2(300,420),vec2(170,50))
    EGP:egpAlpha(28,100)
    EGP:egpColor(28,vec(125,125,125))
    EGP:egpText(29,"Buy Glock 20",vec2(260,410))      
}
elseif(Ranger2:shipmentName() == "Glock 20") {
    
    EGP:egpBox(27,vec2(300,330),vec2(170,100))      
    EGP:egpMaterial(27,"vgui/gfx/vgui/glock18")
    EGP:egpRoundedBox(28,vec2(300,420),vec2(170,50))
    EGP:egpAlpha(28,100)
    EGP:egpColor(28,vec(125,125,125))
    EGP:egpText(29,"Buy Glock 20",vec2(260,410))      
}
elseif(Ranger3:shipmentName() == "Glock 20") {
    
    EGP:egpBox(27,vec2(300,330),vec2(170,100))      
    EGP:egpMaterial(27,"vgui/gfx/vgui/glock18")
    EGP:egpRoundedBox(28,vec2(300,420),vec2(170,50))
    EGP:egpAlpha(28,100)
    EGP:egpColor(28,vec(125,125,125))
    EGP:egpText(29,"Buy Glock 20",vec2(260,410))      
}
elseif(Ranger4:shipmentName() == "Glock 20") {
    
    EGP:egpBox(27,vec2(300,330),vec2(170,100))      
    EGP:egpMaterial(27,"vgui/gfx/vgui/glock18")
    EGP:egpRoundedBox(28,vec2(300,420),vec2(170,50))
    EGP:egpAlpha(28,100)
    EGP:egpColor(28,vec(125,125,125))
    EGP:egpText(29,"Buy Glock 20",vec2(260,410))      
}



else {
    EGP:egpText(30,"Nothing in stock on this page, check other pages.",vec2(256,256))
    EGP:egpAlign(30,1,1)
}


    
   
}

function guns2() {
    EGP:egpRemove(18)
    EGP:egpRemove(19)
    EGP:egpRemove(20)
    EGP:egpRemove(21)
    EGP:egpRemove(22)
    EGP:egpRemove(23)
    EGP:egpRemove(24)
    EGP:egpRemove(25)
    EGP:egpRemove(26)
    EGP:egpRemove(27)
    EGP:egpRemove(28)
    EGP:egpRemove(29)
    EGP:egpRemove(30)
    EGP:egpRemove(31)
    EGP:egpRemove(32)
    EGP:egpRoundedBox(36,vec2(256,110),vec2(200,50))
    EGP:egpAlpha(36,100)
    EGP:egpText(37,"<- Previous page",vec2(180,100))
    EGP:egpColor(36,vec(75,75,75))    
              
    if(Ranger1:shipmentName() == "Raging Bull") {
    EGP:egpText(33,"Picture N/A",vec2(50,130))     
    EGP:egpRoundedBox(34,vec2(100,200),vec2(170,50))
    EGP:egpAlpha(34,100)
    EGP:egpColor(34,vec(125,125,125))
    EGP:egpText(35,"Buy Raging Bull",vec2(35,190))  
}
elseif(Ranger2:shipmentName() == "Raging Bull") {
    EGP:egpText(33,"Picture N/A",vec2(50,130))  
    EGP:egpRoundedBox(34,vec2(100,200),vec2(170,50))
    EGP:egpAlpha(34,100)
    EGP:egpColor(34,vec(125,125,125))
    EGP:egpText(35,"Buy Raging Bull",vec2(35,190))  
}
elseif(Ranger3:shipmentName() == "Raging Bull") {
    EGP:egpText(33,"Picture N/A",vec2(50,130))  
    EGP:egpRoundedBox(34,vec2(100,200),vec2(170,50))
    EGP:egpAlpha(34,100)
    EGP:egpColor(34,vec(125,125,125))
    EGP:egpText(35,"Buy Raging Bull",vec2(35,190))  
}
elseif(Ranger4:shipmentName() == "Raging Bull") {
    EGP:egpText(33,"Picture N/A",vec2(50,130))  
    EGP:egpRoundedBox(34,vec2(100,200),vec2(170,50))
    EGP:egpAlpha(34,100)
    EGP:egpColor(34,vec(125,125,125))
    EGP:egpText(35,"Buy Raging Bull",vec2(35,190))  
}



    else {
    EGP:egpText(30,"Nothing in stock on this page, check other pages.",vec2(256,256))
    EGP:egpAlign(30,1,1)
} 

    }
function info() {
    EGP:egpRemove(18)
    EGP:egpRemove(19)
    EGP:egpRemove(20)
    EGP:egpRemove(21)
    EGP:egpRemove(22)
    EGP:egpRemove(23)
    EGP:egpRemove(24)
    EGP:egpRemove(25)
    EGP:egpRemove(26)
    EGP:egpRemove(27)
    EGP:egpRemove(28)
    EGP:egpRemove(29)
    EGP:egpRemove(30)
    EGP:egpRemove(31)
    EGP:egpRemove(32)
    EGP:egpRemove(34)
    EGP:egpRemove(35)
    EGP:egpText(36,"Info",vec2(240,100))
    EGP:egpSize(36,30)
    EGP:egpText(37,"This E2 was made by QUACK (STEAM_0:1:56399480)",vec2(60,480))
    EGP:egpText(38,"This E2 was made on the server Canadian's Turf BuildRP.",vec2(60,400))
    EGP:egpText(39,"This E2 is completely automatic, it automatically detects prices and name of the gun",vec2(60,320))
    EGP:egpSize(39,12)
    EGP:egpText(40,"You can buy this E2 from QUACK, Contact him on steam for more info",vec2(60,260))
    EGP:egpSize(40,14)
}    
    
}



#Touchscreen function. DO NOT MESS WITH!
function number wirelink:onClick(Player:entity,ID){
    local P=This:egpPos(ID)
    local H=This:egpSize(ID)/2
    return inrange(This:egpCursor(Player),P-H,P+H)
    }





if(EGP:onClick(User,31)) {
    guns2()
}

if(EGP:onClick(User,9)) {
    guns()   
}
if(EGP:onClick(User,36)) {
    guns()
}

if(EGP:onClick(User,19)) {
   moneyRequest(User,(Ranger1:shipmentPrice() / 10 + 2500),"Buy M3 Super 90") 
   moneyRequest(User,(Ranger2:shipmentPrice() / 10 + 2500),"Buy M3 Super 90") 
   moneyRequest(User,(Ranger3:shipmentPrice() / 10 + 2500),"Buy M3 Super 90") 
   moneyRequest(User,(Ranger4:shipmentPrice() / 10 + 2500),"Buy M3 Super 90") 
}

if(EGP:onClick(User,22)) {
   moneyRequest(User,(Ranger1:shipmentPrice() / 10 + 2500),"Buy AK-47") 
   moneyRequest(User,(Ranger2:shipmentPrice() / 10 + 2500),"Buy AK-47") 
   moneyRequest(User,(Ranger3:shipmentPrice() / 10 + 2500),"Buy AK-47") 
   moneyRequest(User,(Ranger4:shipmentPrice() / 10 + 2500),"Buy AK-47") 
    
}

if(EGP:onClick(User,25)) {
   moneyRequest(User,(Ranger1:shipmentPrice() / 10 + 2500),"Buy Deagle") 
   moneyRequest(User,(Ranger2:shipmentPrice() / 10 + 2500),"Buy Deagle") 
   moneyRequest(User,(Ranger3:shipmentPrice() / 10 + 2500),"Buy Deagle") 
   moneyRequest(User,(Ranger4:shipmentPrice() / 10 + 2500),"Buy Deagle") 
    
}
if(EGP:onClick(User,28)) {
   moneyRequest(User,(Ranger1:shipmentPrice() / 10 + 2500),"Buy Glock 20") 
   moneyRequest(User,(Ranger2:shipmentPrice() / 10 + 2500),"Buy Glock 20") 
   moneyRequest(User,(Ranger3:shipmentPrice() / 10 + 2500),"Buy Glock 20") 
   moneyRequest(User,(Ranger4:shipmentPrice() / 10 + 2500),"Buy Glock 20") 
}


if(first()) {
info()
}

if(EGP:onClick(User,10)) {
  info()   
}

if(EGP:onClick(User,1)) {
 load()
 timer("Load",2000)   
}



if(Ranger1:shipmentName() == "M3 Super 90") {
    EGP:egpMaterial(0,"vgui/gfx/vgui/m3")
}
if(Ranger1:shipmentName() == "Desert Eagle") {
    EGP:egpMaterial(0,"vgui/entities/weapon_mad_deagle")
}

if(Ranger1:shipmentName() == "Glock 20") {
    EGP:egpMaterial(0,"vgui/entities/weapon_mad_auto_glock")
}

if(clk("Load")) {
    mainmenu()
}
