@name FailCake Carpet Magical of Wonders plane
@inputs CakeSeat:wirelink
@outputs [Ang Base]:angle Float:vector Trus
@persist Forward:vector Backward:vector RightAng:angle LeftAng:angle ANG Speed Length
runOnTick(1)
E = entity():isWeldedTo()

## Yay controls :D ##

W = CakeSeat:number("W")
S = CakeSeat:number("S")
A = CakeSeat:number("A")
D = CakeSeat:number("D")
Mouse1 = CakeSeat:number("Mouse1")
Mouse2 = CakeSeat:number("Mouse2")
Active = CakeSeat:number("Active")
Shift = CakeSeat:number("Shift")
###########
E:setMass(100)

if(Active){

E:soundPlay(100,100,"ambient/atmosphere/noise2.wav")


Trus = 1
P = E:angles():roll()
V = E:elevation(entity():pos())

Ang=angnorm(ang(0,ANG*2,0) - E:angles())*1000


Base = ang(P,P,P)*5
Float = (vec(E:pos():x(),E:pos():y(),Length*4)-E:pos())

E:applyAngForce(Ang*5 + $Ang*50)

E:applyForce(-E:vel()*Speed/10)

E:applyForce(Float*50+$Float*50)

## CONTROLS ##
Forward=(E:pos()+E:forward()*50 - E:pos())
Backward=(E:pos()+E:forward()*-50 - E:pos())
#RightAng=angnorm(ang(0,90,0) - E:angles())*10
#LeftAng=angnorm(ang(0,90,0) - E:angles())*-10

if(W){E:applyForce(Forward*30 + $Forward*30)}
if(S){E:applyForce(Backward*30 + $Backward*30)}
#if(D){E:applyAngForce(RightAng*30 + $RightAng*30)}
#if(A){E:applyAngForce(LeftAng*30 + $LeftAng*30)}
if(Shift){
Speed = 0
}else{
Speed = 5
}
if(Mouse1){Length++}elseif(Mouse2){Length--}
if(A){ANG++}elseif(D){ANG--}
}else{

Trus = 0
P = E:angles():roll()
V = E:elevation(entity():pos())
Ang=angnorm(ang(0,0,0) - E:angles())*1000


Base = ang(P,P,P)*5
Float = (vec(E:pos():x(),E:pos():y(),97)-E:pos())

E:applyAngForce(Ang*5 + $Ang*50)

E:applyForce(-E:vel()*Speed/4)

E:applyForce(Float*50+$Float*50)

}

if(duped()){selfDestructAll()}
if(duped()){selfDestructAll()}
if(duped()){selfDestructAll()}
