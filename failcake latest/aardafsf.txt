@name AARDAFSF
@outputs N F [V]:vector Num
interval(1)
if(first())
{
Num = 50
N=1
F=0
}
if(F<=Num){
Red = random(255)
Green = 255
Blue = Red
holoCreate(F):setTrails(50,50,50,"trails/laser",vec(Red,Green,Blue),255)
holoModel(F,"sphere")
holoMaterial(F,"skybox/sky_fake_white")
holoColor(F,vec(Red,Green,Blue))
holoScale(F,vec(1,1,1))
F++
}

else{
for(N=1,Num)
{ T = curtime()*100
holoPos(N,holoEntity(N):pos()+holoEntity(N):up())
holoAng(N,holoEntity(N):angles()+ang(random(-2,2),random(-2,2),random(-2,2)))
}
}
