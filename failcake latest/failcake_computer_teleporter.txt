@name FailCake Computer Teleporter
@inputs Activate Scr:wirelink Chair
@outputs Door Machine Teleport Y Lasor Kick
@persist Timer Bip Ready
@trigger all
interval(1000)

if(first()){
Timer = 20
}

Scr:writeString("#########################",2,1,900)
Scr:writeString("#  FailCake Teleporter  #",2,2,999)
Scr:writeString("#########################",2,3,900)
Scr:writeString("    ###|Welcome|###   ",2,4,999)

Scr:writeString("######################",3,6,900)
Scr:writeString("#  TimeLeft : "+Timer+"     #",3,7,999)
Scr:writeString("######################",3,8,900)

if(Activate == 1 & Chair == 1 & Teleport == 0){
Timer -= 1
Kick = 0
Scr:writeCell(2041, 1)
Scr:writeString("#########################",2,1,900)
Scr:writeString("#  FailCake Teleporter  #",2,2,999)
Scr:writeString("#########################",2,3,900)
Scr:writeString("    ###|Welcome|###   ",2,4,999)
if(Timer < 17){Door = 51}
if(Timer <=15){Machine = 72}
Scr:writeString("######################",3,6,900)
Scr:writeString("#   TimeLeft : "+Timer+"    #",3,7,999)
Scr:writeString("######################",3,8,900)
if(Timer == 19){
soundPlay(30000,30000,"ambient/levels/labs/machine_ring_resonance_loop1.wav")
soundPlay(40000,40000,"music/HL1_song14.mp3")
}
if(Timer == 4){
soundPlay(10000,10000,"vo/k_lab/kl_initializing.wav")
}
if(Timer == 2){
soundPlay(20000,20000,"HL1/ambience/particle_suck2.wav")
}
if(Timer == 0){
soundPlay(20000,20000,"beams/beamstart5.wav")
}
if(Timer < 1){
Y = 40
Lasor = 1
}
if(Timer < 0){
Bip +=1
if(Bip == 1){
Scr:writeString("#   TimeLeft : "+Timer+"    #",3,7,999)
}
if(Bip == 2){
Scr:writeString("#   TimeLeft : "+Timer+"    #",3,7,900)
}
if(Bip == 3){Bip = 0}
Timer = 2
Teleport = 1
}

}else{
if(Teleport ==1 ){Kick = 1 Teleport = 0}
soundStop(10000)
soundStop(30000)
soundStop(40000)
Y = 0
Lasor = 0
Teleport = 0
Door = 0
Machine = 0
Timer = 20
}
