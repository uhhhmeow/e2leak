@name FailCakes Helper =D (Thanks to Pixel)
@outputs Grabber Fire
@persist V:vector Gyro:angle 

runOnTick(1)

Plate = entity():isWeldedTo()

Me = owner()

P = Plate:elevation(Me:aimPos())
Y = Plate:bearing(Me:aimPos())
R = Plate:angles():roll()

Gyro = -ang(P,Y,R)*1000

Plate:applyAngForce(Gyro + $Gyro*5)

V = (Me:pos() - Plate:pos() + vec(0,0,100))*500

Plate:applyForce(V + $V*5)

Fire = owner():isCrouch()
if (owner():keyAttack1()) {Fire=1} else {Fire=0}
if (owner():keyAttack2()) {Grabber=1} else {Grabber=0}
