@name FailCake AutoGun Store V2
@inputs Scr:wirelink Active RAk Rmp5 RPistol RShotgun Money Check
@outputs SpawnAk SpawnMp SpawnPis SpawnShot GoCheck Message:string
@persist P Tim
@trigger all
runOnTick(1)

if(first()){
Message = "Autogun Store Open!"    
}

    Scr:writeString("====[",0,14,990)
    Scr:writeString("]====",25,14,990)
    Scr:writeString(Message:sub(P,P+19),5,14,900)
    if (P>=Message:length()+10) {P=0 Refresh=1}
    P+=0.1
    
AkMoney = 550
Mp5Money = 500
PistolMoney = 80
ShotgunMoney = 600


if(RAk){
StockAk = "In Stock"
AK = 1    
}else{
StockAk = "Empty   "
AK = 0
}

if(Rmp5){
StockMp5 = "In Stock"   
MP5 = 1 
}else{
StockMp5 = "Empty   "
MP5 = 0
}

if(RPistol){
StockPistol = "In Stock"    
PISTOL = 1
}else{
StockPistol = "Empty   "
PISTOL = 0
}

if(RShotgun){
StockShotgun = "In Stock"   
SHOT = 1 
}else{
StockShotgun = "Empty   "
SHOT = 0
}



if(Active){
    
if(AK == 0 & MP5 == 0 & PISTOL == 0 & SHOT == 0){
Message = "                     NO STOCKS! "  
Tim = 0   
}else{
Tim += 0.1
if(Tim == 2){
Message = "                     Autogun Store Open! "
}     
}
 
Scr:writeString("::::::::::::::::::::::::::",2,0,999)
Scr:writeString(": FailCake AutoGun Store :",2,1,999)
Scr:writeString("::::::::::::::::::::::::::",2,2,999)
Scr:writeString("Weapon = Price ==> Stock",1,4,900)

Scr:writeString("Ak = "+AkMoney+"$ ==> "+StockAk,1,6,999)
Scr:writeString("Mp5 = "+Mp5Money+"$ ==> "+StockMp5,1,7,999)
Scr:writeString("Pistol = "+PistolMoney+"$ ==> "+StockPistol,1,8,999)
Scr:writeString("Shotgun = "+ShotgunMoney+"$ ==> "+StockShotgun,1,9,999)

Scr:writeString("##############################",0,13,999)
Scr:writeString("##############################",0,15,999) 


 

if(Money == AkMoney & AK == 1 ){
Message = "                      Please Wait for your gun! " 
SpawnAk = 1 
GoCheck = 1  
}

if(Money == Mp5Money & MP5 == 1 ){
SpawnMp = 1   
GoCheck = 1 
Message = "                      Please Wait for your gun! " 
}

if(Money == PistolMoney & PISTOL == 1 ){
SpawnPis = 1 
GoCheck = 1 
Message = "                      Please Wait for your gun! " 
}

if(Money == ShotgunMoney & SHOT == 1 ){
SpawnShot = 1 
GoCheck = 1
Message = "                      Please Wait for your gun! " 
}

  
if(Check > 0 & GoCheck){
Message = "                      Autogun Store Open! "  
SpawnShot = 0
SpawnPis = 0 
SpawnAk = 0
SpawnMp = 0  
GoCheck = 0
}
  
}
