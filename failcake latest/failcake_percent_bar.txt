@name FailCake Percent Bar?
@inputs EGP:wirelink Load MaxLoading Name:string
@outputs 
@persist Space I
@trigger 
interval(200)

if(MaxLoading==0 & I<=50 | first())
{
    while(I<=50){
        EGP:egpRemove(I)
        I++
        Space=50
    }
}
if(MaxLoading!=0)
{
    Name=="Loading..."
    PCA=round((100/MaxLoading)*Load,3)
    ColorA=hsv2rgb(vec((120/100)*PCA,1,1))
    EGP:egpBoxoutline(1,vec2(100,100),vec2(400,70),vec4(255,255,255,255))
    EGP:egpText(2,toString(PCA)+"%",vec2(8,112),ColorA,255)
    EGP:egpText(3,Name,vec2(100,80),vec4(255,255,255,255))
    EGP:egpBox(4,vec2(101,101),vec2((398/MaxLoading)*Load,68),ColorA,255)
}


EGP:egpText(40,"",20,0,0,255,0,255)
EGP:egpDraw()  
