setName("NYEH NYEH NYEH HEH!")
if(first()){
holoCreate(1)
holoModel(1,"hq_cylinder")
holoScale(1,vec(0.2,0.3,3))
holoPos(1,entity():pos() + vec(0,0,30))   
holoCreate(2)
holoModel(2,"hq_icosphere")
holoPos(2,holoEntity(1):pos() + vec(0,-2,18))
holoScale(2,vec(0.4,0.4,0.4))
holoCreate(3)
holoModel(3,"hq_icosphere")
holoPos(3,holoEntity(1):pos() + vec(0,2,18))
holoScale(3,vec(0.4,0.4,0.4))
holoCreate(4)
holoModel(4,"hq_icosphere")
holoPos(4,holoEntity(1):pos() + vec(0,2,-18))
holoScale(4,vec(0.4,0.4,0.4))
holoCreate(5)
holoModel(5,"hq_icosphere")
holoPos(5,holoEntity(1):pos() + vec(0,-2,-18))
holoScale(5,vec(0.4,0.4,0.4))
holoParent(2,1)
holoParent(3,1)
holoParent(4,1)
holoParent(5,1)
holoParent(1,entity())
#holoEntity(1):holoPhysScale(vec(1,1,1))
}

