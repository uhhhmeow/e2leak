@name Auto Door
@outputs Door Door2
@persist [CoolPlayers Last]:array [AFound Found Closest In]:entity Cool:string Max Distance
if(first()|duped()){
    runOnChat(1)
    runOnTick(1)
    CoolPlayers = array(owner():name())
    Distance = 100
}
interval(1)
if(chatClk(owner())){
    Last = owner():lastSaid():explode(" ")
    Max = 0
    foreach(Index,Name:string=CoolPlayers){
        Max = Index
    }
    if(Last[1,string] == "!ad"){
        hideChat(chatClk(owner()))
        if(Last:count() > 1){
            Found = findPlayerByName(Last[2,string])
            if(Found == noentity()){
                print("Player Not Found")
            } else {
                Cool = CoolPlayers:concat()
                if(!Cool:find(Found:name())){
                    print(Found:name() + " is now cool.")
                    CoolPlayers[Max+1,string] = Found:name()
                    entity():soundPlay(1,1,"/buttons/blip1.wav")
                } else {
                    print(Found:name() + " is already cool.")
                    entity():soundPlay(1,1,"/buttons/button10.wav")
                }
            }
        } else {
            print("!ad [Player Name]")
        }
    }
    if(Last[1,string] == "!remove"){
        hideChat(chatClk(owner()))
        if(Last:count() > 1){
            foreach(Index,Name:string=CoolPlayers){
                if(Name:lower():find(Last[2,string])){
                    print(Name + " is not cool anymore.")
                    entity():soundPlay(1,1,"/buttons/blip1.wav")
                    CoolPlayers:remove(Index)
                    break
                }
                if(Index == CoolPlayers:count()){
                    print(Last[2,string] + " is not cool, therefore cannot be removed.")
                    entity():soundPlay(1,1,"/buttons/button10.wav")
                }
            }
        } else {
            print("!remove [Player Name]")
        }
    }
    if(Last[1,string] == "!list"){
        hideChat(chatClk(owner()))
        print(CoolPlayers:count() + " cool player(s):")
        foreach(Index,Name:string=CoolPlayers){
            print(Name)
        }
    }
    if(Last[1,string] == "!dis"){
        hideChat(chatClk(owner()))
        if(Last:count() > 1 & Last[2,string]:toNumber()){
            Distance = Last[2,string]:toNumber()
            print("Open distance set to: "+Last[2,string]:toNumber())
            entity():soundPlay(1,1,"/buttons/blip1.wav")
        } else {
            print("!dis [Distance]")
        }
    }
} else {
    Door = 0
    findByClass("player")
    AFound = findClosest(entity():pos())
    if(AFound:pos():distance(entity():pos())<Distance){
        foreach(Index,Name:string=CoolPlayers){
            if(AFound:name():lower() == Name:lower()){
                Door = 1
            }
        }
    }
}

if(Door==1){Door2=0}
if(Door==0){Door2=50}
