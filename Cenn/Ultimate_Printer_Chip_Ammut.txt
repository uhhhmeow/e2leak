@name Ammuts L4d Tank
@inputs W A S D Space Seat:entity Active Turret:entity M1 M2 
@outputs CamPos:vector CamAng:angle Holo:entity Pos:vector Jump J
@persist Sheild:entity View UBER MoveVel Fall String:array
@trigger 
if(first()){
 #entity():setColor(0,0,0,0)
 runOnTick(1)
 runOnChat(1)
 holoCreate(1)
 holoModel(1,"models/infected/hulk.mdl")
 holoAnim(1,"STAND_MELEE") 
 holoSetPose(1,"move_scale",0)
 holoSkin(1,1)
holoAng(1,ang(holoEntity(1):angles():yaw() * 180))
 holoCreate(5)
 holoAlpha(5,0)
 timer("Startup",750)
 MoveVel = 6


}

Random = 4000






Ranger = rangerOffset(100000,holoEntity(1):pos()+vec(0,0,5000),-holoEntity(1):up())


if(~W & W){
holoAnim(1,"RUN_MELEE")
 holoSetPose(1,"move_scale",1)
 holoSetPose(1,"move_x",1)
 holoSetPose(1,"move_yaw",0)

}

if(~W & !W){
 holoAnim(1,"STAND_MELEE") 
 holoSetPose(1,"move_scale",0)
 holoSetPose(1,"move_x",0)
 holoSetPose(1,"move_yaw",0)
}

if(~A & A){
 holoAnim(1,"RUN_MELEE")
 holoSetPose(1,"move_scale",1)
 holoSetPose(1,"move_y",1)
 holoSetPose(1,"move_yaw",90)

}

if(~A & !A){
 holoAnim(1,"STAND_MELEE") 
 holoSetPose(1,"move_scale",0)
 holoSetPose(1,"move_yaw",0)
}

if(~S & S){
 holoAnim(1,"RUN_MELEE")
 holoSetPose(1,"move_scale",1)
holoSetPose(1,"move_y",1)
 holoSetPose(1,"move_yaw",-180)

}

if(~S & !S){
 holoAnim(1,"STAND_MELEE") 
 holoSetPose(1,"move_scale",0)

 holoSetPose(1,"move_yaw",-360)
}

if(~D & D){
 holoAnim(1,"RUN_MELEE")
 holoSetPose(1,"move_scale",1)
holoSetPose(1,"move_y",1)
 holoSetPose(1,"move_yaw",-90)
}

if(~D & !D){
 holoAnim(1,"STAND_MELEE") 
 holoSetPose(1,"move_scale",0)
 holoSetPose(1,"move_y",0)
}






if(W){
 holoPos(1,holoEntity(1):pos()+holoEntity(1):forward()*MoveVel)

}

if(A){
 holoPos(1,holoEntity(1):pos()+holoEntity(1):right()*-MoveVel)
}

if(S){
 holoPos(1,holoEntity(1):pos()+holoEntity(1):forward()*-MoveVel)
}

if(D){
 holoPos(1,holoEntity(1):pos()+holoEntity(1):right()*MoveVel)
}

CamPos = holoEntity(5):toWorld(vec(-75,0,0))
CamAng = holoEntity(5):angles()

Holo = holoEntity(1)
Ang = Seat:driver():eyeAngles() 

holoAng(1,ang(0,Ang:yaw(),0))

holoAng(5,Ang)
holoPos(5,holoEntity(1):attachmentPos("head"))



if(dupefinished()){
 reset()   
}

if(~M1 & M1){
 UBER = !UBER   
}

if(changed(UBER)&UBER){
holoAnim(1,"melee_raw")

}

if(clk("boom")){
}

if(chatClk(owner())){
    if(owner():lastSaid()=="/kill"){
        Seat:killPod()
        hideChat(1)
    }    
}

if(~M2 & M2){
 View = !View
}

if(View){
    holoAng(5,Ang-ang(0,180,0))
    holoSetPose(1,"body_pitch",Ang:pitch())
} else {
holoAng(5,Ang)
holoSetPose(1,"body_pitch",-Ang:pitch())
}




Ranger = rangerOffset(500000000,holoEntity(1):pos()+vec(0,0,15),-holoEntity(1):up())
G = rangerOffsetHull(holoEntity(1):toWorld(vec(0,0,50)),holoEntity(1):toWorld(vec(0,0,-3)),vec(-10,-10,1),vec(10,10,1))

if(!G:hit()){
    holoAnim(1,"airwalk_primary")
    if(!Fall){Fall = 1}
    if (Jump==0){
    Fall+=0.15
    Fall = clamp(Fall,-25,25)
    holoPos(1,holoEntity(1):pos()-vec(0,0,Fall))
}
}
if(G:hit()){Fall = 0 holoPos(1,G:position())
    }

if(Ranger:pos():z()>holoEntity(1):pos():z()){
    holoPos(1,Ranger:pos())
}

if(!holoEntity(1):pos():isInWorld()){
 holoPos(1,holoEntity(1):toWorld(vec(-50,0,0)))
}

if (changed(Space)&Space==1&Fall==0){Jump=1
    J=4}
if (Jump==1){J-=0.1
    holoPos(1,holoEntity(1):pos()+vec(0,0,J))
}
if (J<=-1){Jump=0}

if(changed(G:hit())){
 holoAnim(1,"STAND_MELEE") }
