@name 
@inputs 
@outputs 
@persist T G [E O]:entity 
@trigger 
@model models/dav0r/hoverball.mdl
if(first()){T=0
E = entity()
O = owner()

holoCreate(1)
holoPos(1,E:pos())
holoModel(1,"hq_torus")
holoScale(1,vec(2,2,1))
G = 1
holoColor(1,vec(0,0,255))
holoParent(1,E)
holoCreate(2)
holoPos(2,E:pos())
holoModel(2,"hq_torus")
holoScale(2,vec(2,2,1))
holoColor(2,vec(255,0,0))
holoParent(2,E)

holoCreate(3)
holoPos(3,E:pos())
holoModel(3,"hq_torus")
holoScale(3,vec(2,2,1))
holoColor(3,vec(0,255,0))
holoParent(3,E)
}


runOnTick(1)

E:setPos(owner():aimPos() + vec(0,0,30))
E:propNotSolid(1)


if(owner():keyAttack2()){
    if(T==0){
        soundPlay(1000,1000,"buttons/button19.wav")
        timer("boop",1000)
        O:tele(E:pos())
    T = 1
    timer("reset",3000)
}
}

if(clk("reset")){T=0}


G++
H = -G
J = -G * 3
K = G * 2
holoAng(1,ang(G,G,G))

holoAng(2,ang(H,H,H))
holoAng(3,ang(J,J,J))
E:setAng(ang(K,K,K))

if(clk("boop")){soundPlay(100,1000,"buttons/button17.wav")
    timer("boop2",1000)}

if(clk("boop2")){soundPlay(100,1000,"buttons/button6.wav")}
