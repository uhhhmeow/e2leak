@name
@inputs 
@outputs 
@persist 
@trigger 

runOnTick(1)

    if (owner():weapon():type()=="weapon_pistol"){
    Head="anim_attachment_RH"
    
    for(I=1,1){
        
        holoCreate(I)
        holoPos(I,owner():attachmentPos(Head))
        holoAng(I,owner():attachmentAng(Head))
        holoPos(I,holoEntity(I):toWorld(vec(0,0,-2+(-I*0))))
        holoAng(I,holoEntity(I):toWorld(ang(0,10,(-I*10)-0)))
        holoParentAttachment(I,owner(),Head)
        holoModel(I,"models/weapons/w_models/w_pistol.mdl")
        holoMaterial(I,"models/weapons/w_models/w_pistol.mdl")
        holoScale(I,vec(1,1,1))
        owner():weapon():setColor(0,0,0,0)
    }
}

else{owner():weapon():setColor(255,255,255,255)}
