@name TF2 Turret
@outputs [E Me Turret]:entity [Amodels Models]:array Phase:string [Ang Angle]:angle Pos:vector Upgrade ModelIndex Array:array Distance Result:string Times Damage HoloDist Circle X Y
@model models/weapons/w_models/w_toolbox.mdl


if(first() | dupefinished())
{


    runOnTick(1)
    runOnChat(1)





owner():weapon():setColor(0,0,0,0)


    
    E = entity()
    Me = owner()   
    Amodels = array("models/buildables/sentry1_heavy.mdl","models/buildables/sentry2_heavy.mdl","models/buildables/sentry3_heavy.mdl")
    Models = array("models/buildables/sentry1_blueprint.mdl","models/buildables/sentry1.mdl","models/buildables/sentry2.mdl","models/buildables/sentry3.mdl","models/buildables/sentry3_rockets.mdl","models/buildables/sapper_sentry2.mdl")
    ModelIndex = 1
    
    Turret = holoCreate(1)
    holoParent(1, E)
    holoModel(1,Models[ModelIndex,string])
    holoCreate(0)
   
    Phase = "Deploy"
}

if(Phase == "Deploy")
{
    Ang = (Me:eyeTrace():hitNormal():toAngle() + ang(90,0,0)):setYaw((Me:aimPos() - Me:pos() ):toAngle():yaw())
    holoPos(1, Me:aimPos())
    holoAng(1, Ang)
    
    if(Me:aimPos():distance(Me:pos()) >= 150) #| !inrange(Ang, ang(-45,-360,-45), ang(45,360,45)))
    {
        holoAnim(1, 1)
    }else
    {
        holoAnim(1, 0)
        if(Me:keyAttack1())
        {
            Phase = "Build 1"
            Angle = Ang
            Pos = Me:aimPos()
            
            #holoModel(1, E:model())
            holoModel(1,Amodels[ModelIndex,string])
            holoAnim(1,2)
            timer("build",5000)   
               holoEntity(1):soundPlay(50,50,"vo/engineer_autobuildingsentry01.wav")
            ModelIndex++
        }
    }    
}elseif(Phase == "Active")
{
   Ang = (Pos + vec(0,0,25) - Me:aimPos()):toAngle() + ang(0,90,0)

 holoSetPose(1,"aim_pitch", clamp(Ang:pitch(), 0, 0)) 
    holoSetPose(1,"aim_yaw", sin(curtime()*20)*90)     
    #soundPlay:("weapons/sentry_scan.wav")

#entity():soundPlay(1,50,"weapons/sentry_scan.wav")
 
    Upgrade = Me:keyAttack2()
    if(changed(Upgrade) & Upgrade & ModelIndex <= 3)
    {
        holoModel(1, Amodels[ModelIndex,string])
        holoAnim(1, "upgrade")
        timer("build",2000)    
        ModelIndex++
    }
}


if(clk("build"))
{
    holoModel(1, Models[ModelIndex,string])
    Phase = "Active"

}

timer("clk",15000)

if(clk("clk")){    holoEntity(1):soundPlay(20,30,"weapons/sentry_scan.wav")}





timer("check",160)
Distance = 100
Result = ""
Times = 100
Damage = 20
interval(2000)
if (clk("check")) {
    findInSphere(holoEntity(1):pos(), Distance)
    Array = findToArray()
    for(I=1,Array:count()) {
        if(Array[I,entity]:isPlayer()) {
            if(Array[I,entity]:isAlive()) {
                if (Result != "whip") {                  
Ent = propSpawn("models/props_c17/oildrum001_explosive.mdl",200)
Ent:propBreak() 
Ent:setPos(holoEntity(1):pos() + vec(0,25,75))
}}}}}




