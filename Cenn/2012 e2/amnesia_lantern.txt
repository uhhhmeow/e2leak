@name Amnesia Lantern
@inputs Light:wirelink
@outputs 
@persist 
@trigger 
runOnTick(1)

if (owner():weapon():type()=="weapon_pistol"){
    Head="anim_attachment_RH"
    
    for(I=1,1){
        holoCreate(I)
        holoPos(I,owner():attachmentPos(Head))
        holoAng(I,owner():attachmentAng(Head))
        holoPos(I,holoEntity(I):toWorld(vec(0,-7,-45+(-I*0))))
        holoAng(I,holoEntity(I):toWorld(ang(0,10,(-I*10)-0)))
        holoParentAttachment(I,owner(),Head)
        holoModel(I,"models/props_2fort/lantern001_off.mdl")
        holoMaterial(I,"models/props_2fort/lantern001_off.mdl")
        holoScale(I,vec(1,1,1))
        holoAlpha(1,255)
        owner():weapon():setColor(0,0,0,0)
    Light["GlowSize",number]=1300
    Light["RGB",vector]=vec(255,126,0)*abs((50+randint(1,50))/100)
    Light["GlowBritness",number]=5
}}else{
owner():weapon():setColor(255,255,255,255)
holoAlpha(1,0)}
