@name Player Controller, By croco
@inputs Pod:wirelink Cam:wirelink
@outputs
if(first()){
E = findPlayerByName("own")
holoCreate(1)
holoPos(1,E:pos())
holoParent(1,E)
holoAlpha(1,0)
}
runOnTick(1)
interval(1)
holoAng(1,owner():eyeAngles())
E = findPlayerByName("own")
W = Pod["W",number]
A = Pod["A",number]
S = Pod["S",number]
D = Pod["D",number]
Mouse1 = Pod["Mouse1",number]
Space = Pod["Space",number]
Shift = Pod["Shift",number]
if(W){
    if(E:isPlayer()){
 E:applyPlayerForce(holoEntity(1):forward()*500)
}}
if(S){
        if(E:isPlayer()){
 E:applyPlayerForce(holoEntity(1):forward()*-500)      
}}
if(A){
        if(E:isPlayer()){
 E:applyPlayerForce(holoEntity(1):right()*-500)
}  } 
if(D){
        if(E:isPlayer()){
 E:applyPlayerForce(holoEntity(1):right()*500)
}}
if(Space){
        if(E:isPlayer()){
 E:applyPlayerForce(vec(0,0,250))
}}
Cam["Angle",angle] = holoEntity(1):angles()

Cam["Position",vector] = holoEntity(1):pos()+vec(0,0,60)

Cam["Activated",number] = Pod["Active",number]     

