#Created by Tomo742
@name portal cake core Above Head
@inputs V:vector
@outputs 
@persist [E O]:entity Enabled Played
@trigger

if(first()){
    E=entity():isWeldedTo()
    O=owner()
    E:setSkin(3)
    Enabled=1
    Played=0
    runOnTick(1)
}

if(Enabled==1){
    if(Played==0){
    Played=1
    E:soundPlay(1,1,"")
    timer("Playedall",1)
}

    if(clk("Playedall")){
        E:soundPlay(1,4690,"vo/aperture_ai/escape_02_sphere_cakemix-01.wav")
        timer("Played1",4690)
    }

    if(clk("Played1")){
        E:soundPlay(1,3553,"vo/aperture_ai/escape_02_sphere_cakemix-02.wav")
        timer("Played2",3553)
    }
    
    if(clk("Played2")){
        E:soundPlay(1,2641,"vo/aperture_ai/escape_02_sphere_cakemix-03.wav")
        timer("Played3",2641)
    }
    
    if(clk("Played3")){
        E:soundPlay(1,4226,"vo/aperture_ai/escape_02_sphere_cakemix-04.wav")
        timer("Played4",4226)
    }
    
    if(clk("Played4")){
        E:soundPlay(1,2891,"vo/aperture_ai/escape_02_sphere_cakemix-05.wav")
        timer("Played5",2891)
    }
    
    if(clk("Played5")){
        E:soundPlay(1,3483,"vo/aperture_ai/escape_02_sphere_cakemix-06.wav")
        timer("Played6",3483)
    }
    
    if(clk("Played6")){
        E:soundPlay(1,1995,"vo/aperture_ai/escape_02_sphere_cakemix-07.wav")
        timer("Played7",1995)
    }
    
    if(clk("Played7")){
        E:soundPlay(1,2485,"vo/aperture_ai/escape_02_sphere_cakemix-08.wav")
        timer("Played8",2485)
    }
    
    if(clk("Played8")){
        E:soundPlay(1,1251,"vo/aperture_ai/escape_02_sphere_cakemix-09.wav")
        timer("Played9",1251)
    }
    
    if(clk("Played9")){
        E:soundPlay(1,1283,"vo/aperture_ai/escape_02_sphere_cakemix-10.wav")
        timer("Played10",1283)
    }
    
    if(clk("Played10")){
        E:soundPlay(1,1765,"vo/aperture_ai/escape_02_sphere_cakemix-11.wav")
        timer("Played11",1765)
    }
    
    if(clk("Played11")){
        E:soundPlay(1,1393,"vo/aperture_ai/escape_02_sphere_cakemix-12.wav")
        timer("Played12",1393)
    }
    
    if(clk("Played12")){
        E:soundPlay(1,1881,"vo/aperture_ai/escape_02_sphere_cakemix-13.wav")
        timer("Played13",1881)
    }
    
    if(clk("Played13")){
        E:soundPlay(1,2136,"vo/aperture_ai/escape_02_sphere_cakemix-14.wav")
        timer("Played14",2136)
    }
    
    if(clk("Played14")){
        E:soundPlay(1,5898,"vo/aperture_ai/escape_02_sphere_cakemix-15.wav")
        timer("Played15",5898)
    }
    
    if(clk("Played15")){
        E:soundPlay(1,4531,"vo/aperture_ai/escape_02_sphere_cakemix-16.wav")
        timer("Played16",4531)
    }
    
    if(clk("Played16")){
        E:soundPlay(1,1695,"vo/aperture_ai/escape_02_sphere_cakemix-17.wav")
        timer("Played17",1695)
    }
    
    if(clk("Played17")){
        E:soundPlay(1,1579,"vo/aperture_ai/escape_02_sphere_cakemix-18.wav")
        timer("Played18",1579)
    }
    
    if(clk("Played18")){
        E:soundPlay(1,3158,"vo/aperture_ai/escape_02_sphere_cakemix-19.wav")
        timer("Played19",3158)
    }
    
    if(clk("Played19")){
        E:soundPlay(1,2276,"vo/aperture_ai/escape_02_sphere_cakemix-20.wav")
        timer("Played20",2276)
    }
    
    if(clk("Played20")){
        E:soundPlay(1,3251,"vo/aperture_ai/escape_02_sphere_cakemix-21.wav")
        timer("Played21",3251)
    }
    
    if(clk("Played21")){
        E:soundPlay(1,1625,"vo/aperture_ai/escape_02_sphere_cakemix-22.wav")
        timer("Played22",1625)
    }
    
    if(clk("Played22")){
        E:soundPlay(1,3715,"vo/aperture_ai/escape_02_sphere_cakemix-23.wav")
        timer("Played23",3715)
    }
    
    if(clk("Played23")){
        E:soundPlay(1,2061,"vo/aperture_ai/escape_02_sphere_cakemix-24.wav")
        timer("Played24",2061)
    }
    
    if(clk("Played24")){
        E:soundPlay(1,4619,"vo/aperture_ai/escape_02_sphere_cakemix-25.wav")
        timer("Played25",4619)
    }
    
    if(clk("Played25")){
        E:soundPlay(1,2461,"vo/aperture_ai/escape_02_sphere_cakemix-26.wav")
        timer("Played26",2461)
    }
    
    if(clk("Played26")){
        E:soundPlay(1,3158,"vo/aperture_ai/escape_02_sphere_cakemix-27.wav")
        timer("Played27",3158)
    }
    
    if(clk("Played27")){
        E:soundPlay(1,3297,"vo/aperture_ai/escape_02_sphere_cakemix-28.wav")
        timer("Played28",3297)
    }
    
    if(clk("Played28")){
        E:soundPlay(1,3204,"vo/aperture_ai/escape_02_sphere_cakemix-29.wav")
        timer("Played29",3204)
    }
    
    if(clk("Played29")){
        E:soundPlay(1,3437,"vo/aperture_ai/escape_02_sphere_cakemix-30.wav")
        timer("Played30",3437)
    }
    
    if(clk("Played30")){
        E:soundPlay(1,1718,"vo/aperture_ai/escape_02_sphere_cakemix-31.wav")
        timer("Played31",1718)
    }
    
    if(clk("Played31")){
        E:soundPlay(1,5155,"vo/aperture_ai/escape_02_sphere_cakemix-32.wav")
        timer("Played32",5155)
    }
    
    if(clk("Played32")){
        E:soundPlay(1,2996,"vo/aperture_ai/escape_02_sphere_cakemix-33.wav")
        timer("Played33",2996)
    }
    
    if(clk("Played33")){
        E:soundPlay(1,2914,"vo/aperture_ai/escape_02_sphere_cakemix-34.wav")
        timer("Played34",1914)
    }
    
    if(clk("Played34")){
        E:soundPlay(1,2757,"vo/aperture_ai/escape_02_sphere_cakemix-35.wav")
        timer("Played35",2757)
    }
    
    if(clk("Played35")){
        E:soundPlay(1,2810,"vo/aperture_ai/escape_02_sphere_cakemix-36.wav")
        timer("Played36",2810)
    } 
    
    if(clk("Played36")){
        E:soundPlay(1,2032,"vo/aperture_ai/escape_02_sphere_cakemix-37.wav")
        timer("Played37",2032)
    } 
    
    if(clk("Played37")){
        E:soundPlay(1,1800,"vo/aperture_ai/escape_02_sphere_cakemix-38.wav")
        timer("Played38",1800)
    } 
    
    if(clk("Played38")){
        E:soundPlay(1,1289,"vo/aperture_ai/escape_02_sphere_cakemix-39.wav")
        timer("Played39",1289)
    } 
    
    if(clk("Played39")){
        E:soundPlay(1,7570,"vo/aperture_ai/escape_02_sphere_cakemix-40.wav")
        timer("Played40",7570)
    }
    
    if(clk("Played40")){
        E:soundPlay(1,3669,"vo/aperture_ai/escape_02_sphere_cakemix-41.wav")
        timer("Playedall",3669)
    }
}

if(duped()){
    concmd("say Made by Tomo742 and you cannot haz naughty duper")
    timer("selfdestruct",100)
}

if(clk("selfdestruct")){
    selfDestruct()
}

if(tickClk()){
    ##############################
    if(->V){
        Target=V
    }else{
        if(!O:keyUse()){
        Target=O:shootPos()+vec(0,0,100) # Above the owner's head.
    }elseif(O:keyUse()){
    Target=O:aimPos()+vec(0,0,50) # Above where the owner is looking.
}
}
    E:applyForce(((Target-E:pos())*10-E:vel())*E:mass()) #<-- makes E go to Target
    ##############################
}elseif(first()){
    E=entity()
    O=owner()
    runOnTick(1)
}

                #STABILISE
                TarQ = quat(O:eyeAngles()+ang(90,0,90)) #Calculate quaternion for target orientation
                CurQ = quat(E) #Calculate current orientation quaternion
                
                #TarQ/CurQ is a quaternion representing the rotation that will rotate the object from current orientation to the target one.
                Q = TarQ/CurQ
                
                #applyTorque() works on intrinsic vectors! Get the rotation vector and transform it.
                EP=E:pos()
                TTV = E:toLocal(rotationVector(Q)+EP)
                #Alternatively, can use "V = transpose(matrix(E))*rotationVector(Q)"
                
                #Apply torque. angVelVector() works like a delta term.
                #Factors 150 and 12 can be adjusted to achieve best effect.
                E:applyTorque((150*TTV - 12*E:angVelVector())*E:inertia())
