#Created by Tomo742
@name Portal anger Core
@inputs 
@outputs 
@persist E:entity Enabled Played
@model models/props_bts/glados_ball_reference.mdl
@trigger

if(first()){
    E=entity()
    E:setSkin(2)
    E:setMass(500)
    Enabled=1
    Played=0
    runOnTick(1)
}

if(tickClk()){
    E:applyAngForce(ang(randvec(0,360))*1000)
}

if(Enabled==1){
    if(Played==0){
    Played=1
    E:soundPlay(1,1,"")
    timer("Playedall",1)
}

    if(clk("Playedall")){
        E:soundPlay(1,1544,"vo/aperture_ai/escape_02_sphere_anger-01.wav")
        timer("Played1",1544)
    }

    if(clk("Played1")){
        E:soundPlay(1,755,"vo/aperture_ai/escape_02_sphere_anger-02.wav")
        timer("Played2",755)
    }
    
    if(clk("Played2")){
        E:soundPlay(1,1102,"vo/aperture_ai/escape_02_sphere_anger-03.wav")
        timer("Played3",1102)
    }
    
    if(clk("Played3")){
        E:soundPlay(1,623,"vo/aperture_ai/escape_02_sphere_anger-04.wav")
        timer("Played4",623)
    }
    
    if(clk("Played4")){
        E:soundPlay(1,1091,"vo/aperture_ai/escape_02_sphere_anger-05.wav")
        timer("Played5",1091)
    }
    
    if(clk("Played5")){
        E:soundPlay(1,929,"vo/aperture_ai/escape_02_sphere_anger-06.wav")
        timer("Played6",929)
    }
    
    if(clk("Played6")){
        E:soundPlay(1,650,"vo/aperture_ai/escape_02_sphere_anger-07.wav")
        timer("Played7",650)
    }
    
    if(clk("Played7")){
        E:soundPlay(1,1007,"vo/aperture_ai/escape_02_sphere_anger-08.wav")
        timer("Played8",1007)
    }
    
    if(clk("Played8")){
        E:soundPlay(1,604,"vo/aperture_ai/escape_02_sphere_anger-09.wav")
        timer("Played9",604)
    }
    
    if(clk("Played9")){
        E:soundPlay(1,813,"vo/aperture_ai/escape_02_sphere_anger-10.wav")
        timer("Played10",813)
    }
    
    if(clk("Played10")){
        E:soundPlay(1,1149,"vo/aperture_ai/escape_02_sphere_anger-11.wav")
        timer("Played11",1149)
    }
    
    if(clk("Played11")){
        E:soundPlay(1,836,"vo/aperture_ai/escape_02_sphere_anger-12.wav")
        timer("Played12",836)
    }
    
    if(clk("Played12")){
        E:soundPlay(1,546,"vo/aperture_ai/escape_02_sphere_anger-13.wav")
        timer("Played13",546)
    }
    
    if(clk("Played13")){
        E:soundPlay(1,1370,"vo/aperture_ai/escape_02_sphere_anger-14.wav")
        timer("Played14",1370)
    }
    
    if(clk("Played14")){
        E:soundPlay(1,813,"vo/aperture_ai/escape_02_sphere_anger-15.wav")
        timer("Played15",813)
    }
    
    if(clk("Played15")){
        E:soundPlay(1,789,"vo/aperture_ai/escape_02_sphere_anger-16.wav")
        timer("Played16",789)
    }
    
    if(clk("Played16")){
        E:soundPlay(1,778,"vo/aperture_ai/escape_02_sphere_anger-17.wav")
        timer("Played17",778)
    }
    
    if(clk("Played17")){
        E:soundPlay(1,697,"vo/aperture_ai/escape_02_sphere_anger-18.wav")
        timer("Played18",697)
    }
    
    if(clk("Played18")){
        E:soundPlay(1,871,"vo/aperture_ai/escape_02_sphere_anger-19.wav")
        timer("Played19",871)
    }
    
    if(clk("Played19")){
        E:soundPlay(1,1044,"vo/aperture_ai/escape_02_sphere_anger-20.wav")
        timer("Played20",1044)
    }
    
    if(clk("Played20")){
        E:soundPlay(1,805,"vo/aperture_ai/escape_02_sphere_anger-21.wav")
        timer("Playedall",805)
    }
}

if(duped()){
    concmd("say Made by Tomo742 and you cannot haz naughty duper")
    timer("selfdestruct",100)
}

if(clk("selfdestruct")){
    selfDestruct()
}
