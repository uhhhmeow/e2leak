@name Barrels o' Fun
@persist Barrels:array Target:entity TargetName:string
@model models/cheeze/wires/nano_math.mdl
if( first() ){
    
    runOnChat(1)
    runOnTick(1)
    
    entity():setAlpha(0)
    
    findIncludePlayerProps(owner())
    findIncludeClass("prop_physics")}

if( chatClk(owner()) ){
    ChatArray = owner():lastSaid():explode(" ")
    
    if( ChatArray:string(1) == "t" ){
     hideChat(1)   
    TargetName = ChatArray:string(2)
    }
}

if( findCanQuery() )
{
    if( TargetName )
    {
        Target = findPlayerByName(TargetName)
        TargetName = ""
    }
    else
    {
        findByClass("models/weapons/w_arman_t.mdl")
        Barrels = findToArray()
    }
}

for(Index = 1, Barrels:count())
{
    Barrel = Barrels:entity(Index)
    
    if( Target:isAlive() )
    {
        if( Barrel:mass() != 50000 )
        {
            
            Barrel:setMass(50000)
            
            Barrel:setAlpha(255)
            
            
        }
        Barrel:applyForce(((Target:pos() + vec(0, 0, 36) - Barrel:massCenter()) * Barrel:pos():distance(Target:pos() + vec(0, 0, 36)) - Barrel:vel()) * Barrel:mass())
    }
}
