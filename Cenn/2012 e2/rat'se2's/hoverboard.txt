@name Rats fucking Hover Thing
@model models/props_junk/wood_pallet001a.mdl
@inputs Seat:wirelink Turret
@persist Speed TurnSpeed Friction:vector Force:vector Hover Dirz Jump Ground
runOnTick(1)
Mass = entity():mass()
if(first() | duped()){
    entity():setAlpha(255)
    entity():setMass(50000)

    Ground = 10


    Hover = Ground
    Dirz = 0
    Jump = 0
    TurnSpeed = 50
    #HOLOS
}
#FRONTS
Base = entity():pos()+entity():vel()/50
Ang = entity():angles()
H = 1
holoPos(H,Base+vec(0,40,0):rotate(Ang))
holoAng(H,Ang+ang(0,0,-90))
H = 2
holoPos(H,Base+vec(0,-40,0):rotate(Ang))
holoAng(H,Ang+ang(0,0,90))
H = 3
holoPos(H,Base+vec(0,0,0):rotate(Ang))
holoAng(H,Ang)
H = 4
holoPos(H,Base+vec(0,0,0):rotate(Ang))
holoAng(H,Ang)
H = 5
holoPos(H,Base+vec(0,0,1.5):rotate(Ang))
holoAng(H,Ang)
H = 6
holoPos(H,Base+vec(0,-37,1):rotate(Ang))
holoAng(H,Ang)
H = 7
holoPos(H,Base+vec(0,37,1):rotate(Ang))
holoAng(H,Ang)
#SOUNDS AND OTHER
soundPitch(1,100+entity():vel():distance(vec(0,0,0))/15)
if(Jump){
    Speed = 0
    Dirz -= 0.2
    if(Hover < Ground){
        Hover = Ground
        Jump = 0
        Dirz = 0
    }
    applyAngForce(-ang(entity():angles():pitch(),0,entity():angles():roll()*0.05)*Mass*10)
}else{
    Friction *= 0.8
    applyAngForce(-ang(entity():angles():pitch()*5,0,entity():angles():roll()*6)*Mass*10)
}
Hover += Dirz
Force = vec(0,0,Hover-entity():pos():z())
applyForce(($Force*10+Force*10)*Mass)
applyForce(Friction*Mass)
applyForce(entity():vel()*-Mass/10)
applyAngForce(entity():angVel()*-Mass*0.9)
if(Seat["Active",normal]){
    if(Seat["W",normal]){
        Forward = entity():right()*Speed
        Friction -= Forward
        applyAngForce(ang(0,0,TurnSpeed/0.9)*-entity():mass()*100)
    }
    if(Seat["S",normal]){
        Forward = entity():right()*Speed
        Friction += Forward
        applyAngForce(ang(0,0,TurnSpeed/0.9)*entity():mass()*100)
    }
    if(Seat["A",normal]){
        applyAngForce(ang(TurnSpeed/-3,TurnSpeed,0)*entity():mass()*100)
    }
    if(Seat["D",normal]){
        applyAngForce(ang(TurnSpeed/-3,TurnSpeed,0)*-entity():mass()*100)
    }
    if(Seat["Space",normal]){
        Friction += vec(0,0,1)
    }
    if(Seat["Shift",normal]){
        Speed = 700
    }else{
        Speed = 15
    }
    if(Seat["Space",normal] & Hover <= Ground){
        Dirz = 10
        Jump = 1
    }
}
