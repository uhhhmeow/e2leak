@name kaoz ultimate laz0r show
@inputs 
@outputs A
@persist B C XA:array YA:array
@trigger 

interval(10)
if (first()) {
    rangerFlags("EZ")
    XA = array()
    YA = array()
    for (X = 0, 50, 1) {
        XA[X,number]=random(0,360)
        YA[X,number]=random(0,360)
        holoCreate(X)
        holoModel(X,"hqsphere2")
        holoScale(X,vec(0.1,0.1,0.1))
        holoColor(X,vec(random(0),random(0),random(0)))
        holoEntity(X):setTrails(9,100,10,"trails/lol",vec(random(250),random(250),random(250)),100000) 
        holoAlpha(X,0)
    }
}
A = 0
for (X = 0, 30, 1) {
    XA[X,number]= XA[X,number] + random(-300,500)
    YA[X,number]= YA[X,number] + random(-300,300)
    RD = rangerAngle(80000,XA[X,number],YA[X,number])
    if (RD:entity():isAlive()) {A = 1}
    holoPos(X,RD:pos() + vec(cos(10)*40,sin(80)*10,tan(20)))
}
