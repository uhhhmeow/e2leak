@name Walking Sound - By Moglizorz
@outputs Speed Playing Song:string SSpeed P:entity I
interval(50)
runOnChat(1)
SSpeed=owner():vel():length()/2.5
if(P!=owner()){P=owner(),hint("------- Moglizorz Walking Sound -------",7),hint("Type !list for a list of songs/commands.",7)}
if(Speed<SSpeed-2){Speed=Speed+5}elseif(Speed>SSpeed+2){Speed=Speed-5}
if(!I){Speed=Speed-2,I=1}else{Speed=Speed+2,I=0}
if(SSpeed>=500){SSpeed=499}
if(Speed>=500){Speed=499}
if((lastSaid()=="!list")&(chatClk())&(lastSpoke()==owner())){
    hint("!1 - Ravebreak",7)
    hint("!2 - Benny Hill",7)
    hint("!3 - Call On Me",7)
    hint("!4 - Dance Now",7)
    hint("!5 - Hamster",7)
    hint("!6 - Hamster Rave",7)
    hint("!7 - Still Alive",7)
    hint("!8 - Bad Boys",7)
    hint("!9 - Beat It",7)
    hint("!10 - (GMan)Rise'n'Shine",7)
    hint("!11 - (GMan)Sleeping on The Job",7)
    hint("!12 - (GMan)Right Man, Wrong Place",7)
    hint("!13 - (GMan)Smell the Ashes",7)
    hint("!14 - (Griggori)Laugh",7)
    hint("!15 - Opera",7)
    hint("!16 - Zombie Breath",7)
    hint("!17 - Zombie Moan",7)
    hint("!18 - Up There!",7)
       
    }

if(owner():lastSaid()=="!1"){ Song="ravebreak.mp3"}
if(owner():lastSaid()=="!2"){ Song="bennyhill_v2.mp3"}
if(owner():lastSaid()=="!3"){ Song="aprilondynasty/callonme.mp3"}
if(owner():lastSaid()=="!4"){ Song="aprilondynasty/dancenow.mp3"}
if(owner():lastSaid()=="!5"){ Song="aprilondynasty/hamster.wav"}
if(owner():lastSaid()=="!6"){ Song="aprilondynasty/hamster2.wav"}
if(owner():lastSaid()=="!7"){ Song="music/portal_still_alive.mp3"}
if(owner():lastSaid()=="!8"){ Song="aprilondynasty/badboys.mp3"}
if(owner():lastSaid()=="!9"){ Song="aprilondynasty/beatit.mp3"}
if(owner():lastSaid()=="!10"){ Song="vo/gman_misc/gman_riseshine.wav"}
if(owner():lastSaid()=="!11"){ Song="vo/gman_misc/gman_02.wav"}
if(owner():lastSaid()=="!12"){ Song="vo/gman_misc/gman_03.wav"}
if(owner():lastSaid()=="!13"){ Song="vo/gman_misc/gman_04.wav"}
if(owner():lastSaid()=="!14"){ Song="vo/ravenholm/madlaugh03.wav"}
if(owner():lastSaid()=="!15"){ Song="ambient/opera.wav"}
if(owner():lastSaid()=="!16"){ Song="npc/zombie_poison/pz_breathe_loop2.wav"}
if(owner():lastSaid()=="!17"){ Song="npc/zombie/moan_loop1.wav"}
if(owner():lastSaid()=="!18"){ Song="vo/npc/female01/upthere01.wav"}

 
if((Speed>0)&(!Playing)){owner():soundPlay(1,0,Song),Playing=1}
elseif((Speed<=0)&(Playing)){soundStop(1),Playing=0}
soundPitch(1,Speed)
