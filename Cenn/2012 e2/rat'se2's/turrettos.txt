@name turrettos
@inputs 
@outputs Fire Reload
@persist Turret:entity 
@trigger 
Turret = entity():isWeldedTo()
 runOnTick(1)


if(owner():keyReload()){
Turret:setPos(owner():pos()+ vec(0,0,150))
Turret:setAng(owner():eyeAngles()+ang(0,0,0))
Turret:setMass(1)

}
if (owner():keyAttack1()) {
    Fire=1
}

else{Fire=0}

if (owner():keyAttack2()) {
    Reload=1
}

else{Reload=0}
