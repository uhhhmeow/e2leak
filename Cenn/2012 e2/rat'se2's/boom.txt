@name 
@outputs ChatAr:array
@persist [O Target Ent]:entity Targ Count
@persist Use Reload M1 M2 Fire

if(first())
{
    runOnTick(1)
    runOnChat(1)
    
    O = owner()
}

Use = O:keyUse()
Reload = O:keyReload()
M1 = O:keyAttack1()
M2 = O:keyAttack2()

if(Use)
{
    if(!Fire)
    {
        Count = numPlayers()
        
        if(changed(M1) & M1 & Targ != Count)
        {
            Targ++
        }
        elseif(changed(M2) & M2 & Targ != 1)
        {
            Targ--
        }
        
        if(changed(Targ) & Targ)
        {
            Target = players()[Targ,entity]
            
            print("[" + Targ + "/" + Count + "]Target: " + Target:name())
        }
    }
    
    if(changed(Reload) & Reload)
    {
        Fire = !Fire
        
        if(Fire == 1)
        {
            print("Fire On")
        }
        else
        {
            print("Fire Off")
        }
    }
}

if(Fire)
{   
    if(Target & Target:isAlive())
    {
        if(!Ent)
        {
            propSpawnEffect(0)
            
            Ent = propSpawn("models/props_phx/ww2bomb.mdl", Target:pos() + vec(0, 0, 50), 1)
            
            Ent:propNotSolid(1)
            Ent:setAlpha(0)
            Ent:propBreak()
        }
    }
}
