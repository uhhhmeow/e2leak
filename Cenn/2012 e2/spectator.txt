@name Croco's Spectator
@inputs Control:wirelink Camera:wirelink
@persist FSpeed RSpeed USpeed W A S D Space Shift Position:vector Null:entity
#Control Description
#Press W and S to move forwards and backwards.
#Press A and D to move sideways.
#Press Space and Shift to move up and down.
#Hold the Right Mouse button to move at the "Boost" speed.
#Use the Mouse to look around.

#To change things like movement speed and appearance, jump down to CONFIGURATION.

runOnTick(1)
Null = holoEntity(0)



if(first()){
    holoCreate(0,Position,vec(0,0,0))}



#Wirelink to Adv Pod Controller
W = Control:number("W")
A = Control:number("A")
S = Control:number("S")
D = Control:number("D")
Mouse1 = Control:number("Mouse1")
Mouse2 = Control:number("Mouse2")
Space = Control:number("Space")
Shift = Control:number("Shift")
R = Control:number("R")
#End Adv Pod Controller Wirelink



#CONFIGURATION
Size =  vec(1,1,1)#Model Size
Boost = 1.25#Number is a percentage of bonus speed added when Mouse2 is pressed.   
Speed = 0.3#Number is the default speed.
Color = vec(125,125,255)#Color of the hologram, in the style vec(red,green,blue)
Alpha = 255#Transparency of the model, 255 is solid, 0 is completely transparent.
Model = "sphere3"#Hologram model; Check the bottom of the code for the list.
Material = "models/wireframe"#Material used on the model. Check the material in the Material tool for the path of the material you want to use.
Offset = vec(-25,0,7.5)#Position of camera based on Holo position. Set to 0,0,0 for Holograms point of view.
#END OF CONFIGURATION



#Wirelink to Camera Controller
Camera:setNumber("Activated",Control:number("Active"))
Camera:setVector("Position", Position + Offset:x()*owner():eye() + Offset:y()*Null:right() + Offset:z()*Null:up() + owner():eye()*FSpeed + Null:right()*RSpeed + Null:up()*USpeed)
Camera:setVector("Direction", owner():eye())
#End Camera Controller Wirelink

if(first()){
            holoCreate(1,Position,Size,ang(),Color)
            holoModel(1,Model)
            holoMaterial(1,Material)
            }



Thrust = (W - S) * (Speed+(Mouse2*Boost))
Strafe = (D - A) * (Speed+(Mouse2*Boost))
Hover = (Space - Shift) * (Speed+(Mouse2*Boost))
FSpeed += Thrust - FSpeed/20
RSpeed += Strafe - RSpeed/20
USpeed += Hover - USpeed/20
if(Control:number("Active") == 0){ 
                                            Position = entity():pos()+entity():up()*8
                                            FSpeed = 0
                                            RSpeed = 0
                                        USpeed = 0}



holoAng(0,owner():eye():toAngle())
holoAng(1,ang(random(360),random(360),random(360)))
Position = Position + Null:forward()*FSpeed + Null:right()*RSpeed + Null:up()*USpeed
holoPos(0,Position)
holoPos(1,Position)



#List of hologram models
#cone, cube, cylinder, icosphere, icosphere2, icosphere3, prism, pyramid, sphere, sphere2, sphere3, torus, torus2, torus3
#Keep in mind that the list might not be current, as new things may and probably will be added.
#For the current list of hologram models, check "http://wiki.garrysmod.com/?title=Wire_Expression2#3D_Holograms
