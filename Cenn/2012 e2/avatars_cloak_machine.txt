@name Cloak Machine
@inputs 
@outputs 
@persist 
@trigger
interval(1) 
runOnTick(1)
O=owner()
hideChat(1)
if (O:lastSaid() == "!active") {
    entity():isWeldedTo():setMaterial("models/shadertest/predator")
    O:setMaterial("models/shadertest/predator")
}

if (O:lastSaid() == "!unactive") {
    entity():isWeldedTo():setMaterial("")
    O:setMaterial("")
}
entity():isWeldedTo():getConstraints()
