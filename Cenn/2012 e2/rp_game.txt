@name The game
@inputs Reset RPU RPD BPU BPD RS BS BE RE 
@outputs RHP REHP RMANA RPOWER RTURN:string RSELECT:string BHP BEHP BMANA BPOWER BTURN:string BSELECT:string 
@persist Turn:string Things:array
@trigger Reset RPU RPD BPU BPD RS BS BE RE Wut
Things=array("Attack","Heal","Mana Boost")
if (first())
{
    StartingMana = 10
    StartingHP = 100
    Wut=0
    RHP=StartingHP
    BHP=StartingHP
    RMANA=StartingMana
    BMANA=StartingMana
    BEHP=RHP
    REHP=BHP
    RTURN="Your turn!"
    BTURN="Red's turn!"
    Turn="Red"
    BSELECT=Things[1,string]
    RSELECT=Things[1,string]
    MaxPower = 15
}

if (Reset==1)
{
        StartingMana = 10
    StartingHP = 100
    RHP=StartingHP
    BHP=StartingHP
    RMANA=StartingMana
    BMANA=StartingMana
    BEHP=RHP
    REHP=BHP
    RTURN="Your turn!"
    BTURN="Red's turn!"
    Turn="Red"
    BSELECT=Things[1,string]
    RSELECT=Things[1,string]
}
if (RS==1 & Turn=="Red")
{
    if (RSELECT==Things[1,string] & Wut==0)
    {
        Wut=1
        RSELECT=Things[2,string]
    }
    if (RSELECT==Things[3,string] & Wut==0)
    {
        Wut=1
        RSELECT=Things[1,string]
    }
    if (RSELECT==Things[2,string] & Wut==0)
    {
        Wut=1
        RSELECT=Things[3,string]
    }
    Wut=0
}
if (BS==1 & Turn=="Blue")
{
    if (BSELECT==Things[1,string] & Wut==0)
    {
        Wut=1
        BSELECT=Things[2,string]
    }
    if (BSELECT==Things[3,string] & Wut==0)
    {
        Wut=1
        BSELECT=Things[1,string]
    }
    if (BSELECT==Things[2,string] & Wut==0)
    {
        Wut=1
        BSELECT=Things[3,string]
    }
    Wut=0
}
if (BPU==1)
{
    if (BPOWER<MaxPower & BPOWER < BMANA)
    {
        BPOWER=BPOWER+10
    }
}
if (BPD==1)
{
    if (BPOWER>0)
    {
        BPOWER=BPOWER-10
    }
}
if (RPU==1)
{
    if (RPOWER<MaxPower & RPOWER < RMANA)
    {
        RPOWER=RPOWER+10
    }
}
if (RPD==1)
{
    if (RPOWER>0)
    {
        RPOWER=RPOWER-10
    }
}

if (RE==1 & Turn=="Red")
{
    if (RSELECT=="Attack")
    {
        BHP=BHP-RPOWER
        RMANA=RMANA-RPOWER
    }
    if (RSELECT=="Heal")
    {
        RHP=RHP+RPOWER
        RMANA=RMANA-RPOWER
    }
    if (RSELECT=="Mana Boost")
    {
        RMANA=RMANA+40
    }
    RMANA=RMANA+10
    BMANA=BMANA+10
    Turn="Blue"
    RPOWER=0
    BEHP=RHP
    REHP=BHP
    RTURN="Blue's turn!"
    BTURN="Your turn!"
}
if (BE==1 & Turn=="Blue")
{
    if (BSELECT=="Attack")
    {
        RHP=RHP-BPOWER
        BMANA=BMANA-BPOWER
    }
    if (BSELECT=="Heal")
    {
        BHP=BHP+BPOWER
        BMANA=BMANA-BPOWER
    }
    if (BSELECT=="Mana Boost")
    {
        BMANA=BMANA+40
    }
    RMANA=RMANA+10
    BMANA=BMANA+10
    Turn="Red"
    RTURN="Your turn!"
    BTURN="Red's turn!"
    RPOWER=0
    BEHP=RHP
    REHP=BHP
}
if (BHP<0)
{
    Turn="nope"
    BHP=0
    REHP=0
    BTURN="RED WON!"
    RTURN="RED WON!"
}
if (RHP<0)
{
    Turn="nope"
    RHP=0
    BEHP=0
    BTURN="BLUE WON!"
    RTURN="BLUE WON!"
}
