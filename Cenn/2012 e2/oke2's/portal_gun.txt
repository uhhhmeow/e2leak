@name Wrench
@inputs 
@outputs 
@persist 
@trigger 
runOnTick(1)
if(first()){
concmd("say This Wrench Was made by Rat Man")    
    Head="anim_attachment_rh"
    
    for(I=1,1){
        holoCreate(I)
        holoPos(I,owner():attachmentPos(Head))
        holoAng(I,owner():attachmentAng(Head))
        holoPos(I,holoEntity(I):toWorld(vec(0,0,-2+(-I*0))))
        holoAng(I,holoEntity(I):toWorld(ang(0,10,(-I*10)-0)))
        holoParentAttachment(I,owner(),Head)
        holoModel(I,"models/weapons/c_models/c_spikewrench/c_spikewrench.mdl")
        holoMaterial(I,"models/weapons/c_models/c_spikewrench/c_spikewrench.mdl")
        holoScale(I,vec(1,1,1))
    }
}
owner():weapon():setColor(0,0,0,0)
