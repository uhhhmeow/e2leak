@name Aaron's Holo Slaying Sphere
@outputs Find
@persist Chat:array Imune:array Target:array Range Search Range2 Target2:array 
if (first()|duped()){    
    if (gGetNum("Said")==0){
    concmd("say Aaron's Slay Bubble, was spawned")}    gSetNum("Said",1)    
     if (gGetStr("Imune")){Imune=gGetStr("Imune"):explode(" ")}    
        runOnChat(1)   
        ### Holo's
         entity():setAlpha(0)        
        holoCreate(10)    
        holoCreate(11)    
        holoScale(10,vec(10,10,10))    
        holoModel(10,"hqicosphere2")    
        holoMaterial(10,"models/screenspace")    
        holoAlpha(100,255)    
        holoColor(255,vec(0,0,0))    
        holoScale(101,vec(-99.99,-99.99,-99.99))    
        holoModel(101,"icosphere2")    
        holoAlpha(101,100)    
        for (I=100,101){        
        holoParent(I,entity())    }   }     
                interval(500)
                if (findCanQuery()){    
                    if (Search==0){        
                        findClearBlackList()        
                        findClearWhiteList()        
                        findExcludePlayer(owner())        
                        if (Imune[1,string]){            
                            for (I=1,Imune:count()){                
                                findExcludePlayer(Imune[I,string])            }        }        
                                findByClass("player")            
                                findInSphere(entity():pos(),1200)        
                                Target=findToArray()        
                                Search=1    }    
                                elseif (Search==1)    {        
                                    findClearBlackList()        
                                    findClearWhiteList()        
                                    findExcludePlayerProps(owner())        
                                    findByModel("models/dav0r/camera.mdl")        
                                    Target2=findToArray()        
                                    Search=0    }}
                                    interval(250)
                                    for (I=1,Target:count()){    
                                        Range=(Target[I,entity]:pos()+vec(0,0,30)):distance(entity():pos())    
                                        if (Target[I,entity]:isAlive() & Range<150 & Target[I,entity]:isPlayer() & Target[I,entity]:team()>owner():team()){        
                                            concmd("ulx unragdoll \""+Target[I,entity]:name()+"\"")        
                                            concmd("ulx slay \""+Target[I,entity]:name()+"\"")    }}
                                            interval(10)
                                            for (I=1,Target2:count()){    
                                                Range2=Target2[I,entity]:pos():distance(entity():pos())    
                                                if (Range2<350){        
                                                    holoCreate(I)        
                                                    holoPos(I,Target2[I,entity]:pos())        
                                                    holoAng(I,Target2[I,entity]:angles())        
                                                    holoParent(I,Target2[I,entity])        
                                                    holoMaterial(I,"models/alyx/emptool_glow")        
                                                    holoScale(I,vec(-3,-2,-2))    }    
                                                    else{holoDelete(I)    }}
                                                    interval(250)
                                                    Chat=owner():lastSaid():explode(" ")
                                                    if (chatClk()){    
                                                        if (lastSpoke():owner()){                
                                                            if (Chat[1,string]=="!add" & Chat[2,string]){            
                                                                Imune:pushString(Chat[2,string])            
                                                                gSetStr("Imune",Imune:concat(" "))            
                                                                hint("[Sphere] Added:  "+Chat[2,string],7)        }                
                                                                if (Chat[1,string]=="!remove"){            
                                                                    for (I=1,Imune:count()){                
                                                                        if (Imune[I,string]:find(Chat[2,string])>0){                    
                                                                            hint("[Sphere] Removed: \""+Imune[I,string]+"\"",7)                    
                                                                            Imune:remove(I)                    
                                                                            gSetStr("Imune",Imune:concat(" "))                }                
                                                                            elseif (Imune[I,string]:find(Chat[2,string])>0)                {                    
                                                                                hint("[Sphere] Name \""+Chat[2,string]+"\" not found",7)                }            }        }                
                                                                                if (Imune:count()>0){            
                                                                                    if (Chat[1,string]=="!imune"){                
                                                                                        hint("[Sphere] Imune:  "+Imune:concat(", "),7)            }        }        
                                                                                        if (Imune:count()==0 & Chat[1,string]=="!imune"){            
                                                                                            hint("[Sphere] Imune:  <none>",7)        }                
                                                                                            if (entity():getAlpha()==255 & Chat[1,string]=="!hidee" | entity():getAlpha()==255 & Chat[1,string]=="!hide"){            
                                                                                                entity():setAlpha(0)            
                                                                                                hint("[Sphere]  Cloaking",7)        }
                                                                                                elseif(entity():getAlpha()==0 & Chat[1,string]=="!hidee" | entity():getAlpha()==0 & Chat[1,string]=="!hide"){            
                                                                                                    hint("[Sphere]  Already Cloaked",7)        }                
                                                                                                    if (entity():getAlpha()==0 & Chat[1,string]=="!showe" | entity():getAlpha()==0 & Chat[1,string]=="!show"){            
                                                                                                        entity():setAlpha(255)            
                                                                                                        hint("[Sphere]  Uncloaking",7)        }
                                                                                                        elseif(entity():getAlpha()==255 & Chat[1,string]=="!showe" | entity():getAlpha()==255 & Chat[1,string]=="!show"){            
                                                                                                            hint("[Sphere] Already Uncloaked",7)        }                
                                                                                                            if (Chat[1,string]=="!reset"){            
                                                                                                                hint("[Sphere] Reseting",7)            
                                                                                                                gSetNum("Said",0)            
                                                                                                                reset()        }                
                                                                                                                if (Chat[1,string]=="!resetall"){            
                                                                                                                    hint("[Sphere] Reseting Everything",7)            
                                                                                                                    gSetNum("Said",0)            
                                                                                                                    gSetStr("Imune","")            
                                                                                                                    reset()        }                
                                                                                                                    if (Chat[1,string]=="!ops"){            
                                                                                                                        hint("[Sphere]: "+ops():toString()+" ops",7)        }                
                                                                                                                        if (Chat[1,string]=="!help" ){            
                                                                                                                            printColor("!add <name> - Adds a player to Imune list")    
                                                                                                                            printColor("!remove <# on Imune list> <name> - Removes a player")            
                                                                                                                            printColor("!imune - Shows people added to Imune list")            
                                                                                                                            printColor("!hide or !hidee - Makes E2 invisable")            
                                                                                                                            printColor("!showe or !show - Makes the E2 visable")            
                                                                                                                            printColor("!reset - Resets the E2, but Imune list stays")            
                                                                                                                            printColor("!ops - Shows the current ops of the E2")        }                
                                                                                                                            if (Chat[1,string]=="!destruct"){            
                                                                                                                                hint("Self Destructing",7)            
                                                                                                                                selfDestructAll()        }    }         }
                                                                                                                                interval(1000)
                                                                                                                                if (!owner()){selfDestruct()}
