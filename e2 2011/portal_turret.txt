@name Portal Turret
@inputs [LTurret RTurret]:wirelink
@outputs Local:vector
@persist Over Dist C2 Zv Speed Scale Open Move Extra [AimAng Pitch]:angle Cos T:table Number
@persist [B H O E Tmp LT RT]:entity [Angle Ang]:angle [Cut Holoscale Tcol]:vector Active
@model models/hunter/blocks/cube05x1x025.mdl

## Portal Turret Replica by OmicroN ##
### You can enable/disable the turret by pressing your Use key on it ###
## To use the turrets, wire each Wirelink to a wire turret and reload the chip ##
## I suggest that you nocollide all the turret before hand as well, or else it will collide ##

if(first() | duped()){
    runOnTick(1)
    runOnLast(1)
    runOnChat(1)
E = entity()
E:setMass(251)

LT = LTurret:entity()
RT = RTurret:entity()
LT:setAlpha(0)
RT:setAlpha(0)

Scale = 0.7
Cut = vec(0,5+1.5,0)*Scale
Speed = 6
Ang = Angle = ang(0,0,90)
Off = 0.2*Scale , Hole = 9.25
T["clip1a",array] = array(vec(0,Off,0),vec(0,0,Off),vec(0,0,-Hole)*Scale,Cut)
T["clip1b",array] = array(vec(0,1,0),vec(0,0,1),vec(-1,1,1),vec(0,-1,0))

T["clip2a",array] = array(vec(0,-Off,0),vec(0,0,-Off),vec(0,0,Hole)*Scale,-Cut)
T["clip2b",array] = array(vec(0,-1,0),vec(0,0,-1),vec(-1,-1,-1),vec(0,1,0))

T["clip3a",array] = array(vec(0,-Off,0),vec(0,0,Off),vec(0,0,-Hole)*Scale,-Cut)
T["clip3b",array] = array(vec(0,-1,0),vec(0,0,1),vec(-1,-1,1),vec(0,1,0))

T["clip4a",array] = array(vec(0,Off,0),vec(0,0,-Off),vec(0,0,Hole)*Scale,Cut)
T["clip4b",array] = array(vec(0,1,0),vec(0,0,-1),vec(-1,1,-1),vec(0,-1,0))

Holoscale = vec(28,20,60)*Scale

N = 0
for(I=1,4){
    holoCreate(I,E:pos(),Holoscale/12,E:toWorld(Ang))
    if(I==1|I==3){
        N++
        holoCreate(4+N,E:pos(),Holoscale/12,E:toWorld(Ang))
        holoModel(4+N,"hq_sphere")
        #holoParent(4+N,1)
            holoCreate(6+N,E:pos(),-Holoscale/12,E:toWorld(Ang),vec()+70)
            holoModel(6+N,"hq_sphere")
            holoParent(6+N,4+N)
    }
    for(I2=1,4){
        holoClipEnabled(I,I2,1)
        holoClip(I,I2,T["clip"+I+"a",array][I2,vector],T["clip"+I+"b",array][I2,vector],0)
    }
    holoModel(I,"hq_sphere")
    if(I!=1){
        holoParent(I,1)
    }else{
        holoParent(I,E)
    }
}

B = holoEntity(1)

holoClipEnabled(3,5,1)
holoClip(3,5,vec(0,0,22)*Scale,vec(0,1.5,-1),0)

holoClipEnabled(1,5,1)
holoClip(1,5,vec(0,0,22)*Scale,vec(0,-1.5,-1),0)

#Bottom Front Clips#
holoClipEnabled(5,1,1)
holoClipEnabled(5,2,1)
holoClipEnabled(5,3,1)
holoClipEnabled(5,4,1)
holoClip(5,1,vec(0,0,22.5)*Scale,vec(0,-1.5,1),0)
holoClip(5,2,vec(0,-0.2,0)*Scale,vec(0,-1,0),0)
holoClip(5,3,-Cut,vec(0,1,0),0)
holoClip(5,4,vec(0.1,0,0),vec(1,0,0),0)

    holoClipEnabled(6,1,1)
    holoClipEnabled(6,2,1)
    holoClipEnabled(6,3,1)
    holoClipEnabled(6,4,1)
    holoClip(6,1,vec(0,0,22.5)*Scale,vec(0,1.5,1),0)
    holoClip(6,2,vec(0,0.2,0)*Scale,vec(0,1,0),0)
    holoClip(6,3,Cut,vec(0,-1,0),0)
    holoClip(6,4,vec(0.1,0,0),vec(1,0,0),0)

holoClipEnabled(7,1,1)
holoClipEnabled(7,2,1)
holoClipEnabled(7,3,1)
holoClipEnabled(7,4,1)
holoClip(7,1,vec(0,0,22.5)*Scale,vec(0,-1.5,1),0)
holoClip(7,2,vec(0,-0.2,0)*Scale,vec(0,-1,0),0)
holoClip(7,3,-Cut,vec(0,1,0),0)
holoClip(7,4,vec(0.1,0,0),vec(1,0,0),0)

    holoClipEnabled(8,1,1)
    holoClipEnabled(8,2,1)
    holoClipEnabled(8,3,1)
    holoClipEnabled(8,4,1)
    holoClip(8,1,vec(0,0,22.5)*Scale,vec(0,1.5,1),0)
    holoClip(8,2,vec(0,0.2,0)*Scale,vec(0,1,0),0)
    holoClip(8,3,Cut,vec(0,-1,0),0)
    holoClip(8,4,vec(0.1,0,0),vec(1,0,0),0)
##//Bottom Front Clips\\##

holoCreate(9,E:pos(),(Holoscale-0.15*Scale)/12,E:toWorld(Ang),vec()+70)
holoClipEnabled(9,1,1)
holoClipEnabled(9,2,1)
holoClipEnabled(9,3,1)
holoClipEnabled(9,4,1)
holoClip(9,1,Cut,vec(0,-1,0),0)
holoClip(9,2,-Cut,vec(0,1,0),0)
holoClip(9,3,vec(0,0,22.5)*Scale,vec(0,1.5,-1),0)
holoClip(9,4,vec(0,0,22.5)*Scale,vec(0,-1.5,-1),0)
holoModel(9,"hq_sphere")
holoParent(9,1)

holoCreate(10,E:pos(),-Holoscale/12,E:toWorld(Ang),vec()+70)
holoClipEnabled(10,1,1)
holoClipEnabled(10,2,1)
holoClipEnabled(10,3,1)
holoClipEnabled(10,4,1)
holoClip(10,1,Cut,vec(0,-1,0),0)
holoClip(10,2,-Cut,vec(0,1,0),0)
holoClip(10,3,vec(0,0,22)*Scale,vec(0,1.5,-1),0)
holoClip(10,4,vec(0,0,22)*Scale,vec(0,-1.5,-1),0)
holoModel(10,"hq_sphere")
holoParent(10,1)

Tmpscale = Holoscale - vec(0.1,0,0.1)
    holoCreate(11,E:pos(),Tmpscale/12,E:toWorld(Ang))
    holoClipEnabled(11,1)
    holoClip(11,Cut,vec(0,1,0),0)
    holoModel(11,"hq_sphere")
    
    holoCreate(12,E:pos(),-Tmpscale/12,E:toWorld(Ang),vec()+70)
    holoClipEnabled(12,1)
    holoClip(12,Cut,vec(0,1,0),0)
    holoModel(12,"hq_sphere")
    holoParent(12,11)

holoCreate(13,E:pos(),Tmpscale/12,E:toWorld(Ang))
holoClipEnabled(13,1)
holoClip(13,-Cut,vec(0,-1,0),0)
holoModel(13,"hq_sphere")

holoCreate(14,E:pos(),-Tmpscale/12,E:toWorld(Ang),vec()+70)
holoClipEnabled(14,1)
holoClip(14,-Cut,vec(0,-1,0),0)
holoModel(14,"hq_sphere")
holoParent(14,13)

for(I=1,14){
    if(holoEntity(I):getColor()==vec()+255){
        holoMaterial(I,"models/shiny")
    }
}
    
E:setAlpha(0)
E:setMass(250)

H = holoCreate(15,E:pos(),vec(6,6,6)*Scale/12,E:toWorld(Ang))
holoModel(15,"hq_sphere")
holoMaterial(15,"phoenix_storms/black_chrome")
holoParent(15,9)

holoParent(11,15)
holoParent(13,15)

holoCreate(16,E:toWorld(vec(13.45,0,0)*Scale),Scale*vec(5.5,5.5,4)/12,E:toWorld(ang(90,0,0)))
holoModel(16,"hq_torus_thick")
holoColor(16,vec(255,0,0))
holoDisableShading(16,1)
holoMaterial(16,"phoenix_storms/middle")
holoParent(16,9)

holoCreate(17,E:toWorld(vec(13.8,0.2,0)*Scale),Scale*vec(9,9,1)/12,E:toWorld(ang(90,0,0)))
holoAng(17,holoEntity(17):toWorld(ang(0,-90,0)))
holoModel(17,"plane")
holoColor(17,vec(255,0,0))
holoDisableShading(17,1)
holoMaterial(17,"models/roller/rollermine_glow")
holoParent(17,9)

    ### Turret 1 ####
    holoCreate(18,E:toWorld(vec(0,0,4.55)*Scale),Scale*vec(14,14,4.2)/12,E:toWorld(ang(0,0,0)),vec()+120)
    holoClipEnabled(18,1)
    holoClip(18,vec(6.9,0,0)*Scale,vec(-1,0,0),0)
    holoModel(18,"hq_rcube")
    holoMaterial(18,"phoenix_storms/mat/mat_phx_carbonfiber")
    holoParent(18,11)
    
    holoCreate(19,E:toWorld(vec(0,0,4.55)*Scale),Scale*-vec(14,14,4.2)/12,E:toWorld(ang(0,0,0)),vec()+120)
    holoClipEnabled(19,1)
    holoClip(19,vec(6.9,0,0)*Scale,vec(-1,0,0),0)
    holoModel(19,"hq_rcube")
    holoMaterial(19,"models/shiny")
    holoParent(19,18)
    
    holoCreate(20,E:toWorld(vec(9.8,0,4.55)*Scale),Scale*-vec(11.2,13.3,4.2)/12,E:toWorld(ang(0,0,0)),vec()+50)
    holoClipEnabled(20,1)
    holoClip(20,vec(-4,0,0)*Scale,vec(-1,0,0),0)
    holoModel(20,"hq_rcube")
    holoMaterial(20,"models/shiny")
    holoParent(20,18)
    
    holoCreate(21,E:toWorld(vec(5,2,4.55)*Scale),Scale*vec(3,3,6)/12,E:toWorld(ang(90,0,0)))
    holoModel(21,"hq_tube_thick")
    holoMaterial(21,"phoenix_storms/iron_rails")
    holoParent(21,18)
    
    holoCreate(22,E:toWorld(vec(5,-2,4.55)*Scale),Scale*vec(3,3,8)/12,E:toWorld(ang(90,0,0)))
    holoModel(22,"hq_tube_thick")
    holoMaterial(22,"phoenix_storms/iron_rails")
    holoParent(22,18)

### Turret 2 ####
holoCreate(23,E:toWorld(vec(0,0,-4.55)*Scale),Scale*vec(14,14,4.2)/12,E:toWorld(ang(0,0,0)),vec()+120)
holoClipEnabled(23,1)
holoClip(23,vec(6.9,0,0)*Scale,vec(-1,0,0),0)
holoModel(23,"hq_rcube")
holoMaterial(23,"phoenix_storms/mat/mat_phx_carbonfiber")
holoParent(23,13)

holoCreate(24,E:toWorld(vec(0,0,-4.55)*Scale),Scale*-vec(14,14,4.2)/12,E:toWorld(ang(0,0,0)),vec()+120)
holoClipEnabled(24,1)
holoClip(24,vec(6.9,0,0)*Scale,vec(-1,0,0),0)
holoModel(24,"hq_rcube")
holoMaterial(24,"models/shiny")
holoParent(24,23)

holoCreate(25,E:toWorld(vec(9.8,0,-4.55)*Scale),Scale*-vec(11.2,13.3,4.2)/12,E:toWorld(ang(0,0,0)),vec()+50)
holoClipEnabled(25,1)
holoClip(25,vec(-4,0,0)*Scale,vec(-1,0,0),0)
holoModel(25,"hq_rcube")
holoMaterial(25,"models/shiny")
holoParent(25,23)

holoCreate(26,E:toWorld(vec(5,2,-4.55)*Scale),Scale*vec(3,3,6)/12,E:toWorld(ang(90,0,0)))
holoModel(26,"hq_tube_thick")
holoMaterial(26,"phoenix_storms/iron_rails")
holoParent(26,23)


holoCreate(27,E:toWorld(vec(5,-2,-4.55)*Scale),Scale*vec(3,3,8)/12,E:toWorld(ang(90,0,0)))
holoModel(27,"hq_tube_thick")
holoMaterial(27,"phoenix_storms/iron_rails")
holoParent(27,23)

#holoPos(15,E:toWorld(vec(0,0,150)))

Move = 180

timer("extra",1000)
}

if(clk("extra")){
    Extra++
    if(Extra==1){
        for(I=1,2){
            holoCreate(27+I,E:pos(),Holoscale/12,E:toWorld(Ang))
            holoModel(27+I,"hq_sphere")
            holoMaterial(27+I,"models/shiny")
            holoParent(27+I,I>1 ? 28 : 9)
                holoCreate(29+I,E:pos(),-Holoscale/12,E:toWorld(Ang),vec()+70)
                holoModel(29+I,"hq_sphere")
                holoParent(29+I,28)
        }
        #Bottom Back Clips#
        holoClipEnabled(28,1,1)
        holoClipEnabled(28,2,1)
        holoClipEnabled(28,3,1)
        holoClipEnabled(28,4,1)
        holoClip(28,1,vec(0,0,22.5)*Scale,vec(0,-1.5,1),0)
        holoClip(28,2,vec(),vec(0,-1,0),0)
        holoClip(28,3,-Cut,vec(0,1,0),0)
        holoClip(28,4,vec(-0.1,0,0)*Scale,vec(-1,0,0),0)
        
            holoClipEnabled(29,1,1)
            holoClipEnabled(29,2,1)
            holoClipEnabled(29,3,1)
            holoClipEnabled(29,4,1)
            holoClip(29,1,vec(0,0,22.5)*Scale,vec(0,1.5,1),0)
            holoClip(29,2,vec(),vec(0,1,0),0)
            holoClip(29,3,Cut,vec(0,-1,0),0)
            holoClip(29,4,vec(-0.1,0,0)*Scale,vec(-1,0,0),0)
        
        holoClipEnabled(30,1,1)
        holoClipEnabled(30,2,1)
        holoClipEnabled(30,3,1)
        holoClipEnabled(30,4,1)
        holoClip(30,1,vec(0,0,22.5)*Scale,vec(0,-1.5,1),0)
        holoClip(30,2,vec(),vec(0,-1,0),0)
        holoClip(30,3,-Cut,vec(0,1,0),0)
        holoClip(30,4,vec(-0.1,0,0)*Scale,vec(-1,0,0),0)
        
            holoClipEnabled(31,1,1)
            holoClipEnabled(31,2,1)
            holoClipEnabled(31,3,1)
            holoClipEnabled(31,4,1)
            holoClip(31,1,vec(0,0,22.5)*Scale,vec(0,1.5,1),0)
            holoClip(31,2,vec(),vec(0,1,0),0)
            holoClip(31,3,Cut,vec(0,-1,0),0)
            holoClip(31,4,vec(-0.1,0,0)*Scale,vec(-1,0,0),0)
        ##//Bottom Back Clips\\##
        
        ## Rear leg controls ##
        holoCreate(32,E:toWorld(vec(-10.25,-15.6,0)*Scale),Scale*vec(1.643,1.643,5.714)/12,E:toWorld(ang(0,0,0)),vec()+70)
        holoModel(32,"hq_cylinder")
        holoParent(32,1)
        
        holoCreate(33,E:toWorld(vec(-9+1,-12-12+1,2.5)*Scale),Scale*(vec(11,12,12)/0.7)/12,E:toWorld(ang(-24,0,0)))
        holoModel(33,"hq_torus_thin")
        holoMaterial(33,"phoenix_storms/black_chrome")
        holoClipEnabled(33,1,1)
        holoClipEnabled(33,2,1)
        holoClip(33,1,vec(0,0.357,1.071)*Scale,vec(0,1.4,-2.4),0)
        holoClip(33,2,vec(-2.857,0,0)*Scale,vec(1,0,0),0)
        holoParent(33,32)
        
        holoCreate(34,E:toWorld(vec(-9+1,-12-12+1,-2.5)*Scale),Scale*(vec(11,12,12)/0.7)/12,E:toWorld(ang(24,0,0)))
        holoModel(34,"hq_torus_thin")
        holoMaterial(34,"phoenix_storms/black_chrome")
        holoClipEnabled(34,1,1)
        holoClipEnabled(34,2,1)
        holoClip(34,1,vec(0,0.357,-1.071)*Scale,vec(0,1.4,2.4),0)
        holoClip(34,2,vec(-2.857,0,0)*Scale,vec(1,0,0),0)
        holoParent(34,32)
        
        ## Front Right Leg controls ##
        
        Tmp = holoCreate(35,E:toWorld(vec(10.25-0.4,-12.2,1.75)*Scale),Scale*(vec(1.15,1.15,2)/0.7)/12,E:toWorld(ang(0,0,0)),vec()+70)
        holoModel(35,"hq_cylinder")
        holoParent(35,1)
        
        Tmp = holoCreate(36,Tmp:toWorld(vec(-6.25,3,0)*Scale),Scale*(vec(10,14,14)/0.7)/12,E:toWorld(ang(0,0,0)))
        holoModel(36,"hq_torus_thin")
        holoMaterial(36,"phoenix_storms/black_chrome")
        holoClipEnabled(36,1,1)
        holoClipEnabled(36,2,1)
        holoClip(36,1,vec(0,-2.857,0)*Scale,vec(0,1,0),0)
        holoClip(36,2,vec(0,0,0),vec(1,0,0),0)
        holoParent(36,35)
        
        Tmp = holoCreate(37,Tmp:toWorld(vec(0,9.5,0)*Scale),Scale*(vec(1.15,1.15,1.5)/0.7)/12,E:toWorld(ang(0,0,0)),vec()+70)
        holoModel(37,"hq_cylinder")
        holoParent(37,36)
        
        holoUnparent(28)
        holoParent(28,33)
        
        timer("extra",1000)
    }elseif(Extra==2){
        holoCreate(38,Tmp:toWorld(vec(-0.5,-0.5,0)*Scale),Scale*vec(2,2,1.5)/12,Tmp:toWorld(ang(90,0,-50)),vec()+70)
        holoModel(38,"hq_cylinder")
        holoParent(38,37)
    
        Tmp = holoCreate(39,Tmp:toWorld(vec(6.25,-9,0)*Scale),Scale*vec(20,26,14)/12,E:toWorld(ang(0,0,0)))
        holoModel(39,"hq_torus_thin")
        holoMaterial(39,"phoenix_storms/black_chrome")
        holoClipEnabled(39,1,1)
        holoClipEnabled(39,2,0)
        holoClip(39,1,vec(-4.286,0,0)*Scale,vec(-1,-0.25,0),0)
        holoClip(39,2,vec(0,0,0),vec(1,0,0),0)
        holoParent(39,38)
        
        holoCreate(40,Tmp:toWorld(vec(-1.5,-11.9-0.3,0)*Scale),Scale*vec(2,2,3)/12,Tmp:toWorld(ang(90,0,20)),vec()+70)
        holoClipEnabled(40,1)
        holoClip(40,vec(-3.75,0,0),vec(0.4,0.4,-2),0)
        holoModel(40,"hq_cylinder")
        holoParent(40,39)
        
        holoParent(6,40)
        
        ## Front Left Leg control ##
        Tmp = holoCreate(41,E:toWorld(vec(9.85,-12.2,-1.75)*Scale),Scale*(vec(1.15,1.15,2)/0.7)/12,E:toWorld(ang(0,0,0)),vec()+70)
        holoModel(41,"hq_cylinder")
        holoParent(41,35)
        
        Tmp = holoCreate(42,Tmp:toWorld(vec(-6.25,3,0)*Scale),Scale*(vec(10,14,14)/0.7)/12,E:toWorld(ang(0,0,0)))
        holoModel(42,"hq_torus_thin")
        holoMaterial(42,"phoenix_storms/black_chrome")
        holoClipEnabled(42,1,1)
        holoClipEnabled(42,2,1)
        holoClip(42,1,vec(0,-2.857,0)*Scale,vec(0,1,0),0)
        holoClip(42,2,vec(0,0,0),vec(1,0,0),0)
        holoParent(42,41)
        
        Tmp = holoCreate(43,Tmp:toWorld(vec(0,9.5,0)*Scale),Scale*(vec(1.15,1.15,1.5)/0.7)/12,E:toWorld(ang(0,0,0)),vec()+70)
        holoModel(43,"hq_cylinder")
        holoParent(43,42)
        
        holoCreate(44,Tmp:toWorld(vec(-0.5,-0.5,0)*Scale),Scale*vec(2,2,1.5)/12,Tmp:toWorld(ang(90,0,-50)),vec()+70)
        holoModel(44,"hq_cylinder")
        holoParent(44,43)
    
        Tmp = holoCreate(45,Tmp:toWorld(vec(6.25,-9,0)*Scale),Scale*vec(20,26,14)/12,E:toWorld(ang(0,0,0)))
        holoModel(45,"hq_torus_thin")
        holoMaterial(45,"phoenix_storms/black_chrome")
        holoClipEnabled(45,1,1)
        holoClipEnabled(45,2,0)
        holoClip(45,1,vec(-4.286,0,0)*Scale,vec(-1,-0.25,0),0)
        holoClip(45,2,vec(0,0,0),vec(1,0,0),0)
        holoParent(45,44)
        
        holoCreate(46,Tmp:toWorld(vec(-1.5,-11.9-0.3,0)*Scale),Scale*vec(2,2,3)/12,Tmp:toWorld(ang(90,0,20)),vec()+70)
        holoClipEnabled(46,1)
        holoClip(46,vec(5.357,0,0)*Scale,vec(-0.4,0.4,-2),0)
        holoModel(46,"hq_cylinder")
        holoParent(46,45)
        
        holoParent(5,46)
        timer("extra",1000)
    }elseif(Extra==3){
        HH = holoEntity(32)
        HH = holoCreate(47,HH:toWorld(vec(5,-10,0)*Scale),Scale*vec(2,2,4)/12,E:toWorld(ang(0,0,0)),vec()+70)
        holoModel(47,"hq_cylinder")
        holoParent(47,32)
        
        HH = holoCreate(48,HH:toWorld(vec(0,8,0)*Scale),Scale*vec(3,16,3)/12,HH:toWorld(ang()))
        holoMaterial(48,"phoenix_storms/black_chrome")
        holoParent(48,47)
        
        holoCreate(49,HH:toWorld(vec(-1.35,7.8,0)*Scale),Scale*vec(1,1,4)/12,E:toWorld(ang(0,0,0)),vec()+70)
        holoModel(49,"hq_cylinder")
        holoParent(49,48)
        
        holoCreate(50,HH:toWorld(vec(0,13.75,0)*Scale),Scale*vec(3,13,3)/12,HH:toWorld(ang()))
        holoMaterial(50,"phoenix_storms/black_chrome")
        holoParent(50,49)
        
        HH = holoEntity(6)
        
        holoCreate(51,HH:toWorld(vec(2,3.5,26)*Scale),Scale*vec(2,2,4.5)/12,HH:toWorld(ang(0,0,35)),vec()+70)
        holoModel(51,"hq_cylinder")
        holoParent(51,6)
        
       HH = holoCreate(52,HH:toWorld(vec(2.25,-0.15,26-1.4)*Scale),Scale*vec(2,3,8)/12,HH:toWorld(ang(0,0,-75)))
        holoMaterial(52,"phoenix_storms/black_chrome")
        holoParent(52,51)
        
        holoCreate(53,HH:toWorld(vec(-0.75,0,-3.7)*Scale),Scale*vec(1,1,3.4)/12,HH:toWorld(ang(0,0,90)),vec()+70)
        holoModel(53,"hq_cylinder")
        holoParent(53,52)
        
        HH = holoCreate(54,HH:toWorld(vec(-1.5,0,0)*Scale),Scale*vec(2,3,8)/12,HH:angles())
        holoMaterial(54,"phoenix_storms/black_chrome")
        holoParent(54,53)
        
        holoCreate(55,HH:toWorld(vec(0,0,0)*Scale),Scale*vec(2.75,1.5,9)/12,HH:toWorld(ang(0,90,0)))
        holoMaterial(55,"phoenix_storms/black_chrome")
        holoModel(55,"prism")
        holoParent(55,54)
        
        timer("extra",1000)
    }elseif(Extra==4){
        HH = holoEntity(5)
        
        holoCreate(56,HH:toWorld(vec(2,-3.5,26)*Scale),Scale*vec(2,2,4.5)/12,HH:toWorld(ang(0,0,-35)),vec()+70)
        holoModel(56,"hq_cylinder")
        holoParent(56,5)
        
        HH = holoCreate(57,HH:toWorld(vec(2.25,-0.15,26-1.4)*Scale),Scale*vec(2,3,8)/12,HH:toWorld(ang(0,0,75)))
        holoMaterial(57,"phoenix_storms/black_chrome")
        holoParent(57,56)
        
        holoCreate(58,HH:toWorld(vec(-0.75,0,-3.7)*Scale),Scale*vec(1,1,3.4)/12,HH:toWorld(ang(0,0,90)),vec()+70)
        holoModel(58,"hq_cylinder")
        holoParent(58,57)
        
        HH = holoCreate(59,HH:toWorld(vec(-1.5,0,0)*Scale),Scale*vec(2,3,8)/12,HH:angles())
        holoMaterial(59,"phoenix_storms/black_chrome")
        holoParent(59,58)
        
        holoCreate(60,HH:toWorld(vec(0,0,0)*Scale),Scale*vec(2.75,1.5,9)/12,HH:toWorld(ang(0,90,0)))
        holoMaterial(60,"phoenix_storms/black_chrome")
        holoModel(60,"prism")
        holoParent(60,59)
        
        holoCreate(61,H:pos(),Scale*vec(4,4,20)/12,H:toWorld(ang(0,0,90)))
        holoModel(61,"hq_cylinder")
        holoMaterial(61,"phoenix_storms/black_chrome")
        holoParent(61,H)
        
        holoCreate(62,H:toWorld(vec(0,0,-5)*Scale),Scale*vec(4,4,17)/12,H:toWorld(ang(0,0,0)))
        holoModel(62,"hq_cylinder")
        holoMaterial(62,"phoenix_storms/black_chrome")
        holoParent(62,1)
        
        HH = holoEntity(16)
        holoCreate(63,HH:toWorld(vec(0,0,300*Scale)),Scale*vec(2,2,600)/12,HH:toWorld(ang()))
        holoModel(63,"hq_cylinder")
        holoMaterial(63,"models/weapons/w_smg1/smg_crosshair")
        holoAlpha(63,0)
        holoParent(63,HH)

        hint("Done",2)
        
        Active = 1
        Open = 1
    }
}

if(Active){

Clk = owner():keyUse() & owner():aimEntity()==E & !E:isPlayerHolding() & (!Move | Move==180)

if(changed(Clk) & Clk){
    Open = !Open
    soundPlay("movement",2,Open ? "npc/turret_floor/deploy.wav" : "npc/turret_floor/retract.wav")
    if(Open){
        timer("deployed",750)
    }
}

if(clk("deployed")){
    soundPlay("deployed",3,"npc/turret_floor/turret_deploy_"+randint(1,6)+".wav")
}
Move = clamp(Move+(!Open-Open)*Speed,0,180)

if(changed(Move)){
    Cos = (1+cos(Move))*5*Scale
    C2 = (1+cos(Move))*5
    Off = 0.1
    
    Zv = -Scale*10+Cos
    holoPos(15,holoEntity(1):toWorld(vec(0,0,Zv)))
    holoPos(18,holoEntity(11):toWorld(vec(0,4.55*Scale,Zv)))
    holoPos(23,holoEntity(13):toWorld(vec(0,-4.55*Scale,Zv)))
    holoScale(61,vec(3,3,min(20,11+Cos))/12)
    
    holoPos(11,H:toWorld(vec(0,Cos+Off,-Zv)))
    holoAng(11,H:toWorld(ang(0,-C2/4,0)))
    
    holoPos(13,H:toWorld(vec(0,-Cos-Off,-Zv)))
    holoAng(13,H:toWorld(ang(0,C2/4,0)))
    
    # Rear leg control #
    holoAng(32,E:toWorld(ang(0,-C2*9,0)))

    # Front right leg #
    holoAng(35,E:toWorld(ang(0,C2*12,0)))
    holoAng(37,E:toWorld(ang(0,C2*9,0)))
    M = matrix(holoEntity(37):toWorld(ang(90,0,-50)))*matrix(ang(0,-C2*2,0))
    holoAng(38,M:toAngle())
    
    M = matrix(holoEntity(39):toWorld(ang(90,0,20)))*matrix(ang(0,-C2*2,0))
    holoAng(40,M:toAngle())
    
    # Front left leg #
    holoAng(43,E:toWorld(ang(0,C2*9,0)))
    M = matrix(holoEntity(43):toWorld(ang(90,0,-50)))*matrix(ang(0,C2*2,0))
    holoAng(44,M:toAngle())
    
    M = matrix(holoEntity(45):toWorld(ang(90,0,20)))*matrix(ang(0,C2*2,0))
    holoAng(46,M:toAngle())
    
    holoAng(47,holoEntity(32):toWorld(ang(0,-C2*10,0)))
    holoAng(49,holoEntity(48):toWorld(ang(0,45-cos(C2*36)*45,0)))
    
    M = matrix(holoEntity(6):toWorld(ang(0,0,35)))*matrix(ang(0,22.5-C2*12.45,0))
    holoAng(51,M:toAngle())
    
    holoAng(53,holoEntity(52):toWorld(ang(-5-C2*17.5,0,90)))
    
    holoPos(55,holoEntity(54):toWorld(vec(0,0, clamp(-18*Scale+Cos*2.5,0,7*Scale) )))
    
    M = matrix(holoEntity(5):toWorld(ang(0,0,-35)))*matrix(ang(0,22.5+C2*7.95,0))
    holoAng(56,M:toAngle())
    
    holoAng(58,holoEntity(57):toWorld(ang(-5-C2*17.5,0,90)))
    
    holoPos(60,holoEntity(59):toWorld(vec(0,0, clamp(-18*Scale+Cos*2.5,0,7*Scale) )))

    holoScale(62,Scale*vec(4,4,17+C2)/12)
    holoPos(62,H:pos()+E:toWorldAxis(vec(0,8.5+C2/2,0)*Scale))
    
    holoPos(1,E:toWorld(vec(0,Cos*1.45,0)))
}

Pitch = E:angles()
Hold = E:isPlayerHolding() & !Move
if(!Hold & !Move){
    Jolt = ceil(abs($Pitch[3]))>1 & !Over
    Over = !inrange(abs(Pitch[3]),70,110)
    if(Over){ Open = 0 }
}

if(changed(Hold) & Hold){
    soundPlay("hold",4,"npc/turret_floor/turret_pickup_"+randint(1,10)+".wav")
}

if(changed(Jolt) & Jolt){
    soundPlay("jolt",5,"npc/turret_floor/turret_collide_"+randint(1,5)+".wav")
}

if(changed(Over) & Over){
    soundPlay("over",5,"npc/turret_floor/turret_disabled_"+randint(1,8)+".wav")
}

if(changed(Open)){
    Color = vec( Open ? 255 : 70, 0, 0 )
    holoColor(16,Color)
    holoColor(17,Color)
    holoAlpha(63,Open*255)
}

Dist = vec2(O:pos()):distance(vec2(E:pos()))

if(findCanQuery()){
    findByClass("player")
    O = findClosest(E:pos())
}

Local = E:toLocal(O:pos())
Away = Dist<300 & !Move & ( inrange(Local[1],32,300) & inrange(Local[3],-90,90) )

if(Away){
    AimAng = clamp(E:toLocal((O:boxCenterW()-H:pos()):toAngle()), ang(-30,-30,90), ang(30,30,90))
    Angle = Angle+( AimAng-Angle)/4
    Dir = O:boxCenterW()-holoEntity(16):pos()
    holoPos(63,holoEntity(16):pos()+Dir:normalized()*300*Scale)
    holoAng(63,Dir:toAngle()+ang(90,0,0))
}else{
    Angle = Angle+(Ang-Angle)/10
}

if(changed(Away) & !Away){
    soundPlay("search",5,"npc/turret_floor/turret_search_"+randint(1,4)+".wav")
    HH = holoEntity(16)
    holoPos(63,HH:toWorld(vec(0,0,300*Scale)))
    holoAng(63,HH:toWorld(ang()))
}

holoAng(15,E:toWorld(Angle))

if(Open){
    H18 = holoEntity(18)
    H23 = holoEntity(23)
    LT:applyForce(((H18:pos()-LT:toWorld(vec(0,1.5,0)))*15-LT:vel()/2)*LT:mass())
    LT:applyTorque((150*LT:toLocalAxis(rotationVector(quat(H18)*qRotation(vec(1,0,0),-90)/quat(LT)))-12*LT:angVelVector())*LT:inertia())
    RT:applyTorque((150*RT:toLocalAxis(rotationVector(quat(holoEntity(23))*qRotation(vec(1,0,0),-90)/quat(RT)))-12*RT:angVelVector())*RT:inertia())
    RT:applyForce(((H23:pos()-RT:toWorld(vec(0,1.5,0)))*15-RT:vel()/2)*RT:mass())
}
}

LTurret["Fire",number] = RTurret["Fire",number] = (O & Away | Over) & Open
