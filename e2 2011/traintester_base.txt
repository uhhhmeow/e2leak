@name TrainTester Base
@inputs EGP:wirelink Dist
@outputs Go Ok
@persist GTable:gtable [MaxSpeed Name Fault]:array [E O]:entity Chat:array LastSpeed Speed Save Print
@persist I Once
@trigger all

if(first() | duped()){
runOnTick(1)
runOnChat(1)

E = entity()
O = owner()
I = 1
Go = 0
Ok = 1
GTable = gTable("vel",2)
GTable:clear()
LastSpeed = 0
EGP:egpClear()   

EGP:egpRoundedBox(1,vec2(255,120),vec2(450,100))
EGP:egpColor(1,vec4(0,26,71,255))
EGP:egpRoundedBoxOutline(2,vec2(255,120),vec2(450,100))

EGP:egpText(3,"----| Top Score |----",vec2(80,0))
EGP:egpFont(3,"verdana",40)

EGP:egpText(4,"Speed : " + Speed,vec2(40,80))
EGP:egpFont(4,"verdana",40)

EGP:egpText(5,"Top Speed : " +LastSpeed ,vec2(40,120))
EGP:egpFont(5,"verdana",40)

EGP:egpRoundedBox(6,vec2(255,327),vec2(450,300))
EGP:egpColor(6,vec4(0,26,71,255))
EGP:egpRoundedBoxOutline(7,vec2(255,327),vec2(450,300))

}

if(chatClk(O)){
Chat = O:lastSaid():explode(" ")   

if(Chat[1,string] == "-start"){
GTable["active",number] = 1 
GTable["name",string] = Chat[2,string] 
print("Starting!")  
Go = 1
Once = 0
}

if(Chat[1,string] == "-stop"){
GTable["active",number] = 0  
Save = 1
Go = 0
print("Stoping")     
}
 
if(Chat[1,string] == "-print"){
Print = 1
print("Printing")     
}

}

if(Dist > 0 & Once == 0){
Once = 1
Fault[Ok,number] = 1 
print("FAULT!")    
}

if(GTable["active",number]){
   
Speed = GTable["speed",number] 
EGP:egpText(4,"Speed : " + floor(Speed,4),vec2(40,80))
EGP:egpText(5,"Top Speed : " +floor(LastSpeed,4) ,vec2(40,120))

if(Speed > LastSpeed){
LastSpeed = Speed   
}


}

if(Save){
Save = 0
Print = 1
Ok += 1

MaxSpeed:pushNumber(LastSpeed)
Name:pushString(GTable["name",string])

print("Saved!")
Speed = 0
LastSpeed = 0  
EGP:egpText(4,"Speed : " + floor(Speed,4),vec2(40,80))
EGP:egpText(5,"Top Speed : " +floor(LastSpeed,4) ,vec2(40,120))  
}


if(Print)
{

for(I = 1,MaxSpeed:count()){
EGP:egpText(I+8,"Name : " + Name[I,string] + " |Top Speed : " +MaxSpeed[I,number]:toString() ,vec2(40,170+I*16))
EGP:egpFont(I+8,"verdana",20)    

if(Fault[I,number] == 1){
EGP:egpColor(I+8,vec4(255,0,0,255))   
}
    
 
if(I >= MaxSpeed:count()){
Print = 0
break    
}
  
}

    
}
