@name FailCake Combine Mine 
@persist E:entity Ply:array Hold MaxH InGround
@trigger all
@model models/props_combine/combine_mine01.mdl

#npc/roller/mine/combine_mine_active_loop1.wav
#npc/roller/mine/combine_mine_deploy1.wav
#npc/roller/mine/combine_mine_deactivate1.wav


if(first() | duped()){
runOnTick(1)
E = entity()

## SETTINGS ##
MaxH = 20
## SETTINGS ##

holoCreate(1)
holoModel(1,"hqcylinder")
holoParent(1,E)
holoScaleUnits(1,vec(10,10,1))
holoColor(1,vec(255,0,0))
holoMaterial(1,"models/debug/debugwhite")
holoPos(1,E:toWorld(vec(0,0,9.4)))

E:soundPlay(1,0,"npc/roller/mine/combine_mine_deploy1.wav")

E:setPos(E:toWorld(vec(0,0,-9)))

InGround = 0
Hold = 0
}

if(findCanQuery() & InGround == 0){
findByClass("player")
findInSphere(E:pos(),300) # Find nearest players
Ply = findToArray()

for(I=0,Ply:count()){
    
if(Ply[I,entity]:weapon():type() == "weapon_physcannon" & Ply[I,entity]:aimEntity() == E & Ply[I,entity]:keyAttack2() & Ply[I,entity]:pos():distance(E:pos()) <= 100){
Hold += 1   
}

}

if(Hold >= MaxH){
E:propFreeze(0)
InGround = 1
Hold = 0
holoColor(1,vec(255,255,0))
print("derp")
E:soundPlay(1,0,"npc/roller/mine/combine_mine_deactivate1.wav")    
}

if(InGround){
    
}
