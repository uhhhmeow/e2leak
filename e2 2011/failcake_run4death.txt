@name FailCake Run4Death
@inputs EGP:wirelink 
@outputs Ops [E O]:entity Mode:string Cmd:array P IO Onc [ClampedPos Pos]:vector GridSize
@outputs [Prop Main Ply EndProp]:entity Begin Vec:array I ElevatorMo T Ready
@outputs Type:string [PropsRot RotPos PropsElev Props Color]:array Reset
@trigger all

Ops = ops()

if(first() | duped()){
runOnTick(1)
runOnChat(1)
runOnFile(1)
propSpawnEffect(0)

## SETTINGS ##

GridSize = 8

## SETTINGS ##

Ply = owner()
Ready = 0
Mode = ""
Type = "normal"
EndProp = noentity()

O = owner()
E = entity()
E:setColor(1,1,1)
E:setMaterial("models/debug/debugwhite")

#setName("FailCake Run4Death [Based on Gauntler]")  

holoCreate(1)
holoModel(1,"models/hunter/blocks/cube2x2x2.mdl")
holoAlpha(1,100)

Begin = 1
hint("Place the Start Block!",7)

}

## The Begin prop

if(Begin){
  
Pos = owner():aimPos() 
ClampedPos = round( Pos / GridSize ) * GridSize 

holoPos(1,ClampedPos + vec(0,0,holoEntity(1):height()/2))

if(O:keyUse()){

Main = propSpawn("models/hunter/blocks/cube2x2x2.mdl",holoEntity(1):pos(),ang(0,0,0),1)
Main:setColor(90,90,90)

#propSpawn("models/hunter/misc/stair1x1.mdl",Main:toWorld(vec(71,0,0)),ang(0,0,0),1)  
#propSpawn("models/hunter/misc/stair1x1.mdl",Main:toWorld(vec(118,0,-47)),ang(0,0,0),1)  
holoAlpha(1,0)
Begin = 0    
} 
   
}





if(Begin == 0){
    
    
# Commands

if(chatClk(O)){

Cmd = O:lastSaid():explode(" ")

if(Cmd[1,string] == "!edit"){
if(Mode != "edit"){
Mode = "edit" 
holoAlpha(1,100)
hint("Mode = " + Mode,7)   
}else{
hint("Already in Edit Mode!",7)
}
}

## Type Setting

if(Cmd[1,string] == "!type"){
print("Type set to " + Type)
    
if(Cmd[2,string] == "elev"){
holoColor(1,vec(0,255,0))
Type = Cmd[2,string]
}

if(Cmd[2,string] == "normal"){
holoColor(1,vec(255,255,255))
Type = Cmd[2,string]
}

if(Cmd[2,string] == "rot"){
holoColor(1,vec(0,0,255))
Type = Cmd[2,string]

}

if(Cmd[2,string] == "end"){
holoColor(1,vec(255,255,0))
Type = Cmd[2,string]
}
}

if(Cmd[1,string] == "!play"){
if(Mode != "play"){
Mode = "play" 
holoAlpha(1,0)
hint("Mode = " + Mode,7)   
}else{
hint("Already in Play Mode!",7)
}
}

}








## - Edit Mode ! Important
if(Mode == "edit"){

# HOLO

Pos = owner():aimPos() 
ClampedPos = round( Pos / GridSize ) * GridSize 

if(O:aimEntity() == noentity()){
holoPos(1,ClampedPos + vec(0,0,holoEntity(1):height()/2))
}else{
if(O:aimEntity():isPlayer() == 0 & O:aimEntity():isNPC() == 0){
holoPos(1,O:aimEntity():pos() + O:aimNormal()*96)
}
}


if(O:keyUse() & Onc == 0){
Onc = 1

if(Type == "normal"){
Prop = propSpawn("models/hunter/blocks/cube2x2x2.mdl",holoEntity(1):pos(),ang(0,0,0),1)
Props:pushEntity(Prop)
}

if(Type == "end"){
    
if(EndProp != noentity()){
EndProp:propDelete()
}

Prop = propSpawn("models/hunter/blocks/cube2x2x2.mdl",holoEntity(1):pos(),ang(0,0,0),1)
Prop:setColor(255,0,0)
EndProp = Prop
}


if(Type == "elev"){
Prop = propSpawn("models/hunter/blocks/cube2x2x2.mdl",holoEntity(1):pos(),ang(0,0,0),1)
PropsElev:pushEntity(Prop)
ElevatorMo = 1
}

if(Type == "rot"){
IO+=1
Prop = propSpawn("models/hunter/blocks/cube2x2x2.mdl",holoEntity(1):pos(),ang(0,0,0),1)
PropsRot:pushEntity(Prop)
RotPos[IO,vector] = Prop:pos()
}

}

timer("hold",700)
if(clk("hold")){
Onc = 0    
}  
}




## Play Mode ##

if(Mode == "play" & EndProp != noentity()){
    
    
if(Ready == 0){
timer("t",10)

if(clk("t")){
    
if(T <= Props:count()){
T+=1 
Props[T,entity]:setColor(1,1,1)  
Props[T,entity]:setMaterial("models/debug/debugwhite") 
}else{

holoCreate(2)
holoModel(2,"cube")
holoPos(2,Ply:toWorld(vec(0,0,0)))
holoAlpha(2,0)
holoParent(2,Ply)
Ready = 1
}

} 
}


if(Ready & Reset == 0){

for(P=0,PropsRot:count()){
Vec[P,vector] = (RotPos[P,vector] - PropsRot[P,entity]:pos())*1000

PropsRot[P,entity]:applyForce(Vec[P,vector])
PropsRot[P,entity]:applyAngForce(ang(0,curtime()*100,0))   
}

  
if(Ply:pos():distance(EndProp:pos()) <= 60){
Win = 1
Ready = 0  
Reset = 1  
}

rangerFilter(Main)
rangerFilter(EndProp)
rangerIgnoreWorld(1)
rangerHitWater(0)

Ranger = rangerOffset(50,holoEntity(2):pos(),-holoEntity(2):up())

if(Ranger:hit()){
Ranger:entity():setColor(hsv2rgb(randint(1,20)*25,1,1))    
} 
   
}
      
}

if(Reset){
Ply = noentity()
timer("t",10)

if(clk("t")){
    
if(T <= Props:count()){
T+=1 
Props[T,entity]:setColor(1,1,1)  
Props[T,entity]:setMaterial("models/debug/debugwhite") 

}else{
Reset = 0
Ready = 1
Win = 0

}

    
}


if(Win){
soundStop(5,2)
holoEntity(2):soundPlay(2,0,"garrysmod/save_load1.wav")   
Win = 0 
T = 0
}




}

}
