@name Copypaster_to_Failcake
@outputs Load Ind PC [Props Model Pos Ang]:array [EP1 EP2 Razn]:vector Ops
@persist KE KR Load Ind PC
@persist [E O ]:entity [Props Model Pos Ang]:array [EP1 EP2]:vector
@model models/props_combine/breenglobe.mdl
interval(400)
#if(duped()){selfDestructAll()}
if(first()){
    #runOnTick(1)
    E=entity()
    O=owner()
    
    
    
    
}#
KE=owner():keyZoom()
KR=owner():keyReload()


if($KE & KE){
    EP1=E:pos()
    Ind=0
    findIncludePlayerProps(owner())
    findByClass("prop_physics")
    Props=findToArray()
    PC=Props:count()
    E:setColor((PC!=0?vec(0,0,255) :vec(255,0,0)))
    for(I=1,PC){
        Tar=Props[I,entity]
        Model[I,string]=Tar:model()
        Pos[I,vector]=Tar:pos()
        Ang[I,angle]=Tar:angles()
        Tar:propDelete()
        }
    
    }

if($KR & KR){
    
    EP2=E:pos()
    Load=1
    Razn=(EP2==EP1?vec() :(EP2-EP1))
    }

if(Load){
    Ind++
    propSpawn(Model[Ind,string],Pos[Ind,vector]+Razn,Ang[Ind,angle],1)
    if(Ind>PC){reset()}
    }

Ops=ops()
