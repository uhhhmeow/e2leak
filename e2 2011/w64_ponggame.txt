@name W64_PongGame
@outputs Data
@persist E:entity Processor:entity ScrW ScrH
@persist Screen:wirelink Graphic:entity Sound:entity Interface:entity
@persist CtrlA CtrlB CtrlC SP1:entity SP2:entity
@persist T1Score T2Score
@persist Rdy1 Rdy2 Velocity:vector2 BallHit
@persist Speed GTABLE:gtable
@trigger 
@model models/hunter/plates/plate025.mdl

interval(100)

if (first()){
    
    E = entity()
    Data = E:id()
    Rdy1 = 1
    Rdy2 = 1
    T1Score = 0
    T2Score = 0
    E["Name", string] = "Pong"
    GTABLE = gTable("CONSOLE_CAKE",1)  
}

if (GTABLE["startup",number] == 1){
    
    E = entity()
    Data = E:id()
    Rdy1 = 1
    Rdy2 = 1
    T1Score = 0
    T2Score = 0
    E["Name", string] = "Pong"
    
    GTABLE["startup",number] = 0

    Processor = entity(E["Processor", number])
    Graphic = Processor["Graphic", entity]
    Sound = Processor["Sound", entity]
    Interface = Processor["Interface", entity]
    Screen = Graphic["Screen", wirelink]
     
    ScrW = Graphic["ScrW", number]
    ScrH = Graphic["ScrH", number]
                
    Screen:egpClear()  

    Screen:egpText(1, T1Score + " | " + T2Score, vec2(ScrW / 2, 0))
    Screen:egpFont(1, "Arial", 30)
    Screen:egpAlign(1, 1, 0)
    
    Screen:egpBox(4, vec2(ScrW / 2, ScrH / 2), vec2(10, 10))
    Screen:egpAlign(4, 1, 1)
    Screen:egpBox(2, vec2(20, ScrH / 2), vec2(20, 160))
    Screen:egpBox(3, vec2(ScrW-20, ScrH / 2), vec2(20, 160))
    Song = "hl1_song25_remix3"
    
    SP1 = Sound["Speaker1", entity]
    SP2 = Sound["Speaker2", entity]
    
    Rnd = random(0, 1)
    
    if (Rnd < 0.5){
        Velocity = vec2(-10,0)
    }else{
        Velocity = vec2(10,0)
    }
            
    timer("Update", 1000)
}

if (clk("Update") && E["Processor", number] > 0){       
    CtrlA = Interface["CtrlA", number]
    CtrlB = Interface["CtrlB", number]
    CtrlC = Interface["CtrlC", number] 
    
    if (CtrlA){
        CHA = Screen:egpPos(2):y() - (Screen:egpSize(2):y() / 2)  - 5
        CHB = Screen:egpPos(2):y() + (Screen:egpSize(2):y() / 2)  + 5
        if (bAnd(CtrlA, 512) == 512 && CHA > 0){
            Screen:egpPos(2, Screen:egpPos(2) - vec2(0, 5))
        }
        if (bAnd(CtrlA, 64) == 64 && CHB < ScrH){
            Screen:egpPos(2, Screen:egpPos(2) + vec2(0, 5))
        }
          
        if (bAnd(CtrlA, 2) == 2 && Rdy1 == 0){
            Rdy1 = 1
        }  
    } 
    if (CtrlB){
        CHA = Screen:egpPos(3):y() - (Screen:egpSize(3):y() / 2)  - 5
        CHB = Screen:egpPos(3):y() + (Screen:egpSize(3):y() / 2)  + 5
        if (bAnd(CtrlB, 512) == 512 && CHA > 0){
            Screen:egpPos(3, Screen:egpPos(3) - vec2(0, 5))
        }
        if (bAnd(CtrlB, 64) == 64 && CHB < ScrH){
            Screen:egpPos(3, Screen:egpPos(3) + vec2(0, 5))
        }
        if (bAnd(CtrlA, 2) == 2 && Rdy2 == 0){
            Rdy2= 1
        }  
    }
    
    ## BALL
    
        
    BallPos = Screen:egpPos(4)
    Ply1Pos = Screen:egpPos(2)
    Ply2Pos = Screen:egpPos(3)
    Ply1Size = Screen:egpSize(2)
    Ply2Size = Screen:egpSize(3)
    
    Ply1HSize = Screen:egpSize(2) / 2
    Ply2HSize = Screen:egpSize(3) / 2
    
    Ply1RXP = Ply1Pos:y() - BallPos:y()
    Ply2RXP = Ply2Pos:y() - BallPos:y()
      
    if (BallPos:y() <= 5){
        Velocity = vec2(Velocity:x(), abs(Velocity:y()) + 2)
    }
    if (BallPos:y() >= ScrH - 5){
        Velocity = vec2(Velocity:x(), -abs(Velocity:y()) - 2)
    }
       
    if (inrange(BallPos:x(), 0, ScrW)){

        if (inrange(BallPos, Ply1Pos - Ply1HSize - vec2(5,5), Ply1Pos + Ply1HSize + vec2(5,5))){
            Velocity = vec2(10, 0)   
            BallHit = 1
            if (Ply1RXP >= 0){
                Velocity = vec2(abs(Velocity:x()), Velocity:y() - (Ply1RXP / 5))
            }
            if(Ply1RXP < 0){
                Velocity = vec2(abs(Velocity:x()), Velocity:y() + (abs(Ply1RXP / 5)))
            }
         }else{
            if (BallHit == 1){
                BallHit = 0
                #E:execute("PlaySound")
            }
        }
        
        if (inrange(BallPos, Ply2Pos - Ply2HSize - vec2(5,5), Ply2Pos + Ply2HSize + vec2(5,5))){
            BallHit = 2
            if (Ply2RXP >= 0){
                Velocity = vec2(-abs(Velocity:x()), Velocity:y() - (Ply2RXP / 5))
            }
            if(Ply2RXP < 0){
                Velocity = vec2(-abs(Velocity:x()), Velocity:y() + abs(Ply2RXP / 5))
            }
        }else{
            if (BallHit == 2){
                BallHit = 0
                #E:execute("PlaySound")
            }
        }
        
        Screen:egpPos(4, BallPos + Velocity)
        
        timer("Update", 1)
    }else{
        if (Screen:egpPos(4):x() <= 0){
            T2Score++
            Velocity = vec2(-10,0)
        }else{
            T1Score++
            Velocity = vec2(10,0)
        }
        Screen:egpSetText(1, T1Score + " | " + T2Score)     
        if (T1Score >= 5 || T2Score >= 5){
            Screen:egpText(5, "Player 1 wins!", vec2(ScrW / 2, ScrH / 2))
            Screen:egpFont(5, "Arial", 60)
            Screen:egpAlign(5, 1, 1)
            if (T2Score >= 5){
                Screen:egpSetText(5, "Player 2 wins!")
            }
            timer("Reset", 5000)
        }else{
            Screen:egpPos(4, vec2(ScrW / 2, ScrH / 2))
            Screen:egpPos(2, vec2(20, ScrH / 2))
            Screen:egpPos(3, vec2(ScrW-20, ScrH / 2))
            timer("Update", 1000)
        }
    }
}

if (clk("Reset")){
    T1Score = 0
    T2Score = 0
    Screen:egpSetText(1, T1Score + " | " + T2Score) 
    Screen:egpPos(2, vec2(20, ScrH / 2))
    Screen:egpPos(3, vec2(ScrW-20, ScrH / 2))
    Screen:egpPos(4, vec2(ScrW / 2, ScrH / 2))
    Screen:egpRemove(5)
    Rnd = random(0, 1)
    
    if (Rnd < 0.5){
        Velocity = vec2(-10,0)
    }else{
        Velocity = vec2(10,0)
    }
    timer("Update", 1000)
}
#[
if (execClk("PlaySound")){
    SP1["DMusic", string] = randint(1,45):toString()
    SP1["PlayL", number] = 0
    SP1["PlayT", number] = 200
    SP1["PlayPath", string] = "vo/heavy_yes03.wav"
    
    SP2["DMusic", string] = randint(1,45):toString()
    SP2["PlayL", number] = 0
    SP2["PlayT", number] = 200
    SP2["PlayPath", string] = "vo/heavy_yell4.wav"
    
    #SP1:execute("Play")
    #SP2:execute("Play")
}
]#
