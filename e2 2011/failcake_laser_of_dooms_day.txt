@name FailCake Laser OF Dooms Day
@outputs On Sequence Go Up Up2 ANG Spins Power Fire Ha KillAll
@persist Tims R R2 R3 Bed BumUp
@trigger all
@model models/props/light_rail_corner.mdl
runOnTick(1)

E = entity()
O = owner()
Aim = O:aimPos()

if(first()){
holoCreate(1)
holoModel(1,"hqcylinder")
holoScaleUnits(1,vec(20,20,0.1))
holoMaterial(1,"models/roller/rollermine_glow")

holoCreate(2)
holoModel(2,"hq_torus_thin")
holoColor(2,vec(255,0,0))
holoScaleUnits(2,vec(20,20,0.5))
holoMaterial(2,"models/Effects/splodearc.mdl")

holoCreate(3)
holoModel(3,"models/props_combine/headcrabcannister01a.mdl")
holoPos(3,E:toWorld(vec(0,0,40)))
holoAng(3,ang(90,0,0))
holoScale(3,vec(0.5,0.5,0.5))
holoParent(3,E)

## The Lol

holoCreate(4)
holoModel(4,"hq_torus")
holoPos(4,E:toWorld(vec(0,0,20)))
holoColor(4,vec(255,1,1))
holoScale(4,vec(3,3,3))
holoParent(4,E)
holoMaterial(4,"models/debug/debugwhite")

holoCreate(5)
holoModel(5,"cube")
holoPos(5,E:toWorld(vec(0,0,70)))
holoScale(5,vec(0.1,0.1,7))
holoParent(5,E)
holoAlpha(5,0)
holoMaterial(5,"models/alyx/emptool_glow")

## Up Rotor

holoCreate(6)
holoModel(6,"cube")
holoPos(6,E:toWorld(vec(0,0,60)))
holoScale(6,vec(0.5,0.5,0.5))
holoParent(6,3)
holoAlpha(6,0)

## aNGLE rOTATING

holoCreate(7)
holoModel(7,"cube")
holoPos(7,E:toWorld(vec(0,0,60)))
holoScale(7,vec(0.5,0.5,0.5))
holoParent(7,6)
holoAlpha(7,0)

holoCreate(8)
holoModel(8,"cube")
holoPos(8,E:toWorld(vec(0,0,60)))
holoScale(8,vec(0.5,0.5,0.5))
holoParent(8,6)
holoAlpha(8,0)

## The Sticks

holoCreate(10)
holoModel(10,"hqcylinder")
holoPos(10,E:toWorld(vec(0,0,65)))
holoScale(10,vec(0.1,0.1,4))
holoParent(10,7)
holoMaterial(10,"phoenix_storms/metalset_1-2")

holoCreate(11)
holoModel(11,"hqcylinder")
holoPos(11,E:toWorld(vec(0,0,65)))
holoScale(11,vec(0.1,0.1,4))
holoParent(11,8)
holoMaterial(11,"phoenix_storms/metalset_1-2")

holoCreate(13)
holoModel(13,"hqicosphere")
holoPos(13,E:toWorld(vec(0,0,180)))
holoScale(13,vec(0.1,0.1,0.1))
holoParent(13,E)
holoMaterial(13,"models/alyx/emptool_glow")

holoCreate(14)
holoModel(14,"hqicosphere")
holoPos(14,E:toWorld(vec(0,0,180)))
holoScale(14,vec(0.1,0.1,0.1))
holoParent(14,E)
holoMaterial(14,"models/shadertest/shader4")

holoCreate(12)
holoModel(12,"models/Effects/splodearc.mdl")
holoScaleUnits(12,vec(0.1,0.1,0.1))
holoParent(12,holoEntity(13))

holoCreate(15)
holoModel(15,"models/Effects/splodearc.mdl")
holoScaleUnits(15,vec(0.1,0.1,0.1))
holoParent(15,holoEntity(13))

holoCreate(16)
holoModel(16,"models/Effects/splodearc.mdl")
holoScaleUnits(16,vec(0.1,0.1,0.1))
holoParent(16,holoEntity(13))

holoCreate(17)
holoModel(17,"hqsphere")
holoScaleUnits(17,vec(3,3,3))
holoColor(17,vec(80,80,80))
holoPos(17,holoEntity(10):toWorld(vec(0,0,13)))
holoParent(17,holoEntity(10))

holoCreate(18)
holoModel(18,"hqsphere")
holoScaleUnits(18,vec(3,3,3))
holoColor(18,vec(80,80,80))
holoPos(18,holoEntity(11):toWorld(vec(0,0,13)))
holoParent(18,holoEntity(11))

holoCreate(19)
holoModel(19,"hqsphere")
holoScaleUnits(19,vec(3,3,3))
holoColor(19,vec(80,80,80))
holoPos(19,holoEntity(10):toWorld(vec(0,0,23)))
holoParent(19,holoEntity(10))

holoCreate(20)
holoModel(20,"hqsphere")
holoScaleUnits(20,vec(3,3,3))
holoColor(20,vec(80,80,80))
holoPos(20,holoEntity(11):toWorld(vec(0,0,23)))
holoParent(20,holoEntity(11))

holoCreate(21)
holoModel(21,"hqsphere")
holoScaleUnits(21,vec(3,3,3))
holoColor(21,vec(80,80,80))
holoPos(21,holoEntity(10):toWorld(vec(0,0,5)))
holoParent(21,holoEntity(10))

holoCreate(22)
holoModel(22,"hqsphere")
holoScaleUnits(22,vec(3,3,3))
holoColor(22,vec(80,80,80))
holoPos(22,holoEntity(11):toWorld(vec(0,0,5)))
holoParent(22,holoEntity(11))

holoCreate(23)
holoModel(23,"cube")
holoScaleUnits(23,vec(3,3,3))
holoColor(23,vec(80,80,80))
holoPos(23,E:toWorld(vec(0,0,180)))
holoParent(23,E)

holoAlpha(10,0)
holoAlpha(11,0)

holoAlpha(13,0)
holoAlpha(14,0)
holoAlpha(15,0)
holoAlpha(16,0)
holoAlpha(12,0)


holoAlpha(17,0)
holoAlpha(18,0)
holoAlpha(19,0)
holoAlpha(20,0)
holoAlpha(21,0)
holoAlpha(22,0)
holoAlpha(23,0)
}

if(On == 0){
holoPos(2,Aim)
holoPos(1,Aim)    
}else{
holoColor(2,vec(0,255,0))
}

if(O:keyUse() & On == 0){
On = 1
holoColor(4,vec(0,255,0))
holoEntity(2):soundPlay(100,100,"buttons/combine_button_locked.wav")
E:soundPlay(1030,1030,"ambient/machines/hydraulic_1.wav")  
}


if(On){
 
if(Go == 0){ 
Tims+=0.1 
if(Tims == 30){
E:soundPlay(10340,10340,"ambient/machines/wall_move1.wav")   
Go = 1 
}
}

if(Go == 1){
    
if(Up <= 80){
Up+=0.5 
holoPos(3,E:toWorld(vec(0,0,40+Up)))  
}  

if(Up == 79){
E:soundPlay(100,100,"ambient/levels/labs/machine_stop1.wav")  
Go = 2  
}
     
}


if(Go == 2){
timer("hold",2000)

if(clk("hold")){
holoAlpha(5,255)
E:soundPlay(100,100,"ambient/machines/power_transformer_loop_2.wav") 
E:soundPlay(1440,1440,"ambient/machines/machine1_hit2.wav") 
Go = 3   
}

}


if(Go == 3){
    
if(Up2 <= 22){
Up2+=0.3

holoAlpha(10,255)
holoAlpha(11,255)
holoAlpha(12,255)

holoAlpha(17,255)
holoAlpha(18,255)
holoAlpha(19,255)
holoAlpha(20,255)
holoAlpha(21,255)
holoAlpha(22,255)

if(Up2 == 0.3){
E:soundPlay(14550,14550,"ambient/machines/wall_move5.wav")     
}

holoPos(10,E:toWorld(vec(0,0,130+Up2)))
holoPos(11,E:toWorld(vec(0,0,130+Up2)))
holoPos(12,E:toWorld(vec(0,0,130+Up2)))

}else{
Go = 4
}
   
}

if(Go == 4){

if(ANG <= 60){
ANG+=0.7
}else{
Go = 5

holoEntity(18):setTrails(10,10,10,"trails/plasma",vec(255,255,255),255)
holoEntity(17):setTrails(10,10,10,"trails/plasma",vec(255,255,255),255)

holoEntity(19):setTrails(10,10,10,"trails/plasma",vec(255,255,255),255)
holoEntity(20):setTrails(10,10,10,"trails/plasma",vec(255,255,255),255)

holoEntity(21):setTrails(10,10,10,"trails/plasma",vec(255,255,255),255)
holoEntity(22):setTrails(10,10,10,"trails/plasma",vec(255,255,255),255)

}

holoAng(7,ang(ANG,0,0))
holoAng(8,ang(-ANG,0,0))    

}

if(Go == 5){
Spins+=3
Power+=0.05
R+=8
R2+=8
R2+=8

if(Fire == 0){
timer("BeginFire",6000)
}

if(clk("BeginFire")){
if(Fire == 0){
Fire = 1  
stoptimer("BeginFire") 
E:soundPlay(2354,2354,"ambient/alarms/citadel_alert_loop2.wav") 
}
}

holoAng(6,ang(0,Spins,0)) 
holoAlpha(13,255)
holoAlpha(14,255)
holoAlpha(12,255)
holoAlpha(15,255)
holoAlpha(16,255)

holoPos(12,holoEntity(13):toWorld(vec(0,0,0)))
holoPos(15,holoEntity(13):toWorld(vec(0,0,0)))
holoPos(16,holoEntity(13):toWorld(vec(0,0,0)))

if(Power == 2){
E:soundPlay(14530,14530,"ambient/machines/city_ventpump_loop1.wav")     
}

if(Power <= 15){
holoScaleUnits(12,vec(Power*3,Power*3,Power*3))
holoScaleUnits(15,vec(Power*3,Power*3,Power*3))
holoScaleUnits(16,vec(Power*3,Power*3,Power*3))

holoScaleUnits(13,vec(Power/0.5,Power/0.5,Power/0.5))
holoScaleUnits(14,vec(Power/0.5,Power/0.5,Power/0.5))
}

holoAng(12,ang(R,R2,R3))  
holoAng(15,ang(-R,-R2,-R3))  
holoAng(16,ang(R,-R2,-R3))
holoAng(5,ang(0,R,0))  
}

}

if(Fire){
timer("FinalFire",2000)

if(clk("FinalFire")){
Ha = 1
    
}    

if(Ha == 1){
Bed+=1

if(Bed == 40){
E:soundPlay(234,234,"ambient/levels/labs/teleport_postblast_thunder1.wav")         
}

if(Bed == 150){
E:soundPlay(23444,23444,"ambient/levels/labs/teleport_mechanism_windup3.wav")     
}   
    
if(Bed == 580){
holoEntity(23):setTrails(50,50,30,"trails/physbeam",vec(255,255,255),255) 
holoEntity(23):soundPlay(2343,2343,"ambient/machines/electric_machine.wav")
}

if(Bed ==579){
E:soundPlay(21344,21344,"ambient/energy/weld1.wav") 
KillAll = 1     
}
    
}
    
}


if(KillAll){
Fire = 0

BumUp+=20

holoPos(23,E:toWorld(vec(0,0,BumUp+180))) 
 
}
