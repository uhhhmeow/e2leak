@name FailCake Can
@outputs Timeout Mod
@persist Song:array Song2:array
@trigger all
runOnTick(1)

O = owner()
E = entity()

if(first()){
Timeout = 0
Mod = 0
Song[1,string]="npc/metropolice/vo/pickupthecan1.wav"
Song[2,string]="npc/metropolice/vo/pickupthecan2.wav"  
Song[3,string]="npc/metropolice/vo/pickupthecan3.wav"  

Song2[1,string]="npc/metropolice/vo/putitinthetrash1.wav"
Song2[2,string]="npc/metropolice/vo/putitinthetrash1.wav"

Prop = propSpawn("models/props_trainstation/trashcan_indoor001b.mdl",E:pos()+vec(0,0,20),ang(0,0,0),1)
E:setAlpha(0)

}

if(O:keyReload() & Timeout == 0){    
if(Mod == 1){
Mod = 0
Timeout = 1
hint("*Changed to Pick Up The Can*",7)    
}

if(Mod == 0){
Mod = 1  
Timeout = 1
hint("*Changed to Put The Can*",7) 
}  

}


if(O:keyAttack2() & Timeout == 0){
Timeout = 1

if(Mod == 0){
Mod = 1
R = randint(1,Song:count())
O:soundPlay(100,100,Song[R,string])    
}elseif(Mod == 1){
Mod = 0
R2 = randint(1,Song2:count())
O:soundPlay(100,100,Song2[R2,string])    
}

}


if(Timeout){
timer("wait",800)
if(clk("wait")){
Timeout = 0
stoptimer("wait")    
}    
}
