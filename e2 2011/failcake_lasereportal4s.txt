@name FailCake LaserEPortal4s Main Laser
@inputs Power
@outputs Main:entity  Current Before Max Onc GLasers Cos
@persist [G G2]:gtable Ranger:ranger
@trigger all
@model models/hunter/blocks/cube025x025x025.mdl

runOnTick(1)

E = entity()

if(first() | duped()){
Onc = 0
E:setAlpha(0)

G = gTable("e2main",1)
G2 = gTable("e2lasers",1)

hint("Made By FailCake",7) 
   
G:clear()
G2:clear()

holoCreate(1)
holoModel(1,"hqcylinder")
holoPos(1,E:toWorld(vec(0,0,10)))
holoColor(1,vec(200,0,0))
holoScaleUnits(1,vec(-1,-1,-10))
holoParent(1,E)
holoMaterial(1,"models/debug/debugwhite")

holoCreate(2)
holoModel(2,"hqcylinder")
holoPos(2,E:toWorld(vec(0,0,10)))
holoColor(2,vec(150,0,0))
holoScaleUnits(2,vec(1,1,10))
holoParent(2,E)
holoMaterial(2,"models/debug/debugwhite")

holoCreate(4)
holoModel(4,"hqcone")
holoPos(4,E:toWorld(vec(0,0,7)))
holoColor(4,vec(90,90,90))
holoScaleUnits(4,vec(7,7,5))
holoParent(4,E)
holoMaterial(4,"models/props_combine/combinethumper002")

holoCreate(6)
holoModel(6,"hqtorus")
holoPos(6,E:toWorld(vec(0,0,5)))
holoColor(6,vec(90,90,90))
holoScaleUnits(6,vec(7,7,5))
holoParent(6,E)
holoMaterial(6,"models/props_combine/tprotato1_sheet")

holoCreate(3)
holoModel(3,"models/props_combine/combine_mine01.mdl")
holoPos(3,E:toWorld(vec(0,0,-6)))
holoParent(3,E)

holoCreate(5)
holoModel(5,"models/props_junk/sawblade001a.mdl")
holoPos(5,E:toWorld(vec(0,0,-4)))
holoParent(5,E)
}


if(Power){

Max = G["Max",number]

if(Onc == 0){
Onc = 1  
E:soundPlay(4,4,"buttons/button1.wav")
E:soundPlay(1,0,"ambient/machines/power_transformer_loop_2.wav")  
soundPitch(1,100)
}

if(Ranger:hit()){
    
for(I=0,Max){
G[I,entity] = Ranger:entity()
}

}else{
for(I=0,Max){
G[I,entity] = noentity()
}
}

Ranger = rangerOffset(400,E:pos(),E:up())
D=round(Ranger:distance())

holoAlpha(1,255)
holoAlpha(2,255)
E:setColor(255,255,255)

holoAng(1,holoEntity(1):toWorld(ang(0,10,0)))
holoAng(2,holoEntity(2):toWorld(ang(0,10,0)))

holoAng(4,holoEntity(5):toWorld(ang(0,1,0)))

holoAng(3,E:toWorld(ang(0,-curtime()*20,0)))
holoAng(5,E:toWorld(ang(0,-curtime()*20,0)))
holoAng(6,E:toWorld(ang(0,curtime()*20,0)))


holoPos(1, E:toWorld(vec(0,0,4))+E:up()*D/2)
holoScaleUnits(1, vec(-1,-1,-D*1.0415))

holoPos(2, E:toWorld(vec(0,0,4))+E:up()*D/2)
holoScaleUnits(2, vec(0.5,0.5,D*1.0415))

}else{

for(I=0,Max){
G[I,entity] = noentity()
}
holoAlpha(1,0)
holoAlpha(2,0)

E:setColor(90,90,90)
soundStop(1,1)
if(Onc == 1){
soundPlay(2,0,"buttons/button18.wav")
Onc = 0
}
}  


if(duped()){
selfDestructAll()    
}

if(owner() == noentity()){
selfDestructAll()         
}
