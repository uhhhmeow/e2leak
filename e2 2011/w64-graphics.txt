@name W64-Graphics
@inputs Screen:wirelink Logo:wirelink
@outputs E:entity
@persist ScrW ScrH
@trigger 
@model models/expression 2/cpu_controller.mdl

if (first() | duped()){
    E = entity()
    E["Screen", wirelink] = Screen
    Screen:egpClear()
    Logo:egpClear()
    
    ScrW = 800
    ScrH = 600
    
    E["ScrW", number] = 800
    E["ScrH", number] = 600
    
    Logo:egpScale(vec2(0, ScrW), vec2(0, ScrH))
    Logo:egpClear() 

    Logo:egpText(1, "Wiremod 64", vec2(ScrW / 2, ScrH * 0.10) )
    Logo:egpFont(1, "Arial", 60)
    Logo:egpColor(1, vec(150, 0, 0))
    Logo:egpAlign(1, 1, 1)
    
    Logo:egpText(5, "V "+E["Ver", string], vec2(ScrW / 2, ScrH * 0.18) )
    Logo:egpFont(5, "Arial", 20)
    Logo:egpColor(5, vec(150, 0, 0))
    Logo:egpAlign(5, 1, 1)
    
    Logo:egpCircle(2, vec2((ScrW / 2), (ScrH / 2)), vec2(ScrW / 4, ScrH / 4))
    Logo:egpColor(2, vec(150, 0, 0))
    Logo:egpMaterial(2, "expression 2/cog")
    
    Logo:egpCircle(3, vec2((ScrW / 1.2), (ScrH / 1.2)), vec2(ScrW / 4, ScrH / 4))
    Logo:egpColor(3, vec(150, 0, 0))
    Logo:egpMaterial(3, "expression 2/cog")
    Logo:egpAngle(3, 8)
    
    Logo:egpCircle(4, vec2((ScrW * 0.18), (ScrH / 1.2)), vec2(ScrW / 4, ScrH / 4))
    Logo:egpColor(4, vec(150, 0, 0))
    Logo:egpMaterial(4, "expression 2/cog")
    Logo:egpAngle(4, 12)
}
