@name SOS
@inputs Scr:wirelink Data User:entity
@outputs RST Gtfo
@persist Y Menu CurY Use Music:array
@model models\props/cs_office/coffee_mug.mdl
Music[1,string]="music/hl1_song25_remix3.mp3"
Music[2,string]="music/portal_still_alive.mp3"
Music[3,string]="music/portal_4000_degrees_kelvin.mp3"

entity():setColor(255,255,255)
if (first()) {timer("RST",5000)}
if (changed(Data) & Data==13) {Use=1} else {Use=0}
if (clk("RST")) {
    if (!(User:isPlayer())) {RST=1} 
    timer("RSTing",10)}
    if (RST) {timer("RSTing",10)}
if (clk("RSTing")) {RST=0 timer("RST",5000)}
if (changed(Data) & Data==17 & Y>1) {Y-=1}
if (changed(Data) & Data==18 & Y<CurY) {Y+=1}
Scr:writeString("S.OS",26,17)
if (Menu==0) {
    Scr:writeString("Home Screen",0,0)
    CurY=3
    if (Y==1) {Scr:writeString("Files",0,5,999,0)} else {Scr:writeString("Files",0,5,777,0)}
    if (Y==2) {Scr:writeString("Music",0,7,999,0)} else {Scr:writeString("Music",0,7,777,0)}
    if (Y==3) {Scr:writeString("Credits",0,9,999,0)} else {Scr:writeString("Credits",0,9,777,0)}
    if (Use==1) {
        if (Y==1) {Use=0 Menu=1}
        if (Y==2) {Use=0 Menu=2}
        if (Y==3) {Use=0 Menu=3}
        
    }
    
    
}
if (Menu==1) {
    CurY=1
    Scr:writeString("File system incomplete.",0,3)
    Scr:writeString("Press ENTER to return",0,4)
    if (Use==1) {Menu=0}
}
if (Menu==2) {
    for (I=1, Music:count()) {
        Scr:writeString(Music[I,string],0,I,777,0)
        if (Y==I) {
         Scr:writeString(Music[I,string],0,I,999,0)   
        }
    }
    CurY=Music:count()+1
    if (changed(Y)) {
    }
    if (Y>Music:count()) {Scr:writeString("Return",0,17,999,0)} else {Scr:writeString("Return",0,17,999,0)}
if (Use==1) {
    if (Y<Music:count()+1) {soundPlay(0,0,Music[Y,string])}
    if (Y>Music:count()) {Menu=0}
}
}
if (changed(Menu)) {Y=1 Use=0 RST=1}
