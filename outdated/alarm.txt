@name Alarm
@inputs R Ent:entity
@persist On Allowed:array
@trigger all
runOnChat(1)
function number isAllowed(S:string) {
    if (S==owner():steamID()) {
        return 1
    }
    for (I=1, Allowed:count()) {
        if (Allowed[I,string]==S) {
            return 1
        }
    }
    return 0
}
if (R & !On & !isAllowed(Ent:steamID())) {
    On=1
    soundPlay(0,0,"/ambient/alarms/alarm1.wav")
    for (I=1, 10) {
        Color=hsv2rgb((360/10)*I,1,1)
        printColor(Color,"Your unraidable base is being raided!")
    }

}
if (chatClk(owner()) & owner():lastSaid()=="/off") {
    hideChat(1)
    On=0
    soundPurge()
}
entity():setAlpha(0)
