@name ThinkFaster_Main
@persist Last:string Operation:string
@persist Answer Waiting Said
@persist Say:string

Time=randint(1500,60000)
print(_HUD_PRINTCENTER,"Time Between: "+round(Time/1000))
interval(Time)
if (!first()) {
if (Waiting) {interval(99999999999999)}
Length=random(1,3)
Equation=""
    OP1=randint(1,3)
    OP2=randint(1,3)
    OP3=randint(1,3)
    N1=randint(0,10)
    N2=randint(0,10)
    N3=randint(0,10)
    N4=randint(0,10)
    #1= +
    #2= -
    #3= *
    #4= /
    if (!Said) {
        Answer=N1
        if (Length>=1) {
    if (OP1==1) {Equation="("+N1+" + "+N2+")" Answer+=N2} 
    elseif (OP1==2) {Equation="("+N1+" - "+N2+")" Answer-=N2}
    elseif (OP1==3) {Equation="("+N1+" * "+N2+")" Answer*=N2}
    elseif (OP1==4) {Equation="("+N1+" / "+N2+")" Answer/=N2}
}
if (Length>=2) {
    if (OP2==1) {Equation+=" + "+N3 Answer+=N3} 
    elseif (OP2==2) {Equation+=" - "+N3 Answer-=N3}
    elseif (OP2==3) {Equation+=" * "+N3 Answer*=N3}
    elseif (OP2==4) {Equation+=" / "+N3 Answer/=N3}
}
if (Length==3) {
    if (OP3==1) {Equation+=N3+" + " + N4 Answer+=N4} 
    elseif (OP3==2) {Equation+=" - " + N4 Answer-=N4}
    elseif (OP3==3) {Equation+=" * " + N4 Answer*=N4}
    elseif (OP3==4) {Equation+=" / " + N4 Answer/=N4}
}
concmd("say "+Equation+" = ?")
print(_HUD_PRINTTALK,""+Equation+" = "+Answer)

}
if (Said) {
runOnChat(1)
}
if (owner():lastSaid():match(Equation)) {Said=1}
if (lastSaid()==Answer:toString()) {concmd("say Correct") reset()}
}
