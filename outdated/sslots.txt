@name SSlots
@inputs Scr:wirelink User2:entity
@model models/Items/combine_rifle_ammo01.mdl
@persist Menu Debug User:entity Roll:array Draw Run Price
interval(100)
Price=50
if (first()) {Menu=0}
if (first() | changed(Scr) | duped() | changed(Menu)) {
    Scr:egpClear()
    Scr:egpBox(512,vec2(256,256),vec2(512,512))
    Scr:egpMaterial(512,"maps/noicon")
    Scr:entity():setColor(vec(256,256,256))
    Scr:entity():setMaterial("models/shiny")
    Draw=1
}

if (Menu==0) {
    if (Draw) {
        Scr:egpBox(1,vec2(256,256),vec2(512,50))
        if (Run>0) {
            Scr:egpColor(1,vec(0,256,0))
            Scr:egpText(2,"Press anywhere to gamble",vec2(256,256))
    } else {
            Scr:egpColor(1,vec(256,0,0))
            Scr:egpText(2,"Please pay the owner $"+toString(Price)+" for a turn",vec2(256,256))
}
            Scr:egpAlign(2,1,1)
            Scr:egpSize(2,30)
            Scr:egpBoxOutline(3,Scr:egpPos(1),Scr:egpSize(1)*vec2(2,1))
            Scr:egpColor(3,vec(0,0,0))
    }
    
}

Draw=0
