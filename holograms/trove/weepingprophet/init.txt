@name holograms/trove/weepingprophet/init
@inputs 
@outputs
@persist Owner:entity
@trigger none
@model models/maxofs2d/cube_tool.mdl

#----------------------
#-- Setup
if (first()) {
    #include "holograms/holocore"
    
    ID = "weeping_prophet"
    
    Chip = entity()
    ScaleFactor = 3
    ToggleColMat = 1
    ToggleShading = 1
} 


#----------------------
#-- Load the hologram and clip data arrays.
elseif (CoreStatus == "InitSpawn") {
    loadContraption()
} 


#----------------------
#-- This is like if (first()) { }, code here is run only once.
elseif (CoreStatus == "InitPostSpawn") {
    CoreStatus = "RunThisCode"  
    
    #[
        Head: 1
        Torso: 61
        Abdomen: 38
    ]#
    
    Owner = owner()
    
    runOnTick(0)
    interval(100)
} 


#----------------------
#-- This is where executing code goes
elseif (CoreStatus == "RunThisCode") {
    
    interval(100)
    
    local Angle_HeadTarget = clamp(holoEntity(61):toLocal((Owner:shootPos() - holoEntity(1):pos()):toAngle()), ang(-35, -75, -35), ang(35, 75, 35))

    holoAng(1, holoEntity(61):toWorld(slerp(quat(Angle_HeadTarget), quat(holoEntity(61):toLocal(holoEntity(1):angles())), 0.5):toAngle()))
    holoAng(108, slerp(quat((Owner:shootPos() - holoEntity(108):pos()):toAngle() + ang(90, 0, 0)), quat(holoEntity(108):angles()), 0.5):toAngle())
    holoAng(112, slerp(quat((Owner:shootPos() - holoEntity(112):pos()):toAngle() + ang(90, 0, 0)), quat(holoEntity(112):angles()), 0.5):toAngle())
}

