@name holograms/vehicles/motorcycle/init
@persist [Suspension Front Rear]:table Down:vector Entity:entity
@trigger none


#----------------------
#-- Setup
if (first()) {
    #include "holograms/holocore"
    
    ID = "Motorcycle_V1"
    
    Chip = entity()
    ScaleFactor = 0.75
    ToggleColMat = 1
    ToggleShading = 0
} 


#----------------------
#-- Load the hologram and clip data arrays.
elseif (CoreStatus == "InitSpawn") {
    loadContraption()
} 


#----------------------
#-- This is like if (first()) { }, code here is run only once.
elseif (CoreStatus == "InitPostSpawn") {
    CoreStatus = "RunThisCode"  
    
    #- Holograms
    holoSkin(44, 2)    
    holoScale(44, vec(1,1,1))
    holoBodygroup(44, 1, 3)
    holoBodygroup(44, 2, 1)
    holoBodygroup(44, 3, 1)

    holoSkin(45, 2)    
    holoScale(45, vec(1,1,1))
    holoBodygroup(45, 1, 3)
    holoBodygroup(45, 2, 1)

    #- Suspension
    Entity = entity()
    Down = vec(0,0,-1)

    Suspension = table(
        "Rear" = table(
            "PosRD"  = Entity:toLocal(holoEntity(44):pos()):setZ(0),
            "Wheel"  = holoEntity(44),
            "ShockT" = holoEntity(42),
            "ShockB" = holoEntity(40),
            "Arm"    = holoEntity(36)
        ),
        "Front" = table(
            "PosRD"  = Entity:toLocal(holoEntity(45):pos()):setZ(0),
            "Wheel"  = holoEntity(45),
            "ShockT" = holoEntity(46),
            "ShockB" = holoEntity(49)        
        )
    )
    
    Rear  = Suspension["Rear", table]
    Front = Suspension["Front", table]

    function table:rSusp()
    {
        #-Suspension
        local Ranger = rangerOffset(50, Entity:toWorld(This["PosRD", vector]), Down)

        local Axis   = Entity:toLocalAxis((Ranger:position() + Ranger:hitNormal()*12.5) - This["Arm", entity]:pos())
        local Atan   = atan(-Axis:z(), Axis:x())
        holoAng(holoIndex(This["Arm", entity]), Entity:toWorld(ang(max(130, Atan <= 0 ? 180 : Atan), 0, 0)))

        #-Shocks
        local Axis = Entity:toLocalAxis( This["ShockB", entity]:pos() - This["ShockT", entity]:pos() )
        holoAng(holoIndex(This["ShockB", entity]), Entity:toWorld(ang(atan(Axis:z(), -Axis:x()), 0, 90)))
        holoAng(holoIndex(This["ShockT", entity]), Entity:toWorld(ang(atan(-Axis:z(), Axis:x()), 0, 90)))
    }
    
    function table:fSusp()
    {
        #-Suspension
        local Ranger = rangerOffset(50, Entity:toWorld(This["PosRD", vector]), Down)

        holoPos(holoIndex(This["Wheel", entity]), Entity:toWorld(This["PosRD", vector]:setZ(-clamp(Ranger:distance(), 25, 40) + 12.5)))

        #-Shocks
        local Axis = Entity:toLocalAxis( This["ShockB", entity]:pos() - This["ShockT", entity]:pos() )
        holoAng(holoIndex(This["ShockB", entity]), Entity:toWorld(ang(atan(Axis:z(), -Axis:x()), 0, 90)))
        holoAng(holoIndex(This["ShockT", entity]), Entity:toWorld(ang(atan(-Axis:z(), Axis:x()), 0, 90)))
    }
    
    timer("RunPhysics", 60)
    runOnTick(0)
} 


#----------------------
#-- This is where executing code goes
elseif (CoreStatus == "RunThisCode") {
    if (clk("RunPhysics")) {
        timer("RunPhysics", 60)

        Down = -Entity:up()

        Front:fSusp()
        Rear:rSusp()
    }
}

