################
# > HAAAX Swep: The hax swep, recreated in E2.
# > How to use: Spawn it, and hold E to fire a monitor where you look.
################

@name HAAAX Swep E2 by Runeneo
@outputs Fire
@persist I

interval(10)

if (first()|duped()) {
    propSpawn("models/props_lab/monitor01a.mdl",0)
    print("Thank you for using The HAAAX Swep E2 by Runeneo!")
    print("Press E to fire the monitor where your looking!")
    holoCreate(0)
    holoAlpha(0, 0)
}

holoPos(0, owner():pos()+vec(0,0,64))

O = owner():pos()
L = owner():aimPos()
Fire = (owner():keyUse())
findIncludePlayerProps(owner())
findByClass("prop_physics")
OwnerProps=findToArray()

for(Index=1, OwnerProps:count()) {
    
PC=OwnerProps[Index,entity]

if (!Fire) {
PC:setAlpha(0)
PC:applyForce(((O-PC:pos()+vec(0,0,110))*10-PC:vel())*PC:mass())
}

if (Fire) {
PC:setAlpha(255)
PC:setMass(50000)
PC:applyForce(((L-PC:pos())*1)*PC:mass()) 
}

if (changed(Fire)&Fire) {
holoEntity(0):soundPlay(I,0,"weapons/hacks01.wav")
I+=1
}
}
