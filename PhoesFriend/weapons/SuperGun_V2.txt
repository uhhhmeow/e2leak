@name SuperGun V2
@inputs Turret:wirelink
@outputs 
@persist Turr:entity
@trigger 

if(first())
{
    runOnTick(1)
    holoCreate(1)
    holoAlpha(1, 0)
}

Turr = Turret:entity()

if(owner():weapon():type() == "weapon_357")
{
    holoPos(1, owner():shootPos()+owner():eye()*20)
    holoAng(1, owner():eyeAngles())
    
    Turr:setPos(holoEntity(1):pos()+holoEntity(1):forward()*60)
    Turr:setAng(holoEntity(1):angles())
    timer("parent",100)
    
    Turr:propNotSolid(1)
    Turr:setAlpha(0)
    
    if(owner():keyAttack2())
    {
        Turret["Fire", number] = 1
    }
    else
    {
        Turret["Fire", number] = 0
    }
}

if(clk("parent"))
{
    Turret["Fire", number] = 0
    #Turr:parentTo(holoEntity(1))
}
