@name Car Controller
@inputs [Driver]:wirelink [Hull]:entity
@outputs 
@persist Core:entity VecDiff
runOnTick(1)
function entity:turnTo(Tarang:angle) {
    V = This:toLocal(rotationVector(quat(Tarang)/quat(This))+This:pos())
    This:applyTorque((150*V - 15*This:angVelVector())*This:inertia()*3)  
}
function entity:moveTo(Tarpos:vector) {
    This:applyForce(((Tarpos-This:pos())*15-This:vel())*(This:mass()*0.75))    
}

function number rotationDifference(E1:entity, E2:entity){
    Out = E1:angles():yaw()
    Out2 = E2:angles():yaw()
    if((Out - Out2) > 180){Out1b = -(360 - (Out))}else{Out1b = Out}
    if((Out2 - Out) > 180){Out2b = -(360 - (Out2))}else{Out2b = Out2}
    return Out2b - Out1b    
}

function number entity:getAltitude(){
    RangerData = rangerOffset(1000,This:pos(),vec(0,0,-1))
    return RangerData:distance()
}

function void entity:hover(Altitude:number, Target:number){
    VecDiff = Target - Altitude
    ForceVec = (($VecDiff*15) + (VecDiff*2))*This:mass()/3
    This:applyForce(vec(0,0,ForceVec))
}

function void entity:stabiliser(OverrideVec:vector, Mul:number){
    This:applyForce(This:vel()*-2*Mul)    
}
if(first() || dupefinished()){
    if(->Hull){
        Core = Hull    
    }else{
        Core = entity():isWeldedTo()
    }
    Core:setMass(1000)
    rangerFilter(Core)
    rangerPersist(1)
}

if(Core:pos() != vec()){
    Altitude = Core:getAltitude()
    HoverHeight = 50
    Core:hover(Altitude, HoverHeight)

    Core:stabiliser(vec(),30)   
    Core:turnTo(ang(0,Core:angles():yaw(),0))
}
