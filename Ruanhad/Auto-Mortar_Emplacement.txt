@name Auto-Mortar Emplacement
@inputs Gun:entity MuzzleVelocity
@outputs Fire Active AoR
@persist TargetPitch
runOnTick(1)

function entity:turnTo(Tarang:angle) {
    Torque = This:toLocal(rotationVector(quat(Tarang)/quat(This))+This:pos())
    This:applyTorque((150*Torque - 15*This:angVelVector())*This:inertia()*3)  
}
function entity:moveTo(Tarpos:vector) {
    This:applyForce(((Tarpos-This:pos())*15-This:vel())*(This:mass()*0.75))    
}

TargetPos = owner():aimPos()
Yaw = (TargetPos - Gun:pos()):toAngle():yaw()
Range = (TargetPos - entity():pos()):length() / 2
G = 0.9
X = Range
Y = (TargetPos:z() - Gun:pos():z())*1
V = MuzzleVelocity
Elev = Gun:elevation(TargetPos)

#AoR = 0.5*asin( (G*(Range/2))/(V*V) )#Ang = 0.5 * asin( (G*D) / (V*V) ); G = Gravity, D = range, V = muzzle velocity
#AoR = atan( (V*V) - G*(G*(X*X) + (2*Y*(V*V))) / G*X )
#AoR = atan( (V^2 + sqrt( V^4 - G*( G*(X^2)*cos(Elev)^2 + 2*V^2*sin(Elev) ) )) / G*X*cos(Elev) )
AoR = atan((V^2 - sqrt(V^4 - G*(G*Range^2 + 2*Y*V^2)))/(G*Range))

#[if(AoR > 0 ){
    TargetPitch = AoR    
}]#
TargetPitch = AoR 

Gun:moveTo(entity():toWorld(vec(0,0,100)))
Gun:turnTo(ang(-90 + TargetPitch,Yaw,0))
