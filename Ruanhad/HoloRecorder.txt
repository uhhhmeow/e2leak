@name HoloRecorder
@outputs Holograms
@persist [PartsPos PartsModel PartsAng Parts]:array Spin
interval(50)

function entity holoData (Index,Model:string,Scale:vector,Pos:vector,Ang:angle,Material:string,Color:vector,Parent:entity){
    holoCreate(Index)
    holoModel(Index,Model)
    holoScale(Index,Scale)
    holoPos(Index,Pos)
    holoAng(Index,Ang)
    holoMaterial(Index,Material)
    holoColor(Index,Color)
    holoParent(Index,Parent)
    return holoEntity(Index)
}
if(first()){
    holoData(0,"cube",vec(1),entity():toWorld(vec(0,0,100)),ang(),"",vec(255),entity())    
}

if(owner():keyUse() && changed(owner():keyUse())){
    Target = owner():aimEntity()
    Parts = Target:getConstraints()
    
    holoModel(0,Target:model())
    #holoAng(0,Target:angles())
    for(I=1,Holograms){
        holoDelete(I)
    }
    
    for(I=1,Parts:count()){
        Part = Parts[I,entity]
        PartsPos:pushVector(Target:toLocal(Part:pos()))
        PartsAng:pushAngle(Target:toLocal(Part:angles()))
        PartsModel:pushString(Part:model())
        holoData(I,Part:model(),vec(1),holoEntity(0):toWorld(PartsPos[I,vector]),holoEntity(0):toWorld(PartsAng[I,angle]),"",vec(255),holoEntity(0))
        Holograms++
    }
        
}
Spin = 1
holoAng(0,holoEntity(0):angles():rotateAroundAxis(entity():up(),Spin))
