@name WireFort Controller - Autosecurity
@inputs Open Open1 Eject [Base InnerLeft InnerRight Left Right]:entity
@outputs [Bounds InsideMin InsideMax O OVel]:vector Fire
@outputs OLocal:vector Players:array Allowed:array PlayerOffsets:array PushPos
@outputs Alarm Push Open2 Open3 Player:entity
@outputs I Y Y1 [AllowedPlayers]:array
runOnTick(1)
Player = Players[1,entity]
if(first()){
    Parts = entity():getConstraints()
    for(I=1,Parts:count()){
        Parts[I,entity]:propFreeze(1)
    }
    Left:soundPlay(0,0, "/thrusters/rocket00.wav")
        Right:soundPlay(1,0, "/thrusters/rocket00.wav")
    InnerLeft:soundPlay(5,0, "/thrusters/rocket00.wav")
        InnerRight:soundPlay(6,0, "/thrusters/rocket00.wav")
holoScale(1,vec(27,20,15)*-1)   
holoPos(1,Base:toWorld(vec(0,0,102)))
holoAng(1,ang())
holoMaterial(1,"models/debug/debugwhite")
holoParent(1,Base)

Allowed:insertEntity(1,owner())
timer("Reset",0)
Y = 130
Push = 130
}

if(Y < 5 | Y > 120){
soundPitch(0, 0)
soundPitch(1, 0)
}else{
soundPitch(0,75)
soundPitch(1,75)
}
if(Y1 < 5 | Y1 > 120){
soundPitch(5, 0)
soundPitch(6, 0)
}else{
soundPitch(5,75)
soundPitch(6,75)
}

if((changed(Y<1)  & Y < 1) | (changed(Y>125)  & Y > 125)){
Left:soundPlay(3,2,"/doors/doorstop4.wav")    
Right:soundPlay(4,2,"/doors/doorstop4.wav")    
}
if((changed(Y1<1)  & Y1 < 1) | (changed(Y1>125)  & Y1 > 125)){
InnerLeft:soundPlay(7,2,"/doors/doorstop4.wav")    
InnerRight:soundPlay(8,2,"/doors/doorstop4.wav")    
}
if(Eject && Alarm){
Open2 = 0    
}else{
Open2 = 1
}
if(Open && Open2){Open3 = 1}else{Open3 = 0}

if(Open3 && Y < 129){Y+=2.5}
if(!Open3 && Y > 0){Y-=5}

Left:setPos(Base:toWorld(vec(-130.5,95+Y,100)))
Right:setPos(Base:toWorld(vec(-130.5,-95-Y,100)))


if(clk("Reset")){

InnerLeft:propFreeze(1)
InnerRight:propFreeze(1)

Left:propFreeze(1)
Right:propFreeze(1)

Left:setAng(Base:toWorld(ang(90,0,0)))
Right:setAng(Base:toWorld(ang(90,0,0)))
InnerLeft:setAng(Base:toWorld(ang(90,0,0)))
InnerRight:setAng(Base:toWorld(ang(90,0,0)))
timer("Reset",500)
}

############## Security Code ############



holoColor(1,vec4(255,0,0,Alarm*255))
findInBox(Base:toWorld(vec(150,150,0)),Base:toWorld(vec(-150,-150,100)))

findClipToClass("player")
findExcludePlayer(owner())
Players = findToArray()
for(I=1,Players:count()){
if(Players[I,entity]:name() != owner():name()){
Alarm = 1
}
PlayerOffsets[I,number] = Base:toLocal(Players[I,entity]:pos()):x()
if(Players[I,entity]:isAlive()!= 1){
Players:remove(I)    
}
}
#Nearest = findClosest(Base:toWorld(vec(0,-140,0)))
#OLocal = Base:toLocal(owner():pos())
Alarm = 0



if(Open1 && Y1 < 80){Y1+=2.5}
if(!Open1 && Y1 > 0){Y1-=2.5}

if(Players:count() > 0 && Eject){
PushPos = PlayerOffsets:max()+42
}
else{
PushPos = 130    
}

if(Eject && Players:count() == 0 && Push < PushPos){Push+=2.5}
if(Eject && Players:count() != 0 && Push > PushPos){Push-=2.5}
if(!Eject && Push < PushPos){Push+=2.5}
if(!Eject && Push > PushPos){Push-=2.5}
   
if(Eject && Push < -40){Fire = 1}else{Fire = 0} 

InnerLeft:setPos(Base:toWorld(vec(Push,95+Y1,100)))
InnerRight:setPos(Base:toWorld(vec(Push,-95-Y1,100)))

InnerLeft:setAng(Base:toWorld(ang(0,90,90)))
InnerRight:setAng(Base:toWorld(ang(0,90,90)))
