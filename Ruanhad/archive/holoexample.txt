@name holoexample
@inputs 
@outputs 
@persist E:entity Inc Dist # variables are kept here if we need them next cycle
interval(50) # this controls how frequently the E2 updates



if(first()){
    E = entity() # the E2 gate.
    holoCreate(1)
    holoModel(1,"hq_sphere")
    holoScale(1,vec(1,1,1)*2)
    holoPos(1,E:pos()+vec(0,0,50))
    holoColor(1,vec(1,0,0)*255)
    holoParent(1,E)
    
    holoCreate(2)
    holoModel(2,"hq_sphere")
    holoScale(2,vec(1,1,1)*3)
    holoPos(2,E:pos()+vec(0,0,50))
    holoColor(2,vec(0,1,0)*255)
    holoParent(2,E)
    
    holoCreate(3)
    holoModel(3,"hq_sphere")
    holoScale(3,vec(1,1,1)*3)
    holoPos(3,E:pos()+vec(0,0,50))
    holoColor(3,vec(0,0,1)*255)
    holoParent(3,E)
    
    
    holoEntity(2):setTrails(300,10,0.5,"trails/physbeam",vec(1,0.5,1)*255,255)
    holoEntity(3):setTrails(300,10,0.5,"trails/physbeam",vec(1,0.5,1)*255,255)
    
    
}

# The following code will make the bottom sphere move in circles
Inc = Inc + 5 # a number that increases constantly
Dist = 150 # Distance that it'll move back and forth
X1 = cos(Inc*1.5)*Dist
Y1 = sin(Inc*1.5)*Dist

X = cos(Inc)*Dist/2
Y = sin(Inc)*Dist/2
holoPos(3,entity():pos() + vec(X,Y,50))

holoPos(2,entity():pos() + vec(-X1,-Y1,50))


