@name Hologram testbed
@outputs 
@persist 
runOnTick(1)

function holoData (Index,Model:string,Scale:vector,Pos:vector,Ang:angle,Mat:string,Color:vector,Parent:entity){
    holoCreate(Index)
    holoModel(Index,Model)
    holoScale(Index,Scale)
    holoPos(Index,Pos)
    holoAng(Index,Ang)
    holoMaterial(Index,Mat)
    holoColor(Index,Color)
    holoParent(Index,Parent)
    
}
if(first()){

    holoData(1,"models/props_interiors/pot01a.mdl",vec(1,1,1),entity():toWorld(vec(0,0,50)),entity():toWorld(ang()),"",vec(1,1,1)*255,entity())    #models/shiny
}
