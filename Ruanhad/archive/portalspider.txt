#Four legged mech with twin turrets and swiveling upper section
#based on a similar design from Supreme Commander 2
@name AAmech
@inputs [Controls Camera]:wirelink [Core FL FR RL RR Torso Larm Rarm]:entity
@outputs Fire
@persist [Campos Camvec Cpos Corepos Torpos Larmpos Rarmpos]:vector Zoom Posture
@persist CoreP CoreY CoreR TorP TorY TorR LarmP LarmY LarmR RarmP RarmY RarmR Inc R:array
@persist [FLfootgo FLToepos FRfootgo FRToepos RLfootgo RLToepos RRfootgo RRToepos]:vector
interval(10)

if(first() | duped()){
Zoom = 200
Posture = 50    
R = entity():getConstraints()
F1footgo = FL:pos()
F2footgo = FR:pos()
F3footgo = RL:pos()
F4footgo = RR:pos()
}

#start controls
W = Controls:number("W")
A = Controls:number("A")
S = Controls:number("S")
D = Controls:number("D")
M1 = Controls:number("Mouse1")
M2 = Controls:number("Mouse2")
Space = Controls:number("Space")
Shift = Controls:number("Shift")#
Mwheelup = Controls:number("NextWeapon")
Mwheeldown = Controls:number("PrevWeapon")
Active = Controls:number("Active")
Seat = Controls:entity("Entity")
#end controls
Camera:setNumber("Activated",Active)
Camera:setVector("Position",Campos)
Camera:setVector("Direction",Camvec)

if(M1){Fire = 1}else{Fire = 0}

if(Mwheelup){Zoom = Zoom + 10}
if(Mwheeldown){Zoom = Zoom - 10}
#Cang = (Cpos - Core:pos()):normalized()
#if((Core:pos() - Cpos):length() > 300){Cammul = 100}else{Cammul = 10}
#if((Core:pos() - Cpos):length() > 10){Cpos = Cpos - (Cang * Cammul)}
Campos = Torso:pos() + vec(0,0,100)-Seat:driver():eye()*Zoom
Camvec = Seat:driver():eye()

if(Space & Posture < 75){Posture = Posture + 1}
if(Space & Posture > 75){Posture = Posture - 1}
if(!Space & !Shift & Posture < 35){Posture = Posture + 1}
if(!Space & !Shift & Posture > 35){Posture = Posture - 1}
#hippos

CoreP = Core:angles():pitch()
CoreY = Core:angVel():yaw()/100
CoreR = Core:angles():roll()
CorePitch = (($CoreP*150)+(CoreP*20))*Core:mass()/3*-1
CoreYaw = (($CoreY*150)+(CoreY*20))*Core:mass()/3*-1
CoreRoll =(($CoreR*150)+(CoreR*20))*Core:mass()/3*-1
Core:applyAngForce(ang(CorePitch,CoreYaw,CoreRoll))

Corepos = Core:pos() - (((FL:pos() + FR:pos() + RL:pos() + RR:pos())/4)+vec(0,0,Posture))
Corevec = (($Corepos*150)+(Corepos*20))*Core:mass()/10*-1
Core:applyForce(Corevec)

#torsopos
Torpos = Torso:pos() - (Core:pos()+vec(0,0,20))
Torvec = (($Torpos*150)+(Torpos*20))*Torso:mass()/15 *-1
Torso:applyForce(Torvec)

if(Active){TorY = Torso:bearing(Seat:driver():eye()*500000)}else{TorY = Torso:bearing(Core:forward()*500000)}
TorP = Torso:angles():pitch()
TorR = Torso:angles():roll()
TorPitch = (($TorP*150)+(TorP*20))*Torso:mass()/3*-1
TorYaw = (($TorY*150)+(TorY*20))*Torso:mass()/3*-1
TorRoll =(($TorR*150)+(TorR*20))*Torso:mass()*-1
Torso:applyAngForce(ang(TorPitch,TorYaw,TorRoll))


#Target = Seat:driver():eye()*500000
rangerFilter(entity():getConstraints())
Tranger = rangerOffset(10000,Campos,Seat:driver():eye())
Target = Tranger:position()
#gun arms
Larmpos = Larm:pos() - Torso:toWorld(vec(0,60,-15))
Larmvec = (($Larmpos*150)+(Larmpos*20))*Larm:mass()/3*-1
Larm:applyForce(Larmvec)

if(Active)
{
    LarmP = Larm:elevation(Target)
    LarmY = Larm:bearing(Target)
}
else
{
    LarmP = Larm:angles():pitch()
    LarmY = Larm:bearing(Torso:forward()*500000)
}
LarmR = Larm:angles():roll()
LarmPitch = (($LarmP*150)+(LarmP*20))*Larm:mass()/3*-1
LarmYaw = (($LarmY*150)+(LarmY*20))*Larm:mass()/3*-1
LarmRoll =(($LarmR*150)+(LarmR*20))*Larm:mass()/3*-1
Larm:applyAngForce(ang(LarmPitch,LarmYaw,LarmRoll))

#Rarm
Rarmpos = Rarm:pos() - Torso:toWorld(vec(0,-60,-15))
Rarmvec = (($Rarmpos*150)+(Rarmpos*20))*Rarm:mass()/3*-1
Rarm:applyForce(Rarmvec)

if(Active)
{
    RarmP = Rarm:elevation(Target)
    RarmY = Rarm:bearing(Target)
}
else
{
    RarmP = Rarm:angles():pitch()
    RarmY = Rarm:bearing(Torso:forward()*500000)
}
RarmR = Rarm:angles():roll()
RarmPitch = (($RarmP*150)+(RarmP*20))*Rarm:mass()/3*-1
RarmYaw = (($RarmY*150)+(RarmY*20))*Rarm:mass()/3*-1
RarmRoll =(($RarmR*150)+(RarmR*20))*Rarm:mass()/3*-1
Rarm:applyAngForce(ang(RarmPitch,RarmYaw,RarmRoll))



#walk
#if(W | A | S | D){
FLToepos = FL:pos() - (FLfootgo + vec(0,0,FL:pos():distance(FLfootgo)/2))
FLToevec = (($FLToepos*150)+(FLToepos*20))*(FL:mass()/10*-1)
FL:applyOffsetForce(FLToevec,FL:pos())

FRToepos = FR:pos() - (FRfootgo + vec(0,0,FR:pos():distance(FRfootgo)/2))
FRToevec = (($FRToepos*150)+(FRToepos*20))*(FR:mass()/10*-1)
FR:applyOffsetForce(FRToevec,FR:pos())

RLToepos = RL:pos() - (RLfootgo + vec(0,0,RL:pos():distance(RLfootgo)/2))
RLToevec = (($RLToepos*150)+(RLToepos*20))*(RL:mass()/10*-1)
RL:applyOffsetForce(RLToevec,RL:pos())

RRToepos = RR:pos() - (RRfootgo + vec(0,0,RR:pos():distance(RRfootgo)/2))
RRToevec = (($RRToepos*150)+(RRToepos*20))*(RR:mass()/10*-1)
RR:applyOffsetForce(RRToevec,RR:pos())
#}

#control
if(W == 1)
{
    if(Inc < 200){Inc = Inc + 5}
if(Inc >= 200){Inc = 0}
    if(Inc <= 50){Foot = 1}
    if(Inc <= 100 & Inc > 50){Foot = 3}
    if(Inc <= 150 & Inc > 100){Foot = 2}
    if(Inc <= 200 & Inc > 150){Foot = 4}
    
Vel = 75
}
if(S == 1 | A == 1 | D == 1)
{
    if(Inc < 200){Inc = Inc + 5}
if(Inc >= 200){Inc = 0}
    if(Inc <= 50){Foot = 1}
    if(Inc <= 100 & Inc > 50){Foot = 3}
    if(Inc <= 150 & Inc > 100){Foot = 2}
    if(Inc <= 200 & Inc > 150){Foot = 4}
    
Vel = -50
}


if(!W & !S & !A & !D)
{
    Foot = 0
    Vel = 0
    }


if(A)
{Dire = Torso:right()}

if(D)
{Dire = Torso:right()*-1}

if(!A & !D)
{Dire = Seat:driver():eye()}

    
if(changed(Foot))
{
    Core:soundPlay(1,500,"ambient\levels\labs\machine_stop1.wav")
    soundPitch(1,300)
    }
if(Foot == 1)
    {
        rangerFilter(R)
        rangerHitEntities(1)
        rangerHitWater(1)
    Foottar = rangerOffset(5000,Torso:toWorld(vec(75,75,0)) + Dire*Vel,vec(0,0,-1))
    FLfootgo = Foottar:position() + vec(0,0,0)   
    }
if(Foot == 2)
    {
        rangerFilter(R)
        rangerHitEntities(1)
        rangerHitWater(1)
    Foottar = rangerOffset(5000,Torso:toWorld(vec(75,-75,0)) + Dire*Vel,vec(0,0,-1))
    FRfootgo = Foottar:position() + vec(0,0,0)        
    }
if(Foot == 3)
    {
        rangerFilter(R)
        rangerHitEntities(1)
        rangerHitWater(1)
    Foottar = rangerOffset(5000,Torso:toWorld(vec(-75,75,0)) + Dire*Vel,vec(0,0,-1))
    RLfootgo = Foottar:position() + vec(0,0,0)
    }
if(Foot == 4)
    {
        rangerFilter(R)
        rangerHitEntities(1)
        rangerHitWater(1)
    Foottar = rangerOffset(5000,Torso:toWorld(vec(-75,-75,0)) + Dire*Vel,vec(0,0,-1))
    RRfootgo = Foottar:position() + vec(0,0,0)
    }


