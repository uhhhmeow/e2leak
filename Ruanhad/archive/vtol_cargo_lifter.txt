@name Vtol Cargo Lifter
@inputs [Core Container]:entity
@outputs Offset:vector Angles:angle
@persist 
runOnTick(1)

Angles = Core:toLocal(Container:angles())
Offset = Core:toLocal(Container:pos())
