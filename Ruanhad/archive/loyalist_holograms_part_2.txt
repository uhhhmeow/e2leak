@name Loyalist Holograms Part 2
@inputs [Lhand Rhand Head Torso Core FL FR RL RR]:entity Color:vector Material:string
@outputs 
@persist 
interval(1000) 
runOnChat(1)
if(first()){
#Shoulders
for(I=1,11){
holoCreate(I)
holoModel(I,"cube")
holoMaterial(I,"Phoenix_storms/mat/mat_phx_metallic")
holoColor(I,vec(65,65,65))
holoPos(I,Torso:pos())
holoAng(I,Torso:angles())
if(I <= 6){
holoParent(I,Torso)
}else{holoParent(I,Core)}
}

holoModel(1,"hqsphere")
holoPos(1,Torso:toWorld(vec(0,55,-20)))
#holoAng(1,Core:toWorld(ang(45,90,-90)):rotateAroundAxis(Core:forward(),90):rotateAroundAxis(Core:right(),10))
holoScale(1,vec(1,1,1)*1.25)

holoModel(2,"hqsphere")
holoPos(2,Torso:toWorld(vec(0,-55,-20)))
#holoAng(2,Core:toWorld(ang(45,90,-90)):rotateAroundAxis(Core:forward(),180):rotateAroundAxis(Core:right(),10))
holoScale(2,vec(1,1,1)*1.25)


holoModel(3,"models/props_junk/TrashBin01a.mdl")
holoPos(3,Torso:toWorld(vec(0,25,-20)))
holoAng(3,Torso:toWorld(ang(60,-90,0)))
holoScale(3,vec(1.5,1,1)*1)

holoModel(4,"models/props_junk/TrashBin01a.mdl")
holoPos(4,Torso:toWorld(vec(0,-25,-20)))
holoAng(4,Torso:toWorld(ang(60,90,0)))
holoScale(4,vec(1.5,1,1)*1)

holoModel(5,"models/props_lab/monitor02.mdl")
holoPos(5,Torso:toWorld(vec(0,25,-10)))
holoAng(5,Torso:toWorld(ang(50,-90,0)))
holoScale(5,vec(1.5,1,1)*1)

holoModel(6,"models/props_lab/monitor02.mdl")
holoPos(6,Torso:toWorld(vec(0,-25,-10)))
holoAng(6,Torso:toWorld(ang(50,90,0)))
holoScale(6,vec(1.5,1,1)*1)

#models/props_junk/TrashBin01a.mdl


holoModel(7,"models/props_junk/TrashBin01a.mdl")
holoPos(7,Torso:toWorld(vec(6,0,-30)))
holoAng(7,Torso:toWorld(ang(0,0,0)))
holoScale(7,vec(1.5,1,1)*0.75)


#models/props_combine/combine_light002a.mdl
holoModel(8,"models/props_combine/combine_light002a.mdl")
holoPos(8,Core:toWorld(vec(25,0,10)))
holoAng(8,Core:toWorld(ang(0,0,0)))
holoScale(8,vec(1,2,1)*1.5)

holoModel(9,"models/props_combine/combinetower001.mdl")
holoPos(9,Core:toWorld(vec(10,-10,0)))
holoAng(9,Core:toWorld(ang(0,90,90)))
holoScale(9,vec(1.25,1,0.5)*0.25)

holoModel(10,"models/mechanics/solid_steel/type_f_6_4.mdl")
holoPos(10,Core:toWorld(vec(0,0,-15)))
holoAng(10,Core:toWorld(ang(0,90,-10)))
holoScale(10,vec(1,1,0.6)*1)

holoModel(11,"models/props_junk/TrashBin01a.mdl")
holoPos(11,Core:toWorld(vec(0,0,-15)))
holoAng(11,Core:toWorld(ang(90,0,0)))
holoScale(11,vec(1,1.5,1.5)*1)

}
holoColor(7,Color)
holoMaterial(7,Material)

#holoColor(11,Color)
#holoMaterial(11,Material)
#holoColor(9,Color)
