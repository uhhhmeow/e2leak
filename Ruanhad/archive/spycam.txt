@name SpyCam
@inputs [Controls Camera]:wirelink
@outputs Pos:vector Ang:angle Dir:vector
@persist
runOnTick(1)
#start controls
W = Controls:number("W")
A = Controls:number("A")
S = Controls:number("S")
D = Controls:number("D")
R = Controls:number("R")
M1 = Controls:number("Mouse1")
M2 = Controls:number("Mouse2")
Space = Controls:number("Space")
Shift = Controls:number("Shift")#
Alt = Controls:number("Alt")#
Mwheelup = Controls:number("NextWeapon")
Mwheeldown = Controls:number("PrevWeapon")
Active = Controls:number("Active")
Seat = Controls:entity("Entity")

#end controls
Camera:setNumber("Activated",Active)
Camera:setVector("Position",Pos)
Camera:setVector("Direction",Dir)

function resetCam(){
    Pos = Seat:pos() + vec(0,0,75) 
    Dir = vec()   
}
Ang = Dir:toAngle()
if(first()){
    resetCam()
}
if(!Active && changed(Active)){
    resetCam()    
}
Dir = Seat:driver():eye()
Speed = 5+(Shift*10)
Dire = (Dir:toAngle():right()*(D-A) + Dir:toAngle():up()*(Space-Alt) + Dir*(W-S))*Speed
Pos += Dire

