@name Tachikoma Mk1
@inputs HeadPitch [Hip FL FR RL RR]:entity
@outputs 
@persist Inc
runOnTick(1)
#E:applyAngForce(-E:angVel()*ang(0,1,0)*shiftL(E:inertia()):toAngle())

function holoData (Index,Model:string,Scale:vector,Pos:vector,Ang:angle,Material:string,Color:vector,Parent:entity){
    holoCreate(Index)
    holoModel(Index,Model)
    holoScale(Index,Scale)
    holoPos(Index,Pos)
    holoAng(Index,Ang)
    holoMaterial(Index,Material)
    holoColor(Index,Color)
    holoParent(Index,Parent)
    
}

PhxMetallic = "Phoenix_storms/mat/mat_phx_metallic"
Shiny = "models/shiny"
Camo = "models/XQM/CellShadedCamo_diffuse"
Grey = vec(1,1,1)*50
PaleGrey = vec(1,1,1)*100
Orange = vec(0.75,0.5,0)*255
Thighlength = 4*12
Shinlength = 5*12

function entity:moveTo(Tarpos:vector){
    This:applyForce(((Tarpos-This:pos())*15-This:vel())*(This:mass()))    
}
function entity:turnTo(Tarang:angle){
    V = This:toLocal(rotationVector(quat(Tarang)/quat(This))+This:pos())
    This:applyTorque((150*V - 15*This:angVelVector())*This:inertia()*3)  
}

function makeLeg(StartIndex,MFHip:entity, Foot:entity, TL,SL,LegScale){
I=StartIndex
while(I < StartIndex + 10){
holoCreate(I)
I++   
}
    TL = TL*LegScale
    SL = SL*LegScale
    holoData(StartIndex,"hqsphere",vec(1,1,1)*2*LegScale,vec(),ang(),Shiny,PaleGrey,MFHip) # Hip Joint
holoData(StartIndex+1,"cube",vec(1,1,1)*0.1*LegScale,vec(),ang(),PhxMetallic,Grey,MFHip) # True Hypotenuse
holoData(StartIndex+2,"cube",vec(1,1,1)*0.1*LegScale,vec(),ang(),PhxMetallic,Grey,holoEntity(StartIndex+1)) # Fake Hypotenuse
holoData(StartIndex+3,"hqsphere",vec(1,1,1)*1.5*LegScale,holoEntity(StartIndex):toWorld(vec(TL,0,0)*LegScale),holoEntity(StartIndex):toWorld(ang(0,0,0)),PhxMetallic,Grey,holoEntity(StartIndex)) # Knee joint    
holoData(StartIndex+4,"cube",vec(Thighlength/12,1,1)*0*LegScale,holoEntity(StartIndex):toWorld(vec(TL/2,0,0)*LegScale),holoEntity(StartIndex):toWorld(ang(0,0,0)),PhxMetallic,Grey,holoEntity(StartIndex))
holoData(StartIndex+5,"cube",vec(Shinlength/12,1,1)*0*LegScale,holoEntity(StartIndex+3):toWorld(vec(-SL/2,0,0)*LegScale),holoEntity(StartIndex+3):toWorld(ang(0,0,0)),PhxMetallic,Grey,holoEntity(StartIndex+3))
Thigh = holoEntity(StartIndex+4)
Shin = holoEntity(StartIndex+5)
#holoData(StartIndex+6,"models/Gibs/gunship_gibs_midsection.mdl",vec(1,0.75,1)*0.5*LegScale,Thigh:toWorld(vec(0,0,7.5)*LegScale),Thigh:toWorld(ang(-20,0,0)),Camo,Orange,Thigh)
holoData(StartIndex+7,"models/Gibs/airboat_broken_engine.mdl",vec(1,1,1)*1*LegScale,Thigh:toWorld(vec(0,0,0)*LegScale),Thigh:toWorld(ang(0,0,180)),"",vec(1,1,1)*255,Thigh)
holoData(StartIndex+8,"models/props_phx/wheels/magnetic_medium.mdl",vec(1,1,1)*1*LegScale,holoEntity(StartIndex):toWorld(vec(0,4,0)*LegScale),Thigh:toWorld(ang(0,0,90)),"",vec(1,1,1)*255,Thigh)
holoData(StartIndex+9,"models/Gibs/Scanner_gib01.mdl",vec(1.4,0.45,1)*3*LegScale,Shin:toWorld(vec(0,0,0)*LegScale),Shin:toWorld(ang(-10,0,0)),"",vec(1,1,1)*255,Shin)

}
function operateLeg(StartIndex,FHip:entity,Foot:entity,TL,SL,Hippos:vector,BipedToggle,ManipScale,Tilt){
    TL = TL*ManipScale
    SL = SL*ManipScale
Avrgpoint = (Hippos + (Foot:pos()+vec(0,0,0)))/2
Hypelength = clamp((Hippos - (Foot:pos()+vec(0,0,0))):length(),0,TL+SL)
HypeAngle = (Hippos - (Foot:pos()+vec(0,0,0))):toAngle() + ang(90,0,0)

Out = FHip:angles():yaw()
Out2 = HypeAngle:yaw()
if((Out - Out2) > 180){Out1b = -(360 - (Out))}else{Out1b = Out}
if((Out2 - Out) > 180){Out2b = -(360 - (Out2))}else{Out2b = Out2}
Out3 = Out2b - Out1b

#LCorAngle = LHypeAngle - ang(0,Out3,0)
holoScale(StartIndex,vec(1,1,1)*2) # size of the hip joint

holoPos(StartIndex+1,Avrgpoint) # position of the hypotenuse Prime
holoScale(StartIndex+1,vec(0.1,0.1,Hypelength/12)*0) # scale of Hype Prime
holoAng(StartIndex+1,HypeAngle) # Angle of Hype- Prime

holoPos(StartIndex+2,Avrgpoint) # 
holoScale(StartIndex+2,vec(0.5,1,Hypelength/15)*0) # Stuff to do with Hype Secundus
holoAng(StartIndex+2,holoEntity(StartIndex+1):toWorld(ang(0,-Out3*BipedToggle+(!BipedToggle*180)+Tilt,0)))

holoPos(StartIndex,holoEntity(StartIndex+2):toWorld(vec(0,0,Hypelength/2))) # position of the hip joint (rolls it back along the length of the hypotenuse)

BodyAng = acos((TL^2 + Hypelength^2 - SL^2)/(2*TL*Hypelength)) - 90
holoAng(StartIndex,holoEntity(StartIndex+2):toWorld(ang(-BodyAng,0,0)))
KneeAng = acos((TL^2 + SL^2 - Hypelength^2)/(2*TL*SL))
holoAng(StartIndex+3,holoEntity(StartIndex):toWorld(ang(-KneeAng,0,0)))

}
if(first() || dupefinished()){
    Hip:setMass(1000)
    timer("FL",1200*0)     
    timer("FR",1200*1)
    timer("RL",1200*2)     
    timer("RR",1200*3)  

    
    #timer("revealHolos",1200*5)   
    timer("updateHolos",50)   
}

if(clk("FL")){makeLeg(1,Hip,FL,Thighlength,Shinlength,1)}
if(clk("FR")){makeLeg(11,Hip,FR,Thighlength,Shinlength,1)}
if(clk("RL")){makeLeg(22,Hip,RL,Thighlength,Shinlength,1)}
if(clk("RR")){makeLeg(33,Hip,RR,Thighlength,Shinlength,1)}


if(clk("revealHolos")){for(I=1,21+6){holoVisible(I,players(),1)}}

Z = 0
Inc++
Foreaft = HeadPitch #sin(Inc)*10

if(clk("updateHolos")){
    Hipoffset = Hip:toWorld(vec(15,35,Z))
operateLeg(1,Hip,FL,Thighlength,Shinlength,Hipoffset,0,1,-Foreaft)
    Hipoffset = Hip:toWorld(vec(15,-35,Z))
operateLeg(11,Hip,FR,Thighlength,Shinlength,Hipoffset,0,1,Foreaft)
    Hipoffset = Hip:toWorld(vec(-15,35,Z))
operateLeg(22,Hip,RL,Thighlength,Shinlength,Hipoffset,0,1,-Foreaft)
    Hipoffset = Hip:toWorld(vec(-15,-35,Z))
operateLeg(33,Hip,RR,Thighlength,Shinlength,Hipoffset,0,1,Foreaft)
timer("updateHolos",50)
}

HipTarget = (FL:pos() + FR:pos() + RL:pos() + RR:pos())/4 + vec(0,0,40) + Hip:forward()*Foreaft
Hip:moveTo(HipTarget)
HAng = ang(sin(Inc)*10,0,0)
Hip:turnTo(HAng)
