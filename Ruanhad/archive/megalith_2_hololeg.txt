@name Megalith 2 HoloLeg
@inputs [Hip Foot]:entity FootHippos:vector
@outputs Diff
@persist 
interval(100)
#FootHippos = vec(25,25,0)
#FalseThighLength = 8 * 12
TrueThighLength = 3 * 12
MidLegLength = 3.5 * 12

ShinLength = 7 * 12
Diff = abs((Foot:pos():z() - Hip:pos():z()) + ShinLength)
FalseThighLength = clamp(((vec2(Foot:pos():x(),Foot:pos():y()) - vec2(Hip:toWorld(FootHippos):x(),Hip:toWorld(FootHippos):y())):length()+Diff)*0.75,0,TrueThighLength+MidLegLength)



HypeLength = clamp((Foot:pos() - Hip:toWorld(FootHippos)):length(),0,ShinLength + TrueThighLength + MidLegLength)

if(first() | dupefinished()){
holoCreate(1) # Hypotenuse Prime
holoCreate(2) # Hip Joint
holoCreate(3) # Knee Joint

holoCreate(4) # False Thigh
holoCreate(5) # Shin

holoCreate(6) # TrueHip
holoCreate(7) # Secondary Knee

holoCreate(8) # True Thigh
holoCreate(9) # Mid Leg


for(I=1,9){
holoMaterial(I,"Phoenix_storms/mat/mat_phx_metallic")
holoColor(I,vec(1,1,1)*50)    
holoScale(I,vec(1.25,1.25,1.25))
}

holoPos(3,holoEntity(2):toWorld(vec(FalseThighLength,0,0))) 

holoPos(4,holoEntity(2):toWorld(vec(FalseThighLength/2,0,0)))
holoAng(4,holoEntity(2):angles())
holoScale(4,vec(FalseThighLength/12,0.25,0.25)) 


holoPos(5,holoEntity(3):toWorld(vec(-ShinLength/2,0,0)))
holoAng(5,holoEntity(3):angles())
holoScale(5,vec(ShinLength/12,1,1)) 

holoPos(7,holoEntity(6):toWorld(vec(TrueThighLength,0,0))) 


holoPos(8,holoEntity(6):toWorld(vec(TrueThighLength/2,0,0)))
holoAng(8,holoEntity(6):angles())
holoScale(8,vec(TrueThighLength/12,1,1)) 


holoPos(9,holoEntity(7):toWorld(vec(-MidLegLength/2,0,0)))
holoAng(9,holoEntity(7):angles())
holoScale(9,vec(MidLegLength/12,1,1)) 

holoColor(1,vec(255,255,0))
holoColor(4,vec(255,255,0))
holoParent(1,Hip)
holoParent(2,1) 
holoParent(3,2)
holoParent(4,2)
holoParent(5,3) 

holoParent(6,4)
holoParent(7,6)
holoParent(8,6)
holoParent(9,7) 

#holoAlpha(1,0)
}
#Hypotenuse
holoPos(1,(Foot:pos() + Hip:toWorld(FootHippos))/2)
holoAng(1,(Foot:pos() - Hip:toWorld(FootHippos)):toAngle())

holoScale(1,vec(HypeLength/12,0.25,0.25))

holoPos(3,holoEntity(2):toWorld(vec(FalseThighLength,0,0))) 

holoPos(4,holoEntity(2):toWorld(vec(FalseThighLength/2,0,0)))
holoAng(4,holoEntity(2):angles())
holoScale(4,vec(FalseThighLength/12,0.25,0.25)) 




#HipJoint
holoPos(2,holoEntity(1):toWorld(vec(-HypeLength/2,0,0)))
Hipang = acos((FalseThighLength^2 + HypeLength^2 - ShinLength^2)/(2*FalseThighLength*HypeLength))
holoAng(2,holoEntity(1):toWorld(ang(-Hipang,0,0)))

#Knee Joint
KneeAng = acos((FalseThighLength^2 + ShinLength^2 - HypeLength^2)/(2*FalseThighLength*ShinLength))
holoAng(3,holoEntity(2):toWorld(ang(-KneeAng,0,0)))

#True Hip
holoPos(6,holoEntity(2):pos())
#holoAng(6,holoEntity(3):toWorld(ang(180,0,0)))
Hipang2 = acos((TrueThighLength^2 + FalseThighLength^2 - MidLegLength^2)/(2*TrueThighLength*FalseThighLength))
holoAng(6,holoEntity(2):toWorld(ang(Hipang2,0,0)))

#Secondary Knee
KneeAng2 = acos((TrueThighLength^2 + MidLegLength^2 - FalseThighLength^2)/(2*TrueThighLength*MidLegLength))
holoAng(7,holoEntity(6):toWorld(ang(KneeAng2,0,0)))

