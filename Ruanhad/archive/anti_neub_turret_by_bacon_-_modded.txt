@name anti neub turret by bacon
@inputs Turret:wirelink [Point1 Point2]:vector
@outputs Targets:array Target:entity
@persist 
@trigger 
@model models/hunter/plates/plate1x1.mdl
interval(10)

function entity holoData(Index,Model:string,Scale:vector,Pos:vector,Ang:angle,Material:string,Color:vector,Parent:entity){
    holoCreate(Index)
    holoModel(Index,Model)
    #ScaleInverter = vec(Scale:y(),Scale:x(),Scale:z())
    ScaleInverter = Scale
    holoScale(Index,ScaleInverter)
    holoPos(Index,Pos)
    holoAng(Index,Ang)
    holoMaterial(Index,Material)
    holoColor(Index,Color)
    holoParent(Index,Parent)
    return holoEntity(Index)
}


if(first()){
    holoData(0,"cube",vec(1,1,1),entity():pos(),ang(),"",vec(1,0,0)*255,entity())   
}

E=Turret:entity()

holoAng(0,ang())
holoPos(0,(Point1 + Point2)/2)
Local = (Point1-Point2)
holoScale(0,(Local/12)*-1)
#findInSphere(E:pos(),10000)
findInBox(Point1,Point2)
findExcludePlayer(owner())
findClipToClass("player")
findSortByDistance(E:pos())
Targets = findToArray()
Target = Targets[1,entity]
E:setPos(entity():pos()+entity():up()*50)
E:setAng((Target:pos()+Target:boxCenter()-E:pos()+E:vel()*10):toAngle())

if(Target:pos() != vec()){    
    Turret["Fire",number]=1 
    holoColor(0,vec(1,0,0)*255)    
}else{
    Turret["Fire",number]=0
    holoColor(0,vec(0,1,0)*255)
}
holoAlpha(0,200)
