@name Killer Rabbit
@persist A B A1 B1 Vec1:vector Vec2:vector Vec3:vector Target:entity
interval(10)

entity():setMass(500)

A1 = A1 + 0.3
A = sin(A1)*75
Sizemul = 0.75
Bmul = 0.2
B = (1 + A/200*Bmul)*Sizemul
#body
holoCreate(1,entity():pos(),vec(1,B *0.75,B*1))
holoModel(1,"dome")
holoParent(1,entity())
holoMaterial(1,"models/debug/debugwhite")

#ear1
holoCreate(2,entity():pos() + entity():forward()*4 + entity():up()*2 + entity():right()*1.5,vec(0.25,0.25,1))
holoModel(2,"hqsphere2")
holoParent(2,entity())
holoMaterial(3,"models/debug/debugwhite")
holoColor(2,vec(200,200,200))

#ear2
holoCreate(3,entity():pos() + entity():forward()*4 + entity():up()*2 + entity():right()*-1.5,vec(0.25,0.25,1))
holoModel(3,"hqsphere2")
holoParent(3,entity())
holoMaterial(3,"models/debug/debugwhite")
holoColor(3,vec(200,200,200))

#head
holoCreate(4,entity():pos() + entity():forward()*4 + entity():up()*1,vec(0.5,0.5,0.5))
holoModel(4,"hqsphere2")
holoParent(4,entity())
holoMaterial(4,"models/debug/debugwhite")

#tail
holoCreate(5,entity():pos() + entity():forward()*-5 + entity():up()*1,vec(0.5,0.5,0.5)*0.5)
holoModel(5,"hqsphere2")
holoParent(5,entity())
holoMaterial(5,"models/debug/debugwhite")

#targetting
findByClass("Player")
Target = findClosest(entity():pos())
if(changed(Target:name()))
{print(Target:name())}

B1 = Target:bearing(entity():pos())
Bear = (($B1*150)+(B1*20))*1

Vec1 = entity():pos() - vec(entity():pos():x(),entity():pos():y(),Target:pos():z()+2)
Vec2 = (($Vec1*150)+(Vec1*20))*150
entity():applyForce(-Vec2)
#entity():applyAngForce(ang(0,-Bear,0))

Vec3 = entity():pos() - Target:pos()+vec(0,0,2)
Vec4 = (($Vec3*150)+(Vec3*20))
entity():applyForce(-Vec4*500)
