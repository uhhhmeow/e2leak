@name Tardis
@inputs Policebox:entity
@outputs 
@persist [PhxMetallic]:string [Blue]:vector ScaleFactor Doors Open
interval(50)
runOnChat(1)
Policebox = entity():isWeldedTo() 
function holoData (Index,Model:string,Scale:vector,Pos:vector,Ang:angle,Mat:string,Color:vector,Parent:entity){
    holoCreate(Index)
    holoModel(Index,Model)
    holoScale(Index,Scale)
    holoPos(Index,Pos)
    holoAng(Index,Ang)
    holoMaterial(Index,Mat)
    holoColor(Index,Color)
    holoParent(Index,Parent)
}
    #[
    holoTextCreate(90)
    holoText(90,"This is a test")    
    holoTextPos(90,Policebox:toWorld(vec(0,0,10)))  
    ]#
    
function holoWriting(Index,Text:string,Scale:vector,Pos:vector,Ang:angle,Mat:string,Color:vector4,Parent:entity){
    #[
    holoTextCreate(Index)
    holoText(Index,Text)
    ]#
    holoScale(Index,Scale)
    #[
    holoTextPos(Index,Pos)
    holoTextAng(Index,Ang)
    holoTextFont(Index,Mat)
    holoTextColor(Index,Color)
    ]#
    holoParent(Index,Parent)
}
if(first()){
    PhxMetallic = "Phoenix_storms/mat/mat_phx_metallic"
    Blue = vec(0.25,0.25,0.75)*255
    Green = vec(0.25,0.75,0.25)*255
    Red = vec(0.75,0.25,0.25)*255
    
    Shiny = "models/shiny"
    Matte = "models/debug/debugwhite"
    ScaleFactor = 0.85
timer("base",1200*0)    
timer("roof",1200*1)    
timer("door_left",1200*2)    
timer("door_right",1200*3)    
timer("door_details",1200*4)    
timer("wall_left",1200*5)    
timer("wall_right",1200*6)    
timer("wall_back",1200*7)    
timer("wall_details",1200*8)    
timer("final",1200*9)    
    
}

if(clk("base")){
    
    holoData(1,"models/hunter/plates/plate2x2.mdl",vec(1,1,1)*1*ScaleFactor,Policebox:toWorld(vec()*ScaleFactor),Policebox:toWorld(ang()),PhxMetallic,Blue,Policebox)
    holoData(2,"models/hunter/plates/plate2x2.mdl",vec(1,1,1)*0.98*ScaleFactor,Policebox:toWorld(vec(0,0,1)*ScaleFactor),Policebox:toWorld(ang()),PhxMetallic,Blue,Policebox)
    #[4 Main Beams]#
    holoData(3,"models/props_phx/misc/iron_beam2.mdl",vec(1,1,1.3)*1*ScaleFactor,Policebox:toWorld(vec(35,40,62)*ScaleFactor),Policebox:toWorld(ang(90,0,0)),PhxMetallic,Blue,Policebox)
    holoData(4,"models/props_phx/misc/iron_beam2.mdl",vec(1,1,1.3)*1*ScaleFactor,Policebox:toWorld(vec(35,-40,62)*ScaleFactor),Policebox:toWorld(ang(90,0,0)),PhxMetallic,Blue,Policebox)
    holoData(5,"models/props_phx/misc/iron_beam2.mdl",vec(1,1,1.3)*1*ScaleFactor,Policebox:toWorld(vec(-45,40,62)*ScaleFactor),Policebox:toWorld(ang(90,0,0)),PhxMetallic,Blue,Policebox)
    holoData(6,"models/props_phx/misc/iron_beam2.mdl",vec(1,1,1.3)*1*ScaleFactor,Policebox:toWorld(vec(-45,-40,62)*ScaleFactor),Policebox:toWorld(ang(90,0,0)),PhxMetallic,Blue,Policebox)
}

if(clk("roof")){
    holoData(9,"models/hunter/plates/plate2x2.mdl",vec(0.95,0.95,3)*0.98*ScaleFactor,Policebox:toWorld(vec(0,0,116)*ScaleFactor),Policebox:toWorld(ang()),PhxMetallic,Blue,Policebox)
    holoData(10,"models/hunter/plates/plate2x2.mdl",vec(0.975,0.975,2)*0.98*ScaleFactor,Policebox:toWorld(vec(0,0,123)*ScaleFactor),Policebox:toWorld(ang()),PhxMetallic,Blue,Policebox)
    holoData(11,"models/hunter/plates/plate2x2.mdl",vec(0.95,0.95,3)*0.98*ScaleFactor,Policebox:toWorld(vec(0,0,128)*ScaleFactor),Policebox:toWorld(ang()),PhxMetallic,Blue,Policebox)
    holoData(12,"models/hunter/triangles/trapezium3x3x1.mdl",vec(0.95,0.95,0.25)*0.98*0.66*ScaleFactor,Policebox:toWorld(vec(0,0,136)*ScaleFactor),Policebox:toWorld(ang()),PhxMetallic,Blue,Policebox)
    holoData(13,"models/hunter/triangles/trapezium3x3x1.mdl",vec(0.22,0.22,0.05)*1*ScaleFactor,holoEntity(12):toWorld(vec(0,0,5)*ScaleFactor),Policebox:toWorld(ang()),PhxMetallic,Blue,Policebox)
    holoData(14,"models/props_trainstation/trashcan_indoor001b.mdl",vec(1,1,1)*0.25*ScaleFactor,holoEntity(12):toWorld(vec(0,0,13)*ScaleFactor),Policebox:toWorld(ang()),PhxMetallic,vec(0.9,0.9,1)*255,Policebox)
    holoData(15,"models/props_c17/pottery01a.mdl",vec(1,1,0.5)*0.9*ScaleFactor,holoEntity(12):toWorld(vec(0,0,21)*ScaleFactor),Policebox:toWorld(ang(0,0,180)),PhxMetallic,Blue,Policebox)
    holoData(16,"models/props_c17/pottery01a.mdl",vec(1,1,0.5)*1.2*ScaleFactor,holoEntity(12):toWorld(vec(0,0,12)*ScaleFactor),Policebox:toWorld(ang(0,0,180)),PhxMetallic,Blue,Policebox)
    holoData(17,"cube",vec(1,1,0.5)*1*ScaleFactor,holoEntity(12):toWorld(vec(0,0,6)*ScaleFactor),Policebox:toWorld(ang(0,0,180)),PhxMetallic,Blue,Policebox)
    holoDisableShading(14,1)
}

if(clk("door_left")){
    #models/hunter/plates/plate1x3.mdl
    holoData(20,"cube",vec(1,1,1)*0*ScaleFactor,Policebox:toWorld(vec(40,-34,0)*ScaleFactor),Policebox:toWorld(ang()),PhxMetallic,Blue,Policebox)
    holoData(21,"models/hunter/plates/plate1x3.mdl",vec(1,0.825,1)*0.9*ScaleFactor,holoEntity(20):toWorld(vec(0,16.5,64)*ScaleFactor),holoEntity(20):toWorld(ang(0,90,90)),PhxMetallic,Blue*0.85,holoEntity(20))
    
    holoData(22,"models/sprops/rectangles/size_1/rect_3x120x3.mdl",vec(1,1,2.5)*1*ScaleFactor,holoEntity(20):toWorld(vec(0,31,62)*ScaleFactor),holoEntity(20):toWorld(ang(90,0,0)),PhxMetallic,Blue,holoEntity(20))
    holoData(23,"models/sprops/rectangles/size_1/rect_3x120x3.mdl",vec(1,1,2.5)*1*ScaleFactor,holoEntity(20):toWorld(vec(0,0,62)*ScaleFactor),holoEntity(20):toWorld(ang(90,0,0)),PhxMetallic,Blue,holoEntity(20))
    
    holoData(24,"models/sprops/rectangles/size_1_5/rect_6x30x3.mdl",vec(1,1.15,1.75)*1*ScaleFactor,holoEntity(20):toWorld(vec(0,17,5)*ScaleFactor),holoEntity(20):toWorld(ang(0,90,0)),PhxMetallic,Blue,holoEntity(20))
    holoData(25,"models/sprops/rectangles/size_1_5/rect_6x30x3.mdl",vec(1,1.15,1.75)*1*ScaleFactor,holoEntity(20):toWorld(vec(0,17,30)*ScaleFactor),holoEntity(20):toWorld(ang(0,90,0)),PhxMetallic,Blue,holoEntity(20))
    holoData(26,"models/sprops/rectangles/size_1_5/rect_6x30x3.mdl",vec(1,1.15,1.75)*1*ScaleFactor,holoEntity(20):toWorld(vec(0,17,55)*ScaleFactor),holoEntity(20):toWorld(ang(0,90,0)),PhxMetallic,Blue,holoEntity(20))
    holoData(27,"models/sprops/rectangles/size_1_5/rect_6x30x3.mdl",vec(1,1.15,1.75)*1*ScaleFactor,holoEntity(20):toWorld(vec(0,17,80)*ScaleFactor),holoEntity(20):toWorld(ang(0,90,0)),PhxMetallic,Blue,holoEntity(20))
    holoData(28,"models/sprops/rectangles/size_1_5/rect_6x30x3.mdl",vec(1,1.15,1.75)*1*ScaleFactor,holoEntity(20):toWorld(vec(0,17,110)*ScaleFactor),holoEntity(20):toWorld(ang(0,90,0)),PhxMetallic,Blue,holoEntity(20))
    holoData(29,"models/props_coalmines/window2.mdl",vec(1,1,1)*0.5*ScaleFactor,holoEntity(20):toWorld(vec(2,15.5,95)*ScaleFactor),holoEntity(20):toWorld(ang(0,0,0)),"",vec(1,1,1)*255,holoEntity(20))
    
}
if(clk("door_right")){
    #models/hunter/plates/plate1x3.mdl
    holoData(30,"cube",vec(1,1,1)*0*ScaleFactor,Policebox:toWorld(vec(40,34,0)*ScaleFactor),Policebox:toWorld(ang()),PhxMetallic,Blue,Policebox)
    holoData(31,"models/hunter/plates/plate1x3.mdl",vec(1,0.825,1)*0.9*ScaleFactor,holoEntity(30):toWorld(vec(0,-16.5,64)*ScaleFactor),holoEntity(30):toWorld(ang(0,90,90)),PhxMetallic,Blue*0.75,holoEntity(30))
    
    holoData(32,"models/sprops/rectangles/size_1/rect_3x120x3.mdl",vec(1,1,2.5)*1*ScaleFactor,holoEntity(30):toWorld(vec(0,-31,62)*ScaleFactor),holoEntity(30):toWorld(ang(90,0,0)),PhxMetallic,Blue,holoEntity(30))
    holoData(33,"models/sprops/rectangles/size_1/rect_3x120x3.mdl",vec(1,1,2.5)*1*ScaleFactor,holoEntity(30):toWorld(vec(0,0,62)*ScaleFactor),holoEntity(30):toWorld(ang(90,0,0)),PhxMetallic,Blue,holoEntity(30))
    
    holoData(34,"models/sprops/rectangles/size_1_5/rect_6x30x3.mdl",vec(1,1.15,1.75)*1*ScaleFactor,holoEntity(30):toWorld(vec(0,-17,5)*ScaleFactor),holoEntity(30):toWorld(ang(0,90,0)),PhxMetallic,Blue,holoEntity(30))
    holoData(35,"models/sprops/rectangles/size_1_5/rect_6x30x3.mdl",vec(1,1.15,1.75)*1*ScaleFactor,holoEntity(30):toWorld(vec(0,-17,30)*ScaleFactor),holoEntity(30):toWorld(ang(0,90,0)),PhxMetallic,Blue,holoEntity(30))
    holoData(36,"models/sprops/rectangles/size_1_5/rect_6x30x3.mdl",vec(1,1.15,1.75)*1*ScaleFactor,holoEntity(30):toWorld(vec(0,-17,55)*ScaleFactor),holoEntity(30):toWorld(ang(0,90,0)),PhxMetallic,Blue,holoEntity(30))
    holoData(37,"models/sprops/rectangles/size_1_5/rect_6x30x3.mdl",vec(1,1.15,1.75)*1*ScaleFactor,holoEntity(30):toWorld(vec(0,-17,80)*ScaleFactor),holoEntity(30):toWorld(ang(0,90,0)),PhxMetallic,Blue,holoEntity(30))
    holoData(38,"models/sprops/rectangles/size_1_5/rect_6x30x3.mdl",vec(1,1.15,1.75)*1*ScaleFactor,holoEntity(30):toWorld(vec(0,-17,110)*ScaleFactor),holoEntity(30):toWorld(ang(0,90,0)),PhxMetallic,Blue,holoEntity(30))
    holoData(39,"models/props_coalmines/window2.mdl",vec(1,1,1)*0.5*ScaleFactor,holoEntity(30):toWorld(vec(2,-15.5,95)*ScaleFactor),holoEntity(30):toWorld(ang(0,0,0)),"",vec(1,1,1)*255,holoEntity(30))
}

if(clk("door_details")){

holoData(40,"models/props/cs_office/phone_p2.mdl",vec(1,1,1)*0.5*ScaleFactor,holoEntity(30):toWorld(vec(3,-30,50)*ScaleFactor),holoEntity(30):toWorld(ang(-90,180,0)),Shiny,vec(1,1,1)*255,holoEntity(30))    
}
if(clk("wall_left")){
    #models/hunter/plates/plate1x3.mdl
    holoData(50,"models/hunter/plates/plate2x3.mdl",vec(1,0.825,1)*0.9*ScaleFactor,Policebox:toWorld(vec(0,40,64)*ScaleFactor),Policebox:toWorld(ang(0,0,90)),PhxMetallic,Blue*0.75,Policebox)
    holoData(51,"models/sprops/rectangles/size_1/rect_3x120x3.mdl",vec(1,1,2.5)*1*ScaleFactor,holoEntity(50):toWorld(vec(0,-3,0)*ScaleFactor),holoEntity(50):toWorld(ang(0,90,0)),PhxMetallic,Blue,holoEntity(50))
    holoData(52,"models/sprops/rectangles/size_1/rect_3x120x3.mdl",vec(1,1,2.5)*1*ScaleFactor,holoEntity(50):toWorld(vec(33,-3,0)*ScaleFactor),holoEntity(50):toWorld(ang(0,90,0)),PhxMetallic,Blue,holoEntity(50))
    holoData(53,"models/sprops/rectangles/size_1/rect_3x120x3.mdl",vec(1,1,2.5)*1*ScaleFactor,holoEntity(50):toWorld(vec(-33,-3,0)*ScaleFactor),holoEntity(50):toWorld(ang(0,90,0)),PhxMetallic,Blue,holoEntity(50))
    holoData(54,"models/sprops/rectangles/size_1/rect_3x60x3.mdl",vec(1,1,2)*1*ScaleFactor,holoEntity(50):toWorld(vec(0,-60,0)*ScaleFactor),holoEntity(50):toWorld(ang(0,0,0)),PhxMetallic,Blue,holoEntity(50))
    holoData(55,"models/sprops/rectangles/size_1/rect_3x60x3.mdl",vec(1,1,2)*1*ScaleFactor,holoEntity(50):toWorld(vec(0,47.5,0)*ScaleFactor),holoEntity(50):toWorld(ang(0,0,0)),PhxMetallic,Blue,holoEntity(50))
    holoData(56,"models/sprops/rectangles/size_1/rect_3x60x3.mdl",vec(1,1,2)*1*ScaleFactor,holoEntity(50):toWorld(vec(0,-34.5,0)*ScaleFactor),holoEntity(50):toWorld(ang(0,0,0)),PhxMetallic,Blue,holoEntity(50))
    holoData(57,"models/sprops/rectangles/size_1/rect_3x60x3.mdl",vec(1,1,2)*1*ScaleFactor,holoEntity(50):toWorld(vec(0,-9.5,0)*ScaleFactor),holoEntity(50):toWorld(ang(0,0,0)),PhxMetallic,Blue,holoEntity(50))
    holoData(58,"models/sprops/rectangles/size_1/rect_3x60x3.mdl",vec(1,1,2)*1*ScaleFactor,holoEntity(50):toWorld(vec(0,16.5,0)*ScaleFactor),holoEntity(50):toWorld(ang(0,0,0)),PhxMetallic,Blue,holoEntity(50))
}

if(clk("wall_right")){
    #models/hunter/plates/plate1x3.mdl
    holoData(60,"models/hunter/plates/plate2x3.mdl",vec(1,0.825,1)*0.9*ScaleFactor,Policebox:toWorld(vec(0,-40,64)*ScaleFactor),Policebox:toWorld(ang(0,180,90)),PhxMetallic,Blue*0.75,Policebox)
    holoData(61,"models/sprops/rectangles/size_1/rect_3x120x3.mdl",vec(1,1,2.5)*1*ScaleFactor,holoEntity(60):toWorld(vec(0,-3,0)*ScaleFactor),holoEntity(60):toWorld(ang(0,90,0)),PhxMetallic,Blue,holoEntity(60))
    holoData(62,"models/sprops/rectangles/size_1/rect_3x120x3.mdl",vec(1,1,2.5)*1*ScaleFactor,holoEntity(60):toWorld(vec(33,-3,0)*ScaleFactor),holoEntity(60):toWorld(ang(0,90,0)),PhxMetallic,Blue,holoEntity(60))
    holoData(63,"models/sprops/rectangles/size_1/rect_3x120x3.mdl",vec(1,1,2.5)*1*ScaleFactor,holoEntity(60):toWorld(vec(-33,-3,0)*ScaleFactor),holoEntity(60):toWorld(ang(0,90,0)),PhxMetallic,Blue,holoEntity(60))
    holoData(64,"models/sprops/rectangles/size_1/rect_3x60x3.mdl",vec(1,2,2)*1*ScaleFactor,holoEntity(60):toWorld(vec(0,-60,0)*ScaleFactor),holoEntity(60):toWorld(ang(0,0,0)),PhxMetallic,Blue,holoEntity(60))
    holoData(65,"models/sprops/rectangles/size_1/rect_3x60x3.mdl",vec(1,2,2)*1*ScaleFactor,holoEntity(60):toWorld(vec(0,47.5,0)*ScaleFactor),holoEntity(60):toWorld(ang(0,0,0)),PhxMetallic,Blue,holoEntity(60))
    holoData(66,"models/sprops/rectangles/size_1/rect_3x60x3.mdl",vec(1,2,2)*1*ScaleFactor,holoEntity(60):toWorld(vec(0,-34.5,0)*ScaleFactor),holoEntity(60):toWorld(ang(0,0,0)),PhxMetallic,Blue,holoEntity(60))
    holoData(67,"models/sprops/rectangles/size_1/rect_3x60x3.mdl",vec(1,2,2)*1*ScaleFactor,holoEntity(60):toWorld(vec(0,-9.5,0)*ScaleFactor),holoEntity(60):toWorld(ang(0,0,0)),PhxMetallic,Blue,holoEntity(60))
    holoData(68,"models/sprops/rectangles/size_1/rect_3x60x3.mdl",vec(1,2,2)*1*ScaleFactor,holoEntity(60):toWorld(vec(0,16.5,0)*ScaleFactor),holoEntity(60):toWorld(ang(0,0,0)),PhxMetallic,Blue,holoEntity(60))
}

if(clk("wall_back")){
    #models/hunter/plates/plate1x3.mdl
    holoData(70,"models/hunter/plates/plate2x3.mdl",vec(1,0.825,1)*0.9*ScaleFactor,Policebox:toWorld(vec(-40,0,64)*ScaleFactor),Policebox:toWorld(ang(0,90,90)),PhxMetallic,Blue*0.75,Policebox)
    holoData(71,"models/sprops/rectangles/size_1/rect_3x120x3.mdl",vec(1,1,2.5)*1*ScaleFactor,holoEntity(70):toWorld(vec(0,-3,0)*ScaleFactor),holoEntity(70):toWorld(ang(0,90,0)),PhxMetallic,Blue,holoEntity(70))
    holoData(72,"models/sprops/rectangles/size_1/rect_3x120x3.mdl",vec(1,1,2.5)*1*ScaleFactor,holoEntity(70):toWorld(vec(33,-3,0)*ScaleFactor),holoEntity(70):toWorld(ang(0,90,0)),PhxMetallic,Blue,holoEntity(70))
    holoData(73,"models/sprops/rectangles/size_1/rect_3x120x3.mdl",vec(1,1,2.5)*1*ScaleFactor,holoEntity(70):toWorld(vec(-33,-3,0)*ScaleFactor),holoEntity(70):toWorld(ang(0,90,0)),PhxMetallic,Blue,holoEntity(70))
    holoData(74,"models/sprops/rectangles/size_1/rect_3x60x3.mdl",vec(1,2,2)*1*ScaleFactor,holoEntity(70):toWorld(vec(0,-60,0)*ScaleFactor),holoEntity(70):toWorld(ang(0,0,0)),PhxMetallic,Blue,holoEntity(70))
    holoData(75,"models/sprops/rectangles/size_1/rect_3x60x3.mdl",vec(1,2,2)*1*ScaleFactor,holoEntity(70):toWorld(vec(0,47.5,0)*ScaleFactor),holoEntity(70):toWorld(ang(0,0,0)),PhxMetallic,Blue,holoEntity(70))
    holoData(76,"models/sprops/rectangles/size_1/rect_3x60x3.mdl",vec(1,2,2)*1*ScaleFactor,holoEntity(70):toWorld(vec(0,-34.5,0)*ScaleFactor),holoEntity(70):toWorld(ang(0,0,0)),PhxMetallic,Blue,holoEntity(70))
    holoData(77,"models/sprops/rectangles/size_1/rect_3x60x3.mdl",vec(1,2,2)*1*ScaleFactor,holoEntity(70):toWorld(vec(0,-9.5,0)*ScaleFactor),holoEntity(70):toWorld(ang(0,0,0)),PhxMetallic,Blue,holoEntity(70))
    holoData(78,"models/sprops/rectangles/size_1/rect_3x60x3.mdl",vec(1,2,2)*1*ScaleFactor,holoEntity(70):toWorld(vec(0,16.5,0)*ScaleFactor),holoEntity(70):toWorld(ang(0,0,0)),PhxMetallic,Blue,holoEntity(70))


}

if(clk("wall_details")){
    holoData(59,"models/props_coalmines/window2.mdl",vec(1,1,1)*0.5*ScaleFactor,holoEntity(50):toWorld(vec(16,32,-2)*ScaleFactor),holoEntity(50):toWorld(ang(90,90,0)),"",vec(1,1,1)*255,holoEntity(50))
    holoData(80,"models/props_coalmines/window2.mdl",vec(1,1,1)*0.5*ScaleFactor,holoEntity(50):toWorld(vec(-16,32,-2)*ScaleFactor),holoEntity(50):toWorld(ang(90,90,0)),"",vec(1,1,1)*255,holoEntity(50))

    holoData(69,"models/props_coalmines/window2.mdl",vec(1,1,1)*0.5*ScaleFactor,holoEntity(60):toWorld(vec(16,32,-2)*ScaleFactor),holoEntity(60):toWorld(ang(90,90,0)),"",vec(1,1,1)*255,holoEntity(60))
    holoData(81,"models/props_coalmines/window2.mdl",vec(1,1,1)*0.5*ScaleFactor,holoEntity(60):toWorld(vec(-16,32,-2)*ScaleFactor),holoEntity(60):toWorld(ang(90,90,0)),"",vec(1,1,1)*255,holoEntity(60))

    holoData(79,"models/props_coalmines/window2.mdl",vec(1,1,1)*0.5*ScaleFactor,holoEntity(70):toWorld(vec(16,32,-2)*ScaleFactor),holoEntity(70):toWorld(ang(90,90,0)),"",vec(1,1,1)*255,holoEntity(70))
    holoData(82,"models/props_coalmines/window2.mdl",vec(1,1,1)*0.5*ScaleFactor,holoEntity(70):toWorld(vec(-16,32,-2)*ScaleFactor),holoEntity(70):toWorld(ang(90,90,0)),"",vec(1,1,1)*255,holoEntity(70))
    
    holoData(83,"models/hunter/plates/plate2x2.mdl",vec(0.8,1,3)*0.98*ScaleFactor,Policebox:toWorld(vec(0,0,123)*ScaleFactor),Policebox:toWorld(ang()),Matte,vec(1,1,1)*20,Policebox)
    holoData(84,"models/hunter/plates/plate2x2.mdl",vec(1,0.8,3)*0.98*ScaleFactor,Policebox:toWorld(vec(0,0,123)*ScaleFactor),Policebox:toWorld(ang()),Matte,vec(1,1,1)*20,Policebox)
    
    holoData(85,"models/hunter/plates/plate2x2.mdl",vec(1,1,7.5)*1*ScaleFactor,Policebox:toWorld(vec()*ScaleFactor),Policebox:toWorld(ang()),PhxMetallic,Blue,Policebox)
       
}

if(clk("signage")){
    #[
    holoTextCreate(90)
    holoText(90,"This is a test")    
    holoTextPos(90,Policebox:toWorld(vec(0,0,10)))  
    ]#  
}

if(Open && Doors < 80){
    Doors+=5
}elseif(!Open && Doors > 0){  
    Doors-=5
}
#Doors= sin(curtime()*250)*45+45

holoAng(20,Policebox:toWorld(ang(0,Doors,0)))
holoAng(30,Policebox:toWorld(ang(0,-Doors,0)))

if(clk("final")){
    for(I=0,100){
        holoVisible(I,players(),1) 
    }    
}
#if(changed(owner():lastSaidWhen()) && owner():lastSaid() == "!Open"){
    
  #  Open = !Open   
#}
#if(changed(owner():lastSaidWhen()) && owner():lastSaid() == "!Camo1"){}

if(changed(owner():lastSaid())&& owner():lastSaid() == "//resetE2"){reset()}

