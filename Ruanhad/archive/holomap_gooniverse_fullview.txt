@name holoMap Gooniverse FullView
@inputs Planet
@outputs 
@persist J PlanetRadius TerrainOffset Scalefator [BasePos Position Scale Color Terrainpos TerrainScale TerrainColor ]:vector
interval(300)
BasePos = entity():pos() + vec(0,0,150)
Scalefactor = 150

if(first()){
    J = 12
    
    for(I=1,J){ # players
holoCreate(I)
holoModel(I,"cube")
holoScale(I,owner():boxSize()/Scalefactor/12) 
holoMaterial(I,"Phoenix_storms/mat/mat_phx_metallic")
holoParent(I,entity())
}
for(I=J+1,2){
holoCreate(I)
holoModel(I,"hq_sphere")
holoAng(I,ang())
holoScale(I,vec(1,1,1))
holoMaterial(I,"Phoenix_storms/mat/mat_phx_metallic")
holoParent(I,entity())
}
}

if(Planet == 1){
#Hiigara
Position = vec(-9675,-6077,-8164) # -9728,-1312,-8168
PlanetRadius = 6077 - 1312
Color = vec(100,255,200)
TerrainOffset = 2
TerrainColor = vec(1,1,1)*100
}
elseif(Planet == 2){

#kobol
Position = vec(9901,9234,4608) # 13725,9234,4608
PlanetRadius = 13725 - 9901
Color = vec(1,1,1)*255
TerrainOffset = 2
TerrainColor = vec(1,1,1)*100
}
elseif(Planet == 3){

#Coruscant
Position = vec(40,15,4612) # -1111,3,4608
PlanetRadius = 1111 - 40
Color = vec(1,1,1)*100
TerrainOffset = 1
TerrainColor = vec(1,1,1)*100
}
elseif(Planet == 4){
#Endgame
Position = vec(1648,7115,-10240) # 
PlanetRadius = (Position - vec(2575,4929,-10240)):length()
Color = vec(255,100,100)
TerrainOffset = 1.5
TerrainColor = vec(1,1,1)*100
}
elseif(Planet == 5){
#Demeter
Position = vec(1648,7115,-10240) # 
PlanetRadius = (Position - vec(2575,4929,-10240)):length()
Color = vec(100,255,100)
TerrainOffset = 2
TerrainColor = vec(1,1,1)*100
}
else{
#Cerberus
Position = vec(7995,-10008,-2048) # 
PlanetRadius = (Position - vec(7482,-5564,-2048)):length()
Color = vec(200,200,100)
TerrainOffset = 2
TerrainColor = vec(1,1,1)*100
}
#if(clk("UpdatePlayers") | changed(players():count())){
Targets = players()
#}



for(I=1,J){
if(Targets[I,entity]:isAlive() != 1){
Targets:removeEntity(I)
holoScale(I,vec())
}else{holoScale(I,vec(30,30,70)/Scalefactor/3)} 

if(Targets[I,entity]:vehicle()){
holoColor(I,vec(255,100,100))}
elseif(Targets[I,entity]:inNoclip()){
holoColor(I,vec(100,255,100))}
else{holoColor(I,vec(100,100,255))}   
holoPos(I,BasePos + Targets[I,entity]:pos()/Scalefactor)
holoAng(I,Targets[I,entity]:angles())    


}  


Position = entity():pos() + vec(0,0,100)

holoPos(J+1,Position)
holoScale(J+1,vec(1,1,1)*(PlanetRadius/Scalefactor)/12*-2)
holoColor(J+1,Color)
holoPos(J+2,holoEntity(1):pos() - vec(0,0,TerrainOffset))
holoScale(J+2,vec(1,1,0.01)*(PlanetRadius/Scalefactor)/12*1.95)
holoColor(J+2,TerrainColor)
