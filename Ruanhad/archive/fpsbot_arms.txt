@name FPSbot arms
@inputs [Screen Controls]:wirelink [Gun Head Torso]:entity Gunammo Missileammo
@outputs ROPos:vector Time Disttravelled FireGun FireMissiles MissCount
@persist [Gunpos]:vector R:array GunActive MissileActive Choice PodUP PodPitch Angle Pos:vector2
@persist CanFireMissiles Fireable2
runOnTick(1)
M1 = Controls:number("Mouse1")
Mwheelup = Controls:number("NextWeapon")
Mwheeldown = Controls:number("PrevWeapon")
Active = Controls:number("Active")
Seat = Controls:entity("Entity")
    Pos = egpScrSize(Seat:driver())/2
if(first()){
Fireable2 = 1
MissCount = 30
Screen:egpClear()
Screen:egpLine(4,vec2(),vec2())
Screen:egpLine(5,vec2(),vec2())

Screen:egpText(8,"Gun1",vec2())
Screen:egpText(9,"Gun1",vec2())
Screen:egpText(10,"Gun1",vec2())

Screen:egpBox(11,vec2(),vec2(200,100))
Screen:egpBox(12,vec2(),vec2(200,100))
Screen:egpBox(13,vec2(),vec2(200,100))

Toggle = 0
#shoulders
timer("CanFireMissiles",10)

for(I=1,10){
holoCreate(I)
holoMaterial(I,"Phoenix_storms/mat/mat_phx_metallic")
holoPos(I,Torso:pos())
holoColor(I,vec(50,50,50))    
#holoParent(I,Torso)
GunActive = 0
}  
#shoulders
holoAng(1,Torso:toWorld(ang())) #Left 
holoPos(1,Torso:toWorld(vec(0,20,10)))    
holoParent(1,Torso)

holoAng(2,Torso:toWorld(ang()))  #Right
holoPos(2,Torso:toWorld(vec(0,-20,10)))  
holoParent(2,Torso)

 # Left Hand Position
holoAng(3,Torso:toWorld(ang()))
holoPos(3,Gun:toWorld(vec(45,0,5)))
holoParent(3,Gun)

#Missile Pods
holoPos(7,holoEntity(6):toWorld(vec(0,-15,2)))
holoPos(8,holoEntity(6):toWorld(vec(0,15,2)))
holoModel(7,"models/props_lab/kennel_physics.mdl")
holoModel(8,"models/props_lab/kennel_physics.mdl")
holoScale(7,vec(1,1,0.5)*0.5)
holoScale(8,vec(1,1,0.5)*0.5)
#holoScale(7,vec(1,1,0.5)*2)
#holoScale(8,vec(1,1,0.5)*2)

holoPos(9,Torso:toWorld(vec(-25,0,15)))
holoAng(9,Torso:angles())
holoScale(9,vec(0.5,0.5,5))

holoPos(10,Torso:toWorld(vec(-20,0,18)))
holoAng(10,Torso:toWorld(ang(60,0,0)))
holoScale(10,vec(2,0.5,2))


holoParent(4,Torso)
holoParent(6,Torso)
holoParent(7,6)
holoParent(8,6)
holoParent(9,Torso)
holoParent(10,Torso)


R = entity():getConstraints()
rangerFilter(R)
rangerPersist(1)

timer("slowupdate",50)
}

if(Seat:driver():isTyping()){Chat = 1}else{Chat = 0}

if(round(Choice) == 1){GunActive = 1}else{GunActive = 0}
if(round(Choice) == -1){MissileActive = 1}else{MissileActive = 0}
# Left Arm position/angles
if(clk("slowupdate")){  
Hpos4 = (holoEntity(1):pos() + holoEntity(3):pos())/2
holoPos(4,Hpos4)
HDist4 = (holoEntity(1):pos()-holoEntity(3):pos()):length()
holoScale(4,vec(HDist4/12,1,1))
holoAng(4,(holoEntity(1):pos()-holoEntity(3):pos()):toAngle())

#right arm
Hpos5 = (holoEntity(2):pos() + Gun:pos())/2
holoPos(5,Hpos5)
HDist5 = (holoEntity(2):pos()-Gun:pos()):length()
holoScale(5,vec(HDist5/12,1,1))
holoAng(5,(holoEntity(2):pos()-Gun:pos()):toAngle())


Screen:egpLine(4,Pos+vec2(30,0),Pos+vec2(500,0))
Screen:egpLine(5,Pos+vec2(-30,0),Pos+vec2(-500,0))

Screen:egpPos(8,Pos + vec2(-650,100))
Screen:egpSetText(8,"Assault Rifle" + " : " + Gunammo:toString())
Screen:egpPos(9,Pos + vec2(-650,-100))
Screen:egpSetText(9,"Missile Pods" + " : " + Missileammo:toString())
Screen:egpPos(10,Pos + vec2(-650,0))
Screen:egpSetText(10,"Head Look")

Screen:egpPos(11,Pos + vec2(-600,100))
Screen:egpPos(12,Pos + vec2(-600,-100))
Screen:egpPos(13,Pos + vec2(-600,0))

Screen:egpAlpha(11,50)
Screen:egpAlpha(12,50)
Screen:egpAlpha(13,50)
timer("slowupdate",50)
}
Eye = Seat:driver():eye()
EyeMul = Eye*500000

ROOutput = rangerOffset(500000,Gun:toWorld(vec(0,0,20)),Eye) 
ROPos = ROOutput:position() # Target position

ROPosOffset = ROPos - Gun:pos()
ROPOCor = ROPos:setZ(Gun:pos():z()) # imaginary target location for use in calculating the "crow-flies" distance
ROPOZDiff = ROOutput:position():z() - Gun:pos():z() # difference in Z axis between gun position and aimed target position
#ROPosOffset = Gun:toLocal(ROPos)
Crowflies = (ROPOCor - Gun:pos()):length()
Velocity = 600 # muzzle velocity
Grav = 9.8 #gravity
#angle = atan(v^2 +- sqrt(v^4 - g(gx^2 + 2yv^2)))/(gx))
Angle = atan( (Velocity^2 - sqrt(Velocity^4 - Grav*(Grav*(Crowflies^2) + 2*ROPOZDiff*Velocity^2 )  ))/(Grav*Crowflies)  )

#DesiredDist = (ROOutput:position() - Gun:pos()):length()
# D = ((VcosAng)/g)*( VsinAng + sqrt( (VsinAng)^2 + 2gy ) )
# T = D/(VcosAng) = (VsinAng + sqrt((VsinAng)^2) + 2gy)/g

Disttravelled = ((Velocity*cos(Angle))/Grav) * ( Velocity*sin(Angle) + sqrt( (Velocity*sin(Angle))^2 + 2*Grav*ROPOZDiff ) )
DesiredDist = Disttravelled
#Time = 

Time = (DesiredDist)/Velocity*cos(Velocity/DesiredDist)

#GAng = ang(-Seat:elevation(EyeMul),-Seat:bearing(EyeMul)+Seat:angles():yaw(),0)
if(GunActive){
GAng = ang(-Angle,-Seat:bearing(EyeMul)+Seat:angles():yaw(),0)
Gunpos = Gun:pos() - Torso:toWorld(vec(25,-30,-15))
}
else{
if(Chat){
    GAng = Torso:toWorld(ang(40,80,2))
    Gunpos = Gun:pos() - Torso:toWorld(vec(30,-20,-15))
    
    #Insert Controls for Left Arm movement
    
    }else{
GAng = Torso:toWorld(ang(20,80,5))
Gunpos = Gun:pos() - Torso:toWorld(vec(30,-20,-10))
}
}

GV = Gun:toLocal(rotationVector(quat(GAng)/quat(Gun))+Gun:pos())
Gun:applyTorque((150*GV - 15*Gun:angVelVector())*Gun:mass()/10)  

Gvec = (($Gunpos*150)+(Gunpos*20))*Gun:mass()/3*-1
Gun:applyForce(Gvec)

#Missile Pods
if(MissileActive & PodUP < 40){PodUP += 1}
if(MissileActive & PodUP > 35 & PodPitch < 0){PodPitch+=3}

if(!MissileActive & PodPitch > -90){PodPitch -= 2}
if(!MissileActive & PodPitch <= -85 & PodUP > 0){PodUP -= 1}

if(MissileActive){PodYaw = (Head:angles():yaw() - Torso:angles():yaw())}else{PodYaw = 0}
if(MissileActive){PodPitchAim = (Head:angles():pitch() - Torso:angles():pitch())}else{PodPitchAim = 0}
holoPos(6,Torso:toWorld(vec(-25,0,PodUP)))
holoAng(6,Torso:toWorld(ang(PodPitch + PodPitchAim,PodYaw,0)))#+ang(PodPitch,PodYaw,0))

#Screen Controls





if(!Active){Choice = 0}
if(Mwheelup){Choice = Choice+0.75}
if(Mwheeldown){Choice = Choice-0.75}
if(Choice > 1){Choice = 1}
if(Choice < -1){Choice = -1}
#Choice = Mwheelup-Mwheeldown
if(round(Choice) == 1){Screen:egpAlpha(11,100)}else{Screen:egpAlpha(11,50)}#Assault Rifle
if(round(Choice) == -1){Screen:egpAlpha(12,100)}else{Screen:egpAlpha(12,50)}#Missiles
if(round(Choice) == 0){Screen:egpAlpha(13,100)}else{Screen:egpAlpha(13,50)}


#Firing
if(GunActive){FireGun = 1}else{FireGun = 0}
if(MissileActive){FireMissiles = 1}else{FireMissiles = 0}

if(clk("Fireable1")){
#    MissCount = 30
Fireable2 = 1    
}
if(clk("ReloadMissiles")){
MissCount = 15    
}

if(M1 & FireMissiles & Fireable2 == 1 & MissCount >=1){
    #Basic Rocket Launcher
    #Port = randint(4,7)
  #pewFire(holoEntity(7):toWorld(vec(0,0,0)),holoEntity(7):forward(),"Basic Rocket Launcher")
  #pewFire(holoEntity(8):toWorld(vec(0,0,0)),holoEntity(8):forward(),"Basic Rocket Launcher")
MissCount -= 1
if(MissCount <= 0){
timer("ReloadMissiles",5000)    
}
    Fireable2 = 0
    timer("Fireable1",500)
    }
