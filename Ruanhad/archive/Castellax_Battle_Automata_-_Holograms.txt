@name Castellax Battle Automata - Holograms
@inputs [Torso Hip FL FR AL AR Turret]:entity
@outputs
@persist 
interval(50)
PhxMetallic = "Phoenix_storms/mat/mat_phx_metallic"
Grey = vec(1,1,1)*50
Red = vec(1,0,0)*50
Green = vec(0,1,0)*50
Blue = vec(0,0,1)*50

ThighLength = 4*12
ShinLength = 4*12
function holoData(Index,Model:string,Scale:vector,Pos:vector,Ang:angle,Material:string,Color:vector,Parent:entity){
    holoCreate(Index)
    holoModel(Index,Model)
    holoScale(Index,Scale)
    holoPos(Index,Pos)
    holoAng(Index,Ang)
    holoMaterial(Index,Material)
    holoColor(Index,Color)
    holoParent(Index,Parent)
    
}

function makeLeg(StartIndex,Hip:entity, Foot:entity, TL,SL){
I=StartIndex
while(I < StartIndex + 6){
holoCreate(I)
I++   
}
holoData(StartIndex,"cube",vec(1,1,1)*0.1,vec(),ang(),PhxMetallic,Grey,Hip) # Hip Joint
holoData(StartIndex+1,"cube",vec(1,1,1)*0.1,vec(),ang(),PhxMetallic,Grey,Hip) # True Hypotenuse
holoData(StartIndex+2,"cube",vec(1,1,1)*0.1,vec(),ang(),PhxMetallic,Grey,holoEntity(StartIndex+1)) # Fake Hypotenuse
holoData(StartIndex+3,"cube",vec(1,1,1)*1,holoEntity(StartIndex):toWorld(vec(TL,0,0)),holoEntity(StartIndex):toWorld(ang(0,0,0)),PhxMetallic,Grey,holoEntity(StartIndex)) # Knee joint    
holoData(StartIndex+4,"cube",vec(TL/12,2,2.5),holoEntity(StartIndex):toWorld(vec(TL/2,0,0)),holoEntity(StartIndex):angles(),PhxMetallic,Grey,holoEntity(StartIndex))
holoData(StartIndex+5,"cube",vec(SL/12,3,3),holoEntity(StartIndex+3):toWorld(vec(-SL/2,0,0)),holoEntity(StartIndex+3):angles(),PhxMetallic,Grey,holoEntity(StartIndex+3))

}
function operateLeg(StartIndex,Hippos:vector,Foot:entity,TL,SL,BipedToggle){
Avrgpoint = (Hippos + Foot:pos())/2
Hypelength = clamp((Hippos - Foot:pos()):length(),0,TL+SL)
HypeAngle = (Hippos - Foot:pos()):toAngle() + ang(90,0,0)

Out = Hip:angles():yaw()
Out2 = HypeAngle:yaw()
if((Out - Out2) > 180){Out1b = -(360 - (Out))}else{Out1b = Out}
if((Out2 - Out) > 180){Out2b = -(360 - (Out2))}else{Out2b = Out2}
Out3 = Out2b - Out1b

#LCorAngle = LHypeAngle - ang(0,Out3,0)
holoScale(StartIndex,vec(1,1,1)*0.1)

holoPos(StartIndex+1,Avrgpoint)
holoScale(StartIndex+1,vec(0.1,0.1,Hypelength/12)*0)
holoAng(StartIndex+1,HypeAngle)

holoPos(StartIndex+2,Avrgpoint)
holoScale(StartIndex+2,vec(0.5,1,Hypelength/15)*0)
holoAng(StartIndex+2,holoEntity(StartIndex+1):toWorld(ang(0,-Out3*BipedToggle+(!BipedToggle*180),0)))

holoPos(StartIndex,holoEntity(StartIndex+2):toWorld(vec(0,0,Hypelength/2)))

BodyAng = acos((TL^2 + Hypelength^2 - SL^2)/(2*TL*Hypelength)) - 90
holoAng(StartIndex,holoEntity(StartIndex+2):toWorld(ang(-BodyAng,0,0)))
KneeAng = acos((TL^2 + SL^2 - Hypelength^2)/(2*TL*SL))
holoAng(StartIndex+3,holoEntity(StartIndex):toWorld(ang(-KneeAng,0,0)))
}

if(first()){
    makeLeg(0,Hip,FL,ThighLength,ShinLength)    
    makeLeg(6,Hip,FR,ThighLength,ShinLength)    
}
operateLeg(0,Hip:toWorld(vec(0,35,10)),FL,ThighLength,ShinLength,1)
operateLeg(6,Hip:toWorld(vec(0,-35,10)),FR,ThighLength,ShinLength,1)


