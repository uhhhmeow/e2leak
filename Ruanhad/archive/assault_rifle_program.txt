@name Assault Rifle Program
@inputs Controls:wirelink [Gun Shoulders]:entity
@outputs Angle
@persist Gpos:vector Parts:array Fireable1
runOnTick(1)

if(first()){
holoCreate(1)
holoScale(1,vec(1,1,1)*3)
holoModel(1,"models/weapons/w_rif_famas.mdl")
holoPos(1,Gun:pos())
holoAng(1,Gun:angles())
holoParent(1,Gun)
Parts = entity():getConstraints()    
    
    rangerFilter(Parts)
    rangerHitEntities(1)
    rangerPersist(1)
    
    Fireable1 = 1
}
#start controls
W = Controls:number("W")
A = Controls:number("A")
S = Controls:number("S")
D = Controls:number("D")
M1 = Controls:number("Mouse1")
M2 = Controls:number("Mouse2")
Space = Controls:number("Space")
Shift = Controls:number("Shift")
Alt = Controls:number("Alt")
Mwheelup = Controls:number("NextWeapon")
Mwheeldown = Controls:number("PrevWeapon")
Active = Controls:number("Active")
Seat = Controls:entity("Entity")
#end controls

Eyevec = Seat:driver():eye()
###########################################
if(Active){
Targetpos = rangerOffset(5000,Seat:toWorld(vec(0,0,50)),Eyevec):position()
Gpos = Gun:pos() - Shoulders:toWorld(vec(40-abs(Seat:bearing(Targetpos)),clamp(-abs(Seat:bearing(Targetpos))-20,-40,40),-25))
}
else
{
Targetpos = Shoulders:toWorld(vec(0.5,1.25,0.5)*500)  
Gpos = Gun:pos() - Shoulders:toWorld(vec(30,-20,-30))
}

Ang = Seat:toWorld(ang(-Seat:elevation(Targetpos),-Seat:bearing(Targetpos),0))
V = Gun:toLocal(rotationVector(quat(Ang)/quat(Gun))+Gun:pos())
Gun:applyTorque((150*V - 15*Gun:angVelVector())*Gun:mass()/10)  

Angle = Seat:bearing(Eyevec*500000)

Gvec = (($Gpos*150)+(Gpos*20))*Gun:mass()/10*-1
Gun:applyForce(Gvec)


if(clk("Fireable1")){
Fireable1 = 1    
}

if(M1 & Fireable1 == 1){
    pewFire(Gun:toWorld(vec(10,0,0)),Gun:forward(),"Minigun")
    #holoEntity(22):shootTo(holoEntity(22):pos()+vec(-3,0,0),holoEntity(22):up(),vec2(0.01,0.01),1000,100,1,2,1)
    Fireable1 = 0
    timer("Fireable1",100)
    }
