@name function based holo-leg
@inputs [FL FR MFL MFR MRL MRR RL RR]:entity
@outputs 
@persist 
interval(50)
PhxMetallic = "Phoenix_storms/mat/mat_phx_metallic"
Grey = vec(1,1,1)*50
Thighlength = 5*12
Shinlength = 7*12

function holodata (Index,Model:string,Scale:vector,Pos:vector,Ang:angle,Material:string,Color:vector,Parent:entity){
#    holoCreate(Index)
    holoModel(Index,Model)
    holoScale(Index,Scale)
    holoPos(Index,Pos)
    holoAng(Index,Ang)
    holoMaterial(Index,Material)
    holoColor(Index,Color)
    holoParent(Index,Parent)
    
}
function makeLeg(StartIndex,Hip:entity, Foot:entity, TL,SL){
I=StartIndex
while(I < StartIndex + 6){
holoCreate(I)
I++   
}
holodata(StartIndex,"cube",vec(1,1,1)*0.1,vec(),ang(),PhxMetallic,Grey,Hip) # Hip Joint
holodata(StartIndex+1,"cube",vec(1,1,1)*0.1,vec(),ang(),PhxMetallic,Grey,Hip) # True Hypotenuse
holodata(StartIndex+2,"cube",vec(1,1,1)*0.1,vec(),ang(),PhxMetallic,Grey,holoEntity(StartIndex+1)) # Fake Hypotenuse
holodata(StartIndex+3,"cube",vec(1,1,1)*1,holoEntity(StartIndex):toWorld(vec(TL,0,0)),holoEntity(StartIndex):toWorld(ang(0,0,0)),PhxMetallic,Grey,holoEntity(StartIndex)) # Knee joint    
holodata(StartIndex+4,"cube",vec(TL/12,1,1),holoEntity(StartIndex):toWorld(vec(TL/2,0,0)),holoEntity(StartIndex):angles(),PhxMetallic,Grey,holoEntity(StartIndex))
holodata(StartIndex+5,"cube",vec(SL/12,1,1),holoEntity(StartIndex+3):toWorld(vec(-SL/2,0,0)),holoEntity(StartIndex+3):angles(),PhxMetallic,Grey,holoEntity(StartIndex+3))

}
function operateLeg(StartIndex,Hip:entity,Foot:entity,TL,SL,Hippos:vector,BipedToggle){
Avrgpoint = (Hippos + Foot:pos())/2
Hypelength = clamp((Hippos - Foot:pos()):length(),0,TL+SL)
HypeAngle = (Hippos - Foot:pos()):toAngle() + ang(90,0,0)

Out = Hip:angles():yaw()
Out2 = HypeAngle:yaw()
if((Out - Out2) > 180){Out1b = -(360 - (Out))}else{Out1b = Out}
if((Out2 - Out) > 180){Out2b = -(360 - (Out2))}else{Out2b = Out2}
Out3 = Out2b - Out1b

#LCorAngle = LHypeAngle - ang(0,Out3,0)
holoScale(StartIndex,vec(1,1,1)*0.1)

holoPos(StartIndex+1,Avrgpoint)
holoScale(StartIndex+1,vec(0.1,0.1,Hypelength/12)*0)
holoAng(StartIndex+1,HypeAngle)

holoPos(StartIndex+2,Avrgpoint)
holoScale(StartIndex+2,vec(0.5,1,Hypelength/15)*0)
holoAng(StartIndex+2,holoEntity(StartIndex+1):toWorld(ang(0,-Out3*BipedToggle+(!BipedToggle*180),0)))

holoPos(StartIndex,holoEntity(StartIndex+2):toWorld(vec(0,0,Hypelength/2)))

BodyAng = acos((TL^2 + Hypelength^2 - SL^2)/(2*TL*Hypelength)) - 90
holoAng(StartIndex,holoEntity(StartIndex+2):toWorld(ang(-BodyAng,0,0)))
KneeAng = acos((TL^2 + SL^2 - Hypelength^2)/(2*TL*SL))
holoAng(StartIndex+3,holoEntity(StartIndex):toWorld(ang(-KneeAng,0,0)))
}

if(first()){
timer("FL",0)    
timer("FR",1200*1)    
timer("RL",1200*2)    
timer("RR",1200*3)    
timer("ML",1200*4)    
timer("MR",1200*5) 
timer("ML2",1200*6)    
timer("MR2",1200*7) 
}
if(clk("FL")){

makeLeg(1,entity(),FL,Thighlength,Shinlength)    
}
if(clk("FR")){
makeLeg(7,entity(),FR,Thighlength,Shinlength)    
}
if(clk("RL")){
makeLeg(13,entity(),RL,Thighlength,Shinlength)    
}
if(clk("RR")){
makeLeg(19,entity(),RR,Thighlength,Shinlength)    
}
if(clk("ML")){
makeLeg(25,entity(),MFL,Thighlength,Shinlength)    
}
if(clk("MR")){
makeLeg(31,entity(),MFR,Thighlength,Shinlength)    
}
if(clk("ML2")){
makeLeg(37,entity(),MRL,Thighlength,Shinlength)    
}
if(clk("MR2")){
makeLeg(43,entity(),MRR,Thighlength,Shinlength)    
}
    Hippos = entity():toWorld(vec(30,10,0))
operateLeg(1,entity(),FL,Thighlength,Shinlength,Hippos,0)
    Hippos = entity():toWorld(vec(30,-10,0))
operateLeg(7,entity(),FR,Thighlength,Shinlength,Hippos,0)
    Hippos = entity():toWorld(vec(-30,10,0))
operateLeg(13,entity(),RL,Thighlength,Shinlength,Hippos,0)
    Hippos = entity():toWorld(vec(-30,-10,0))
operateLeg(19,entity(),RR,Thighlength,Shinlength,Hippos,0)
    Hippos = entity():toWorld(vec(-10,20,0))
operateLeg(25,entity(),MRL,Thighlength,Shinlength,Hippos,0)
    Hippos = entity():toWorld(vec(-10,-20,0))
operateLeg(31,entity(),MRR,Thighlength,Shinlength,Hippos,0)
    Hippos = entity():toWorld(vec(10,20,0))
operateLeg(37,entity(),MFL,Thighlength,Shinlength,Hippos,0)
    Hippos = entity():toWorld(vec(10,-20,0))
operateLeg(43,entity(),MFR,Thighlength,Shinlength,Hippos,0)
