@name applyTorque Rotor
@inputs Rotor:entity
@outputs RotOut
@persist 
runOnTick(1)
Torque = 10
RotOut = Rotor:angVel():yaw()
Rotor:applyTorque(vec(0,0,Rotor:angVel():yaw()*-1*0.5 + Torque/2))
