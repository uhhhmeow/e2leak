@name Plasma Cannon - Heavy
@inputs M1 Gun:entity Ignore:entity
@outputs Mode
@persist APCount CanFire J K [ActiveProj Seed Parts]:array Vec P:array Target:entity Inc
runOnChat(1)
interval(75)
Vel = 300
if(first()){
    J=100
    I=1
    Parts = entity():getConstraints()
    Parts:unshiftEntity(entity())
    

    rangerDefaultZero(1)
    rangerIgnoreWorld(0)
    rangerFilter(Parts)
    rangerHitEntities(1)
    rangerPersist(1)
    
    CanFire = 1
    CanFireM = 1
}


function holodata (Index,Model:string,Scale:vector,Pos:vector,Ang:angle,Material:string,Color:vector,Parent:entity){
    holoCreate(Index)
#    holoAlpha(Index,0)
    holoScale(Index,Scale)
    holoModel(Index,Model)
    holoPos(Index,Pos)
    holoAng(Index,Ang)
    holoMaterial(Index,Material)
    holoColor(Index,Color)

#    holoParent(Index,Parent)
    
}

if(changed(owner():lastSaidWhen()) && owner():lastSaid() == "//Firemode"){
    Mode = !Mode
    hint("Firemode: "+Mode,2)
    hideChat(1)    
}



function detonate (Index){
    #Bomb = propSpawn("models/props_phx/misc/potato_launcher_explosive.mdl",holoEntity(Index):pos(),ang(),0) 
    #Bomb = propSpawn("models/props_phx/misc/flakshell_big.mdl",holoEntity(Index):pos(),ang(),0) 
    #Bomb = propSpawn("models/props_phx/misc/potato_launcher_explosive.mdl",holoEntity(Index):pos(),ang(),0) 
    #Bomb = propSpawn("models/props_phx/mk82.mdl",holoEntity(Index):pos(),ang(),0) 
    Bomb = propSpawn("models/props_phx/ww2bomb.mdl",holoEntity(Index):pos(),ang(),0) 
    Bomb:propBreak()
}
function entity nearestPlayer(Pos:vector){
    
    P = players()
    NearestIndex = 0
    NearestDist = 99999
    for(I=1,P:count()){
        Dist = (P[I,entity]:pos() - Pos):length()
        if(Dist < NearestDist && P[I,entity] != owner() && P[I,entity]:getMaterial() != "models/props_combine/com_shield001a" && P[I,entity]:isAlive() && P[I,entity] != Ignore){
           NearestDist = Dist
           NearestIndex = I         
       }        
   }
    return P[NearestIndex,entity]
}
function proximityDetonate(Index, Pos:vector){
    
    Vec = (Pos - holoEntity(ActiveProj[Index,number]):pos()):length()
    if(Vec < 200){
        #detonate(Index)
        #ActiveProj[Index,entity]:propDelete()
        #Bomb = propSpawn("models/props_phx/misc/potato_launcher_explosive.mdl",holoEntity(ActiveProj[Index,number]):pos(),ang(),0) 
        Bomb = propSpawn("models/props_phx/ww2bomb.mdl",holoEntity(ActiveProj[Index,number]):pos(),ang(),0) 
        #Bomb = propSpawn("models/props_phx/mk82.mdl",holoEntity(ActiveProj[Index,number]):pos(),ang(),0) 
        Bomb:propBreak()
        holoDelete(ActiveProj[Index,number])
        ActiveProj:remove(Index)  
    }
}

if(clk("CanFire")){CanFire = 1}
if(clk("CanFireM")){CanFireM = 1}
function string randSound(){
    #Sounds = array("/ambient/energy/ion_cannon_shot1.wav","/ambient/energy/ion_cannon_shot2.wav","/ambient/energy/ion_cannon_shot3.wav")    
    Sounds = array("/weapons/airboat/airboat_gun_energy2.wav","/ambient/energy/ion_cannon_shot2.wav","/ambient/energy/ion_cannon_shot3.wav")    
    return Sounds[1,string]
    #randint(1,3),string]
}

Inc++
if(M1 && CanFire && ActiveProj:count() < 15){
    holodata(J,"hqsphere",vec(2,1,1)*2,Gun:toWorld(vec(-Vel+40 + 25,0,0)),Gun:angles(),"models/effects/comball_glow1",vec(1,1,1)*255,noentity())
    #holoAlpha(J,0)
    holoEntity(J):setTrails(300,10,0.15,"trails/physbeam",vec(1,0.5,1)*255,255)
    #holoEntity(J):setTrails(500,0,3,"trails/laser",vec(0.75,1,0.75)*255,255)
    SoundIndex = Inc
    Gun:soundPlay(SoundIndex,3,randSound())
    soundPitch(SoundIndex,25)
    ActiveProj:insertNumber(1,J)
    J++    
    timer("CanFire",500)
    CanFire = 0
}

for(I=0,ActiveProj:count()){
    
    if(holoEntity(ActiveProj[I,number]):pos() != vec()){
        Target = nearestPlayer(holoEntity(ActiveProj[I,number]):pos())
        
        proximityDetonate(I,Target:pos())
        holoPos(ActiveProj[I,number],holoEntity(ActiveProj[I,number]):toWorld(vec(Vel,0,0)))

        Dist = rangerOffset(holoEntity(ActiveProj[I,number]):toWorld(vec(Vel*0.5,0,0)),holoEntity(ActiveProj[I,number]):toWorld(vec(0,0,0))):distance()
        if((Dist != 0) || !holoEntity(ActiveProj[I,number]):toWorld(vec(Vel,0,0)):isInWorld()){
            detonate(ActiveProj[I,number])
            holoDelete(ActiveProj[I,number])
            ActiveProj:remove(I)  
            Dist = 0
       }    
    }
}
APCount = ActiveProj:count()
if(maxquota() < 200){reset()}

