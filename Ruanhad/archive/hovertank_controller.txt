@name Hovertank Controller
@inputs [Controls Camera]:wirelink [Core Turret GunMount]:entity MVel
@outputs Ranger Altitude Material:string [PColor SColor]:vector YawAddon TurretOffset:vector Crowflies TargetEnt:entity Zoom [Campos Camvec]:vector
@persist Vert P1 Y1 R1 Parts:array Inc
@persist [Gunpos]:vector GunActive MissileActive Choice PodUP PodPitch Angle Pos:vector2
@persist CanFireMissiles Fireable2 Mass
runOnTick(1)

function holoData (Index,Model:string,Scale:vector,Pos:vector,Ang:angle,Material:string,Color:vector,Parent:entity){
    holoCreate(Index)
    holoModel(Index,Model)
    holoScale(Index,Scale)
    holoPos(Index,Pos)
    holoAng(Index,Ang)
    holoMaterial(Index,Material)
    holoColor(Index,Color)
    holoParent(Index,Parent)
}

function entity:turnTo(Tarang:angle) {
    V = This:toLocal(rotationVector(quat(Tarang)/quat(This))+This:pos())
    This:applyTorque((150*V - 15*This:angVelVector())*This:inertia()*3)  
}
function entity:moveTo(Tarpos:vector) {
    This:applyForce(((Tarpos-This:pos())*15-This:vel())*(This:mass()))    
}
function number rotationDifference(Out, Out2){
    if((Out - Out2) > 180){Out1b = -(360 - (Out))}else{Out1b = Out}
    if((Out2 - Out) > 180){Out2b = -(360 - (Out2))}else{Out2b = Out2}
    return Out2b - Out1b    
}

function entity:hoverForce(VertForce){
    local Force = (VertForce - This:pos():z())*15-This:vel():z()*This:mass()
    This:applyForce(vec(0,0,Force))    
}

function entity:turnToWeak(Tarang:angle) {
    V = This:toLocal(rotationVector(quat(Tarang)/quat(This))+This:pos())
    This:applyTorque((15*V - 15*This:angVelVector())*This:inertia())  
}

#start controls
W = Controls:number("W")
A = Controls:number("A")
S = Controls:number("S")
D = Controls:number("D")
R = Controls:number("R")
M1 = Controls:number("Mouse1")
M2 = Controls:number("Mouse2")
Space = Controls:number("Space")
Alt = Controls:number("Alt")
Shift = Controls:number("Shift")#
Mwheelup = Controls:number("NextWeapon")
Mwheeldown = Controls:number("PrevWeapon")
Zoom = Controls:number("Zoom")
Active = Controls:number("Active")
Seat = Controls:entity("Entity")

Shift = 0
if(first()){
    Parts = entity():getConstraints()
    Core:setMass(15000)
    Mass = Core:mass() + 1000
    for(I=1,Parts:count()){
        if(!Parts[I,entity]:isVehicle()){
            Parts[I,entity]:setAlpha(255)  
            #Mass+= Parts[I,entity]:mass()  
        }
    }
    
    rangerFilter(Parts)
    rangerHitWater(1)
    rangerPersist(1) 
    
    Vert = 75   
    Material = "Phoenix_storms/mat/mat_phx_metallic"
    PColor = vec(39,117,234)*1*0.8
    
    #Mass = 9999
    
}

if(Mwheelup){Vert+=15}
if(Mwheeldown && Vert > 50){Vert-=15}
Inc+=3
Bob = sin(Inc)*1
Ranger = rangerOffset(100000,Core:pos(),vec(0,0,-1)):distance()

Altitude = Ranger - Vert + Bob
HoverVec = (($Altitude*150)+(Altitude*20))*Mass/30 * -1
if(Space && changed(Space)){
    Jump = 100    
}else{
    Jump = 0
}
if((Ranger) < (Vert)){
    Core:applyForce(vec(0,0,HoverVec + Jump*Mass*15))
}else{ 
    Core:applyForce(vec(0,0,9.81*Mass*-1))

}
StabVel = Core:vel()/100 + Core:forward()*(S-W)*(10+(Shift*25)) + Core:right()*(A-D)*(10+(Shift*25))
Stabiliser = (($StabVel*150)+(StabVel*20))*Mass/50 * -1
Core:applyForce(Stabiliser:setZ(0))

StabAng = Core:angVel()/30 + ang(0,0,(A-D)*5)
AngStab = (($StabAng*150)+(StabAng*20))*Mass/30 * -1

Core:applyAngForce(AngStab)

P1 = Core:angles():pitch()
Pitch = (($P1*150)+(P1*20))*Mass/2 * -1

YawAddon = rotationDifference(Turret:angles():yaw(), Core:angles():yaw())
if(W || A || S || D){
    Y1 = YawAddon
}else{
    Y1 = Core:angVel():yaw()/3
}
Yaw = (($Y1*150)+(Y1*20))*Mass/3 * -1

R1 = Core:angles():roll()
Roll = (($R1*150)+(R1*20))*Mass/3 * -1

#if(!(W || A || S || D)){
#   Yaw = 0 
#}

Core:applyAngForce(ang(Pitch,Yaw,Roll)) #(A-D)*Core:mass()*(30 + (Alt*30)) + 

Camvec = Turret:toLocalAxis((Seat:driver():eyeAngles()):forward())
Campos = Core:pos() + Camvec*-150 + vec(0,0,100)

Camera:setNumber("Activated",Active)
Camera:setVector("Position",Campos)
Camera:setVector("Direction",Camvec)

Turret:propFreeze(1)
Turret:setPos(Core:toWorld(vec(0,0,24)))
#Turret:setAng(Camvec:toAngle():setPitch(0):setRoll(Core:angles():roll()))

if(Active){
Turret:setAng(Camvec:toAngle():setPitch(0))
}

ROOutput = rangerOffset(500000,Campos,Camvec) 
TargetEnt = ROOutput:entity()
ROPos = ROOutput:position() # Target position

ROPosOffset = ROPos - GunMount:pos()
ROPOCor = ROPos:setZ(GunMount:pos():z()) # imaginary target location for use in calculating the "crow-flies" distance
ROPOZDiff = ROOutput:position():z() - GunMount:pos():z() # difference in Z axis between gun position and aimed target position
Crowflies = (ROPOCor - GunMount:pos()):length()
Velocity = MVel*3.5 # muzzle velocity
Grav = 9.8 #gravity
Angle = atan( (Velocity^2 - sqrt(Velocity^4 - Grav*(Grav*(Crowflies^2) + 2*ROPOZDiff*Velocity^2 )  ))/(Grav*Crowflies)  )
Disttravelled = ((Velocity*cos(Angle))/Grav) * ( Velocity*sin(Angle) + sqrt( (Velocity*sin(Angle))^2 + 2*Grav*ROPOZDiff ) )
DesiredDist = Disttravelled
#Time = 

Time = (DesiredDist)/Velocity*cos(Velocity/DesiredDist)

GAng = ang(-Angle,Turret:angles():yaw(),Turret:angles():roll())

if(!Active){
GAng = Turret:angles()    
}
TurretOffset = Turret:toLocal(GunMount:pos())

GunMount:propFreeze(1)
#if(Active){
    GunMount:setAng(GAng)
#}
GunMount:setPos(Turret:toWorld(vec(47.5,0,0)))

if(Active){
    #Core:propFreeze(Zoom)
}else{

}
#GunMount:turnTo(GAng)
