@name Variable Bezier
@inputs [Hip Foot Mid1 Mid2 Mid3 Mid4]:entity
@outputs 
@persist A
interval(10) 

if(first()){
    for(I=1,20){
holoCreate(I)
holoModel(I,"hq_sphere")

holoAng(I,ang(0,0,0))
holoMaterial(I,"Phoenix_storms/mat/mat_phx_metallic")
holoColor(I,vec(170,130,170))
holoParent(I,entity())
}
}
A = A + 1
P0 = entity():pos()
P1 = entity():pos() + (sin(A)*vec(50,1,1)) + vec(0,0,300)
P2 = P1 + (sin(A+90)*vec(50,75,1)) + vec(0,0,100)
P3 = P2 + (sin(-A+180)*vec(75,50,1)) + vec(0,0,300)
P4 = P3 + (sin(-A)*vec(100,0,0)) + (cos(-A)*vec(0,50,0)) + vec(0,0,300)
P5 = P4 + (sin(-A)*vec(75,0,0)) + (cos(-A)*vec(0,60,0)) + vec(0,0,300)

#P0 = entity():pos()
#P1 = Mid1:pos()
#P2 = Mid2:pos()
#P3 = Mid3:pos()
#P4 = Mid4:pos()
#P5 = Foot:pos()

for(I=1,30){
    T = I/30
One =      ((1-T)^5)*P0
Two =    5*((1-T)^4)*T^1*P1
Three = 10*((1-T)^3)*T^2*P2
Four =  10*((1-T)^2)*T^3*P3
Five =   5*((1-T)^1)*T^4*P4
Six =                T^5*P5

Bezier = One+Two+Three+Four+Five+Six
holoScale(30 - I + 1,vec(2,2,2) + vec(2,2,2)*T*2)
holoPos(I,Bezier)
#holoAng(I,(holoEntity(I):pos() - holoEntity(I-1):pos()):toAngle()+ang(90,0,0))    

}
