@name function based hexa-spider
@inputs Damage DType:string
@outputs DistToTarget Vel DiffZ Target:entity [FL FR ML MR ML2 MR2 RL RR]:vector Health Alive AttackingCar DToggle
@persist Inc Posture  [Hip]:entity [MatP MatS]:string [ColP ColS]:vector Foot SoundList:array Init RageAttack
runOnTick(100)
runOnChat(1)

if(changed(owner():lastSaidWhen()) && owner():lastSaid():left(8) == "!follow "){
    hideChat(1)
    Target = findPlayerByName(owner():lastSaid():sub(9))
    if(Init == 1){Init = 0}
}

if(Init == 0){
    Hip:propFreeze(0)
}
if(DType != "Crush" && Damage > 0){ #  && Damage != 0 && changed(Damage)
    Health -= Damage
    DToggle = 1
}else{
    DToggle = 0
    #Health -= Damage/50
}
if(Health < 300){Health+=0.1}

if(first() || dupefinished()){
    SoundList[1,string] = "/ambient/forest_bird1.wav"
    SoundList[2,string] = "/ambient/forest_bird2.wav"
    SoundList[3,string] = "/ambient/forest_bird3.wav"
    SoundList[4,string] = "/ambient/forest_bird4.wav"
    SoundList[5,string] = "/ambient/forest_bird5.wav"
    SoundList[6,string] = "/ambient/forest_bird6.wav"
    SoundList[7,string] = "/ambient/forest_bird7.wav"
    SoundList[8,string] = "/ambient/forest_bird8.wav"
    SoundList[9,string] = "/ambient/forest_bird9.wav"
    SoundList[10,string] = "/ambient/creatures/rats1.wav"
    SoundList[11,string] = "/ambient/creatures/rats2.wav"
    SoundList[12,string] = "/ambient/creatures/rats3.wav"
    SoundList[13,string] = "/ambient/creatures/rats4.wav"
    
    SoundList[14,string] = "/npc/advisor/advisorscreenvx01.wav"
    SoundList[15,string] = "/npc/advisor/advisorscreenvx02.wav"
    SoundList[16,string] = "/npc/advisor/advisorscreenvx03.wav"
    SoundList[17,string] = "/npc/advisor/advisorscreenvx04.wav"
    SoundList[18,string] = "/npc/advisor/advisorscreenvx05.wav"
    SoundList[19,string] = "/npc/advisor/advisorscreenvx06.wav"
    SoundList[20,string] = "/npc/advisor/advisorscreenvx07.wav"
    SoundList[21,string] = "/npc/advisor/advisorscreenvx08.wav"

    SoundList[22,string] = "/npc/zombie/zombie_pain1.wav"        
    SoundList[23,string] = "/npc/zombie/zombie_pain2.wav"        
    SoundList[24,string] = "/npc/zombie/zombie_pain3.wav"        
    SoundList[25,string] = "/npc/zombie/zombie_pain4.wav"        
    
    #Hip:soundPlay(0,0, "/common/talk.wav")
    Alive = 1
    Health = 300
    Target = owner()
    Hip = entity():isWeldedTo()
        Hip:setAlpha(0)
    timer("FL",1200*0)
    timer("FR",1200*1)
    timer("ML",1200*2)
    timer("MR",1200*3)
    timer("ML2",1200*4)
    timer("MR2",1200*5)
    timer("RL",1200*6)
    timer("RR",1200*7)
    
    timer("final",1200*10)
    
    timer("bodywork",1200*8)
    Hip:setMass(1000)
    Posture = 40
    timer("UpdateLegs",1200*9)
    MatP = "Phoenix_storms/mat/mat_phx_metallic" #"Cmats/Base_Grey"
    MatS = "models/shiny"
    ColP = vec(1,1,1)*50
    ColS = vec(1,1,0)*255
    timer("soundEffects",1200*9)
    entity():setAlpha(0)
    Init = 1
    
    rangerHitWater(1)
    rangerPersist(1)
}

function routineChirrups(){
    Rand = randint(10,13) 
    Hip:soundPlay(Inc,5,SoundList[Rand,string])   
}

function attackGrowls(){
    Rand = randint(14,21) 
    Hip:soundPlay(Inc,5,SoundList[Rand,string])   
}

function painSounds(){
    Rand = randint(22,25) 
    Hip:soundPlay(Inc,5,SoundList[Rand,string])   
}

if(clk("soundEffects")){
    if(AttackingCar || RageAttack){
        if(Alive){
            attackGrowls()
        }
    timer("soundEffects",750)    
    }else{
        if(Alive){
            routineChirrups()
        }
    timer("soundEffects",1500)
    }    
}
if(Damage != 0){
    painSounds()    
}

AttackingCar = Target:inVehicle()

if(Health <= 0){
Alive = 0 
timer("Respawn",15000)   
}
if(clk("Respawn")){
Health = 300
Alive = 1    
}
if(changed(Foot)){
    #Hip:soundPlay(0,0, "/common/talk.wav")    
}
function holoData (Index,Model:string,Scale:vector,Pos:vector,Ang:angle,Material:string,Color:vector,Parent:entity){
    holoCreate(Index)
    holoModel(Index,Model)
    holoScale(Index,Scale)
    holoPos(Index,Pos)
    holoAng(Index,Ang)
    holoMaterial(Index,Material)
    holoColor(Index,Color)
    holoParent(Index,Parent)
    
}
function makeLeg(I:number, H:entity) {

    holoData(I,"hqsphere",vec(0.2,0.2,0.2),H:pos(),ang(0,0,0),"",vec(1,1,1)*255,H) # hip
    holoData(I+1,"hqsphere",vec(0.35,0.35,4.15),H:pos() + vec(0,0,-25),ang(0,0,0),MatP,ColP,holoEntity(I)) # thigh
    holoData(I+2,"hqsphere",vec(0.2,0.2,0.2),H:pos() + vec(0,0,-50),ang(0,0,0),MatS,ColS,holoEntity(I+1)) # first joint
    holoData(I+3,"hqsphere",vec(0.25,0.25,2.0),H:pos() + vec(0,0,-62.5),ang(0,0,0),MatP,ColP,holoEntity(I+2)) # midleg
    holoData(I+4,"hqsphere",vec(0.2,0.2,0.2),H:pos() + vec(0,0,-75),ang(0,0,0),MatS,ColS,holoEntity(I+3)) # second joint
    holoData(I+5,"hqsphere",vec(0.15,0.15,4.15),H:pos() + vec(0,0,-100),ang(0,0,0),MatP,ColP,holoEntity(I+4)) # shin
    holoData(I+6,"hqsphere",vec(0.1,0.1,0.2),H:pos() + vec(0,0,-125),ang(0,0,0),MatS,ColS,holoEntity(I+5)) # superflous toe
    holoData(I+7,"hqsphere",vec(0.2,0.2,0.2),H:pos(),ang(0,0,0),"",vec(1,1,1)*255,noentity()) # endpoint marker

    holoAlpha(I+7,32)

    
}

function vector interpolateVector(V1:vector, V2:vector,InterpVelocity){ # interpolates smoothly from V1 to V2
RangeV1ToV2 = (V1 - V2):length()
DirectionV1ToV2 = (V1 - V2):normalized()

InterpVector = V1+((DirectionV1ToV2*-2)*(RangeV1ToV2/5)*InterpVelocity)
return InterpVector    
}

function operateLeg(I:number,H:entity,F:vector){

holoPos(I+7,interpolateVector(holoEntity(I+7):pos(),F + vec(0,0,(holoEntity(I+7):pos() - F):length()/2.5),2+(AttackingCar*1)))
Dist1 = (H:pos() - holoEntity(I+7):pos()):length() # length from hip to foot - straight-line
Dx2 = H:toLocal(holoEntity(I+7):pos()):x()
Dy2 = H:toLocal(holoEntity(I+7):pos()):y()
    Yaw= atan(Dy2,Dx2)
    
Dy = H:toLocal(holoEntity(I+7):pos()):z()
Pitch = -asin(Dy/Dist1)-90

if(Dist1<120){
Squeeze = acos(((Dist1-25)/2)/50)
}else{
Squeeze =0
}
holoAng(I,H:toWorld(ang(Pitch-Squeeze,Yaw,0))) # hip
holoAng(I+2,H:toWorld(ang(Pitch,Yaw,0))) # upper knee
holoAng(I+4,H:toWorld(ang(Pitch+Squeeze,Yaw,0))) # lower knee    
}
function number rotationDifference(Out, Out2){
    if((Out - Out2) > 180){Out1b = -(360 - (Out))}else{Out1b = Out}
    if((Out2 - Out) > 180){Out2b = -(360 - (Out2))}else{Out2b = Out2}
    return Out2b - Out1b    
}
function entity:moveTo(Tarpos:vector){
    This:applyForce(((Tarpos-This:pos())*15-This:vel())*(This:mass()*0.75))    
}
function entity:turnTo(Tarang:angle){
    V = This:toLocal(rotationVector(quat(Tarang)/quat(This))+This:pos())
    This:applyTorque((150*V - 15*This:angVelVector())*This:inertia()*3)  
}



if(clk("FL")){makeLeg(0,Hip)}
if(clk("FR")){makeLeg(8,Hip)}
if(clk("ML")){makeLeg(16,Hip)}
if(clk("MR")){makeLeg(24,Hip)}
if(clk("ML2")){makeLeg(32,Hip)}
if(clk("MR2")){makeLeg(40,Hip)}
if(clk("RL")){makeLeg(48,Hip)}
if(clk("RR")){makeLeg(56,Hip)}
if(Target:inVehicle()){
MinRange = 0}else{
MinRange = 200
}
if(Alive){
Inc+=5+(AttackingCar*2)
Cycle = Inc%120
if(Cycle > 0 && Cycle <= 60){Foot = 1}
if(Cycle > 60 && Cycle <= 120){Foot = 2}

if(changed(owner():lastSaidWhen()) && owner():lastSaid():left(5) == "!rage"){
    hideChat(1)
    RageAttack = !RageAttack    
}

    Dire = Hip:forward() + Hip:right()*((Target:pos() - Hip:pos()):length() < 350)*0
    DistToTarget = clamp((Target:pos() - Hip:pos()):length() - MinRange,-200,200)
if(AttackingCar){
    Vel = (($DistToTarget*150)+(DistToTarget*20))/(60 - AttackingCar*30)
}else{
    if((Target:pos() - Hip:pos()):length() > MinRange - (RageAttack*MinRange)){
        Vel = 75 + ((AttackingCar || RageAttack)*25)
    }
    else{
        Vel = 0
    }
}
VertOffset = 50
if(Foot == 1 && changed(Foot)){
    Range = rangerOffset(300,Hip:toWorld(vec(50,50,VertOffset)) + Dire*Vel,Hip:up()*-1)
    FL = Range:position()
    if(Range:distance() >= 300){FL = rangerOffset(300,Hip:toWorld(vec(50,50,VertOffset)) + Dire*Vel,Hip:up()*-1):position()}
    Range = rangerOffset(300,Hip:toWorld(vec(25,-70,VertOffset)) + Dire*Vel,Hip:up()*-1)
    MR = Range:position()
    if(Range:distance() >= 300){MR = rangerOffset(300,Hip:toWorld(vec(25,-70,VertOffset)) + Dire*Vel,Hip:up()*-1):position()}
    Range = rangerOffset(300,Hip:toWorld(vec(-25,70,VertOffset)) + Dire*Vel,Hip:up()*-1)
    ML2 = Range:position()
    if(Range:distance() >= 300){ML2 = rangerOffset(300,Hip:toWorld(vec(-25,70,VertOffset)) + Dire*Vel,Hip:up()*-1):position()}
    Range = rangerOffset(300,Hip:toWorld(vec(-50,-50,VertOffset)) + Dire*Vel,Hip:up()*-1)
    RR = Range:position()
    if(Range:distance() >= 300){RR = rangerOffset(300,Hip:toWorld(vec(-50,-50,VertOffset)) + Dire*Vel,Hip:up()*-1):position()}
}
if(Foot == 2 && changed(Foot)){
    Range = rangerOffset(300,Hip:toWorld(vec(50,-50,VertOffset)) + Dire*Vel,Hip:up()*-1)
    FR = Range:position()
    if(Range:distance() >= 300){FR = rangerOffset(300,Hip:toWorld(vec(50,-50,VertOffset)) + Dire*Vel,Hip:up()*-1):position()}
    Range = rangerOffset(300,Hip:toWorld(vec(25,70,VertOffset)) + Dire*Vel,Hip:up()*-1)
    ML = Range:position()
    if(Range:distance() >= 300){ML = rangerOffset(300,Hip:toWorld(vec(25,70,VertOffset)) + Dire*Vel,Hip:up()*-1):position()}
    Range = rangerOffset(300,Hip:toWorld(vec(-25,-70,VertOffset)) + Dire*Vel,Hip:up()*-1)
    MR2 = Range:position()
    if(Range:distance() >= 300){MR2 = rangerOffset(300,Hip:toWorld(vec(-25,-70,VertOffset)) + Dire*Vel,Hip:up()*-1):position()}
    Range = rangerOffset(300,Hip:toWorld(vec(-50,50,VertOffset)) + Dire*Vel,Hip:up()*-1)
    RL = Range:position()
    if(Range:distance() >= 300){RL = rangerOffset(300,Hip:toWorld(vec(-50,50,VertOffset)) + Dire*Vel,Hip:up()*-1):position()}
}
}else{
FL = Hip:toWorld(vec(5,5,-30))
FR = Hip:toWorld(vec(5,-5,-30))
ML = Hip:toWorld(vec(2,5,-30))
MR = Hip:toWorld(vec(2,-5,-30))
ML2 = Hip:toWorld(vec(-2,5,-30))
MR2 = Hip:toWorld(vec(-2,-5,-30))
RL = Hip:toWorld(vec(-5,5,-30))
RR = Hip:toWorld(vec(-5,-5,-30))
    
}

if(clk("UpdateLegs")){
    operateLeg(0,Hip,FL)
    operateLeg(8,Hip,FR)
    operateLeg(16,Hip,ML)
    operateLeg(24,Hip,MR)
    operateLeg(32,Hip,ML2)
    operateLeg(40,Hip,MR2)
    operateLeg(48,Hip,RL)
    operateLeg(56,Hip,RR)
    timer("UpdateLegs",50)
}


if(clk("bodywork")){
holoData(64,"models/Gibs/Shield_Scanner_Gib3.mdl",vec(1,1,1.5)*2,Hip:toWorld(vec(-30,0,10)),Hip:toWorld(ang(-25,-10,-5)),MatP,ColP,Hip)    
holoData(65,"models/Gibs/Shield_Scanner_Gib3.mdl",vec(1,1,1)*1.5,Hip:toWorld(vec(0,0,0)),Hip:toWorld(ang(-25,-10,-5)),MatP,ColP,Hip)    
holoData(66,"models/Gibs/gunship_gibs_sensorarray.mdl",vec(1,1,1)*1,Hip:toWorld(vec(20,0,0)),Hip:toWorld(ang(0,0,0)),MatP,vec(1,1,1)*20,Hip)    
holoData(67,"models/Gibs/Antlion_gib_small_1.mdl",vec(1,1,1)*2.5,Hip:toWorld(vec(25,6,-5)),Hip:toWorld(ang(70,0,0)),MatP,ColP,holoEntity(66))    
holoData(68,"models/Gibs/Antlion_gib_small_1.mdl",vec(1,1,1)*2.5,Hip:toWorld(vec(25,-6,-5)),Hip:toWorld(ang(70,0,0)),MatP,ColP,holoEntity(66))    
    
}#models/Gibs/gunship_gibs_sensorarray.mdl

Breath = sin((Inc/5) * (1+AttackingCar*3))*2.5
Nibble = sin((Inc*2) * (1+AttackingCar*3))*2.5 
holoAng(67,holoEntity(66):toWorld(ang(70+(AttackingCar * sin(Inc*3)*30 - 50),Nibble*-5,0)))
holoAng(68,holoEntity(66):toWorld(ang(70+(AttackingCar * sin(Inc*3)*30 - 50),Nibble*5,0)))

AvrgPoint = (FL+FR+ML+MR+ML2+MR2+RL+RR)/8
if(Alive){
Hip:moveTo(AvrgPoint+vec(0,0,Posture+Breath + ((Target:pos() - Hip:pos()):length() < 150 && AttackingCar)* -20))
}
DiffZ =((FL:z() + FR:z())/2 - (RL:z() + RR:z())/2)
Pitch = asin(((FL:z() + FR:z())/2 - (RL:z() + RR:z())/2 - 90))

DireTravel = (((FL + FR)/2 + (RL + RR)/2)/2):normalized()# + (Target:pos() - holoEntity(66):pos()):normalized())/2
LocalUp = (AvrgPoint - Hip:pos()):normalized()
quat(DireTravel,LocalUp):toAngle()
#holoAng(66,quat(DireTravel,LocalUp):toAngle())
holoAng(66,((Target:pos()+vec(0,0,50)+Target:vel()*AttackingCar*3) - holoEntity(66):pos()):toAngle()+ang(0,0,Breath*5 + 5))
#rotationDifference(Hip:angles():yaw(),holoEntity(66):angles():yaw())
HAng = ang(0,Hip:angles():yaw() + rotationDifference(Hip:angles():yaw(),holoEntity(66):angles():yaw())/2.5,0)
if(Alive){
Hip:turnTo((HAng:rotateAroundAxis(Hip:right(),DiffZ)))
}
#Hip:turnTo((:pos() - Hip:pos()):toAngle():setPitch(-DiffZ/2):setRoll(0))
#Hip:turnTo((owner():pos() - Hip:pos()):toAngle():setPitch(-DiffZ/2):setRoll(0))

if(clk("final")){
    for(I=1,100){
       #holoVisible(I,owner(),1)    
    }
}

if(maxquota() < 200){ 
    reset()
}
