@name XV9
@inputs [Controls Camera]:wirelink [Torso Hip FL FR AL AR]:entity
@outputs 
@persist
runOnTick(1)

function holoData (Index,Model:string,Scale:vector,Pos:vector,Ang:angle,Mat:string,Color:vector,Parent:entity){
    holoCreate(Index)
    holoModel(Index,Model)
    holoScale(Index,Scale)
    holoPos(Index,Pos)
    holoAng(Index,Ang)
    holoMaterial(Index,Mat)
    holoColor(Index,Color)
    holoParent(Index,Parent)
}
Black = vec(1,1,1)*10
DarkRed = vec(0.3,0.1,0.1)*255
DarkBlue = vec(0.1,0.1,0.3)*200
Grey = vec(1,1,1)*50
LightGrey = vec(1,1,1)*150
PhxMetallic = "Phoenix_storms/mat/mat_phx_metallic"
Shiny = "models/shiny"
if(first()){
    #Torso = entity():isWeldedTo()   
    
    timer("torso_01",1200*0) 
}

if(clk("torso_01")){
    
    holoData(1,"models/props_junk/TrashBin01a.mdl",vec(2,1,1)*1,Hip:toWorld(vec(-30,0,10)),Hip:toWorld(ang(0,180,0)),PhxMetallic,DarkRed,Hip)    
    holoData(2,"models/props_junk/TrashBin01a.mdl",vec(1,2,0.5)*1,holoEntity(1):toWorld(vec(-5,0,-20)),holoEntity(1):toWorld(ang(40,0,180)),PhxMetallic,DarkRed,Hip)    
    holoData(3,"models/props_junk/TrashBin01a.mdl",vec(1,1,0.75)*1,Hip:toWorld(vec(30,0,5)),Hip:toWorld(ang(10,0,0)),PhxMetallic,DarkRed,Hip)    
    holoData(4,"models/props_lab/monitor02.mdl",vec(1,1,1)*1,holoEntity(3):toWorld(vec(-15,0,-20)),holoEntity(3):toWorld(ang(-100,180,0)),PhxMetallic,DarkBlue,Hip)    
    holoData(5,"models/mechanics/wheels/wheel_speed_72.mdl",vec(1,1,1)*0.45,Hip:toWorld(vec(0,0,22.5)),Hip:toWorld(ang(0,0,0)),PhxMetallic,Grey,Hip)    
    holoData(6,"models/XQM/wingpiece3.mdl",vec(0.9,1,1)*0.55,Hip:toWorld(vec(0,0,20)),Hip:toWorld(ang(0,90,7.5)),PhxMetallic,Grey,Hip)    
    holoData(7,"models/XQM/wingpiece3.mdl",vec(0.7,1,1)*0.55,Hip:toWorld(vec(0,0,-20)),Hip:toWorld(ang(0,90,5)),PhxMetallic,Grey,Hip)    
    holoData(8,"cube",vec(5,1.25,3)*1,Hip:toWorld(vec(0,0,0)),Hip:toWorld(ang(5,0,0)),PhxMetallic,Grey,Hip)    
    
    #[
    holoData(1,"models/XQM/cylinderx1large.mdl",vec(0.65,1,1)*1,Torso:toWorld(vec(-40,40,35)),Torso:toWorld(ang(0,90,0)),PhxMetallic,Black,Torso)    
    holoData(2,"models/XQM/cylinderx1large.mdl",vec(0.65,1,1)*1,Torso:toWorld(vec(-40,-40,35)),Torso:toWorld(ang(0,90,0)),PhxMetallic,Black,Torso)    
    holoData(3,"models/XQM/cylinderx1large.mdl",vec(0.15,1,1)*1,Torso:toWorld(vec(-40,90,35)),Torso:toWorld(ang(0,90,0)),PhxMetallic,Black,Torso)    
    holoData(4,"models/XQM/cylinderx1large.mdl",vec(0.15,1,1)*1,Torso:toWorld(vec(-40,-90,35)),Torso:toWorld(ang(0,90,0)),PhxMetallic,Black,Torso)    
    
    holoData(5,"models/hunter/tubes/tube1x1x4c.mdl",vec(1.15,1.15,0.875)*1.1,Torso:toWorld(vec(-40,90,35)),Torso:toWorld(ang(-90,90,0)),PhxMetallic,Black,Torso)    
    
    holoData(6,"models/hunter/blocks/cube05x2x025.mdl",vec(1,0.75,0.5)*1,Torso:toWorld(vec(10,30,45)),Torso:toWorld(ang(90,90,0)):rotateAroundAxis(Torso:right(),-10):rotateAroundAxis(Torso:up(),5),PhxMetallic,DarkRed,Torso)    
    holoData(7,"models/hunter/blocks/cube05x2x025.mdl",vec(1,0.75,0.75)*1,holoEntity(6):toWorld(vec(0.5,0,1)),holoEntity(6):toWorld(ang(30,0,0)),PhxMetallic,DarkRed,Torso)    
   
    holoData(8,"models/hunter/blocks/cube05x2x025.mdl",vec(1,0.75,0.5)*1,Torso:toWorld(vec(10,-30,45)),Torso:toWorld(ang(90,90,0)):rotateAroundAxis(Torso:right(),-10):rotateAroundAxis(Torso:up(),-5),PhxMetallic,DarkRed,Torso)    
    holoData(9,"models/hunter/blocks/cube05x2x025.mdl",vec(1,0.75,0.75)*1,holoEntity(8):toWorld(vec(0.5,0,-1)),holoEntity(8):toWorld(ang(-30,0,0)),PhxMetallic,DarkRed,Torso)    
    ]#

    # holoData(5,"models/XQM/cylinderx1large.mdl",vec(0.5,2,0.75)*0.75,Torso:toWorld(vec(75,0,35)),Torso:toWorld(ang(0,90,45)),PhxMetallic,Black,Torso)    
    # holoData(6,"cube",vec(1.85,7.5,8)*1,holoEntity(5):toWorld(vec(0,0,-47.5)),holoEntity(5):toWorld(ang(0,0,0)),PhxMetallic,Black,Torso)    
    
}
