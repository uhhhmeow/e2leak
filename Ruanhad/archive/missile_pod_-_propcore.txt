@name Missile Pod - propcore
@inputs Fire Pod:entity
@outputs Fire1 Fire2 Seek1 Seek2 Loaded1 Loaded2 [Missile1 Missile2]:entity Tube
runOnTick(1)

if(first()){

for(I=1,4){
holoCreate(I)

holoParent(I,Pod)    
}

holoModel(1,"hqcylinder")
holoPos(1,Pod:toWorld(vec(0,12,0)))
holoAng(1,Pod:toWorld(ang(90,0,0)))
holoMaterial(1,"models/debug/debugwhite")
holoColor(1,vec(1,1,1)*60)
holoScale(1,vec(1,1,3)*1.5)

holoModel(2,"hqcylinder")
holoPos(2,Pod:toWorld(vec(0,-12,0)))
holoAng(2,Pod:toWorld(ang(90,0,0)))
holoMaterial(2,"models/debug/debugwhite")
holoColor(2,vec(1,1,1)*60)
holoScale(2,vec(1,1,3)*1.5)

holoModel(3,"models/props_phx/misc/flakshell_big.mdl")
holoPos(3,Pod:toWorld(vec(20,12,0)))
holoAng(3,Pod:toWorld(ang(90,0,0)))
holoScale(3,vec(1,1,0.5)*0.65)

holoModel(4,"models/props_phx/misc/flakshell_big.mdl")
holoPos(4,Pod:toWorld(vec(20,-12,0)))
holoAng(4,Pod:toWorld(ang(90,0,0)))
holoScale(4,vec(1,1,0.5)*0.65)
    
}
Target = owner():aimPos()
#if(Fire && changed(Fire)){Tube++}
Tube = Loaded1 + Loaded2
if(Fire && Tube == 2){Fire1 = 1}
if(Fire && Tube == 1){Fire2 = 1}

#if(Fire && Loaded1){Fire1 = Fire}
#elseif(Fire && Loaded1 == 0 && Loaded2 == 1){Fire2 = Fire}
if(Fire == 0){
Fire1 = 0
Fire2 = 0    
}

#Missile 1
if(Missile1:pos() == vec()){
    Missile1:removeTrails()
    holoAlpha(3,255)
Seek1 = 0
#if(Loaded1 == 0){Tube--}
Loaded1 = 1 
   
}
if(Fire1 && Loaded1){
Missile1 = propSpawn("models/props_phx/misc/potato_launcher_explosive.mdl",Pod:toWorld(vec(20,12,0)),Pod:angles(),0)    
Missile1:setTrails(50,50,10,"trails/smoke",vec(1,1,1)*255,255)

holoAlpha(3,0)
timer("Seek1",500)
Loaded1 = 0    
}
if(clk("Seek1")){
Seek1 = 1   
}

if(Seek1 == 1){
Missile1:applyForce((Target - Missile1:pos()):normalized()*Missile1:mass()*2500)    
#Missile1:setAng((Target - Missile1:pos()):toAngle())
M1Ang = (Target - Missile1:pos()):toAngle()
M1V = Missile1:toLocal(rotationVector(quat(M1Ang)/quat(Missile1))+Missile1:pos())
Missile1:applyTorque((150*M1V - 15*Missile1:angVelVector())*Missile1:inertia()*4) 
}
Missile1:applyForce(Missile1:forward()*Missile1:mass()*1000)


#Missile 2
if(Missile2:pos() == vec()){
    Missile2:removeTrails()
    holoAlpha(4,255)
Seek2 = 0
#if(Loaded2 == 0){Tube--}
Loaded2 = 1 
   
}
if(Fire2 && Loaded2){
Missile2 = propSpawn("models/props_phx/misc/potato_launcher_explosive.mdl",Pod:toWorld(vec(20,12,0)),Pod:angles(),0)    
Missile2:setTrails(50,50,10,"trails/smoke",vec(1,1,1)*255,255)

holoAlpha(4,0)
timer("Seek2",500)
Loaded2 = 0    
}
if(clk("Seek2")){
Seek2 = 1   
}

if(Seek2 == 1){
Missile2:applyForce((Target - Missile2:pos()):normalized()*Missile2:mass()*2500)    
#Missile2:setAng((Target - Missile1:pos()):toAngle())
M2Ang = (Target - Missile2:pos()):toAngle()
M2V = Missile2:toLocal(rotationVector(quat(M2Ang)/quat(Missile2))+Missile2:pos())
Missile2:applyTorque((150*M2V - 15*Missile2:angVelVector())*Missile2:inertia()*4) 

}
Missile2:applyForce(Missile2:forward()*Missile2:mass()*1000)

