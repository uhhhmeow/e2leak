@name 
@inputs [Hip Foot]:entity Offset:entity
@outputs 
@persist 
interval(10) 

if(first()){
    for(I=1,20){
holoCreate(I)
holoModel(I,"cone")

holoAng(I,ang(0,0,0))
holoMaterial(I,"Phoenix_storms/mat/mat_phx_metallic")
holoColor(I,vec(51,51,51))
holoParent(I,entity())
}
}

P0 = Hip:pos()

P2 = Foot:pos()
P1 = Offset:pos()
for(I=1,20){
    T = I/20
One = ((1-T)^2)*P0
Two = 2*(1-T)*T*P1
Three = (T^2)*P2
Bezier = One+Two+Three
holoScale(20 - I+ 1,vec(5,5,5) + vec(2,2,2)*T*2)
holoPos(I,Bezier)

Aimang = (Bezier - holoEntity(I-1):pos()):toAngle()
Aimang = angnorm(Aimang) + ang(90,0,0)
holoAng(I,Aimang)
holoAng(1,ang(0,0,0))
}
