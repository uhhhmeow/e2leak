@name HKQuadmech Holograms
@inputs [Torso Hip FL FR RL RR]:entity [FLHippos FRHippos RLHippos RRHippos Color]:vector Material:string
@outputs Bob
@persist ThighLength MidLength ShinLength Bob
interval(50)


if(first()){   
ThighLength = 5*12 # actual shin
MidLength = 2*12 # Upper leg
ShinLength = 4*12 # Mid section
}
Bob+=3
ThighLength = sin(Bob)*2 + 60

Grey = vec(1,1,1)*50
GreyMetal = vec(49,49,60)
PhxMetallic = "Phoenix_storms/mat/mat_phx_metallic"
#MatteMetal = "sprops/trans/wheels/sprops_rim"

function holoData (Index,Model:string,Scale:vector,Pos:vector,Ang:angle,Material:string,Color:vector,Parent:entity){
    holoCreate(Index)
    holoModel(Index,Model)
    holoScale(Index,Scale)
    holoPos(Index,Pos)
    holoAng(Index,Ang)
    holoMaterial(Index,Material)
    holoColor(Index,Color)
    holoParent(Index,Parent)
}

function number rotationDifference(Out, Out2){
    if((Out - Out2) > 180){Out1b = -(360 - (Out))}else{Out1b = Out}
    if((Out2 - Out) > 180){Out2b = -(360 - (Out2))}else{Out2b = Out2}
    return Out2b - Out1b    
}


function makeLeg3(StartIndex){   
    holoData(StartIndex,"cube",vec(1,1,1),Hip:toWorld(vec(0,0,20)),Hip:toWorld(ang(90,0,0)),"",vec(1,0.25,0.25)*255,Hip) # Main Hypotenuse
    holoData(StartIndex+1,"cube",vec(1,1,1),Hip:toWorld(vec(0,0,20)),Hip:toWorld(ang(90,0,0)),"",vec(1,0.25,0.25)*255,holoEntity(StartIndex)) # Main Hypotenuse
    holoData(StartIndex+2,"cube",vec(1,1,1)*0.3,holoEntity(StartIndex+1):toWorld(vec(0,0,20)),holoEntity(StartIndex+1):toWorld(ang()),"",vec(0,1,0)*255,holoEntity(StartIndex+1)) # Hip Joint
    
    holoData(StartIndex+3,"cube",vec(1,1,1),holoEntity(StartIndex+2):toWorld(vec(0,0,0)),holoEntity(StartIndex+2):toWorld(ang(0,0,0)),"",vec(0,0,1)*255,holoEntity(StartIndex+2)) # Second Hypotenuse (False Thigh)
    
    #shin
    holoData(StartIndex+4,"hqsphere",vec(1,1,1)*0,holoEntity(StartIndex+3):toWorld(vec(0,0,0)),holoEntity(StartIndex+3):toWorld(ang()),"models/shiny",vec(1,1,1)*75,holoEntity(StartIndex+3)) # Second Hip Joint
    holoData(StartIndex+5,"cube",vec(ThighLength/12,1,1)*0,holoEntity(StartIndex+4):toWorld(vec(-ThighLength/2,0,0)),holoEntity(StartIndex+4):toWorld(ang(0,0,0)),"",vec(1,0.25,0.25)*255,holoEntity(StartIndex+4)) # true Thigh
    
    #mid
    holoData(StartIndex+8,"hqsphere",vec(1,1,1)*1.3*0,holoEntity(StartIndex+4):toWorld(vec(0,0,0)),holoEntity(StartIndex+4):toWorld(ang()),"models/shiny",vec(1,1,1)*55,holoEntity(StartIndex+4)) # Ankle Joint
    holoData(StartIndex+9,"cube",vec(ShinLength/12,1,1)*0,holoEntity(StartIndex+8):toWorld(vec(ShinLength/2,0,0)),holoEntity(StartIndex+8):toWorld(ang(0,0,0)),"",vec(1,0.25,0.25)*255,holoEntity(StartIndex+8)) # true Shin
    
    #thigh
    holoData(StartIndex+6,"hqsphere",vec(1,1,1)*1.25,holoEntity(StartIndex+8):toWorld(vec(ShinLength,0,0)),holoEntity(StartIndex+8):toWorld(ang()),"models/shiny",vec(1,1,1)*55,holoEntity(StartIndex+8)) # True Knee Joint
    #holoData(StartIndex+7,"cube",vec(MidLength/12,1,1),holoEntity(StartIndex+6):toWorld(vec(MidLength/2,0,0)),holoEntity(StartIndex+6):toWorld(ang(0,0,0)),"",vec(1,0.25,0.25)*255,holoEntity(StartIndex+6)) # true Midleg

}
function makeLeg3Details(StartIndex){
"models/props_wasteland/light_spotlight01_lamp.mdl" 
holoData(StartIndex+10,"models/props_wasteland/light_spotlight01_lamp.mdl",vec(0.75,1,1)*0.85,holoEntity(StartIndex+4):toWorld(vec(-ThighLength,0,7.5)),holoEntity(StartIndex+4):toWorld(ang(0,0,180)),PhxMetallic,GreyMetal,holoEntity(StartIndex+4))
holoData(StartIndex+11,"models/props_wasteland/light_spotlight01_lamp.mdl",vec(0.75,1,1)*0.825,holoEntity(StartIndex+10):toWorld(vec(15,0,0)),holoEntity(StartIndex+10):toWorld(ang(0,180,0)),PhxMetallic,GreyMetal,holoEntity(StartIndex+10))
holoData(StartIndex+12,"models/Items/combine_rifle_ammo01.mdl",vec(1,1,1)*1.5,holoEntity(StartIndex+10):toWorld(vec(20,0,3)),holoEntity(StartIndex+10):toWorld(ang(90,0,0)),PhxMetallic,GreyMetal,holoEntity(StartIndex+10))

holoData(StartIndex+13,"hqcylinder",vec(0.5,0.5,5)*1,holoEntity(StartIndex+4):toWorld(vec(-ThighLength/2 + 5,0,0)),holoEntity(StartIndex+4):toWorld(ang(90,0,0)),"models/shiny",vec(1,1,1)*100,holoEntity(StartIndex+4))
holoData(StartIndex+14,"models/props_junk/PopCan01a.mdl",vec(1,1,0.75)*2,holoEntity(StartIndex+4):toWorld(vec(0,0,0)),holoEntity(StartIndex+4):toWorld(ang(90,0,0)),PhxMetallic,GreyMetal,holoEntity(StartIndex+4))

holoData(StartIndex+15,"models/NatesWheel/nateswheelwide.mdl",vec(1,2,1)*0.25,holoEntity(StartIndex+8):toWorld(vec(0,0,0)),holoEntity(StartIndex+8):toWorld(ang(0,90,0)),PhxMetallic,GreyMetal,holoEntity(StartIndex+8))
holoData(StartIndex+16,"models/Gibs/Scanner_gib01.mdl",vec(0.75,1,1)*2,holoEntity(StartIndex+8):toWorld(vec(20,0,-2)),holoEntity(StartIndex+8):toWorld(ang(-10,180,0)),PhxMetallic,GreyMetal,holoEntity(StartIndex+8))
holoData(StartIndex+17,"models/Gibs/Scanner_gib01.mdl",vec(0.5,1,1)*2,holoEntity(StartIndex+8):toWorld(vec(27.5,0,2)),holoEntity(StartIndex+8):toWorld(ang(10,0,180)),PhxMetallic,GreyMetal,holoEntity(StartIndex+8))
holoData(StartIndex+18,"hqsphere",vec(1,1,1)*1.5,holoEntity(StartIndex+6):toWorld(vec(27.5,0,2)),holoEntity(StartIndex+6):toWorld(ang(10,0,180)),PhxMetallic,GreyMetal,holoEntity(StartIndex+6))
holoData(StartIndex+19,"models/props_junk/TrashBin01a.mdl",vec(1,1,1)*0.5,holoEntity(StartIndex+6):toWorld(vec(12.5,0,2)),holoEntity(StartIndex+6):toWorld(ang(90,0,0)),PhxMetallic,GreyMetal,holoEntity(StartIndex+6))

}

function updateColMat(Index){
    holoColor(Index,Color)    
    holoMaterial(Index,Material)    
}

function operateLeg3(StartIndex,HPos:vector,F:vector){
    Avrgpoint = (HPos + (F+vec(0,0,0)))/2
    Hypelength = clamp((HPos - (F+vec(0,0,0))):length(),MidLength*1.25,ThighLength+MidLength+ShinLength)
    HypeAngle = (HPos - (F+vec(0,0,0))):toAngle() + ang(90,0,0)
    #holoScale(StartIndex+5,vec(ThighLength/12,1,1)*1)
    #holoScale(StartIndex+9,vec(ShinLength/12,1,1)*1)
    #holoScale(StartIndex+7,vec(MidLength/12,1,1)*1)
    holoPos(StartIndex+10,holoEntity(StartIndex+4):toWorld(vec(-ThighLength,0,3)))
    holoPos(StartIndex,Avrgpoint)
    holoScale(StartIndex,vec(0.1,0.1,Hypelength/12)*0) # scale of Hype Prime
    holoAng(StartIndex,HypeAngle) # Angle of Hype- Prime    
    
    holoPos(StartIndex+1,Avrgpoint) # 
    holoScale(StartIndex+1,vec(0.5,1,Hypelength/15)*0) # Stuff to do with Hype Secundus
    holoAng(StartIndex+1,holoEntity(StartIndex):toWorld(ang(0,0,0)))
    
    Hypotenuse = Hypelength
    
    SecondHypotenuse = sqrt( MidLength^2 + ThighLength^2 - 2*MidLength*ThighLength*cos(160) )
    
    holoScale(StartIndex+3,vec(SecondHypotenuse/12,0.25,0.25)*0) # true Thigh
    holoPos(StartIndex+2,holoEntity(StartIndex+1):toWorld(vec(0,0,-Hypotenuse/2))) # false hip
    
    BodyAng = acos((SecondHypotenuse^2 + Hypotenuse^2 - ShinLength^2)/(2*SecondHypotenuse*Hypotenuse)) - 90
    holoAng(StartIndex+2,holoEntity(StartIndex+1):toWorld(ang(-BodyAng,0,0))) # false hip
    
    holoPos(StartIndex+3,holoEntity(StartIndex+2):toWorld(vec(-SecondHypotenuse/2,0,0))) # position the secondary hypotenuse
    
    holoPos(StartIndex+8,holoEntity(StartIndex+4):toWorld(vec(-ThighLength,0,0)))
    KneeAng = acos((SecondHypotenuse^2 + ShinLength^2 - Hypotenuse^2)/(2*SecondHypotenuse*ShinLength))
    holoAng(StartIndex+8,holoEntity(StartIndex+2):toWorld(ang(-KneeAng,0,0))) # ankle
    
    holoPos(StartIndex+4,holoEntity(StartIndex+3):toWorld(vec(SecondHypotenuse/2,0,0))) # position the True Hip
    FalseBodyAng = acos(((ThighLength)^2 + SecondHypotenuse^2 - MidLength^2)/(2*(ThighLength)*SecondHypotenuse))
    holoAng(StartIndex+4,holoEntity(StartIndex+3):toWorld(ang(-FalseBodyAng,0,0))) # true hip
    
    FalseKneeAng = acos(((ThighLength)^2 + MidLength^2 - SecondHypotenuse^2)/(2*(ThighLength)*MidLength))
    holoAng(StartIndex+6,holoEntity(StartIndex+4):toWorld(ang(-FalseKneeAng,0,0))) # true knee
}

if(first()){    
timer("Hip",1200*0)
timer("Torso",1200*1)    
timer("FL",1200*2)    
timer("FLDetails",1200*3)    
timer("FR",1200*4)
timer("FRDetails",1200*5)
timer("RL",1200*6)
timer("RLDetails",1200*7)
timer("RR",1200*8)   
timer("RRDetails",1200*9) 

 
timer("final",1200*10) 
}

if(clk("FL")){makeLeg3(0)}
if(clk("FLDetails")){makeLeg3Details(0)}
if(clk("FR")){makeLeg3(20)}
if(clk("FRDetails")){makeLeg3Details(20)}
if(clk("RL")){makeLeg3(40)}
if(clk("RLDetails")){makeLeg3Details(40)}
if(clk("RR")){makeLeg3(60)}
if(clk("RRDetails")){makeLeg3Details(60)}

operateLeg3(0,Hip:toWorld(vec(10,10,0)),FL:pos())
operateLeg3(20,Hip:toWorld(vec(10,-10,0)),FR:pos())
operateLeg3(40,Hip:toWorld(vec(-10,10,0)),RL:pos())
operateLeg3(60,Hip:toWorld(vec(-10,-10,0)),RR:pos())


if(clk("Hip")){
    holoData(80,"models/props_interiors/Furniture_Couch02a.mdl",vec(1,1,1)*0.5,Hip:toWorld(vec(10,10,5)),Hip:toWorld(ang(90,45,0)),PhxMetallic,GreyMetal,Hip)    
    holoData(81,"models/props_interiors/Furniture_Couch02a.mdl",vec(1,1,1)*0.5,Hip:toWorld(vec(10,-10,5)),Hip:toWorld(ang(90,-45,0)),PhxMetallic,GreyMetal,Hip)    
    holoData(82,"models/props_interiors/Furniture_Couch02a.mdl",vec(1,1,1)*0.5,Hip:toWorld(vec(-10,10,5)),Hip:toWorld(ang(90,135,0)),PhxMetallic,GreyMetal,Hip)    
    holoData(83,"models/props_interiors/Furniture_Couch02a.mdl",vec(1,1,1)*0.5,Hip:toWorld(vec(-10,-10,5)),Hip:toWorld(ang(90,-135,0)),PhxMetallic,GreyMetal,Hip)    
    holoData(84,"models/props_phx/wheels/magnetic_large_base.mdl",vec(1,1,1)*1,Hip:toWorld(vec(0,0,0)),Hip:toWorld(ang(0,0,0)),PhxMetallic,GreyMetal,Hip)    
}

if(clk("Torso")){
#models/props_combine/combine_emitter01.mdl    
    holoData(90,"models/props_combine/combine_smallmonitor001.mdl",vec(1,1,1)*1.25,Torso:toWorld(vec(0,20,0)),Torso:toWorld(ang(0,-90,0)),PhxMetallic,GreyMetal,Torso)    
    holoData(91,"models/props_combine/combine_smallmonitor001.mdl",vec(1,1,1)*1.25,Torso:toWorld(vec(3,-20,0)),Torso:toWorld(ang(0,90,0)),PhxMetallic,GreyMetal,Torso)    
    holoData(92,"models/props_combine/combine_emitter01.mdl",vec(1,1,1)*1,Torso:toWorld(vec(0,0,0)),Torso:toWorld(ang(90,0,0)),PhxMetallic,GreyMetal,Torso)    
    holoData(93,"models/props_combine/combine_emitter01.mdl",vec(1,1,1)*1,Torso:toWorld(vec(0,0,0)),Torso:toWorld(ang(90,180,0)),PhxMetallic,GreyMetal,Torso)    

    #holoData(94,"models/props_combine/combine_emitter01.mdl",vec(1,1,1)*1,AR:toWorld(vec(0,0,0)),AR:toWorld(ang(90,180,0)),PhxMetallic,GreyMetal,AR)    

}


if(clk("final")){
    for(I=1,100){
        #holoVisible(I,players(),1)    
    }    
}
