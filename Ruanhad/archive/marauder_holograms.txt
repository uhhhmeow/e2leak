@name Marauder Holograms
@inputs [Torso FL FR AL AR]:entity
@outputs 
@persist Hip:entity Inc Index
interval(50)

if(first()){
    Hip = Torso
timer("FL",0)    
timer("FR",1200*1)     
timer("Torso1",1200*2)     
timer("Torso2",1200*3)     
timer("Torso3",1200*4)     
timer("Feet",1200*5)     
timer("Autocannon",1200*6)     
timer("Missilepod",1200*7)     
}


PhxMetallic = "Phoenix_storms/mat/mat_phx_metallic"
Shiny = "models/shiny"
Grey = vec(1,1,1)*50
PaleGrey = vec(1,1,1)*100
Thighlength = 3.5*12
Shinlength = 3*12

function holodata (Index,Model:string,Scale:vector,Pos:vector,Ang:angle,Mat:string,Color:vector,Parent:entity){
#    holoCreate(Index)
    holoModel(Index,Model)
    holoScale(Index,Scale)
    holoPos(Index,Pos)
    holoAng(Index,Ang)
    holoMaterial(Index,Mat)
    holoColor(Index,Color)
    holoParent(Index,Parent)
    
}

function vector scaleConvert(In:vector){
    local OutVec = vec(In:y(),In:x(),In:z())
    return OutVec
}

function makeLeg(StartIndex,MFHip:entity, Foot:entity, TL,SL){
I=StartIndex
while(I < StartIndex + 6){
holoCreate(I)
I++   
}
holodata(StartIndex,"hqsphere",vec(1,1,1)*2,vec(),ang(),Shiny,PaleGrey,MFHip) # Hip Joint
holodata(StartIndex+1,"cube",vec(1,1,1)*0.1,vec(),ang(),PhxMetallic,Grey,MFHip) # True Hypotenuse
holodata(StartIndex+2,"cube",vec(1,1,1)*0.1,vec(),ang(),PhxMetallic,Grey,holoEntity(StartIndex+1)) # Fake Hypotenuse
holodata(StartIndex+3,"hqsphere",vec(1,1,1)*1.5,holoEntity(StartIndex):toWorld(vec(TL,0,0)),holoEntity(StartIndex):toWorld(ang(0,0,0)),PhxMetallic,Grey,holoEntity(StartIndex)) # Knee joint    
holodata(StartIndex+4,"models/props_junk/TrashBin01a.mdl",vec(1,1,1)*1.25,holoEntity(StartIndex):toWorld(vec(TL/2,0,0)),holoEntity(StartIndex):toWorld(ang(-90,0,0)),PhxMetallic,Grey,holoEntity(StartIndex))
holodata(StartIndex+5,"models/props_junk/garbage_milkcarton001a.mdl",vec(1,1,1)*3,holoEntity(StartIndex+3):toWorld(vec(-SL/2,0,0)),holoEntity(StartIndex+3):toWorld(ang(0,90,90)),PhxMetallic,Grey,holoEntity(StartIndex+3))
}
function operateLeg(StartIndex,FHip:entity,Foot:entity,TL,SL,Hippos:vector,BipedToggle){
Avrgpoint = (Hippos + (Foot:pos()+vec(0,0,0)))/2
Hypelength = clamp((Hippos - (Foot:pos()+vec(0,0,0))):length(),0,TL+SL)
HypeAngle = (Hippos - (Foot:pos()+vec(0,0,0))):toAngle() + ang(90,0,0)

Out = FHip:angles():yaw()
Out2 = HypeAngle:yaw()
if((Out - Out2) > 180){Out1b = -(360 - (Out))}else{Out1b = Out}
if((Out2 - Out) > 180){Out2b = -(360 - (Out2))}else{Out2b = Out2}
Out3 = Out2b - Out1b

#LCorAngle = LHypeAngle - ang(0,Out3,0)
holoScale(StartIndex,vec(1,1,1)*2)

holoPos(StartIndex+1,Avrgpoint)
holoScale(StartIndex+1,vec(0.1,0.1,Hypelength/12)*0)
holoAng(StartIndex+1,HypeAngle)

holoPos(StartIndex+2,Avrgpoint)
holoScale(StartIndex+2,vec(0.5,1,Hypelength/15)*0)
holoAng(StartIndex+2,holoEntity(StartIndex+1):toWorld(ang(0,-Out3*BipedToggle+(!BipedToggle*180),0)))

holoPos(StartIndex,holoEntity(StartIndex+2):toWorld(vec(0,0,Hypelength/2)))

BodyAng = acos((TL^2 + Hypelength^2 - SL^2)/(2*TL*Hypelength)) - 90
holoAng(StartIndex,holoEntity(StartIndex+2):toWorld(ang(-BodyAng,0,0)))
KneeAng = acos((TL^2 + SL^2 - Hypelength^2)/(2*TL*SL))
holoAng(StartIndex+3,holoEntity(StartIndex):toWorld(ang(-KneeAng,0,0)))

}

if(clk("FL")){makeLeg(1,Hip,FL,Thighlength,Shinlength)}
if(clk("FR")){makeLeg(7,Hip,FR,Thighlength,Shinlength)}

Inc++
Z = -45

    Hipoffset = Hip:toWorld(vec(-10,-35,Z))
operateLeg(1,Hip,FL,Thighlength,Shinlength,Hipoffset,1)
    Hipoffset = Hip:toWorld(vec(-10,35,Z))
operateLeg(7,Hip,FR,Thighlength,Shinlength,Hipoffset,1)

if(clk("Torso1")){
    Index = 13
    for(I=Index,Index+9){
    holoCreate(I)
}
holodata(Index,"models/props_phx/wheels/magnetic_large.mdl",vec(1,1,1)*0.65,Torso:toWorld(vec(27.5,0,0)),Torso:toWorld(ang(90,0,0)),PhxMetallic,Grey,Torso)    
holodata(Index+1,"models/props_phx/wheels/magnetic_large.mdl",vec(1,1,1)*0.75,holoEntity(13):toWorld(vec(0,0,2.5)),holoEntity(13):toWorld(ang(0,0,0)),PhxMetallic,Grey,Torso)    

holodata(Index+2,"models/props_c17/canister_propane01a.mdl",scaleConvert(vec(1,1,1.75))*0.5,Torso:toWorld(vec(-60,35,-20)),Torso:toWorld(ang(75,0,0)),PhxMetallic,Grey,Torso)    
holodata(Index+3,"models/props_c17/canister_propane01a.mdl",scaleConvert(vec(1,1,1.75))*0.5,Torso:toWorld(vec(-60,-35,-20)),Torso:toWorld(ang(75,0,0)),PhxMetallic,Grey,Torso)    

holodata(Index+4,"models/props_c17/canister_propane01a.mdl",scaleConvert(vec(1,1,1.75))*0.5,Torso:toWorld(vec(-65,35,10)),Torso:toWorld(ang(95,0,0)),PhxMetallic,Grey,Torso)    
holodata(Index+5,"models/props_c17/canister_propane01a.mdl",scaleConvert(vec(1,1,1.75))*0.5,Torso:toWorld(vec(-65,-35,10)),Torso:toWorld(ang(95,0,0)),PhxMetallic,Grey,Torso)  

holodata(Index+6,"models/props_c17/canister_propane01a.mdl",scaleConvert(vec(1,1,1.75))*0.5,Torso:toWorld(vec(-55,35,47.5)),Torso:toWorld(ang(125,0,0)),PhxMetallic,Grey,Torso)    
holodata(Index+7,"models/props_c17/canister_propane01a.mdl",scaleConvert(vec(1,1,1.75))*0.5,Torso:toWorld(vec(-55,-35,47.5)),Torso:toWorld(ang(125,0,0)),PhxMetallic,Grey,Torso)  

holodata(Index+8,"models/props_wasteland/laundry_washer001a.mdl",scaleConvert(vec(1,1,0.35))*0.5,Torso:toWorld(vec(-35,-2.5,0)),Torso:toWorld(ang(90,180,0)),PhxMetallic,Grey,Torso)  
holodata(Index+9,"models/props_interiors/BathTub01a.mdl",scaleConvert(vec(0.8,1,1))*1,Torso:toWorld(vec(0,-6,-40)),Torso:toWorld(ang(0,0,0)),PhxMetallic,Grey,Torso)  


}

if(clk("Torso2")){
Index = 13+10
for(I=Index,Index+9){
    holoCreate(I)
}
holodata(Index,"models/props_junk/TrashBin01a.mdl",scaleConvert(vec(0.75,1,1))*0.75,Torso:toWorld(vec(32.5,0,-35)),Torso:toWorld(ang(20,0,0)),PhxMetallic,Grey,Torso)  
holodata(Index+1,"models/props_interiors/Furniture_Couch02a.mdl",vec(1,1,1)*1,Torso:toWorld(vec(-20,0,15)),Torso:toWorld(ang(90,0,0)),PhxMetallic,Grey,Torso)  
holodata(Index+2,"models/props_interiors/Furniture_Couch02a.mdl",vec(1,1,1)*1,Torso:toWorld(vec(-20,0,-20)),Torso:toWorld(ang(-90,180,0)),PhxMetallic,Grey,Torso)  
holodata(Index+3,"models/props_docks/dock01_cleat01a.mdl",vec(1,1,1)*1.25,Torso:toWorld(vec(20,0,-20)),Torso:toWorld(ang(0,90,90)),PhxMetallic,Grey,Torso)  
holodata(Index+4,"models/XQM/wingpiece1.mdl",scaleConvert(vec(1,0.75,1))*0.75,Torso:toWorld(vec(20,0,-15)),Torso:toWorld(ang(0,90,-30)),Shiny,vec(1,1,1)*75,Torso)  

holodata(Index+5,"models/props_combine/headcrabcannister01a.mdl",scaleConvert(vec(1,3,1))*0.45,Torso:toWorld(vec(0,22.5,-20)),Torso:toWorld(ang(105,90,0)),PhxMetallic,Grey,Torso)  
holodata(Index+6,"models/props_combine/headcrabcannister01a.mdl",scaleConvert(vec(1,3,1))*0.45,Torso:toWorld(vec(0,-22.5,-20)),Torso:toWorld(ang(105,-90,0)),PhxMetallic,Grey,Torso)  

holodata(Index+7,"models/props_lab/tpplug.mdl",scaleConvert(vec(1.5,0.5,1))*3.5,Torso:toWorld(vec(0,-30,5)),Torso:toWorld(ang(0,-90,0)),PhxMetallic,Grey,Torso)  
holodata(Index+8,"models/props_lab/tpplug.mdl",scaleConvert(vec(1.5,0.5,1))*3.5,Torso:toWorld(vec(0,30,5)),Torso:toWorld(ang(0,90,0)),PhxMetallic,Grey,Torso)  

holodata(Index+9,"models/props_canal/boat002b.mdl",scaleConvert(vec(0.5,1,0.75))*1,Torso:toWorld(vec(0,0,30)),Torso:toWorld(ang(0,0,180)),PhxMetallic,Grey,Torso)  
 
}

if(clk("Torso3")){
Index = 33
for(I=Index,Index+9){
    holoCreate(I)
}    
holodata(Index,"models/props_lab/teleportring.mdl",scaleConvert(vec(1,1,2))*0.9,Torso:toWorld(vec(10,0,0)),Torso:toWorld(ang(-90,0,0)),"",vec(1,1,1)*255,Torso)  
holodata(Index+1,"models/props_lab/teleportring.mdl",scaleConvert(vec(1,1,2))*0.7,Torso:toWorld(vec(20,0,-3)),Torso:toWorld(ang(-90,0,0)),"",vec(1,1,1)*255,Torso)  
holodata(Index+2,"models/props_lab/teleportring.mdl",scaleConvert(vec(1,1,1.5))*0.75,Torso:toWorld(vec(35,0,0)),Torso:toWorld(ang(-90,0,0)),PhxMetallic,Grey,Torso)  
holodata(Index+3,"models/props_phx/wheels/magnetic_large.mdl",scaleConvert(vec(1,1.5,1))*0.95,Torso:toWorld(vec(30,0,0)),Torso:toWorld(ang(-90,0,0)),PhxMetallic,Grey,Torso)  

holodata(Index+4,"models/props_interiors/Furniture_Couch02a.mdl",scaleConvert(vec(0.75,1.5,1))*0.75,Torso:toWorld(vec(0,25,15)),Torso:toWorld(ang(70,-90,0)),PhxMetallic,Grey,Torso)  
holodata(Index+5,"models/props_interiors/Furniture_Couch02a.mdl",scaleConvert(vec(0.75,1.5,1))*0.75,Torso:toWorld(vec(0,-25,15)),Torso:toWorld(ang(70,90,0)),PhxMetallic,Grey,Torso)  
holodata(Index+6,"models/props_interiors/Furniture_Couch02a.mdl",scaleConvert(vec(0.75,1.5,1))*0.75,Torso:toWorld(vec(0,-25,15)),Torso:toWorld(ang(70,90,0)),PhxMetallic,Grey,Torso)  

holodata(Index+7,"models/props_combine/headcrabcannister01a.mdl",scaleConvert(vec(0.5,1.5,1.5))*0.5,Torso:toWorld(vec(-10,-30,-25)),Torso:toWorld(ang(-85,90,0)),PhxMetallic,Grey,Torso)  
holodata(Index+8,"models/props_combine/headcrabcannister01a.mdl",scaleConvert(vec(0.5,1.5,1.5))*0.5,Torso:toWorld(vec(-10,30,-25)),Torso:toWorld(ang(-85,-90,0)),PhxMetallic,Grey,Torso)  

holodata(Index+9,"models/props_c17/clock01.mdl",vec(1,1,1)*2.5,Torso:toWorld(vec(27.5,0,0)),Torso:toWorld(ang(90,0,0)),"Phoenix_storms/glass",vec(1,0,0)*255,Torso)
#holoAlpha(42,255)  
}

if(clk("Feet")){
Index = 43
for(I=Index,Index+1){
    holoCreate(I)
}  
holodata(Index,"models/props_lab/monitor01b.mdl",vec(1,1,1)*2,FL:toWorld(vec(20,0,0)),FL:toWorld(ang(0,180,0)),PhxMetallic,Grey,FL)  
holodata(Index+1,"models/props_lab/monitor01b.mdl",vec(1,1,1)*2,FR:toWorld(vec(20,0,0)),FR:toWorld(ang(0,180,0)),PhxMetallic,Grey,FR)  
}
if(clk("Autocannon")){
Index = 45
for(I=Index,Index+10){
    holoCreate(I)
}  
holodata(Index,"models/props_junk/TrashBin01a.mdl",scaleConvert(vec(1,0.75,1.25))*1,AL:toWorld(vec(24,0,-10)),AL:toWorld(ang(-90,0,0)),PhxMetallic,Grey,AL)   
holodata(Index+1,"models/props_lab/jar01a.mdl",scaleConvert(vec(1,1,1.5))*1.25,AL:toWorld(vec(60,0,-6)),AL:toWorld(ang(90,0,0)),PhxMetallic,Grey,AL)   
holodata(Index+2,"models/props_lab/jar01a.mdl",scaleConvert(vec(1,1,4))*0.45,AL:toWorld(vec(60,0,-16)),AL:toWorld(ang(90,0,0)),PhxMetallic,Grey,AL)   
holodata(Index+3,"models/props_lab/citizenradio.mdl",vec(1,1,1)*0.75,AL:toWorld(vec(15,0,-1)),AL:toWorld(ang(0,0,0)),PhxMetallic,Grey,AL)   
holodata(Index+4,"models/props_junk/TrashBin01a.mdl",vec(1,1,1)*1,AL:toWorld(vec(0,0,0)),AL:toWorld(ang(0,-90,0)),PhxMetallic,Grey,AL)   
holodata(Index+5,"models/props_junk/TrashDumpster01a.mdl",vec(1,1,1)*0.35,AL:toWorld(vec(0,-10,-10)),AL:toWorld(ang(-90,-90,0)),PhxMetallic,Grey,AL)   
}
if(clk("Missilepod")){
Index = 55
for(I=Index,Index+10){
    holoCreate(I)
} 
holodata(Index,"models/props_junk/TrashBin01a.mdl",scaleConvert(vec(1,2,1))*1,AR:toWorld(vec(0,5,0)),AR:toWorld(ang(0,90,0)),PhxMetallic,Grey,AR)   
holodata(Index+1,"hq_stube_thin",scaleConvert(vec(1,1.5,0.25))*3*0.7,AR:toWorld(vec(30,0,0)*0.75),AR:toWorld(ang(90,0,0)),PhxMetallic,Grey,AR)   
holodata(Index+2,"models/props_c17/substation_transformer01b.mdl",vec(1,1,1)*0.2*0.75,AR:toWorld(vec(30,0,-15)*0.75),AR:toWorld(ang(0,0,0)),PhxMetallic,Grey,AR)   
holodata(Index+3,"models/props_c17/substation_transformer01b.mdl",vec(1,1,1)*0.2*0.75,AR:toWorld(vec(30,0,0)*0.75),AR:toWorld(ang(0,0,0)),PhxMetallic,Grey,AR)   
holodata(Index+4,"models/props_c17/substation_transformer01b.mdl",vec(1,1,1)*0.2*0.75,AR:toWorld(vec(30,0,15)*0.75),AR:toWorld(ang(0,0,0)),PhxMetallic,Grey,AR)   
holodata(Index+5,"hq_rcube",scaleConvert(vec(2,3,4))*1,AR:toWorld(vec(3,0,0)),AR:toWorld(ang(0,0,0)),PhxMetallic,Grey,AR) 
}
