@name Mad Mossman
@inputs Controls:wirelink Camera:wirelink Ragdoll:entity
@outputs [F1Toepos F2Toepos]:vector
@persist [Avrgps Cpos Hpos Hippos F1footgo F2footgo Lhandpos Rhandpos]:vector Inc Foot Hipang Zoom FlapInc
interval(10)
function bone:moveTo(Tarpos:vector){
    This:applyForce(((Tarpos-This:pos())*15-This:vel())*(This:mass()))    
}

Head = Ragdoll:bone(10)
Chest = Ragdoll:bone(1)
Hip = Ragdoll:bone(0)

Lhand = Ragdoll:bone(7)
Rhand = Ragdoll:bone(5)

Lfoot = Ragdoll:bone(14)
Rfoot = Ragdoll:bone(13)

F1Toe = Lfoot:pos()
F2Toe = Rfoot:pos()
#start controls
W = Controls:number("W")
A = Controls:number("A")
S = Controls:number("S")
D = Controls:number("D")
M1 = Controls:number("Mouse1")
M2 = Controls:number("Mouse2")
Space = Controls:number("Space")
Shift = Controls:number("Shift")#
Mwheelup = Controls:number("NextWeapon")
Mwheeldown = Controls:number("PrevWeapon")
Active = Controls:number("Active")
Seat = Controls:entity("Entity")
#end controls




#end controls
if(first()){
F1footgo = Hip:toWorld(vec(0,-5,-30))
F2footgo = Hip:toWorld(vec(0,5,-30))
R=entity():getConstraints() 
Zoom = 300
Foot = 0
F = 1
timer("quack",500)
}

if(Mwheelup){Zoom = Zoom + 10}
if(Mwheeldown){Zoom = Zoom - 10}
Camvec = Seat:driver():eye()
Campos = Head:pos() + (Camvec*-Zoom)
#start controls
Camera:setNumber("Activated",Active)
Camera:setVector("Position",Campos)
Camera:setVector("Direction",Camvec)

if(Active){
if(changed(Active)){
    reset()
F1footgo = F1Toe
F2footgo = F2Toe
}
Avrgps = (Lfoot:pos() + Rfoot:pos())/2
if(Shift)
{
Chest:applyForce(Camvec*500000)
W = 1    
}
if(!Shift){

if(Space){Head:applyForce(vec(0,0,30000))}

Hpos = Head:pos() - (Avrgps + vec(0,0,60))
Hvec = (($Hpos*150)+(Hpos*20))*-1
Head:applyForce(Hvec)


Cpos = Chest:pos() - (Avrgps + vec(0,0,50))
Cvec = (($Cpos*150)+(Cpos*20))*-5
#Chest:applyForce(Cvec)

Hippos = Hip:pos() - (Avrgps + vec(0,0,40))
Hipvec = (($Hippos*150)+(Hippos*20))*-3
Hip:applyForce(Hipvec)

Hipang = Hip:bearing(Camvec*500000) - 90
Hyaw = (($Hipang*150)+(Hipang*20))*-2
Hip:applyAngForce(ang(Hyaw,0,0))

if(M1){
    Lhand:setMass(10)
    Rhand:setMass(10)
    #Hip:applyAngForce(ang(10000,0,0))
    }
    else
    {
    Lhand:setMass(1)
    Rhand:setMass(1)    
        
    }




#Body Average point


#control
if(W == 1 | A == 1 | D == 1)
{
    if(Inc < 200){Inc = Inc + 7.5}
if(Inc >= 200){Inc = 0}
    if(Inc <= 100){Foot = 1}
    if(Inc <= 200 & Inc > 100){Foot = 2}
    
Vel = 15
}
if(S == 1)
{
    if(Inc < 200){Inc = Inc + 7.5}
if(Inc >= 200){Inc = 0}
    if(Inc <= 100){Foot = 1}
    if(Inc <= 200 & Inc > 100){Foot = 2}
    
Vel = -15
}
if(!W & !S & !A & !D)
{
    Foot = 0
    Vel = 0
}
if(A){Dire = Camvec:rotate(ang(0,90,0))}
if(D){Dire = Camvec:rotate(ang(0,90,0))*-1}
if(!A & !D){Dire = Seat:driver():eye()}

    
    
if(Foot == 1)
    {
        rangerFilter(Ragdoll)
        rangerHitEntities(1)
        rangerHitWater(1)
    Foottar = rangerOffset(5000,Hip:toWorld(vec(-5,0,0)) + Dire*Vel,vec(0,0,-1))
    F1footgo = Foottar:position() + vec(0,0,0)   
    }
if(Foot == 2)
    {
        rangerFilter(Ragdoll)
        rangerHitEntities(1)
        rangerHitWater(1)
    Foottar = rangerOffset(5000,Hip:toWorld(vec(5,0,0)) + Dire*Vel,vec(0,0,-1))
    F2footgo = Foottar:position() + vec(0,0,0)        
    }

F1Dist = F1footgo:distance(Hip:pos())
F2Dist = F2footgo:distance(Hip:pos())

F1Toepos = F1Toe - (F1footgo + vec(0,0,F1Toe:distance(F1footgo)/1.5))
F1Toevec = (($F1Toepos*150)+(F1Toepos*20))*-3


F2Toepos = F2Toe - (F2footgo + vec(0,0,F2Toe:distance(F2footgo)/1.5))
F2Toevec = (($F2Toepos*150)+(F2Toepos*20))*-3


Lfoot:applyOffsetForce(F1Toevec,F1Toe)

Rfoot:applyOffsetForce(F2Toevec,F2Toe)
}
FlapInc+=10
Flap = sin(FlapInc)*50

function birdCallSound(Ragdoll:entity){
    Rand = randint(1,3) 
    Ragdoll:soundPlay(curtime(),5,"/ambient/creatures/seagull_pain" +Rand+".wav")   
}
if(clk("quack")){
    if(M1){
birdCallSound(Ragdoll)
#Chest:applyForce(vec(0,0,100000))
}
timer("quack",500)    
}
#grab
if(M1)
    {
        #Chest:applyForce(Seat:driver():eye()*-50000)
        Lhand:moveTo(Chest:toWorld(vec(Flap+10,0,-50)))
        Rhand:moveTo(Chest:toWorld(vec(Flap+10,0,50)))
    #Lhand:applyForce(Seat:driver():eye()*10000000)
    #Rhand:applyForce(Seat:driver():eye()*10000000)
    }


}#end active


if(M2){
    Lhandpos = Head:pos() - (Chest:pos() + Seat:driver():eye()*5)
    Lhandvec = (($Lhandpos*150)+(Lhandpos*20))*-10
    Lhand:applyForce(Lhandvec)
    #Rhand:applyForce(Seat:driver():eye()*100)    
}
