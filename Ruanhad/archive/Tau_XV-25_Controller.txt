@name Tau XV-25 Controller
@inputs [Controls Camera]:wirelink [FL FR Hip Torso AL AR]:entity
@outputs 
@persist 
runOnTick(1)

function entity:turnTo(Tarang:angle) {
    V = This:toLocal(rotationVector(quat(Tarang)/quat(This))+This:pos())
    This:applyTorque((150*V - 15*This:angVelVector())*This:inertia()*3)  
}
function entity:moveTo(Tarpos:vector) {
    This:applyForce(((Tarpos-This:pos())*15-This:vel())*(This:mass()*0.75))    
}

if(first()){
    
}

Torso:moveTo(Hip:toWorld(vec(0,0,25)))
Torso:turnTo(Hip:angles())

AL:moveTo(Torso:toWorld(vec(0,50,-25)))
AR:moveTo(Torso:toWorld(vec(0,-50,-25)))

AL:turnTo(Torso:toWorld(ang()))
AR:turnTo(Torso:toWorld(ang()))
