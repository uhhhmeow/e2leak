@name Power Armor Controller
@inputs [Controls Camera Hud]:wirelink [Body FL FR AL AR Hip Gun]:entity
@outputs [LHip RHip]:vector MissilePodL:entity MissilePodR:entity [FLEntity FREntity]:entity  GunYaw Pilot:string
@persist [FLfootgo FRfootgo FLToepos FRToepos LPos RPos]:vector
@persist Offset OffsetYaw OffsetInc Grounded FPS WASD BodyAng Foot Lastfoot FLAngY FRAngY Inc 
runOnTick(1)
#start controls
W = Controls:number("W")
A = Controls:number("A")
S = Controls:number("S")
D = Controls:number("D")
R = Controls:number("R")
M1 = Controls:number("Mouse1")
M2 = Controls:number("Mouse2")
Space = Controls:number("Space")
Shift = Controls:number("Shift")#
Alt = Controls:number("Alt")
Mwheelup = Controls:number("NextWeapon")
Mwheeldown = Controls:number("PrevWeapon")
Active = Controls:number("Active")
Seat = Controls:entity("Entity")

Pilot = Seat:driver():name()
Grey = vec(1,1,1)*50
PhxMetallic = "Phoenix_storms/mat/mat_phx_metallic"
function holodata (Index,Model:string,Scale:vector,Pos:vector,Ang:angle,Material:string,Color:vector,Parent:entity){
    holoCreate(Index)
    holoModel(Index,Model)
    holoScale(Index,Scale)
    holoPos(Index,Pos)
    holoAng(Index,Ang)
    holoMaterial(Index,Material)
    holoColor(Index,Color)
    holoParent(Index,Parent)
    
}

function number rotationDifference(E1:entity, E2:entity){
Out = E1:angles():yaw()
Out2 = E2:angles():yaw()
if((Out - Out2) > 180){Out1b = -(360 - (Out))}else{Out1b = Out}
if((Out2 - Out) > 180){Out2b = -(360 - (Out2))}else{Out2b = Out2}
return Out2b - Out1b    
}

function number isSteamFriendLong(E1:entity, E2:entity){
SFriends = E2:steamFriends()
N=1

while(N<=SFriends:count()){
if(E1 == SFriends[N,entity]){Out = 1}else{
N++    
}

return Out        
}
    
}

Camvec = Body:toLocalAxis((Seat:driver():eyeAngles()):forward())
EyeMul = Camvec*50000

if(W|A|S|D){WASD = 1}else{WASD = 0}
if(changed(R) && R){FPS = !FPS}


if(first()){
FL:setMass(400)
FR:setMass(400)
Seat:setMass(400)
Gun:setMass(400)

FL:soundPlay(0,0, "/ambient/machines/machine_whine1.wav")
FR:soundPlay(1,0, "/ambient/machines/machine_whine1.wav")
FLfootgo = FL:pos()
FRfootgo = FR:pos()

Parts = entity():getConstraints()
rangerFilter(Parts:add(players()))
rangerHitEntities(1)
rangerHitWater(1)
rangerPersist(1)  

   holodata(1,"cube",vec(2,1,1),Body:toWorld(vec(-5,0,75)),ang(),PhxMetallic,Grey,Body) 
   holodata(2,"cube",vec(1,2,1),Body:toWorld(vec(-5,0,15)),ang(),PhxMetallic,Grey,Body) 
   holodata(3,"models/hunter/blocks/cube05x05x05.mdl",vec(1,2,1)*0.5,Body:toWorld(vec(-15,30,80)),ang(),"models/Magnusson_Teleporter/Magnusson_teleporter_fxglow_off",vec(1,1,1)*255,Body) 
   holodata(4,"hq_stube",vec(1.15,1.15,2.1),holoEntity(3):toWorld(vec(0,0,0)),holoEntity(3):toWorld(ang(90,0,0)),PhxMetallic,Grey,holoEntity(3)) 
   holodata(5,"hqcylinder",vec(1.15,1.15,1.5),holoEntity(3):toWorld(vec(0,0,0)),holoEntity(3):toWorld(ang(0,0,90)),PhxMetallic,Grey,holoEntity(3)) 

   holodata(6,"models/hunter/blocks/cube05x05x05.mdl",vec(1,2,1)*0.5,Body:toWorld(vec(-15,-30,80)),ang(),"models/Magnusson_Teleporter/Magnusson_teleporter_fxglow_off",vec(1,1,1)*255,Body) 
   holodata(7,"hq_stube",vec(1.15,1.15,2.1),holoEntity(6):toWorld(vec(0,0,0)),holoEntity(6):toWorld(ang(90,0,0)),PhxMetallic,Grey,holoEntity(6)) 
   holodata(8,"hqcylinder",vec(1.15,1.15,1.5),holoEntity(6):toWorld(vec(0,0,0)),holoEntity(6):toWorld(ang(0,0,90)),PhxMetallic,Grey,holoEntity(6))

#models/props_wasteland/light_spotlight01_base.mdl 
   holodata(9,"models/props_wasteland/light_spotlight01_base.mdl",vec(1,2,1),Body:toWorld(vec(-15,30,75)),ang(),PhxMetallic,Grey,Body) 
   holodata(10,"models/props_wasteland/light_spotlight01_base.mdl",vec(1,2,1),Body:toWorld(vec(-15,-30,75)),ang(),PhxMetallic,Grey,Body) 
Grounded = 1
timer("HeadDetails",1200)
}

if(clk("HeadDetails")){
#models/props_wasteland/laundry_washer003.mdl    
   holodata(11,"models/props_wasteland/laundry_washer003.mdl",vec(1,1,1)*0.25,holoEntity(1):toWorld(vec(0,0.5,0)),holoEntity(1):toWorld(ang(0,0,0)),PhxMetallic,Grey,holoEntity(1)) 
   holodata(12,"hq_sphere",vec(1,1,1)*1,holoEntity(1):toWorld(vec(10,0,0)),holoEntity(1):toWorld(ang(0,0,0)),"models/shiny",vec(255,0,0),holoEntity(1)) 
}


if(Seat:driver() == owner()){holoColor(12,vec(255,0,0))}
elseif(isSteamFriendLong(Seat:driver(),owner())){holoColor(12,vec(0,255,0))}
else{holoColor(12,teamColor(Seat:driver():team()))}

MissilePodL = holoEntity(3)
MissilePodR = holoEntity(6)

soundPitch(0, FL:vel():length()/10)
soundPitch(1, FR:vel():length()/10)

###############################################################
#if(Seat:driver() != owner()){Seat:hintDriver("SECURITY SYSTEMS ENGAGED, EJECTING!",7),Seat:ejectPod()}
###############################################################
if(FPS){
Campos = holoEntity(1):toWorld(vec(12,0,0))
}
else{
Campos = Body:pos() + Camvec*-200 + vec(0,0,100)  
    }
Camera:setNumber("Activated",Active)
Camera:setVector("Direction",Camvec)
Camera:setVector("Position",Campos)
Camera:setEntity("Parent",holoEntity(1))

Aimpos = rangerOffset(10000,Campos,Camvec):position()


if(Active){
holoAng(1,Camvec:toAngle())
holoAng(3,Camvec:toAngle())
holoAng(6,Camvec:toAngle())
holoAng(9,ang(0,Camvec:toAngle():yaw(),0))
holoAng(10,ang(0,Camvec:toAngle():yaw(),0))
OffsetYaw = BodyAng/4/3

}else{
holoAng(1,holoEntity(2):angles())
holoAng(3,holoEntity(2):angles())
holoAng(6,holoEntity(2):angles())
holoAng(9,holoEntity(2):angles())
holoAng(10,holoEntity(2):angles())

OffsetYaw = 0
}
if(Grounded){
if(Alt){
Posture = 25}else{
Posture = 35
}
FootAng = rotationDifference(FL,FR)
holoAng(2,ang(Body:angles():pitch(),FR:angles():yaw() - FootAng/2,0))
BodyAng = rotationDifference(holoEntity(2),holoEntity(1))

BAng = ang(Shift*10+ Alt*75,holoEntity(2):angles():yaw() + BodyAng/4,0)


Avrgpoint = ((FL:pos() + FR:pos())/2) + Body:forward()*0
Bodypos = (Avrgpoint+vec(0,0,Posture + (Shift*-5) - (abs(FL:pos():z() - FR:pos():z()))/2)+holoEntity(2):right()*(OffsetInc))
Body:applyForce(((Bodypos-Body:pos())*15-Body:vel())*(Body:mass()))


}else{ # if its in flight, reorganise the heirarchy and make the body independant of the feet
BAng = ang(-Seat:elevation(EyeMul),-Seat:bearing(EyeMul)+Seat:angles():yaw(),0)
holoAng(2,Body:angles())
}
BV = Body:toLocal(rotationVector(quat(BAng)/quat(Body))+Body:pos())
Body:applyTorque((150*BV - 15*Body:angVelVector())*Body:inertia()*3)  



FLfootrange = (FL:pos() - FLfootgo:setZ(FL:pos():z())):length()
FRfootrange = (FR:pos() - FRfootgo:setZ(FR:pos():z())):length()

FLToepos1 = FL:pos() - (FLfootgo + vec(0,0,FLfootrange/1.5))
FRToepos1 = FR:pos() - (FRfootgo + vec(0,0,FRfootrange/1.5))
FLToepos = FL:vel()/(50* (Shift*10 + 1)) + FLToepos1:normalized()*vec(1,1,2.5)*clamp(FLToepos1:length()/2.5,0,10)*(1)
FRToepos = FR:vel()/(50* (Shift*10 + 1)) + FRToepos1:normalized()*vec(1,1,2.5)*clamp(FRToepos1:length()/2.5,0,10)*(1)


FLToevec = (($FLToepos*150)+(FLToepos*20))*(FL:mass()/5*-1)
FL:applyForce(FLToevec)

FRToevec = (($FRToepos*150)+(FRToepos*20))*(FR:mass()/5*-1)
FR:applyForce(FRToevec)

FLAng = ang((Foot == 1)*20,FLAngY,0)
FRAng = ang((Foot == 2)*20,FRAngY,0)





FLV = FL:toLocal(rotationVector(quat(FLAng)/quat(FL))+FL:pos())
FL:applyTorque((150*FLV - 15*FL:angVelVector())*FL:inertia()*3)  


FRV = FR:toLocal(rotationVector(quat(FRAng)/quat(FR))+FR:pos())
FR:applyTorque((150*FRV - 15*FR:angVelVector())*FR:inertia()*3)  
GunYaw = rotationDifference(Hip,Gun)
if(Active){
Gunpos = Hip:toWorld(vec(20,-15,Gun:angles():pitch()/2 + 20)) + Body:right()*abs(GunYaw/5) + Body:forward()*GunYaw/5
GunAng = (Aimpos - Gunpos):toAngle()
}else{
Gunpos = Hip:toWorld(vec(15,-10,10))
GunAng = Body:toWorld(ang(20,70,0))
}

Gun:applyForce(((Gunpos-Gun:pos())*15-Gun:vel())*(Gun:mass()))

GunV = Gun:toLocal(rotationVector(quat(GunAng)/quat(Gun))+Gun:pos())
Gun:applyTorque((150*GunV - 15*Gun:angVelVector())*Gun:inertia()*3)  

#[
if(WASD){
    Vel = (30* (Shift*0.5 + 1))
if((FL:pos() - FLfootgo):length() < 10 & Foot != 2){Foot = 2}
elseif((FR:pos() - FRfootgo):length() < 10 & Foot == 2){Foot = 1}
}
else{
    Vel = 0
    Foot = 0
    Offset = 0
    }


Dire = Body:forward()*(A-D)*0.75 + Body:right()*(W-S)

FLOffset = Body:toLocal(FLfootgo)

if((FL:pos() - FR:pos()):length() < 10 && !WASD){
if(Lastfoot == 2){Foot = 1}
if(Lastfoot == 1){Foot = 2}    
}
]#
if(WASD)
{
if(Inc < 200){Inc = Inc + 3.75 + (Shift*0.75)}
    if(Inc >= 200){Inc = 0}
    if(Inc <= 100 & Inc > 0)   {Foot = 1}
    if(Inc <= 200 & Inc > 100)  {Foot = 2}
    
Vel = (20* (Shift*0.25 + 1)) 
}else{
Foot = 0
Vel = 0
Offset = 0
}


Dire = holoEntity(2):forward()*(W-S) + holoEntity(2):right()*(D-A)*0.75

FLOffset = Body:toLocal(FLfootgo)

if(!WASD && Body:toLocal(FLfootgo):y() >=0 ){
#Foot = 1
}
if(!WASD && Body:toLocal(FRfootgo):y() <=0 ){
#Foot = 2
}

if(Grounded){
if(Foot == 1)
    {
    Foottar = rangerOffset(5000,Body:toWorld(vec(0,-10,50)) + Dire*Vel,vec(0,0,-1))
    FLEntity = Foottar:entity()
LPos = FLEntity:toLocal(Foottar:position())+vec(0,0,18)
if(Foottar:entity()){
FLfootgo = FLEntity:toWorld(LPos)
}else{
FLfootgo = Foottar:position() + vec(0,0,8)} 

    Offset = 5
    FLAngY = holoEntity(1):angles():yaw()
    Lastfoot = 1
    }
if(Foot == 2)
    {
    Foottar = rangerOffset(5000,Body:toWorld(vec(0,10,50)) + Dire*Vel,vec(0,0,-1))
    FREntity = Foottar:entity()
RPos = FREntity:toLocal(Foottar:position())+vec(0,0,18)
if(Foottar:entity()){
FRfootgo = FREntity:toWorld(RPos)
}else{
FRfootgo = Foottar:position() + vec(0,0,8)}       
    Offset = -5
    FRAngY = holoEntity(1):angles():yaw()
    Lastfoot = 2
    }
    
}else{
#    Foottar = rangerOffset(5000,Body:toWorld(vec(-10,40,50)) + Dire*Vel,vec(0,0,-1))
#    FLfootgo = Foottar:position() + vec(0,0,7)
      
#    Foottar = rangerOffset(5000,Body:toWorld(vec(-10,-40,50)) + Dire*Vel,vec(0,0,-1))
#    FRfootgo = Foottar:position() + vec(0,0,7)   
#Offset = 0
#Body:applyForce(Dire*(Shift*0.25 + 1)*Vel*Body:mass()/2)      

}
if(!WASD){
if(FLEntity){FLfootgo =  FLEntity:toWorld( LPos)}
if(FREntity){FRfootgo =  FREntity:toWorld( RPos)}
}
if(OffsetInc < (Offset + OffsetYaw)){OffsetInc+=0.5}
if(OffsetInc > (Offset + OffsetYaw)){OffsetInc-=0.5}
