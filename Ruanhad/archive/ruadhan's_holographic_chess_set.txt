@name Ruadhan's Holographic Chess Set
@outputs [Grid]:table OAP:vector
@persist Str:string 
@model models/props_c17/FurnitureTable001a.mdl
interval(100)

function holoData (Index,Model:string,Scale:vector,Pos:vector,Ang:angle,Mat:string,Color:vector,Parent:entity){
    holoCreate(Index)
    holoModel(Index,Model)
    holoScale(Index,Scale)
    holoPos(Index,Pos)
    holoAng(Index,Ang)
    holoMaterial(Index,Mat)
    holoColor(Index,Color)
    holoParent(Index,Parent)
}

function number dotProduct(User:entity, Button:vector){
    E1 = User:aimPos()
    
    V1 = User:shootPos() - E1
    V2 = User:shootPos() - Button
    
    Dot = V1:dot(V2)
    #A*cos(Theta)*B
    D1 = Dot/V1:length()
    D2 = acos(D1/V2:length())    
    return D2
}

if(first()){
entity():setAlpha(0)
timer("board",0)    
timer("white_pieces",1200*1)    
timer("white_pawns",1200*2) 
timer("black_pieces",1200*3)    
timer("black_pawns",1200*4)    
timer("initBoard",1200*5)    
}

if(clk("board")){
    holoData(40,"models/props_phx/games/chess/board.mdl",vec(1,1,1)*0.1,entity():toWorld(vec(0,0,18)),entity():toWorld(ang(-90,0,0)),"",vec(1,1,1)*255,entity())    
    holoData(41,"models/props_c17/FurnitureTable001a.mdl",vec(1,1,1)*1,entity():toWorld(vec(0,0,0)),entity():toWorld(ang(0,0,0)),"",vec(1,1,1)*255,entity())    
    holoData(42,"hq_cylinder",vec(1,1,1)*0.25,entity():toWorld(vec(0,0,22)),entity():toWorld(ang(0,0,0)),"",vec(1,0.5,0.5)*255,entity())    
    #holoAlpha(42,100)  
}

if(clk("white_pieces")){
holoData(1,"models/props_phx/games/chess/white_king.mdl",vec(1,1,1)*0.1,entity():toWorld(vec(-10.9,1.5,20)),entity():toWorld(ang(0,0,0)),"",vec(1,1,1)*255,entity())    
holoData(2,"models/props_phx/games/chess/white_queen.mdl",vec(1,1,1)*0.1,entity():toWorld(vec(-10.9,-1.4,20)),entity():toWorld(ang(0,0,0)),"",vec(1,1,1)*255,entity())    
holoData(3,"models/props_phx/games/chess/white_bishop.mdl",vec(1,1,1)*0.1,entity():toWorld(vec(-10.9,4.5,20)),entity():toWorld(ang(0,0,0)),"",vec(1,1,1)*255,entity())    
holoData(4,"models/props_phx/games/chess/white_bishop.mdl",vec(1,1,1)*0.1,entity():toWorld(vec(-10.9,-4.5,20)),entity():toWorld(ang(0,0,0)),"",vec(1,1,1)*255,entity())    

holoData(5,"models/props_phx/games/chess/white_knight.mdl",vec(1,1,1)*0.1,entity():toWorld(vec(-10.9,7.5,20)),entity():toWorld(ang(0,0,0)),"",vec(1,1,1)*255,entity())    
holoData(6,"models/props_phx/games/chess/white_knight.mdl",vec(1,1,1)*0.1,entity():toWorld(vec(-10.9,-7.5,20)),entity():toWorld(ang(0,0,0)),"",vec(1,1,1)*255,entity())    
holoData(7,"models/props_phx/games/chess/white_rook.mdl",vec(1,1,1)*0.1,entity():toWorld(vec(-10.9,10.8,20)),entity():toWorld(ang(0,0,0)),"",vec(1,1,1)*255,entity())    
holoData(8,"models/props_phx/games/chess/white_rook.mdl",vec(1,1,1)*0.1,entity():toWorld(vec(-10.9,-10.7,20)),entity():toWorld(ang(0,0,0)),"",vec(1,1,1)*255,entity())        
}

if(clk("white_pawns")){
holoData(11,"models/props_phx/games/chess/white_pawn.mdl",vec(1,1,1)*0.1,entity():toWorld(vec(-7.5,1.5,20)),entity():toWorld(ang(0,180,0)),"",vec(1,1,1)*255,entity())    
holoData(12,"models/props_phx/games/chess/white_pawn.mdl",vec(1,1,1)*0.1,entity():toWorld(vec(-7.5,-1.4,20)),entity():toWorld(ang(0,180,0)),"",vec(1,1,1)*255,entity())    
holoData(13,"models/props_phx/games/chess/white_pawn.mdl",vec(1,1,1)*0.1,entity():toWorld(vec(-7.5,4.5,20)),entity():toWorld(ang(0,180,0)),"",vec(1,1,1)*255,entity())    
holoData(14,"models/props_phx/games/chess/white_pawn.mdl",vec(1,1,1)*0.1,entity():toWorld(vec(-7.5,-4.5,20)),entity():toWorld(ang(0,180,0)),"",vec(1,1,1)*255,entity())    
holoData(15,"models/props_phx/games/chess/white_pawn.mdl",vec(1,1,1)*0.1,entity():toWorld(vec(-7.5,7.5,20)),entity():toWorld(ang(0,180,0)),"",vec(1,1,1)*255,entity())    
holoData(16,"models/props_phx/games/chess/white_pawn.mdl",vec(1,1,1)*0.1,entity():toWorld(vec(-7.5,-7.5,20)),entity():toWorld(ang(0,180,0)),"",vec(1,1,1)*255,entity())    
holoData(17,"models/props_phx/games/chess/white_pawn.mdl",vec(1,1,1)*0.1,entity():toWorld(vec(-7.5,10.8,20)),entity():toWorld(ang(0,180,0)),"",vec(1,1,1)*255,entity())    
holoData(18,"models/props_phx/games/chess/white_pawn.mdl",vec(1,1,1)*0.1,entity():toWorld(vec(-7.5,-10.7,20)),entity():toWorld(ang(0,180,0)),"",vec(1,1,1)*255,entity())        
    
}

if(clk("black_pieces")){
holoData(21,"models/props_phx/games/chess/black_king.mdl",vec(1,1,1)*0.1,entity():toWorld(vec(10.8,1.5,20)),entity():toWorld(ang(0,180,0)),"",vec(1,1,1)*255,entity())    
holoData(22,"models/props_phx/games/chess/black_queen.mdl",vec(1,1,1)*0.1,entity():toWorld(vec(10.8,-1.4,20)),entity():toWorld(ang(0,180,0)),"",vec(1,1,1)*255,entity())    
holoData(23,"models/props_phx/games/chess/black_bishop.mdl",vec(1,1,1)*0.1,entity():toWorld(vec(10.8,4.5,20)),entity():toWorld(ang(0,180,0)),"",vec(1,1,1)*255,entity())    
holoData(24,"models/props_phx/games/chess/black_bishop.mdl",vec(1,1,1)*0.1,entity():toWorld(vec(10.8,-4.5,20)),entity():toWorld(ang(0,180,0)),"",vec(1,1,1)*255,entity())    

holoData(25,"models/props_phx/games/chess/black_knight.mdl",vec(1,1,1)*0.1,entity():toWorld(vec(10.8,7.5,20)),entity():toWorld(ang(0,180,0)),"",vec(1,1,1)*255,entity())    
holoData(26,"models/props_phx/games/chess/black_knight.mdl",vec(1,1,1)*0.1,entity():toWorld(vec(10.8,-7.5,20)),entity():toWorld(ang(0,180,0)),"",vec(1,1,1)*255,entity())    
holoData(27,"models/props_phx/games/chess/black_rook.mdl",vec(1,1,1)*0.1,entity():toWorld(vec(10.8,10.8,20)),entity():toWorld(ang(0,180,0)),"",vec(1,1,1)*255,entity())    
holoData(28,"models/props_phx/games/chess/black_rook.mdl",vec(1,1,1)*0.1,entity():toWorld(vec(10.8,-10.7,20)),entity():toWorld(ang(0,180,0)),"",vec(1,1,1)*255,entity())        
}

if(clk("black_pawns")){
holoData(31,"models/props_phx/games/chess/black_pawn.mdl",vec(1,1,1)*0.1,entity():toWorld(vec(7.5,1.5,20)),entity():toWorld(ang(0,180,0)),"",vec(1,1,1)*255,entity())    
holoData(32,"models/props_phx/games/chess/black_pawn.mdl",vec(1,1,1)*0.1,entity():toWorld(vec(7.5,-1.4,20)),entity():toWorld(ang(0,180,0)),"",vec(1,1,1)*255,entity())    
holoData(33,"models/props_phx/games/chess/black_pawn.mdl",vec(1,1,1)*0.1,entity():toWorld(vec(7.5,4.5,20)),entity():toWorld(ang(0,180,0)),"",vec(1,1,1)*255,entity())    
holoData(34,"models/props_phx/games/chess/black_pawn.mdl",vec(1,1,1)*0.1,entity():toWorld(vec(7.5,-4.5,20)),entity():toWorld(ang(0,180,0)),"",vec(1,1,1)*255,entity())    
holoData(35,"models/props_phx/games/chess/black_pawn.mdl",vec(1,1,1)*0.1,entity():toWorld(vec(7.5,7.5,20)),entity():toWorld(ang(0,180,0)),"",vec(1,1,1)*255,entity())    
holoData(36,"models/props_phx/games/chess/black_pawn.mdl",vec(1,1,1)*0.1,entity():toWorld(vec(7.5,-7.5,20)),entity():toWorld(ang(0,180,0)),"",vec(1,1,1)*255,entity())    
holoData(37,"models/props_phx/games/chess/black_pawn.mdl",vec(1,1,1)*0.1,entity():toWorld(vec(7.5,10.8,20)),entity():toWorld(ang(0,180,0)),"",vec(1,1,1)*255,entity())    
holoData(38,"models/props_phx/games/chess/black_pawn.mdl",vec(1,1,1)*0.1,entity():toWorld(vec(7.5,-10.7,20)),entity():toWorld(ang(0,180,0)),"",vec(1,1,1)*255,entity())        
    
}

#-10.9,10.8,20

#10.8,-10.7,20
Height = (10.8+10.9)/8
Width = (10.8+10.7)/8

if(clk("initBoard")){
    for(I=1,8){
        Grid[I,array] = array()
    }
    for(J=1,8){
        for(I=1,8){
            Grid[J,array][I,vector] = vec(-10.9 + Height*J,10.8+Width*I,20)
        }
    }
    
    for(J=1,8){
        Str = ""
        for(I=1,8){
            Str+=Grid[J,array][I,vector]
        }
        #print(Str)   
    }
}

OAP = owner():aimPos()
LOAP = entity():toLocal(OAP)
X = clamp(LOAP:x(),-10.9,10.8)
Y = clamp(LOAP:y(),-10.7,10.8)
Z = 20

holoPos(42,entity():toWorld(vec(X,Y,Z)))
holoColor(42,vec(!owner():keyUse(),owner():keyUse(),0)*255)
