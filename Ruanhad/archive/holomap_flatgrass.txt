@name HoloMap Flatgrass
@inputs #Base:entity
@outputs Targets:array
@persist Scalefactor BasePos:vector Targets:array J
interval(500)
Base = entity():isWeldedTo()
BasePos = Base:pos() + vec(0,0,5)
if(first() | dupefinished()){
    J = 12 # number of players to track
Scalefactor = 100


for(I=1,J){ # players
holoCreate(I)
holoModel(I,"cube")
holoScale(I,owner():boxSize()/Scalefactor) 
holoMaterial(I,"Phoenix_storms/mat/mat_phx_metallic")
holoParent(I,Base)
}

for(I=J+1,J*2){
holoCreate(I)
holoPos(I,holoEntity(I-J):toWorld(vec(0,0,0)))
holoModel(I,"sphere")
holoScale(I,vec(1,1,1)*-0.25) 
holoMaterial(I,"Phoenix_storms/mat/mat_phx_metallic")
holoParent(I,I-J)   
}

timer("UpdatePlayers",100)
}
if(clk("UpdatePlayers") | changed(players():count())){
Targets = players()
timer("UpdatePlayers",100)
}
 
for(I=1,J){
holoColor(I+J,teamColor(Targets[I,entity]:team()))    
}
holoColor(25,vec(255,1,1))  
for(I=1,J){
if(Targets[I,entity]:isAlive() != 1 | holoEntity(I):pos() == vec()){
#Targets:removeEntity(I)
#holoScale(I,vec())
}else{holoScale(I,vec(30,30,65)/Scalefactor/4.75)} 

if(Targets[I,entity]:vehicle()){
holoColor(I,vec(255,100,100))}
elseif(Targets[I,entity]:inNoclip()){
holoColor(I,vec(100,255,100))}
else{holoColor(I,vec(100,100,255))}   
holoPos(I,BasePos + Targets[I,entity]:pos()/Scalefactor)
holoAng(I,Targets[I,entity]:angles())    


}  
