@name Laser Reflection
@inputs 
@outputs 
@persist EndPos:array ExcidentVectors:array
interval(100)
function holoData (Index,Model:string,Scale:vector,Pos:vector,Ang:angle,Material:string,Color:vector,Parent:entity){
    holoCreate(Index)
    holoModel(Index,Model)
    holoScale(Index,Scale)
    holoPos(Index,Pos)
    holoAng(Index,Ang)
    holoMaterial(Index,Material)
    holoColor(Index,Color)
    holoParent(Index,Parent)
}
if(first()){
    for(I=1,20){
    holoData(I,"cube",vec(1,1,1),vec(),ang(),"",vec(1,0,0)*255,entity())    
    holoDisableShading(I,1)    
    }
    #holoData(2,"cube",vec(1,1,1),vec(),ang(),"",vec(1,0,0)*255,entity())    
    #holoData(3,"cube",vec(1,1,1),vec(),ang(),"",vec(1,0,0)*255,entity())  
    
    #holoDisableShading(1,1)  
    #holoDisableShading(2,1)  
    #holoDisableShading(3,1)  
  
  
}
EndPos[0,vector] = entity():pos()
InitTarget = owner():aimPos()
InitDirvec = (InitTarget - entity():pos()):normalized()
ExcidentVectors[0,vector] = InitDirvec
for(I=1,20){
    
    Ranger = rangerOffset(5000,EndPos[I-1,vector],ExcidentVectors[I-1,vector]) 
    EndPos[I,vector] = Ranger:pos()
    NormVec = Ranger:hitNormal()
    ExcidentVectors[I,vector] = NormVec - (NormVec + ExcidentVectors[I-1,vector]):toAngle():rotateAroundAxis(NormVec,180):forward()
    holoPos(I,(EndPos[I-1,vector] + EndPos[I,vector])/2)   
    holoAng(I,(EndPos[I-1,vector] - EndPos[I,vector]):toAngle()) 
    holoScale(I,vec((EndPos[I-1,vector] - EndPos[I,vector]):length()/12,0.25,0.25))  
}
