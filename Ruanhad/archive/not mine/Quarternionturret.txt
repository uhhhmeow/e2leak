@name Turret
@inputs Move
@outputs Fire
@persist CurPos:vector E:entity
@trigger 
if (first()|duped()) {
    E = entity():isWeldedTo() #Get the square PHX prop
    CurPos = E:pos() + vec(0,0,50) #Set the current pos to the position of the square prop + 50
    runOnTick(1) #Make it run every tick
}
 
if (Move) {
    CurPos = owner():aimPos() + vec(0,0,50) #Set the position of the square to where you are aiming + 50
}
 
E:applyForce(((CurPos-E:toWorld(E:boxCenter()))*100-E:vel())*E:mass())
 
 
#This applyForce formula is one I have found .. somewhere.. dunno lol
#it goes like this
# ((TargetPos - CurrentPos) * Multiplier - Velocity) * Mass)
#and its very effective and can make ANY prop of any size and weight fly
#almost instantly to where you want.
#I dont know exactly why this works so good... but it does.
#There are of course other applyforce formulas, but this is the one I always use
#the multipliers I most often use is 20, 50 or 100. The higher it is, the faster it flies
#but dont make it too high or it will spaz to death.
 
#Get the angle we want the prop to point in
TargetAngle = (owner():aimPos()-E:pos()):toAngle()
 
#Turn that into a Quaternion
DesiredQuat = quat(TargetAngle)
 
#Get the current quaternion (this is the "current angle" so to speak. as you remember in the applyForce code
#                           you need TargetPos-CurrentPos. Well its the same here)
CurrentQuat = quat(E)
 
#This calculation is comparable with "TargetPos-CurrentPos" in applyForce. Its actually exactly the same 
#except with quaternions you use / instead of -
DifferenceQuat = DesiredQuat / CurrentQuat
 
#Get the rotationVector (no idea what that does lol. I just memorized it.)
Rotation = rotationVector(DifferenceQuat)
 
#And then we need to add its own position (I have also no idea why..)
Rotation2 = Rotation + E:pos()
 
#And finally make that local (needs to be done)
Torque = E:toLocal(Rotation2)
 
#NOW we can apply the torque! You can see similarities to the applyForce code
E:applyTorque((Torque*1000-E:angVelVector()*20)*E:inertia())
 
#This is the formula as I memorized it ^^ in other words, before I split it up.
#Torque = E:toLocal(rotationVector(quat((owner():aimPos()-E:pos()):toAngle())/quat(E))+E:pos())
#E:applyTorque((Torque*500-E:angVelVector()*20)*E:inertia())
 
#Here is an applyAngForce code. Untested, so I don't know if it works
#Ang = ang(E:elevation(owner():aimPos()),E:bearing(owner():aimPos()),E:angles():roll()) * -1000
#E:applyAngForce(Ang+$Ang*5)
#Because it uses $ (delta), you need to put Ang ("Ang:angle") in persist if you want to use it.
