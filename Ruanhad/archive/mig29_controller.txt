@name Mig29 controller
@inputs Controls:wirelink Camera:wirelink Core:entity
@outputs Cannon Thrust
@persist Corerotvel:angle Camvec:vector FireableM
interval(10)

#start controls
W = Controls:number("W")
A = Controls:number("A")
S = Controls:number("S")
D = Controls:number("D")
M1 = Controls:number("Mouse1")
M2 = Controls:number("Mouse2")
Space = Controls:number("Space")
Shift = Controls:number("Shift")#
Alt = Controls:number("Alt")
Mwheelup = Controls:number("NextWeapon")
Mwheeldown = Controls:number("PrevWeapon")
Active = Controls:number("Active")
Seat = Controls:entity("Entity")
#end controls



if(M1){Cannon = 1}
else{Cannon = 0}

if(first())
{
    Camvec = Core:forward()
    Core:setMass(200)
#holograms
#nosecone
    holoCreate(1,Core:toWorld(vec(250,0,25)))
    holoModel(1,"models/props_phx/misc/flakshell_big.mdl")
    holoMaterial(1,"Phoenix_storms/mat/mat_phx_metallic")
    holoScale(1,vec(1.2,1.2,2))
    holoColor(1,vec(38,38,38))
    holoAng(1,Core:toWorld(ang(90,0,0)))
    holoParent(1,Core)
    
    holoCreate(2,holoEntity(1):toWorld(vec(0,0,50)))
    holoModel(2,"models/props_phx/misc/flakshell_big.mdl")
    holoMaterial(2,"Phoenix_storms/mat/mat_phx_metallic")
    holoScale(2,vec(0.1,0.1,2))
    holoColor(2,vec(38,38,38))
    holoAng(2,holoEntity(1):toWorld(ang(0,0,0)))
    holoParent(2,Core)

    holoCreate(3,Core:toWorld(vec(200,22,30)))
    holoModel(3,"models/props_combine/headcrabcannister01a.mdl")
    holoMaterial(3,"Phoenix_storms/mat/mat_phx_metallic")
    holoScale(3,vec(1,0.5,1))
    holoColor(3,vec(116,127,127))
    holoAng(3,Core:toWorld(ang(0,-5,0)))
    holoParent(3,Core)
    
    holoCreate(4,Core:toWorld(vec(200,-22,30)))
    holoModel(4,"models/props_combine/headcrabcannister01a.mdl")
    holoMaterial(4,"Phoenix_storms/mat/mat_phx_metallic")
    holoScale(4,vec(1,0.5,1))
    holoColor(4,vec(116,127,127))
    holoAng(4,Core:toWorld(ang(0,5,0)))
    holoParent(4,Core)
    
    holoCreate(5,Core:toWorld(vec(105,23,30)))
    holoModel(5,"models/props_combine/headcrabcannister01a.mdl")
    holoMaterial(5,"Phoenix_storms/mat/mat_phx_metallic")
    holoScale(5,vec(1,0.5,1))
    holoColor(5,vec(116,127,127))
    holoAng(5,Core:toWorld(ang(0,185,0)))
    holoParent(5,Core)
    
    holoCreate(6,Core:toWorld(vec(105,-23,30)))
    holoModel(6,"models/props_combine/headcrabcannister01a.mdl")
    holoMaterial(6,"Phoenix_storms/mat/mat_phx_metallic")
    holoScale(6,vec(1,0.5,1))
    holoColor(6,vec(116,127,127))
    holoAng(6,Core:toWorld(ang(0,-185,0)))
    holoParent(6,Core)

#forwing
    holoCreate(7,Core:toWorld(vec(100,0,15)))
    holoModel(7,"models/XQM/wingtip1.mdl")
    holoMaterial(7,"Phoenix_storms/mat/mat_phx_metallic")
    holoScale(7,vec(6,10,5))
    holoColor(7,vec(116,127,127))
    holoAng(7,Core:toWorld(ang(0,90,0)))
    holoParent(7,Core)
    
    holoCreate(8,Core:toWorld(vec(0,0,15)))
    holoModel(8,"models/props_combine/headcrabcannister01a.mdl")
    holoMaterial(8,"Phoenix_storms/mat/mat_phx_metallic")
    holoScale(8,vec(2,4,1))
    holoColor(8,vec(116,127,127))
    holoAng(8,Core:toWorld(ang(0,0,0)))
    holoParent(8,Core)
    
    #tailfins
    
    holoCreate(9,Core:toWorld(vec(-200,-50,75)))
    holoModel(9,"models/XQM/jettailpiece1big.mdl")
    holoMaterial(9,"Phoenix_storms/mat/mat_phx_metallic")
    holoScale(9,vec(1,1.5,1))
    holoColor(9,vec(116,127,127))
    holoAng(9,Core:toWorld(ang(0,90,0)))
    holoParent(9,Core)

    holoCreate(10,Core:toWorld(vec(-200,50,75)))
    holoModel(10,"models/XQM/jettailpiece1big.mdl")
    holoMaterial(10,"Phoenix_storms/mat/mat_phx_metallic")
    holoScale(10,vec(1,1.5,1))
    holoColor(10,vec(116,127,127))
    holoAng(10,Core:toWorld(ang(0,90,0)))
    holoParent(10,Core)
    
    #engines
    holoCreate(11,Core:toWorld(vec(0,-35,-20)))
    holoModel(11,"models/props_phx/box_torpedo.mdl")
    holoMaterial(11,"Phoenix_storms/mat/mat_phx_metallic")
    holoScale(11,vec(1,1,0.5))
    holoColor(11,vec(116,127,127))
    holoAng(11,Core:toWorld(ang(0,0,-5)))
    holoParent(11,Core)

    holoCreate(12,Core:toWorld(vec(0,35,-20)))
    holoModel(12,"models/props_phx/box_torpedo.mdl")
    holoMaterial(12,"Phoenix_storms/mat/mat_phx_metallic")
    holoScale(12,vec(1,1,0.5))
    holoColor(12,vec(116,127,127))
    holoAng(12,Core:toWorld(ang(0,0,5)))
    holoParent(12,Core)
    
    holoCreate(13,Core:toWorld(vec(-200,-100,0)))
    holoModel(13,"models/XQM/jettailpiece1big.mdl")
    holoMaterial(13,"Phoenix_storms/mat/mat_phx_metallic")
    holoScale(13,vec(1,1.5,1))
    holoColor(13,vec(116,127,127))
    holoAng(13,Core:toWorld(ang(-95,90,0)))
    holoParent(13,Core)

    holoCreate(14,Core:toWorld(vec(-200,100,0 )))
    holoModel(14,"models/XQM/jettailpiece1big.mdl")
    holoMaterial(14,"Phoenix_storms/mat/mat_phx_metallic")
    holoScale(14,vec(1,1.5,1))
    holoColor(14,vec(116,127,127))
    holoAng(14,Core:toWorld(ang(95,90,0)))
    holoParent(14,Core)
    
    holoCreate(15,Core:toWorld(vec(-150,0,10)))
    holoModel(15,"models/props_combine/headcrabcannister01a.mdl")
    holoMaterial(15,"Phoenix_storms/mat/mat_phx_metallic")#
    holoScale(15,vec(1.2,3,1.5))
    holoColor(15,vec(116,127,127))
    holoAng(15,Core:toWorld(ang(-3,180,0)))
    holoParent(15,Core)
timer("FireableM",500)
FireableM = 1
}
if(Mwheeldown){Thrust = Thrust + 15}
if(Mwheelup){Thrust = Thrust - 15}
if(!Active){Thrust = 0}
Core:applyOffsetForce(Core:forward()*Core:mass()*Thrust,Core:toWorld(vec(50,0,15)))
if(W){Core:applyOffsetForce(Core:up()*Core:mass()*Core:vel():length()/750*-7,Core:toWorld(vec(200,0,0)))}
if(S){Core:applyOffsetForce(Core:up()*Core:mass()*Core:vel():length()/750*7,Core:toWorld(vec(200,0,0)))}
if(Shift){Core:applyOffsetForce(Core:right()*Core:mass()*Core:vel():length()/750*-5,Core:toWorld(vec(200,0,0)))}
if(Space){Core:applyOffsetForce(Core:right()*Core:mass()*Core:vel():length()/750*5,Core:toWorld(vec(200,0,0)))}
if(A){Core:applyOffsetForce(Core:up()*Core:mass()*Core:vel():length()/750*5,Core:toWorld(vec(0,-150,0)))}
if(D){Core:applyOffsetForce(Core:up()*Core:mass()*Core:vel():length()/750*5,Core:toWorld(vec(0,150,0)))}
Corerotvel = Core:angVel()
Stabrot = (($Corerotvel*150)+(Corerotvel*20))*Core:mass()/50*-1
Core:applyAngForce(Stabrot)


Camang = (Core:forward()):toAngle()
Camang = Camang:setRoll(Core:angles():roll())

Camvec = Seat:driver():eye()

Campos = Core:pos()+Camvec*-500
#Camera:setNumber("Activated",Active)
Camera:setAngle("Angle", Camang)
Camera:setVector("Position", Campos)



if(clk("FireableM")){
FireableM = 1    
}

if(M2& FireableM){
#pewFire(Core:toWorld(vec(10,-150,-10)),(Core:forward()),"Basic Rocket Launcher")    
#pewFire(Core:toWorld(vec(10,150,-10)),(Core:forward()),"Basic Rocket Launcher")    
FireableM = 0
    timer("FireableM",200) 
}

if(M1){
#pewFire(Core:toWorld(vec(100,25,20)),Core:forward(),"Gauss Gun")    
}
