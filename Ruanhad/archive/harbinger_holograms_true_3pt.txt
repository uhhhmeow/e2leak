@name Harbinger Holograms True 3Pt
@inputs [Hip FL FR RL RR]:entity [FLHippos FRHippos RLHippos RRHippos Color]:vector Material:string OffsetAng
@outputs
@persist 
interval(50) 

ThighLength = 23*12
MidLength = 10*12
ShinLength = 10*12

Grey = vec(1,1,1)*50
PhxMetallic = "Phoenix_storms/mat/mat_phx_metallic"

function holoData (Index,Model:string,Scale:vector,Pos:vector,Ang:angle,Material:string,Color:vector,Parent:entity){
    holoCreate(Index)
    holoModel(Index,Model)
    ScaleFactor = vec(Scale:y(),Scale:x(),Scale:z())
    holoScale(Index,ScaleFactor)
    holoPos(Index,Pos)
    holoAng(Index,Ang)
    holoMaterial(Index,Material)
    holoColor(Index,Color)
    holoParent(Index,Parent)
}

function number rotationDifference(Out, Out2){
    if((Out - Out2) > 180){Out1b = -(360 - (Out))}else{Out1b = Out}
    if((Out2 - Out) > 180){Out2b = -(360 - (Out2))}else{Out2b = Out2}
    return Out2b - Out1b    
}

function makeLeg3(StartIndex){   
    holoData(StartIndex,"cube",vec(1,1,1),Hip:toWorld(vec(0,0,20)),Hip:toWorld(ang(90,0,0)),"",vec(1,0.25,0.25)*255,Hip) # Main Hypotenuse
    holoData(StartIndex+1,"cube",vec(1,1,1),Hip:toWorld(vec(0,0,20)),Hip:toWorld(ang(90,0,0)),"",vec(1,0.25,0.25)*255,holoEntity(StartIndex)) # Main Hypotenuse
    holoData(StartIndex+2,"cube",vec(1,1,1)*0.3,holoEntity(StartIndex+1):toWorld(vec(0,0,20)),holoEntity(StartIndex+1):toWorld(ang()),"",vec(0,1,0)*255,holoEntity(StartIndex+1)) # Hip Joint
    
    holoData(StartIndex+3,"cube",vec(1,1,1),holoEntity(StartIndex+2):toWorld(vec(0,0,0)),holoEntity(StartIndex+2):toWorld(ang(0,0,0)),"",vec(0,0,1)*255,holoEntity(StartIndex+2)) # Second Hypotenuse (False Thigh)
    
    holoData(StartIndex+4,"hqsphere",vec(1,1,1)*2,holoEntity(StartIndex+3):toWorld(vec(0,0,0)),holoEntity(StartIndex+3):toWorld(ang()),"models/shiny",vec(1,1,1)*75,holoEntity(StartIndex+3)) # Second Hip Joint
    holoData(StartIndex+5,"hqsphere",vec(ThighLength/12,1,1),holoEntity(StartIndex+4):toWorld(vec(-ThighLength/2,0,0)),holoEntity(StartIndex+4):toWorld(ang(0,0,0)),"",vec(1,0.25,0.25)*255,holoEntity(StartIndex+4)) # true Thigh
    
    
    holoData(StartIndex+8,"hqsphere",vec(1,1,1)*1.3,holoEntity(StartIndex+4):toWorld(vec(0,0,0)),holoEntity(StartIndex+4):toWorld(ang()),"models/shiny",vec(1,1,1)*55,holoEntity(StartIndex+4)) # Ankle Joint
    holoData(StartIndex+9,"hqsphere",vec(ShinLength/12,1,1)*0,holoEntity(StartIndex+8):toWorld(vec(ShinLength/2,0,0)),holoEntity(StartIndex+8):toWorld(ang(0,0,0)),"",vec(1,0.25,0.25)*255,holoEntity(StartIndex+8)) # true Shin
    
    holoData(StartIndex+6,"hqsphere",vec(0.75,1,0.75)*1.25,holoEntity(StartIndex+8):toWorld(vec(ShinLength,0,0)),holoEntity(StartIndex+8):toWorld(ang()),"models/shiny",vec(1,1,1)*55,holoEntity(StartIndex+8)) # True Knee Joint
    holoData(StartIndex+7,"hqsphere",vec(2,2,1),holoEntity(StartIndex+6):toWorld(vec(MidLength/2-10,0,0)),holoEntity(StartIndex+6):toWorld(ang(0,0,0)),"models/shiny",vec(1,1,1)*55,holoEntity(StartIndex+6)) # true Midleg

}
function makeLeg3Details(StartIndex){
    holoScale(StartIndex+4,vec())
    holoScale(StartIndex+5,vec())
    holoScale(StartIndex+6,vec())
    holoScale(StartIndex+7,vec())
#holoData(StartIndex+10,"models/Gibs/Scanner_gib01.mdl",                vec(3,1,1.5)*2,      holoEntity(StartIndex+4):toWorld(vec(-ThighLength + 66,0,-10)),holoEntity(StartIndex+4):toWorld(ang(10,0,0)),   PhxMetallic,Grey,holoEntity(StartIndex+4))
#holoData(StartIndex+11,"models/Gibs/Scanner_gib01.mdl",                vec(3,1,1.5)*2,      holoEntity(StartIndex+4):toWorld(vec(-ThighLength + 170+36,0,-10)),holoEntity(StartIndex+4):toWorld(ang(10,180,0)), PhxMetallic,Grey,holoEntity(StartIndex+4))
#holoData(StartIndex+12,"models/props_combine/headcrabcannister01a.mdl",vec(0.75,1,0.5),     holoEntity(StartIndex+4):toWorld(vec(-ThighLength + 50,0,-10)),  holoEntity(StartIndex+4):toWorld(ang(10,0,0)) ,  PhxMetallic,Grey,holoEntity(StartIndex+4))
#holoData(StartIndex+13,"models/Gibs/Scanner_gib01.mdl",                vec(3,1,1.5)*1.8,    holoEntity(StartIndex+4):toWorld(vec(-ThighLength + 170+36,0,-10)),holoEntity(StartIndex+4):toWorld(ang(160,0,0)),  PhxMetallic,Grey,holoEntity(StartIndex+4))
#holoData(StartIndex+14,"models/props_combine/headcrabcannister01a.mdl",vec(0.75,1,0.5)*0.85,holoEntity(StartIndex+4):toWorld(vec(-ThighLength + 250,0,0)),   holoEntity(StartIndex+4):toWorld(ang(-10,0,0)),  PhxMetallic,Grey,holoEntity(StartIndex+4))    

#holoData(StartIndex+15,"models/NatesWheel/nateswheelwide.mdl",          vec(2,1,1)*0.5,      holoEntity(StartIndex+4):toWorld(vec(-ThighLength,0,0)),       holoEntity(StartIndex+4):toWorld(ang(0,90,0)),   PhxMetallic,Grey,holoEntity(StartIndex+4))
#holoData(StartIndex+16,"models/NatesWheel/nateswheelwide.mdl",          vec(2,1,1)*0.5,      holoEntity(StartIndex+6):toWorld(vec(MidLength,0,0)),       holoEntity(StartIndex+6):toWorld(ang(0,90,0)),   PhxMetallic,Grey,holoEntity(StartIndex+6))
#holoData(StartIndex+17,"models/Gibs/Scanner_gib01.mdl",                 vec(1.5,0.5,1)*3,    holoEntity(StartIndex+8):toWorld(vec(ShinLength/2,0,0)),   holoEntity(StartIndex+8):toWorld(ang(0,180,0)),  PhxMetallic,Grey,holoEntity(StartIndex+8))
#holoData(StartIndex+18,"models/Gibs/Scanner_gib01.mdl",                 vec(1.5,0.5,1)*3,    holoEntity(StartIndex+6):toWorld(vec(MidLength/2,0,0)),  holoEntity(StartIndex+6):toWorld(ang(0,0,0)),PhxMetallic,Grey,holoEntity(StartIndex+6))
#holoData(StartIndex+19,"models/XQM/airplanewheel1medium.mdl",           vec(2,1,1)*0.85,     holoEntity(StartIndex+6):toWorld(vec(0,0,0)),   holoEntity(StartIndex+6):toWorld(ang(0,90,0)),   PhxMetallic,Grey,holoEntity(StartIndex+6))

holoData(StartIndex+10,"models/Gibs/Scanner_gib01.mdl",                vec(1.5,3,1)*2,      holoEntity(StartIndex+4):toWorld(vec(-ThighLength + 66,0,-10)),holoEntity(StartIndex+4):toWorld(ang(10,0,0)),   PhxMetallic,Grey,holoEntity(StartIndex+4))
holoData(StartIndex+11,"models/Gibs/Scanner_gib01.mdl",                vec(1.5,3,1)*2,      holoEntity(StartIndex+4):toWorld(vec(-ThighLength + 170+36,0,-10)),holoEntity(StartIndex+4):toWorld(ang(10,180,0)), PhxMetallic,Grey,holoEntity(StartIndex+4))
holoData(StartIndex+12,"models/props_combine/headcrabcannister01a.mdl",vec(1,0.75,0.5),     holoEntity(StartIndex+4):toWorld(vec(-ThighLength + 50,0,-10)),  holoEntity(StartIndex+4):toWorld(ang(10,0,0)) ,  PhxMetallic,Grey,holoEntity(StartIndex+4))
holoData(StartIndex+13,"models/Gibs/Scanner_gib01.mdl",                vec(1.5,3,1)*1.8,    holoEntity(StartIndex+4):toWorld(vec(-ThighLength + 170+36,0,-10)),holoEntity(StartIndex+4):toWorld(ang(160,0,0)),  PhxMetallic,Grey,holoEntity(StartIndex+4))
holoData(StartIndex+14,"models/props_combine/headcrabcannister01a.mdl",vec(1,0.75,0.5)*0.85,holoEntity(StartIndex+4):toWorld(vec(-ThighLength + 250,0,0)),   holoEntity(StartIndex+4):toWorld(ang(-10,0,0)),  PhxMetallic,Grey,holoEntity(StartIndex+4))    

holoData(StartIndex+15,"models/NatesWheel/nateswheelwide.mdl",          vec(1,2,1)*0.5,      holoEntity(StartIndex+4):toWorld(vec(-ThighLength,0,0)),       holoEntity(StartIndex+4):toWorld(ang(0,90,0)),   PhxMetallic,Grey,holoEntity(StartIndex+4))
holoData(StartIndex+16,"models/NatesWheel/nateswheelwide.mdl",          vec(1,2,1)*0.5,      holoEntity(StartIndex+6):toWorld(vec(MidLength,0,0)),       holoEntity(StartIndex+6):toWorld(ang(0,90,0)),   PhxMetallic,Grey,holoEntity(StartIndex+6))
holoData(StartIndex+17,"models/Gibs/Scanner_gib01.mdl",                 vec(1,1.5,0.5)*3,    holoEntity(StartIndex+8):toWorld(vec(ShinLength/2,0,0)),   holoEntity(StartIndex+8):toWorld(ang(0,180,0)),  PhxMetallic,Grey,holoEntity(StartIndex+8))
holoData(StartIndex+18,"models/Gibs/Scanner_gib01.mdl",                 vec(1,1.5,0.5)*3,    holoEntity(StartIndex+6):toWorld(vec(MidLength/2,0,0)),  holoEntity(StartIndex+6):toWorld(ang(0,0,0)),PhxMetallic,Grey,holoEntity(StartIndex+6))
holoData(StartIndex+19,"models/XQM/airplanewheel1medium.mdl",           vec(1,2,1)*0.85,     holoEntity(StartIndex+6):toWorld(vec(0,0,0)),   holoEntity(StartIndex+6):toWorld(ang(0,90,0)),   PhxMetallic,Grey,holoEntity(StartIndex+6))
    
}

function updateColMat(Index){
    holoColor(Index,Color)    
    holoMaterial(Index,Material)    
}

function operateLeg3(StartIndex,HPos:vector,F:vector){
    Avrgpoint = (HPos + (F+vec(0,0,0)))/2
    Hypelength = clamp((HPos - (F+vec(0,0,0))):length(),ThighLength*0.95,ThighLength+MidLength+ShinLength)
    HypeAngle = (HPos - (F+vec(0,0,0))):toAngle() + ang(90,0,0)
    
    holoPos(StartIndex,Avrgpoint)
    holoScale(StartIndex,vec(0.1,0.1,Hypelength/12)*0) # scale of Hype Prime
    holoAng(StartIndex,HypeAngle) # Angle of Hype- Prime    
    
    holoPos(StartIndex+1,Avrgpoint) # 
    holoScale(StartIndex+1,vec(0.5,1,Hypelength/15)*0) # Stuff to do with Hype Secundus
    holoAng(StartIndex+1,holoEntity(StartIndex):toWorld(ang(0,0,0)))
    
    Hypotenuse = Hypelength
    SecondHypotenuse = sqrt( MidLength^2 + ThighLength^2 - 2*MidLength*ThighLength*cos(140) )
    
    holoScale(StartIndex+3,vec(SecondHypotenuse/12,0.25,0.25)*0) # true Thigh
    holoPos(StartIndex+2,holoEntity(StartIndex+1):toWorld(vec(0,0,-Hypotenuse/2))) # false hip
    
    BodyAng = acos((SecondHypotenuse^2 + Hypotenuse^2 - ShinLength^2)/(2*SecondHypotenuse*Hypotenuse)) - 90
    holoAng(StartIndex+2,holoEntity(StartIndex+1):toWorld(ang(-BodyAng,0,0))) # false hip
    
    holoPos(StartIndex+3,holoEntity(StartIndex+2):toWorld(vec(-SecondHypotenuse/2,0,0))) # position the secondary hypotenuse
    
    holoPos(StartIndex+8,holoEntity(StartIndex+4):toWorld(vec(-ThighLength,0,0)))
    KneeAng = acos((SecondHypotenuse^2 + ShinLength^2 - Hypotenuse^2)/(2*SecondHypotenuse*ShinLength))
    holoAng(StartIndex+8,holoEntity(StartIndex+2):toWorld(ang(-KneeAng,0,0))) # ankle
    
    holoPos(StartIndex+4,holoEntity(StartIndex+3):toWorld(vec(SecondHypotenuse/2,0,0))) # position the True Hip
    FalseBodyAng = acos(((ThighLength)^2 + SecondHypotenuse^2 - MidLength^2)/(2*(ThighLength)*SecondHypotenuse))
    holoAng(StartIndex+4,holoEntity(StartIndex+3):toWorld(ang(-FalseBodyAng,0,0))) # true hip
    
    FalseKneeAng = acos(((ThighLength)^2 + MidLength^2 - SecondHypotenuse^2)/(2*(ThighLength)*MidLength))
    holoAng(StartIndex+6,holoEntity(StartIndex+4):toWorld(ang(-FalseKneeAng,0,0))) # true knee
}

if(first()){        
timer("FL",1200*0)    
timer("FLDetails",1200*1)    
timer("FR",1200*2)
timer("FRDetails",1200*3)
timer("RL",1200*4)
timer("RLDetails",1200*5)
timer("RR",1200*6)   
timer("RRDetails",1200*7)  
timer("final",1200*8) 
}

if(clk("FL")){makeLeg3(0)}
if(clk("FLDetails")){makeLeg3Details(0)}
if(clk("FR")){makeLeg3(20)}
if(clk("FRDetails")){makeLeg3Details(20)}
if(clk("RL")){makeLeg3(40)}
if(clk("RLDetails")){makeLeg3Details(40)}
if(clk("RR")){makeLeg3(60)}
if(clk("RRDetails")){makeLeg3Details(60)}

operateLeg3(0,Hip:toWorld(vec(30,30,0)),FL:pos())
operateLeg3(20,Hip:toWorld(vec(30,-30,0)),FR:pos())
operateLeg3(40,Hip:toWorld(vec(-30,30,0)),RL:pos())
operateLeg3(60,Hip:toWorld(vec(-30,-30,0)),RR:pos())

updateColMat(12)
updateColMat(14)

updateColMat(32)
updateColMat(34)

updateColMat(52)
updateColMat(54)

updateColMat(72)
updateColMat(74)

if(clk("final")){
    for(I=1,100){
        holoVisible(I,players(),1)    
    }
}
