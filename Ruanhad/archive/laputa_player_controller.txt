@name Laputa Player controller
@inputs [Controls Camera]:wirelink DisallowMovement Base:entity Deck
@outputs ProjectedMovement:vector2 [OutPos OutDir]:vector
@persist Pos:vector Ang:angle DeckInt
interval(50)
function entity holoData(Index,Model:string,Scale:vector,Pos:vector,Ang:angle,Material:string,Color:vector,Parent:entity){
    holoCreate(Index)
    holoModel(Index,Model)
    holoScale(Index,Scale)
    holoPos(Index,Pos)
    holoAng(Index,Ang)
    holoMaterial(Index,Material)
    holoColor(Index,Color)
    holoParent(Index,Parent)
    return holoEntity(Index)
}

#start controls
W = Controls:number("W")
A = Controls:number("A")
S = Controls:number("S")
D = Controls:number("D")
R = Controls:number("R")
M1 = Controls:number("Mouse1")
M2 = Controls:number("Mouse2")
Space = Controls:number("Space")
Alt = Controls:number("Alt")
Shift = Controls:number("Shift")#
Mwheelup = Controls:number("NextWeapon")
Mwheeldown = Controls:number("PrevWeapon")
Active = Controls:number("Active")
Seat = Controls:entity("Entity")

WASD = (W || A || S || D)

if(Mwheelup && changed(Mwheelup)){
    DeckInt--
}
if(Mwheeldown && changed(Mwheeldown)){
    DeckInt++   
}

if(first()){
   holoData(1,"models/props_lab/huladoll.mdl",vec(1,1,1)*1,Base:toWorld(vec(0,0,0)),Base:toWorld(ang()),"",vec(255),Base)     
   #holoData(1,"cube",vec(0.25,0.25,0.5)*1,Base:toWorld(vec(0,0,0)),Base:toWorld(ang()),"",vec(255),Base)     
   holoData(2,"models/Gibs/HGIBS.mdl",vec(0.3)*1,holoEntity(1):toWorld(vec(0,0,5.5)),holoEntity(1):toWorld(ang()),"",vec(255),holoEntity(1))     
}
Camvec = Base:toLocalAxis((Seat:driver():eyeAngles()):forward())

Ang = (Seat:driver():eye()):toAngle():setPitch(0)
Pos = Pos + (Seat:driver():eye()):toAngle():setPitch(0):forward()*(W-S)*(1+Shift) + (Seat:driver():eye()):toAngle():setPitch(0):right()*(D-A)*(1+Shift)#vec((W-S),A-D,0)*(1 + Shift)
LocalPos = Pos + vec(0,0,DeckInt*15)
holoPos(1,Base:toWorld(LocalPos + vec(0,0,2)))
holoAng(1,Base:toWorld(Ang))
Pitch = Seat:driver():eye():toAngle():pitch()
holoAng(2,holoEntity(1):toWorld(ang(Pitch,0,0)))

Campos = LocalPos + vec(0,0,7)
Camvec = Base:toWorld(Ang + ang(Pitch,0,0)):forward()

Camera:setNumber("Activated",Active)
Camera:setVector("Position",Base:toWorld(Campos))
Camera:setAngle("Angle",Base:toWorld(Ang + ang(Pitch,0,0)))

OutPos = LocalPos
OutDir = Camvec
