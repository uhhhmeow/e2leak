@name Forcefield Module V3
@inputs Core:entity Radius
@outputs Players:array InterS:array
@persist 
interval(50)
function vector intersection(InboundVector:vector,Tpos:vector){
    R = max(Radius,100)
    Center = entity():pos()
    P0 = InboundVector
    P1 = Tpos
    
    X0 = P0:x()
    Y0 = P0:y()
    Z0 = P0:z()
    
    X1 = P1:x()
    Y1 = P1:y()
    Z1 = P1:z()
    
    CX = Center:x()
    CY = Center:y()
    CZ = Center:z()
    
    DX = X1 - X0
    DY = Y1 - Y0
    DZ = Z1 - Z0
    
    LDir = P1 - P0
    SD = P0 - Center

    A = LDir:length()^2
    B = 2 * SD:dot(LDir)
    C = SD:length()^2 - R^2
    
    Discriminant= (B^2) - 4*A*C
    T = (-B-sqrt(Discriminant))/(2*A)
    
    Pos = vec(X0+T*DX,Y0+T*DY,Z0+T*DZ)
    return Pos
}

function holoData (Index,Model:string,Scale:vector,Pos:vector,Ang:angle,Mat:string,Color:vector,Parent:entity){
    holoCreate(Index)
    holoModel(Index,Model)
    holoScale(Index,Scale)
    holoPos(Index,Pos)
    holoAng(Index,Ang)
    holoMaterial(Index,Mat)
    holoColor(Index,Color)
    holoParent(Index,Parent)
}

if(first()){
    holoData(20,"icosphere3",vec(1,1,1)*2*max(Radius/12,100/12),entity():toWorld(vec(0,0,0)),entity():toWorld(ang(0,0,0)),"models/debug/debugwhite",vec(0.5,0.5,1)*255,entity())
    holoData(21,"icosphere3",vec(1,1,1)*-2*max(Radius/12,100/12)*0.99,entity():toWorld(vec(0,0,0)),entity():toWorld(ang(0,0,0)),"models/debug/debugwhite",vec(0.5,0.5,1)*255,entity())
    holoDisableShading(20,1) 
    holoDisableShading(21,1) 
timer("generateShields",0)    
}

if(clk("generateShields")){
    for(I=1,15){
        holoData(I,"cube",vec(1,1,1),entity():toWorld(vec(0,0,0)),entity():toWorld(ang(0,0,0)),"models/gibs/metalgibs/metal_gibs",vec(1,1,1)*255,entity())
    }
    
}
Players = players()

for(I=1,Players:count()){
    InterS[I,vector] = intersection(Players[I,entity]:shootPos(),Players[I,entity]:aimPos())
}

for(I=1,15){
    holoPos(I,InterS[I,vector])    
}



#Player:shootpos()

