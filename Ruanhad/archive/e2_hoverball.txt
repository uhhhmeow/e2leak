@name E2 Hoverball
@inputs In OnOff
@outputs 
@persist E:entity Persist Offset:vector
runOnTick(1)

if(first()){
    E = entity():isWeldedTo() 
    Persist = entity():pos():z()   
    Offset = E:toLocal(entity():pos())
}

function entity:hover(Altitude:number){
    #if(This:toWorld(Offset):z() < Altitude){
        This:applyOffsetForce(((vec(This:toWorld(Offset):x(),This:toWorld(Offset):y(),Altitude)-This:toWorld(Offset))*15-This:vel())*(This:mass()/3),This:toWorld(Offset))    
   # }
}
Persist+=In
E:hover(Persist)
