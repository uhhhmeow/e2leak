@name Iris Door - Propcore
@inputs Open 
@outputs Parts:array
@persist J Side Frame:entity
#@model models/hunter/misc/platehole4x4.mdl 
interval(50)
function holodata (Index,Model:string,Scale:vector,Pos:vector,Ang:angle,Material:string,Color:vector,Parent:entity){
    holoCreate(Index)
    holoModel(Index,Model)
    holoScale(Index,Scale)
    holoPos(Index,Pos)
    holoAng(Index,Ang)
    holoMaterial(Index,Material)
    holoColor(Index,Color)
    holoParent(Index,Parent)
    
}
if(first()){
    Frame = entity():isWeldedTo()
    for(I=1,15){
holodata(I,"models/hunter/plates/tri2x1.mdl",vec(1.25,1,0.1)*1.5,entity():toWorld(vec(0,0,-10)),entity():toWorld(ang()),"Phoenix_storms/chrome",vec(1,1,1)*50,Frame)
}

}
if(Parts:count() < 15 && Frame:model() == "models/hunter/misc/platehole4x4.mdl"){
Parts:insertEntity(1,propSpawn("models/hunter/triangles/4x4.mdl",vec(),ang(),1))
Parts[1,entity]:setAlpha(0)    
}
if(Open && Side > 52.5){Side-=5}
if(Open && Side < 52.5){Side+=5}
if(!Open && Side > -30){Side-=5}
if(!Open && Side < -30){Side+=5}

if(changed(Side)){
for(I=1,15){
    X = cos(I*24)*(40+Side)
    Y = sin(I*24)*(40+Side)
holoPos(I,Frame:toWorld(vec(X,Y,0)))    
holoAng(I,Frame:toWorld(ang(0,I*24 + 180-(Side/1.25),0))) 
Parts[I,entity]:setPos(holoEntity(I):pos())
Parts[I,entity]:setAng(holoEntity(I):toWorld(ang(0,-90,0)))
Parts[I,entity]:propFreeze(1)
}
}
