# ----------------------------------------------------------------------------------------------------------------------------- #
# ____                                      __          __  __          ___                                                     #
#/\  _ `.                                  /\ \        /\ \/\ \        /\_ \                                                    #
#\ \ \/\ \  _ __    __     __      ___ ___ \ \/ ____   \ \ \_\ \    ___\//\ \     ___      __   _ __    __      ___ ___         #
# \ \ \ \ \/\`'__\/'__`\ /'__`\  /' __` __`\\/ /',__\   \ \  _  \  / __`\\ \ \   / __`\  /'_ `\/\`'__\/'__`\  /' __` __`\       #
#  \ \ \_\ \ \ \//\  __//\ \ \.\_/\ \/\ \/\ \ /\__, `\   \ \ \ \ \/\ \ \ \\_\ \_/\ \ \ \/\ \ \ \ \ \//\ \ \.\_/\ \/\ \/\ \      #
#   \ \____/\ \_\\ \____\ \__/.\_\ \_\ \_\ \_\\/\____/    \ \_\ \_\ \____//\____\ \____/\ \____ \ \_\\ \__/.\_\ \_\ \_\ \_\     #
#    \/___/  \/_/ \/____/\/__/\/_/\/_/\/_/\/_/ \/___/      \/_/\/_/\/___/ \/____/\/___/  \/___ \ \/_/ \/__/\/_/\/_/\/_/\/_/     #
#                                                                                          /\____/                              #
#                                                                                          \_/__/                               #
# ----------------------------------------------------------------------------------------------------------------------------- #
# Modeler Version: v5.1
# Date: 1/10/2019 @ 1238
# Holograms: 5
# Notes: 
# --------------------------------------------- #
@name eyeball
@persist Hologram:table Player:array Target:entity HoloEnt:entity Cam Seat:entity Driver:entity

if( first( ) ) {

    Hologram = table()
    Hologram[ "model", array ] = array("models/holograms/hq_sphere.mdl","models/holograms/hq_sphere.mdl","models/holograms/hq_sphere.mdl","models/holograms/hq_sphere.mdl","models/holograms/hq_torus.mdl")
    Hologram[ "position", array ] = array(vec(0,0,38.209),vec(0,0,38.209),vec(7.4009,0,38.209),vec(9.001,0,38.209),vec(6.4011,0,38.209))
    Hologram[ "angle", array ] = array(ang(0,0,0),ang(0,0,0),ang(0,0,0),ang(0,0,0),ang(90,0,0))
    Hologram[ "scale", array ] = array(vec(1.7,1.7,1.7),vec(-1.9,-1.9,-1.9),vec(0.4,1.2,1.2),vec(0.4,0.7,0.7),vec(1.3,1.3,1.3))
    Hologram[ "color", array ] = array(vec(100,100,100),vec(0,0,0),vec(100,50,50),vec(0,0,0),vec(100,70,70))
    Hologram[ "parent", array ] = array(0,1,1,1,1)

    function buffer( ) {
        local Next = Hologram[ "next", number ]
        for( I = Next, Next + clamp( holoRemainingSpawns( ), 0, Hologram[ "model", array ]:count( ) - Next + 1 ) ) {
            holoCreate(
                I,
                entity():toWorld(Hologram["position", array][I, vector]),
                Hologram["scale", array][I, vector],
                entity():toWorld(Hologram["angle", array][I, angle]),
                vec4(Hologram["color", array][I, vector], 255),
                Hologram["model", array][I, string]
            )
            holoMaterial(I, "debug/debugdrawflat")
            holoParent(I, 1)
            Hologram[ "next", number ] = I
        }
        if( Next <= Hologram[ "model", array ]:count( ) ) { timer( "buffer", 0.1 ) } else {
            interval( 333 )
            Player = players()
            Target = owner()
            Pos = entity():pos() + entity():up() * 12
            holoPos( 1, Pos )
            cameraCreate( 1, Pos, ang(0) )
            #cameraToggle( 1, 1 )
            HoloEnt = holoEntity( 1 )
            runOnTick( 1 )
            runOnKeys( owner(), 1 )
            Cam = 0
            holoParent( 1, entity() )
        }
    } buffer( )
    
}

if( clk( "buffer" ) ) { buffer( ) } elseif( clk( ) ) {
    interval( 333 )
    local Dist = 1000000
    for( I = 1, Player:count() ) {
        local D = Player[ I, entity ]:pos():distance( entity():pos() )
        if( D < Dist ) {
            Target = Player[ I, entity ]
            Dist = D
        }
    }
}

if( Cam ) {
    Ang = owner():eye():toAngle()
    holoAng( 1, Ang )
    cameraPos( 1, HoloEnt:pos() + HoloEnt:forward() * 18 )
    cameraAng( 1, Ang )
}

if( keyClk() ) {
    if( keyClkPressed() == "g" & keyClk():keyPressed( "g" ) ) {
        Cam = !Cam
        cameraToggle( 1, Cam )
    }
}

if( owner():keyForward() ) {
    holoPos( 1, holoEntity( 1 ):pos() + holoEntity( 1 ):forward() * 5 )
}

