@name EGP Text Screen
@persist Size Text:string
@inputs Scr:wirelink
if (first()) {
    Size=40 #Size
    Text="I am the text"
}
Scr:egpClear()
Scr:egpText(1,Text,vec2(256,256))
Scr:egpSize(1,Size)
Scr:egpAlign(1,1,1)
