@name Elevatoratronifacator
@inputs C AE:entity BE:entity CE:entity DE:entity
@outputs Dist
@persist Min Moving Users:array IsAllowed A B
Users[1,string]=owner():name()
Users[2,string]="Obsidian legs"
Users[3,string]="[JokerIce] Wingflyer"
Min=20
Max=1605
if (first()) {Dist=Min}
if (A==1 & IsAllowed) {timer("A",100) Moving=1}
if (clk("A") &  Moving) {Dist+=10 timer("A",100)}
if (B==1) {timer("B",100) Moving=1}
if (clk("B") & B & Moving) {Dist-=10 timer("B",100)}
if (C==1) {Moving=0}
if (changed(AE) & AE) {
    IsAllowed=0
    for (I=1,Users:count()) {
        if (AE:name()==Users[I,string]) {
            A=1
            IsAllowed=1
            Moving=1
            timer("A",100)
        }
    
}
}
if (Dist>Max) {Dist=Max A=0 B=0 Moving=0}
if (Dist<Min) {Dist=Min A=0 B=0 Moving=0}
if (changed(BE) & BE) {
    IsAllowed=0
    for (I=1,Users:count()) {
        if (BE:name()==Users[I,string]) {
            IsAllowed=1
            B=1
            Moving=1
            timer("B",100)
        }
    
}
}
if (changed(CE) & CE) {
    IsAllowed=0
    for (I=1,Users:count()) {
        if (CE:name()==Users[I,string]) {
            IsAllowed=1
            Moving=1
            stoptimer("A")
            A=0
            B=1
            timer("B",100)
        }
    
}
}
if (changed(DE) & DE) {
    IsAllowed=0
    for (I=1,Users:count()) {
        if (DE:name()==Users[I,string]) {
            IsAllowed=1
            Moving=1
            stoptimer("B")
            B=0
            A=1
            timer("A",100)
        }
    
}
}
if (C==1) {A=0 B=0 Moving=0}
