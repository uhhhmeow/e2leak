@name Auto Collecter v2
@inputs EGP:wirelink E:entity
@outputs 
@persist

if(first()) {

    
    function clear() {  
    EGP:egpClear()
    }
    Timer = 2
    function timer() {
        clear()
        
        #Design
        EGP:egpBox(1, vec2(256,256), vec2(526,526))
        EGP:egpColor(1, vec(30,30,30))
        EGP:egpRoundedBox(2, vec2(250,10), vec2(425,100))
        EGP:egpColor(2,vec(50,50,50))
        EGP:egpAlign(2,1)
        EGP:egpRoundedBox(3,vec2(250,250),vec2(300,300))
        EGP:egpColor(3,vec(60,60,60))
        
        
        #Text
        EGP:egpText(5,"Auto Collector!", vec2(250,10))
        EGP:egpAlign(5,1)
        EGP:egpSize(5,40)
        EGP:egpText(18,E:damage,vec2(250,170))
        EGP:egpSize(18,250)
        EGP:egpAlign(18,1)
    }
}
interval(500)
