@name Cookie Clicker
@inputs EGP:wirelink User:entity 
@outputs Fire Cookies
@persist Index  Per X Y
@trigger 
Cookies2 = Cookies:toString()
Per2 = Per:toString()
if(first() || duped())
    {
    X = 50
    Y = 100
    function clear(){EGP:egpClear()}
    
    function void createBox(Index,Pos:vector2,Size:vector2,Color:vector,Align){
    EGP:egpBox(Index,Pos,Size)
    EGP:egpColor(Index,Color)
    }
    
    function void createBoxOutline(Index,Pos:vector2,Size:vector2,Color:vector,Align){
    EGP:egpBoxOutline(Index,Pos,Size)
    EGP:egpColor(Index,Color)
    EGP:egpAlign(Index,Align)
    }
    
    function void createCircle(Index,Pos:vector2,Size:vector2,Color:vector,Align){
    EGP:egpCircle(Index,Pos,Size)
    EGP:egpColor(Index,Color)
    EGP:egpAlign(Index,Align)
    }
    
    function void createCircleOutline(Index,Pos:vector2,Size:vector2,Color:vector,Align){
    EGP:egpCircleOutline(Index,Pos,Size)
    EGP:egpColor(Index,Color)
    EGP:egpAlign(Index,Align)
    }
    
    function void createRoundedBox(Index,Pos:vector2,Size:vector2,Color:vector,Align){
    EGP:egpRoundedBox(Index,Pos,Size)
    EGP:egpColor(Index,Color)
    EGP:egpAlign(Index,Align)
    }
    
    function void createRoundedBoxOutline(Index,Pos:vector2,Size:vector2,Color:vector,Align){
    EGP:egpRoundedBoxOutline(Index,Pos,Size)
    EGP:egpColor(Index,Color)
    EGP:egpAlign(Index,Align)
    }
    
    function void createText(Index,Text:string,Pos:vector2,Size,Color:vector,Align,Alpha){
    EGP:egpText(Index,Text,Pos)
    EGP:egpColor(Index,Color)
    EGP:egpAlign(Index,Align)
    EGP:egpSize(Index,Size)
    EGP:egpAlpha(Index,Alpha)
    }
    
    function void createIcon(Index,Pos:vector2,Size:vector2,Color:vector,Align,String:string){
    EGP:egpBox(Index,Pos,Size)
    EGP:egpColor(Index,Color)
    EGP:egpAlign(Index,Align)
    EGP:egpMaterial(Index,String)
    }
    
    function void parentCursor(Parent){
        EGP:egpParentToCursor(Parent)
    }
    
    #Pages#
    function homepage()
    {
    timer("homepage",1)
    clear()
    createBox(1,vec2(250,250),vec2(525,525),vec(0,102,255),1)
    createRoundedBox(2,vec2(250,60),vec2(305,100),vec(255,255,255),1)
    createText(3,"Cookie Clicker!",vec2(250,30),50,vec(95,95,95),1,255)
    createCircle(4,vec2(250,300),vec2(120,120),vec(127,70,0),1)
    createCircle(5,vec2(250,290),vec2(10,10),vec(100,50,0),1)
    createBox(6,vec2(200,360),vec2(15,15),vec(100,50,0),1)
    createRoundedBox(7,vec2(180,270),vec2(20,20),vec(100,50,0),1)
    createRoundedBox(8,vec2(270,220),vec2(20,20),vec(100,50,0),1)
    createBox(9,vec2(340,260),vec2(15,15),vec(100,50,0),1)
    createBox(10,vec2(290,350),vec2(15,15),vec(100,50,0),1)
    createText(11,"+1",vec2(200,250),40,vec(150,150,150),1,100)
    createText(12,"+1",vec2(300,300),40,vec(150,150,150),1,100)
    createText(13,"+1",vec2(260,250),40,vec(150,150,150),1,100)
    createText(14,"+1",vec2(200,300),40,vec(150,150,150),1,100)
    createIcon(25,vec2(400,250),vec2(50,50),vec(255,255,255),1,"sprites/arrow")
    parentCursor(25)
    }
    
    function cookiePage() {
    stoptimer("homepage")
    timer("CP",1000)
    clear()
    createBox(15,vec2(250,250),vec2(525,525),vec(0,102,255),1)
    createRoundedBox(16,vec2(250,60),vec2(305,100),vec(255,255,255),1) 
    createText(17,"Click Click!",vec2(250,30),50,vec(95,95,95),1,255)
    createCircle(18,vec2(250,300),vec2(120,120),vec(127,70,0),1)
    createCircle(19,vec2(250,290),vec2(10,10),vec(100,50,0),1)
    createBox(20,vec2(200,360),vec2(15,15),vec(100,50,0),1)
    createRoundedBox(21,vec2(180,270),vec2(20,20),vec(100,50,0),1)
    createRoundedBox(22,vec2(270,220),vec2(20,20),vec(100,50,0),1)
    createBox(23,vec2(340,260),vec2(15,15),vec(100,50,0),1)
    createBox(24,vec2(290,350),vec2(15,15),vec(100,50,0),1)
    createIcon(25,vec2(400,250),vec2(50,50),vec(255,255,255),1,"sprites/arrow")
    parentCursor(25)
    createText(26,"You Win 10$ Per 100 Cookies!",vec2(250,450),30,vec(255,255,255),1,255)
    
    }
    
    homepage()
    }
    if("homepage"){
    if(inrange(EGP:egpCursor(User),EGP:egpPos(4)-EGP:egpSize(4)/2,EGP:egpPos(4)+EGP:egpSize(4)/2))
    {cookiePage()}
    }
    
    if (User:keyUse()){
    if(inrange(EGP:egpCursor(User),EGP:egpPos(18)-EGP:egpSize(18)/2,EGP:egpPos(18)+EGP:egpSize(18)/2)){
    Cookies = Cookies+1}
    createText(29,Cookies2,vec2(250,150),30,vec(255,255,255),1,255)
    if(Cookies == Y){
        Y = Y+100
        moneyGive(User,10)
    }
}
timer("1",100)
timer("2",250)
if(clk("1")){Fire = 0}
if(clk("2")){Fire = 1}
