@name GunLocker_v6
@inputs EGP:wirelink [User R1 R2 R3 R4]:entity
@outputs Fire1 Fire2 Fire3 Fire4
@persist G GS A B C D A2 Loading
@trigger 
if(first()){
    
    function void clear(){EGP:egpClear()}
    
    function void createBox(Index,Pos:vector2,Size:vector2,Color:vector,Align){
    EGP:egpBox(Index,Pos,Size)
    EGP:egpColor(Index,Color)
    }
    
    function void createBoxOutline(Index,Pos:vector2,Size:vector2,Color:vector,Align){
    EGP:egpBoxOutline(Index,Pos,Size)
    EGP:egpColor(Index,Color)
    EGP:egpAlign(Index,Align)
    }
    
    function void createCircle(Index,Pos:vector2,Size:vector2,Color:vector,Align){
    EGP:egpCircle(Index,Pos,Size)
    EGP:egpColor(Index,Color)
    EGP:egpAlign(Index,Align)
    }
    
    function void createCircleOutline(Index,Pos:vector2,Size:vector2,Color:vector,Align){
    EGP:egpCircleOutline(Index,Pos,Size)
    EGP:egpColor(Index,Color)
    EGP:egpAlign(Index,Align)
    }
    
    function void createRoundedBox(Index,Pos:vector2,Size:vector2,Color:vector,Align){
    EGP:egpRoundedBox(Index,Pos,Size)
    EGP:egpColor(Index,Color)
    EGP:egpAlign(Index,Align)
    }
    
    function void createRoundedBoxOutline(Index,Pos:vector2,Size:vector2,Color:vector,Align){
    EGP:egpRoundedBoxOutline(Index,Pos,Size)
    EGP:egpColor(Index,Color)
    EGP:egpAlign(Index,Align)
    }
    
    function void createText(Index,Text:string,Pos:vector2,Size,Color:vector,Align,Alpha){
    EGP:egpText(Index,Text,Pos)
    EGP:egpColor(Index,Color)
    EGP:egpAlign(Index,Align)
    EGP:egpSize(Index,Size)
    EGP:egpAlpha(Index,Alpha)
    }
    
    function void createIcon(Index,Pos:vector2,Size:vector2,Color:vector,Align,String:string){
    EGP:egpBox(Index,Pos,Size)
    EGP:egpColor(Index,Color)
    EGP:egpAlign(Index,Align)
    EGP:egpMaterial(Index,String)
    }
    
    function void createSound(Index,String:string,Pitch,Volume){
    soundPlay(Index,500,String)
    soundPitch(Index,Pitch)
    soundVolume(Index,Volume)
    }
    
    function void changeFont(Index,String:string){
    #WireGPU_ConsoleFont
    #Coolvetica
    #Arial
    #Lucida Console
    #Trebuchet
    #Courier New
    #Times New Roman
    #ChatFont
    #Marlett
    EGP:egpFont(Index,String)
    }
    
    function number button(Player:entity,ID){
    return inrange(EGP:egpCursor(Player),EGP:egpPos(ID)-(EGP:egpSize(ID)/2),EGP:egpPos(ID)+(EGP:egpSize(ID)/2))
    }
    
    function void outOfStock(Entity:entity,Pos:vector2,ID,ID2){
    if(G){createText(ID,Entity:shipmentName(),Pos,30,vec(255),1,255)}
    if(Entity:shipmentName() == "Invalid Shipment" & G){createText(ID,"Out of Stock!",Pos,30,vec(255),1,255) EGP:egpColor(ID2,vec(100,0,0))}
    else {EGP:egpColor(ID2,vec(0,100,0))}
    }
    

    
    function void shipAmount(Entity:entity,Pos:vector2,ID){
    if(G){createText(ID,"Amount: "+Entity:shipmentAmount(),Pos,30,vec(255),1,255)}
    if(Entity:shipmentAmount() == -1 & G){createText(ID,"Amount: 0",Pos,30,vec(255),1,255)}
    }
    

    function void homePage(){
    clear()
    createBox(1,vec2(250,250),vec2(525,525),vec(30,30,30),1)
    createBox(2,vec2(29,250),vec2(58,525),vec(100,0,0),1)
    createRoundedBox(3,vec2(275,10),vec2(400,100),vec(60,60,60),1)
    createRoundedBox(4,vec2(275,200),vec2(200,100),vec(60,60,60),1)
    createRoundedBox(5,vec2(275,350),vec2(200,100),vec(60,60,60),1)
    createCircle(6,vec2(28,30),vec2(20,20),vec(255,255,255),1)
    createCircleOutline(7,vec2(28,250),vec2(20,20),vec(255,255,255),1)
    createCircleOutline(8,vec2(28,480),vec2(20,20),vec(255,255,255),1)
    createBox(9,vec2(28,140),vec2(10,180),vec(255,255,255),1)
    createBox(10,vec2(28,365),vec2(10,190),vec(255,255,255),1)
    createText(11,"1",vec2(28,5),45,vec(30,30,30),1,255)
    createText(12,"2",vec2(28,225),45,vec(30,30,30),1,255)
    createText(13,"3",vec2(28,456),45,vec(30,30,30),1,255)
    createText(14,"Welcome to Gunlocker",vec2(275,10),40,vec(255,255,255),1,255)
    createText(15,"Begin!",vec2(270,175),50,vec(255,255,255),1,255)
    createText(16,"Credits",vec2(275,325),50,vec(255,255,255),1,255)
    G = 0
    }
    
    function void gunPage(){
    clear()
    createBox(17,vec2(250,250),vec2(525,525),vec(30,30,30),1)
    createBox(18,vec2(29,250),vec2(58,525),vec(100,0,0),1)
    createRoundedBox(19,vec2(275,10),vec2(400,100),vec(60,60,60),1)
    createCircleOutline(20,vec2(28,30),vec2(20,20),vec(255,255,255),1)
    createCircle(21,vec2(28,250),vec2(20,20),vec(255,255,255),1)
    createCircleOutline(22,vec2(28,480),vec2(20,20),vec(255,255,255),1)
    createBox(23,vec2(28,140),vec2(10,180),vec(255,255,255),1)
    createBox(24,vec2(28,365),vec2(10,190),vec(255,255,255),1)
    createRoundedBox(25,vec2(190,200),vec2(190,110),vec(100,0,0),1)
    createRoundedBox(26,vec2(390,200),vec2(190,110),vec(100,0,0),1)
    createRoundedBox(27,vec2(190,350),vec2(190,110),vec(100,0,0),1)
    createRoundedBox(28,vec2(390,350),vec2(190,110),vec(100,0,0),1)
    createText(29,"1",vec2(28,5),45,vec(30,30,30),1,255)
    createText(30,"2",vec2(28,225),45,vec(30,30,30),1,255)
    createText(31,"3",vec2(28,456),45,vec(30,30,30),1,255)
    createText(32,"Choose Your Gun!",vec2(275,10),40,vec(255,255,255),1,255)
    G = 1
    }
    
    function void checkoutPage(){
    clear()
    createBox(42,vec2(250,250),vec2(525,525),vec(30,30,30),1)
    createBox(43,vec2(29,250),vec2(58,525),vec(100,0,0),1)
    createRoundedBox(44,vec2(275,10),vec2(400,100),vec(60,60,60),1)
    createCircleOutline(45,vec2(28,30),vec2(20,20),vec(255,255,255),1)
    createCircleOutline(46,vec2(28,250),vec2(20,20),vec(255,255,255),1)
    createCircle(47,vec2(28,480),vec2(20,20),vec(255,255,255),1)
    createBox(48,vec2(28,140),vec2(10,180),vec(255,255,255),1)
    createBox(49,vec2(28,365),vec2(10,190),vec(255,255,255),1)
    createRoundedBox(51,vec2(270,250),vec2(250,100),vec(50),1)
    createText(52,"1",vec2(28,5),45,vec(30,30,30),1,255)
    createText(53,"2",vec2(28,225),45,vec(30,30,30),1,255)
    createText(54,"3",vec2(28,456),45,vec(30,30,30),1,255)
    createText(55,"Checkout!",vec2(275,10),40,vec(255,255,255),1,255)
    createText(56,"Checkout",vec2(270,230),40,vec(255),1,255)
    G = 0
    }
    
    function void loadingPage(){
    clear()
    timer("loadingPage",500)
    createBox(57,vec2(250,250),vec2(525,525),vec(30,30,30),1)
    createBox(58,vec2(29,250),vec2(58,525),vec(100,0,0),1)
    createRoundedBox(59,vec2(275,10),vec2(400,100),vec(60,60,60),1)
    createCircleOutline(60,vec2(28,30),vec2(20,20),vec(255,255,255),1)
    createCircleOutline(61,vec2(28,250),vec2(20,20),vec(255,255,255),1)
    createCircleOutline(62,vec2(28,480),vec2(20,20),vec(255,255,255),1)
    createBox(63,vec2(28,140),vec2(10,180),vec(255,255,255),1)
    createBox(64,vec2(28,365),vec2(10,190),vec(255,255,255),1)
    createText(65,"1",vec2(28,5),45,vec(30,30,30),1,255)
    createText(66,"2",vec2(28,225),45,vec(30,30,30),1,255)
    createText(67,"3",vec2(28,456),45,vec(30,30,30),1,255)
    createText(69,"Loading...",vec2(275,10),40,vec(255,255,255),1,255)
    G = 0
    }
    
    homePage()
    }
if(clk("loadingPage") & Loading == 1){gunPage()}
if(clk("loadingPage") & Loading == 2){checkoutPage()}
if(clk("loadingPage") & Loading == 3){checkoutPage()}
if(clk("loadingPage") & Loading == 4){checkoutPage()}
if(clk("loadingPage") & Loading == 5){checkoutPage()}
if(User:keyUse()){
    if(button(User,4)){Loading = 1 loadingPage()} 
    if(button(User,25) & A){loadingPage() Loading = 2 A2 = 1 P1 = 10}
    if(button(User,26) & B){loadingPage() Loading = 3 A2 = 2 P1 = 10}
    if(button(User,27) & C){loadingPage() Loading = 4 A2 = 3 P1 = 10}
    if(button(User,28) & D){loadingPage() Loading = 5 A2 = 4 P1 = 10}
    if(button(User,51) & A2 == 1){moneyRequest(User,P1,30,"R1")}
    if(button(User,51) & A2 == 2){moneyRequest(User,P1,30,"R2")}
    if(button(User,51) & A2 == 3){moneyRequest(User,P1,30,"R3")}
    if(button(User,51) & A2 == 4){moneyRequest(User,P1,30,"R4")}
}
#Shipment Detections/shipment reqirements
outOfStock(R1,vec2(190,150),33,25)
shipAmount(R1,vec2(175,220),34)
outOfStock(R2,vec2(390,150),35,26)
shipAmount(R2,vec2(375,220),36)
outOfStock(R3,vec2(190,300),38,27)
shipAmount(R3,vec2(175,370),39)
outOfStock(R4,vec2(390,300),40,28)
shipAmount(R4,vec2(375,370),41)
if(R1:shipmentAmount() > 0){A = 1 }else{A = 0}
if(R2:shipmentAmount() > 0){B = 1 }else{B = 0}
if(R3:shipmentAmount() > 0){C = 1 }else{C = 0}
if(R4:shipmentAmount() > 0){D = 1 }else{D = 0}
print(A2)
