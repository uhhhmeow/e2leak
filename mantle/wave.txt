# Mantle Wave Functions
# Author: WingFlyer

if(first()) {
    function number sinWave(X:number,A:number, Hz:number, W:number){
        return A*sin(2*pi()*Hz*X + W)
    }
    
    function number cosWave(X:number,A:number, Hz:number, W:number){
        return A*cos(2*pi()*Hz*X + W)
    }
    
    function number sawWave(X:number,P:number){
        return X % P
    }
    
    function number triWave(X:number,A:number, P:number){
        #return abs((X % P) - (P/2))
        return (A/P) * (P - abs(X % (2*P) - P) )
    }
    
    # R can never be larger than Period
    function number sqrWave(X:number,P:number,R:number,On:number,Off:number){
        return (X % P) < R ? On : Off
    }
    
    #Height of Wave is (P/2)^2
    function number concavetriWave(X:number, P:number){
        return abs((X % P) - (P/2))^2
    }
    
    #Height of Wave is (P/2)^0.5
    function number convextriWave(X:number, P:number){
        return abs((X % P) - (P/2))^0.5
    }
}