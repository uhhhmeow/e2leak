@name Scriptis' Shop
@model models/hunter/plates/plate075.mdl
@inputs YY Scr:wirelink Use
@outputs RST
@persist Menu Items:gtable Price:gtable Price2:gtable Music:gtable Cursong
@persist Page Page2 Y TXT:string TXT2:string MusicEnabled Person:entity
@trigger 
MusicEnabled=1
Items=gTable("SSItems")
Price=gTable("SSPrice")
Price2=gTable("SSPrice2")
Music=gTable("SSMusic")
Items:clear()
Price:clear()
Price2:clear()
Music:clear()
Y=YY
Items[1,string]="Deagle"
Price[1,string]="1,100"
Price2[1,string]="10,050"
Items[2,string]="P228"
Price[2,string]="350"
Price2[2,string]="3,100"
Items[3,string]="Salmon"
Price[3,string]="20"
Price2[3,string]="180"
Items[4,string]="Coffee"
Price[4,string]="30"
Price2[4,string]="275"
Items[5,string]="Beer"
Price[5,string]="30"
Price2[5,string]="275"
Items[6,string]="Orange"
Price[6,string]="7"
Price2[6,string]="60"
Items[7,string]="LgMedkit"
Price[7,string]="40"
Price2[7,string]="375"
Items[8,string]="SMedkit"
Price[8,string]="15"
Price2[8,string]="120"
Items[9,string]="Halon"
Price[9,string]="375"
Price2[9,string]="3650"
Items[10,string]="Glock"
Price[10,string]="400"
Price2[10,string]="3750"


Music[1,string]="music/hl1_song25_remix3.mp3"
Music[2,string]="music/hl2_song20_submix0.mp3"
Music[3,string]="music/hl2_song26.mp3"
Music[4,string]="music/portal_4000_degrees_kelvin.mp3"
Music[5,string]="music/portal_still_alive.mp3"
Music[6,string]="music/hl2_song15.mp3"
Music[7,string]="ambient/music/looping_radio_mix.wav"
Music[8,string]="ui/gamestartup10.mp3"
Music[9,string]="ui/gamestartup1.mp3"
Music[10,string]="ui/gamestartup2.mp3"
Music[11,string]="ui/gamestartup3.mp3"
Music[12,string]="ui/gamestartup4.mp3"

TXT="Page "+Page:toString()+" of "+ceil(Music:count()/8):toString()
TXT2="Page "+Page2:toString()+" of "+ceil(Items:count()/8):toString()
if (first()) {Menu=0 Cursong=1 Page=1 Page2=1}
timer("Autofix",10000)
if (clk("Autofix")) {RST=1 }
if (RST==1) {timer("Autofix2",10)}
if (clk("Autofix2")) {RST=0 timer("Autofix",10000)}
Scr:writeString("Scriptis' Shop",14-round("Scriptis' Shop":length()/2),0,900)
Scr:writeString("Menu",14-round("Menu":length()/2),17)
if (Y>0.95) {Scr:writeString("Menu",14-round("Menu":length()/2),17,999,0)} else {Scr:writeString("Menu",14-round("Menu":length()/2),17,777)}
if (changed(Use) & Use) {
    Person=ranger(50):entity()
    if (Y>0.95) {Menu=1}
    if (Menu==1 & Y>0.25 & Y<0.3) {Menu=2}
    # if (Menu==1 & owner():pos():distance(entity():pos())<40 & Y<0.65 & Y<0.7) { if (MusicEnabled==1) {MusicEnabled=0} else {MusicEnabled=1} }
    if (Menu==1 & Y>0.55 & Y<0.6) {Menu=3}
    if (Menu==1 & Y>0.45 & Y<0.5 & MusicEnabled==1 ) {Menu=4} # Music
    if (Menu==4 & Y>0.9 & Y<0.95) {soundPlay(0,0,Music[Cursong+(Page*8)-8,string])}
    if (Menu==4 & Y>0.85 & Y<0.9 & Cursong<8) {Cursong=Cursong+1}
    if (Menu==4 & Y>0.8 & Y<0.85 & Cursong>1) {Cursong=Cursong-1}
    if (Menu==4 & Y>0.75 & Y<0.8) {Page=Page+1 Cursong=1 RST=1
        TXT="Page "+Page:toString()+" of "+ceil(Music:count()/8):toString()
        TXT2="Page "+Page2:toString()+" of "+ceil(Items:count()/8):toString()}
    if (Menu==2 & Y>0.75 & Y<0.8) {Page2=Page2+1 RST=1 
        TXT="Page "+Page:toString()+" of "+ceil(Music:count()/8):toString()
        TXT2="Page "+Page2:toString()+" of "+ceil(Items:count()/8):toString()

}
}
#if (Page>floor(Music:count()/8)) {Page=1}
if (Menu==1) {
if (Y>0.25 & Y<0.3 ) {Scr:writeString("Shop",round(14-"Shop":length()/2),4,999,0)} else {Scr:writeString("Shop",round(14-"Shop":length()/2),4,777)}
if (Y>0.35 & Y<0.4 ) {Scr:writeString("Donate",round(14-"Donate":length()/2),6,900,0)} else {Scr:writeString("Donate",round(14-"Donate":length()/2),6,900)}
if (Y>0.45 & Y<0.5 & MusicEnabled==1 ) {Scr:writeString("Music",round(14-"Music":length()/2),8,999,0)} else {Scr:writeString("Music",round(14-"Music":length()/2),8,777)}
if (MusicEnabled==0) {Scr:writeString("Music",round(14-"Music":length()/2),8,900,0)}
if (Y>0.55 & Y<0.6 ) {Scr:writeString("Help & Credits",round(14-"Help & Credits":length()/2),10,999,0)} else {Scr:writeString("Help & Credits",round(14-"Help & Credits":length()/2),10,777)}
}
if (Page>ceil(Music:count()/8)) {Page=1}
if (Page2>ceil(Items:count()/8)) {Page2=1}
if (Page==ceil(Music:count()/8) & Cursong>Page*8-Music:count()) {Cursong=Page*8-Music:count()}
if (changed(Cursong) & Menu==4) {
    for (I=1,Music:count()) {
        Scr:writeString("Song",0,2)
        Scr:writeString(Music[I+(Page*8)-8,string],0,I+3,777)
        if (I>7) {break}
    }
    Scr:writeString(Music[Cursong+(Page*8)-8,string],0,Cursong+3,999,0)
    }
    
    
if (Menu==4) {
    Scr:writeString("Song",0,2)
    for (I=1,Music:count()) {
        Scr:writeString("Song",0,2)
        Scr:writeString(Music[I+(Page*8)-8,string],0,I+3,777)
        if (I>7) {break}
    }
    Scr:writeString(Music[Cursong+(Page*8)-8,string],0,Cursong+3,999,0)
    if (Y>0.9 & Y<0.95 ) {Scr:writeString("Play",round(14-"Play":length()/2),16,999,0)} else {Scr:writeString("Play",round(14-"Play":length()/2),16,777)}
    if (Y>0.85 & Y<0.9 ) {Scr:writeString("Down",round(14-"Down":length()/2),15,999,0)} else {Scr:writeString("Down",round(14-"Down":length()/2),15,777)}
    if (Y>0.8 & Y<0.85 ) {Scr:writeString("Up",round(14-"Up":length()/2),14,999,0)} else {Scr:writeString("Up",round(14-"Up":length()/2),14,777)}
    if (Y>0.75 & Y<0.8 ) {
        Scr:writeString(TXT,round(14-(TXT:length())/2),13,999,0)} else {
        Scr:writeString(TXT,round(14-(TXT:length())/2),13,777)}

}


if (Menu==2) {
        Scr:writeString("Name",0,2)
        Scr:writeString("Item",13,2)
        Scr:writeString("Shipment",19,2)
    for (I=1,Items:count()) {
        Scr:writeString("Name",0,2)
        Scr:writeString("Item",13,2)
        Scr:writeString("Shipment",19,2)
        Scr:writeString(Items[I+(Page2*8)-8,string],0,I+3)
        Scr:writeString(Price[I+(Page2*8)-8,string],13,I+3)
        Scr:writeString(Price2[I+(Page2*8)-8,string],19,I+3)
        if (I>7) {break}
    }
        if (Y>0.75 & Y<0.8 ) {
        Scr:writeString(TXT2,round(14-(TXT2:length())/2),13,999)} else {
        Scr:writeString(TXT2,round(14-(TXT2:length())/2),13,777)}

}


if (changed(Menu)) {RST=1}
if (Menu==3) {
    Scr:writeString("How to use Scriptis' Shop",0,2)
    Scr:writeString("The items on the SHOP menu are",0,4)
    Scr:writeString("purchased from Scriptis.",0,5)
    Scr:writeString("I hope to make this",0,6)
    Scr:writeString("An autovendor in time.",0,7)
    Scr:writeString("By Scriptis for DarkRP",0,9,900)
    Scr:writeString("Version 3.14",0,10)
}


if (Cursong>Music:count()) {Cursong=Music:count()}
if (Cursong<1) {Cursong=1}
