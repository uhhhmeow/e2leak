@name SShop5
#Data in
@inputs Scr:wirelink User2:entity Prices:array 
@inputs Items:array Reloading AMT Available:array
#EGP and Money
@persist Menu User:entity Use Credit:array
@persist Current Using RollOver Draw
#Alarm
@persist Chat OutText:string Alarm Larmtext:string
@persist Stocking
#Timeout
@persist MaxTime TimeLeft Select
#Data Out
@outputs Buy
#Trigger
@trigger all
#TIMERS######################################################################
if (User:keyUse()) {TimeLeft=MaxTime}
Draw=changed(Menu)
timer("TimeDown",1000)
if (Menu==-1) {TimeLeft=0}
ID=ceil((User:steamID():sub(11):toNumber())/100000)
if (clk("TimeDown")) {
    Draw=1
    if (Menu!=6) {
    if (TimeLeft<(MaxTime/3)+1 & TimeLeft) {
    Scr:egpText(60,"You will be logged off in " +TimeLeft+" seconds.",vec2(256,500))
    Scr:egpAlign(60,1,1)
    Scr:egpColor(60,vec(255,0,0))
} else {Scr:egpRemove(60)}
    if (TimeLeft & Menu!=4 & Menu!=4.1) {
    TimeLeft-=1
} elseif (Menu!=4 & Menu!=4.1) {User=noentity() Menu=-1}
}

timer("TimeDown",1000)  
}

#MAIN#######################################################################
runOnChat(1)
runOnFile(1)
MaxTime=15
W=Scr
O=User
OutText="/advert [SShop5] My shop in 45th aparments is being robbed!"
interval(100)
Use=(User:keyUse() & !Using)
if (Use) {Using=1}
if (Using & !User:keyUse()) {Using=0}
if (changed(Menu)) {
    Scr:egpClear()
    Use=0
    Scr:egpBox(40,vec2(256,256),vec2(512,512))
    Scr:egpMaterial(40,"maps/noicon")
}



if (!User & !Alarm) {Menu=-1}
if (User & User:aimEntity()!=Scr:entity()) {
    if (Menu!=4 & Menu!=4.1) {
    User=noentity() Menu=-1
} elseif (User:pos():distance(Scr:entity():pos())>200) {
    User=noentity()
    Menu=-1
}
    }
#STARTUP#####################################################################
if (first() | duped()) {
    Menu=-1
    Scr:egpClear()
    
    
        function number pIR(Num:number) {
        if(inrange(W:egpCursor(O):x(),W:egpPos(Num):x()-(W:egpSize(Num):x()/2),W:egpPos(Num):x()+(W:egpSize(Num):x()/2))&
        inrange(W:egpCursor(O):y(),W:egpPos(Num):y()-(W:egpSize(Num):y()/2),W:egpPos(Num):y()+(W:egpSize(Num):y()/2))) {
            RollOver=Num
            return 1
        } else {
            return 0
        }
    }
    
    
}


#START MENU###############################################################
if (Menu==0) {
    if (Draw) {
    Scr:egpText(1,"SShop5",vec2(256,15))
    Scr:egpAlign(1,1,1)
    Scr:egpSize(1,50)
    Scr:egpRoundedBox(2,vec2(256,256),vec2(150,30))
    Scr:egpText(3,"Start",vec2(256,256))
    Scr:egpColor(2,120,120,120,255)
    Scr:egpAlign(3,1,1)
    Scr:egpRoundedBoxOutline(4,vec2(256,256),vec2(150,30))
    Scr:egpAlpha(4,0)
}
    if (pIR(2)) {
        Scr:egpAlpha(4,255)
        if (Use) {
            Menu=1
            Use=0
    }
} else {Scr:egpAlpha(4,0)}
}
#LOGIN#####################################################################
if (Menu==-1) {
    if (Draw) {
    Scr:egpBox(2,vec2(256,256),vec2(512,100))
    Scr:egpColor(2,80,80,80,255)
    Scr:egpText(1,"Press Anywere to Begin",vec2(256,256))
    Scr:egpAlign(1,1,1)
    Scr:egpSize(1,50)
}
    if (User2) {User=User2}
    if (User) {Menu=0 Use=0}
}

#MAIN MENU#####################################################################

if (Menu==1) {
    Scr:egpText(1,"Select a product",vec2(256,10))
    Scr:egpSize(1,30)
    Scr:egpAlign(1,1,1)
    for (I=1, Items:count()) {
        if (Draw & Available[I,number]) {
            Scr:egpRoundedBox(10*I+1,vec2(256,50+(I*50)),vec2(512,40))
            Scr:egpText(10*I+2,Items[I,string]+" | $"+Prices[I,number]+".00",vec2(5,50+(I*50)))
            Scr:egpAlign(10*I+2,0,1)
        }
            if (pIR(10*I+1)) {
                Scr:egpColor(10*I+1,vec(0,255,0))
                if (Use) {
                    Select=I
                    Menu=4
                    Use=0
                }
        } else {Scr:egpColor(10*I+1,vec(128,128,128))}
    }
}



#PURCHASE MENU############################################################
if (Menu==4) {
        Credits=Credit[ID,number]
        Price=Prices[Select,number]
        Needed=Price-Credits
    if (Draw) {

    Scr:egpText(10,"Confirmation and Payment",vec2(256,10))
    Scr:egpSize(10,30)
    Scr:egpAlign(10,1,1)
    Scr:egpText(20,"Would you like to purchase "+Items[Select,string],vec2(256,40))
    Scr:egpText(30,"For $"+Prices[Select,number]+".00?",vec2(256,70))
    Scr:egpText(31,"Credits: "+Credits,vec2(256,100))
    Scr:egpAlign(20,1,1)
    Scr:egpAlign(30,1,1)
    Scr:egpSize(20,30)
    Scr:egpSize(30,25)
    Scr:egpAlign(31,1,1)
    Scr:egpSize(31,25)
    Scr:egpRoundedBox(2,vec2(256,200),vec2(200,40))
    if (Needed>0) {
        Text="Insert $"+Needed+" to machine"
} else {Text="Confirm"}
    Scr:egpText(3,Text,vec2(256,200))
    Scr:egpColor(2,120,120,120,255)
    Scr:egpAlign(3,1,1)
    Scr:egpRoundedBox(4,vec2(256,400),vec2(200,40))
    Scr:egpText(5,"Cancel",vec2(256,400))
    Scr:egpAlign(5,1,1)
    Scr:egpColor(4,120,120,120,255)
    Scr:egpRoundedBoxOutline(7,vec2(256,200),vec2(200,40))
    Scr:egpRoundedBoxOutline(8,vec2(256,400),vec2(200,40))
    Scr:egpAlpha(7,0)
    Scr:egpAlpha(8,0)
    if (Needed) {
        Scr:egpAlpha(2,0)
} else {Scr:egpAlpha(2,255)}
    
}
    if (pIR(2) & Needed<=0) {
        Scr:egpAlpha(7,255)
        if (Use & Needed<=0) {
            Buy=Select
            Menu=4.1 Use=0
            Credit[ID,number]=Credit[ID,number]-Prices[Select,number]
            hint("Removed "+Prices[Select,number]+" from "+User:name()+"'s account via purchase",5)
        }
} else {Scr:egpAlpha(7,0)}
    if (pIR(4)) {
        Scr:egpAlpha(8,255)
        if (Use) {Menu=3}
} else {Scr:egpAlpha(8,0)}

    
}




if (Menu==4.1) {
    Buy=Select
    Scr:egpText(1,"Vending "+Items[Select,string],vec2(256,256))
    Scr:egpSize(1,50)
    Scr:egpAlign(1,1,1)
    Scr:egpText(2,"Thanks for SShopping!",vec2(256,300))
    Scr:egpAlign(2,1,1)
    Scr:egpSize(2,30)
    timer("Finish",4000)
}
if (clk("Finish")) {
    User=noentity()
    Buy=0
    Menu=-1
}




#CREDIT SYSTEM##########################################################
if (changed(Menu)) {Scr:egpClear()}
if (AMT>Current) {
    if (!User) {
        findByClass("player")
        findSortByDistance(Scr:entity():pos())
        User=find()
    }
    Add=AMT-Current
    ID=ceil((User:steamID():sub(11):toNumber())/100000)
    Credit[ID,number]=Credit[ID,number]+Add
    Current=AMT
    hint("[SShop5] Added "+Add+" to "+User:name()+"'s account.",5)
}
if (AMT<Current) {
    Removed=Current-AMT
    Current=AMT
    hint("[SShop5] $"+Removed+" removed. | Say /alarm to signal alarm.",5)
}


############ALARM FUNCTIONS#################################################
if (chatClk(owner()) & owner():lastSaid()=="/alarm") {
    hideChat(1)
    Scr:entity():soundPlay(0,0,"/ambient/alarms/alarm1.wav")
    Chat=1
    Alarm=1
    Menu=6
    timer("ThanksAnyway",1000)
}
if (clk("ThanksAnyway")) {
    Chat=0     
    hint("[SShop5] Signalled police. Say /off to halt alarm.",5)
}
if (Chat) {
    concmd("say "+OutText)
}
if (chatClk(owner()) & owner():lastSaid()==(OutText)) {
    Chat=0
    hint("[SShop5] Signalled police. Say /off to halt alarm.",5)
}

if (chatClk(owner()) & owner():lastSaid()=="/off") {
    hideChat(1)
    hint("[SShop5] Alarm deactivated.",5)
    soundPurge()
    Alarm=0
    Menu=-1
}

if (chatClk(owner()) & owner():lastSaid()=="/stock" & !Alarm) {
    hideChat(1)
    if (Stocking) {
    hint("[SShop5] Enabled Shopping",5)
    soundPurge()
    Stocking=0
    Menu=-1
} else {
    hint("[SShop5] Disabled Shopping",5)
    Stocking=1
    Menu=6
}
}
#LOCK MENU##################################################################
if (Menu==6) {
    if (Alarm) {Larmtext="Robbery!"} else {Larmtext="Restocking!"}
    Scr:egpBox(3,vec2(256,256),vec2(512,100))
    Scr:egpColor(3,80,80,80,255)
    Scr:egpText(2,Larmtext,vec2(256,256))
    Scr:egpSize(2,50)
    Scr:egpAlign(2,1,1)
    Scr:egpColor(2,vec(255,0,0))

}
#AUTOSHUTOFF
if (Menu==4 & !Select) {Menu=1}

#Rollover
if (changed(RollOver)) {
    soundVolume(0,200)
    Scr:entity():soundPlay(0,0,"/ui/buttonrollover.wav")
}
if (Stocking) {Menu=6}
Draw=0
