@name !Holoslots
@model models/hunter/blocks/cube05x05x05.mdl
@persist Models:array Stage On Price Que:array Last:entity
@persist Spinning Checking CurModel Ang Payed Sound Cons:array CE
if (first() | duped()) {
    CE=1
    Price=50
    CurModel=1
    setName("The Box")
    entity():setColor(150,150,150,140)
    entity():setMaterial("models/debug/debugwhite")
    Models[1,string]="models/props/cs_assault/Money.mdl"
    Models[2,string]="models/weapons/w_eq_fraggrenade.mdl"
    Models[3,string]="models/props_junk/Shoe001a.mdl"
    Models[4,string]="models/Gibs/HGIBS.mdl"
    Models[5,string]="models/props_lab/jar01b.mdl"
    Cons[1,string]=""
    Cons[2,string]="slay explode"
    Cons[3,string]="slap"
    Cons[4,string]="slay"
    Cons[5,string]="slay rocket"
    holoCreate(1)
    holoPos(1,entity():pos())
    holoParent(1,entity())
    holoScale(1,vec(1.5,1.5,1.5))
}
if (Last) {timer("Reload",5000)}
if (clk("Reload") & !On) {Last=noentity()}
Players=players()
for (I=1, Players:count()) {
    if (Players[I,entity]:keyUse() & !(entity():isPlayerHolding()) &  Players[I,entity]:aimEntity()==entity() & Players[I,entity]:pos():distance(entity():pos())<200 & !On & !Last) {
        Last=Players[I,entity]
        #ifdef askForMoney(entity,number)
        askForMoney(Players[I,entity],Price)
        #else
        Spinning=50 On=1 
        soundPlay(0,0,"/buttons/bell1.wav")
        #endif
    }
}
#ifdef deductClk()
if (deductClk() & !On) {Spinning=50 On=1 soundPlay(0,0,"/buttons/bell1.wav") }
#endif
if (Spinning) {Spinning-=1 CurModel=randint(1,Models:count())}
if (Spinning==0 & !Checking & On) {Checking=1 soundPlay(0,0,"/buttons/button14.wav") }
if (Checking) {entity():setColor(0,0,255,80) timer("Check",2500)} 
if (clk("Check")) {
    if (CurModel==1) {
        if (!Payed & !Sound) {
        soundPlay(0,0,"/ui/hitsound.wav")
        #ifdef giveMoney(entity,number)
        giveMoney(Last,Price*10)
        #endif
        Payed=1
        Sound=1
    }
    } elseif (!Sound) {
    soundPlay(0,0,"/ui/duel_challenge_rejected.wav")
    Sep=Cons[CurModel,string]:explode(" ")
    if (CE) {
    concmd("FAdmin")
    concmd("fadmin "+Sep[1,string]+" "+(Last:name():explode(" ")[1,string])+" "+Sep[2,string])
}
    Sound=1
}
timer("Restart",5000)
}
if (clk("Restart")) {reset() }
Ang++
holoAng(1,ang(0,Ang,0))
holoModel(1,Models[CurModel,string])
interval(100)
