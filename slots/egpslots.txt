@name EGPSlots
@inputs Scr:wirelink User:entity
@persist A1 A2 A3 Icons:array Pos:vector2
@persist Spin
@trigger all
@model models/hunter/plates/plate.mdl
Pos=Scr:egpCursor(User)
if (first()) {Scr:egpClear()}
Scr:egpText(1,"SSlots",vec2(256,15))
Scr:egpAlign(1,1,1)
Scr:egpSize(1,50)
Scr:egpText(2,"Get 3-in-a-row to win!",vec2(256,40))
Scr:egpAlign(2,1,1)
Scr:egpBox(3,vec2(256/3,256),vec2(100,100))
Scr:egpBox(4,vec2(256,256),vec2(100,100))
Scr:egpBox(5,vec2(256+(512/3),256),vec2(100,100))
Scr:egpBoxOutline(6,vec2(256/3,256),vec2(100,100))
Scr:egpBoxOutline(7,vec2(256,256),vec2(100,100))
Scr:egpBoxOutline(8,vec2(256+(512/3),256),vec2(100,100))
Scr:egpColor(6,0,255,0,255)
Scr:egpColor(7,0,255,0,255)
Scr:egpColor(8,0,255,0,255)
Scr:egpSize(8,25/3)
Scr:egpSize(6,25/3)
Scr:egpSize(7,25/3)
Scr:egpRoundedBox(9,vec2(256,412),vec2(150,50))
Icons[1,string]="$"
Icons[2,string]="X"
Icons[3,string]="7"
Icons[4,string]="#"
Icons[5,string]="@"
    LeftTop=Scr:egpPos(9)-Scr:egpSize(9)/2
    RightBottom=Scr:egpPos(9)+Scr:egpSize(9)/2
    if (inrange(Pos,LeftTop,RightBottom)) {
        if (User:keyUse() & Scr:egpCursor(User) & !Spin) {
            print("Spin")
            Spin=1
            }
    }
if (Spin) {
    A1+=randint(1,2)
    A2+=randint(1,2)
    A3+=randint(1,3)
    if (A1>Icons:count()) {A1=(A1-Icons:count())}
    if (A2>Icons:count()) {A2=(A2-Icons:count())}
    if (A3>Icons:count()) {A3=(A3-Icons:count())}
    Scr:egpText(10,Icons[A1,string],Scr:egpPos(3))
    Scr:egpText(11,Icons[A2,string],Scr:egpPos(4))
    Scr:egpText(12,Icons[A3,string],Scr:egpPos(5))
    Scr:egpColor(10,0,0,255,255)
    Scr:egpColor(11,0,0,255,255)
    Scr:egpColor(12,0,0,255,255)
    Scr:egpSize(10,50)
    Scr:egpSize(11,50)
    Scr:egpSize(12,50)
    Scr:egpAlign(12,1,1)
    Scr:egpAlign(10,1,1)
    Scr:egpAlign(11,1,1)
    Spin=0
} 
Scr:egpText(13,"Spin",Scr:egpPos(9))
Scr:egpAlign(13,1,1)
Scr:egpColor(9,80,80,80,255)
Scr:egpText(14,"By Scriptis",vec2(256,490))
Scr:egpAlign(14,1,1)
