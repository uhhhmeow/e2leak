@name miscIllusion3
@outputs Scale Core:entity




if(first()){
    
Scale = 1
holoCreate(0)
holoParent(0, entity())

# HOLO CREATION
for(I=1,80){
    holoCreate(I) 
    holoDisableShading(I,1)
    holoParent(I,holoEntity(0))
    }

holoPos(0, entity():toWorld(vec(0,0,65*Scale)))
holoScaleUnits(0, vec(100,1,70)*Scale)
#holoAlpha(0,0)

Core = holoEntity(0)

# COLLUMNS    
for(I=1,13){
    holoScaleUnits(I,vec(0.4,1.1,70)*Scale)
    holoPos(I, Core:toWorld(vec(-49+(I*7),0,4.5)*Scale))
    holoColor(I,vec(200))
    }
    
# ROWS
for(I=1,9){
    holoScaleUnits(I+13,vec(93,1.4,0.4)*Scale)
    holoPos(I+13, Core:toWorld(vec(0,0,-30.5+(I*7))*Scale))
    holoColor(I+13,vec(200))
    }
    
# DIAGONAL TOP RIGHT TO LEFT DOWN
for(I=1,19){
    holoScaleUnits(I+22,vec(0.4,1.4,270)*Scale)
    holoPos(I+22, Core:toWorld(vec(-70+(I*7),0,4.5)*Scale))
    holoAng(I+22, Core:toWorld(ang(45,0,0)))
    holoColor(I+22,vec(200))
    }

# DIAGONAL TOP RIGHT TO LEFT DOWN
for(I=1,19){
    holoScaleUnits(I+42,vec(0.4,1.4,270)*Scale)
    holoPos(I+42, Core:toWorld(vec(-70+(I*7),0,4.5)*Scale))
    holoAng(I+42, Core:toWorld(ang(-45,0,0)))
    holoColor(I+42,vec(200))
    }

for(I=1,80){
    holoClipEnabled(I,1,1)
    holoClipEnabled(I,2,1)
    holoClipEnabled(I,3,1)
    holoClipEnabled(I,4,1)
    
    holoClip(I,1,Core:toWorld(vec(0,0,35)),-Core:up(),1)
    holoClip(I,2,Core:toWorld(vec(0,0,-35)),Core:up(),1)
    holoClip(I,3,Core:toWorld(vec(50,0,0)),-Core:forward(),1)
    holoClip(I,4,Core:toWorld(vec(-50,0,0)),Core:forward(),1)
    
   
    
    }


}






































































if(first()){
    entity():setAlpha(0)
    holoCreate(890)
    holoPos(890, entity():toWorld(vec(0,0,3)))
    holoAng(890, entity():toWorld(ang(0,0,0)))
    holoModel(890, "rcube_thin")
    holoScaleUnits(890,vec(6.6,6.6,6.6))
    holoClipEnabled(890,1,1)
    holoClip(890,1,vec(0,0,-2.3),vec(0,0,-1),0)
    holoParent(890,entity())
    holoColor(890, vec(60))
    #
    holoCreate(891)
    holoPos(891, entity():toWorld(vec(0,0,0.7)))
    holoAng(891, entity():toWorld(ang(0,0,0)))
    holoModel(891, "rcube_thin")
    holoScaleUnits(891,vec(6.6,6.6,0.01))
    holoParent(891,entity())
    holoColor(891, vec(60))
    holoMaterial(891, "debug/debugdrawflat")
    #
    holoCreate(892)
    holoPos(892, entity():toWorld(vec(0,0,0.75)))
    holoAng(892, entity():toWorld(ang(0,0,0)))
    holoModel(892, "rcube_thin")
    holoScaleUnits(892,vec(5.9,5.9,0.05))
    holoParent(892,entity())
    holoColor(892, vec(255))
    holoMaterial(892, "debug/debugdrawflat")
    #
    holoCreate(893)
    holoPos(893, entity():toWorld(vec(-1.5,-1.5,0.85)))
    holoAng(893, entity():toWorld(ang(0,0,0)))
    holoModel(893, "plane")
    holoScaleUnits(893,vec(8.6,8.6,10))
    holoParent(893,entity())
    holoColor(893, vec(160))
    holoMaterial(893, "expression 2/cog_prop")
    #
    holoClipEnabled(893,1,1)
    holoClipEnabled(893,2,1)
    holoClipEnabled(893,3,1)
    holoClip(893,1,entity():toWorld(vec(-2.85,-2.85,0.42)),entity():forward(),1)
    holoClip(893,2,entity():toWorld(vec(-2.85,-2.85,0.42)),-entity():right(),1)
    holoClip(893,3,entity():toWorld(vec(-2.2,-2.2,0.42)),entity():forward()-entity():right(),1)
    
    }
    
if(entity():vel()){
    holoClip(893,1,entity():toWorld(vec(-2.85,-1.5,0.42)),entity():forward(),1)
    holoClip(893,2,entity():toWorld(vec(-2.85,-2.85,0.42)),-entity():right(),1)
    holoClip(893,3,entity():toWorld(vec(-2.2,-2.2,0.42)),entity():forward()-entity():right(),1)
    }
    holoAng(893, entity():toWorld(ang(0,-curtime()*14,0)))
    interval(60)
