@name miscEucliDoors
@outputs Debug Master:entity State T1 T2 T3 T4 T5 T6 T7 T8 T9 StateLag RandColor M1lag Materials:array Models:array Random1 Random2
@model models/hunter/plates/plate1x2.mdl

if(first()){
    State=1
runOnTick(1)

#Master = owner()

entity():setAlpha(0)

for(I=1,26){
        holoCreate(I)
        holoPos(I, entity():boxCenterW()+vec(0,0,80)) # delete later
        #holoColor(I, randvec(40,255))
        #holoDisableShading(I, 1)
    }


#holoPos(1, entity():toWorld(vec(0,70,0)))

# STAND
holoPos(1, entity():toWorld(vec(0,-49,0)))
holoAng(1, entity():toWorld(ang(0,0,0)))
holoScaleUnits(1, vec(300,4.5,8.5))
holoParent(1, entity())

# LEFT DOOR FRAME BASE
holoPos(2, entity():toWorld(vec(24,0,0)))
holoAng(2, entity():toWorld(ang(0,0,0)))
holoScaleUnits(2, vec(1,95.39,4.5))
holoParent(2, entity())

# RIGHT DOOR FRAME BASE
holoPos(3, entity():toWorld(vec(-24,0,0)))
holoAng(3, entity():toWorld(ang(0,0,0)))
holoScaleUnits(3, vec(1,95.39,4.5))
holoParent(3, entity())

# TOP DOOR FRAME BASE
holoPos(4, entity():toWorld(vec(0,47.2,0)))
holoAng(4, entity():toWorld(ang(0,0,0)))
holoScaleUnits(4, vec(47.95,1,4.5))
holoParent(4, entity())

# LEFT DOOR FRAME OVER-FRAME
holoPos(5, entity():toWorld(vec(25.25,0,0)))
holoAng(5, entity():toWorld(ang(0,0,0)))
holoScaleUnits(5, vec(1.5,95.39,7))
holoParent(5, entity())

# RIGHT DOOR FRAME OVER-FRAME
holoPos(6, entity():toWorld(vec(-25.25,0,0)))
holoAng(6, entity():toWorld(ang(0,0,0)))
holoScaleUnits(6, vec(1.5,95.39,7))
holoParent(6, entity())

# TOP DOOR FRAME BASE
holoPos(7, entity():toWorld(vec(0,48.45,0)))
holoAng(7, entity():toWorld(ang(0,0,0)))
holoScaleUnits(7, vec(51.95,1.5,7))
holoParent(7, entity())
for(I=1,7){holoMaterial(I, "phoenix_storms/black_chrome")}
#################
################
#DOOR PARTS

# TOP
# TOP DOOR
holoPos(8, entity():toWorld(vec(0,37,0)))
holoAng(8, entity():toWorld(ang(0,0,0)))
holoScaleUnits(8, vec(51.95,21.5,2))
holoParent(8, entity())

# UNDER-TOP-LEFT DOOR
holoPos(9, entity():toWorld(vec(10,15.5,0)))
holoAng(9, entity():toWorld(ang(0,0,0)))
holoScaleUnits(9, vec(31.95,21.5,2))
holoParent(9, entity())

# UNDER-TOP-RIGHT DOOR(smaller one)
holoPos(10, entity():toWorld(vec(-15,17.5,0)))
holoAng(10, entity():toWorld(ang(0,0,0)))
holoScaleUnits(10, vec(18.05,17.5,2))
holoParent(10, entity())

#MIDDLE RIGHT(smallest one)
holoPos(11, entity():toWorld(vec(-15,6.75,0)))
holoAng(11, entity():toWorld(ang(0,0,0)))
holoScaleUnits(11, vec(18.05,4,2))
holoParent(11, entity())

# MIDDLE-LEFT DOOR(smaller one)
holoPos(12, entity():toWorld(vec(17,-4,0)))
holoAng(12, entity():toWorld(ang(0,0,0)))
holoScaleUnits(12, vec(13.1,17.5,2))
holoParent(12, entity())

# MIDDLE-RIGHT DOOR(smaller one)
holoPos(13, entity():toWorld(vec(-6.6,-4,0)))
holoAng(13, entity():toWorld(ang(0,0,0)))
holoScaleUnits(13, vec(34.1,17.5,2))
holoParent(13, entity())

# MIDDLE-BOTTOM(smaller one)
holoPos(14, entity():toWorld(vec(9.6,-17,0)))
holoAng(14, entity():toWorld(ang(0,0,0)))
holoScaleUnits(14, vec(28.1,8.5,2))
holoParent(14, entity())

# BOTTOM(chubby one)
holoPos(15, entity():toWorld(vec(9.6,-34,0)))
holoAng(15, entity():toWorld(ang(0,0,0)))
holoScaleUnits(15, vec(28.1,25.5,2))
holoParent(15, entity())

# BOTTOM(high one)
holoPos(16, entity():toWorld(vec(-14,-30,0)))
holoAng(16, entity():toWorld(ang(0,0,0)))
holoScaleUnits(16, vec(19.1,34.5,2))
holoParent(16, entity())

#
for(I=8,16){
    holoColor(I, vec(255))
    #holoDisableShading(I, 1)
    }

# ADDITIONAL DECALS AND SHIT


# CYLINDER 1
holoPos(17, entity():toWorld(vec(0,-48.7,0)))
holoAng(17, entity():toWorld(ang(0,0,0)))
holoModel(17, "hq_cylinder")
holoScaleUnits(17, vec(290,290,6))
holoParent(17, entity())
holoClipEnabled(17,1,1)
holoClip(17,1,vec(0,1,0),vec(0,1,0),0)
holoClipEnabled(17,2,1)
holoClip(17,2,vec(-26,0,0),vec(-1,0,0),0)
#holoMaterial(17, "debug/debugdrawflat")

# CYLINDER 2
holoPos(18, entity():toWorld(vec(0,-48.7,0)))
holoAng(18, entity():toWorld(ang(0,0,0)))
holoModel(18, "hq_cylinder")
holoScaleUnits(18, vec(290,290,6))
holoParent(18, entity())
holoClipEnabled(18,1,1)
holoClip(18,1,vec(0,1,0),vec(0,1,0),0)
holoClipEnabled(18,2,1)
holoClip(18,2,vec(26,0,0),vec(1,0,0),0)

# CYLINDER 2 TOP
holoPos(19, entity():toWorld(vec(0,-48.7,0)))
holoAng(19, entity():toWorld(ang(0,0,0)))
holoModel(19, "hq_cylinder")
holoScaleUnits(19, vec(295,295,6))
holoParent(19, entity())
holoClipEnabled(19,1,1)
holoClip(19,1,vec(0,97.5,0),vec(0,1,0),0)
holoClipEnabled(19,2,1)
holoClip(19,2,vec(26,0,0),vec(-1,0,0),0)
holoClipEnabled(19,3,1)
holoClip(19,3,vec(-26,0,0),vec(1,0,0),0)

# ARCH 
holoPos(20, entity():toWorld(vec(0,-48.7,0)))
holoAng(20, entity():toWorld(ang(0,0,0)))
holoModel(20, "hq_tube_thin")
holoScaleUnits(20, vec(300,300,8))
holoParent(20, entity())
holoClipEnabled(20,1,1)
holoClip(20,1,vec(0,1,0),vec(0,1,0),0)
holoMaterial(20, "phoenix_storms/black_chrome")

# BACKGROUND SKY
holoPos(21, entity():toWorld(vec(0,-48.7,0)))
holoAng(21, entity():toWorld(ang(0,0,0)))
holoModel(21, "hq_sphere")
holoScaleUnits(21, vec(-295))
holoParent(21, entity())
holoClipEnabled(21,1,1)
holoClip(21,1,vec(0,0,1),vec(0,0,1),0)
holoColor(21, vec(255))
holoDisableShading(21, 1)

# BACKGROUND GROUND
holoPos(22, entity():toWorld(vec(0,-48.7,0)))
holoAng(22, entity():toWorld(ang(0,0,90)))
holoModel(22, "hq_cylinder")
holoScaleUnits(22, vec(300,300,1))
holoParent(22, entity())
holoClipEnabled(22,1,1)
holoClip(22,1,vec(0,1,0),vec(0,1,0),0)
holoColor(22, vec(255))
holoDisableShading(22, 1)

# BACK WALL ON THE DOORS
holoPos(23, entity():toWorld(vec(0,-48.7,0)))
holoAng(23, entity():toWorld(ang(0,0,0)))
holoModel(23, "hq_cylinder")
holoScaleUnits(23, vec(300,300,8))
holoParent(23, entity())
holoClipEnabled(23,1,1)
holoClip(23,1,vec(0,1,0),vec(0,1,0),0)
holoMaterial(23, "phoenix_storms/black_chrome")
holoClipEnabled(23,2,1)
holoClip(23,2,vec(0,0,1),vec(0,0,1),0)

# CAT
holoPos(24, entity():toWorld(vec(0,0,0)))
holoAng(24, entity():toWorld(ang(0,90,0)))
holoMaterial(24, "matsys_regressiontest/background")
holoScaleUnits(24, vec(95,95,1))
holoAlpha(24, 100)
holoParent(24, entity())

# MODEL
holoPos(25, entity():toWorld(vec(0,0,60)))
holoAng(25, entity():toWorld(ang(0,90,0)))
holoMaterial(25, "matsys_regressiontest/background")
holoScaleUnits(25, vec(50))
#holoAlpha(25, 100)
holoParent(25, entity())

# MODEL OUTLINE
holoPos(26, entity():toWorld(vec(0,0,60)))
holoAng(26, entity():toWorld(ang(0,90,0)))
holoMaterial(26, "debug/debugdrawflat")
holoScaleUnits(26, -vec(52))
#holoAlpha(26, 100)
holoParent(26, holoEntity(25))


Models = array("hq_sphere", "hq_rcylinder_thin", "hq_tube", "hq_rcube_thin", "icosphere", "hq_torus", "hq_sphere3")







Materials = array("effects/flashlight/caustics", "debug/env_cubemap_model", "beer/wiremod/gate_wirelogo", 
"debug/env_cubemap_model_translucent", "gmod/edit_fog", "phoenix_storms/window", "shadertest/cubemapdemo", 
"effects/flashlight/tech", "gmod/edit_sun", "hlmv/background", "perftest/loader/crate02", "phoenix_storms/checkers_map",
"phoenix_storms/ps_grass", "shadertest/seamless5", "effects/flashlight/tech")























}
#print(4, entity():boxSize():toString())

if(owner():aimEntity()==entity()&owner():keyAttack1()){M1lag=40}
if(M1lag>0){M1lag--}


if(!M1lag){Use = Master:keyUse()}

if(Master:aimEntity()==entity()&Use&$Use&State==1){
    
    RandColor = random(1,330)
    
    
    
    holoModel(25, Models[random(1,Models:count()),string])
    holoMaterial(25, Materials[random(1,Materials:count()),string])
    
    holoModel(26, holoEntity(25):model())
    
    holoAng(26, holoEntity(25):angles())
    
    holoColor(21, hsv2rgb(vec(RandColor,1,1)))
    holoColor(22, hsv2rgb(vec(RandColor,1,1)))
    holoColor(26, hsv2rgb(vec((RandColor+180)%360,1,1)))
    
    
    soundVolume(1, 0.5)
    soundPlay(1, soundDuration("doors/doormove3.wav"), "doors/doormove3.wav")
    soundPitch(1, 60)
    
    soundPlay(2, soundDuration("doors/doormove2.wav"), "doors/doormove2.wav")
    soundPitch(2, 155)
    
    State=2}





# STATE 2 - OPENING
if(State==2){
    
    StateLag++
    
    if(T1<80){T1+=0.6}
    if(T2<9){T2+=0.1}
    if(T3<9){T3+=0.2}
    if(T4<30){T4+=0.3}
    if(T5<30){T5+=0.8}
    if(T6<70){T6+=0.5}
    if(T7<80){T7+=2}
    if(T8<30){T8+=0.2}
    if(T9<10){T9+=0.3}
    
    
holoPos(8, entity():toWorld(vec(0,37+sin(T1)*21,0)))
holoPos(9, entity():toWorld(vec(10+sin(T2*T2)*30,15.5,0)))
holoPos(10, entity():toWorld(vec(-15-sin(T3*T2)*19,17.5,0)))
holoPos(11, entity():toWorld(vec(-15-sin(T4*T4/8)*19,6.75,0)))
holoPos(12, entity():toWorld(vec(17+sin(T5*T4/20)*20,-4,0)))
holoPos(13, entity():toWorld(vec(-6.6-sin(T6)*38,-4,0)))
holoPos(14, entity():toWorld(vec(9.6+sin(T7)*30,-17,0)))
holoPos(15, entity():toWorld(vec(9.6,-34-sin(T8*T7/18)*36,0)))
holoPos(16, entity():toWorld(vec(-14,-30-sin(T9*T8/4)*37,0)))

#if(StateLag>180){State=3 StateLag=0}
if(T8>29){State=3 StateLag=0 soundStop(1) soundStop(2)}
}    

if(Master:aimEntity()==entity()&Use&$Use&State==3& StateLag==0){State=4
    
    soundVolume(1, 0.5)
    soundPlay(1, soundDuration("doors/doormove3.wav"), "doors/doormove3.wav")
    soundPitch(1, 60)
    
    soundPlay(2, soundDuration("doors/doormove2.wav"), "doors/doormove2.wav")
    soundPitch(2, 155)
    }





# AFTER DOORS ARE CLOSED THIS FUNCTION WOULD THEN CLOSE THEM

if(State==4){
    
    
    StateLag++
    
    if(T1>0){T1-=0.6}
    if(T2>0){T2-=0.1}
    if(T3>0){T3-=0.2}
    if(T4>0){T4-=0.3}
    if(T5>0){T5-=0.8}
    if(T6>0){T6-=0.5}
    if(T7>0){T7-=2}
    if(T8>0){T8-=0.2}
    if(T9>0){T9-=0.3}
    
    
holoPos(8, entity():toWorld(vec(0,37+sin(T1)*21,0)))
holoPos(9, entity():toWorld(vec(10+sin(T2*T2)*30,15.5,0)))
holoPos(10, entity():toWorld(vec(-15-sin(T3*T2)*19,17.5,0)))
holoPos(11, entity():toWorld(vec(-15-sin(T4*T4/8)*19,6.75,0)))
holoPos(12, entity():toWorld(vec(17+sin(T5*T4/20)*20,-4,0)))
holoPos(13, entity():toWorld(vec(-6.6-sin(T6)*38,-4,0)))
holoPos(14, entity():toWorld(vec(9.6+sin(T7)*30,-17,0)))
holoPos(15, entity():toWorld(vec(9.6,-34-sin(T8*T7/18)*36,0)))
holoPos(16, entity():toWorld(vec(-14,-30-sin(T9*T8/4)*37,0)))

#if(StateLag>180){State=1 StateLag=0}
    if(T8<0.5){State=1 StateLag=0 soundStop(1) soundStop(2)}

    }


# SELECTING PLAYER

if(!Master){
for(I=1,players():count()){
    
    if(players()[I,entity]:aimEntity()==entity() 
    & players()[I,entity]:keyUse() 
    &players()[I,entity]:pos():distance(entity():pos())<220){Master = players()[I,entity]}
        
        

    
    }
}

# DESELECTING
if(Master:pos():distance(entity():pos())>300){
    
    
    
    State=4
    StateLag=0
    Master=noentity()
    }


# MODEL
holoAng(25, entity():toWorld(ang(curtime()*20,0,0)))





















#print(4, ops():toString())
#Debug=sin(Timer)



########################
# EUCLIDEAN CLIPS
#####################

# GROUND BACKGROUND
holoClipEnabled(22,3,1)
holoClip(22,3, Master:pos(), (entity():toWorld(vec(80,0,0))-Master:pos()):cross(vec(0,0,1)), 1)

holoClipEnabled(22,4,1)
holoClip(22,4, Master:pos(), (entity():toWorld(vec(-80,0,0))-Master:pos()):cross(vec(0,0,-1)), 1)

# SKY BACKGROUND
holoClipEnabled(21,3,1)
holoClip(21,3, Master:pos(), (entity():toWorld(vec(80,0,0))-Master:pos()):cross(vec(0,0,1)), 1)

holoClipEnabled(21,4,1)
holoClip(21,4, Master:pos(), (entity():toWorld(vec(-80,0,0))-Master:pos()):cross(vec(0,0,-1)), 1)

# MODEL
holoClipEnabled(25,3,1)
holoClip(25,3, Master:pos(), (entity():toWorld(vec(80,0,0))-Master:pos()):cross(vec(0,0,1)), 1)

holoClipEnabled(25,4,1)
holoClip(25,4, Master:pos(), (entity():toWorld(vec(-80,0,0))-Master:pos()):cross(vec(0,0,-1)), 1)

# MODEL OUTLINE
holoClipEnabled(26,3,1)
holoClip(26,3, Master:pos(), (entity():toWorld(vec(80,0,0))-Master:pos()):cross(vec(0,0,1)), 1)

holoClipEnabled(26,4,1)
holoClip(26,4, Master:pos(), (entity():toWorld(vec(-80,0,0))-Master:pos()):cross(vec(0,0,-1)), 1)











# HOLO VISIBILITY
holoVisible(21, players(), 0)
holoVisible(22, players(), 0)
holoVisible(24, players(), 1)
holoVisible(25, players(), 0)
holoVisible(26, players(), 0)


holoVisible(21, Master, 1)
holoVisible(22, Master, 1)
holoVisible(24, Master, 0)
holoVisible(25, Master, 1)
holoVisible(26, Master, 1)

