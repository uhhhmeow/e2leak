@name iDomino
@outputs Use Mode Angle:angle ColorMod E:entity M1 M2 FreezeTimer Domina:array Dystans Reload BoomTimer
#@model models/hunter/plates/plate025x05.mdl

#models/hunter/plates/plate025x05.mdl

runOnTick(1)

#DISTANCE BETWEEN PROPS
Dystans=24






Use = owner():keyUse()

M1 = owner():keyAttack1()
M2 = owner():keyAttack2()
Reload = owner():keyReload()

if(Reload){BoomTimer++}else{BoomTimer=0}
if(BoomTimer>50){for(I=1,Domina:count())
    {
        findResult(I):applyForce((findResult(I):pos() - owner():aimPos())*100)
        findResult(I):propFreeze(0)
        findResult(I):setTrails(40,0,200,"trails/laser",findResult(I):getColor(),255)
        findResult(I):propGravity(0)
        
        
        }  
    
    
    
    
    
    
    BoomTimer=0}

if(M2){FreezeTimer++}
if(!M2){FreezeTimer=0}

if(first()){
    for(I=1,15){
        ColorMod=1
        holoCreate(I)
        holoAlpha(I, 0)
    }
    
    Mode = 1
    
    #OKRAG GLOWNY
    holoModel(1, "hq_cylinder")
    holoScaleUnits(1, vec(10,10,2))
    holoMaterial(1, "debug/debugdrawflat")
    
    #"GLOWKA" STRZALKI
    holoModel(2, "prism")
    holoScaleUnits(2, vec(8,2,8))
    holoMaterial(2, "debug/debugdrawflat")
    
    #"GLOWKA" STRZALKI - IN-LINE
    holoModel(5, "prism")
    holoScaleUnits(5, vec(7,2.3,7))
    holoMaterial(5, "debug/debugdrawflat")
    holoColor(5, vec(0,0,0))
    
    #LINIA STRZALKI
    holoMaterial(3, "debug/debugdrawflat")
    
    #LINIA STRZALKI - IN-LINE
    holoMaterial(6, "debug/debugdrawflat")
    holoColor(6, vec(0,0,0))
    
    #OKRAG DODATKOWY
    holoModel(4, "hq_cylinder")
    holoScaleUnits(4, vec(8,8,2.5))
    holoParent(4, holoEntity(1))
    holoMaterial(4, "debug/debugdrawflat")
    holoColor(4, vec(0,0,0))
    
    #PODGLAD SPAWNU
    holoModel(7, "models/hunter/plates/plate05x1.mdl")
}


#MODE 1 - STARTING POS

if(Mode==1){
    holoAlpha(1, 255)
    holoAlpha(4, 255)
    holoPos(1, owner():aimPos())
}

#MODE 2 - DOMINO
if(Use & $Use){Mode++}
if(Mode>2){Mode=1}

if(Mode==1){
    holoAlpha(2, 0)
    holoAlpha(3, 0)
    holoAlpha(5, 0)
    holoAlpha(6, 0)
    holoAlpha(7, 0)
}

if(Mode==2){
    holoAlpha(2, 255)
    holoAlpha(3, 255)
    holoAlpha(5, 255)
    holoAlpha(6, 255)
    holoAlpha(7, 200)
}

#Angle
Angle = (owner():aimPos() - holoEntity(1):pos()):toAngle() + ang(0,90,90)
#Angle2 = Angle:toAngle()

#"GLOWKA" STRZALKI
holoPos(2, owner():aimPos())
holoAng(2, Angle)

#"GLOWKA" STRZALKI - IN-LINE
holoPos(5, owner():aimPos())
holoAng(5, Angle)

#LINIA STRZALKI
holoAng(3, Angle)
holoPos(3, (owner():aimPos() + holoEntity(1):pos())/2)
holoScaleUnits(3, vec(2,2,holoEntity(1):pos():distance(owner():aimPos())))

#LINIA STRZALKI - IN-LINE
holoAng(6, Angle)
holoPos(6, (owner():aimPos() + holoEntity(1):pos())/2)
holoScaleUnits(6, vec(1.6,2.3,holoEntity(1):pos():distance(owner():aimPos())))

holoAng(7, Angle)
holoPos(7, (owner():aimPos() + holoEntity(1):pos())/2+vec(0,0,26))


#SPAWN KOD
if(owner():aimPos():distance(holoEntity(1):pos())>Dystans  & Mode==2 & !owner():aimEntity()){ 
    E=propSpawn("models/hunter/blocks/cube05x1x025.mdl",holoEntity(7):pos(),holoEntity(7):angles(),1)
  #  E:setMaterial("phoenix_storms/pack2/darkgrey")
    E:setMaterial("debug/debugdrawflat")
    E:setMaterial("models/debug/debugwhite")
    E:setColor(hsv2rgb(vec(ColorMod,1,1)))
    holoPos(1,owner():aimPos())
    ColorMod+=10
}

findByModel("models/hunter/blocks/cube05x1x025.mdl")
Domina = findToArray()

if(FreezeTimer>50&!M1){for(I=1,Domina:count()){findResult(I):propFreeze(0)}}
if(FreezeTimer>50&M1){for(I=1,Domina:count()){findResult(I):propDelete()}}




if(ColorMod>700){ColorMod=1}

if(owner():aimEntity()){
    Mode=1
    holoAlpha(1, 0)
    holoAlpha(4, 0)
}else{
    holoAlpha(1, 255)
    holoAlpha(4, 255)
}
