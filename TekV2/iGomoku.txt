@name iGomoku
@outputs Debug Turn [CoreHull BoardHolo Player1 Player2]:entity [LinesColor ColumnsSize RowsSize Stone1Color Stone2Color]:vector Use1 Use2 CurStoneHolo
@outputs Lag StoneIsHere P:array FR T OnClearButton LastSaid:string LastSaidWhen
@outputs  Rows:array TempArray:array StoneCheckArray:array
#@outputs [Row1 Row2 Row3 Row4 Row5 Row6 Row7 Row8 Row9 Row10 Row11 Row12 Row13 Row14 Row15]:string
@model models/hunter/plates/plate2x2.mdl

runOnTick(1)
runOnChat(1)


if(first()){
    
    #GLOBALS
    
    GameType = 1
    # 1 - GOMOKU
    # 2 - CONNECT 4
    
    Lag=-5
    Turn=1
    CurStoneHolo=43 #STARTING HOLOGRAM FOR STONES, IT INCREASES BY ONE WITH EACH MOVE
    # debug
    #Player1=owner()
    #Player2=owner()
    
    # E2 ALPHA
    entity():setAlpha(0)
    
    # COLORS
    LinesColor = vec(45,25,0)
    Stone1Color = vec(0)
    Stone2Color = vec(255)
    
    #LINES SIZES
    ColumnsSize = vec(84,0.4,4)
    RowsSize = vec(0.4,84,4)
    
    #ARRAYS PRESET

    Rows[1,string]="000000000000000"
    Rows[2,string]="000000000000000"
    Rows[3,string]="000000000000000"
    Rows[4,string]="000000000000000"
    Rows[5,string]="000000000000000"
    Rows[6,string]="000000000000000"
    Rows[7,string]="000000000000000"
    Rows[8,string]="000000000000000"
    Rows[9,string]="000000000000000"
    Rows[10,string]="000000000000000"
    Rows[11,string]="000000000000000"
    Rows[12,string]="000000000000000"
    Rows[13,string]="000000000000000"
    Rows[14,string]="000000000000000"
    Rows[15,string]="000000000000000"
    
    
    
    
    
    for(I=1,42){
        holoCreate(I)
        holoPos(I, entity():boxCenterW()+vec(0,0,80)) # delete later
        holoColor(I, randvec(40,255))
        holoDisableShading(I, 1)
    }


    #HULL
    holoPos(1, entity():boxCenterW())
    holoAng(1, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(1, entity():boxSize()+vec(1,1,-0.3))
    holoMaterial(1, "phoenix_storms/black_chrome")
    #holoModel(1, "")
    holoColor(1, vec(255,255,255))
    holoParent(1, entity())
    
    CoreHull = holoEntity(1)

    # BOARD (CHANGE MATERIALS FOR DIFFERENT TEXTURE)
    holoPos(2, entity():boxCenterW())
    holoAng(2, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(2, entity():boxSize()+vec(0,0,0))
    holoMaterial(2, "phoenix_storms/fender_wood")
    #holoModel(2, "")
    holoColor(2, vec(255,255,255))
    holoParent(2, entity())
    
    BoardHolo = holoEntity(2)
    
    ##################
    ######## COLUMNS
    ##################
    
    # CENTER
    holoPos(3, entity():toWorld(vec(0,0,0)))
    holoAng(3, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(3, ColumnsSize)
    holoMaterial(3, "debug/debugdrawflat")
    #holoModel(3, "")
    holoColor(3, LinesColor)
    holoParent(3, entity())

    # RIGHT 1
    holoPos(4, entity():toWorld(vec(0,6,0)))
    holoAng(4, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(4, ColumnsSize)
    holoMaterial(4, "debug/debugdrawflat")
    #holoModel(4, "")
    holoColor(4, LinesColor)
    holoParent(4, entity())

    # RIGHT 2
    holoPos(5, entity():toWorld(vec(0,12,0)))
    holoAng(5, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(5, ColumnsSize)
    holoMaterial(5, "debug/debugdrawflat")
    #holoModel(5, "")
    holoColor(5, LinesColor)
    holoParent(5, entity())
   
    # RIGHT 3
    holoPos(6, entity():toWorld(vec(0,18,0)))
    holoAng(6, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(6, ColumnsSize)
    holoMaterial(6, "debug/debugdrawflat")
    #holoModel(6, "")
    holoColor(6, LinesColor)
    holoParent(6, entity())

    # RIGHT 4
    holoPos(7, entity():toWorld(vec(0,24,0)))
    holoAng(7, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(7, ColumnsSize)
    holoMaterial(7, "debug/debugdrawflat")
    #holoModel(7, "")
    holoColor(7, LinesColor)
    holoParent(7, entity())
    
    # RIGHT 5
    holoPos(8, entity():toWorld(vec(0,30,0)))
    holoAng(8, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(8, ColumnsSize)
    holoMaterial(8, "debug/debugdrawflat")
    #holoModel(8, "")
    holoColor(8, LinesColor)
    holoParent(8, entity())
    
    # RIGHT 6
    holoPos(9, entity():toWorld(vec(0,36,0)))
    holoAng(9, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(9, ColumnsSize)
    holoMaterial(9, "debug/debugdrawflat")
    #holoModel(9, "")
    holoColor(9, LinesColor)
    holoParent(9, entity())
    
    # RIGHT 7
    holoPos(10, entity():toWorld(vec(0,42,0)))
    holoAng(10, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(10, ColumnsSize)
    holoMaterial(10, "debug/debugdrawflat")
    #holoModel(10, "")
    holoColor(10, LinesColor)
    holoParent(10, entity())
    
    # LEFT 1
    holoPos(11, entity():toWorld(vec(0,-6,0)))
    holoAng(11, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(11, ColumnsSize)
    holoMaterial(11, "debug/debugdrawflat")
    #holoModel(11, "")
    holoColor(11, LinesColor)
    holoParent(11, entity())

    # LEFT 2
    holoPos(12, entity():toWorld(vec(0,-12,0)))
    holoAng(12, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(12, ColumnsSize)
    holoMaterial(12, "debug/debugdrawflat")
    #holoModel(12, "")
    holoColor(12, LinesColor)
    holoParent(12, entity())

    # LEFT 3
    holoPos(13, entity():toWorld(vec(0,-18,0)))
    holoAng(13, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(13, ColumnsSize)
    holoMaterial(13, "debug/debugdrawflat")
    #holoModel(13, "")
    holoColor(13, LinesColor)
    holoParent(13, entity())

    # LEFT 4
    holoPos(14, entity():toWorld(vec(0,-24,0)))
    holoAng(14, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(14, ColumnsSize)
    holoMaterial(14, "debug/debugdrawflat")
    #holoModel(14, "")
    holoColor(14, LinesColor)
    holoParent(14, entity())
    
    # LEFT 5
    holoPos(15, entity():toWorld(vec(0,-30,0)))
    holoAng(15, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(15, ColumnsSize)
    holoMaterial(15, "debug/debugdrawflat")
    #holoModel(15, "")
    holoColor(15, LinesColor)
    holoParent(15, entity())
    
    # LEFT 6
    holoPos(16, entity():toWorld(vec(0,-36,0)))
    holoAng(16, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(16, ColumnsSize)
    holoMaterial(16, "debug/debugdrawflat")
    #holoModel(16, "")
    holoColor(16, LinesColor)
    holoParent(16, entity())
    
    # LEFT 7
    holoPos(17, entity():toWorld(vec(0,-42,0)))
    holoAng(17, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(17, ColumnsSize)
    holoMaterial(17, "debug/debugdrawflat")
    #holoModel(17, "")
    holoColor(17, LinesColor)
    holoParent(17, entity())

    ##################
    ######## ROWS
    ##################

    # CENTER
    holoPos(18, entity():toWorld(vec(0,0,0)))
    holoAng(18, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(18, RowsSize)
    holoMaterial(18, "debug/debugdrawflat")
    #holoModel(18, "")
    holoColor(18, LinesColor)
    holoParent(18, entity())

    # UP 1
    holoPos(19, entity():toWorld(vec(6,0,0)))
    holoAng(19, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(19, RowsSize)
    holoMaterial(19, "debug/debugdrawflat")
    #holoModel(19, "")
    holoColor(19, LinesColor)
    holoParent(19, entity())

    # UP 2
    holoPos(20, entity():toWorld(vec(12,0,0)))
    holoAng(20, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(20, RowsSize)
    holoMaterial(20, "debug/debugdrawflat")
    #holoModel(20, "")
    holoColor(20, LinesColor)
    holoParent(20, entity())
    
    # UP 3
    holoPos(21, entity():toWorld(vec(18,0,0)))
    holoAng(21, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(21, RowsSize)
    holoMaterial(21, "debug/debugdrawflat")
    #holoModel(21, "")
    holoColor(21, LinesColor)
    holoParent(21, entity())
    
    # UP 4
    holoPos(22, entity():toWorld(vec(24,0,0)))
    holoAng(22, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(22, RowsSize)
    holoMaterial(22, "debug/debugdrawflat")
    #holoModel(22, "")
    holoColor(22, LinesColor)
    holoParent(22, entity())
    
    # UP 5
    holoPos(23, entity():toWorld(vec(30,0,0)))
    holoAng(23, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(23, RowsSize)
    holoMaterial(23, "debug/debugdrawflat")
    #holoModel(23, "")
    holoColor(23, LinesColor)
    holoParent(23, entity())
    
    # UP 6
    holoPos(24, entity():toWorld(vec(36,0,0)))
    holoAng(24, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(24, RowsSize)
    holoMaterial(24, "debug/debugdrawflat")
    #holoModel(24, "")
    holoColor(24, LinesColor)
    holoParent(24, entity())
    
    # UP 7
    holoPos(25, entity():toWorld(vec(42,0,0)))
    holoAng(25, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(25, RowsSize)
    holoMaterial(25, "debug/debugdrawflat")
    #holoModel(25, "")
    holoColor(25, LinesColor)
    holoParent(25, entity())

    # DOWN 1
    holoPos(26, entity():toWorld(vec(-6,0,0)))
    holoAng(26, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(26, RowsSize)
    holoMaterial(26, "debug/debugdrawflat")
    #holoModel(26, "")
    holoColor(26, LinesColor)
    holoParent(26, entity())
    
    # DOWN 2
    holoPos(27, entity():toWorld(vec(-12,0,0)))
    holoAng(27, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(27, RowsSize)
    holoMaterial(27, "debug/debugdrawflat")
    #holoModel(27, "")
    holoColor(27, LinesColor)
    holoParent(27, entity())
    
    # DOWN 3
    holoPos(28, entity():toWorld(vec(-18,0,0)))
    holoAng(28, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(28, RowsSize)
    holoMaterial(28, "debug/debugdrawflat")
    #holoModel(28, "")
    holoColor(28, LinesColor)
    holoParent(28, entity())
    
    # DOWN 4
    holoPos(29, entity():toWorld(vec(-24,0,0)))
    holoAng(29, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(29, RowsSize)
    holoMaterial(29, "debug/debugdrawflat")
    #holoModel(29, "")
    holoColor(29, LinesColor)
    holoParent(29, entity())
    
    # DOWN 5
    holoPos(30, entity():toWorld(vec(-30,0,0)))
    holoAng(30, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(30, RowsSize)
    holoMaterial(30, "debug/debugdrawflat")
    #holoModel(30, "")
    holoColor(30, LinesColor)
    holoParent(30, entity())
    
    # DOWN 6
    holoPos(31, entity():toWorld(vec(-36,0,0)))
    holoAng(31, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(31, RowsSize)
    holoMaterial(31, "debug/debugdrawflat")
    #holoModel(31, "")
    holoColor(31, LinesColor)
    holoParent(31, entity())
    
    # DOWN 7
    holoPos(32, entity():toWorld(vec(-42,0,0)))
    holoAng(32, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(32, RowsSize)
    holoMaterial(32, "debug/debugdrawflat")
    #holoModel(32, "")
    holoColor(32, LinesColor)
    holoParent(32, entity())
    
    # POINTER
    holoScaleUnits(33, vec(5,5,4.2))    
    holoModel(33, "hq_tube")
    holoAlpha(33, 0)

    # TURN INDICATOR HULL #1
    holoPos(34, entity():toWorld(vec(-47.5,0,0)))
    holoAng(34, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(34, vec(3,84,4.5))
    holoMaterial(34, "phoenix_storms/black_chrome")
    #holoModel(34, "")
    holoColor(34, vec())
    holoParent(34, entity())

    # TURN INDICATOR HULL #2
    holoPos(35, entity():toWorld(vec(-47.5,0,0)))
    holoAng(35, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(35, vec(2.5,83,5))
    holoMaterial(35, "phoenix_storms/fender_wood")
    #holoModel(35, "")
    holoColor(35, vec(255))
    holoParent(35, entity())

    # TURN INDICATOR
    holoPos(36, entity():toWorld(vec(-47.5,0,0)))
    holoAng(36, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(36, vec(1.5,81,5.5))
    holoMaterial(36, "debug/debugdrawflat")
    #holoModel(36, "")
    holoColor(36, vec(0,200,180))
    holoParent(36, entity())

    # SURRENDER BUTTON OUTLINE
    holoPos(37, entity():toWorld(vec(-44.5,44.5,0)))
    holoAng(37, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(37, vec(5,5,4))
    holoMaterial(37, "debug/debugdrawflat")
    holoModel(37, "hq_cylinder")
    holoColor(37, vec())
    holoParent(37, entity())

    # SURRENDER BUTTON FILL
    holoPos(38, entity():toWorld(vec(-44.5,44.5,0)))
    holoAng(38, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(38, vec(4.5,4.5,4.2))
    holoMaterial(38, "debug/debugdrawflat")
    holoModel(38, "hq_cylinder")
    holoColor(38, vec(155))
    holoParent(38, entity())
    
    # SURRENDER BUTTON FLAG-POLE
    holoPos(39, entity():toWorld(vec(-44.5,44.5,0)))
    holoAng(39, entity():toWorld(ang(0,-45,0)))
    holoScaleUnits(39, vec(3.6,0.5,4.4))
    holoMaterial(39, "debug/debugdrawflat")
    #holoModel(39, "hq_cylinder")
    holoColor(39, vec(255,0,0))
    holoParent(39, entity())
    
    # SURRENDER BUTTON FLAG-POLE
    holoPos(40, entity():toWorld(vec(-44.5,44.5,0)))
    holoAng(40, entity():toWorld(ang(0,45,0)))
    holoScaleUnits(40, vec(3.6,0.5,4.4))
    holoMaterial(40, "debug/debugdrawflat")
    #holoModel(40, "hq_cylinder")
    holoColor(40, vec(255,0,0))
    holoParent(40, entity())
    
    # PLAYER INDICATOR 1
    holoColor(41, Stone1Color)
    holoModel(41, "pyramid")
    holoAng(41, ang(180,0,0))
    holoDisableShading(41, 0)
    holoMaterial(41, "")
    
    # PLAYER INDICATOR 2
    holoColor(42, Stone2Color)
    holoModel(42, "pyramid")
    holoAng(42, ang(180,0,0))
    holoDisableShading(42, 0)
    holoMaterial(42, "")
    

#holoScaleUnits(60,vec(1,1,6))
}

Use1 = Player1:keyUse()
Use2 = Player2:keyUse()










#############################
# MOVEMENT AND PLACING STONES

if(Turn==1 &Lag==20){
    
    # DELETING
    if(Player1:aimPos():distance(holoEntity(38):pos())<3){holoColor(38, vec(220)) OnClearButton=1}else{holoColor(38, vec(155)) OnClearButton=0}
    if(Player1:aimPos():distance(holoEntity(38):pos())<3&Use1){reset()}
    
    if(
inrange((round((entity():toLocal(Player1:aimPos())/6))*6):x(),-42,42)&
inrange((round((entity():toLocal(Player1:aimPos())/6))*6):y(),-42,42)&
inrange((round((entity():toLocal(Player1:aimPos())/6))*6):z(),-1,1)&OnClearButton==0
){
    
    holoPos(33, entity():toWorld(round((entity():toLocal(Player1:aimPos())/6))*6))
    holoAlpha(33, 255)
    holoColor(33, Stone1Color)
    holoColor(36, vec())
    
    #CHECKING FOR STONE
    # StoneCheckArray
    StoneCheckArray = Rows[((round((entity():toLocal(Player1:aimPos())/6))*6):x()/6)+8,string]:explode("")
    if(StoneCheckArray[((round((entity():toLocal(Player1:aimPos())/6))*6):y()/6)+8,string]!="0"){StoneIsHere=1}else{StoneIsHere=0}
    
    # PLACING STONE
    if(Use1&$Use1&Lag==20&StoneIsHere==0&OnClearButton==0){
        holoCreate(CurStoneHolo)
        holoAlpha(CurStoneHolo, 0)
        holoMaterial(CurStoneHolo, "debug/debugdrawflat")
        holoPos(CurStoneHolo, entity():toWorld(round((entity():toLocal(Player1:aimPos())/6))*6))
        holoModel(CurStoneHolo, "hq_rcylinder_thin")
        holoColor(CurStoneHolo, Stone1Color)
        holoScaleUnits(CurStoneHolo, vec(6,6,5))
        holoAlpha(CurStoneHolo, 255)
        
        # SAVING STONE DATA INTO ARRAY
        TempArray=Rows[((round((entity():toLocal(Player1:aimPos())/6))*6):x()/6)+8,string]:explode("")
        TempArray[((round((entity():toLocal(Player1:aimPos())/6))*6):y()/6)+8,string]="1"
        
        
        Rows[((round((entity():toLocal(Player1:aimPos())/6))*6):x()/6)+8,string]=
        TempArray[1,string]+TempArray[2,string]+TempArray[3,string]+TempArray[4,string]+TempArray[5,string]+TempArray[6,string]+
        TempArray[7,string]+TempArray[8,string]+TempArray[9,string]+TempArray[10,string]+TempArray[11,string]+TempArray[12,string]+
        TempArray[12,string]+TempArray[14,string]+TempArray[15,string]
        
        
        soundPlay(20,soundDuration("buttons/lightswitch2.wav"),"buttons/lightswitch2.wav")
        soundPitch(20, 200)
        CurStoneHolo++
        Turn=2
        Lag=0
        
  
    }
    }elseif(Turn==1){holoAlpha(33, 0)}
    

    
}



if(Turn==2 & Lag==20){
    
    # DELETING
    if(Player2:aimPos():distance(holoEntity(38):pos())<3){holoColor(38, vec(220)) OnClearButton=1}else{holoColor(38, vec(155)) OnClearButton=0}
    if(Player2:aimPos():distance(holoEntity(38):pos())<3&Use2){reset()}
    
    if(
inrange((round((entity():toLocal(Player2:aimPos())/6))*6):x(),-42,42)&
inrange((round((entity():toLocal(Player2:aimPos())/6))*6):y(),-42,42)&
inrange((round((entity():toLocal(Player2:aimPos())/6))*6):z(),-1,1)&OnClearButton==0
){
    holoPos(33, entity():toWorld(round((entity():toLocal(Player2:aimPos())/6))*6))
    holoAlpha(33, 255)
    holoColor(33, Stone2Color)
    holoColor(36, vec(255))
    
    #CHECKING FOR STONE
    # StoneCheckArray
    StoneCheckArray = Rows[((round((entity():toLocal(Player2:aimPos())/6))*6):x()/6)+8,string]:explode("")
    if(StoneCheckArray[((round((entity():toLocal(Player2:aimPos())/6))*6):y()/6)+8,string]!="0"){StoneIsHere=1}else{StoneIsHere=0}
    
    # PLACING STONE
    if(Use2&$Use2&Lag==20&StoneIsHere==0&OnClearButton==0){
        holoCreate(CurStoneHolo)
        holoMaterial(CurStoneHolo, "debug/debugdrawflat")
        holoPos(CurStoneHolo, entity():toWorld(round((entity():toLocal(Player2:aimPos())/6))*6))
        holoModel(CurStoneHolo, "hq_rcylinder_thin")
        holoColor(CurStoneHolo, Stone2Color)
        holoScaleUnits(CurStoneHolo, vec(6,6,5))
        
        # SAVING STONE DATA INTO ARRAY
        TempArray=Rows[((round((entity():toLocal(Player2:aimPos())/6))*6):x()/6)+8,string]:explode("")
        TempArray[((round((entity():toLocal(Player2:aimPos())/6))*6):y()/6)+8,string]="2"
        
        
        Rows[((round((entity():toLocal(Player2:aimPos())/6))*6):x()/6)+8,string]=
        TempArray[1,string]+TempArray[2,string]+TempArray[3,string]+TempArray[4,string]+TempArray[5,string]+TempArray[6,string]+
        TempArray[7,string]+TempArray[8,string]+TempArray[9,string]+TempArray[10,string]+TempArray[11,string]+TempArray[12,string]+
        TempArray[12,string]+TempArray[14,string]+TempArray[15,string]
        
        ######################
        # COLUMNS ARE DETECTED, BUT I FORGOT ABOUT ROWS. GOTTA ADD THEM AND IT WILL BE AWESOME.
        # k works now... gotta zzz, working on it next tiem.
        
        
        soundPlay(20,soundDuration("buttons/lightswitch2.wav"),"buttons/lightswitch2.wav")
        soundPitch(20, 200)
        CurStoneHolo++
        Turn=1
        Lag=0
        
  
    }
    }elseif(Turn==2){holoAlpha(33, 0)}
    

    
}





# LAG
Lag++
if(Lag>=20){Lag=20}



#PLAYER CHOOSING

findByClass("Player")
P=findToArray()
if(Lag==20&!findResult(FR):keyAttack1() & findResult(FR)!=Player2 & findResult(FR):aimEntity()==entity() & findResult(FR):keyUse() & !Player1 & findResult(FR):pos():distance(entity():pos())<350){Player1 = findResult(FR) holoEntity(5):soundPlay(20,soundDuration("/buttons/button24.wav"),"/buttons/button24.wav") Przerwa=2 Lag=10}elseif(!Player1){FR++}

if(Lag==20&findResult(FR):aimEntity()==entity() & findResult(FR)!=Player1 & findResult(FR):keyUse() &!Player2 & findResult(FR):pos():distance(entity():pos())<350){Player2 = findResult(FR) holoEntity(5):soundPlay(20,soundDuration("/buttons/button24.wav"),"/buttons/button24.wav") Przerwa=2 Lag=10}elseif(!Player2){FR++}





if(FR>P:count()+1){FR=1}

Debug2 = P:count()

# PLAYER INDICATOR

if(Player1){holoPos(41,Player1:pos() + vec(0,0,120+sin(T*2))) holoAlpha(41, 255)}else{holoAlpha(41, 0)}
if(Player2){holoPos(42,Player2:pos() + vec(0,0,120+sin(T*2))) holoAlpha(42, 255)}else{holoAlpha(42, 0)}

if(Player1 & Player1:pos():distance(entity():pos())>350){Player1=noentity() holoEntity(5):soundPlay(20,soundDuration("/buttons/button1.wav"),"/buttons/button1.wav")}
if(Player2 & Player2:pos():distance(entity():pos())>350){Player2=noentity() holoEntity(5):soundPlay(20,soundDuration("/buttons/button1.wav"),"/buttons/button1.wav")}

if(Player2==Player1){Player2=noentity()}


# ANGLING INDICATOR
holoAng(33, entity():toWorld(ang(0,0,0)))


LastSaid=owner():lastSaid() 
LastSaidWhen=owner():lastSaidWhen()
if(LastSaid=="-undo"&$LastSaidWhen){}



#print(4, Rows[((round((entity():toLocal(CurPlayer:aimPos())/6))*6):x()/6)+8,string]:index(((round((entity():toLocal(CurPlayer:aimPos())/6))*6):y()/6)+8))
# Rows[((round((entity():toLocal(CurPlayer:aimPos())/6))*6):x()/6)+8,string]:index(((round((entity():toLocal(CurPlayer:aimPos())/6))*6):y()/6)+8)


















# ROW SELECTION
#if((round((entity():toLocal(owner():aimPos())/6))*6):x()==42){CurrentRow=Row1}








#if(owner():keyUse()){DebString=DebString:replace("l","6")}
#print(4, DebString)








#print(4, (round((entity():toLocal(owner():aimPos())/6))*6):toString())
#print(4, ((round((entity():toLocal(owner():aimPos())/6))*6):y()/6):toString())

#[ POINTING DEBUG - WORKING CODE
if(
inrange((round((entity():toLocal(owner():aimPos())/6))*6):x(),-42,42)&
inrange((round((entity():toLocal(owner():aimPos())/6))*6):y(),-42,42)&
inrange((round((entity():toLocal(owner():aimPos())/6))*6):z(),-1,1)
){holoPos(60, entity():toWorld(round((entity():toLocal(owner():aimPos())/6))*6))}


