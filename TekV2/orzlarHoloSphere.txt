@name orzlarHoloSphere
@inputs 
@outputs 
@persist [Sp,Cur,Count,RadX,RadY,RadZ]
@trigger 

if(first()){
    
    Sp = 2
    Cur = 2
    
    RadX = 100
    RadY = 100
    RadZ = 100
    Count = 1
    
        
    runOnTick(1)
    
    holoCreate(0)
    holoPos(0, entity():toWorld(vec(60,0,60)))
    holoScale(0, vec(0.5))
    
    for(I = 1,Count,1){
        holoCreate(I)
        holoScale(I, vec(0.5)) 
        holoEntity(I):setTrails(10,10,20, "trails/laser", vec(255), 255)  
    }
    
}

    Cur = Cur + Sp
    
    
for( I = 1,Count,1){
    
    Func = Cur + I * (3.60 / Count)
    HalfFunc = (Cur/40 / 1) + I * (360 / Count)
    ZFunc = sin(HalfFunc)
    ZDeriv = cos(HalfFunc)
    
    Pos = holoEntity(0):pos() + vec( sin(Func)*1.5 * RadX * ZDeriv, cos(Func)*1.5 * RadY * ZDeriv, ZFunc * RadZ )
    
    holoPos(I, Pos)
    
}
