@name Snake
@inputs 
@outputs Limit Cel:entity T Sin
@persist 
@trigger 

interval(10)

Limit = 10
Sin++
if(perf()){
if(!Cel){Cel=findPlayerByName("Name")}

if(first()){
    for(I=1, Limit){
        holoCreate(I)
        holoModel(I, "hq_cylinder")
        holoScaleUnits(I, vec(11,11,13))
        holoColor(I, vec(240,184,160))
        
    }
    holoPos(1, entity():toWorld(vec(0,0,0)))
    holoAng(1, entity():toWorld(ang(0,0,0)))
   # holoParent(1, entity())
    
    holoCreate(Limit+2)
    holoColor(Limit+2, vec(220,160,140))
    holoModel(Limit+2, "hq_sphere")
    holoScaleUnits(Limit+2, vec(7.8,7.8,7.8))
    
    holoAlpha(1, 0)
    holoPos(Limit+2, holoEntity(2):toWorld(vec(0,0,-4)))
    holoParent(Limit+2, holoEntity(2))
    
    holoCreate(Limit+3)
    holoColor(Limit+3, vec(220,160,140))
    holoModel(Limit+3, "hq_sphere")
    holoScaleUnits(Limit+3, vec(14,14,14))
    holoPos(Limit+3, holoEntity(Limit):toWorld(vec(5,5,4)))
    holoParent(Limit+3, holoEntity(Limit))
    
    holoCreate(Limit+4)
    holoColor(Limit+4, vec(220,160,140))
    holoModel(Limit+4, "hq_sphere")
    holoScaleUnits(Limit+4, vec(14,14,14))
    holoPos(Limit+4, holoEntity(Limit):toWorld(vec(5,-5,4)))
    holoParent(Limit+4, holoEntity(Limit))
    
}

for(I=1, Limit){
    
    holoAng(-I+Limit+2, (((holoEntity(-I+Limit+2):pos()-holoEntity(-I+Limit+1):pos()):toAngle())+ang(90,0,0)))
    holoPos(-I+Limit+2, holoEntity(-I+Limit+1):pos()+holoEntity(-I+Limit+2):up()*8)
    holoScaleUnits(I+1, vec(8,8,holoEntity(I):pos():distance(holoEntity(I+1):pos())))
    T+=0.1
}
    


if(T>360){T=0}



if(Cel:pos():distance(holoEntity(1):pos())>200){holoPos(1, holoEntity(1):pos() + holoEntity(1):up()*2) holoAng(1, (holoEntity(1):pos()-(Cel:pos()+vec(sin(Sin*8)*200,cos(Sin*8)*200,40))):toAngle()-ang(90,0,0))}
if(Cel:pos():distance(holoEntity(1):pos())>20&Cel:pos():distance(holoEntity(1):pos())<200){holoPos(1, holoEntity(1):pos() + holoEntity(1):up()*1) holoAng(1, (holoEntity(1):pos()-(Cel:pos()+vec(sin(Sin*1)*155,cos(Sin*1)*155,40))):toAngle()-ang(90,0,0))}





#if(owner():keyUse()){
#    holoAng(1, ang(45,0,0))
#}

#[if(first()){
    holoParentAttachment(1, owner(), "anim_attachment_RH")
    holoPos(1,owner():attachmentPos("anim_attachment_RH"))
    holoAng(1,owner():attachmentAng("anim_attachment_RH")+ang(0,45,0))
}
]#
}
