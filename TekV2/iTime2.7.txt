@name iTime2
@outputs Player:entity Core:entity RecordMode RepeatType ButtonDelay
@outputs State:string Props:array 
@persist Frames:table [PropsAll PropsLeft CurrentFrame]:array Frame Start End Speed

interval(1)

# iTime
# Recording code by Crixab
# Design by Tek
# If you are reading this you propably stole it because I don't share E2s you frikkin shmuck >:[

if(first()){
    #SETTINGS
    Player = owner()
    
    for(I=1,9){
        holoCreate(I)
        holoPos(I, entity():toWorld(vec(0,0,120)))
    }

    #CORE
    Core = holoEntity(1)
    holoPos(1, entity():toWorld(vec(0,0,10)))
    holoAng(1, (Player:shootPos()-Core:pos()):toAngle()-ang(90,0,0))
    #holoAng(1, ang(0,0,0))
    holoMaterial(1, "models/wireframe")
    holoAlpha(1, 0)
    
##### PANEL 1
    # PRISM LEFT 1
    holoPos(2, Core:toWorld(vec(0,40,0)))
    holoAng(2, Core:toWorld(ang(0,90,-90)))
    holoScaleUnits(2, vec(80,0.1,16))
    holoModel(2, "right_prism")
    holoColor(2, vec(204,204,204))
    holoClipEnabled(2,1)
    holoClip(2, vec(0,0,0), vec(-1,0,0),0)
    holoMaterial(2, "debug/debugdrawflat")
    
    #PRISM RIGHT 1
    holoPos(3, Core:toWorld(vec(0,-40,0)))
    holoAng(3, Core:toWorld(ang(0,270,90)))
    holoScaleUnits(3, vec(80,0.1,16))
    holoModel(3, "right_prism")
    holoColor(3, vec(204))
    holoClipEnabled(3,1)
    holoClip(3, vec(0,0,0), vec(-1,0,0),0)
    holoMaterial(3, "debug/debugdrawflat")

    # PRISM LEFT FOOTER  1
    holoPos(4, Core:toWorld(vec(-1,40,0.1)))
    holoAng(4, Core:toWorld(ang(0,90,-90)))
    holoScaleUnits(4, vec(80,0.1,16))
    holoModel(4, "right_prism")
    holoColor(4, vec(193,193,193))
    holoClipEnabled(4,1)
    holoClip(4, vec(0,0,0), vec(-1,0,0),0)
    holoMaterial(4, "debug/debugdrawflat")
    
    #PRISM RIGHT FOOTER 1
    holoPos(5, Core:toWorld(vec(-1,-40,0.1)))
    holoAng(5, Core:toWorld(ang(0,270,90)))
    holoScaleUnits(5, vec(80,0.1,16))
    holoModel(5, "right_prism")
    holoColor(5, vec(193))
    holoClipEnabled(5,1)
    holoClip(5, vec(0,0,0), vec(-1,0,0),0)
    holoMaterial(5, "debug/debugdrawflat")
    
    local Holos=table(
        # HEADER
        array(vec(8.25,0,0),ang(0,270,90),vec(80,0.1,0.5),"",vec(193)),
        # SLIDER-FIELD
        array(vec(10,0,0),ang(0,270,90),vec(80,0.1,3),"",vec(221)),
        # SLIDER HEADER
        array(vec(11.75,0,0),ang(0,270,90),vec(80,0.1,0.5),"",vec(193)),
        # SLIDER RED
        array(vec(10,0,-0.1),ang(0,270,90),vec(80,0.1,3),"",vec(221,100,100)),
    #### ICONS - RECORD MODE
        # RECORD ICON RING
        array(vec(0.5,0,-0.1),ang(0,0,0),vec(10,10,0.1),"hq_tube_thin",vec(193)),
        # RECORD ICON CYLINDER
        array(vec(0.5,0,-0.1),ang(0,0,0),vec(8,8,0.1),"hq_cylinder",vec(193)),
    #### ICONS - PLAY MODE
        # PAUSE BUTTON LEFT
        array(vec(3,-1.5,-0.1),ang(0,0,0),vec(6,1.5,0.3),"",vec(102)),
        # PAUSE BUTTON RIGHT
        array(vec(3,1.5,-0.1),ang(0,0,0),vec(6,1.5,0.3),"",vec(102)),
        # HALF SPEED LEFT PRISM 1
        array(vec(3,-10,-0.1),ang(0,0,90),vec(6,0.2,4),"prism",vec(204)),
        # HALF SPEED LEFT PRISM 2 FILL
        array(vec(3,-11,-0.1),ang(0,0,90),vec(6,0.1,4), "prism",vec(102)),
        # HALF SPEED RIGHT PRISM 1
        array(vec(3,10 ,-0.1),ang(0,180,90),vec(6,0.2,4), "prism",vec(204)),
        # HALF SPEED RIGHT PRISM 2 FILL
        array(vec(3,11,-0.1),ang(0,180,90),vec(6,0.1,4), "prism",vec(102)),
        # NORMAL SPEED LEFT
        array(vec(3,-20.5,-0.1),ang(0,0,90),vec(6,0.2,4), "prism",vec(102)),
        # NORMAL SPEED RIGHT
        array(vec(3,20.5,-0.1),ang(0,180,90),vec(6,0.2,4), "prism",vec(102)),
        # 2X NORMAL SPEED LEFT 1#
        array(vec(3,-30,-0.1),ang(0,0,90),vec(6,0.2,4), "prism",vec(204)),
        # 2X NORMAL SPEED LEFT 2#
        array(vec(3,-31,-0.1),ang(0,0,90),vec(6,0.1,4), "prism",vec(102)),
        # 2X NORMAL SPEED LEFT 3#
        array(vec(3,-29.5,-0.1),ang(0,0,90),vec(6,0.3,4), "prism",vec(102)),
        # 2X NORMAL SPEED RIGHT 1#
        array(vec(3,30,-0.1),ang(0,180,90),vec(6,0.2,4), "prism",vec(204)),
        # 2X NORMAL SPEED RIGHT 2#
        array(vec(3,31,-0.1),ang(0,180,90),vec(6,0.1,4), "prism",vec(102)),
        # 2X NORMAL SPEED RIGHT 3#
        array(vec(3,29.5,-0.1),ang(0,180,90),vec(6,0.3,4), "prism",vec(102)),
        # RESET BUTTON RING
        array(vec(-5,0,-0.1),ang(0,0,0),vec(4,4,0.1), "hq_tube",vec(102)),
        # RESET BUTTON /
        array(vec(-5,0,-0.1),ang(0,45,0),vec(2.5,0.5,0.1), "",vec(102)),
        # RESET BUTTON \
        array(vec(-5,0,-0.1),ang(0,-45,0),vec(2.5,0.5,0.1), "",vec(102)),
        # SLIDER MARKER
        array(vec(10,-20,-0.1),ang(0,270,90),vec(0.5,0.3,3.4), "",vec(0)),
        # CLASSIC REPEAT RING
        array(vec(-4,-5,-0.1),ang(0,0,0),vec(4,4,0.1), "hq_tube",vec(102)),
        # CLASSIC REPEAT INNER RING
        array(vec(-4,-5,-0.1),ang(0,0,0),vec(2.6,2.6,0.1), "hq_tube",vec(102)),
        # CLASSIC REPEAT PRISM FILL
        array(vec(-4.2,-6.1,-0.1),ang(0,270,90),vec(0.8,0.15,0.8), "prism",vec(193)),
        # CLASSIC REPEAT PRISM ARROW
        array(vec(-4,-6.1,-0.1),ang(0,270,90),vec(0.8,0.2,0.8), "prism",vec(102)),
        # REBOUND REPEAT RING
        array(vec(-4,5,-0.1),ang(0,0,0),vec(4,4,0.1), "hq_tube",vec(102)),
        # REBOUND REPEAT ARROW LEFT
        array(vec(-4,4.2,-0.1),ang(0,0,90),vec(1,0.2,1), "prism",vec(102)),
        # REBOUND REPEAT ARROW RIGHT
        array(vec(-4,5.8,-0.1),ang(0,180,90),vec(1,0.2,1), "prism",vec(102)))
    for(I=1,Holos:count()){
        local R=Holos[I,array]
        holoCreate(I+5,Core:toWorld(R[1,vector]),R[3,vector]/12,Core:toWorld(R[2,angle]),R[5,vector],R[4,string]?:"cube")
        holoMaterial(I+5, "debug/debugdrawflat")
        if((I+5)>=12){holoAlpha(I+5, 0)}
    }
    
    ##BACKEND
    
    function start_recording(Props:array){
        State="recording"
        Frames=table()
        PropsAll=Props:clone()
        PropsLeft=PropsAll:clone()
        CurrentFrame=array()
        foreach(I,P:entity=PropsAll){P:propFreeze(0)}
    }
    function stop_recording(){
        State="finish_frame"
    }
    function delete_recording(){
        State="ready"
        Frames=table()
    }
    function jump(FrameIn){
        Frame=FrameIn
        CurrentFrame=Frames[Frame,array]:clone()
        PropsLeft=PropsAll:clone()
    }
    function setSpeed(SpeedIn){
        Speed=SpeedIn
    }
    function setRange(StartIn,EndIn){
        Start=StartIn
        End=EndIn
    }
    
    State="ready"
    
    runOnTick(1)
}
if(tickClk()){
    ##BACKEND
    setName(format("iTime2\nby Tek&crixab\nState: %s\nProps: %d\nFrame: %f/%d (%d-%d)\nSpeed: %f",State,PropsAll:count(),Frame,Frames:count(),Start,End,Speed))
    if(State=="recording"|State=="finish_frame"){
        while(perf()&PropsLeft:count()){
            local Prop=PropsLeft:shiftEntity()
            CurrentFrame:pushVector(Prop:pos())
            CurrentFrame:pushAngle(Prop:angles())
        }
        if(!PropsLeft:count()){
            Frames:pushArray(CurrentFrame)
            if(State=="recording"){
                CurrentFrame=array()
                PropsLeft=PropsAll:clone()
            }
            else{
                foreach(I,P:entity=PropsAll){P:propFreeze(1)}
                State="playback"
                CurrentFrame=CurrentFrame:clone()
                PropsLeft=PropsAll:clone()
                Start=1
                End=Frames:count()
                Frame=End
            }
        }
    }
    if(State=="playback"){
        if(PropsLeft:count()){
            local C=0
            while(perf()&PropsLeft:count()&C<30){ 
                local Prop=PropsLeft:shiftEntity()
                Prop:setPos(CurrentFrame:shiftVector())
                Prop:setAng(CurrentFrame:shiftAngle())
                C++
            }
        }
        if(!PropsLeft:count()&Speed){
            local OldFrame=Frame
            Frame=clamp(Frame+Speed,Start,End)
            if(floor(Frame)!=floor(OldFrame)){
                CurrentFrame=Frames[floor(Frame),array]:clone()
                PropsLeft=PropsAll:clone()
            }
        }
    }
    if(RepeatType>0&Frame==(Speed>0?End :Start)){
        if(RepeatType==1){jump((Speed<0?End :Start))}
        else{setSpeed(-Speed)}
    }
    
    ##TOUCH
    BStart = Player:shootPos()
    BDir = Player:eye()
    Normal = holoEntity(1):up()
    if(BDir:dot(Normal)>0){ ##Player facing holo, roughly
        Vec=BStart+BDir*((holoEntity(1):pos()-BStart):dot(Normal)/BDir:dot(Normal))
        holoPos(60, Vec)
        holoAng(60, holoEntity(1):angles())
        
        #######################
        # RECORD HOVER
        if(Vec:distance(Core:pos())<5 & RecordMode==0){
            holoColor(11, vec(200,200,160))
            # STARTING RECORDING[ENTITY DISCOVERY PHASE]
            if(Player:keyUse()){
                RecordMode=1
                holoColor(11, vec(255,0,0))
                findIncludePlayerProps(Player)
                findByClass("prop_physics")
                Props=findToArray()
                timer("record_trigger", 500)
            }
                
            
        }elseif(RecordMode==0){
            holoColor(11, vec(193))
        }
        
        # ENDING RECORD BUTTON, GOING TO PLAY FUNCTION BUTTONS
        if(RecordMode==2 & Player:keyUse()){
            stop_recording()
            
            holoAlpha(10, 0)
            holoAlpha(11, 0)
            for(I=12,36){holoAlpha(I, 255)}
            RecordMode=3
        }
        
    
        # PAUSE ACTIVATION
        if(Vec:distance(Core:pos())<5 & RecordMode==3 & Player:keyUse()){
            for(I=12,28){
                if(I==14|I==16|I==20|I==23){continue}
                holoColor(I, vec(102))
                }
            
            holoColor(12, vec(100,200,100))
            holoColor(13, vec(100,200,100))
            
            setSpeed(0)
            }
        
        # -0.25 PLAY
        if(Vec:distance(holoEntity(14):pos())<5 & RecordMode==3 & Player:keyUse()|Speed==-0.25){
            for(I=12,28){
                if(I==14|I==16|I==20|I==23){continue}
                holoColor(I, vec(102))
                }
            holoColor(15, vec(100,200,100))
            
            setSpeed(-0.25)
        }
        
        # +0.25 PLAY
        if(Vec:distance(holoEntity(16):pos())<5 & RecordMode==3 & Player:keyUse()|Speed==0.25){
            for(I=12,28){
                if(I==14|I==16|I==20|I==23){continue}
                holoColor(I, vec(102))
                }
            holoColor(17, vec(100,200,100))
            
            setSpeed(0.25)
        }
        
        # -1 PLAY
        if(Vec:distance(holoEntity(18):pos())<5 & RecordMode==3 & Player:keyUse()|Speed==-1){
            for(I=12,28){
                if(I==14|I==16|I==20|I==23){continue}
                holoColor(I, vec(102))
                }
            holoColor(18, vec(100,200,100))
            
            setSpeed(-1)
        }
        
        # +1 PLAY
        if(Vec:distance(holoEntity(19):pos())<5 & RecordMode==3 & Player:keyUse()|Speed==1){
            for(I=12,28){
                if(I==14|I==16|I==20|I==23){continue}
                holoColor(I, vec(102))
                }
            holoColor(19, vec(100,200,100))
            
            setSpeed(1)
        }
            
        # -2 PLAY
        if(Vec:distance(holoEntity(20):pos())<5 & RecordMode==3 & Player:keyUse()|Speed==-2){
            for(I=12,28){
                if(I==14|I==16|I==20|I==23){continue}
                holoColor(I, vec(102))
                }
            holoColor(21, vec(100,200,100))
            holoColor(22, vec(100,200,100))
            
            setSpeed(-2)
        }
        
        # +2 PLAY
        if(Vec:distance(holoEntity(23):pos())<5 & RecordMode==3 & Player:keyUse()|Speed==2){
            for(I=12,28){
                if(I==14|I==16|I==20|I==23){continue}
                holoColor(I, vec(102))
                }
            holoColor(24, vec(100,200,100))
            holoColor(25, vec(100,200,100))
            
            setSpeed(2)
        }
        
        # RESET RECORDING
        if(Vec:distance(holoEntity(26):pos())<2 & RecordMode==3 & Player:keyUse()){
            delete_recording()
            #timer("resetRecordMode", 500)
            for(I=12,36){holoAlpha(I, 0)}
            holoAlpha(10, 255)
            holoAlpha(11, 255)
            holoColor(11, vec(193))
            RecordMode=0
            }
        #if(clk("resetRecordMode")){
            
            #RecordMode=0
        #}
        
        # CLASSIC REPEAT MODE
        if(RepeatType!=1&Vec:distance(holoEntity(30):pos())<2 & RecordMode==3 & Player:keyUse() & ButtonDelay<=0){
            
            RepeatType=1
            
            holoColor(30, vec(100,100,255))
            holoColor(31, vec(100,100,255))
            holoColor(33, vec(100,100,255))
            
            holoColor(34, vec(102))
            holoColor(35, vec(102))
            holoColor(36, vec(102))
            
            ButtonDelay=10
            }
        
        # REBOUND REPEAT MODE
        if(RepeatType!=2&Vec:distance(holoEntity(34):pos())<2 & RecordMode==3 & Player:keyUse() & ButtonDelay<=0){
            
            RepeatType=2
            
            holoColor(30, vec(102))
            holoColor(31, vec(102))
            holoColor(33, vec(102))
            
            holoColor(34, vec(100,100,255))
            holoColor(35, vec(100,100,255))
            holoColor(36, vec(100,100,255))
            
            ButtonDelay=10
        }
        
        # CLASSIC REPEAT MODE DISABLE
        if(RepeatType==1 & Vec:distance(holoEntity(30):pos())<2 & RecordMode==3 & Player:keyUse() & ButtonDelay<=0){
            
            RepeatType=0
            
            holoColor(30, vec(102))
            holoColor(31, vec(102))
            holoColor(33, vec(102))
    
            ButtonDelay=10
            }
        
        # REBOUND REPEAT MODE DISABLE
        if(RepeatType==2 & Vec:distance(holoEntity(34):pos())<2 & RecordMode==3 & Player:keyUse() & ButtonDelay<=0){
            
            RepeatType=0
            
            holoColor(34, vec(102))
            holoColor(35, vec(102))
            holoColor(36, vec(102))
    
            ButtonDelay=10
            }
        
        
        
    }
}
 # TRIGGER FOR RECORDING FUNCTION
if(clk("record_trigger")){
    start_recording(Props)
    RecordMode=2
}
ButtonDelay--
