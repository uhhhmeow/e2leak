@name notosPiano
@outputs Player1:entity EGP:wirelink ASoundLineHeight ASoundLineIndexW ASoundLineIndexB AOnBlack Use1 [AWhiteKeysPitch ABlackKeysPitch]:array AVolume1 APitch1 LMB1
@outputs ASequencer:table ACurHolo ALag APlay ASequence AVolumeArray:array
#@model models/props_c17/FurnitureWashingmachine001a.mdl

interval(50)

EGP = entity():isWeldedTo():wirelink()
if(!Player1){
    Player1=owner()
    #Player1=findPlayerByName("Bobd")
}
    
Cursor=EGP:egpCursor(Player1)
Use1 = Player1:keyUse()
LMB1 = Player1:keyAttack2()

if(first()){
    #entity():soundPlay(1, 0, "synth/saw.wav")
    soundVolume(1, 0)
    
    for(I=1,17){
        #holoCreate(I)
        holoEntity(I):soundPlay(I, 0, "synth/saw.wav")
        soundVolume(I, 0)
        AVolumeArray[I,number]=0
        }
    
    AWhiteKeysPitch = array(130,146,164,174,196,220,246,261,293,329,349,392,440)
    ABlackKeysPitch = array(138, 155, 185, 207, 233, 277, 311)
    
    
    EGP:egpClear()
    
    # BACKGROUND
    EGP:egpBox(1, vec2(256,256), vec2(512,512))
    EGP:egpColor(1, vec(0,163,217))
    
    # PIANO-KEYS FRAME
    EGP:egpBox(2, vec2(256,425), vec2(500,162))
    EGP:egpColor(2, vec(0))
    
    # WHITE-KEY 1#
    EGP:egpBox(3, vec2(31,425), vec2(48,160))
    EGP:egpColor(3, vec(255))
    
    # WHITE-KEY 2#
    EGP:egpBox(4, vec2(81,425), vec2(48,160))
    EGP:egpColor(4, vec(255))

    # WHITE-KEY 3#
    EGP:egpBox(5, vec2(131,425), vec2(48,160))
    EGP:egpColor(5, vec(255))
    
    # WHITE-KEY 4#
    EGP:egpBox(6, vec2(181,425), vec2(48,160))
    EGP:egpColor(6, vec(255))
    
    # WHITE-KEY 5#
    EGP:egpBox(7, vec2(231,425), vec2(48,160))
    EGP:egpColor(7, vec(255))
    
    # WHITE-KEY 6#
    EGP:egpBox(8, vec2(281,425), vec2(48,160))
    EGP:egpColor(8, vec(255))
    
    # WHITE-KEY 7#
    EGP:egpBox(9, vec2(331,425), vec2(48,160))
    EGP:egpColor(9, vec(255))
    
    # WHITE-KEY 8#
    EGP:egpBox(10, vec2(381,425), vec2(48,160))
    EGP:egpColor(10, vec(255))
    
    # WHITE-KEY 9#
    EGP:egpBox(11, vec2(431,425), vec2(48,160))
    EGP:egpColor(11, vec(255))
    
    # WHITE-KEY 10#
    EGP:egpBox(12, vec2(481,425), vec2(48,160))
    EGP:egpColor(12, vec(255))
    
    ######### BLACKS
    
    # BLACK-KEY 1#
    EGP:egpBox(13, vec2(56,395), vec2(28,100))
    EGP:egpColor(13, vec(0,0,0))
    
    # BLACK-KEY 2#
    EGP:egpBox(14, vec2(106,395), vec2(28,100))
    EGP:egpColor(14, vec(0,0,0))
    
    # BLACK-KEY 3#
    EGP:egpBox(15, vec2(206,395), vec2(28,100))
    EGP:egpColor(15, vec(0,0,0))
    
    # BLACK-KEY 4#
    EGP:egpBox(16, vec2(256,395), vec2(28,100))
    EGP:egpColor(16, vec(0,0,0))
    
    # BLACK-KEY 5#
    EGP:egpBox(17, vec2(306,395), vec2(28,100))
    EGP:egpColor(17, vec(0,0,0))
    
    # BLACK-KEY 6#
    EGP:egpBox(18, vec2(406,395), vec2(28,100))
    EGP:egpColor(18, vec(0,0,0))
    
    # BLACK-KEY 7#
    EGP:egpBox(19, vec2(456,395), vec2(28,100))
    EGP:egpColor(19, vec(0,0,0))
    
    ######### BLACK GRADIENTS
    
    # BLACK-KEY GRADIENT 1#
    EGP:egpBox(20, vec2(56,414), vec2(24,50))
    EGP:egpColor(20, vec(200))
    EGP:egpMaterial(20, "gui/gradient_up")
    EGP:egpAlpha(20, 45)
    
    # BLACK-KEY GRADIENT 2#
    EGP:egpBox(21, vec2(106,414), vec2(24,50))
    EGP:egpColor(21, vec(200))
    EGP:egpMaterial(21, "gui/gradient_up")
    EGP:egpAlpha(21, 45)
    
    # BLACK-KEY GRADIENT 3#
    EGP:egpBox(22, vec2(206,414), vec2(24,50))
    EGP:egpColor(22, vec(200))
    EGP:egpMaterial(22, "gui/gradient_up")
    EGP:egpAlpha(22, 45)
    
    # BLACK-KEY GRADIENT 4#
    EGP:egpBox(23, vec2(256,414), vec2(24,50))
    EGP:egpColor(23, vec(200))
    EGP:egpMaterial(23, "gui/gradient_up")
    EGP:egpAlpha(23, 45)
    
    # BLACK-KEY GRADIENT 5#
    EGP:egpBox(24, vec2(306,414), vec2(24,50))
    EGP:egpColor(24, vec(200))
    EGP:egpMaterial(24, "gui/gradient_up")
    EGP:egpAlpha(24, 45)
    
    # BLACK-KEY GRADIENT 6#
    EGP:egpBox(25, vec2(406,414), vec2(24,50))
    EGP:egpColor(25, vec(200))
    EGP:egpMaterial(25, "gui/gradient_up")
    EGP:egpAlpha(25, 45)
    
    # BLACK-KEY GRADIENT 7#
    EGP:egpBox(26, vec2(456,414), vec2(24,50))
    EGP:egpColor(26, vec(200))
    EGP:egpMaterial(26, "gui/gradient_up")
    EGP:egpAlpha(26, 45)
    
    ######################################
    # WHITE KEY UNDERLINE
    EGP:egpBox(27, vec2(256,501), vec2(498,8))
    EGP:egpColor(27, vec(0,0,0))
    EGP:egpAlpha(27, 100)
    
    ######################################
    # STUDIO BACKGROUND
    EGP:egpBox(28, vec2(256,165), vec2(500,320))
    EGP:egpColor(28, vec(51))
    
    ######################################
    # WHITE KEYS SOUND LINES
    
    # WHITE-KEY SOUNDLINE 1#
    EGP:egpBox(29, vec2(31,165), vec2(2,320))
    EGP:egpColor(29, vec(255,84,0))
    
    # WHITE-KEY SOUNDLINE 2#
    EGP:egpBox(30, vec2(81,165), vec2(2,320))
    EGP:egpColor(30, vec(255,255,0))

    # WHITE-KEY SOUNDLINE 3#
    EGP:egpBox(31, vec2(131,165), vec2(2,320))
    EGP:egpColor(31, vec(83,255,0))
    
    # WHITE-KEY SOUNDLINE 4#
    EGP:egpBox(32, vec2(181,165), vec2(2,320))
    EGP:egpColor(32, vec(0,255,0))
    
    # WHITE-KEY SOUNDLINE 5#
    EGP:egpBox(33, vec2(231,165), vec2(2,320))
    EGP:egpColor(33, vec(0,255,171))
    
    # WHITE-KEY SOUNDLINE 6#
    EGP:egpBox(34, vec2(281,165), vec2(2,320))
    EGP:egpColor(34, vec(0,171,255))
    
    # WHITE-KEY SOUNDLINE 7#
    EGP:egpBox(35, vec2(331,165), vec2(2,320))
    EGP:egpColor(35, vec(0,0,255))
    
    # WHITE-KEY SOUNDLINE 8#
    EGP:egpBox(36, vec2(381,165), vec2(2,320))
    EGP:egpColor(36, vec(83,0,255))
    
    # WHITE-KEY SOUNDLINE 9#
    EGP:egpBox(37, vec2(431,165), vec2(2,320))
    EGP:egpColor(37, vec(255,0,171))
    
    # WHITE-KEY SOUNDLINE 10#
    EGP:egpBox(38, vec2(481,165), vec2(2,320))
    EGP:egpColor(38, vec(255,0,84))
    
    ######################################
    # BLACK KEYS SOUNDLINES 
    
    # BLACK-KEY SOUNDLINE 1#
    EGP:egpBox(39, vec2(56,165), vec2(2,320))
    EGP:egpColor(39, vec(70))
    
    # BLACK-KEY SOUNDLINE 2#
    EGP:egpBox(40, vec2(106,165), vec2(2,320))
    EGP:egpColor(40, vec(70))
    
    # BLACK-KEY SOUNDLINE 3#
    EGP:egpBox(41, vec2(206,165), vec2(2,320))
    EGP:egpColor(41, vec(70))
    
    # BLACK-KEY SOUNDLINE 4#
    EGP:egpBox(42, vec2(256,165), vec2(2,320))
    EGP:egpColor(42, vec(70))
    
    # BLACK-KEY SOUNDLINE 5#
    EGP:egpBox(43, vec2(306,165), vec2(2,320))
    EGP:egpColor(43, vec(70))
    
    # BLACK-KEY SOUNDLINE 6#
    EGP:egpBox(44, vec2(406,165), vec2(2,320))
    EGP:egpColor(44, vec(70))
    
    # BLACK-KEY SOUNDLINE 7#
    EGP:egpBox(45, vec2(456,165), vec2(2,320))
    EGP:egpColor(45, vec(70))
    
    # RETURN TO MENU SHADOW
    EGP:egpText(47, "RETURN TO MENU", vec2(7,327))
    EGP:egpFont(47, "Roboto")
    EGP:egpColor(47, vec(0))
    EGP:egpSize(47, 16)
    
    # RETURN TO MENU
    EGP:egpText(48, "RETURN TO MENU", vec2(6,326))
    EGP:egpFont(48, "Roboto")
    EGP:egpColor(48, vec(255))
    EGP:egpSize(48, 16)
    
    # PLAY SHADOW
    EGP:egpText(49, "PLAY", vec2(256,327))
    EGP:egpFont(49, "Roboto")
    EGP:egpColor(49, vec(0))
    EGP:egpSize(49, 16)
    
    # PLAY
    EGP:egpText(50, "PLAY", vec2(255,326))
    EGP:egpFont(50, "Roboto")
    EGP:egpColor(50, vec(255))
    EGP:egpSize(50, 16)
    
    # CLEAR SHADOW
    EGP:egpText(51, "CLEAR", vec2(461,327))
    EGP:egpFont(51, "Roboto")
    EGP:egpColor(51, vec(0))
    EGP:egpSize(51, 16)
    
    # CLEAR
    EGP:egpText(52, "CLEAR", vec2(460,326))
    EGP:egpFont(52, "Roboto")
    EGP:egpColor(52, vec(255))
    EGP:egpSize(52, 16)
    
    for(I=53,119){EGP:egpBox(100, vec2(-10,-10), vec2(10,10))}
    
    # SEQUENCER MARKER
    EGP:egpBox(120, vec2(256,320), vec2(500,10))
    EGP:egpAlpha(120, 60)
    
    for(Y=1,32){ASequencer[Y,array]=array()}
    ACurHolo=53
    ASequence=32
}

# WHITE SOUNDLINES DETECTION
for(I=29,38){
if(inrange(Cursor, EGP:egpPos(I)-vec2(6,160), EGP:egpPos(I)+vec2(6,160))){
    EGP:egpBoxOutline(46, vec2(EGP:egpPos(I):x(),round(Cursor:y()/10)*10), vec2(10,10)) 
    ASoundLineHeight = (round(Cursor:y()/10)*10)/10
    ASoundLineIndexW = 38-I

    if(ACurHolo<119&Use1&(ASequencer[ASoundLineIndexW,array][ASoundLineHeight,number]==0)){
        ASequencer[ASoundLineIndexW,array][ASoundLineHeight,number]=ACurHolo 
        EGP:egpBox(ACurHolo, vec2(EGP:egpPos(I):x(),round(Cursor:y()/10)*10), vec2(10,10))
        EGP:egpColor(ACurHolo,EGP:egpColor(I))
        ACurHolo++
        ALag=10
        }
}}

# BLACKS SOUNDLINES DETECTION
for(I=39,45){
if(inrange(Cursor, EGP:egpPos(I)-vec2(6,160), EGP:egpPos(I)+vec2(6,160))){
    EGP:egpBoxOutline(46, vec2(EGP:egpPos(I):x(),round(Cursor:y()/10)*10), vec2(10,10)) 
    ASoundLineHeight = (round(Cursor:y()/10)*10)/10
    ASoundLineIndexB = 45-I
    
    if(ACurHolo<119&Use1&(ASequencer[ASoundLineIndexW,array][ASoundLineHeight,number]==0)){
        ASequencer[ASoundLineIndexW,array][ASoundLineHeight,number]=ACurHolo 
        EGP:egpBox(ACurHolo, vec2(EGP:egpPos(I):x(),round(Cursor:y()/10)*10), vec2(10,10))
        EGP:egpColor(ACurHolo,vec(70))
        ACurHolo++
        ALag=10
        } 
}}
 

# KEYBOARD FUNCTIONALITY
# BLACKS
for(I=13,19){
    if(inrange(Cursor, EGP:egpPos(I)-EGP:egpSize(I)/2,EGP:egpPos(I)+EGP:egpSize(I)/2)){
        EGP:egpColor(I, vec(80))
        AOnBlack=1
        if(Use1){ soundPitch(1, ABlackKeysPitch[(I-12),number]/6.60*2) AVolume1=1 }
    }else{EGP:egpColor(I, vec(0))}    
}

 # WHITES
if(AOnBlack==0){
    for(I=3,12){
        if(inrange(Cursor, EGP:egpPos(I)-EGP:egpSize(I)/2,EGP:egpPos(I)+EGP:egpSize(I)/2)){
        EGP:egpColor(I, hsv2rgb(vec((I-2)*32,1,1)))
        if(Use1){ soundPitch(1, AWhiteKeysPitch[(I-2),number]/6.60*2) AVolume1=1 }
    }else{EGP:egpColor(I, vec(255))}            
}
    
    
    
    
}

# CLEARING SEQUENCER
if(inrange(Cursor, EGP:egpPos(52), EGP:egpPos(52)+vec2(40,15))){
    EGP:egpColor(52, vec(255,150,150))
    if(Use1&$Use1){reset()}
    }else{
    EGP:egpColor(52, vec(255))
}

# PLAYING
if(inrange(Cursor, EGP:egpPos(50), EGP:egpPos(52)+vec2(35,15))){
    EGP:egpColor(50, vec(150,255,150))
    if(Use1&$Use1){APlay=1}
    }else{
    EGP:egpColor(50, vec(255))
}

if(APlay){
    ASequence-=0.4
    
    for(I=1,17){
        if(ASequencer[I,array][round(ASequence),number]>0){ 
            soundPitch(I, AWhiteKeysPitch[I,number]/6.60*2) 
            AVolumeArray[I,number]=2
            }
        }
    
    EGP:egpBox(120, vec2(256,(ASequence*10)-0), vec2(500,10))
    
    if(ASequence<1){APlay=0 ASequence=32}
    }



print(4, round(ASequence):toString())
#print(4, ASequencer[ASoundLineIndexW,array][ASoundLineHeight,number]:toString() )

#soundVolume(1, AVolume1)
AVolume1-=0.1
AOnBlack=0
ALag-=0.5
for(I=1,17){
    AVolumeArray[I,number]=AVolumeArray[I,number]-0.2
    soundVolume(I, AVolumeArray[I,number])
    }



if(first()){
    entity():setAlpha(0)
    holoCreate(890)
    holoPos(890, entity():toWorld(vec(0,0,3)))
    holoAng(890, entity():toWorld(ang(0,0,0)))
    holoModel(890, "rcube_thin")
    holoScaleUnits(890,vec(6.6,6.6,6.6))
    holoClipEnabled(890,1,1)
    holoClip(890,1,vec(0,0,-2.3),vec(0,0,-1),0)
    holoParent(890,entity())
    holoColor(890, vec(60))
    #
    holoCreate(891)
    holoPos(891, entity():toWorld(vec(0,0,0.7)))
    holoAng(891, entity():toWorld(ang(0,0,0)))
    holoModel(891, "rcube_thin")
    holoScaleUnits(891,vec(6.6,6.6,0.01))
    holoParent(891,entity())
    holoColor(891, vec(60))
    holoMaterial(891, "debug/debugdrawflat")
    #
    holoCreate(892)
    holoPos(892, entity():toWorld(vec(0,0,0.75)))
    holoAng(892, entity():toWorld(ang(0,0,0)))
    holoModel(892, "rcube_thin")
    holoScaleUnits(892,vec(5.9,5.9,0.05))
    holoParent(892,entity())
    holoColor(892, vec(255))
    holoMaterial(892, "debug/debugdrawflat")
    #
    holoCreate(893)
    holoPos(893, entity():toWorld(vec(-1.5,-1.5,0.85)))
    holoAng(893, entity():toWorld(ang(0,0,0)))
    holoModel(893, "plane")
    holoScaleUnits(893,vec(8.6,8.6,10))
    holoParent(893,entity())
    holoColor(893, vec(160))
    holoMaterial(893, "expression 2/cog_prop")
    #
    holoClipEnabled(893,1,1)
    holoClipEnabled(893,2,1)
    holoClipEnabled(893,3,1)
    holoClip(893,1,entity():toWorld(vec(-2.85,-2.85,0.42)),entity():forward(),1)
    holoClip(893,2,entity():toWorld(vec(-2.85,-2.85,0.42)),-entity():right(),1)
    holoClip(893,3,entity():toWorld(vec(-2.2,-2.2,0.42)),entity():forward()-entity():right(),1)
    
    }
    
if(entity():vel()){
    holoClip(893,1,entity():toWorld(vec(-2.85,-1.5,0.42)),entity():forward(),1)
    holoClip(893,2,entity():toWorld(vec(-2.85,-2.85,0.42)),-entity():right(),1)
    holoClip(893,3,entity():toWorld(vec(-2.2,-2.2,0.42)),entity():forward()-entity():right(),1)
    }
    holoAng(893, entity():toWorld(ang(0,-curtime()*14,0)))
