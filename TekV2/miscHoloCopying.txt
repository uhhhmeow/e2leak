@name miscHoloCopying
@outputs Mode OriginAimPos:vector Props:array Distance Index Scale

interval(100)

if(first()){
    Mode=1
    Index=1
    
    Scale = 1
    
    holoCreate(1000)
    holoModel(1000, "models/sprops/geometry/sphere_144.mdl")
    holoMaterial(1000, "models/wireframe")
    holoScaleUnits(1000, vec(18))
    #holoColor(1000, vec(255))
    holoAlpha(1000, 55)
    
    holoCreate(2000)
    holoModel(2000, "models/sprops/geometry/sphere_144.mdl")
    holoMaterial(2000, "models/wireframe")
    holoScaleUnits(2000, vec(-18))
    holoParent(2000, holoEntity(1000))
    holoAlpha(2000, 15)
    #holoColor(2000, vec(160))
}

if(changed(owner():keyUse()) & owner():keyUse() & Mode==1){
    OriginAimPos=owner():aimPos()
    holoPos(1000, OriginAimPos)
    holoAng(1000,ang(0))
    }

if(owner():keyUse() & Mode==1){
    holoScaleUnits(1000, vec(OriginAimPos:distance(owner():aimPos())*2))
    holoScaleUnits(2000, -vec(OriginAimPos:distance(owner():aimPos())*2))
    
    Distance=OriginAimPos:distance(owner():aimPos())*2
    }

if(changed(owner():keyReload()) & owner():keyReload() & Mode==1){
    
    findByClass("prop_*")
    findInSphere(holoEntity(1000):pos(), Distance/2)
    
    Props = findToArray()
    
    Mode=2
    hint("spawning..", 2)
    }

if(Mode==2 & holoCanCreate()){
    
    if(Props[Index,entity] & !holoEntity(Index)){holoCreate(Index)}
    holoModel(Index, Props[Index,entity]:model())
    holoAlpha(Index, Props[Index,entity]:getAlpha())
    holoAng(Index, Props[Index,entity]:angles())
    holoMaterial(Index, Props[Index,entity]:getMaterial())
    holoColor(Index, Props[Index,entity]:getColor())
    holoScale(Index, vec(Scale))
    holoPos(Index, entity():toWorld(holoEntity(1000):toLocal(Props[Index,entity]:pos())))
    Index++
    if(Index>Props:count()){Mode=1}
    
    
    
    
    
    
    
    
    
    }




