@name iDancingLamp_bottom
@outputs Model:string Size:vector T ClipOffset MovementType MovementTimer BottomHoloLock TopHoloLock Scale

interval(1) # DEFAULT 30
MovementTimer++
#make two vars for movement and mix them when changing mode

if(first()){
    Scale=1
    
    # SETTINGS
    Model = "models/mossman.mdl"
    
    # models/props_c17/gravestone_statue001a.mdl
    # models/props_interiors/furniture_lamp01a.mdl
    # models/props_trainstation/payphone001a.mdl
    # models/ichthyosaur.mdl
    # models/maxofs2d/gm_painting.mdl
    # models/props_wasteland/lights_industrialcluster01a.mdl
    # models/props_trainstation/clock01.mdl
    # models/props_trainstation/handrail_64decoration001a.mdl
    # models/props_c17/gravestone_cross001a.mdl
    # models/props_trainstation/pole_448connection001a.mdl
    # models/props_wasteland/buoy01.mdl
    # models/props_c17/bench01a.mdl
    # models/props_c17/Lockers001a.mdl
    # models/props_c17/truss03b.mdl
    # models/props_trainstation/light_256ceilingmounted001a.mdl
    # models/props_wasteland/prison_archgate002a.mdl
    # models/props_wasteland/wood_fence01a.mdl
    # models/props_buildings/watertower_002a.mdl
    # models/props_buildings/watertower_001a.mdl
    # models/props_c17/substation_circuitbreaker01a.mdl
    # models/props_debris/plaster_wall001a.mdl
    # models/props_debris/plaster_wall002a.mdl
    
    # CLIP OFFSET(60 IS DEFAULT, 40 WORKS FINE AS WELL) SET LOWER TO GET RID OF GAPS AT COST OF FIDELITY
    # FOR BOTTOM ALIGNED MODELS DEFAULT IS 120
    ClipOffset = 120
    
    # MOVEMENT TYPE
    MovementType = 1
    
    # 1 - mild front-back swinging
    # 2 - strong front-back swinging        # FRONT-BACK SWING TYPE
    # 3 - extreme front-back swinging
    
    # 4 - mild side to side swinging
    # 5 - strong side to side swinging        # SIDE TO SIDE SWING TYPE
    # 6 - extreme side to side swinging
    
    # 7 - mild swing-rotating around
    # 8 - extreme swing-rotating around     # ROTATION TYPE
    # 9 - mild center-rotating around
    
    # 10 - looks at owner(yaw rotation)
    # 11 - full body twist                  # VARIOUS
    
    
    # HOLOGRAM LOCK, LOCKS TOP HOLOGRAMS FROM CHANGING ANGLE
    # USEFULL FOR EXAMPLE WITH LAMP TO MAKE ITS LAMPSHADE NOT BEND
    BottomHoloLock = 1 # DEFAULT IS 1
    TopHoloLock = 23 # DEFAULT IS 30
    
    
    
    
    
    
    
    
    
    
    
    
    
    # CREATING BASE
    holoCreate(0)
    holoModel(0, Model)
    holoPos(0, entity():toWorld(vec(0,0,0)))
    Size=holoEntity(0):boxSize()
    holoAlpha(0,0)
    holoParent(0,entity())
        
    # POSITIONING AND SETTING JOINTS
    for(I=1,30){
        holoCreate(I)
        holoPos(I, holoEntity(0):toWorld((vec(0,0,-(Size:z()/60)+((Size:z()/30)*I)))*Scale))
        holoScaleUnits(I, vec(40,0.2,0.2))
        holoColor(I, hsv2rgb(vec(I*12,1,1)))
        holoParent(I, holoEntity(I-1))
        holoColor(1, vec(255))
        holoColor(30,vec(0))
        #holoAlpha(I,0)
        
        }
    
    # POSITIONING AND SETTING UP VISCLIP
    for(I=31,60){
        holoCreate(I)
        holoScale(I, vec(Scale))
        holoModel(I, Model)
        holoPos(I, holoEntity(0):toWorld((vec(0,0,0))*Scale))
        #holoColor(I, vec(255))
        holoMaterial(I, "debug/debugdrawflat")
        
        holoColor(I, hsv2rgb(vec(-(I-30)*12,1,1)))
        
        holoClipEnabled(I,1,1)
        holoClipEnabled(I,2,1)
        
        holoParent(I, holoEntity(30-(I-30)))
    
        # LIMITER
        if(I>31){
        if(I<60){
        holoClip(I,1,vec(0,0,(-(Size:z()/ClipOffset)+Size:z()-((Size:z()/60)*(I-15))))*Scale,vec(0,0,1),0)
        holoClip(I,2,vec(0,0,((Size:z()/ClipOffset)+Size:z()-((Size:z()/60)*(I-15))))*Scale,vec(0,0,-1),0)
        }
        else{holoClip(I,2,vec(0,0,((Size:z()/ClipOffset)+Size:z()-((Size:z()/60)*(I-15))))*Scale,vec(0,0,-1),0)}
    }else{
    holoClip(I,1,vec(0,0,(-(Size:z()/ClipOffset)+Size:z()-((Size:z()/60)*(I-15))))*Scale,vec(0,0,1),0)
    
}
    
    
    #holoModel(50, "models/props_interiors/furniture_lamp01a.mdl")
    #holoPos(50, entity():toWorld(vec(0,0,Size:z()/2)))
    
    
}}

###### MOVEMENT TYPES

# FRONT-BACK
if(MovementType==1){for(I=BottomHoloLock,TopHoloLock){holoAng(I, entity():toWorld(ang(cos(MovementTimer*2)*(I*(I/20)),0,0)))}} # MILD FRONT-BACK SWINGING
if(MovementType==2){for(I=BottomHoloLock,TopHoloLock){holoAng(I, entity():toWorld(ang(cos(MovementTimer*2)*(I*(I/8)),0,0)))}} # STRONG FRONT-BACK SWINGING
if(MovementType==3){for(I=BottomHoloLock,TopHoloLock){holoAng(I, entity():toWorld(ang(cos(MovementTimer*2)*(I*(I/4)),0,0)))}} # EXTREME SWINGING FRONT-BACK

# SIDE TO SIDE
if(MovementType==4){for(I=BottomHoloLock,TopHoloLock){holoAng(I, entity():toWorld(ang(0,0,cos(MovementTimer*2)*(I*(I/20)))))}} # MILD FRONT-BACK SWINGING
if(MovementType==5){for(I=BottomHoloLock,TopHoloLock){holoAng(I, entity():toWorld(ang(0,0,cos(MovementTimer*2)*(I*(I/8)))))}} # STRONG FRONT-BACK SWINGING
if(MovementType==6){for(I=BottomHoloLock,TopHoloLock){holoAng(I, entity():toWorld(ang(0,0,cos(MovementTimer*2)*(I*(I/4)))))}} # EXTREME SWINGING FRONT-BACK

# ROTATION TYPES
if(MovementType==7){for(I=BottomHoloLock,TopHoloLock){holoAng(I, entity():toWorld(ang(cos(MovementTimer*2)*((I/2)*(I/20)),0,sin(MovementTimer*2)*((I/2)*(I/20)))))}} # MILD SWING-ROTATING AROUND
if(MovementType==8){for(I=BottomHoloLock,TopHoloLock){holoAng(I, entity():toWorld(ang(cos(MovementTimer*2)*((I)*(I/10)),0,sin(MovementTimer*2)*((I)*(I/10)))))}} # MILD SWING-ROTATING AROUND
if(MovementType==9){for(I=BottomHoloLock,TopHoloLock){holoAng(I, entity():toWorld(ang(0,cos(MovementTimer*2)*((I/2)*(I/10)),0)))}} # MILD CENTER-ROTATING AROUND

# VARIED
if(MovementType==10){for(I=BottomHoloLock,TopHoloLock){holoAng(I, entity():toWorld(ang(0,(-entity():bearing(owner():pos())/30)*I,0)))}} # LOOKS AT OWNER(yaw rotation)
if(MovementType==11){for(I=BottomHoloLock,TopHoloLock){holoAng(I, entity():toWorld(ang(cos((MovementTimer+(I*8))*4)*20,0,sin((MovementTimer+(I*8))*4)*20)))}}













if(first()){
    entity():setAlpha(0)
    holoCreate(890)
    holoPos(890, entity():toWorld(vec(0,0,3)))
    holoAng(890, entity():toWorld(ang(0,0,0)))
    holoModel(890, "rcube_thin")
    holoScaleUnits(890,vec(6.6,6.6,6.6))
    holoClipEnabled(890,1,1)
    holoClip(890,1,vec(0,0,-2.3),vec(0,0,-1),0)
    holoParent(890,entity())
    holoColor(890, vec(60))
    #
    holoCreate(891)
    holoPos(891, entity():toWorld(vec(0,0,0.7)))
    holoAng(891, entity():toWorld(ang(0,0,0)))
    holoModel(891, "rcube_thin")
    holoScaleUnits(891,vec(6.6,6.6,0.01))
    holoParent(891,entity())
    holoColor(891, vec(60))
    holoMaterial(891, "debug/debugdrawflat")
    #
    holoCreate(892)
    holoPos(892, entity():toWorld(vec(0,0,0.75)))
    holoAng(892, entity():toWorld(ang(0,0,0)))
    holoModel(892, "rcube_thin")
    holoScaleUnits(892,vec(5.9,5.9,0.05))
    holoParent(892,entity())
    holoColor(892, vec(255))
    holoMaterial(892, "debug/debugdrawflat")
    #
    holoCreate(893)
    holoPos(893, entity():toWorld(vec(-1.5,-1.5,0.85)))
    holoAng(893, entity():toWorld(ang(0,0,0)))
    holoModel(893, "plane")
    holoScaleUnits(893,vec(8.6,8.6,10))
    holoParent(893,entity())
    holoColor(893, vec(160))
    holoMaterial(893, "expression 2/cog_prop")
    #
    holoClipEnabled(893,1,1)
    holoClipEnabled(893,2,1)
    holoClipEnabled(893,3,1)
    holoClip(893,1,entity():toWorld(vec(-2.85,-2.85,0.42)),entity():forward(),1)
    holoClip(893,2,entity():toWorld(vec(-2.85,-2.85,0.42)),-entity():right(),1)
    holoClip(893,3,entity():toWorld(vec(-2.2,-2.2,0.42)),entity():forward()-entity():right(),1)
    
    }
    
if(entity():vel()){
    holoClip(893,1,entity():toWorld(vec(-2.85,-1.5,0.42)),entity():forward(),1)
    holoClip(893,2,entity():toWorld(vec(-2.85,-2.85,0.42)),-entity():right(),1)
    holoClip(893,3,entity():toWorld(vec(-2.2,-2.2,0.42)),entity():forward()-entity():right(),1)
    }
    holoAng(893, entity():toWorld(ang(0,-curtime()*14,0)))




