@name iSpring
@inputs 
@outputs Limit T Dlugosc
@persist 
@trigger 

runOnTick(1)

Limit = 20
Dlugosc = 5
Grubosc = 8

if(first()){
    for(I=1, Limit+3){
        holoCreate(I)
        holoModel(I, "hq_cylinder")
        holoScaleUnits(I, vec(Grubosc,Grubosc,Dlugosc))
        
    }
    holoPos(1, entity():toWorld(vec(0,0,0)))
    holoAng(1, entity():toWorld(ang(0,0,0)))
    holoParent(1, entity())
    
    holoPos(Limit+1, holoEntity(Limit):toWorld(vec(0,0,-2.2)))
    holoParent(Limit+1, holoEntity(Limit))
    holoModel(Limit+1, "hq_sphere")
    holoScaleUnits(Limit+1, vec(Grubosc-0.2,Grubosc-0.2,Grubosc))
}

for(I=1, Limit){
    
    if(I==1){
            holoPos(I,entity():pos()+entity():up()*6)    
            Kat = (holoEntity(I+1):pos()-entity():pos()):toAngle()+ang(90,0,0)
            holoAng(I,Kat)
        }
        elseif(I==Limit)
        {
            holoPos(I,holoEntity(I-1):pos()+holoEntity(I-1):up()*Dlugosc)
            Kat = (holoEntity(I-1):pos()-holoEntity(I):pos()):toAngle()+ang(90,0,0)
            holoAng(I,Kat)    
        }
        else
        {
            holoPos(I,holoEntity(I-1):pos()+holoEntity(I-1):up()*Dlugosc)
            Kat = (holoEntity(I+1):pos()-holoEntity(I-1):pos()):toAngle()+ang(90,0,0)
            holoAng(I,Kat)
            holoScaleUnits(I, vec(Grubosc,Grubosc,holoEntity(I-1):pos():distance(holoEntity(I):pos())))
        }      
    
}



if(T>360){T=0}
#[if(owner():keyUse()){
#    holoAng(1, ang(45,0,0))
#}
holoParentAttachment(1, owner(), "anim_attachment_RH")
    holoPos(1,owner():attachmentPos("anim_attachment_RH"))
    holoAng(1,owner():attachmentAng("anim_attachment_RH")+ang(0,45,0))
