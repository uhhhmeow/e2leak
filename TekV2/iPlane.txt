@name iPlane
#@model models/hunter/plates/plate1x1.mdl 
@outputs Debug Turrets:array Plane:entity Center:entity T GroundRanger:ranger WheelsSwitch WheelsSwitchTimer Player:entity W A S D Shift Space Ctrl
@outputs WheelRangerBackD WheelRangerRightD WheelRangerLeftD PlanePos:vector GroundRangerD M1 Weaponize PropellerSpeed2
@outputs PlaneAng:angle Pitch Yaw Roll PlaneAngNorm:angle PlaneVecVel:vector PlaneAngVel:angle PropellerSpeed SpeedDisp Rotation M2 EyePitch EyeYaw Inertia:angle
@outputs Nitro [HullBodyColor HullSecondaryColor WingsColor PropellerColor EngineColor RotorColor DecalColor]:vector TrailMode FrontRanger1:ranger Timer HoloFix
@outputs FrontRanger2:ranger WallAvoid IsInSeat YawDot PitchDot YawDotTemp PitchDotTemp [Turret1 Turret2]:entity ParentTimer Parented [Fire1 Fire2]:wirelink
runOnTick(1)
runOnLast(1)
###### CREDITS ############################################################################################################
# THIS PLANE WAS MADE BY.... WHO GIVES A SHIT ? AM I GOING TO BE E-FAMOUS IF I PUT MY NICK HERE ?.. didn't think so.      #
# Oh... but i kinda took part of Raveos plane code for this one so there you have it...  and Name helped with weaponizing #
# who cares about credits though..........................My name is Tek and I made this plane, SPREAD THE WORD !!!!!     #
###########################################################################################################################

### STEERING ##########
#
# W - Accelerate
# S - Decelerate
# Mouse - Movement
#
########################
#hint(ops():toString(), 0.1)

###########################################
####  OPTIONS #############################
if(first()){
# PLANE COLOR
HullBodyColor = hsv2rgb((random(1,360)*curtime())%360,1,1)
HullSecondaryColor = vec(153,0,51)
WingsColor = vec(153,0,51)
PropellerColor = HullBodyColor
EngineColor = vec(60,60,60)
RotorColor = vec(0,0,0)
DecalColor = vec(0,0,0)

# WALL AVOIDER(turning it on will make the plane avoid walls(most of the time))
WallAvoid=1

# WEAPONIZE(enables turrets)
Weaponize=1

# HOLO FIX(fixes broken after-patch triangle tail)
HoloFix=1


###########################################

################## WARNING ################
######  MESSY AS FUCK CODE AHEAD !  #######
###########################################
# ... you have been warned ...








Plane = entity():isWeldedTo()

}




M1 = Player:keyAttack1()
M2 = Player:keyAttack2()
W = Player:keyForward()
A = Player:keyLeft()
S = Player:keyBack()
D = Player:keyRight()
Shift = Player:keySprint()
Ctrl = Player:keyDuck()
Space = Player:keyJump()
    
if(changed(Plane:driver())){Player = Plane:driver()}

if(first()){
    for(I=0,62){
        holoCreate(I) 
        #holoDisableShading(I, 1)
        #holoColor(I, randvec(100,255))
        #holoColor(I, vec(100,100,100))
        #holoMaterial(I, "phoenix_storms//pack2/redlight")
        
        }
    
    holoPos(0, Plane:toWorld(vec(0,10,10)))
    holoAng(0, Plane:toWorld(ang(0,-90,0)))
    holoAlpha(0, 0)
    
    Center = holoEntity(0)
    
    # RPOPELLER ROTATION CENTER
    holoPos(16, Plane:toWorld(vec(0,10,10)))
    holoAng(16, Plane:toWorld(ang(0,-90,0)))
    holoAlpha(16, 0)
    
    # HULL
    holoPos(1, Center:toWorld(vec(0,0,0)))
    holoAng(1, Center:toWorld(ang(0,0,0)))
    #holoMaterial(1, "bull/various/subwoofer")
    holoModel(1, "hq_sphere")
    holoColor(1, HullBodyColor)
    holoScaleUnits(1, vec(160,30,30))
    holoParent(1, Center)
    
    holoClipEnabled(1,1,1)
    holoClip(1,1, vec(-1.5,0,0),vec(1,0,0), 0)
    
    # RIGHT TAIL DECAL(PREVIOUS HOLO, INTERSECTION HAD TO BE REMOVED DUE TO INCOMPATIBLE CLIPPING)
    #holoPos(2, Center:toWorld(vec(-0.7,0,0)))
    holoPos(2, Center:toWorld(vec(42.2,12,6)))
    holoAng(2, Center:toWorld(ang(0,180,90)))
    #holoMaterial(2, "bull/various/subwoofer")
    holoModel(2, "right_prism")
    holoColor(2, HullBodyColor)
    if(HoloFix==1){holoScaleUnits(2, vec(23,2,23))}else{holoScaleUnits(2, vec(2,23,23))}
    holoParent(2, Center)
    
    # FRONT RQCYLINDER
    holoPos(3, Center:toWorld(vec(-1.4,0,0)))
    holoAng(3, Center:toWorld(ang(90,0,0)))
    #holoMaterial(3, "bull/various/subwoofer")
    holoModel(3, "hq_rcylinder_thick")
    holoColor(3, HullSecondaryColor)
    holoScaleUnits(3, vec(30,30,40))
    holoParent(3, Center)
    
    holoClipEnabled(3,1,1)
    holoClip(3,1, vec(0,0,0),vec(0,0,-1), 0)
    
    holoClipEnabled(3,2,1)
    holoClip(3,2, vec(0,0,-17),vec(0,0,1), 0)
    
    #FIRST RING AROUND PROPELLER
    holoPos(4, Center:toWorld(vec(-18,0,0)))
    holoAng(4, Center:toWorld(ang(90,0,0)))
    #holoMaterial(4, "bull/various/subwoofer")
    holoModel(4, "hq_hdome_thin")
    holoColor(4, HullSecondaryColor)
    holoScaleUnits(4, vec(26,26,35))
    holoParent(4, Center)
    
    # SECOND RING AROUND PROPELLER
    holoPos(5, Center:toWorld(vec(-17.5,0,0)))
    holoAng(5, Center:toWorld(ang(90,0,0)))
    #holoMaterial(5, "bull/various/subwoofer")
    holoModel(5, "hq_hdome_thin")
    holoColor(5, HullSecondaryColor)
    holoScaleUnits(5, vec(24,24,33))
    holoParent(5, Center)
    
    # PROPELLER RAIL #1
    holoPos(6, Center:toWorld(vec(-16,0,0)))
    holoAng(6, Center:toWorld(ang(0,0,0)))
    #holoMaterial(6, "bull/various/subwoofer")
    holoModel(6, "hq_cylinder")
    holoColor(6, EngineColor)
    holoScaleUnits(6, vec(2.5,2.5,23))
    holoParent(6, Center)
    
    # PROPELLER RAIL #2
    holoPos(7, Center:toWorld(vec(-16,0,0)))
    holoAng(7, Center:toWorld(ang(0,0,-45)))
    #holoMaterial(7, "bull/various/subwoofer")
    holoModel(7, "hq_cylinder")
    holoColor(7, EngineColor)
    holoScaleUnits(7, vec(2.5,2.5,23))
    holoParent(7, Center)
    
    # PROPELLER RAIL #3
    holoPos(8, Center:toWorld(vec(-16,0,0)))
    holoAng(8, Center:toWorld(ang(0,0,45)))
    #holoMaterial(8, "bull/various/subwoofer")
    holoModel(8, "hq_cylinder")
    holoColor(8, EngineColor)
    holoScaleUnits(8, vec(2.5,2.5,23))
    holoParent(8, Center)
    
    # PROPELLER RAIL #4
    holoPos(9, Center:toWorld(vec(-16,0,0)))
    holoAng(9, Center:toWorld(ang(0,0,90)))
    #holoMaterial(9, "bull/various/subwoofer")
    holoModel(9, "hq_cylinder")
    holoColor(9, EngineColor)
    holoScaleUnits(9, vec(2.5,2.5,23))
    holoParent(9, Center)
    
    # PROPELLER BASE
    holoPos(10, Center:toWorld(vec(-17.5,0,0)))
    holoAng(10, Center:toWorld(ang(90,0,0)))
    #holoMaterial(10, "bull/various/subwoofer")
    holoModel(10, "hq_dome")
    holoColor(10, EngineColor)
    holoScaleUnits(10, vec(12,12,100))
    holoParent(10, Center)
    
    # PROPPELER BACKGROUND
    holoPos(11, Center:toWorld(vec(-16,0,0)))
    holoAng(11, Center:toWorld(ang(90,0,0)))
    holoMaterial(11, "bull/various/subwoofer")
    holoModel(11, "hq_cylinder")
    holoColor(11, vec(200))
    holoScaleUnits(11, vec(25,25,0.1))
    holoParent(11, Center)
    
    # PROPPELER FIRST ROTOR THING..
    holoPos(12, Center:toWorld(vec(-18,0,0)))
    holoAng(12, Center:toWorld(ang(90,0,0)))
    #holoMaterial(12, "bull/various/subwoofer")
    holoModel(12, "hq_cylinder")
    holoColor(12, EngineColor)
    holoScaleUnits(12, vec(8,8,5))
    holoParent(12, holoEntity(16))
    
    # PROPPELER WINGS BASE+FINISH
    holoPos(13, Center:toWorld(vec(-19.5,0,0)))
    holoAng(13, Center:toWorld(ang(-90,0,0)))
    #holoMaterial(13, "bull/various/subwoofer")
    holoModel(13, "hq_dome")
    holoColor(13, HullSecondaryColor)
    holoScaleUnits(13, vec(12,12,12))
    holoParent(13, holoEntity(16))
    
    # PROPPELER WING 1
    holoPos(14, Center:toWorld(vec(-21,0,15)))
    holoAng(14, Center:toWorld(ang(0,25,0)))
    #holoMaterial(14, "bull/various/subwoofer")
    holoModel(14, "hq_rcube_thin")
    holoColor(14, PropellerColor)
    holoScaleUnits(14, vec(1,4,30))
    holoParent(14, holoEntity(16))
    
    # PROPPELER WING 1
    holoPos(15, Center:toWorld(vec(-21,0,-15)))
    holoAng(15, Center:toWorld(ang(0,-25,0)))
    #holoMaterial(15, "bull/various/subwoofer")
    holoModel(15, "hq_rcube_thin")
    holoColor(15, PropellerColor)
    holoScaleUnits(15, vec(1,4,30))
    holoParent(15, holoEntity(16))
    
    #SKIP TO HOLOGRAM 17 SINCE 16 IS USED AS ROTOR CENTRE
    
    
    holoClipEnabled(1,2,1)
    holoClip(1,2, vec(40,0,0),vec(-1,0,0), 0)
    
    # HULL TAIL END
    holoPos(17, Center:toWorld(vec(34.8,0,0)))
    holoAng(17, Center:toWorld(ang(0,0,0)))
    #holoMaterial(17, "bull/various/subwoofer")
    holoModel(17, "hq_sphere")
    holoColor(17, HullSecondaryColor)
    holoScaleUnits(17, vec(40,26.8,26.8))
    holoParent(17, Center)
    
    holoClipEnabled(17,1,1)
    holoClip(17,1, vec(5,0,0),vec(1,0,0), 0)
    
    # RIGHT WING
    holoPos(18, Center:toWorld(vec(18,25,0)))
    holoAng(18, Center:toWorld(ang(0,-10,-90)))
    #holoMaterial(18, "phoenix_storms/black_chrome")
    holoModel(18, "hq_cubinder")
    holoColor(18, WingsColor)
    holoScaleUnits(18, vec(30,5,30))
    holoParent(18, Center)
    
    # LEFT WING
    holoPos(19, Center:toWorld(vec(18,-25,0)))
    holoAng(19, Center:toWorld(ang(0,10,90)))
    #holoMaterial(19, "phoenix_storms/black_chrome")
    holoModel(19, "hq_cubinder")
    holoColor(19, WingsColor)
    holoScaleUnits(19, vec(30,5,30))
    holoParent(19, Center)
    
    # WINGS INTERSECTION
    holoPos(20, Center:toWorld(vec(16.2,0,0)))
    holoAng(20, Center:toWorld(ang(0,0,0)))
    #holoMaterial(20, "phoenix_storms/black_chrome")
    holoModel(20, "hq_rcube_thin")
    holoColor(20, HullSecondaryColor)
    holoScaleUnits(20, vec(32,30.7,6))
    holoParent(20, Center)
    
    # TAIL DECAL
    holoPos(21, Center:toWorld(vec(45,0,16)))
    holoAng(21, Center:toWorld(ang(-20,180,0)))
    #holoMaterial(21, "bull/various/subwoofer")
    holoModel(21, "right_prism")
    holoColor(21, HullBodyColor)
    if(HoloFix==1){holoScaleUnits(21, vec(20,2,20))}else{holoScaleUnits(21, vec(2,20,20))}
    holoParent(21, Center)
    
    # RIGHT WING FINISH
    holoPos(22, Center:toWorld(vec(20.6,39.7,0)))
    holoAng(22, Center:toWorld(ang(0,-10,-90)))
    #holoMaterial(22, "phoenix_storms/black_chrome")
    holoModel(22, "hq_dome")
    holoColor(22, WingsColor)
    holoScaleUnits(22, vec(30,5,5))
    holoParent(22, Center)
    
    # LEFT WING FINISH
    holoPos(23, Center:toWorld(vec(20.6,-39.7,0)))
    holoAng(23, Center:toWorld(ang(0,10,90)))
    #holoMaterial(23, "phoenix_storms/black_chrome")
    holoModel(23, "hq_dome")
    holoColor(23, WingsColor)
    holoScaleUnits(23, vec(30,5,5))
    holoParent(23, Center)
    
    # SCREEN HULL
    holoPos(24, Center:toWorld(vec(-2,0,12)))
    holoAng(24, Center:toWorld(ang(-110,0,0)))
    #holoMaterial(24, "bull/various/subwoofer")
    holoModel(24, "hq_dome")
    holoColor(24, HullSecondaryColor)
    holoScaleUnits(24, vec(13,15,10))
    holoParent(24, Center)
    
    # SCREEN 
    holoPos(25, Center:toWorld(vec(-1.9,0,12)))
    holoAng(25, Center:toWorld(ang(-110,0,0)))
    #holoMaterial(25, "bull/various/subwoofer")
    holoModel(25, "hq_dome")
    holoColor(25, vec(0,0,0))
    holoScaleUnits(25, vec(12.5,14.5,10))
    holoParent(25, Center)
    
    # LEFT TAIL DECAL
    #holoPos(26, Center:toWorld(vec(-0.7,0,0)))
    holoPos(26, Center:toWorld(vec(42.2,-12,6)))
    holoAng(26, Center:toWorld(ang(0,-180,-90)))
    #holoMaterial(2, "bull/various/subwoofer")
    holoModel(26, "right_prism")
    holoColor(26, HullBodyColor)
    if(HoloFix==1){holoScaleUnits(26, vec(23,2,23))}else{holoScaleUnits(26, vec(2,23,23))}
    holoParent(26, Center)
    
    # "REAL" PROPELLER
    holoPos(27, Center:toWorld(vec(-21,0,0)))
    holoAng(27, Center:toWorld(ang(90,0,0)))
    holoMaterial(27, "phoenix_storms/airboat_blur02")
    holoModel(27, "hq_cylinder")
    #holoColor(27, vec(0,0,0))
    holoScaleUnits(27, vec(60,60,1))
    holoParent(27, holoEntity(16))
    
    # BACK WHEEL SUPPORT
    holoPos(28, Center:toWorld(vec(45,0,-8)))
    holoAng(28, Center:toWorld(ang(-20,0,0)))
    #holoMaterial(28, "bull/various/subwoofer")
    holoModel(28, "hq_cylinder")
    holoColor(28, HullSecondaryColor)
    holoScaleUnits(28, vec(4,4,10))
    holoParent(28, Center)
    
    # BACK WHEEL SUPPORT GRABBER
    holoPos(29, holoEntity(28):toWorld(vec(0.48,0,-6.4)))
    holoAng(29, holoEntity(28):toWorld(ang(-15,0,0)))
    #holoMaterial(29, "bull/various/subwoofer")
    holoModel(29, "hq_cubinder")
    holoColor(29, HullBodyColor)
    holoScaleUnits(29, vec(4,4,4))
    holoParent(29, holoEntity(28))
    
    # BACK WHEEL SUPPORT WHEEL GRAB
    holoPos(30, holoEntity(29):toWorld(vec(0,0,-3.5)))
    holoAng(30, holoEntity(29):toWorld(ang(90,0,0)))
    #holoMaterial(30, "bull/various/subwoofer")
    holoModel(30, "hq_stube")
    #holoColor(30, HullBodyColor)
    holoScaleUnits(30, vec(5,5,1))
    holoParent(30, holoEntity(28))
    
    # WHEEL BLACK
    holoPos(31, holoEntity(30):toWorld(vec(2.5,0,0)))
    holoAng(31, holoEntity(30):toWorld(ang(0,0,90)))
    #holoMaterial(31, "bull/various/subwoofer")
    holoModel(31, "hq_torus_oldsize")
    holoColor(31, vec(50,50,50))
    holoScaleUnits(31, vec(8,8,14))
    holoParent(31, holoEntity(28))
    
    # WHEEL WHITE
    holoPos(32, holoEntity(30):toWorld(vec(2.5,0,0)))
    holoAng(32, holoEntity(30):toWorld(ang(0,0,90)))
    #holoMaterial(32, "bull/various/subwoofer")
    holoModel(32, "hq_cylinder")
    holoColor(32, vec(200,200,200))
    holoScaleUnits(32, vec(6,6,3.1))
    holoParent(32, holoEntity(28))
    ###################################################################
    # RIGHT WHEEL SUPPORT
    holoPos(33, Center:toWorld(vec(2,10,-12)))
    holoAng(33, Center:toWorld(ang(20,3,12)))
    #holoMaterial(33, "bull/various/subwoofer")
    holoModel(33, "hq_cylinder")
    holoColor(33, HullSecondaryColor)
    holoScaleUnits(33, vec(4,4,10))
    holoParent(33, Center)
    
    # RIGHT WHEEL SUPPORT GRABBER
    holoPos(34, holoEntity(33):toWorld(vec(0.48,0,-6.4)))
    holoAng(34, holoEntity(33):toWorld(ang(-15,0,0)))
    #holoMaterial(34, "bull/various/subwoofer")
    holoModel(34, "hq_cubinder")
    holoColor(34, HullBodyColor)
    holoScaleUnits(34, vec(4,4,4))
    holoParent(34, holoEntity(33))
    
    # RIGHT WHEEL SUPPORT WHEEL GRAB
    holoPos(35, holoEntity(34):toWorld(vec(0,0,-3.5)))
    holoAng(35, holoEntity(34):toWorld(ang(90,0,0)))
    #holoMaterial(35, "bull/various/subwoofer")
    holoModel(35, "hq_stube")
    #holoColor(35, vec(0,0,0))
    holoScaleUnits(35, vec(5,5,1))
    holoParent(35, holoEntity(33))
    
    # WHEEL BLACK - RIGHT WHEEL
    holoPos(36, holoEntity(35):toWorld(vec(2.5,0,0)))
    holoAng(36, holoEntity(35):toWorld(ang(0,0,90)))
    #holoMaterial(36, "bull/various/subwoofer")
    holoModel(36, "hq_torus_oldsize")
    holoColor(36, vec(50,50,50))
    holoScaleUnits(36, vec(8,8,14))
    holoParent(36, holoEntity(33))
    
    # WHEEL WHITE - RIGHT WHEEL
    holoPos(37, holoEntity(35):toWorld(vec(2.5,0,0)))
    holoAng(37, holoEntity(35):toWorld(ang(0,0,90)))
    #holoMaterial(37, "bull/various/subwoofer")
    holoModel(37, "hq_cylinder")
    holoColor(37, vec(200,200,200))
    holoScaleUnits(37, vec(6,6,3.1))
    holoParent(37, holoEntity(33))
    
    ###################
    
    # LEFT WHEEL SUPPORT
    holoPos(38, Center:toWorld(vec(2,-10,-12)))
    holoAng(38, Center:toWorld(ang(20,-3,-12)))
    #holoMaterial(38, "bull/various/subwoofer")
    holoModel(38, "hq_cylinder")
    holoColor(38, HullSecondaryColor)
    holoScaleUnits(38, vec(4,4,10))
    holoParent(38, Center)
    
    # LEFT WHEEL SUPPORT GRABBER
    holoPos(39, holoEntity(38):toWorld(vec(0.48,0,-6.4)))
    holoAng(39, holoEntity(38):toWorld(ang(-15,0,0)))
    #holoMaterial(39, "bull/various/subwoofer")
    holoModel(39, "hq_cubinder")
    holoColor(39, HullBodyColor)
    holoScaleUnits(39, vec(4,4,4))
    holoParent(39, holoEntity(38))
    
    # LEFT WHEEL SUPPORT WHEEL GRAB
    holoPos(40, holoEntity(39):toWorld(vec(0,0,-3.5)))
    holoAng(40, holoEntity(39):toWorld(ang(90,0,0)))
    #holoMaterial(40, "bull/various/subwoofer")
    holoModel(40, "hq_stube")
    #holoColor(40, vec(0,0,0))
    holoScaleUnits(40, vec(5,5,1))
    holoParent(40, holoEntity(38))
    
    # WHEEL BLACK - LEFT WHEEL
    holoPos(41, holoEntity(40):toWorld(vec(2.5,0,0)))
    holoAng(41, holoEntity(40):toWorld(ang(0,0,90)))
    #holoMaterial(41, "bull/various/subwoofer")
    holoModel(41, "hq_torus_oldsize")
    holoColor(41, vec(50,50,50))
    holoScaleUnits(41, vec(8,8,14))
    holoParent(41, holoEntity(38))
    
    # WHEEL WHITE - LEFT WHEEL
    holoPos(42, holoEntity(40):toWorld(vec(2.5,0,0)))
    holoAng(42, holoEntity(40):toWorld(ang(0,0,90)))
    #holoMaterial(42, "bull/various/subwoofer")
    holoModel(42, "hq_cylinder")
    holoColor(42, vec(200,200,200))
    holoScaleUnits(42, vec(6,6,3.1))
    holoParent(42, holoEntity(38))
    
    # RIGHT WING TRAIL HOLOGRAM
    holoPos(43, Center:toWorld(vec(35,37.2,0)))
    holoAng(43, Center:toWorld(ang(0,-10,-90)))
    #holoMaterial(43, "phoenix_storms/black_chrome")
    #holoModel(43, "hq_dome")
    #holoColor(43, HullBodyColor)
    holoScaleUnits(43, vec(0.3,1.3,0.3))
    holoParent(43, Center)
    holoAlpha(43, 0)
    
    # RIGHT WING TRAIL HOLOGRAM
    holoPos(44, Center:toWorld(vec(35,-37.2,0)))
    holoAng(44, Center:toWorld(ang(0,10,-90)))
    #holoMaterial(44, "phoenix_storms/black_chrome")
    #holoModel(44, "hq_dome")
    #holoColor(44, vec(0,0,0))
    holoScaleUnits(44, vec(0.3,1.3,0.3))
    holoParent(44, Center)
    holoAlpha(44, 0)
    
    # SPEED METER = MIDDLE DIGIT
    holoPos(45, Center:toWorld(vec(-3.6,0,16.5)))
    holoAng(45, Center:toWorld(ang(-20,0,0)))
    #holoMaterial(45, "bull/various/subwoofer")
    #holoModel(45, "hq_dome")
    holoColor(45, vec(0,200,0))
    holoScaleUnits(45, vec(0.25,1.7,1.7))
    holoParent(45, Center)
    
    # SPEED METER = LEFT DIGIT
    holoPos(46, Center:toWorld(vec(-3.6,-2.1,16.5)))
    holoAng(46, Center:toWorld(ang(-20,0,0)))
    #holoMaterial(46, "bull/various/subwoofer")
    #holoModel(46, "hq_dome")
    holoColor(46, vec(0,200,0))
    holoScaleUnits(46, vec(0.25,1.7,1.7))
    holoParent(46, Center)
    
    # SPEED METER = RIGHT DIGIT
    holoPos(47, Center:toWorld(vec(-3.6,2.1,16.5)))
    holoAng(47, Center:toWorld(ang(-20,0,0)))
    #holoMaterial(47, "bull/various/subwoofer")
    #holoModel(47, "hq_dome")
    holoColor(47, vec(0,200,0))
    holoScaleUnits(47, vec(0.25,1.7,1.7))
    holoParent(47, Center)
    
    # DECAL - EXHAUST RIGHT
    holoPos(48, Center:toWorld(vec(-10.4,13.4,-1)))
    holoAng(48, Center:toWorld(ang(-40,0,20)))
    #holoMaterial(48, "bull/various/subwoofer")
    holoModel(48, "hq_tube_thick")
    holoColor(48, HullSecondaryColor)
    holoScaleUnits(48, vec(3,3,10))
    holoParent(48, Center)
    
    # DECAL - EXHAUST RIGHT
    holoPos(49, Center:toWorld(vec(-10.4,-13.4,-1)))
    holoAng(49, Center:toWorld(ang(-40,0,-20)))
    #holoMaterial(49, "bull/various/subwoofer")
    holoModel(49, "hq_tube_thick")
    holoColor(49, HullSecondaryColor)
    holoScaleUnits(49, vec(3,3,10))
    holoParent(49, Center)
    
    ####WEAPONIZE
    if(Weaponize==1){
    # GUN BASE - RIGHT
    holoPos(50, Center:toWorld(vec(10,28.5,-2)))
    holoAng(50, Center:toWorld(ang(90,0,0)))
    #holoMaterial(50, "bull/various/subwoofer")
    holoModel(50, "hq_cylinder")
    holoColor(50, HullSecondaryColor)
    holoScaleUnits(50, vec(3,3,15))
    holoParent(50, Center)
    
    #GUN BODY - RIGHT
    holoPos(51, Center:toWorld(vec(8,28.5,-2)))
    holoAng(51, Center:toWorld(ang(90,0,0)))
    #holoMaterial(51, "bull/various/subwoofer")
    holoModel(51, "hq_tube_thick")
    holoColor(51, HullSecondaryColor)
    holoScaleUnits(51, vec(2.5,2.5,15))
    holoParent(51, Center)
    
    #GUN TIP - RIGHT
    holoPos(52, Center:toWorld(vec(7,28.5,-2)))
    holoAng(52, Center:toWorld(ang(90,0,0)))
    #holoMaterial(52, "bull/various/subwoofer")
    holoModel(52, "hq_tube_thick")
    holoColor(52, vec(123,0,41))
    holoScaleUnits(52, vec(2.2,2.2,15))
    holoParent(52, Center)
    
    #GUN BLACKHOLO - RIGHT
    holoPos(53, Center:toWorld(vec(7,28.5,-2)))
    holoAng(53, Center:toWorld(ang(90,0,0)))
    #holoMaterial(53, "bull/various/subwoofer")
    holoModel(53, "hq_cylinder")
    holoColor(53, vec(0,0,0))
    holoScaleUnits(53, vec(2,2,14))
    holoParent(53, Center)
    
    # GUN SPHERE DECAL - RIGHT
    holoPos(54, Center:toWorld(vec(2.8,28.5,-2)))
    holoAng(54, Center:toWorld(ang(90,0,0)))
    #holoMaterial(54, "bull/various/subwoofer")
    holoModel(54, "hq_sphere")
    holoColor(54, HullSecondaryColor)
    holoScaleUnits(54, vec(3,3,3))
    holoParent(54, Center)
    
    # GUN BACK-SPHERE DECAL - RIGHT
    holoPos(55, Center:toWorld(vec(17.5,28.5,-2)))
    holoAng(55, Center:toWorld(ang(90,0,0)))
    #holoMaterial(55, "bull/various/subwoofer")
    holoModel(55, "hq_sphere")
    holoColor(55, HullSecondaryColor)
    holoScaleUnits(55, vec(3,3,4))
    holoParent(55, Center)
    
    #####
    
    # GUN BASE - LEFT
    holoPos(56, Center:toWorld(vec(10,-28.5,-2)))
    holoAng(56, Center:toWorld(ang(90,0,0)))
    #holoMaterial(56, "bull/various/subwoofer")
    holoModel(56, "hq_cylinder")
    holoColor(56, HullSecondaryColor)
    holoScaleUnits(56, vec(3,3,15))
    holoParent(56, Center)
    
    #GUN BODY - LEFT
    holoPos(57, Center:toWorld(vec(8,-28.5,-2)))
    holoAng(57, Center:toWorld(ang(90,0,0)))
    #holoMaterial(57, "bull/various/subwoofer")
    holoModel(57, "hq_tube_thick")
    holoColor(57, HullSecondaryColor)
    holoScaleUnits(57, vec(2.5,2.5,15))
    holoParent(57, Center)
    
    #GUN TIP - LEFT
    holoPos(58, Center:toWorld(vec(7,-28.5,-2)))
    holoAng(58, Center:toWorld(ang(90,0,0)))
    #holoMaterial(58, "bull/various/subwoofer")
    holoModel(58, "hq_tube_thick")
    holoColor(58, vec(123,0,41))
    holoScaleUnits(58, vec(2.2,2.2,15))
    holoParent(58, Center)
    
    #GUN BLACKHOLO - LEFT
    holoPos(59, Center:toWorld(vec(7,-28.5,-2)))
    holoAng(59, Center:toWorld(ang(90,0,0)))
    #holoMaterial(59, "bull/various/subwoofer")
    holoModel(59, "hq_cylinder")
    holoColor(59, vec(0,0,0))
    holoScaleUnits(59, vec(2,2,14))
    holoParent(59, Center)
    
    # GUN SPHERE DECAL - LEFT
    holoPos(60, Center:toWorld(vec(2.8,-28.5,-2)))
    holoAng(60, Center:toWorld(ang(90,0,0)))
    #holoMaterial(60, "bull/various/subwoofer")
    holoModel(60, "hq_sphere")
    holoColor(60, HullSecondaryColor)
    holoScaleUnits(60, vec(3,3,3))
    holoParent(60, Center)
    
    # GUN BACK-SPHERE DECAL - LEFT
    holoPos(61, Center:toWorld(vec(17.5,-28.5,-2)))
    holoAng(61, Center:toWorld(ang(90,0,0)))
    #holoMaterial(61, "bull/various/subwoofer")
    holoModel(61, "hq_sphere")
    holoColor(61, HullSecondaryColor)
    holoScaleUnits(61, vec(3,3,4))
    holoParent(61, Center) 
    
    entity():propFreeze(1)
}else{for(I=50,61){
holoAlpha(I,0)
holoParent(I, entity())}}
    
    # SCREEN DOWN HULL
    holoPos(62, Center:toWorld(vec(-2,0,13)))
    holoAng(62, Center:toWorld(ang(-110,0,0)))
    #holoMaterial(62, "bull/various/subwoofer")
    holoModel(62, "hq_dome")
    holoColor(62, HullSecondaryColor)
    holoScaleUnits(62, vec(5,15,10))
    holoParent(62, Center)
        
    holoParent(0, entity())
    holoParent(16, entity())
    entity():setMass(0)
    Plane:setMass(50000)
    Plane:setAlpha(0)
    entity():setAlpha(0)
    
    
    if(Weaponize==1){
    findInSphere(Plane:pos(), 50)
    findClipToClass("gmod_wire_turret")
    Turrets=findToArray()
    for(I=1,Turrets:count()){
        if(Turrets[I, entity]:owner()==owner() & Turrets[I, entity]:type()=="gmod_wire_turret"&!Turret1){Turret1=Turrets[I, entity]}
        if(Turrets[I, entity]:owner()==owner() & Turrets[I, entity]:type()=="gmod_wire_turret"&Turret1){Turret2=Turrets[I, entity]}
        }
    
        #Turret1:setColor(randvec(1,255))
        #Turret2:setColor(randvec(1,255))
        
        Turret1:setPos(Center:toWorld(vec(10,30,-4)))
        Turret1:setAng(Center:toWorld(ang(2,-179,0)))
        
        Turret2:setPos(Center:toWorld(vec(10,-27,-4)))
        Turret2:setAng(Center:toWorld(ang(2,-181,0)))
        
        Turret1:setAlpha(0)
        Turret2:setAlpha(0)
        
        Turret1:setMass(0)
        Turret2:setMass(0)
        
        
        
}
    rangerFilter(entity())
    rangerFilter(Plane)
    rangerFilter(players())
    rangerPersist(1)
    WheelsSwitch=0
    Plane:soundPlay(1,0,"/vehicles/airboat/fan_blade_idle_loop1.wav")
    holoEntity(0):soundPlay(2,0,"vehicles/airboat/fan_motor_fullthrottle_loop1.wav")
    
    
}
if(Weaponize==1){
ParentTimer++
if(ParentTimer>20 & Parented==0){
    Turret1:parentTo(entity())
    Turret2:parentTo(entity())
    entity():propFreeze(0)
    Parented=1
}}
if(WheelsSwitch==1){WheelsSwitchTimer+=0.7}else{WheelsSwitchTimer-=0.7}
if(WheelsSwitchTimer>=60){WheelsSwitchTimer=60}
if(WheelsSwitchTimer<=0){WheelsSwitchTimer=0}

    #BACK
    holoPos(28, Center:toWorld(vec(45,0,-8)))
    holoAng(28, Center:toWorld(ang(-20-sin(WheelsSwitchTimer*2)*70,0,0)))
    #LEFT
    holoPos(33, Center:toWorld(vec(2,10,-12)))
    holoAng(33, Center:toWorld(ang(20-sin(WheelsSwitchTimer*2)*100,3,12-sin(WheelsSwitchTimer)*20)))
    #RIGHT
    holoPos(38, Center:toWorld(vec(2,-10,-12)))
    holoAng(38, Center:toWorld(ang(20-sin(WheelsSwitchTimer*2)*100,-3,-12+sin(WheelsSwitchTimer)*20)))

if(Player&GroundRangerD>120){WheelsSwitch=1}
if(Player&GroundRangerD<120&PropellerSpeed<300){WheelsSwitch=0}

##############################
#STAYING ON GROUND

PlanePos = Plane:pos()
PlaneAng = Plane:angles()
PlaneAngNorm = angnorm(-Plane:angles()+ang(0,0,0))

PlaneVecVel = Plane:vel()
PlaneAngVel = Plane:angVel()

Pitch = PlaneAng:pitch()
Yaw = PlaneAng:yaw()
Roll = PlaneAng:roll()

# GROUND DETECTION
GroundRanger=rangerOffset(99999, Plane:pos(), vec(0,0,-1))
GroundRangerD = GroundRanger:distance()

# GRAVITY SETTING IF PLAYER IS IN THE CHAIR
if(!Player){
    Plane:applyForce(vec(0,0,1*6.0131)*Plane:mass())
}
if(Player&PropellerSpeed>500){
    Plane:applyForce(vec(0,0,1*9.0131)*Plane:mass())
}

# LANDING ANGLES+POS + STANDBY
# HIGH HEIGHT

if(!Player&GroundRangerD<99999&GroundRanger:hit()&GroundRangerD>400){
    Plane:applyAngForce(ang(25+Roll+$Roll*5,0,-(Pitch+$Pitch*5))*Plane:mass()/15)
    Plane:applyForce(Plane:forward()*Plane:mass()*2)
    #hint("dzia", 1)
}
# NEAR LAND
if(!Player&GroundRangerD>100&GroundRanger:hit()&GroundRangerD<400){
    Plane:applyAngForce(ang(15+Roll+$Roll*5,0,-(Pitch+$Pitch*5))*Plane:mass()/2)
    Plane:applyForce(Plane:forward()*Plane:mass()*2)
    WheelsSwitch=0
}
# LAND
if(!Player&GroundRangerD<40&GroundRanger:hit()){
    Plane:applyAngForce(ang(-6+Roll+$Roll*5,0,-(Pitch+$Pitch*5))*Plane:mass())
    Plane:applyForce(vec(0,0,19.5-(GroundRangerD+$GroundRangerD*8))*Plane:mass())
    Plane:applyForce(-(PlaneVecVel)*Plane:mass()/30)
    Plane:applyAngForce(-PlaneAngVel*Plane:mass()/30)
    WheelsSwitch=0
}
# LAND, IMMOBILE - NOT ENOUGH SPEED
if(Player&PropellerSpeed<100&GroundRangerD<40&GroundRanger:hit()){
    Plane:applyAngForce(ang(-25+Roll+$Roll*5,0,-(Pitch+$Pitch*5))*Plane:mass()) # CHANGED -6 TO -30 IN ROLL
    Plane:applyForce(vec(0,0,19.5-(GroundRangerD+$GroundRangerD*8))*Plane:mass()*2) # multiplied apply force *2 instead of *1
    Plane:applyForce(-(PlaneVecVel)*Plane:mass()/30)
    Plane:applyAngForce(-PlaneAngVel*Plane:mass()/30)
    WheelsSwitch=0
}

#print(4, (vec(0,0,19.5-(GroundRangerD+$GroundRangerD*8))*Plane:mass()*2):toString())



# SPEED MOD AND PROPPELER CONTROLL
if(!Player){PropellerSpeed-=13}
if(Player&W){PropellerSpeed+=7}
if(Player&S){PropellerSpeed-=15}


if(PropellerSpeed<=0){PropellerSpeed=0}
if(PropellerSpeed>=4000){PropellerSpeed=4000}
Rotation += clamp(PropellerSpeed/20, 0,200)
holoAng(16, Center:toWorld(ang(0,0,-Rotation)))

#ALPHING
if(PropellerSpeed<999){
    holoAlpha(14, 255)
    holoAlpha(15, 255)
    holoAlpha(27, 0)
    }else{
    holoAlpha(14, 0)
    holoAlpha(15, 0)
    holoAlpha(27, 255)
}



SpeedDisp=round(abs(Plane:velL():y())/3)


# IF ONLY ONE DIGIT - 46 is left 45 mid and 47 is right
if(Player){
if(!SpeedDisp:toString():index(2)){
    holoEntity(45):setMaterial("models/cheeze/buttons2/0")
    holoEntity(46):setMaterial("models/cheeze/buttons2/0")
    holoEntity(47):setMaterial("models/cheeze/buttons2/"+(0+SpeedDisp:toString():index(1):toNumber()):toString())
    }
# IF TWO
if(!SpeedDisp:toString():index(3)&SpeedDisp:toString():index(2)){
    holoEntity(45):setMaterial("models/cheeze/buttons2/"+(0+SpeedDisp:toString():index(1):toNumber()):toString())
    holoEntity(46):setMaterial("models/cheeze/buttons2/0")
    holoEntity(47):setMaterial("models/cheeze/buttons2/"+(0+SpeedDisp:toString():index(2):toNumber()):toString())
    }
# IF THREE
if(SpeedDisp:toString():index(3)){
    holoEntity(45):setMaterial("models/cheeze/buttons2/"+(0+SpeedDisp:toString():index(2):toNumber()):toString())
    holoEntity(46):setMaterial("models/cheeze/buttons2/"+(0+SpeedDisp:toString():index(1):toNumber()):toString())
    holoEntity(47):setMaterial("models/cheeze/buttons2/"+(0+SpeedDisp:toString():index(3):toNumber()):toString())
    }
}else{
holoEntity(45):setMaterial("models/cheeze/buttons2/minus")
holoEntity(46):setMaterial("models/cheeze/buttons2/minus")
holoEntity(47):setMaterial("models/cheeze/buttons2/minus")
}
# STEERING
# TAKE OFF
if(Player){
if(Player&PropellerSpeed>100&PropellerSpeed<1500&GroundRangerD<40&GroundRanger:hit()){
    Plane:applyAngForce(ang(-25+Roll+$Roll*5,0,-(Pitch+$Pitch*5))*Plane:mass()) # CAHNGED -6 TO -35 IN ROLL 
    Plane:applyForce(clamp((vec(0,0,19.5-(GroundRangerD+$GroundRangerD*8))*Plane:mass()*2),vec(0),vec(500000))) # multiplied mass *2 instead of *1 [THAT SHIT CAUSES "JUMP BUG" ]
    Plane:applyForce(-(PlaneVecVel)*Plane:mass()/30)
    Plane:applyAngForce(-PlaneAngVel*Plane:mass()/30)
    WheelsSwitch=0
    Plane:applyForce(Plane:forward()*Plane:mass()*(PropellerSpeed/100))
}

# GENERAL FORWARD FLIGHT
if(Player&PropellerSpeed>100){
    Plane:applyForce(Plane:forward()*Plane:mass()*(PropellerSpeed/60)*Nitro)
    Plane:applyForce(-(PlaneVecVel)*Plane:mass()/25) # VEL NULLIFICATION
}
#if(Player&PropellerSpeed<1000){Plane:applyForce(-(PlaneVecVel)*Plane:mass()/25)}

# NITRO
if(PropellerSpeed>3000&Shift){Nitro+=0.03}else{Nitro-=0.06}
if(Nitro<=1){Nitro=1}
if(Nitro>=1.7){Nitro=1.7}
}

#FLIGHT 
# CREDITS - RAVEO'S INSTANT AIRPLANE CODE YawDot PitchDot
if(Player){
if(GroundRangerD>40){
YawDot=vec(Player:eye():dot(Plane:right()),Player:eye():dot(Plane:right()),0):y()
}else{
YawDot=vec(Player:eye():dot(Plane:right()),Player:eye():dot(Plane:right()),0):y()/2
}

if(PropellerSpeed>1000){
PitchDot=vec(0,Player:eye():dot(Plane:up()),Player:eye():dot(Plane:up())):z()
}else{
PitchDot=0}

#ROTATION
if(!M2){
    Plane:applyAngForce(ang(-PitchDot*1.5,-YawDot,YawDot*10)*Plane:mass()*100)
    YawDotTemp = YawDot
    PitchDotTemp = PitchDot
    
}else{Plane:applyAngForce(ang(-PitchDotTemp*1.5,-YawDotTemp,YawDotTemp*10)*Plane:mass()*100)}

Plane:applyAngForce(ang(PlaneAngVel:roll(),-PlaneAngVel:yaw(),-PlaneAngVel:pitch())*Plane:mass())
Plane:applyAngForce(ang(Roll*1,0,-Pitch*10)*Plane:mass()*3)
}

# TRAILING
if(SpeedDisp>300){TrailMode=1}
if(SpeedDisp<200){TrailMode=0}
if(changed(TrailMode)&TrailMode==1){
    holoEntity(43):setTrails(20,40,1,"trails/laser",vec(255,255,255),255)
    holoEntity(44):setTrails(20,40,1,"trails/laser",vec(255,255,255),255)
}
if(changed(TrailMode)&TrailMode==0){
    holoEntity(43):removeTrails()
    holoEntity(44):removeTrails()
    }

# SOUNDS
# SOUND IS PREMADE IN IF FIRST
soundPitch(1, (PropellerSpeed/8))
soundVolume(1,0.5*Player:isPlayer())

soundVolume(2, (Nitro-1.3)*Player:isPlayer())
soundPitch(2, 150)

# FRONT RANGER - ANTI-WALLCRASHING
rangerFilter(players())
FrontRanger1=rangerOffset(clamp(40+SpeedDisp/3,40,200), vec(0,0,6)+Plane:pos(), Plane:forward()+Plane:right()/3)
rangerFilter(players())
FrontRanger2=rangerOffset(clamp(40+SpeedDisp/3,40,200), vec(0,0,6)+Plane:pos(), Plane:forward()-Plane:right()/3)

# WALL AVOID APPLYFORCE
if(WallAvoid&Player){
if(PropellerSpeed>200){
Plane:applyAngForce(ang(0,(FrontRanger2:distance()-FrontRanger1:distance())*4,0)*Plane:mass()*Nitro)
}
# STUCK-BOUNCE(This once unstucks the plane in case its.... stuck)
if(FrontRanger1:distance()<18|FrontRanger2:distance()<18){
    Plane:applyForce((Plane:pos()-FrontRanger1:pos())*Plane:mass()*100)
    Plane:applyAngForce(ang(0,1000,0)*Plane:mass())
    PropellerSpeed=PropellerSpeed-(PropellerSpeed/4)
    }
}

# DRIVER NOTICE
if(Plane:driver()){IsInSeat=1}
else{IsInSeat=0}
if(IsInSeat&$IsInSeat){
    Plane:hintDriver("W: Accelerate",9)
    Plane:hintDriver("S: Decelerate",9)
    if(Turret1){Plane:hintDriver("Mouse1: Shoot", 9)}
    Plane:hintDriver("Mouse2: Look around", 9)
    Plane:hintDriver("Shift: NITRO(works only on high speeds)",9)
}
# RE-ALPHING SEATON DELETE
if(last()){Plane:setAlpha(255) holoDeleteAll()}

# SHOOTING
if(first()){
Fire1=Turret1:wirelink()
Fire2=Turret2:wirelink()
}
if(M1&Turret1){ 
    Fire1["Fire",number]=1  
    Fire2["Fire",number]=1 
    timer("FireSound", 100)
    if(clk("FireSound")){
        holoEntity(16):soundPlay(5, soundDuration("weapons/ar2/fire1.wav"), "weapons/ar2/fire1.wav")
        soundVolume(5, 0.5)
        soundPitch(5, 200)
        }
    
    
    }else{ Fire1["Fire",number]=0 Fire2["Fire",number]=0 }

#if(owner():steamID()!="STEAM_0:0:18823712"){selfDestructAll()}
#print(4, ops():toString())
