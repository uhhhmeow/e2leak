@name miscConnect4MP
@inputs 
@outputs Test TestTimer TestTimer2 VecLocal:vector Mode T1 T2 T3 X Y ID Vec:vector Victory
@persist Width Height ScrHeight [ColorBlack ColorGrey ColorBlue ColorRed ColorYellow ColorGreyDark ColorWhite]:vector Player:entity

Player=owner()



if(first()){
    Mode=7
    # SCREEN SIZE
    Width = 142
    Height = 95
    
    # screen height
    ScrHeight = 60
    
    ColorBlack = vec(0)
    ColorGrey = vec(220)
    ColorGreyDark=vec(180)
    ColorBlue = vec(0,101,176)
    ColorRed = vec(255,0,0)
    ColorYellow = vec(255,255,0)
    ColorWhite = vec(255)
    
    runOnTick(1)
    
    
    
    
    
    
    
    
    
    
    
    for(I=1,60){
        holoCreate(I)
        holoPos(I, entity():boxCenterW()+vec(0,0,120)) # delete later
        holoColor(I, randvec(40,255))
        #holoDisableShading(I, 1)
    }
    
    #Point-Line Intersection holo(screen)
    holoPos(1, entity():pos() + vec(0,0,ScrHeight))
    holoAng(1, ang(0,(entity():pos()-owner():pos()):toAngle():yaw()+180,0))
    holoScaleUnits(1, vec(1,3,Height))
    holoMaterial(1, "debug/debugdrawflat")
    #holoModel(1, "")
    holoColor(1, ColorGrey)
    holoParent(1, entity())
    
    # LEFT SCREEN
    holoPos(2, holoEntity(1):toWorld(vec(0,55,0)))
    holoAng(2, holoEntity(1):toWorld(ang(0,0,0)))
    holoScaleUnits(2, vec(1,150,Height))
    holoMaterial(2, "debug/debugdrawflat")
    #holoModel(2, "")
    holoColor(2, ColorGrey)
    holoParent(2, entity())
    holoClipEnabled(2,1,1)
    holoClip(2, holoEntity(1):toWorld(vec(0,0,0)), holoEntity(1):right(), 1)
    
    # LEFT SCREEN HULL
    holoPos(3, holoEntity(1):toWorld(vec(-0.2,55,0)))
    holoAng(3, holoEntity(1):toWorld(ang(0,0,0)))
    holoScaleUnits(3, vec(1,151.5,Height+1.5))
    holoMaterial(3, "phoenix_storms/black_chrome")
    #holoModel(3, "")
    holoColor(3, vec(255,255,255))
    holoParent(3, entity())
    holoClipEnabled(3,1,1)
    holoClip(3, holoEntity(1):toWorld(vec(0,2,0)), holoEntity(1):right(), 1)
    
    # LEFT SCREEN
    holoPos(4, holoEntity(1):toWorld(vec(0,-55,0)))
    holoAng(4, holoEntity(1):toWorld(ang(0,0,0)))
    holoScaleUnits(4, vec(1,150,Height))
    holoMaterial(4, "debug/debugdrawflat")
    #holoModel(4, "")
    holoColor(4, ColorGrey)
    holoParent(4, entity())
    holoClipEnabled(4,1,1)
    holoClip(4, entity():pos() + vec(0,0,ScrHeight), -holoEntity(1):right(), 1)
    
    # LEFT SCREEN HULL
    holoPos(5, holoEntity(1):toWorld(vec(-0.2,-55,0)))
    holoAng(5, holoEntity(1):toWorld(ang(0,0,0)))
    holoScaleUnits(5, vec(1,151.5,Height+1.5))
    holoMaterial(5, "phoenix_storms/black_chrome")
    #holoModel(5, "")
    holoColor(5, vec(255,255,255))
    holoParent(5, entity())
    holoClipEnabled(5,1,1)
    holoClip(5, holoEntity(1):toWorld(vec(0,-2,0)), -holoEntity(1):right(), 1)
    
    ###
    # ICON
    
    # CYLINDER 1
    holoPos(6, holoEntity(1):toWorld(vec(0.1,-6,30)))
    holoAng(6, holoEntity(1):toWorld(ang(90,0,0)))
    holoScaleUnits(6, vec(12,12,1))
    holoMaterial(6, "debug/debugdrawflat")
    holoModel(6, "hq_cylinder")
    holoColor(6, ColorBlue)
    holoParent(6, entity())
    
    # CYLINDER 2
    holoPos(7, holoEntity(1):toWorld(vec(0.1,6,30)))
    holoAng(7, holoEntity(1):toWorld(ang(90,0,0)))
    holoScaleUnits(7, vec(12,12,1))
    holoMaterial(7, "debug/debugdrawflat")
    holoModel(7, "hq_cylinder")
    holoColor(7, ColorBlue)
    holoParent(7, entity())
    
    # CYLINDER 3
    holoPos(8, holoEntity(1):toWorld(vec(0.1,-6,18)))
    holoAng(8, holoEntity(1):toWorld(ang(90,0,0)))
    holoScaleUnits(8, vec(12,12,1))
    holoMaterial(8, "debug/debugdrawflat")
    holoModel(8, "hq_cylinder")
    holoColor(8, ColorBlue)
    holoParent(8, entity())
    
    # CYLINDER 4
    holoPos(9, holoEntity(1):toWorld(vec(0.1,6,18)))
    holoAng(9, holoEntity(1):toWorld(ang(90,0,0)))
    holoScaleUnits(9, vec(12,12,1))
    holoMaterial(9, "debug/debugdrawflat")
    holoModel(9, "hq_cylinder")
    holoColor(9, ColorBlue)
    holoParent(9, entity())
    
    # BOX 1
    holoPos(10, holoEntity(1):toWorld(vec(0.1,0,24)))
    holoAng(10, holoEntity(1):toWorld(ang(0,0,0)))
    holoScaleUnits(10, vec(1,24,12))
    holoMaterial(10, "debug/debugdrawflat")
    #holoModel(10, "hq_cylinder")
    holoColor(10, ColorBlue)
    holoParent(10, entity())
    
    # BOX 2
    holoPos(11, holoEntity(1):toWorld(vec(0.1,0,24)))
    holoAng(11, holoEntity(1):toWorld(ang(0,0,0)))
    holoScaleUnits(11, vec(1,12,24))
    holoMaterial(11, "debug/debugdrawflat")
    #holoModel(11, "hq_cylinder")
    holoColor(11, ColorBlue)
    holoParent(11, entity())
    
   # TOKEN 1 OUTLINE
    holoPos(12, holoEntity(1):toWorld(vec(0.15,-6,30)))
    holoAng(12, holoEntity(1):toWorld(ang(90,0,0)))
    holoScaleUnits(12, vec(10,10,1))
    holoMaterial(12, "debug/debugdrawflat")
    holoModel(12, "hq_cylinder")
    holoColor(12, ColorBlack)
    holoParent(12, entity())
    
    # TOKEN 2 OUTLINE
    holoPos(13, holoEntity(1):toWorld(vec(0.15,6,30)))
    holoAng(13, holoEntity(1):toWorld(ang(90,0,0)))
    holoScaleUnits(13, vec(10,10,1))
    holoMaterial(13, "debug/debugdrawflat")
    holoModel(13, "hq_cylinder")
    holoColor(13, ColorBlack)
    holoParent(13, entity())
    
    # TOKEN 3 OUTLINE
    holoPos(14, holoEntity(1):toWorld(vec(0.15,-6,18)))
    holoAng(14, holoEntity(1):toWorld(ang(90,0,0)))
    holoScaleUnits(14, vec(10,10,1))
    holoMaterial(14, "debug/debugdrawflat")
    holoModel(14, "hq_cylinder")
    holoColor(14, ColorBlack)
    holoParent(14, entity())
    
    # TOKEN 4 OUTLINE
    holoPos(15, holoEntity(1):toWorld(vec(0.15,6,18)))
    holoAng(15, holoEntity(1):toWorld(ang(90,0,0)))
    holoScaleUnits(15, vec(10,10,1))
    holoMaterial(15, "debug/debugdrawflat")
    holoModel(15, "hq_cylinder")
    holoColor(15, ColorBlack)
    holoParent(15, entity()) 
    
    # TOKEN 1
    holoPos(16, holoEntity(1):toWorld(vec(0.2,-6,30)))
    holoAng(16, holoEntity(1):toWorld(ang(90,0,0)))
    holoScaleUnits(16, vec(9,9,1))
    holoMaterial(16, "debug/debugdrawflat")
    holoModel(16, "hq_cylinder")
    holoColor(16, ColorYellow)
    holoParent(16, entity())
    
    # TOKEN 2
    holoPos(17, holoEntity(1):toWorld(vec(0.2,6,30)))
    holoAng(17, holoEntity(1):toWorld(ang(90,0,0)))
    holoScaleUnits(17, vec(9,9,1))
    holoMaterial(17, "debug/debugdrawflat")
    holoModel(17, "hq_cylinder")
    holoColor(17, ColorRed)
    holoParent(17, entity())
    
    # TOKEN 3
    holoPos(18, holoEntity(1):toWorld(vec(0.2,-6,18)))
    holoAng(18, holoEntity(1):toWorld(ang(90,0,0)))
    holoScaleUnits(18, vec(9,9,1))
    holoMaterial(18, "debug/debugdrawflat")
    holoModel(18, "hq_cylinder")
    holoColor(18, ColorRed)
    holoParent(18, entity())
    
    # TOKEN 4
    holoPos(19, holoEntity(1):toWorld(vec(0.2,6,18)))
    holoAng(19, holoEntity(1):toWorld(ang(90,0,0)))
    holoScaleUnits(19, vec(9,9,1))
    holoMaterial(19, "debug/debugdrawflat")
    holoModel(19, "hq_cylinder")
    holoColor(19, ColorYellow)
    holoParent(19, entity()) 
    
    # BUTTON OUTLINE
    holoPos(20, holoEntity(1):toWorld(vec(0.1,0,0)))
    holoAng(20, holoEntity(1):toWorld(ang(0,0,0)))
    holoScaleUnits(20, vec(1,22,8))
    holoMaterial(20, "debug/debugdrawflat")
    #holoModel(20, "hq_cylinder")
    holoColor(20, ColorBlack)
    holoParent(20, entity())
    
    # BUTTON FILL
    holoPos(21, holoEntity(1):toWorld(vec(0.15,0,0)))
    holoAng(21, holoEntity(1):toWorld(ang(0,0,0)))
    holoScaleUnits(21, vec(1,21,7))
    holoMaterial(21, "debug/debugdrawflat")
    #holoModel(21, "hq_cylinder")
    holoColor(21, ColorGreyDark)
    holoParent(21, entity())
    
    # TEXT
    # MIDDLE "N" ####################################################################################
    holoPos(22, holoEntity(1):toWorld(vec(0.25,0,0)))
    holoAng(22, holoEntity(1):toWorld(ang(0,0,0)))
    holoScaleUnits(22, vec(1,0.6,1.2))
    holoMaterial(22, "debug/debugdrawflat")
    #holoModel(22, "hq_cylinder")
    holoColor(22, ColorBlack)
    holoParent(22, entity())
    
    holoPos(23, holoEntity(1):toWorld(vec(0.25,0.4,-0.58)))
    holoAng(23, holoEntity(1):toWorld(ang(0,0,0)))
    holoScaleUnits(23, vec(1,0.6,0.8))
    holoMaterial(23, "debug/debugdrawflat")
    #holoModel(23, "hq_cylinder")
    holoColor(23, ColorBlack)
    holoParent(23, entity())
    
    holoPos(24, holoEntity(1):toWorld(vec(0.25,-0.4,0.6)))
    holoAng(24, holoEntity(1):toWorld(ang(0,0,0)))
    holoScaleUnits(24, vec(1,0.6,0.8))
    holoMaterial(24, "debug/debugdrawflat")
    #holoModel(24, "hq_cylinder")
    holoColor(24, ColorBlack)
    holoParent(24, entity())
    
    holoPos(25, holoEntity(1):toWorld(vec(0.25,-0.8,0)))
    holoAng(25, holoEntity(1):toWorld(ang(0,0,0)))
    holoScaleUnits(25, vec(1,0.6,2))
    holoMaterial(25, "debug/debugdrawflat")
    #holoModel(25, "hq_cylinder")
    holoColor(25, ColorBlack)
    holoParent(25, entity())
    
    holoPos(26, holoEntity(1):toWorld(vec(0.25,0.8,0)))
    holoAng(26, holoEntity(1):toWorld(ang(0,0,0)))
    holoScaleUnits(26, vec(1,0.6,2))
    holoMaterial(26, "debug/debugdrawflat")
    #holoModel(26, "hq_cylinder")
    holoColor(26, ColorBlack)
    holoParent(26, entity())
    
    # LEFT "N" #####################################################################################
    holoPos(27, holoEntity(1):toWorld(vec(0.25,-3,0)))
    holoAng(27, holoEntity(1):toWorld(ang(0,0,0)))
    holoScaleUnits(27, vec(1,0.6,1.2))
    holoMaterial(27, "debug/debugdrawflat")
    #holoModel(27, "hq_cylinder")
    holoColor(27, ColorBlack)
    holoParent(27, entity())
    
    holoPos(28, holoEntity(1):toWorld(vec(0.25,-2.6,-0.57)))
    holoAng(28, holoEntity(1):toWorld(ang(0,0,0)))
    holoScaleUnits(28, vec(1,0.6,0.8))
    holoMaterial(28, "debug/debugdrawflat")
    #holoModel(28, "hq_cylinder")
    holoColor(28, ColorBlack)
    holoParent(28, entity())
    
    holoPos(29, holoEntity(1):toWorld(vec(0.25,-3.4,0.6)))
    holoAng(29, holoEntity(1):toWorld(ang(0,0,0)))
    holoScaleUnits(29, vec(1,0.6,0.8))
    holoMaterial(29, "debug/debugdrawflat")
    #holoModel(29, "hq_cylinder")
    holoColor(29, ColorBlack)
    holoParent(29, entity())
    
    holoPos(30, holoEntity(1):toWorld(vec(0.25,-3.8,0)))
    holoAng(30, holoEntity(1):toWorld(ang(0,0,0)))
    holoScaleUnits(30, vec(1,0.6,2))
    holoMaterial(30, "debug/debugdrawflat")
    #holoModel(30, "hq_cylinder")
    holoColor(30, ColorBlack)
    holoParent(30, entity())
    
    holoPos(31, holoEntity(1):toWorld(vec(0.25,-2.2,0)))
    holoAng(31, holoEntity(1):toWorld(ang(0,0,0)))
    holoScaleUnits(31, vec(1,0.6,2))
    holoMaterial(31, "debug/debugdrawflat")
    #holoModel(31, "hq_cylinder")
    holoColor(31, ColorBlack)
    holoParent(31, entity())
    
# RIGHT "E"
    holoPos(32, holoEntity(1):toWorld(vec(0.25,2.2,0)))
    holoAng(32, holoEntity(1):toWorld(ang(0,0,0)))
    holoScaleUnits(32, vec(1,0.6,2))
    holoMaterial(32, "debug/debugdrawflat")
    #holoModel(32, "hq_cylinder")
    holoColor(32, ColorBlack)
    holoParent(32, entity())
    
    holoPos(33, holoEntity(1):toWorld(vec(0.25,2.7,0)))
    holoAng(33, holoEntity(1):toWorld(ang(0,0,0)))
    holoScaleUnits(33, vec(1,1.5,0.6))
    holoMaterial(33, "debug/debugdrawflat")
    #holoModel(33, "hq_cylinder")
    holoColor(33, ColorBlack)
    holoParent(33, entity())
    
    holoPos(34, holoEntity(1):toWorld(vec(0.25,2.7,0.72)))
    holoAng(34, holoEntity(1):toWorld(ang(0,0,0)))
    holoScaleUnits(34, vec(1,1.5,0.6))
    holoMaterial(34, "debug/debugdrawflat")
    #holoModel(34, "hq_cylinder")
    holoColor(34, ColorBlack)
    holoParent(34, entity())
    
    holoPos(35, holoEntity(1):toWorld(vec(0.25,2.7,-0.7)))
    holoAng(35, holoEntity(1):toWorld(ang(0,0,0)))
    holoScaleUnits(35, vec(1,1.5,0.6))
    holoMaterial(35, "debug/debugdrawflat")
    #holoModel(35, "hq_cylinder")
    holoColor(35, ColorBlack)
    holoParent(35, entity())
    
    # LEFT "O"
    holoPos(36, holoEntity(1):toWorld(vec(0.25,-5.2,0)))
    holoAng(36, holoEntity(1):toWorld(ang(0,0,0)))
    holoScaleUnits(36, vec(1,0.6,2))
    holoMaterial(36, "debug/debugdrawflat")
    #holoModel(36, "hq_cylinder")
    holoColor(36, ColorBlack)
    holoParent(36, entity())
    
    holoPos(37, holoEntity(1):toWorld(vec(0.25,-6.2,0)))
    holoAng(37, holoEntity(1):toWorld(ang(0,0,0)))
    holoScaleUnits(37, vec(1,0.6,2))
    holoMaterial(37, "debug/debugdrawflat")
    #holoModel(37, "hq_cylinder")
    holoColor(37, ColorBlack)
    holoParent(37, entity())
    
    holoPos(38, holoEntity(1):toWorld(vec(0.25,-5.7,0.72)))
    holoAng(38, holoEntity(1):toWorld(ang(0,0,0)))
    holoScaleUnits(38, vec(1,1.5,0.6))
    holoMaterial(38, "debug/debugdrawflat")
    #holoModel(38, "hq_cylinder")
    holoColor(38, ColorBlack)
    holoParent(38, entity())
    
    holoPos(39, holoEntity(1):toWorld(vec(0.25,-5.7,-0.7)))
    holoAng(39, holoEntity(1):toWorld(ang(0,0,0)))
    holoScaleUnits(39, vec(1,1.5,0.6))
    holoMaterial(39, "debug/debugdrawflat")
    #holoModel(39, "hq_cylinder")
    holoColor(39, ColorBlack)
    holoParent(39, entity())
    
    # RIGHT "C"
    holoPos(40, holoEntity(1):toWorld(vec(0.25,4.6,0)))
    holoAng(40, holoEntity(1):toWorld(ang(0,0,0)))
    holoScaleUnits(40, vec(1,0.6,2))
    holoMaterial(40, "debug/debugdrawflat")
    #holoModel(40, "hq_cylinder")
    holoColor(40, ColorBlack)
    holoParent(40, entity())
    
    holoPos(41, holoEntity(1):toWorld(vec(0.25,5.1,0.72)))
    holoAng(41, holoEntity(1):toWorld(ang(0,0,0)))
    holoScaleUnits(41, vec(1,1.5,0.6))
    holoMaterial(41, "debug/debugdrawflat")
    #holoModel(41, "hq_cylinder")
    holoColor(41, ColorBlack)
    holoParent(41, entity())
    
    holoPos(42, holoEntity(1):toWorld(vec(0.25,5.1,-0.7)))
    holoAng(42, holoEntity(1):toWorld(ang(0,0,0)))
    holoScaleUnits(42, vec(1,1.5,0.6))
    holoMaterial(42, "debug/debugdrawflat")
    #holoModel(42, "hq_cylinder")
    holoColor(42, ColorBlack)
    holoParent(42, entity())
    
    # LEFT C
    holoPos(43, holoEntity(1):toWorld(vec(0.25,-8.6,0)))
    holoAng(43, holoEntity(1):toWorld(ang(0,0,0)))
    holoScaleUnits(43, vec(1,0.6,2))
    holoMaterial(43, "debug/debugdrawflat")
    #holoModel(43, "hq_cylinder")
    holoColor(43, ColorBlack)
    holoParent(43, entity())
    
    holoPos(44, holoEntity(1):toWorld(vec(0.25,-8.1,0.72)))
    holoAng(44, holoEntity(1):toWorld(ang(0,0,0)))
    holoScaleUnits(44, vec(1,1.5,0.6))
    holoMaterial(44, "debug/debugdrawflat")
    #holoModel(44, "hq_cylinder")
    holoColor(44, ColorBlack)
    holoParent(44, entity())
    
    holoPos(45, holoEntity(1):toWorld(vec(0.25,-8.1,-0.7)))
    holoAng(45, holoEntity(1):toWorld(ang(0,0,0)))
    holoScaleUnits(45, vec(1,1.5,0.6))
    holoMaterial(45, "debug/debugdrawflat")
    #holoModel(45, "hq_cylinder")
    holoColor(45, ColorBlack)
    holoParent(45, entity())
    
    # RIGHT T
    holoPos(46, holoEntity(1):toWorld(vec(0.25,7.6,0)))
    holoAng(46, holoEntity(1):toWorld(ang(0,0,0)))
    holoScaleUnits(46, vec(1,0.6,2))
    holoMaterial(46, "debug/debugdrawflat")
    #holoModel(46, "hq_cylinder")
    holoColor(46, ColorBlack)
    holoParent(46, entity())
    
    holoPos(47, holoEntity(1):toWorld(vec(0.25,7.6,0.72)))
    holoAng(47, holoEntity(1):toWorld(ang(0,0,0)))
    holoScaleUnits(47, vec(1,1.8,0.6))
    holoMaterial(47, "debug/debugdrawflat")
    #holoModel(47, "hq_cylinder")
    holoColor(47, ColorBlack)
    holoParent(47, entity())
    
    # COG
    holoPos(48, holoEntity(1):toWorld(vec(0.1,0,-3.5)))
    holoAng(48, holoEntity(1):toWorld(ang(0,0,0)))
    holoScaleUnits(48, vec(1,22,22))
    holoMaterial(48, "expression 2/cog_prop")
    holoColor(48, ColorBlack)
    holoParent(48, entity())
    holoClipEnabled(48, 1,1)
    holoClip(48, holoEntity(48):pos()-vec(0,0,1), -holoEntity(48):up(),1)
    holoAlpha(48,0)
}


Use = Player:keyUse()

# CONNECT BUTTON HIGHLIGHTING
if(inrange(VecLocal, vec(-1,-10.5,-3.5), vec(1,10.5,3.5)) & Mode==0){holoColor(21, ColorWhite)}elseif(Mode==0){holoColor(21, ColorGreyDark)}
if(inrange(VecLocal, vec(-1,-10.5,-3.5), vec(1,10.5,3.5)) & Mode==0 & Use){Mode=1}

# MODE 1, CONNECTING TO SERVER
if(Mode==1){
    holoAlpha(48, 255)
    holoAng(48, holoEntity(1):toWorld(ang(0,0,-curtime()*15)))
    holoColor(21, vec(60,120+abs(sin(curtime()*100)*200),60))



#################################################
#################################################
#################################################
#################################################
################## DAMIANU PISZ SWOJ SHIT TUTAJ
#################################################
#################################################
#################################################
#################################################

# if(connected){Mode=2} jesli "costam (twoj gibberish z laczeniem) to mode rowna sie 2. Narazie daje to debugowo do keyUse

}











################ MODE 2
# EXPANDING THE SCREEN
if(Mode==2){
if(TestTimer<26){TestTimer+=0.4}else{Mode=3}
TestTimer2 = sin(TestTimer*4)*50



holoPos(2, holoEntity(1):toWorld(vec(0,55-TestTimer2,0)))
holoPos(3, holoEntity(1):toWorld(vec(-0.1,55-TestTimer2,0)))
holoPos(4, holoEntity(1):toWorld(vec(0,-55+TestTimer2,0)))
holoPos(5, holoEntity(1):toWorld(vec(-0.1,-55+TestTimer2,0)))

holoColor(1, mix(ColorBlue,ColorGrey, (TestTimer/26)%26))
holoColor(2, mix(ColorBlue,ColorGrey, (TestTimer/26)%26))
holoColor(4, mix(ColorBlue,ColorGrey, (TestTimer/26)%26))
    
for(I=6,48){ holoAlpha(I, 0) holoColor(I, ColorBlack) } # POTEM ZMIENI SIE NA ColorBlue
    
}
################# MODE 3
# SPAWNING SLOTS
if(Mode==3){
    
    # DAMIANU SPAWNIJ SLOTY W HOLOGRAMACH OD 6 DO 60(42 sloty = hologramy od 6 do 48)
    for(Y=1,6,1)
    {
        for(X=1,7,1)
        {
            ID=6*(X)+(Y-1)
            holoModel(ID,"hq_cylinder")
            holoScaleUnits(ID,vec(12,12,1))
            holoParent(ID,1)
            holoPos(ID,holoEntity(1):toWorld(vec(0.1,15*(7/2)-15*X+15/2,15*(6/2)-15*Y+15/2)))
            holoAng(ID,holoEntity(1):toWorld(ang(90,0,0)))
            holoColor(ID,vec(255,255,255))
            holoAlpha(ID,255)
            holoDisableShading(ID,0)
            holoShadow(ID,0)

        }
    }
    Mode=4
    # ALPHA DAJ NA 255 ALE COLOR DAJ ColorBlue, dzieki czemu nie bedzie ich widac


}

if(Mode==4)
{
    for(X=1,7,1)
    {
        for(Y=1,6,1)
        {
            ID=6*(X)+(Y-1)
            holoColor(ID, mix(ColorBlack,ColorBlue,T3/10))
        }
    }
        # END OF LOOP
    if(T2<40){T2++}elseif(T3<10){
        T3+=0.4
        # PETLA ZWIEKSZAJACA COLOR DO ColorBlack poprzez holoColor(I, mix(ColorBlack,ColorBlue,-T3/10))
        if(T3>=10){T3=10 Mode=5}
        }   

}



# KOD GRY
if(Mode==5){
    T3=0
    
    # ALAN ADD DETAILS
    }




# VICTORY PHASE 1 - FADING SLOTS OUT
if(Mode==6){
    T3+=0.5
    if(T3>=10){T3=10 Mode=7}
    for(I=6,48){
        holoColor(I, mix(ColorBlue,ColorBlack,T3/10))}
}

# VICTORY PHASE 2 POSITIONING AND SLIDING
if(Mode==7){
    
    if(TestTimer>=0){TestTimer-=0.4}else{
        TestTimer=0 
        if(Victory){Mode=9}else{Mode=8}
        
        }
    TestTimer2 = sin(TestTimer*4)*50
    
    
    
    holoPos(2, holoEntity(1):toWorld(vec(0,55-TestTimer2,0)))
    holoPos(3, holoEntity(1):toWorld(vec(-0.1,55-TestTimer2,0)))
    holoPos(4, holoEntity(1):toWorld(vec(0,-55+TestTimer2,0)))
    holoPos(5, holoEntity(1):toWorld(vec(-0.1,-55+TestTimer2,0)))
    
    holoColor(1, mix(ColorBlue,ColorGrey, (TestTimer/26)%26))
    holoColor(2, mix(ColorBlue,ColorGrey, (TestTimer/26)%26))
    holoColor(4, mix(ColorBlue,ColorGrey, (TestTimer/26)%26))
        
    for(I=6,48){ holoAlpha(I, 0) holoColor(I, ColorBlack) } # POTEM ZMIENI SIE NA ColorBlue

    


}

# VICTORY MESSAGE
if(Mode==8){
    
    # "O"
    holoPos(10, holoEntity(1):toWorld(vec(0.1,0,43)))
    holoAng(10, holoEntity(1):toWorld(ang(0,0,0)))
    holoScaleUnits(10, vec(1,3,3))
    holoMaterial(10, "debug/debugdrawflat")
    holoModel(10, "cube")
    holoColor(10, ColorBlack)
    holoParent(10, entity())
    holoAlpha(10, 255)
    
    holoPos(11, holoEntity(1):toWorld(vec(0.1,0,25)))
    holoAng(11, holoEntity(1):toWorld(ang(0,0,0)))
    holoScaleUnits(11, vec(1,3,3))
    holoMaterial(11, "debug/debugdrawflat")
    holoModel(11, "cube")
    holoColor(11, ColorBlack)
    holoParent(11, entity())
    holoAlpha(11, 255)
    
    holoPos(12, holoEntity(1):toWorld(vec(0.1,-3,34)))
    holoAng(12, holoEntity(1):toWorld(ang(0,0,0)))
    holoScaleUnits(12, vec(1,3,21))
    holoMaterial(12, "debug/debugdrawflat")
    holoModel(12, "cube")
    holoColor(12, ColorBlack)
    holoParent(12, entity())
    holoAlpha(12, 255)
    
    holoPos(13, holoEntity(1):toWorld(vec(0.1,3,34)))
    holoAng(13, holoEntity(1):toWorld(ang(0,0,0)))
    holoScaleUnits(13, vec(1,3,21))
    holoMaterial(13, "debug/debugdrawflat")
    holoModel(13, "cube")
    holoColor(13, ColorBlack)
    holoParent(13, entity())
    holoAlpha(13, 255)
    
    # "Y"
    holoPos(14, holoEntity(1):toWorld(vec(0.1,-9,39)))
    holoAng(14, holoEntity(1):toWorld(ang(0,0,0)))
    holoScaleUnits(14, vec(1,3,11))
    holoMaterial(14, "debug/debugdrawflat")
    holoModel(14, "cube")
    holoColor(14, ColorBlack)
    holoParent(14, entity())
    holoAlpha(14, 255)
    
    holoPos(15, holoEntity(1):toWorld(vec(0.1,-15,39)))
    holoAng(15, holoEntity(1):toWorld(ang(0,0,0)))
    holoScaleUnits(15, vec(1,3,11))
    holoMaterial(15, "debug/debugdrawflat")
    holoModel(15, "cube")
    holoColor(15, ColorBlack)
    holoParent(15, entity())
    holoAlpha(15, 255)
    
    holoPos(16, holoEntity(1):toWorld(vec(0.1,-12,29)))
    holoAng(16, holoEntity(1):toWorld(ang(0,0,0)))
    holoScaleUnits(16, vec(1,3,11))
    holoMaterial(16, "debug/debugdrawflat")
    holoModel(16, "cube")
    holoColor(16, ColorBlack)
    holoParent(16, entity())
    holoAlpha(16, 255)
    
    # "U"
    holoPos(17, holoEntity(1):toWorld(vec(0.1,12,25)))
    holoAng(17, holoEntity(1):toWorld(ang(0,0,0)))
    holoScaleUnits(17, vec(1,3,3))
    holoMaterial(17, "debug/debugdrawflat")
    holoModel(17, "cube")
    holoColor(17, ColorBlack)
    holoParent(17, entity())
    holoAlpha(17, 255)
    
    holoPos(18, holoEntity(1):toWorld(vec(0.1,9,34)))
    holoAng(18, holoEntity(1):toWorld(ang(0,0,0)))
    holoScaleUnits(18, vec(1,3,21))
    holoMaterial(18, "debug/debugdrawflat")
    holoModel(18, "cube")
    holoColor(18, ColorBlack)
    holoParent(18, entity())
    holoAlpha(18, 255)
    
    holoPos(19, holoEntity(1):toWorld(vec(0.1,15,34)))
    holoAng(19, holoEntity(1):toWorld(ang(0,0,0)))
    holoScaleUnits(19, vec(1,3,21))
    holoMaterial(19, "debug/debugdrawflat")
    holoModel(19, "cube")
    holoColor(19, ColorBlack)
    holoParent(19, entity())
    holoAlpha(19, 255)  
    
    # "I"
    holoPos(20, holoEntity(1):toWorld(vec(0.2,0,15)))
    holoAng(20, holoEntity(1):toWorld(ang(0,0,0)))
    holoScaleUnits(20, vec(1,2,8))
    holoMaterial(20, "debug/debugdrawflat")
    holoModel(20, "cube")
    holoColor(20, ColorGrey)
    holoParent(20, entity())
    holoAlpha(20, 255)
    
    holoPos(21, holoEntity(1):toWorld(vec(0.1,0,15)))
    holoAng(21, holoEntity(1):toWorld(ang(0,0,0)))
    holoScaleUnits(21, vec(1,6,12))
    holoMaterial(21, "debug/debugdrawflat")
    holoModel(21, "cube")
    holoColor(21, ColorBlack)
    holoParent(21, entity())
    holoAlpha(21, 255)
    
    # "W"
    holoPos(22, holoEntity(1):toWorld(vec(0.1,-6,16)))
    holoAng(22, holoEntity(1):toWorld(ang(0,0,0)))
    holoScaleUnits(22, vec(1,2,10))
    holoMaterial(22, "debug/debugdrawflat")
    holoModel(22, "cube")
    holoColor(22, ColorBlack)
    holoParent(22, entity())
    holoAlpha(22, 255)
    
    holoPos(23, holoEntity(1):toWorld(vec(0.1,-8,10)))
    holoAng(23, holoEntity(1):toWorld(ang(0,0,0)))
    holoScaleUnits(23, vec(1,2,2))
    holoMaterial(23, "debug/debugdrawflat")
    holoModel(23, "cube")
    holoColor(23, ColorBlack)
    holoParent(23, entity())
    holoAlpha(23, 255)
    
    holoPos(24, holoEntity(1):toWorld(vec(0.1,-10,16)))
    holoAng(24, holoEntity(1):toWorld(ang(0,0,0)))
    holoScaleUnits(24, vec(1,2,10))
    holoMaterial(24, "debug/debugdrawflat")
    holoModel(24, "cube")
    holoColor(24, ColorBlack)
    holoParent(24, entity())
    holoAlpha(24, 255)
    
    holoPos(25, holoEntity(1):toWorld(vec(0.1,-12,10)))
    holoAng(25, holoEntity(1):toWorld(ang(0,0,0)))
    holoScaleUnits(25, vec(1,2,2))
    holoMaterial(25, "debug/debugdrawflat")
    holoModel(25, "cube")
    holoColor(25, ColorBlack)
    holoParent(25, entity())
    holoAlpha(25, 255)
    
    holoPos(26, holoEntity(1):toWorld(vec(0.1,-14,16)))
    holoAng(26, holoEntity(1):toWorld(ang(0,0,0)))
    holoScaleUnits(26, vec(1,2,10))
    holoMaterial(26, "debug/debugdrawflat")
    holoModel(26, "cube")
    holoColor(26, ColorBlack)
    holoParent(26, entity())
    holoAlpha(26, 255)
    
    # "N"
    holoPos(27, holoEntity(1):toWorld(vec(0.1,6,15)))
    holoAng(27, holoEntity(1):toWorld(ang(0,0,0)))
    holoScaleUnits(27, vec(1,2,12))
    holoMaterial(27, "debug/debugdrawflat")
    holoModel(27, "cube")
    holoColor(27, ColorBlack)
    holoParent(27, entity())
    holoAlpha(27, 255)
    
    holoPos(28, holoEntity(1):toWorld(vec(0.1,8,17)))
    holoAng(28, holoEntity(1):toWorld(ang(0,0,0)))
    holoScaleUnits(28, vec(1,2,4))
    holoMaterial(28, "debug/debugdrawflat")
    holoModel(28, "cube")
    holoColor(28, ColorBlack)
    holoParent(28, entity())
    holoAlpha(28, 255)
    
    holoPos(29, holoEntity(1):toWorld(vec(0.1,10,15)))
    holoAng(29, holoEntity(1):toWorld(ang(0,0,0)))
    holoScaleUnits(29, vec(1,2,4))
    holoMaterial(29, "debug/debugdrawflat")
    holoModel(29, "cube")
    holoColor(29, ColorBlack)
    holoParent(29, entity())
    holoAlpha(29, 255)
    
    holoPos(30, holoEntity(1):toWorld(vec(0.1,12,13)))
    holoAng(30, holoEntity(1):toWorld(ang(0,0,0)))
    holoScaleUnits(30, vec(1,2,4))
    holoMaterial(30, "debug/debugdrawflat")
    holoModel(30, "cube")
    holoColor(30, ColorBlack)
    holoParent(30, entity())
    holoAlpha(30, 255)
    
    holoPos(31, holoEntity(1):toWorld(vec(0.1,14,15)))
    holoAng(31, holoEntity(1):toWorld(ang(0,0,0)))
    holoScaleUnits(31, vec(1,2,12))
    holoMaterial(31, "debug/debugdrawflat")
    holoModel(31, "cube")
    holoColor(31, ColorBlack)
    holoParent(31, entity())
    holoAlpha(31, 255)
    
    # PLAY BUTTON OUTLINE
    holoPos(32, holoEntity(1):toWorld(vec(0.1,0,-7)))
    holoAng(32, holoEntity(1):toWorld(ang(0,0,0)))
    holoScaleUnits(32, vec(1,28,12))
    holoMaterial(32, "debug/debugdrawflat")
    holoModel(32, "cube")
    holoColor(32, ColorBlack)
    holoParent(32, entity())
    holoAlpha(32, 255)
    
    # PLAY BUTTON FILL
    holoPos(33, holoEntity(1):toWorld(vec(0.2,0,-7)))
    holoAng(33, holoEntity(1):toWorld(ang(0,0,0)))
    holoScaleUnits(33, vec(1,27,11))
    holoMaterial(33, "debug/debugdrawflat")
    holoModel(33, "cube")
    holoColor(33, ColorGreyDark)
    holoParent(33, entity())
    holoAlpha(33, 255)
    
    # RESET BUTTON OUTLINE
    holoPos(34, holoEntity(1):toWorld(vec(0.1,0,-24)))
    holoAng(34, holoEntity(1):toWorld(ang(0,0,0)))
    holoScaleUnits(34, vec(1,28,12))
    holoMaterial(34, "debug/debugdrawflat")
    holoModel(34, "cube")
    holoColor(34, ColorBlack)
    holoParent(34, entity())
    holoAlpha(34, 255)
    
    # RESET BUTTON FILL
    holoPos(35, holoEntity(1):toWorld(vec(0.2,0,-24)))
    holoAng(35, holoEntity(1):toWorld(ang(0,0,0)))
    holoScaleUnits(35, vec(1,27,11))
    holoMaterial(35, "debug/debugdrawflat")
    holoModel(35, "cube")
    holoColor(35, ColorGreyDark)
    holoParent(35, entity())
    holoAlpha(35, 255)
    
    # PLAY BUTTON PRISM
    holoPos(36, holoEntity(1):toWorld(vec(0.25,0,-7)))
    holoAng(36, holoEntity(1):toWorld(ang(90,90,0)))
    holoScaleUnits(36, vec(5,1,5))
    holoMaterial(36, "debug/debugdrawflat")
    holoModel(36, "prism")
    holoColor(36, vec(0,180,0))
    holoParent(36, entity())
    holoAlpha(36, 255)
    
    # RESET BUTTON BOX1
    holoPos(37, holoEntity(1):toWorld(vec(0.25,0,-24)))
    holoAng(37, holoEntity(1):toWorld(ang(0,0,45)))
    holoScaleUnits(37, vec(1,2,7))
    holoMaterial(37, "debug/debugdrawflat")
    holoModel(37, "cube")
    holoColor(37, vec(180,0,0))
    holoParent(37, entity())
    holoAlpha(37, 255)
    
    # RESET BUTTON BOX1
    holoPos(38, holoEntity(1):toWorld(vec(0.25,0,-24)))
    holoAng(38, holoEntity(1):toWorld(ang(0,0,-45)))
    holoScaleUnits(38, vec(1,2,7))
    holoMaterial(38, "debug/debugdrawflat")
    holoModel(38, "cube")
    holoColor(38, vec(180,0,0))
    holoParent(38, entity())
    holoAlpha(38, 255)
    
    Mode=10
}

# MODE 10 - RESET/PLAY AGAIN
if(Mode==10){
    
    # PLAY BUTTON HIGHLIGHT
    if(inrange(VecLocal, vec(-1,-13.6,-12.7),vec(1,13.6,-1.7))){
        holoColor(33, ColorWhite)
        if(Use){Mode=2}
        }elseif(Mode==10){holoColor(33, ColorGreyDark)}
    
    # RESET BUTTON HIGHLIGHT
    if(inrange(VecLocal, vec(-1,-13.6,-29.7),vec(1,13.6,-18.7))){holoColor(35, ColorWhite)
        if(Use){reset()}
        
        }elseif(Mode==10){holoColor(35, ColorGreyDark)}
    
    
}


















###################
# DEBUGOWY "CURSOR" HOLO
holoPos(60, Vec)
holoScaleUnits(60, vec(2))
holoModel(60, "hq_sphere")

# DEBUGOWE ZMIENIANIE MODA
#if(changed(owner():keyUse())&owner():keyUse()){Mode=7}












# POINT LINE INTERSECTION
Holo = holoEntity(1)

Origin = Player:shootPos()
OriginDirVec = Player:eye()

PlanePoint = Holo:pos() 
Normal = Holo:forward() 
LinePoint1 = Origin 
LinePoint2 = Origin+OriginDirVec 
X = (Normal:dot(PlanePoint-LinePoint1))/(Normal:dot(LinePoint2-LinePoint1))
Vec = LinePoint1+X*(LinePoint2-LinePoint1)
VecLocal = holoEntity(1):toLocal(Vec)




#print(4, VecLocal:toString())





#[










#TEST
if(changed(owner():keyUse())&owner():keyUse()){Test=1}
if(Test){
if(TestTimer<26){TestTimer+=0.4}
TestTimer2 = sin(TestTimer*4)*50



holoPos(2, holoEntity(1):toWorld(vec(0,55-TestTimer2,0)))
holoPoos(3, holoEntity(1):toWorld(vec(-0.1,55-TestTimer2,0)))
holoPos(4, holoEntity(1):toWorld(vec(0,-55+TestTimer2,0)))
holoPos(5, holoEntity(1):toWorld(vec(-0.1,-55+TestTimer2,0)))

holoColor(1, mix(ColorBlue,ColorGrey, (TestTimer/26)%26))
holoColor(2, mix(ColorBlue,ColorGrey, (TestTimer/26)%26))
holoColor(4, mix(ColorBlue,ColorGrey, (TestTimer/26)%26))


}
