# Name is optional. It displays name in popup window when looking directly at expression.
@name HoverPlateExample

@inputs
@outputs
# inputs are variables that will flow into E2, outputs are variables that we will output out of E2, we dont need those in
# this tutorial so i left it empty(although you may as well delete them altogether)

@persist Plate:entity Ranger:ranger Distance Angles:angle Pos:vector
# persist on other hand are values that dont necessarilly do any of those two above but you want them to be stored
# between executions of the code.(Every time code is executed variables that are not in inputs/outputs/persist get reset to zero)


# runOnTick(1) forces Expression2 to run its code infinitelly
runOnTick(1)

# We declare that variable we called "Plate" is entity welded to expression2(in code E2 is "entity()")
Plate = entity():isWeldedTo()

# Rangers are in a sense distance-o-meters(although they can output a lot more informations than that.)
# I'll create a ranger that will always look down on the ground and display heigh of the plate above the ground

# First though we need to filter out one ranger ray can touch during its travel to the bottom... namelly, the plate
rangerFilter(Plate)

# Now we declare that variable "Ranger" is our ranger with ray distance of 1000, start position of the ray directly inside of our plate, and direction going to the bottom of the map
Ranger = rangerOffset(1000, Plate:pos(), vec(0,0,-1))

# Making distance into a variable(its not necesary in most cases, but I like shortening the stuff we put in code later)
# in this case it's also a necessity because we need to use delta(I'll explain that one later.)
Distance = Ranger:distance()

# Applying force on the plate, but only on vector Z(up-down)
# What we are doing to that Z vector though, lets ellaborate on that.
# What I'm doing im taking a height I want a plate to be on, so lets say 60'
# and then im subtracting the distance from the ranger, which is a preety much
# a distance between ground and the plate. What this causes is that whenever 
# plate is above or below 60 units of height, the value gotten from this calculation
# will cause apply force to push it back in that direction.

# Lastly we added that alpha, that $ sign... Delta is a change of value over time,
# so whenever our plate moves abruptly then that $ value will be higher and by adding it to
# actual value we can stabilize the change making the plate be as balanced as possible.
Plate:applyForce( vec(0,0,60-(Distance+$Distance*8))*Plate:mass() )

# Additionally we are going to nullify some of the velocity so the plate will have "friction"
# and not fly away if we push it. Normally this should be done in the applyForce above so
# it wont use too much "Operations Per Second". But for this example ill do this separately.
# First let's make new variable for position of the plate
Pos = Plate:pos()

# The faster our plate goes in any direction the higher delta is
# by negating that delta we can effectivelly make our plate to stop moving
# we also need to multiply that negation by mass of our plate so applyforce
# will even have enough force to push that object, and additionally i multiplied
# that mass by 1.5 for stronger effect of slowing
Plate:applyForce(-$Pos*Plate:mass()*1.5)


# Lastly we are going to force our plate angles.
# First though, we will use delta as well, so lets make a variable for plate angles.
Angles = Plate:angles()

# now this is a little more complex so im going to break down this calculation
# What we are doing here is angles nullyfing, basically we are applying negative angles
# on the plate, so when plate is rotated lets say ni 45 degrees, then its going to push 
# the plate with 45 units BACK in other direction.

# Let's start then.
# 1. Angles + $Angles*8  (change *8 to whatever number, too little and balancing will be slower, too much and it will freak out)
# 2. negate those 2 add ( )    -( Angles + $Angles*8 )
# 3. multiply by mass of the plate so our applyForce will even have enough force applied to make the prop turn/move
#    -( Angles + $Angles*8 )*Plate:mass()
# 4. i checked and multiplying by prop mass isnt enough sadly, so we are additionally going to multiply that by 10
#    -( Angles + $Angles*8 )*Plate:mass()*10
# 5. And lastly.. we dont want to alter yaw(turning angle) so we need to force this one to 0
# we do it with setYaw() function, put everything we wrote by now in parenthesis and add that function
# ( -( Angles + $Angles*8 )*Plate:mass()*10 ):setYaw(0)
# 6. Put newly made code inside applyAngForce
# 7. ????????????
# 8. PROFIT !</dank meme>
Plate:applyAngForce( (-(Angles+$Angles*8)*Plate:mass()*10):setYaw(0) )





#[ 

There are more advanced techniques used to turn shit around, but you will have to research these by yourself, have fun coding
Cheers
~Tek






