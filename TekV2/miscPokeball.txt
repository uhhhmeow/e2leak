@name miscPokeball
@inputs 
@outputs PropIdx HoloIdx
@persist 
@trigger 

runOnTick(1)


if(changed(owner():keyUse()) & owner():keyUse()){
    propSpawnUndo(0)
    
    
    E = propSpawn("models/sprops/geometry/sphere_9.mdl", entity():toWorld(vec(0,0,40)), ang(0), 0)
    E:setAlpha(0)
    E:setMass(10000)
    
    
    holoCreate(HoloIdx)
    holoScaleUnits(HoloIdx, E:boxSize())
    holoPos(HoloIdx, E:pos())
    holoAng(HoloIdx, E:angles())
    holoModel(HoloIdx, "hq_sphere")
    holoClipEnabled(HoloIdx, 1)
    holoClip(HoloIdx,vec(0,0,0.5),vec(0,0,1),0)
    holoParent(HoloIdx, E)
    holoMaterial(HoloIdx, "debug/debugdrawflat")
    holoColor(HoloIdx,vec(255,0,0))
    HoloIdx++
    
    holoCreate(HoloIdx)
    holoScaleUnits(HoloIdx, E:boxSize())
    holoPos(HoloIdx, E:pos())
    holoAng(HoloIdx, E:angles())
    holoModel(HoloIdx, "hq_sphere")
    holoClipEnabled(HoloIdx, 1)
    holoClip(HoloIdx,vec(0,0,-0.5),vec(0,0,-1),0)
    holoColor(HoloIdx,vec(255))
    holoParent(HoloIdx, E)
    holoMaterial(HoloIdx, "debug/debugdrawflat")
    HoloIdx++
    
    holoCreate(HoloIdx)
    holoScaleUnits(HoloIdx, E:boxSize())
    holoPos(HoloIdx, E:pos())
    holoAng(HoloIdx, E:angles())
    holoModel(HoloIdx, "hq_sphere")
    holoClipEnabled(HoloIdx, 1, 1)
    holoClip(HoloIdx,1,vec(0,0,0.5),vec(0,0,-1),0)
    holoClipEnabled(HoloIdx, 2, 1)
    holoClip(HoloIdx,2,vec(0,0,-0.5),vec(0,0,1),0)
    holoColor(HoloIdx, vec(0))
    holoParent(HoloIdx, E)
    holoMaterial(HoloIdx, "debug/debugdrawflat")
    HoloIdx++
    
    holoCreate(HoloIdx)
    holoScaleUnits(HoloIdx, vec(3,3,1))
    holoPos(HoloIdx, E:toWorld(vec(4.3,0,0)))
    holoAng(HoloIdx, E:angles()+ang(90,0,0))
    holoModel(HoloIdx, "hq_cylinder")
    holoParent(HoloIdx, E)
    holoColor(HoloIdx, vec(0))
    holoMaterial(HoloIdx, "debug/debugdrawflat")
    HoloIdx++
    
    holoCreate(HoloIdx)
    holoScaleUnits(HoloIdx, vec(2,2,1))
    holoPos(HoloIdx, E:toWorld(vec(4.4,0,0)))
    holoAng(HoloIdx, E:angles()+ang(90,0,0))
    holoModel(HoloIdx, "hq_cylinder")
    holoParent(HoloIdx, E)
    holoColor(HoloIdx, vec(255))
    holoMaterial(HoloIdx, "debug/debugdrawflat")
    HoloIdx++
    
    E:propFreeze(0)
    }
