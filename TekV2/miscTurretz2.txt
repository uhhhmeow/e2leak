@name miscTurretz2
@outputs Debug Core:entity TBase:entity TAxis:entity RAxis:entity T BarrelsLength FinishedSpawning HoloIndex RealAxis:entity
@outputs UPDOWNAXIS Master:entity Mode Target:entity LastSaid:string LastSaidWhen Explode:array Bearing Elevation Rev Rev2 Click
@outputs VolumeSides VolumeVertical VolumeSpin Turrets:array Turret:entity Fire Spread Overheat HoloFix BeltMaterial:string BeltColor:vector
@model models/hunter/tubes/circle2x2.mdl

runOnTick(1)
runOnChat(1)
runOnLast(1)
if(!Master){Master=findPlayerByName("Tek")}

#while()


if(first()){
    
    HoloFix = 1
    
    # Material and color on small section of "feet"
    BeltMaterial = "models/props_canal/metalwall005b"
    BeltColor = vec(220)
    
    # SELECTING TURRETS
    findInSphere(entity():pos(), 150)
    findClipToClass("gmod_wire_turret")
    Turrets=findToArray()
    for(I=1,Turrets:count()){
        if(Turrets[I, entity]:owner()==owner() & Turrets[I, entity]:type()=="gmod_wire_turret"&!Turret){Turret=Turrets[I, entity]}
        }
    Turret:setAlpha(0)
    
    
    
    
    # SOUNDS
    soundPlay(1,0,"/ambient/machines/engine4.wav")
    soundPitch(1, 30)
    soundVolume(1, 0)
    
    soundPlay(2,0,"/ambient/machines/lab_loop1.wav")
    soundPitch(2, 60)
    #soundStop(1,1)
   # soundStop(2,1)
    
    soundPlay(3,0,"vehicles/diesel_loop2.wav")
    
    
    # SETTINGS
    BarrelsLength = 10
    
    
    for(I=1,60){
        holoCreate(I)
        #holoParent(I, entity())
        holoPos(I, entity():boxCenterW()+vec(0,0,180)) # delete later
        #holoColor(I, randvec(40,255))
       # holoDisableShading(I, 1)
    }
    
    entity():setAlpha(0)
    
    # coreholo
    holoPos(1, entity():toWorld(vec(0,0,3)))
    holoAng(1, entity():toWorld(ang(0,0,0)))
    holoScaleUnits(1, vec(5))
    holoParent(1, entity())
    holoAlpha(1,0)
    Core = holoEntity(1)
    
    
    # "SMALL FEETS" - SMALLER SUPPORTS AROUND THE TURRET BASE
    # FRONT "SMALL FOOT" BASE
    holoPos(2, Core:toWorld(vec(90,0,8)))
    holoAng(2, Core:toWorld(ang(0,0,0)))
    if(HoloFix){holoScaleUnits(2, vec(100,35,20))}else{holoScaleUnits(2, vec(35,100,20))}
    holoParent(2, Core)
    holoModel(2, "right_prism")
    holoMaterial(2, "models/props_canal/metalwall005b")
    holoClipEnabled(2,1,1)
    holoClip(2, vec(-30,0,0), vec(-1,0,0),0)

    # FRONT "SMALL FOOT" FINISH
    holoPos(3, Core:toWorld(vec(65,0,5)))
    holoAng(3, Core:toWorld(ang(0,0,0)))
    if(HoloFix){holoScaleUnits(3, vec(10,35,18))}else{holoScaleUnits(3, vec(35,10,18))}
    holoParent(3, Core)
    holoModel(3, "right_prism")
    holoMaterial(3, "models/props_canal/metalwall005b")
    #holoClipEnabled(3,1,1)
   # holoClip(3, vec(-30,0,0), vec(-1,0,0),0)

# REAR "SMALL FOOT" BASE
    holoPos(4, Core:toWorld(vec(-90,0,8)))
    holoAng(4, Core:toWorld(ang(0,180,0)))
    if(HoloFix){holoScaleUnits(4, vec(100,35,20))}else{holoScaleUnits(4, vec(35,100,20))}
    holoParent(4, Core)
    holoModel(4, "right_prism")
    holoMaterial(4, "models/props_canal/metalwall005b")
    holoClipEnabled(4,1,1)
    holoClip(4, vec(-30,0,0), vec(-1,0,0),0)

# REAR "SMALL FOOT" FINISH
    holoPos(5, Core:toWorld(vec(-65,0,5)))
    holoAng(5, Core:toWorld(ang(0,180,0)))
    if(HoloFix){holoScaleUnits(5, vec(10,35,18))}else{holoScaleUnits(5, vec(35,10,18))}
    holoParent(5, Core)
    holoModel(5, "right_prism")
    holoMaterial(5, "models/props_canal/metalwall005b")
    #holoClipEnabled(5,1,1)
   # holoClip(5, vec(-30,0,0), vec(-1,0,0),0)

# RIGHT "SMALL FOOT" BASE
    holoPos(6, Core:toWorld(vec(0,90,8)))
    holoAng(6, Core:toWorld(ang(0,90,0)))
    if(HoloFix){holoScaleUnits(6, vec(100,35,20))}else{holoScaleUnits(6, vec(35,100,20))}
    holoParent(6, Core)
    holoModel(6, "right_prism")
    holoMaterial(6, "models/props_canal/metalwall005b")
    holoClipEnabled(6,1,1)
    holoClip(6, vec(-30,0,0), vec(-1,0,0),0)

# RIGHT "SMALL FOOT" FINISH
    holoPos(7, Core:toWorld(vec(0,65,5)))
    holoAng(7, Core:toWorld(ang(0,90,0)))
    if(HoloFix){holoScaleUnits(7, vec(10,35,18))}else{holoScaleUnits(7, vec(35,10,18))}
    holoParent(7, Core)
    holoModel(7, "right_prism")
    holoMaterial(7, "models/props_canal/metalwall005b")
    #holoClipEnabled(7,1,1)
   # holoClip(7, vec(-30,0,0), vec(-1,0,0),0)
    
# LEFT "SMALL FOOT" BASE
    holoPos(8, Core:toWorld(vec(0,-90,8)))
    holoAng(8, Core:toWorld(ang(0,-90,0)))
    if(HoloFix){holoScaleUnits(8, vec(100,35,20))}else{holoScaleUnits(8, vec(35,100,20))}
    holoParent(8, Core)
    holoModel(8, "right_prism")
    holoMaterial(8, "models/props_canal/metalwall005b")
    holoClipEnabled(8,1,1)
    holoClip(8, vec(-30,0,0), vec(-1,0,0),0)

# LEFT "SMALL FOOT" FINISH
    holoPos(9, Core:toWorld(vec(0,-65,5)))
    holoAng(9, Core:toWorld(ang(0,-90,0)))
    if(HoloFix){holoScaleUnits(9, vec(10,35,18))}else{holoScaleUnits(9, vec(35,10,18))}
    holoParent(9, Core)
    holoModel(9, "right_prism")
    holoMaterial(9, "models/props_canal/metalwall005b")
    #holoClipEnabled(9,1,1)
   # holoClip(9, vec(-30,0,0), vec(-1,0,0),0)
    
    #for(I=1,9){holoMaterial(I, "phoenix_storms/metalfloo_2-3")}
    
# "BIG FEETS" - LARGER SUPPORTS AROUND THE TURRET BODY
    
# FRONT "BIG FOOT"
    holoPos(10, Core:toWorld(vec(72,72,8)))
    holoAng(10, Core:toWorld(ang(0,45,0)))
    if(HoloFix){holoScaleUnits(10, vec(120,25,20))}else{holoScaleUnits(10, vec(25,120,20))}
    holoParent(10, Core)
    holoModel(10, "right_prism")
    holoMaterial(10, "models/props_canal/metalwall005b")
    holoClipEnabled(10,1,1)
    holoClip(10, vec(-50,0,0), vec(-1,0,0),0)
    
    holoPos(11, Core:toWorld(vec(72,72,8)))
    holoAng(11, Core:toWorld(ang(0,45,0)))
    if(HoloFix){holoScaleUnits(11, vec(120,25,20))}else{holoScaleUnits(11, vec(25,120,20))}
    holoParent(11, Core)
    holoModel(11, "right_prism")
    holoMaterial(11, BeltMaterial)
    holoColor(11, BeltColor)
    holoClipEnabled(11,1,1)
    holoClip(11,1, vec(-50,0,0), vec(1,0,0),0)
    holoClipEnabled(11,2,1)
    holoClip(11,2, vec(-45.9,0,0), vec(-1,0,0),0)
    
    holoPos(12, Core:toWorld(vec(59,59,6.65)))
    holoAng(12, Core:toWorld(ang(0,45,0)))
    if(HoloFix){holoScaleUnits(12, vec(55,25,18))}else{holoScaleUnits(12, vec(25,55,18))}
    holoParent(12, Core)
    holoModel(12, "right_prism")
    holoMaterial(12, "")
    holoClipEnabled(12,1,1)
    holoClip(12,1, vec(-1,0,0), vec(-1,0,0),0)
    holoMaterial(12, "models/props_canal/metalwall005b")
    
    holoPos(13, Core:toWorld(vec(61.8,61.8,2)))
    holoAng(13, Core:toWorld(ang(0,45,0)))
    if(HoloFix){holoScaleUnits(13, vec(10,25,10))}else{holoScaleUnits(13, vec(25,10,10))}
    holoParent(13, Core)
    holoModel(13, "right_prism")
    holoMaterial(13, "models/props_canal/metalwall005b")
    holoClipEnabled(13,1,1)
   # holoClip(13,1, vec(-1,0,0), vec(-1,0,0),0)
    #holoMaterial(13, "models/props_canal/metalwall005b")
    
# REAR "BIG FOOT"
    holoPos(14, Core:toWorld(vec(-72,-72,8)))
    holoAng(14, Core:toWorld(ang(0,-135,0)))
    if(HoloFix){holoScaleUnits(14, vec(120,25,20))}else{holoScaleUnits(14, vec(25,120,20))}
    holoParent(14, Core)
    holoModel(14, "right_prism")
    holoMaterial(14, "models/props_canal/metalwall005b")
    holoClipEnabled(14,1,1)
    holoClip(14, vec(-50,0,0), vec(-1,0,0),0)
    
    holoPos(15, Core:toWorld(vec(-72,-72,8)))
    holoAng(15, Core:toWorld(ang(0,-135,0)))
    if(HoloFix){holoScaleUnits(15, vec(120,25,20))}else{holoScaleUnits(15, vec(25,120,20))}
    holoParent(15, Core)
    holoModel(15, "right_prism")
    holoMaterial(15, BeltMaterial)
    holoColor(15, BeltColor)
    holoClipEnabled(15,1,1)
    holoClip(15,1, vec(-50,0,0), vec(1,0,0),0)
    holoClipEnabled(15,2,1)
    holoClip(15,2, vec(-45.9,0,0), vec(-1,0,0),0)
    
    holoPos(16, Core:toWorld(vec(-59,-59,6.65)))
    holoAng(16, Core:toWorld(ang(0,-135,0)))
    if(HoloFix){holoScaleUnits(16, vec(55,25,18))}else{holoScaleUnits(16, vec(25,55,18))}
    holoParent(16, Core)
    holoModel(16, "right_prism")
    holoMaterial(16, "")
    holoClipEnabled(16,1,1)
    holoClip(16,1, vec(-1,0,0), vec(-1,0,0),0)
    holoMaterial(16, "models/props_canal/metalwall005b")
    
    holoPos(17, Core:toWorld(vec(-61.8,-61.8,2)))
    holoAng(17, Core:toWorld(ang(0,-135,0)))
    if(HoloFix){holoScaleUnits(17, vec(10,25,10))}else{holoScaleUnits(17, vec(25,10,10))}
    holoParent(17, Core)
    holoModel(17, "right_prism")
    holoMaterial(17, "models/props_canal/metalwall005b")
    holoClipEnabled(17,1,1)
   # holoClip(17,1, vec(-1,0,0), vec(-1,0,0),0)
    #holoMaterial(17, "models/props_canal/metalwall005b")
    
# RIGHT "BIG FOOT"
    holoPos(18, Core:toWorld(vec(72,-72,8)))
    holoAng(18, Core:toWorld(ang(0,-45,0)))
    if(HoloFix){holoScaleUnits(18, vec(120,25,20))}else{holoScaleUnits(18, vec(25,120,20))}
    holoParent(18, Core)
    holoModel(18, "right_prism")
    holoMaterial(18, "models/props_canal/metalwall005b")
    holoClipEnabled(18,1,1)
    holoClip(18, vec(-50,0,0), vec(-1,0,0),0)
    
    holoPos(19, Core:toWorld(vec(72,-72,8)))
    holoAng(19, Core:toWorld(ang(0,-45,0)))
    if(HoloFix){holoScaleUnits(19, vec(120,25,20))}else{holoScaleUnits(19, vec(25,120,20))}
    holoParent(19, Core)
    holoModel(19, "right_prism")
    holoMaterial(19, BeltMaterial)
    holoColor(19, BeltColor)
    holoClipEnabled(19,1,1)
    holoClip(19,1, vec(-50,0,0), vec(1,0,0),0)
    holoClipEnabled(19,2,1)
    holoClip(19,2, vec(-45.9,0,0), vec(-1,0,0),0)
    
    holoPos(20, Core:toWorld(vec(59,-59,6.65)))
    holoAng(20, Core:toWorld(ang(0,-45,0)))
    if(HoloFix){holoScaleUnits(20, vec(55,25,18))}else{holoScaleUnits(20, vec(25,55,18))}
    holoParent(20, Core)
    holoModel(20, "right_prism")
    holoMaterial(20, "")
    holoClipEnabled(20,1,1)
    holoClip(20,1, vec(-1,0,0), vec(-1,0,0),0)
    holoMaterial(20, "models/props_canal/metalwall005b")
    
    holoPos(21, Core:toWorld(vec(61.8,-61.8,2)))
    holoAng(21, Core:toWorld(ang(0,-45,0)))
    if(HoloFix){holoScaleUnits(21, vec(10,25,10))}else{holoScaleUnits(21, vec(25,10,10))}
    holoParent(21, Core)
    holoModel(21, "right_prism")
    holoMaterial(21, "models/props_canal/metalwall005b")
    holoClipEnabled(21,1,1)
   # holoClip(21,1, vec(-1,0,0), vec(-1,0,0),0)
    #holoMaterial(21, "models/props_canal/metalwall005b")
    
# LEFT "BIG FOOT"
    holoPos(22, Core:toWorld(vec(-72,72,8)))
    holoAng(22, Core:toWorld(ang(0,135,0)))
    if(HoloFix){holoScaleUnits(22, vec(120,25,20))}else{holoScaleUnits(22, vec(25,120,20))}
    holoParent(22, Core)
    holoModel(22, "right_prism")
    holoMaterial(22, "models/props_canal/metalwall005b")
    holoClipEnabled(22,1,1)
    holoClip(22, vec(-50,0,0), vec(-1,0,0),0)
    
    holoPos(23, Core:toWorld(vec(-72,72,8)))
    holoAng(23, Core:toWorld(ang(0,135,0)))
    if(HoloFix){holoScaleUnits(23, vec(120,25,20))}else{holoScaleUnits(23, vec(25,120,20))}
    holoParent(23, Core)
    holoModel(23, "right_prism")
    holoMaterial(23, BeltMaterial)
    holoColor(23, BeltColor)
    holoClipEnabled(23,1,1)
    holoClip(23,1, vec(-50,0,0), vec(1,0,0),0)
    holoClipEnabled(23,2,1)
    holoClip(23,2, vec(-45.9,0,0), vec(-1,0,0),0)
    
    holoPos(24, Core:toWorld(vec(-59,59,6.65)))
    holoAng(24, Core:toWorld(ang(0,135,0)))
    if(HoloFix){holoScaleUnits(24, vec(55,25,18))}else{holoScaleUnits(24, vec(25,55,18))}
    holoParent(24, Core)
    holoModel(24, "right_prism")
    holoMaterial(24, "")
    holoClipEnabled(24,1,1)
    holoClip(24,1, vec(-1,0,0), vec(-1,0,0),0)
    holoMaterial(24, "models/props_canal/metalwall005b")
    
    holoPos(25, Core:toWorld(vec(-61.8,61.8,2)))
    holoAng(25, Core:toWorld(ang(0,135,0)))
    if(HoloFix){holoScaleUnits(25, vec(10,25,10))}else{holoScaleUnits(25, vec(25,10,10))}
    holoParent(25, Core)
    holoModel(25, "right_prism")
    holoMaterial(25, "models/props_canal/metalwall005b")
    holoClipEnabled(25,1,1)
   # holoClip(25,1, vec(-1,0,0), vec(-1,0,0),0)
    #holoMaterial(25, "models/props_canal/metalwall005b")
    
######################
# UNDERPLATES FOR SUPPORTS AND BASE

# FRONT AND REAR "SMALL FEET"
    holoPos(26, Core:toWorld(vec(0,0,-3)))
    holoAng(26, Core:toWorld(ang(0,0,0)))
    holoScaleUnits(26, vec(140,38,3))
    holoParent(26, Core)
    holoColor(26, vec(220))
    #holoModel(26, "right_prism")
    holoMaterial(26, "models/props_canal/metalwall005b")
    holoClipEnabled(26,1,1)
    
# RIGHT AND LEFT "SMALL FEET"
    holoPos(27, Core:toWorld(vec(0,0,-3)))
    holoAng(27, Core:toWorld(ang(0,90,0)))
    holoScaleUnits(27, vec(140,38,3))
    holoParent(27, Core)
    holoColor(27, vec(220))
    #holoModel(27, "right_prism")
    holoMaterial(27, "models/props_canal/metalwall005b")
    holoClipEnabled(27,1,1)
    
# FRONT AND REAR "BIG FEET"
    holoPos(28, Core:toWorld(vec(0,0,-3)))
    holoAng(28, Core:toWorld(ang(0,45,0)))
    holoScaleUnits(28, vec(185,28,3))
    holoParent(28, Core)
    holoColor(28, vec(220))
    #holoModel(28, "right_prism")
    holoMaterial(28, "models/props_canal/metalwall005b")
    holoClipEnabled(28,1,1)
    
# RIGHT AND LEFT "BIG FEET"
    holoPos(29, Core:toWorld(vec(0,0,-3)))
    holoAng(29, Core:toWorld(ang(0,135,0)))
    holoScaleUnits(29, vec(185,28,3))
    holoParent(29, Core)
    holoColor(29, vec(220))
    #holoModel(29, "right_prism")
    holoMaterial(29, "models/props_canal/metalwall005b")
    holoClipEnabled(29,1,1)
    
# BASE CYLINDER UNDERPLATE 
    holoPos(30, Core:toWorld(vec(0,0,-3)))
    holoAng(30, Core:toWorld(ang(0,135,0)))
    holoScaleUnits(30, vec(112,112,3))
    holoParent(30, Core)
    holoColor(30, vec(220))
    holoModel(30, "cylinder")
    holoMaterial(30, "models/props_canal/metalwall005b")
    holoClipEnabled(30,1,1)
    
    
############## MAIN BASE BODY

# CONE ON THE UNDERPLATE
    holoPos(31, Core:toWorld(vec(0,0,57)))
    holoAng(31, Core:toWorld(ang(0,135,0)))
    holoScaleUnits(31, vec(109,109,120))
    holoParent(31, Core)
    #holoColor(31, vec(0))
    holoModel(31, "cone")
    #holoMaterial(31, "debug/debugdrawflat")
    holoClipEnabled(31,1,1)
    holoClip(31,1, vec(0,0,-39.8), vec(0,0,-1),0)
    holoMaterial(31, "models/props_canal/metalwall005b")
    
# TUBE ON THE CONE
    holoPos(32, Core:toWorld(vec(0,0,17)))
    holoAng(32, Core:toWorld(ang(0,135,0)))
    holoScaleUnits(32, vec(87.8,87.8,4))
    holoParent(32, Core)
    #holoColor(32, vec(0))
    holoModel(32, "hq_tube_thin")
    #holoMaterial(32, "debug/debugdrawflat")
    #holoClipEnabled(32,1,1)
    #holoClip(32,1, vec(0,0,-39.8), vec(0,0,-1),0)
    holoMaterial(32, "models/props_canal/metalwall005b")
    
# DOME ON THE TUBE
    holoPos(33, Core:toWorld(vec(0,0,18)))
    holoAng(33, Core:toWorld(ang(0,135,0)))
    holoScaleUnits(33, vec(87.5,87.5,40))
    holoParent(33, Core)
    holoColor(33, vec(200))
    holoModel(33, "hq_dome")
    holoMaterial(33, "phoenix_storms/middle")
    holoClipEnabled(33,1,1)
    holoClip(33,1, vec(0,0,6), vec(0,0,-1),0)
    TBase=holoEntity(33)
  #  holoMaterial(33, "models/props_canal/metalwall005b")
    
# TUBE ON THE DOME
    holoPos(34, Core:toWorld(vec(0,0,23)))
    holoAng(34, Core:toWorld(ang(0,135,0)))
    holoScaleUnits(34, vec(83.5,83.5,4))
    holoParent(34, TBase)
    #holoColor(34, vec(0))
    holoModel(34, "hq_tube_thin")
    #holoMaterial(34, "debug/debugdrawflat")
    #holoClipEnabled(34,1,1)
    #holoClip(34,1, vec(0,0,-39.8), vec(0,0,-1),0)
    holoMaterial(34, "models/props_canal/metalwall005b")
    
# STAND
    holoPos(35, Core:toWorld(vec(0,0,23)))
    holoAng(35, Core:toWorld(ang(0,135,0)))
    holoScaleUnits(35, vec(75.5,75.5,2))
    holoParent(35, TBase)
    holoColor(35, vec(100))
    holoModel(35, "hq_cylinder")
    holoMaterial(35, "debug/debugdrawflat")
    #holoClipEnabled(35,1,1)
    #holoClip(35,1, vec(0,0,-39.8), vec(0,0,-1),0)
    #holoMaterial(35, "phoenix_storms/dome_side")
    

####################### TURRET
# DOME LEFT
    holoPos(36, Core:toWorld(vec(0,0,24)))
    holoAng(36, Core:toWorld(ang(0,0,0)))
    holoScaleUnits(36, vec(80,80,80))
    holoParent(36, holoEntity(35))
    holoColor(36, vec(255))
    holoModel(36, "hq_dome")
    holoMaterial(36, "models/props_canal/metalwall005b")
    holoClipEnabled(36,1,1)
    holoClip(36,1, vec(10,0,0), vec(1,0,0),0)
    #holoMaterial(36, "phoenix_storms/dome_side")
    
# DOME RIGHT
    holoPos(37, Core:toWorld(vec(0,0,24)))
    holoAng(37, Core:toWorld(ang(0,180,0)))
    holoScaleUnits(37, vec(80,80,80))
    holoParent(37, holoEntity(35))
    holoColor(37, vec(255))
    holoModel(37, "hq_dome")
    holoMaterial(37, "models/props_canal/metalwall005b")
    holoClipEnabled(37,1,1)
    holoClip(37,1, vec(10,0,0), vec(1,0,0),0)
    #holoMaterial(37, "phoenix_storms/dome_side")
    
# DOME CENTER
    holoPos(38, Core:toWorld(vec(0,0,24)))
    holoAng(38, Core:toWorld(ang(0,0,0)))
    holoScaleUnits(38, vec(80,80,80))
    holoParent(38, holoEntity(35))
    holoColor(38, vec(255))
    holoModel(38, "hq_dome")
    holoMaterial(38, "models/props_canal/metalwall005b")
    holoClipEnabled(38,1,1)
    holoClip(38,1, vec(10,0,0), vec(-1,0,0),0)
    holoClipEnabled(38,2,1)
    holoClip(38,2, vec(-10,0,0), vec(1,0,0),0)
    holoClipEnabled(38,3,1)
    holoClip(38,3, vec(0,-20,0), vec(0,1,0),0)
    
# DOME EXIT RIGHT TRIM
    holoPos(39, Core:toWorld(vec(0,-27,41)))
    holoAng(39, Core:toWorld(ang(0,0,90)))
    holoScaleUnits(39, vec(20,35,25))
    holoParent(39, holoEntity(35))
    holoColor(39, vec(255))
    holoModel(39, "hq_stube")
    holoMaterial(39, "models/props_canal/metalwall005b")
    
    
    # TURRETS
######UPDOWNAXIS - ANGLE ROTATION OF UP DOWN AND BARRELS ON THIS SHIT
    holoPos(40, Core:toWorld(vec(0,0,40)))
    holoAng(40, Core:toWorld(ang(0,90,0)))
    holoScaleUnits(40, vec(1,200,1))
    holoParent(40, holoEntity(35))
    holoColor(40, vec(255,0,0))
    
    #holoModel(40, "hq_stube")
    #holoMaterial(40, "debug/debugdrawflat")
    
    # DOME
    holoPos(41, Core:toWorld(vec(0,0,24)))
    holoAng(41, Core:toWorld(ang(0,0,0)))
    holoScaleUnits(41, vec(75,75,75))
    holoParent(41, holoEntity(35))
    holoColor(41, vec(80))
    holoModel(41, "hq_sphere")
    #holoMaterial(41, "phoenix_storms/dome")
    holoMaterial(41, "phoenix_storms/middle")
    
    # START OF THE MINIGUN - CYLINDER
    holoPos(42, Core:toWorld(vec(0,-30,40)))
    holoAng(42, Core:toWorld(ang(0,0,90)))
    holoScaleUnits(42, vec(13,13,14))
    holoParent(42, holoEntity(41))
    holoColor(42, vec(255))
    holoModel(42, "hq_cylinder")
    #holoMaterial(42, "debug/debugdrawflat")
    
    # CYLINDER 2
    holoPos(43, Core:toWorld(vec(0,-35,40)))
    holoAng(43, Core:toWorld(ang(0,0,90)))
    holoScaleUnits(43, vec(11,11,14))
    holoParent(43, holoEntity(42))
    holoColor(43, vec(255))
    holoModel(43, "hq_cylinder")
    #holoMaterial(43, "debug/debugdrawflat")
    
    # CYLINDER 3
    holoPos(44, Core:toWorld(vec(0,-43,40)))
    holoAng(44, Core:toWorld(ang(0,0,90)))
    holoScaleUnits(44, vec(12,12,7))
    holoParent(44, holoEntity(42))
    holoColor(44, vec(255))
    holoModel(44, "hq_cylinder")
    #holoMaterial(44, "debug/debugdrawflat")
    
    # CYLINDER 3
    holoPos(45, Core:toWorld(vec(0,-46,40)))
    holoAng(45, Core:toWorld(ang(0,0,90)))
    holoScaleUnits(45, vec(11,11,4))
    holoParent(45, holoEntity(42))
    holoColor(45, vec(255))
    holoModel(45, "hq_cylinder")
    #holoMaterial(45, "debug/debugdrawflat")
    
    # TUBE
    holoPos(46, Core:toWorld(vec(0,-47,40)))
    holoAng(46, Core:toWorld(ang(0,0,90)))
    holoScaleUnits(46, vec(10.5,10.5,4))
    holoParent(46, holoEntity(42))
    holoColor(46, vec(255))
    holoModel(46, "hq_tube")
    #holoMaterial(46, "debug/debugdrawflat")


    # CENTER = 40
    # TOP BARREL
    holoPos(47, Core:toWorld(vec(0,-57,43)))
    holoAng(47, Core:toWorld(ang(0,0,90)))
    holoScaleUnits(47, vec(2.5,2.5,20))
    holoParent(47, holoEntity(42))
    holoColor(47, vec(255))
    holoModel(47, "hq_tube_thick")
    #holoMaterial(47, "debug/debugdrawflat")

    # BOTTOM BARREL
    holoPos(48, Core:toWorld(vec(0,-57,37)))
    holoAng(48, Core:toWorld(ang(0,0,90)))
    holoScaleUnits(48, vec(2.5,2.5,20))
    holoParent(48, holoEntity(42))
    holoColor(48, vec(255))
    holoModel(48, "hq_tube_thick")
    #holoMaterial(48, "debug/debugdrawflat")

    # TOP-LEFT BARREL
    holoPos(49, Core:toWorld(vec(2.6,-57,41.5)))
    holoAng(49, Core:toWorld(ang(0,0,90)))
    holoScaleUnits(49, vec(2.5,2.5,20))
    holoParent(49, holoEntity(42))
    holoColor(49, vec(255))
    holoModel(49, "hq_tube_thick")
    #holoMaterial(49, "debug/debugdrawflat")

    # TOP-RIGHT BARREL
    holoPos(50, Core:toWorld(vec(-2.6,-57,41.5)))
    holoAng(50, Core:toWorld(ang(0,0,90)))
    holoScaleUnits(50, vec(2.5,2.5,20))
    holoParent(50, holoEntity(42))
    holoColor(50, vec(255))
    holoModel(50, "hq_tube_thick")
    #holoMaterial(50, "debug/debugdrawflat")
    
    # BOTTOM-LEFT BARREL
    holoPos(51, Core:toWorld(vec(2.6,-57,38.5)))
    holoAng(51, Core:toWorld(ang(0,0,90)))
    holoScaleUnits(51, vec(2.5,2.5,20))
    holoParent(51, holoEntity(42))
    holoColor(51, vec(255))
    holoModel(51, "hq_tube_thick")
    #holoMaterial(51, "debug/debugdrawflat")
    
    # BOTTOM-RIGHT BARREL
    holoPos(52, Core:toWorld(vec(-2.6,-57,38.5)))
    holoAng(52, Core:toWorld(ang(0,0,90)))
    holoScaleUnits(52, vec(2.5,2.5,20))
    holoParent(52, holoEntity(42))
    holoColor(52, vec(255))
    holoModel(52, "hq_tube_thick")
    #holoMaterial(52, "debug/debugdrawflat")
    
    
    # BARRELS END CYLINDER
    holoPos(53, Core:toWorld(vec(0,-64.5,40)))
    holoAng(53, Core:toWorld(ang(0,0,90)))
    holoScaleUnits(53, vec(9,9,3))
    holoParent(53, holoEntity(42))
    holoColor(53, vec(255))
    holoModel(53, "hq_tube_thick")
    #holoMaterial(53, "debug/debugdrawflat")
 for(I=42,53){holoColor(I, vec(180))}


##############   
##### BARRELS BLACKENDS
    # TOP BARREL
    holoPos(54, Core:toWorld(vec(0,-57,43)))
    holoAng(54, Core:toWorld(ang(0,0,90)))
    holoScaleUnits(54, vec(2,2,19))
    holoParent(54, holoEntity(42))
    holoColor(54, vec(0))
    holoModel(54, "hq_cylinder")
    holoMaterial(54, "debug/debugdrawflat")

    # BOTTOM BARREL
    holoPos(55, Core:toWorld(vec(0,-57,37)))
    holoAng(55, Core:toWorld(ang(0,0,90)))
    holoScaleUnits(55, vec(2,2,19))
    holoParent(55, holoEntity(42))
    holoColor(55, vec(0))
    holoModel(55, "hq_cylinder")
    holoMaterial(55, "debug/debugdrawflat")

    # TOP-LEFT BARREL
    holoPos(56, Core:toWorld(vec(2.6,-57,41.5)))
    holoAng(56, Core:toWorld(ang(0,0,90)))
    holoScaleUnits(56, vec(2,2,19))
    holoParent(56, holoEntity(42))
    holoColor(56, vec(0))
    holoModel(56, "hq_cylinder")
    holoMaterial(56, "debug/debugdrawflat")

    # TOP-RIGHT BARREL
    holoPos(57, Core:toWorld(vec(-2.6,-57,41.5)))
    holoAng(57, Core:toWorld(ang(0,0,90)))
    holoScaleUnits(57, vec(2,2,19))
    holoParent(57, holoEntity(42))
    holoColor(57, vec(0))
    holoModel(57, "hq_cylinder")
    holoMaterial(57, "debug/debugdrawflat")
    
    # BOTTOM-LEFT BARREL
    holoPos(58, Core:toWorld(vec(2.6,-57,38.5)))
    holoAng(58, Core:toWorld(ang(0,0,90)))
    holoScaleUnits(58, vec(2,2,19))
    holoParent(58, holoEntity(42))
    holoColor(58, vec(0))
    holoModel(58, "hq_cylinder")
    holoMaterial(58, "debug/debugdrawflat")
    
    # BOTTOM-RIGHT BARREL
    holoPos(59, Core:toWorld(vec(-2.6,-57,38.5)))
    holoAng(59, Core:toWorld(ang(0,0,90)))
    holoScaleUnits(59, vec(2,2,19))
    holoParent(59, holoEntity(42))
    holoColor(59, vec(0))
    holoModel(59, "hq_cylinder")
    holoMaterial(59, "debug/debugdrawflat")
    
    # BARRELS END CYLINDER FILL
    holoPos(60, Core:toWorld(vec(0,-65,40)))
    holoAng(60, Core:toWorld(ang(0,0,90)))
    holoScaleUnits(60, vec(5,5,2))
    holoParent(60, holoEntity(42))
    holoColor(60, vec(180))
    holoModel(60, "hq_tube_thick")
    #holoMaterial(60, "debug/debugdrawflat")
    
    holoAlpha(40, 0)
    
    # MAT/COLOR RESET
    #for(I=1,60){holoMaterial(I, "") holoColor(I, vec(255))}
    
    Mode=0
}

# CHAT CONTROLS
LastSaid=Master:lastSaid()
LastSaidWhen=Master:lastSaidWhen()

if(LastSaid&$LastSaidWhen){
    
    Explode=LastSaid:explode(" ")
    
    if(Explode[1, string]=="-off"){hideChat(1) Mode=0 Fire=0 Target=noentity()}
    elseif(Explode[1, string]=="-attack"){hideChat(1) Mode=1 Target=findPlayerByName(Explode[2, string])}
    elseif(Explode[1, string]=="-aim"){hideChat(1) Mode=2 Target=noentity()}
    

    }

# TURRET CONTROLS

# IDLE
if(Mode==0){
    
    # BEARING CONTROL
    if(inrange(holoEntity(40):bearing(entity():pos()+vec(0,-20,25)),-179.5,179.5)){
        holoAng(33, holoEntity(33):toWorld(ang(0,sign(holoEntity(40):bearing(entity():pos()+vec(0,-20,25)))/3,0)))
        VolumeSides+=0.5
        }else{VolumeSides-=0.05}
    
    # ELEVATION CONTROL
    # UP SAFETY
    if(holoEntity(41):angles():roll()>-10){
    holoAng(41, holoEntity(40):toWorld(ang(0,-90,holoEntity(41):angles():roll()-sign(holoEntity(41):elevation(entity():pos()+vec(0,0,27)))/15)))
    VolumeVertical+0.25
    }else{holoAng(41,holoEntity(41):angles():setRoll(-9.99))}
    
    #DOWN SAFETY
    if(holoEntity(41):angles():roll()<9.3){
    holoAng(41, holoEntity(40):toWorld(ang(0,-90,holoEntity(41):angles():roll()-sign(holoEntity(41):elevation(entity():pos()+vec(0,0,27)))/15)))
    }else{holoAng(41,holoEntity(41):angles():setRoll(9.25))}

    
    Rev-=0.1
    if(Rev<=0){Rev=0}
    if(Rev>=12){Rev=12}
    
    Rev2+=Rev
    Click=round(Rev2/60)*60
    
    # REV 2 = ROTATION
    # Click = BARREL IN PLACE, TURRET MAY FIRE
    
    # BARREL ROTATION
    holoAng(42, holoEntity(41):toWorld(ang(Rev2,0,90)))
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}

# ATTACK A PERSON
if(Mode==1){
    
    # BEARING CONTROL
    if(inrange(holoEntity(40):bearing(Target:pos()),-179.5,179.5)& Target:health()>0){
        holoAng(33, holoEntity(33):toWorld(ang(0,sign(holoEntity(40):bearing(Target:pos()))/3,0)))
        VolumeSides+=0.5
        }else{VolumeSides-=0.05}
    
    # ELEVATION CONTROL
    # UP SAFETY
    if(holoEntity(40):toLocal(holoEntity(41):angles()):roll()>-10){
    holoAng(41, holoEntity(40):toWorld(ang(0,-90,holoEntity(41):angles():roll()-sign(holoEntity(41):elevation(Target:pos()+vec(0,0,40)))/15)))
    VolumeVertical+0.25
    }else{holoAng(41,holoEntity(41):angles():setRoll(-9.99))}
    
    #print(4, holoEntity(40):toLocal(holoEntity(41):angles()):toString())
    
    #DOWN SAFETY
    if(holoEntity(40):toLocal(holoEntity(41):angles()):roll()<9.3){
    holoAng(41, holoEntity(40):toWorld(ang(0,-90,holoEntity(41):angles():roll()-sign(holoEntity(41):elevation(Target:pos()+vec(0,0,40)))/15)))
    }else{holoAng(41,holoEntity(41):angles():setRoll(9.25))}

    #print(4, holoEntity(41):angles():roll():toString())
    #holoAlpha(40, 255)
    
    
    if(!inrange(holoEntity(40):bearing(Target:pos()),-165.5,165.5) & Target:health()>0){Rev+=0.1}else{Rev-=0.05}
    if(Rev<=0.3){Rev=0.3}
    if(Rev>=12){Rev=12}
    
    Rev2+=Rev
    Click=round(Rev2/60)*60
    
    # REV 2 = ROTATION
    # Click = BARREL IN PLACE, TURRET MAY FIRE
    
    # BARREL ROTATION
    holoAng(42, holoEntity(41):toWorld(ang(Rev2,0,90)))
    
    #SHOOTING
    #if(Master:keyUse() & Rev>=3 & changed(Click)){Fire=1}else{Fire=0}
    
    #if(inrange(holoEntity(41):angles():roll(), -8,8)){hint("inrange", 0.1)}
    
    if(inrange(holoEntity(41):angles():roll(), -10,10)&!inrange(holoEntity(40):bearing(Target:pos()),-179.5,179.5) & Rev>1 & changed(Click) & Target:health()>0){Fire=1}else{Fire=0}
    
    }


# MANUAL AIM
if(Mode==2){
    
    # BEARING CONTROL
    if(inrange(holoEntity(40):bearing(Master:aimPos()),-179.5,179.5)){
        holoAng(33, holoEntity(33):angles()+ang(0,sign(holoEntity(40):bearing(Master:aimPos()))/3,0))
        VolumeSides+=0.5
        }else{VolumeSides-=0.05}
    
    # ELEVATION CONTROL
    # UP SAFETY
    if(holoEntity(41):angles():roll()>-10){
    holoAng(41, holoEntity(40):toWorld(ang(0,-90,holoEntity(41):angles():roll()-sign(holoEntity(41):elevation(Master:aimPos()))/15)))
    VolumeVertical+0.25
    }else{holoAng(41,holoEntity(41):angles():setRoll(-9.99))}
    
    #DOWN SAFETY
    if(holoEntity(41):angles():roll()<9.3){
    holoAng(41, holoEntity(40):toWorld(ang(0,-90,holoEntity(41):angles():roll()-sign(holoEntity(41):elevation(Master:aimPos()))/15)))
    }else{holoAng(41,holoEntity(41):angles():setRoll(9.25))}

    
    if(Master:keyUse()){Rev+=0.1}else{Rev-=0.05}
    if(Rev<=0.3){Rev=0.3}
    if(Rev>=12){Rev=12}
    
    Rev2+=Rev
    Click=round(Rev2/60)*60
    
    # REV 2 = ROTATION
    # Click = BARREL IN PLACE, TURRET MAY FIRE
    
    # BARREL ROTATION
    holoAng(42, holoEntity(41):toWorld(ang(Rev2,0,90)))
    
    #SHOOTING
    if(Master:keyUse() & Rev>=3 & changed(Click)){Fire=1}else{Fire=0}
    
    }





# TURRET APPLYFORCE AND QUAT

Spread = Rev/6


################QUATERNION
if(Mode==1){Pos = Target:toWorld(Target:boxCenter())}
elseif(Mode==2){Pos = holoEntity(60):toWorld(vec(0,0,60))}
elseif(Mode==0){Pos = holoEntity(60):toWorld(vec(0,0,60))}
Eye = (Pos-Turret:pos()+Target:vel()*0.1):normalized()  #calculate direction to the target; velocity helps in compensating for target's movement
#calculate the quaternions
A = Eye:toAngle()
TarQ = quat(A)
CurQ = quat(Turret)
Q = TarQ/CurQ
V = transpose(matrix(Turret))*rotationVector(Q)
#Aim
Turret:applyTorque((200*V - 10*Turret:angVelVector())*Turret:inertia()*2) # DEFAULT WAS 100*V AND INERTIA WAS *1
#This is here to make the turret hover over the phx plate
Diff = holoEntity(60):toWorld(vec(0,0,0))-Turret:pos()-holoEntity(60):up()*3
Turret:applyForce((Diff*5 - Turret:vel())*Turret:mass())
#Ranger for checking if we are hitting the right target
#RD = rangerOffset(60000,Turret:pos()+20*Turret:forward(),Turret:forward())
#if(RD:entity()==Target){Fire=1}else{Fire=0}
#Ranger = RD:entity()

WLFire=Turret:wirelink()
if(Fire){WLFire["Fire",number]=1}else{WLFire["Fire",number]=0}
#Fire1["Fire",number]=1 






















#VolumeSiddes VolumeVertical VolumeSpin


# VOLUME LIMITERS AND CONTROLS
if(VolumeSides>=0.5){VolumeSides=0.5}
soundVolume(1, VolumeSides)

if(VolumeVertical>=0.5){VolumeVertical=0.5}
soundVolume(2, VolumeVertical)

VolumeSpin=Rev/12
soundPitch(3, VolumeSpin*240)
soundVolume(3, 1)










#####FAIL SAFES

#RIGHT/LEFT 
#holoAng(33, Core:toWorld(ang(0,T,0)))
#holoAng(33, ang(0,T,0))

# UP DOWN
#holoAng(41, holoEntity(40):toWorld(ang(0,-90,-T/6)))

# BARRELS
#holoAng(42, holoEntity(41):toWorld(ang(T*8,0,90)))
T+=0.1


# OVERHEATING
if(changed(Click) & inrange(holoEntity(41):angles():roll(), -10,10)){Overheat+=5}else{Overheat-=0.5}
if(Overheat<=150){Overheat=150}
if(Overheat>=225){Overheat=225}


for(I=47,52){holoColor(I, vec(Overheat,150-(Overheat/10),150-(Overheat/10)))}
#print(4, sign(holoEntity(40):bearing(Target:pos())):toString())


###########################
############ DEBUG MODE
###########################

if(Mode==4){
    
    # BEARING CONTROL
    if(inrange(holoEntity(40):bearing(Target:pos()),-179.5,179.5)& Target:health()>0){
        holoAng(33, holoEntity(33):toWorld(ang(0,sign(holoEntity(40):bearing(Target:pos()))/3,0)))
        VolumeSides+=0.5
        }else{VolumeSides-=0.05}
    
    # ELEVATION CONTROL
    # UP SAFETY
    if(holoEntity(40):toLocal(holoEntity(41):angles()):roll()>-10){
    holoAng(41, holoEntity(40):toWorld(ang(0,-90,holoEntity(41):angles():roll()-sign(holoEntity(41):elevation(Target:pos()+vec(0,0,40)))/15)))
    VolumeVertical+0.25
    }else{holoAng(41,holoEntity(41):angles():setRoll(-9.99))}
    
    print(4, holoEntity(40):toLocal(holoEntity(41):angles()):toString())
    
    #DOWN SAFETY
    if(holoEntity(41):angles():roll()<9.3){
    holoAng(41, holoEntity(40):toWorld(ang(0,-90,holoEntity(41):angles():roll()-sign(holoEntity(41):elevation(Target:pos()+vec(0,0,40)))/15)))
    }else{holoAng(41,holoEntity(41):angles():setRoll(9.25))}

    #print(4, holoEntity(41):angles():roll():toString())
    #holoAlpha(40, 255)
    
    
    if(!inrange(holoEntity(40):bearing(Target:pos()),-165.5,165.5) & Target:health()>0){Rev+=0.1}else{Rev-=0.05}
    if(Rev<=0.3){Rev=0.3}
    if(Rev>=12){Rev=12}
    
    Rev2+=Rev
    Click=round(Rev2/60)*60
    
    # REV 2 = ROTATION
    # Click = BARREL IN PLACE, TURRET MAY FIRE
    
    # BARREL ROTATION
    holoAng(42, holoEntity(41):toWorld(ang(Rev2,0,90)))
    
    #SHOOTING
    #if(Master:keyUse() & Rev>=3 & changed(Click)){Fire=1}else{Fire=0}
    
    #if(inrange(holoEntity(41):angles():roll(), -8,8)){hint("inrange", 0.1)}
    
    if(inrange(holoEntity(41):angles():roll(), -10,10)&!inrange(holoEntity(40):bearing(Target:pos()),-179.5,179.5) & Rev>1 & changed(Click) & Target:health()>0){Fire=1}else{Fire=0}
    
    }








