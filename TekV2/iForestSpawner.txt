@name iForest
@inputs 
@outputs Mode On D:array X Y Z Use Lag Przerwa Location:vector R:ranger Tree:array Dystans Count Distance Interval ClearRadius Theme
@persist 
@model models/props_phx/games/chess/black_dama.mdl

############### #############################################################
### iForest   # Big button activates spawning (Click E, works only for owner)
### By Tek  # Small one deletes the trees(hold the E button)
############### #############################################################

propSpawnUndo(0)

Theme = 1
# 1 = default
# 2 = christmassy, tf2 trees

Distance = 1000
# Radius for trees spread
# Do not set less than value of Clear radius
# (equal or less value than in ClearRadius will not make spawn 
# trees at all)

Trees = 60
# Maximum amount of desired trees
# Note: It's actually amount of props that E2 need to
# detect you have so if you have props spawned you will
# generate less trees.

Interval = 30
# Interval between spawns

ClearRadius = 500
# Radius from entity in which trees will not be spawned






# THE CODE. #######################################
# Some of the Variables may be written in polish language.

interval(10)

if(owner():aimEntity()==entity() & entity():pos():distance(owner():aimPos())<8.2){On=1}else{On=0}

# TREE INDEX
# Some of the trees in the index are pasted few times
# it is so "random" function will paste more of the 
# "Small" trees instead of big ones

# By adding more duplicates and/or another trees you need to increase 
# array maximum amount in Spawn Control, and add new strings in
# delete control if you added new trees to the index.

# DEFAULT TREES
if(Theme==1){
    #Small 1
    Tree[1, string]="models/props_foliage/tree_deciduous_01a-lod.mdl"
    Tree[2, string]="models/props_foliage/tree_deciduous_01a-lod.mdl"
    Tree[3, string]="models/props_foliage/tree_deciduous_01a-lod.mdl"
    Tree[4, string]="models/props_foliage/tree_deciduous_01a-lod.mdl"
    Tree[5, string]="models/props_foliage/tree_deciduous_01a-lod.mdl"
    
    #Big
    Tree[6, string]="models/props_foliage/oak_tree01.mdl"
    
    #Small 2
    Tree[7, string]="models/props_foliage/tree_deciduous_01a.mdl"
    Tree[8, string]="models/props_foliage/tree_deciduous_01a.mdl"
    Tree[9, string]="models/props_foliage/tree_deciduous_01a.mdl"
    Tree[10, string]="models/props_foliage/tree_deciduous_01a.mdl"
    Tree[11, string]="models/props_foliage/tree_deciduous_01a.mdl"
    
    #Wood trash - single log
    Tree[12, string]="models/props_foliage/driftwood_03a.mdl"
    
    #Wood trash
    Tree[13, string]="models/props_foliage/driftwood_clump_01a.mdl"
    Tree[14, string]="models/props_foliage/driftwood_clump_02a.mdl"
    Tree[15, string]="models/props_foliage/driftwood_clump_03a.mdl"
}

# CHRISTMAS TREES
if(Theme==2){
    
    # SMALL ONES
    Tree[1, string]="models/props_foliage/tree_pine_small_snow.mdl"
    Tree[2, string]="models/props_foliage/tree_pine_small_snow.mdl"
    Tree[3, string]="models/props_foliage/tree_pine_small_snow.mdl"
    Tree[4, string]="models/props_foliage/tree_pine_small_snow.mdl"
    Tree[5, string]="models/props_foliage/tree_pine_small_snow.mdl"
    
    # BIG ONE
    Tree[6, string]="models/props_foliage/tree_pine_huge_snow.mdl"
    
    #SUPER SMALL ONES
    Tree[7, string]="models/props_foliage/tree_pine_extrasmall_snow.mdl"
    Tree[8, string]="models/props_foliage/tree_pine_extrasmall_snow.mdl"
    Tree[9, string]="models/props_foliage/tree_pine_extrasmall_snow.mdl"
    Tree[10, string]="models/props_foliage/tree_pine_extrasmall_snow.mdl"
    Tree[11, string]="models/props_foliage/tree_pine_extrasmall_snow.mdl"
    
    #BIG BUSH
    Tree[12, string]="models/props_foliage/shrub_03b.mdl"
    
    #SDMALL BUSH
    Tree[13, string]="models/props_foliage/shrub_03c.mdl"
    Tree[14, string]="models/props_foliage/shrub_03c.mdl"
    Tree[15, string]="models/props_foliage/shrub_03c.mdl"
    
}

Use=owner():keyUse()
X = entity():toLocal(owner():aimPos()):x()
Y = entity():toLocal(owner():aimPos()):y()
Z = entity():toLocal(owner():aimPos()):z()

Lag--
if(Lag<=0){Lag=0}
Przerwa--
if(Przerwa<=0){Przerwa=0}


if(first()){
    for(I=1,5){
        holoCreate(I)
    }
    
    entity():setAlpha(0)
    
    Mode=1
    
    #CORE
    holoPos(1, entity():toWorld(vec(0,0,1.37)))
    holoParent(1, entity())
    holoScaleUnits(1, entity():boxSize()+vec(0,0,-1.8))
    holoModel(1, "hq_cylinder")
    #holoMaterial(1, "phoenix_storms/chrome")
    holoMaterial(1, "debug/debugdrawflat")
 
    #TOP COVER THINGY
    holoPos(2, entity():toWorld(vec(0,0,2.9)))
    holoParent(2, entity())
    holoScaleUnits(2, entity():boxSize()+vec(0.4,0.4,-4))
    holoModel(2, "hq_cylinder")
    holoMaterial(2, "phoenix_storms/black_chrome")
    #holoMaterial(2, "debug/debugdrawflat")
   
    #BOTTOM COVER THINGY
    holoPos(3, entity():toWorld(vec(0,0,0.4)))
    holoParent(3, entity())
    holoScaleUnits(3, entity():boxSize()+vec(0.4,0.4,-3))
    holoModel(3, "hq_cylinder")
    holoMaterial(3, "phoenix_storms/black_chrome")
    #holoMaterial(3, "debug/debugdrawflat")
    
    #BUTTON - SPAWN
    holoPos(4, entity():toWorld(vec(0,0,3)))
    holoParent(4, entity())
    holoScaleUnits(4, entity():boxSize()+vec(-4,-4,-4))
    holoModel(4, "hq_cylinder")
    holoColor(4, vec(100,255,100))
    #holoMaterial(4, "phoenix_storms/chrome")
    holoMaterial(4, "debug/debugdrawflat")
    holoClipEnabled(4,1)
    holoClip(4,vec(0,-4,0),vec(0,1,0),0)
    
    #BUTTON - DELETE
    holoPos(5, entity():toWorld(vec(0,0,3)))
    holoParent(5, entity())
    holoScaleUnits(5, entity():boxSize()+vec(-4,-4,-4))
    holoModel(5, "hq_cylinder")
    holoColor(5, vec(255,100,100))
    #holoMaterial(5, "phoenix_storms/chrome")
    holoMaterial(5, "debug/debugdrawflat")
    holoClipEnabled(5,1)
    holoClip(5,vec(0,-4,0),vec(0,-1,0),0)

}
#D=entity():pos():distance(owner():aimPos())

#SPAWN BUTTON AND SPAWN CONTROL(also core color control)
if(Mode==1&On & Y>-4.05&!Use){holoColor(4, vec(150,255,150))}elseif(!Use){holoColor(4, vec(200,200,200))}

if(Mode==1&On & Y>-4.05&Use&D:count()>=Trees){holoColor(1, vec(255,80,80)) }elseif(Mode==1){holoColor(1, vec(255,255,255))}

if(Mode==1&On & Y>-4.05&Use&$Use&!Lag &D:count()<Trees){Mode=2 Lag=20 soundPlay(2,soundDuration("buttons/combine_button3.wav"),"buttons/combine_button3.wav")}
if(Mode==2&On & Y>-4.05&Use&$Use&!Lag){Mode=1 Lag=20}


if(Mode==2){holoColor(4, vec(100,255,100))}

if(Mode==2 & Przerwa==0){ Location=entity():toWorld(vec(random(-Distance,Distance),random(-Distance,Distance),1000)) }
if(Mode==2 & Przerwa==0){ R = rangerOffset(99999999, Location, vec(0,0,-1))}
if(Mode==2 & Przerwa==0 & !R:entity() & R:position():distance(entity():pos())>ClearRadius){ propSpawn(Tree[random(1,15), string],R:position(),ang(0,random(-180,180),0),1) Przerwa=Interval}

if(Mode==2 & D:count()>=Trees){Mode=1 holoColor(1, vec(255,255,255)) soundPlay(3,soundDuration("buttons/combine_button2.wav"),"buttons/combine_button2.wav")}
if(Mode==2 & D:count()< Trees){ holoColor(1, vec(255,255,100)) }


#DELETE BUTTON AND DELETE CONTROL
#findExcludePlayerProps("*")

findIncludePlayerProps("Tek")
findByClass("prop_physics")


D = findToArray()


#Count++
if(Count>D:count()){Count=1}

if(On & Y<-4.05&!Use){holoColor(5, vec(255,60,60))}elseif(!Use){holoColor(5, vec(255,100,100))}
if(On & Y<-4.05&Use){holoColor(5, vec(255,30,30)) Mode=1 
    if(
    findResult(Count):model()=="models/props_foliage/tree_deciduous_01a-lod.mdl"
    |findResult(Count):model()=="models/props_foliage/oak_tree01.mdl"
    |findResult(Count):model()=="models/props_foliage/tree_deciduous_01a.mdl"
    |findResult(Count):model()=="models/props_foliage/driftwood_03a.mdl"
    |findResult(Count):model()=="models/props_foliage/driftwood_clump_01a.mdl"
    |findResult(Count):model()=="models/props_foliage/driftwood_clump_02a.mdl"
    |findResult(Count):model()=="models/props_foliage/driftwood_clump_03a.mdl"){findResult(Count):propDelete()}else{Count++}
}

if(Count==0){Count=1}



