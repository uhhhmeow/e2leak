@name FailCake BomB
@persist Mode:string T:entity Cur Found R:array Fire Spos:vector Randx TV:vector F1:vector StDist Go Sound2 Sound
@trigger all
interval(100)
runOnChat(1)

O = owner()
LS = O:lastSaid()
E = entity():isWeldedTo()

if(first() | duped()) {
    findByClass("player")
    R = findToArray()
    Found = 1
    Spos = E:pos()
}

E:setMass(1200)

if(clk("start")) {
    Go = 1
}

if(LS:lower():left(2) == "#look" & chatClk(O)) {
    Found = 0
}

if(!Found) {
    Name = LS:lower():sub(4, LS:length())
    CurCheck = R:entity(Cur):name():lower():find(Name) > 0
    
    #print(toString(CurCheck))
    if(CurCheck) {
        T = R:entity(Cur)
        print(T:name())
        Found = 1
    } else {
        Cur++
    }
    
    if(Cur > R:count()) {
        Cur = 0
        print("Player not found.")
        Found = 1
    }
}

if(chatClk(O) & LS:lower() == "#fire" & T:name():length() > 0) {
    Fire = 1
    timer("start", randint(0, 10000))
}

if(Go) {
    if(Fire) {
        if(!StDist) {
            StDist = abs(E:pos():x() - T:pos():x()) + abs(E:pos():y() - T:pos():y())
        }
        
        Dist = abs(E:pos():x() - T:pos():x()) + abs(E:pos():y() - T:pos():y())
        
        if(Dist > StDist/2 & !(Mode == "drop")) {
            Mode = "shootup"
        } elseif(!(Mode == "superdown")) {
            Mode = "drop"
        }
        
        Dist = E:pos():distance(T:pos())
        if(Mode == "shootup") {
            F1 = (T:pos()+vec(0,0,T:height()) - E:pos()) * E:mass() - E:vel()*25
            F2 = vec(0, 0, Dist*E:mass())
            E:applyForce(F1 + F2)
        } elseif(Mode == "drop") {
            if(!Randx) { 
                Randx = randint(-250, 250)
                Randy = randint(-250, 250) 
                TV = T:pos() + vec(Randx, Randy, T:height())
                F = TV - E:pos()
                F1 = vec(F:x(), F:y(), F:z())
            } 
            
            E:applyForce(F1 * E:mass())
        
            if(E:pos():distance(TV) < 100) {
                Mode = "superdown"
            }
        } elseif(Mode == "superdown") {
            E:applyOffsetForce(E:up() * 10, E:forward()*10000)
            E:applyOffsetForce(E:up() * -10, E:forward()*-10000)
            E:applyForce(vec(0, 0, -999999999))
        }

            if(!Sound & E:pos():distance(TV) < 1250) {
                soundPlay(0,0,"weapons/mortar/mortar_shell_incomming1.wav")
                soundVolume(0,3)
                Sound = 1
            }
            
            if(!Sound2 & E:pos():distance(TV) < 200) {
                soundPlay(1,0,"weapons/explode" + toString(randint(3, 5)) + ".wav")
                soundVolume(1,3)
                Sound2 = 1
            }
            
            #print(toString(-1900 - E:vel():z()))
            if(E:health() == 0 & floor((-1900 - E:vel():z())/750) == 0) {
                selfDestructAll()
            }
        }
        #print(Mode)
}
