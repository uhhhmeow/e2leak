@name FailCake UnderWater Beast
@inputs 
@outputs 
@persist P B R X2 Y B2 P2 O:entity Overlook Body:entity StuckPos:vector StuckTime Targets:array Target:entity TargetPos:vector Aggress State Config Init NextThink I
@trigger all
runOnTick(1)

#USAGE
#spawn two refrigerators within 100 units, red will aim at blue

if (duped()) {
#if (first()) {
    State = 0
}

#startup state
if (State == 0)
{
    interval(50)
    
    findIncludeModel("models/hunter/blocks/cube1x1x1.mdl")
    findInSphere(entity():pos(),100)
    findClipToClass("prop_physics")
    Body = findClosest(entity():pos())
    Body:setMass(100) 
    Body:soundPlay(1, 0, "")
    
    O = owner()
    Overlook = 0
    Init = 0
    Config = 0
    State = 1
    Aggress = 0
    StuckTime = curtime()
    StuckPos = vec(0,0,0)
    TargetPos = vec(0,0,0)
    NextThink = curtime()
}
#config state
#0. Select Target
if (State == 1)
{
    interval(50)

    if (Config == 0) {
        if (Init == 0)
        {
            findClearBlackList() 
            findIncludeClass("player")
            if (Overlook == 1)
            {
                findExcludePlayer(O)
            }
            findInSphere(Body:pos(), 1000)
            Targets = findToArray()
            I = 0
            Init = 1
        }
        if (I < 10)
        {
            Target = Targets:entity(randint(0, Targets:count()))
            if (Target:isAlive())
            {
                Aggress = 1
                StuckPos = vec(0,0,0)
                StuckTime = curtime() + random(8, 12)
                Config = 0
                State = 2
            }
            I++
        } else {
            Aggress = 0
            Config = 0
            State = 2
        }
    } else {
        Config = 0
        State = 2
    }
    
}
#active state
#Aggress levels:
#0. wander
#1. follow
#2. attack
if (State == 2)
{
    interval(30)
    Speed = 400
    
    if (Aggress == 0) {
        if (NextThink < curtime())
        {
            TargetPos = Body:pos() + vec(random(-1000,1000),random(-1000,1000),random(-1000,1000))
            Body:applyForce(Body:vel() * (Body:mass() * -1))
            if (random(30) < 10) {
                soundPitch(2, randint(40, 100))
                Body:soundPlay(2, 0, "weapons/knife/knife_stab.wav")
                Overlook = 0
                O = owner()
            }
            
            State = 1
            Config = 0
            Init = 0
            NextThink = curtime() + random(1, 8)
        }
    } elseif (Aggress == 1) {
        Speed = 600
        TargetPos = Target:pos()
        if (Target:isAlive() & (Body:pos():distance(Target:pos()) < 1000)) {
            if (NextThink < curtime())
            {
                if ((random(10) > 5) | (Body:pos():distance(Target:pos()) < 500)) {
                    Body:applyForce(Body:vel() * (Body:mass() * -1))
                    soundPitch(2, randint(40, 100))
                    SoundTemp = random(0, 30)
                    if (SoundTemp < 10) {
                        Body:soundPlay(2, 0, "weapons/knife/knife_deploy1.wav")
                    } elseif (SoundTemp < 20) {
                        Body:soundPlay(2, 0, "weapons/knife/knife_stab.wav")
                    }
                    Aggress = 2
                } else {
                    Body:applyForce((Body:vel() * (Body:mass() * -1)) + vec(random(-800,800),random(-800,800),random(-800,800)))
                }
                NextThink = curtime() + random(1, 8)
            }
        } else {
            State = 1
            Config = 0
            Init = 0
        }
    } elseif (Aggress == 2) {
        Speed = 5000
        TargetPos = Target:pos()
        if (NextThink < curtime())
        {
            Body:applyForce(Body:vel() * (Body:mass() * -1))
            Aggress = 1
            NextThink = curtime() + random(1, 8)
        }
        #Thrash
        if ((Body:pos():distance(Target:pos()) < 60) & (randint(30) < 20))
        {
            soundPitch(2, randint(80, 120))
            Body:soundPlay(2, 0, "weapons/knife/knife_stab.wav")
            Body:applyAngForce(Body:mass() * ang(random(-1000, 1000),random(-1000, 1000),random(-1000, 1000)))
        }
        #Gotcha
        if (!Target:isAlive())
        {
            soundPitch(2, randint(10, 60))
            Target:soundPlay(2, 0, "weapons/knife/knife_stab.wav")
            Aggress = 1
        }
    }

    #Unstick
    if (Aggress > 0)
    {
        if (StuckTime < curtime())
        {
            if (Body:pos():distance(StuckPos) < 128)
            {
                Body:applyForce((Body:vel() * (Body:mass() * -1)) + vec(random(-800,800),random(-800,800),random(-800,800)))
                Body:soundPlay(2, 0, "npc/ichthyosaur/snap_miss.wav")
                Overlook = 1
                O = Target
                State = 1
                Config = 0
                Init = 0
            }
            StuckPos = Body:pos()
            StuckTime = curtime() + random(8, 12)
        }
    }

    #Face Forward
    R = Body:angles():roll()*2
    RR = Body:angVel():roll()*2
    B2 = Body:bearing(Body:pos() + Body:vel())
    B= (32*$B2 + 2*B2)
    P2 = Body:elevation(Body:pos() + Body:vel())
    P = (32*$P2 + 2*P2)
    PP = Body:angVel():pitch()
    X2=200
    Y=50
    
    Body:applyAngForce(ang(-P * X2,-B *X2 ,-R * X2- RR * Y))
    
    TargetDir = TargetPos - Body:pos()
    EntVel = TargetDir:normalized() * (Speed)
    Body:applyForce(EntVel)

}
