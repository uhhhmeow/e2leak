@name Gorge Metch
runOnTick(1)
E1=entity(),EE=(E1:isWeldedTo() ? E1:isWeldedTo() : E1)
EA=-EE:angles()*15-EE:angVel()*2 
EV=EE:massCenter(),ER=EE:right(),EF=EE:forward(),EU=EE:up()
EE:applyForce((vec(0,0,10.7)-EE:vel())*EE:mass())
Lev=EE:inertia():length()
EE:applyAngForce(EA*Lev)

