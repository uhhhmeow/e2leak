@name KeyPad Admin For Pixel
@inputs C1 C5
@outputs Green1 Screen Green2 Fire
@persist Reset
if(first()){
    C1 = 0
    C5 = 0
    C7 = 0
    Green1 = 0
    Green2 = 0
    Fire = 0
}
interval(100)
if(Green1 == 1 & Green2 == 1){
Fire = 1
Reset += 1
Screen = Reset
}
if(Reset > 10){
    C1 = 0
    C5 = 0
    C7 = 0
    Reset = 0
    Screen = 0
    Fire = 0
    Green1 = 0
    Green2 = 0
}
if(C1 == 1){
Green1 = 1
}
if(C5 == 1){
Green2 = 1
}
if(C7 == 1){
Green2 = 1
}
