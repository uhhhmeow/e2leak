@name Foot Steps
@persist Dist Index Material:string Rng:ranger MaxPrint Step
@persist Feet:array [Chainlink Dirt Concrete Duct Grass Ice Metal Mud Metalgrate Sand Slosh Snow Tile Wade Wood Woodpanel]:array
runOnTick(1)
O=owner()
Material=Rng:matType()
Dist=(O:pos()-holoEntity(Index):pos()):length()

if(first())
{
    MaxPrint=50 # maximum holoCreate
    Step=70     # step size
    
    Index=1
    holoCreate(Index)
    holoModel(Index,"models/props_junk/Shoe001a.mdl")
    holoScale(Index,vec(1,1,0.1))
    
    #these ARRAYs are to long
    Chainlink=array(
    "player/footsteps/chainlink1.wav",
    "player/footsteps/chainlink2.wav",
    "player/footsteps/chainlink3.wav",
    "player/footsteps/chainlink4.wav")
    
    Concrete=array(
    "player/footsteps/concrete1.wav",
    "player/footsteps/concrete2.wav",
    "player/footsteps/concrete3.wav",
    "player/footsteps/concrete4.wav")
    
    Dirt=array(
    "player/footsteps/dirt1.wav",
    "player/footsteps/dirt2.wav",
    "player/footsteps/dirt3.wav",
    "player/footsteps/dirt4.wav")
    
    Duct=array(
    "player/footsteps/duct1.wav",
    "player/footsteps/duct2.wav",
    "player/footsteps/duct3.wav",
    "player/footsteps/duct4.wav")
    
    Grass=array(
    "player/footsteps/grass1.wav",
    "player/footsteps/grass2.wav",
    "player/footsteps/grass3.wav",
    "player/footsteps/grass4.wav")
    
    Gravel=array(
    "player/footsteps/gravel1.wav",
    "player/footsteps/gravel2.wav",
    "player/footsteps/gravel3.wav",
    "player/footsteps/gravel4.wav")
    
    Ice=array(
    "player/footsteps/ice1.wav",
    "player/footsteps/ice2.wav",
    "player/footsteps/ice3.wav",
    "player/footsteps/ice4.wav")
    
    Metal=array(
    "player/footsteps/metal1.wav",
    "player/footsteps/metal2.wav",
    "player/footsteps/metal3.wav",
    "player/footsteps/metal4.wav")
    
    Metalgrate=array(
    "player/footsteps/metalgrate1.wav",
    "player/footsteps/metalgrate2.wav",
    "player/footsteps/metalgrate3.wav",
    "player/footsteps/metalgrate4.wav")
    
    Mud=array(
    "player/footsteps/mud1.wav",
    "player/footsteps/mud2.wav",
    "player/footsteps/mud3.wav",
    "player/footsteps/mud4.wav")
    
    Sand=array(
    "player/footsteps/sand1.wav",
    "player/footsteps/sand2.wav",
    "player/footsteps/sand3.wav",
    "player/footsteps/sand4.wav")
    
    Slosh=array(
    "player/footsteps/slosh1.wav",
    "player/footsteps/slosh2.wav",
    "player/footsteps/slosh3.wav",
    "player/footsteps/slosh4.wav")
    
    Snow=array(
    "player/footsteps/snow1.wav",
    "player/footsteps/snow2.wav",
    "player/footsteps/snow3.wav",
    "player/footsteps/snow4.wav",
    "player/footsteps/snow5.wav",
    "player/footsteps/snow6.wav")
    
    Tile=array(
    "player/footsteps/tile1.wav",
    "player/footsteps/tile2.wav",
    "player/footsteps/tile3.wav",
    "player/footsteps/tile4.wav")
    
    Wade=array(
    "player/footsteps/wade1.wav",
    "player/footsteps/wade2.wav",
    "player/footsteps/wade3.wav",
    "player/footsteps/wade4.wav",
    "player/footsteps/wade5.wav",
    "player/footsteps/wade6.wav",
    "player/footsteps/wade7.wav",
    "player/footsteps/wade8.wav")
    
    Wood=array(
    "player/footsteps/wood1.wav",
    "player/footsteps/wood2.wav",
    "player/footsteps/wood3.wav",
    "player/footsteps/wood4.wav")
    
    Woodpanel=array(
    "player/footsteps/woodpanel1.wav",
    "player/footsteps/woodpanel2.wav",
    "player/footsteps/woodpanel3.wav",
    "player/footsteps/woodpanel4.wav")
}


if(O:isOnGround()&changed(Dist)&Dist>Step)
{
    Index++
    
    if(mod(Index,2)){
    holoCreate(Index)
    holoModel(Index,"models/props_junk/Shoe001a.mdl")
    holoScale(Index,vec(1,1,0.35))
    holoAng(Index,O:angles()+ang(0,0,180))
    holoPos(Index,O:pos()+vec(0,0,-1)-(O:right()*8)) 
    }
    else{
    holoCreate(Index)
    holoModel(Index,"models/props_junk/Shoe001a.mdl")
    holoScale(Index,vec(1,1,0.35))
    holoAng(Index,O:angles()+ang(0,0,180))
    holoPos(Index,O:pos()+vec(0,0,-1)+(O:right()*6)) }
    holoDelete(Index-MaxPrint)
    holoEntity(Index):soundPlay(Index,100+Index,Feet:string(randint(1,Feet:count())))
    
    Rng=rangerOffset(100,O:pos(),O:up()*-1)
    
    if(changed(Material)&Material=="concrete"){Feet=Concrete}
    if(changed(Material)&Material=="chainlink"){Feet=Chainlink}
    if(changed(Material)&Material=="dirt"){Feet=Dirt}
    if(changed(Material)&Material=="duct"){Feet=Duct}
    if(changed(Material)&Material=="grass"){Feet=Grass}
    if(changed(Material)&Material=="ice"){Feet=Ice}
    if(changed(Material)&Material=="metal"){Feet=Metal}
    if(changed(Material)&Material=="mud"){Feet=Mud}
    if(changed(Material)&Material=="grate"){Feet=Metalgrate}
    if(changed(Material)&Material=="sand"){Feet=Sand}
    if(changed(Material)&Material=="slosh"){Feet=Slosh}
    if(changed(Material)&Material=="snow"){Feet=Snow}
    if(changed(Material)&Material=="tile"){Feet=Tile}
    if(changed(Material)&Material=="wade"){Feet=Wade}
    if(changed(Material)&Material=="wood"){Feet=Wood}
    if(changed(Material)&Material=="woodpanel"){Feet=Woodpanel}
    
    if(changed(Index)&Index>Index+holoRemainingSpawns()-1)
    {
        Index=1
        
        holoDelete(Index)
    }
}
