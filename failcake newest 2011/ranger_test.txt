@name Ranger Test
@inputs 
@outputs Once T
@persist Vec:vector T Ran1:ranger Ran2:ranger
@trigger all

runOnTick(1)

E = entity()
Es = entity():isWeldedTo()
O = owner()


holoCreate(4)
holoModel(4,"cube")
holoScale(4,vec(3,3,3))    

T+=0.1

Ran1 = rangerOffset(100,holoEntity(4):pos(),holoEntity(4):forward())
Ran2 = rangerOffset(100,holoEntity(4):pos(),-holoEntity(4):forward())

holoCreate(4)
holoModel(4,"cube")
holoScale(4,vec(3,3,3)) 
holoPos(4,((Ran1:position() + Ran2:position())/2 + holoEntity(1):toWorld(vec(T,0,0))))

if(Ran1:hit()){
holoCreate(1)
holoModel(1,"cube")
holoScaleUnits(1,vec(5,5,5))
holoPos(1,Ran1:position())    
}else{
holoDelete(1)
}

if(Ran2:hit()){
holoCreate(2)
holoModel(2,"cube")
holoScaleUnits(2,vec(5,5,5))
holoPos(2,Ran2:position())    
}else{
holoDelete(2)
}


