@name Philcraft
@inputs [Cam Pod]:wirelink
@persist [Pos Targ AimPos CamPos]:vector [Ang HeadAng]:angle 
@persist [AA LS]:array [AimEnt O Temp]:entity
@persist [Material Color Model Sound]:string
@persist [Materials Colors Models Sounds Doors]:table
@persist A B C D E F G H J K L M N KA1 KA2
@persist Noclip Door R Water Torch
if (first() | duped()) {
    Players=glonDecode(gGetStr("ply"))
    runOnTick(1), runOnChat(1)
    for(I=1,8) {
        holoCreate(I)
        if(I<5) {holoScaleUnits(I,vec(2,2,6))}
    }
    O = owner()
    holoCreate(0)
    holoScale(0,vec(1,1,1))
    holoAlpha(0,150)
    holoColor(0,vec(0,0,0))
    for(I=1,5) {holoParent(I,6)}
    holoScaleUnits(7,vec(2,2,2))
    holoScaleUnits(8,vec(2,2,2))
    holoScaleUnits(5,vec(4,4,4))
    holoMaterial(5,"beer/wiremod/gate_e2")
    holoScaleUnits(6,vec(2,4,6))
    B=1
    A=-180
    Pos=entity():pos()
    gShare(1)
    Cam["Angle",angle]=holoEntity(5):angles()
    Cam["Parent",entity]=holoEntity(5)
    P=255
    Players:pushEntity(holoEntity(6))
    gSetStr("ply",glonEncode(Players))
}

Sounds=glonDecodeTable(gGetStr("sou"))


if (chatClk()) {
    LS=lastSaid():explode(" ")
    Cam["Position",vector]=holoEntity(5):pos()
    if(LS[1,string]=="/material"){
        Materials=glonDecodeTable(gGetStr("mat"))
        Materials[LS:string(2),string]=LS:string(3)
        gSetStr("mat",glonEncode(Materials))
    }
    if(LS[1,string]=="!mat"){
        Material=LS:string(2)
    }
    if(LS[1,string]=="/color"){
        Colors=glonDecodeTable(gGetStr("col"))
        Colors[LS[2,string],vector]=vec(LS[3,string]:toNumber(),LS[4,string]:toNumber(),LS[5,string]:toNumber())
        gSetStr("col",glonEncode(Colors))
    }
    if(LS[1,string]=="!color"){
        Color=LS[2,string]
    }
    if(LS[1,string]=="/model"){
        Models=glonDecodeTable(gGetStr("mod"))
        Models[LS[2,string],string]=LS[3,string]
        gSetStr("mod",glonEncode(Models))
    }
    if(LS[1,string]=="!model"){
        Model=LS[2,string]
        holoModel(0,Models[Model,string])
    }
    if(LS[1,string]=="/sound"){
        Sounds=glonDecodeTable(gGetStr("sou"))
        Sounds[LS[2,string],string]=LS[3,string]
        gSetStr("sou",glonEncode(Sounds))
    }
    if(LS[1,string]=="!sound"){
        Sounds=glonDecodeTable(gGetStr("sou"))
        K=!K
        holoEntity(5+K):soundPlay(10,0,Sounds[LS[2,string],string])
    }
    if(LS[1,string]=="!noclip"){
        Noclip=!Noclip
    }
    if(LS[1,string]=="!door"){
        Door=!Door
    }
    if(LS[1,string]=="!water"){
        Water=!Water
    }
    if(LS[1,string]=="!torch"){
        Torch=!Torch
    }
    if(owner():lastSaid():explode(""):string(1)=="/"){hideChat(1)}
    if(owner():lastSaid():explode(""):string(1)=="!"){hideChat(1)}
}
CamPos=vec(Cam["X",number],Cam["Y",number],Cam["Z",number])
KA1=Pod["Entity",entity]:driver():keyAttack1()
KA2=Pod["Entity",entity]:driver():keyAttack2()
if($Pos:setZ(0):length()){
    if(F<=0){G=random(20,50)}
    if(F>G){G=0}
    if(G){F+=1}
    else{F-=1}
}
else{F=0}
Cam["Activated",number]=Pod["Active",number]
if(owner():keyAttack2()){
    Targ=owner():aimPos()
}
AA[1,number]=holoEntity(1):angles():pitch()+holoEntity(1):angles():roll()
AA[2,number]=holoEntity(2):angles():pitch()+holoEntity(2):angles():roll()
AA[3,number]=holoEntity(1):angles():roll()

Ang=Pod["Entity",entity]:driver():eyeAngles()
holoPos(6,Pos+vec(0,0,10))
holoPos(5,holoEntity(6):pos()+vec(0,0,2.75+cos(Ang:pitch())*2)+holoEntity(6):forward()*sin(Ang:pitch())*2)
holoAng(6,Ang:setPitch(0))
holoAng(5,Pod["Entity",entity]:driver():eyeAngles())

holoPos(7,holoEntity(6):pos()+vec(0,0,1.9)+holoEntity(6):right()*2.7)
holoAng(7,holoEntity(6):angles()+ang(A,0,F))
holoPos(8,holoEntity(6):pos()+vec(0,0,1.9)-holoEntity(6):right()*2.7)
holoAng(8,holoEntity(6):angles()+ang(180-A,0,F))

holoPos(1,holoEntity(7):pos()+holoEntity(7):up()*1.9)
holoAng(1,holoEntity(7):angles())
holoPos(2,holoEntity(8):pos()-holoEntity(8):up()*1.9)
holoAng(2,holoEntity(8):angles())

holoPos(3,holoEntity(6):pos()-vec(0,0,3)+holoEntity(6):right()*0.9-holoEntity(6):forward()*sin(A)*2.5+holoEntity(6):up()*cos(A)*2.5)
holoAng(3,holoEntity(6):angles()+ang(-A,0,0))
holoPos(4,holoEntity(6):pos()-vec(0,0,3)-holoEntity(6):right()*0.9+holoEntity(6):forward()*sin(A)*2.5+holoEntity(6):up()*cos(A)*2.5)
holoAng(4,holoEntity(6):angles()+ang(A,0,0))
E=Pod["W",number]+Pod["S",number]+Pod["D",number]+Pod["A",number]
if(E){
    A+=B*5*(Pod["Shift",number]+1)
    if(A>=-100){B=-1}
    if(A<=-260){B=1}
}
else{A=-180}
if(N){   
    if(Pod["W",number]){
        Pos=Pos+holoEntity(6):forward()*(Pod["Shift",number]+1)
    }
    if(Pod["S",number]){
        Pos=Pos-holoEntity(6):forward()*1
    }
    if(Pod["A",number]){
        Pos=Pos-holoEntity(6):right()*1
    }
    if(Pod["D",number]){
        Pos=Pos+holoEntity(6):right()*1
    }
}
else{
    if(Pod["W",number]&!rangerOffset(Pos+vec(0,0,1),Pos+vec(0,0,1)+holoEntity(6):forward()*3):hit()){
        Pos=Pos+holoEntity(6):forward()*(Pod["Shift",number]+1)
    }
    if(Pod["S",number]&!rangerOffset(Pos+vec(0,0,1),Pos+vec(0,0,1)-holoEntity(6):forward()*3):hit()){
        Pos=Pos-holoEntity(6):forward()*1
    }
    if(Pod["A",number]&!rangerOffset(Pos+vec(0,0,1),Pos+vec(0,0,1)-holoEntity(6):right()*3):hit()){
        Pos=Pos-holoEntity(6):right()*1
    }
    if(Pod["D",number]&!rangerOffset(Pos+vec(0,0,1),Pos+vec(0,0,1)+holoEntity(6):right()*3):hit()){
        Pos=Pos+holoEntity(6):right()*1
    }
}    
if(N){
    if(Pod["Space",number]){
        D=1
    }
    if(Pod["Alt",number]){
        D=-1
    }
    elseif(!Pod["Space",number]){D=0}
}
else{   
    if(Pod["Space",number]&rangerOffset(Pos-vec(0,0,0),Pos-vec(0,0,2)):hit()){
        D=2
    }
    if(!rangerOffset(Pos+vec(0,0,1),Pos):hit()){
        D-=0.1
    }
    elseif(D<0){D=0}
    if(D&rangerOffset(holoEntity(5):pos(),holoEntity(5):pos()+vec(0,0,2)):hit()){
        D=-0.5
    }
}
Pos=Pos+vec(0,0,D)
if(!N){Pos=rangerOffset(Pos+vec(0,0,1),Pos):position()}
AimPos=rangerOffset(holoEntity(5):pos(),holoEntity(5):pos()+holoEntity(5):forward()*1000):position()
AimEnt=rangerOffset(holoEntity(5):pos(),holoEntity(5):pos()+holoEntity(5):forward()*1000):entity()

if($A&A==-180){
    J=!J
    holoEntity(3+J):soundPlay(1,-1,"player/footsteps/dirt1.wav")
}
if(!AimEnt|(AimEnt:owner()==owner()))
{
    if($KA1==1&AimEnt:type()=="prop_physics"){
        H=!H
        holoEntity(1+H):soundPlay(0,-1,"physics/cardboard/cardboard_box_impact_hard"+randint(1,7):toString()+".wav")
        AimEnt:propDelete()
    }
    if($KA2==1&rangerOffset(holoEntity(5):pos(),holoEntity(5):pos()+holoEntity(5):forward()*45):hit()){
        if(Water){
            Waters=glonDecode(gGetStr("wat"))
            Temp=propSpawn("models/hunter/blocks/cube025x025x025.mdl",round(AimPos/11.75)*11.75,1)
            Temp:setAlpha(170)
            Temp:setColor(vec(0,0,255))
            gSetEnt("wat",Temp)
        }
        if(Door){
            Doors=glonDecodeTable(gGetStr("doo"))
            TempAng=ang(0,90+round(Ang:yaw()/90)*90,90)
            Temp=propSpawn("models/hunter/plates/plate025x05.mdl",round(AimPos/11.75)*11.75+vec(0,0,6)-TempAng:up()*4.5,TempAng,1)
            Temp:setMaterial("phoenix_storms/wood")
            Doors[Temp:id():toString(),number]=1
            gSetStr("doo",glonEncode(Doors))
        }
        if(Torch){
            TempAng=rangerOffset(holoEntity(5):pos(),holoEntity(5):pos()+holoEntity(5):forward()*100):hitNormal():toAngle()
            if(TempAng:roll()){propSpawn("models/hunter/plates/plate025.mdl",round(AimPos/11.75)*11.75,TempAng+ang(90,0,90),1)}
            else{propSpawn("models/hunter/plates/plate025.mdl",round(AimPos/11.75)*11.75,TempAng+ang(90,0,45),1)}
        }
        if(!Torch&!Door&!Water){      
            Materials=glonDecodeTable(gGetStr("mat"))     
            Colors=glonDecodeTable(gGetStr("col"))    
            Models=glonDecodeTable(gGetStr("mod")) 
            Temp=propSpawn(Models[Model,string],round(AimPos/11.75)*11.75,1)
            Temp:setMaterial(Materials[Material,string])
            Temp:setColor(Colors[Color,vector])
        }
        H=!H
        holoEntity(1+H):soundPlay(2,-1,"ambient/water/drip"+randint(1,4):toString()+".wav")
    }
}
holoPos(0,round(AimPos/11.75)*11.75)

R=Pod["R",number]
if($R==1&AimEnt:model()=="models/hunter/plates/plate025x05.mdl"){
    Doors=glonDecodeTable(gGetStr("doo"))
    L=!L
    holoEntity(5+L):soundPlay(4,0,"doors/door_wood_close1.wav")
    Doors[AimEnt:id():toString(),number]=-Doors[AimEnt:id():toString(),number]
    
    AimEnt:setAng(AimEnt:angles()+ang(0,Doors[AimEnt:id():toString(),number]*90,0))
    TempAng=AimEnt:angles()
    AimEnt:setPos(AimEnt:pos()-TempAng:forward()*5*Doors[AimEnt:id():toString(),number]+TempAng:up()*5)
    
    gSetStr("doo",glonEncode(Doors))

}
if(Noclip|rangerOffset(Pos+vec(0,0,1),Pos):entity():getColor()==vec(0,0,255)){
    N=1
}
else{N=0}
if(!Noclip&rangerOffset(Pos+vec(0,0,1),Pos):entity():getColor()==vec(0,0,255)){
    Pos=Pos-vec(0,0,0.5)
}
