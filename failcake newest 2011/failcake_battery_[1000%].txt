@name FailCake Battery
@inputs On
@outputs Battery Up Color Power
@persist Up Color
@trigger all
@model models/kobilica/capacatitor.mdl
runOnTick(1)

E = entity()

if(first() |duped()){
setName("FailCake Battery [200%]") 
E:setColor(1,1,1)  
E:setAlpha(255) 
E:setMaterial("models/debug/debugwhite")
Battery = 100   
Up = 10
Color = 255
Power = 0
holoCreate(1)
holoModel(1,"hqcylinder") 
holoColor(1,vec(1,1,1))
holoScaleUnits(1,vec(-12,-12,-20))
holoPos(1,E:toWorld(vec(0,0,10)))
holoParent(1,E)
holoMaterial(1,"models/debug/debugwhite")

holoCreate(2)
holoModel(2,"hqcylinder") 
holoColor(2,vec(1,1,1))
holoScaleUnits(2,vec(8,8,3))
holoPos(2,E:toWorld(vec(0,0,21)))
holoAlpha(2,255)
holoParent(2,E)
holoMaterial(2,"models/debug/debugwhite")

holoCreate(3)
holoModel(3,"hqcylinder") 
holoParent(3,E)
holoMaterial(3,"models/debug/debugwhite")
holoColor(3,vec(0,255,0))
holoScaleUnits(3,vec(11,11,20))
holoPos(3,E:toWorld(vec(0,0,10)))

}



if(On){

timer("Drain",100)

if(clk("Drain")){
if(Battery + 100 > 0){
Battery = Battery - 1
Up-=0.05
Color-= 1
if(Battery + 100 >= 100){
holoColor(3,vec(0,Color,0))
}elseif(Battery + 100 >= 70){
holoColor(3,vec(Color,Color,0))
}elseif(Battery + 100 >= 50){
holoColor(3,vec(Color,0,0))
}
Power = 1
holoScaleUnits(3,vec(11,11,10+Battery/10))
holoPos(3,E:toWorld(vec(0,0,Up)))
Show = Battery + 100
setName("FailCake Battery [" + Show + "%]")  
}else{
stoptimer("Drain")
Power = 0
setName("FailCake Battery [EMPTY]")  
}  

}
}else{
Power = 0
stoptimer("Drain")
}
