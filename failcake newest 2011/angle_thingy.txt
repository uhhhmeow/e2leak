@name Angle thingy
@inputs 
@outputs 
@persist Ang:angle E:entity
@trigger 

TarQ = quat(Ang)
CurQ = quat(E)
Q = TarQ/CurQ
V = E:toLocal(rotationVector(Q)+E:pos())
E:applyTorque((150*V - 12*E:angVelVector())*E:inertia())   
