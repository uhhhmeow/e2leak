@name FailCake Turtle Girl V2
@outputs Holo ModelAny Rand2 Water R
@trigger all
@model models/props/de_tides/Vending_turtle.mdl

#############################
# Added Swiming and Fliping #
#############################
# Girl #

E = entity()
Water = entity():isUnderWater()
E:setColor(200,0,200,255)
if(first()){
hint("Made by FailCake",7)  
}

Rand2 = randint(1000,2000)   
Move=array()
Move:setString(1,"player/footsteps/slosh1.wav")
Move:setString(2,"player/footsteps/slosh2.wav")
Move:setString(3,"player/footsteps/slosh3.wav")
Move:setString(4,"player/footsteps/slosh4.wav")


timer("jump",Rand2)
if(clk("jump")){
Ang = E:angles():roll()

if(Water == 0){
    if(Ang<170){
        E:applyForce(-E:right() * 200 + E:up() * 200)
        E:soundPlay(100,100,"ambient/levels/canals/drip4.wav") 
        soundPitch(100,200)   
    }
    else{
        E:applyTorque(vec(0,1,0) * 1500)
        E:soundPlay(100,100,"ambient/levels/canals/drip4.wav") 
        soundPitch(100,200)  
    }
}else{
E:applyForce(-E:right() * 300)
E:soundPlay(7331,0,Move:string(R))
R=randint(1,4)
}
}else{
E:applyForce(vec(0,0,0)) 
soundStop(100)
soundStop(7331)
}

if(duped()){selfDestructAll()}
