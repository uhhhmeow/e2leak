@name FailCake Portable Spawns V1
@outputs Creating HoloAny [Reason Model]:string Timeout Allow Set HP
@outputs Max Once2 Dead F Space
@persist [Tele Spawns]:array T
@trigger all
@model models/Gibs/HGIBS.mdl

runOnTick(1)
runOnChat(1)

E = entity()

if(first()|duped()){
TotalSpawn = 0
hint("Made By FailCake",7)
hint("Say -create to Create a Spawn Place!",7)   

### Check HoloAny ###
Set = 0
print("Checking if the Server Has HoloModelAny!")
holoCreate(0)
holoModel(0,"models/props_junk/popcan01a.mdl")
holoPos(0,E:pos()+vec(0,0,30))

Model = "models/props_junk/popcan01a.mdl"

if(holoEntity(0):model() == Model){
HoloAny = 1   
hint("[NOTE] HoloModelAny Detected!",7)
holoDelete(0)
}else{
HoloAny = 0
hint("[WARNING] No HoloModelAny Detected!",7)
holoDelete(0)
} 
print("Done!")
### Check HoloAny ###

}   


Tele[1,string] = "ambient/machines/teleport1.wav"
Tele[2,string] = "ambient/machines/teleport3.wav"
Tele[3,string] = "ambient/machines/teleport4.wav"

String = owner():lastSaid():explode(" ")

if(chatClk(owner())){  
               
if(String[1,string]=="-create" & Creating == 0){      
Creating = 1
hint("Point and then Press E to Create the Spawn!",7)  
hint("Once you are done, Press MOUSE2 to Exit Create Mode",7)     
}

if(String[1,string]=="-set" & Creating == 0 & Spawns:count() >= 1){      

if(String[2,string]:toNumber()<= Spawns:count() & String[2,string]:toNumber() > 0){
Set = String[2,string]:toNumber()
hint("Spawn Selected = "+Set,7)       
}else{
hint("[ERROR] Theres only "+Spawns:count()+" Spawns!",7) 
}
}else{
if(String[1,string]=="-set"){
if(Creating == 0){
hint("[ERROR] Theres No Spawns!",7) 
}else{
hint("[ERROR] Exit Create Mode First!",7) 
}   
}
}

}
HP = owner():health()

if(Set > 0){
    
for(S=2,4){

T+=cos(1)
R1 = cos(T)*20
R2 = sin(T)*20
    
    
holoCreate(S)
holoModel(S,"cube")
holoScaleUnits(S,vec(2,2,2))
holoAlpha(S,0)
holoPos(S,Spawns[Set,entity]:pos()+vec(R1,R2,S*5))
holoParent(S,Spawns[Set,entity])

if(Once2 == 0){
holoEntity(S):setTrails(5,5,4,"trails/physbeam",vec(255,255,255),255)
if(S >= 4){
Once2 = 1
}
}

}

if(changed(owner():isAlive()) & owner():isAlive()) {
owner():setEntPos(Spawns[Set,entity]:pos()+vec(0,0,50)) 
F = randint(1,3)
Spawns[Set,entity]:soundPlay(100,100,Tele[F,string])    
}

}else{
for(S2 = 2,4){
holoDelete(S2)
}
}



if(Creating){
    
holoCreate(1)
holoAlpha(1,200)   
holoPos(1,owner():aimPos())

if(HoloAny){
holoModel(1,"models/props_junk/sawblade001a.mdl") 
}else{
holoModel(1,"cube") 
holoMaterial(1,"models/wireframe")
}

holoColor(1,vec(0,255,0))   
Allow = 1 

if(owner():keyAttack2()){
Creating = 0
holoDelete(1)
hint("Exited Create Mode!",7)    
}

if(owner():keyUse() & Timeout == 0 & Allow == 1){
 
Spawn = propSpawn("models/props_junk/sawblade001a.mdl",owner():aimPos(),ang(0,0,0),1)
Spawns:pushEntity(Spawn)

hint("Spawn Created!",7)
Timeout = 1
holoEntity(1):soundPlay(100,100,"buttons/button9.wav")

}elseif(owner():keyUse() & Timeout == 0 & Allow == 0){
holoEntity(1):soundPlay(100,100,"buttons/button8.wav")
hint("Cannot Create Spawn. Reason : " + Reason,7)
Timeout = 1
}

if(Timeout){
timer("wait",1000)
if(clk("wait")){
Timeout = 0
soundStop(100)
stoptimer("wait")    
}
}

}


if(Spawns:count() >= 5){
Max = 1    
}else{
Max = 0
}

for(I=0,Spawns:count()){
    
if(Spawns[I,entity] == noentity()){
Spawns:remove(I)    
}   
 
}

if(duped()){selfDestructAll()}
