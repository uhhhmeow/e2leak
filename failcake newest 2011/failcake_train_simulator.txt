@name FailCake Train Simulator
@inputs On
@outputs 
@persist 
@trigger all
runOnTick(1)

E = entity()

if(first() | duped()){
E:setColor(1,1,1) 

holoCreate(1)
holoModel(1,"hq_rcube_thick")
holoPos(1,E:toWorld(vec(0,0,5)))
holoScale(1,vec(0.5,0.5,0.5))
holoMaterial(1,"models/wireframe")
holoParent(1,E)    
    
}

Rang1 = rangerOffset(100,holoEntity(1):pos(),holoEntity(1):right())
Rang2 = rangerOffset(100,holoEntity(1):pos(),-holoEntity(1):right())

holoPos(1,Rang2:pos()+(Rang2:pos():distance(Rang1:pos())/2))


if(Rang1:hit()){
    
holoCreate(2)
holoModel(2,"hq_rcube_thick")
holoPos(2,Rang1:position())
holoScale(2,vec(0.5,0.5,0.5))
holoColor(2,vec(255,0,0))
holoMaterial(2,"models/wireframe")
holoParent(2,E)        
    
}else{

holoDelete(2)

}

if(Rang2:hit()){
    
holoCreate(3)
holoModel(3,"hq_rcube_thick")
holoPos(3,Rang2:position())
holoScale(3,vec(0.5,0.5,0.5))
holoColor(3,vec(255,0,0))
holoMaterial(3,"models/wireframe")
holoParent(3,E)        
    
}else{

holoDelete(3)

}
