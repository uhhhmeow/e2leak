@name FailCake Shield v3
@outputs Scale I I2 I3 Model:string On Alpha
@persist Players:array Goto2:array T
@trigger all 

runOnTick(1)
runOnChat(1)

E = entity()

Goto2 = owner():lastSaid():explode(" ")


if(first()){
E:setAlpha(0)
Scale = 60
Kill = 1
Model = "icosphere3"    
On = 0  
Alpha = 0

findExcludePlayer("Vana")
findExcludePlayer("okei")
findExcludePlayer("CzechHero")
findExcludePlayer("Hati") 
findExcludePlayer(owner())   
findByClass("player")
Players = findToArray()
I2 = 6
I = 1
I3 = 1
Wait = 1
I4 = 6

holoCreate(1)
holoModel(1,Model)
holoPos(1,E:pos()+vec(0,0,Scale))
holoScale(1,vec(Scale,Scale,Scale))
holoColor(1,vec(1,1,1))
holoColor(2,vec(90,90,90))
holoParent(1,E)

holoCreate(2)
holoModel(2,Model)
holoPos(2,E:pos()+vec(0,0,Scale))
holoScale(2,vec(-Scale,-Scale,-Scale))
holoAlpha(2,150)
holoMaterial(2,"models/props_combine/stasisshield_sheet")
holoMaterial(1,"sprites/heatwave")
holoParent(2,E)

holoCreate(0)
holoModel(0,Model)
holoPos(0,E:pos()+vec(0,0,Scale))
holoScale(0,vec(-Scale,-Scale,-Scale))
holoAlpha(0,150)
holoMaterial(0,"models/alyx/emptool_glow")
holoParent(0,E)

holoCreate(3)
holoModel(3,Model)
holoPos(3,E:pos()+vec(0,0,Scale))
holoScale(3,vec(Scale-0.3,Scale-0.3,Scale-0.3))
holoAlpha(3,255)
holoMaterial(3,"models/shadertest/predator")
holoParent(3,E)

holoCreate(4)
holoModel(4,Model)
holoPos(4,E:pos()+vec(0,0,Scale))
holoScale(4,vec(Scale-0.2,Scale-0.2,Scale-0.2))
holoAlpha(4,255)
holoMaterial(4,"models/props_c17/fisheyelens")
holoParent(4,E)

holoCreate(5)
holoModel(5,Model)
holoPos(5,E:pos()+vec(0,0,Scale))
holoScale(5,vec(Scale-0.1,Scale-0.1,Scale-0.1))
holoAlpha(5,255)
holoMaterial(5,"models/props_combine/stasisshield_sheet")
holoParent(5,E)

holoCreate(20)
holoModel(20,Model)
holoPos(20,E:pos()+vec(0,0,Scale))
holoScale(20,vec(Scale,Scale,Scale))
holoAlpha(20,255)
holoMaterial(20,"models/props_combine/portalball001_sheet")
holoParent(20,E)


for(L = 0,5){
holoAlpha(L,0)
holoAlpha(20,0)
}

while(I<=Players:count()){
I2++
I++
holoCreate(I2)
holoModel(I2,"cube")
holoPos(I2,Players[I,entity]:pos()+vec(0,0,50))
holoScale(I2,vec(-7,-7,-7))
holoAlpha(I2,0)
holoColor(I2,vec(1,1,1))
holoParent(I2,Players[I,entity])   
#holoMaterial(I2,"models/alyx/emptool_glow")
holoMaterial(I2,"models/props_c17/paper01")
}
}


if(chatClk(owner())){             
if(Goto2[1,string]=="-on" & On == 0)
{  On = 1
    E:soundPlay(1,1999,"")
print("On")
}
if(Goto2[1,string]=="-off" & On == 1)
{  On = 0
   print("Off") 
soundStop(1)
}
}


if(On == 1){
for(L = 0,5){
holoAlpha(L,255)
holoAlpha(20,255)
}
}

if(On == 0){
for(L1 = 0,Players:count()){
holoAlpha(L1,0)
}
for(L2 = 1,Players:count()){
holoAlpha(L2+5,0)
}
}
T+=1
if(On == 1){
timer("up",500)

for(L4 = 0,5){
holoAng(L4,ang(T,T,T))
}

if(clk("up")){
    
for(I3 = 1,Players:count()){
    
if(Players[I3,entity]:pos():distance(E:pos()) <= Scale*7){
holoAlpha(I3+5,255) 
}else{
holoAlpha(I3+5,0)  
}

}
}
}

if(duped()){selfDestructAll()}
