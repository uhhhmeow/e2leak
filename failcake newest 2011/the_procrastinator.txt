@name The Procrastinator
@inputs Fast FastBack Forward Backward Record SlowGo SlowBack
@outputs PosCount
@persist BeenOn Go Reverse EA:array PA:array AA:array Count
runOnTick(1)

PosCount = PA:count()

if(Record&~Record&!BeenOn)
{
findExcludeEntities(entity():getConstraints())
findIncludePlayerProps(owner())    
findInSphere(entity():pos(),1000000000)
EA = findToArray()   
Count = 0
Go = 1
for(I=1,EA:count())
{
EA[I,entity]:propFreeze(0)    
}
}

#A[X+Y*SizeX]

if(Record)
{
Count++   
for(I=1,EA:count())
{
#if(ops()>5000){break}
PA[I+Count*EA:count(),vector] = EA[I,entity]:pos()
AA[I+Count*EA:count(),angle] = EA[I,entity]:angles()    
}
 
BeenOn=1
}

if(!Record&PA:count()>EA:count()*2&BeenOn)
{
for(I=1,EA:count())
{    
#if(ops()>5000){break}
EA[I,entity]:setPos(PA[I+Count*EA:count(),vector])
EA[I,entity]:setAng(AA[I+Count*EA:count(),angle])
} 
}

if(Backward&!Record)
{
if(Count>1)
{
Count--
}
}

if(Forward&!Record)
{
if(Count<(PA:count()/EA:count())-1)
{
Count++
}
}

if(SlowGo&!Record)
{
if(Count<(PA:count()/EA:count())-1)
{
timer("Go",100)
if(clk("Go"))
{
Count++
}
}
}

if(SlowBack&!Record)
{
if(Count>1)
{
timer("Back",100)
if(clk("Back"))
{
Count--
}
}
}

if(FastBack&!Record)
{
if(Count>1)
{
Count=Count-10
}
}

if(Fast&!Record)
{
if(Count<(PA:count()/EA:count())-1)
{
Count=Count+10
}
}
