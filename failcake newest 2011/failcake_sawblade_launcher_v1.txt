@name FailCake SawBlade Launcher V1
@inputs 
@outputs Ang T Ready Targ Once Ply:entity Ok Shoot Saw:entity Kill Mh Prop
@persist Vect:vector T2 Pos:vector Saws:array
@trigger all
@model 

runOnTick(1)
runOnChat(1)
E = entity()
O = owner()
Eye = O:eye()

if(first()){
holoCreate(1)
Mh = 255
Prop = 0
holoModel(1,"models/props_junk/sawblade001a.mdl")
holoPos(1,E:toWorld(vec(0,0,30)))
holoParent(1,E)
holoAlpha(1,0)  
holoEntity(1):soundPlay(100,100,"ambient/machines/spin_loop.wav")
}

holoAlpha(1,155) 


Goto = owner():lastSaid():explode(" ")


if(chatClk(owner())){ 
                
if(Goto[1,string]=="-target")
{      
    hideChat(1)
    E:soundPlay(34,45,"buttons/button9.wav")
    hint("Select the Target!",7)
    Targ = 1
    Ok = 0 
}else{

}

}

if(Targ == 1){
if(owner():keyUse() & Once == 0 & owner():aimEntity() != noentity() ){
Once = 1
Ply = owner():aimEntity()
Ok = 1
if(owner():aimEntity():isPlayer() | owner():aimEntity():isNPC()){
hint("Target Selected! Name = "+Ply:name(),7)  
}else{
hint("Target Selected! Name = "+Ply:model(),7)  
Prop = 1
}
}
}

if(O:keyAttack1() & O:keyAttack2() & Ready == 1 & Shoot == 0){
Saw = propSpawn("models/props_junk/sawblade001a.mdl",holoEntity(1):pos(),ang(0,0,0),0)
Saw:setTrails(10,10,3,"trails/physbeam",vec(255,255,255),255)
Saws:pushEntity(Saw)
Shoot = 1
}

if(Shoot){
    
T2+=10

Vect = (Ply:pos()-Saw:pos())
Saw:applyForce(Vect*200 + $Vect*200)    
TarQ = quat(ang(0,T2,0))
CurQ = quat(Saw)
Q = TarQ/CurQ
V = Saw:toLocal(rotationVector(Q)+Saw:pos())
Saw:applyTorque((900*V - 12*Saw:angVelVector())*Saw:inertia()) 

if(Ply == noentity() & Prop == 1){
Ply = noentity()
Shoot = 0
Ready = 0
Once = 0
Ok = 0
Targ = 0
Kill = 1  
Prop = 0 
} 
   
if(Ply:health() <= 0 & Prop == 0){
Ply = noentity()
Shoot = 0
Ready = 0
Once = 0
Ok = 0
Targ = 0
Kill = 1  
Prop = 0 
}

}

soundPitch(100,T*5)
E:setColor(T*20,T*10,0) 

if(O:keyAttack2() & Ok == 1){
   
if(T <= 30){
T+=0.1
}else{
Ready = 1
}

Ang+=T

holoAng(1,ang(0,Ang,0))
 
}else{

if(T <= 10){
Ready = 0    
}

if(T >= 0){
T-=0.1
Ang+=T
}else{
T = 0
Ang = T
}

holoAng(1,ang(0,Ang,0))

}


if(Kill){
Mh-=0.5
Saws[1,entity]:setColor(Mh,Mh,Mh,Mh)    

if(Mh <= 0){
Kill = 0
Saws[1,entity]:propDelete()
}

}
