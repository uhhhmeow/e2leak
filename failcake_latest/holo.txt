@name holo
@inputs 
@outputs Spawn S
@persist 
@trigger all
runOnTick(1)

if(first() | dupefinished())
{
    propSpawnEffect(0)
     PropMain = propSpawn("models/props/door_01_frame_reference.mdl",entity():pos() + vec(0,0,0),ang(0,0,0),1)
    Door1 = propSpawn("models/props/door_01_lftdoor_reference.mdl",entity():pos() + vec(0,0,0),ang(0,0,0),1)
Door2 = propSpawn("models/props/door_01_rtdoor_reference.mdl",entity():pos() + vec(0,0,0),ang(0,0,0),1)

}


if(findCanQuery())
{
    findIncludeClass("player")
    Find = findInSphere(entity():pos(),100)
}

if(changed(Find)) 
{
    if(Find)
    {Door1:setPos(entity():toWorld(vec(0,38,0)))}
    else{Door1:setPos(entity():toWorld(vec(0,0,0)))}
}

if(findCanQuery())
{
    findIncludeClass("player")
    Find = findInSphere(entity():pos(),125)
}

if(changed(Find)) 
{
    if(Find)
    {Door2:setPos(entity():toWorld(vec(0,-38,0)))}
    else{Door2:setPos(entity():toWorld(vec(0,0,0)))}
}

