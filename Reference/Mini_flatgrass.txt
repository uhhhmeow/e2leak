@name Mini flatgrass
@inputs 
@outputs 
@persist 
@trigger 
@model models/hunter/plates/plate8x16.mdl

holoCreate(1)
  holoModel(1,"models/hunter/plates/plate8x16.mdl")
    holoScale(1,vec(1,1,16.5))
    holoColor(1,vec(255,255,255))
    holoMaterial(1,"models/props/de_inferno/infflrd")


holoCreate(2)
holoModel(2,"models/hunter/plates/plate32x32.mdl")
holoScale(2,vec(4,4,0.5))
holoColor(2,vec(255,255,255))
holoMaterial(2,"phoenix_storms/ps_grass")
#holoColor(2,vec(32,255,6))

entity():setPos(entity():pos() + vec(0,0,30))
entity():setAlpha(0)


holoCreate(3)
holoModel(3,"square")
holoMaterial(3,"models/props/cs_office/clouds")
holoScale(3, vec(-9999,-9999,-99999))
holoParent(3,entity())



holoCreate(4)
holoModel(4,"square")
holoMaterial(4,"models/rendertarget")
holoScale(4, vec(9999999,9999999,9999999))
holoParent(4,entity())
