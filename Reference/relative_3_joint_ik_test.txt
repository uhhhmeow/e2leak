@name 
@persist  [ P1 P2 P3 ]:vector 
if( first( ) ) {
    
    for( I = 1, 4 ) {
        holoCreate( I, 
            entity():toWorld( vec( 15, 0, ( I - 1 ) * 20 ) ), 
            vec( 0.3 ), 
            entity():toWorld( ang( 0,0,0 ) ), 
            vec4( vec(255), 255 ), 
            "models/holograms/cube.mdl"
        )
        holoParent( I, I - 1 )
    }
    
    holoParent( 1, entity( ) )
    
    
    holoCreate( 5 )
    holoScale( 5, vec( 0.3 ) )
    holoColor( 5, vec( 255,0,0) )
    holoCreate( 6 )
    holoScale( 6, vec( 0.3 ) )
    holoColor( 6, vec( 255,0,0) )
    holoCreate( 7 )
    holoScale( 7, vec( 0.3 ) )
    holoColor( 7, vec( 255,0,0) )
    
    function entity:ik3( A, B, C, LengthA, LengthB, Destination:vector ) {
        
        Distance = This:pos():distance( Destination )
        Ae = max( Distance, LengthB )
        Be = LengthA
        Ce = min( Distance, LengthB )
        Calc = sqrt( ( (Ae + Be - Ce + Be) * (-Ae + Be + Ce + Be) * (Ae - Be - Ce + Be) * (Ae + Be - Ce - Be) ) / (4 * (Ae - Ce) ^ 2) )
        local Arcs = ( sign( Distance - LengthA ) + 1 ) / 2 ? asin( Calc / LengthA ) : acos( Calc / LengthA ) + 90
        
        holoAng( A, This:toWorld( ang( 0, -This:bearing( Destination ) - 90, This:elevation( Destination ) + Arcs - 90 ) ) )
        holoAng( B, holoEntity( A ):toWorld( ang( 0, 0, -Arcs ) ) )
        holoAng( C, holoEntity( B ):toWorld( ang( 0, 0, -Arcs ) ) )
    }
    
    P1 = entity():toWorld( vec( 20, -20, 0 ) )
    P2 = entity():toWorld( vec(  0, -20, 10 ) )
    P3 = entity():toWorld( vec( -20, -20, 0 ) )
}
interval(60)
holoEntity( 1 ):ik3( 1, 2, 3, 20, 20, bezier( P1, P2, P3, 0.5 + sin( realtime() * 50 ) * 0.5 ) )
holoPos( 5, P1 )
holoPos( 6, P2 )
holoPos( 7, P3 )
