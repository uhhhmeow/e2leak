@name It's hip to be cubic
@outputs XYZ:vector
@persist Size Reference:entity
@trigger 

if(first()){
    Size = 100
    # 0: reference cube
    Reference = holoCreate(0)
    holoParent(0, entity())
    holoScale(0, vec(Size / 12))
    holoAlpha(0, 100)
    # 1: normalized indicator
    # 2, 3, 4: X, Y, Z axis components
    for(I = 1, 4){
        holoCreate(I)
        holoColor(I, select(I, vec(1), vec(1, 0, 0), vec(0, 1, 0), vec(0, 0, 1)) * 255)
        holoParent(I, 0)
    }

}
interval(100)
if( owner( ):keyUse( ) ){
    local WorldVec = owner( ):shootPos( ) + owner( ):eye( ) * 1000
    local V = clamp( Reference:toLocal( WorldVec ):normalized( ) * sqrt( 3 ) * 2, vec( -1 ), vec( 1 ) ) * Size  / 2
    holoPos(1, Reference:toWorld(V))
    local YZ = V * vec(1, 0, 0)
    local XZ = V * vec(0, 1, 0)
    local XY = V * vec(0, 0, 1)
    holoPos(2, Reference:toWorld(YZ))
    holoPos(3, Reference:toWorld(XZ))
    holoPos(4, Reference:toWorld(XY))
    XYZ = V
}

