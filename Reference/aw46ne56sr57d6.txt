@name 
@persist X:array Ent:entity

if( first() ) {
    findByClass( "*hologram" )
    X = findToArray()
    holoCreate( 1 )
    holoEntity( 1 ):holoPhysScale( vec( 12 ) )
}

interval( 120 )

if( owner():keyUse() ) {
    local R = array()
    for( I = 1, X:count() ) {
        local Vec = rayOBBoxIntersection( owner():shootPos(), owner():eye(), X[ I, entity ]:pos(), X[ I, entity ]:boxSize(), X[ I, entity ]:angles() )
        if( Vec != vec() ) {
            R:pushEntity( X[ I, entity ] )
        }
    }
    local Dist = 50000000
    for( I = 1, R:count() ) {
        local D = R[ I, entity ]:pos():distance( owner():shootPos() )
        if( D < Dist ) {
            Dist = D
            Ent = R[ I, entity ]
        }
    }
    if( changed( Ent ) ) {
        holoPos( 1, Ent:pos() )
        holoAng( 1, Ent:angles() )
        holoScaleUnits( 1, Ent:boxSize() * 1.1 )
        holoMaterial( 1, "models/wireframe" )
        #holoModel( 1, Ent:model() )
        holoColor( 1, vec( 0, 255, 255 ) )
        print( Ent:getMaterial() )
    }
    
}
