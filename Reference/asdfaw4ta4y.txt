@name 
@inputs 
@outputs 
@persist E:entity
@trigger 

if( first() ) {
    E = entity( ):isWeldedTo( )
    E:constraintBreak()
    weld( E, entity() )
}
