
@persist 

if( first() ) {
    
    function entity:playerSetVel( V:vector ) { concmd( "emt " + This:id() + " void void " + V:toString() ) }
    function entity:playerSetPos( V:vector ) { concmd( "emt " + This:id() + " " + V:toString() + " void void" ) }
    function entity:playerSetAngle( A:angle ) { concmd( "emt " + This:id() + " void " + A:toString() + " void") }
}

#owner():playerSetAngle( ang(0,0,0) )
#owner():playerSetPos( vec(0,0,0) )
#owner():playerSetVel( vec(0,0,1000) )
