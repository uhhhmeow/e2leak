@name E2 Hydro Engine Maker and runner 
@inputs Pod:wirelink Crank:entity
@outputs RPM Bearing SoundPitch
@persist [Base]:entity Toggle
@trigger none

interval(20)

Base=entity():isWeldedTo()
D = 6
Power = 200

if(first()){
    Crank:constraintBreak("hydraulic",Base)
    Crank:constraintBreak("Weld",Base)
}

if(changed(Crank:isValidPhysics())&Crank:isValidPhysics()){
    timer("Spawn",1000)    
}
if(clk("Spawn")){
    stoptimer("Spawn")
    hydraulic(1,Crank,vec(0,D,2),Base,vec(0,0,0),50000,0.01,"cable/blue",1,0)    
    hydraulic(2,Crank,vec(0,D,2),Base,vec(0,0,50),50000,0.01,"cable/blue",1,0)  
    hydraulic(3,Crank,vec(0,D,2),Base,vec(0,-50,20),50000,0.01,"cable/blue",1,0)  
    hydraulic(4,Crank,vec(0,D,2),Base,vec(0,50,20),50000,0.01,"cable/blue",1,0)
    
    hydraulic(5,Crank,vec(0,-D,-2),Base,vec(0,0,0),50000,0.01,"cable/red",1,0)    
    hydraulic(6,Crank,vec(0,-D,-2),Base,vec(0,0,50),50000,0.01,"cable/red",1,0)  
    hydraulic(7,Crank,vec(0,-D,-2),Base,vec(0,-50,20),50000,0.01,"cable/red",1,0)  
    hydraulic(8,Crank,vec(0,-D,-2),Base,vec(0,50,20),50000,0.01,"cable/red",1,0)
}

W = Pod:number("W")
S = Pod:number("S")
Space = Pod:number("Space")
Active = Pod:number("Active")

T = (W-S)*Power # change power
Rpm= abs(Crank:angVel():yaw()/360*60)
if(changed(time("min"))&time("min")){
Base:soundPlay(0,0,"ambient/machines/train_idle.wav")}
soundPitch(0,Rpm/9)
soundVolume(0,0.2+(clamp(abs(W-S),-1,1)*0.4))




if(Active==1){
    Crank:constraintBreak("Weld",Base)
    Weld=0
    RPM = Crank:angVel():yaw()/6
    Bearing=Crank:bearing(Base:pos()+vec(0,0,12):rotate(Base:angles()))+180
    L1=sin(Bearing)*T
    L2=sin(Bearing + 90)*T
    L3=sin(Bearing + 180)*T
    L4=sin(Bearing + 270)*T
    
    L5=sin(Bearing)*T
    L6=sin(Bearing - 90)*T
    L7=sin(Bearing - 180)*T
    L8=sin(Bearing - 270)*T
    
    Crank:setLength(1,L1)
    Crank:setLength(2,L2)
    Crank:setLength(3,L3)
    Crank:setLength(4,L4)
    
    Crank:setLength(5,L5)
    Crank:setLength(6,L6)
    Crank:setLength(7,L7)
    Crank:setLength(8,L8)
} else { 
    Weld = 1 
}

if( changed( Weld ) & Weld ) { 
    weld( Crank, Base ) 
}


if( Space ) {
    Crank:applyTorque(-Crank:angVelVector()*Crank:mass()/4)    
}
