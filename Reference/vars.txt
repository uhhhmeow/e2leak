
@persist VAR:table Me:entity Chip:entity

if( first() ) {
    
    VAR = table()
    Me = owner()
    Chip = entity()
    
    function string string:s() { return VAR[This, string] }
    function string:s( STRING:string ) { VAR[This, string] = STRING }
    function vector string:v() { return VAR[This, vector] }
    function string:v( VECTOR:vector ) { VAR[This, vector] = VECTOR }
    function vector2 string:xv2() { return VAR[This, vector2] }
    function string:xv2( VECTOR:vector2 ) { VAR[This, vector2] = VECTOR }
    function vector4 string:xv4() { return VAR[This, vector4] }
    function string:xv4( VECTOR:vector4 ) { VAR[This, vector4] = VECTOR }
    function matrix string:m() { return VAR[This, matrix] }
    function string:m( MATRIX:matrix ) { VAR[This, matrix] = MATRIX }
    function matrix2 string:xm2() { return VAR[This, matrix2] }
    function string:xm2( MATRIX:matrix2 ) { VAR[This, matrix2] = MATRIX }
    function matrix4 string:xm4() { return VAR[This, matrix4] }
    function string:xm4( MATRIX:matrix4 ) { VAR[This, matrix4] = MATRIX }
    function angle string:a() { return VAR[This, angle] }
    function string:a( ANGLE:angle ) { VAR[This, angle] = ANGLE }
    function quaternion string:q() { return VAR[This, quaternion] }
    function string:q( QUATERNION:quaternion ) { VAR[This, quaternion] = QUATERNION }
    function array string:r() { return VAR[This, array] }
    function string:r( ARRAY:array ) { VAR[This, array] = ARRAY }
    function table string:t() { return VAR[This, table] }
    function string:t( TABLE:table ) { VAR[This, table] = TABLE }
    function bone string:b() { return VAR[This, bone] }
    function string:b( BONE:bone ) { VAR[This, bone] = BONE }
    function complex string:c() { return VAR[This, complex] }
    function string:c( COMPLEX:complex ) { VAR[This, complex] = COMPLEX }
    function wirelink string:xwl() { return VAR[This, wirelink] }
    function string:xwl( WIRELINK:wirelink ) { VAR[This, wirelink] = WIRELINK }
    function ranger string:xrd() { return VAR[This, ranger] }
    function string:xrd( RANGER:ranger ) { VAR[This, ranger] = RANGER }
    function entity string:e() { return VAR[This, entity] }
    function string:e( ENTITY:entity ) { VAR[This, entity] = ENTITY }
    function number string:n() { return VAR[This, number] }
    function string:n( NUMBER:number ) { VAR[This, number] = NUMBER }
    function string string:f() { return VAR[This, string] }
    function string:f( FUNCTION:string ) { VAR[This, string] = FUNCTION }
    
    "me":e( owner() )
    "chip":e( entity() )
    
    #[
    # Lets set a global variable without using @persist!
    "myvar":v( vec() )
    
    # Now lets get the contents
    print( "myvar":v() )
    
    # Now lets set it to something else
    "myvar":v( vec(255) )
    
    # Now lets get the contents again
    print( "myvar":v() )
    ]#
}

