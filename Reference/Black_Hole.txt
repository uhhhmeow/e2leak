@name Black Hole
#@model models/Combine_Helicopter/helicopter_bomb01.mdl
@persist X Y
#exit() # Remove this
if(first()){
    X = 0
    Y = realtime( ) + 120
}

interval( 60 )

if(clk()){
    if( Y > realtime( ) ) {
        if( holoCanCreate( ) ) {
            X++
            holoCreate(X):setTrails(1000, 1000, 1, "trails/lol", vec( randint(0,255),randint(0,255),randint(0,255) ), 255)
            holoParent(X, entity())
            holoAlpha(X, 0)
        }
        for(I = 1, X){
            #holoPos(I, entity():pos() + randvec() * randint(0, 100000 ))
            holoPos(I, owner( ):shootPos( ) + randvec() * randint(0, 100 ))
        }
    }
}
