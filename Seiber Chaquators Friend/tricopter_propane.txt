@name 
@inputs Pod:wirelink Body:entity
@outputs Soundpitch
@persist RP RR RY Rot Tilt Prop:entity Recoil
runOnTick(1)
Body = entity():isWeldedTo()
E = entity()
if(first()){
Body:setAlpha(0)
Scale = 1.3
Body:setMass(500)
}
W = Pod["W",number]
A = Pod["A",number]
S = Pod["S",number]
D = Pod["D",number]
R = Pod["R",number]
Space = Pod["Space",number]
Shift = Pod["Shift",number]
M1 = Pod["Mouse1",number]
M2 = Pod["Mouse2",number]
Active = Pod["Active",number]
Alt = Pod["Alt",number]
Driver = Pod["Entity",entity]:driver()

if(changed(Active))
{
    if(Active)
    {
        soundPlay("rotor",0,"NPC_AttackHelicopter.Rotors")
    }else
    {
        soundStop("rotor")
    }
}
Soundpitch = clamp(Body:vel():length()/20, 140, 160)
soundPitch("rotor",Soundpitch)



if(changed(R)&R){
    propSpawnEffect(0)
    Prop = propSpawn("models/props_junk/propane_tank001a.mdl",holoEntity(17):toWorld(vec(0,0,100)),holoEntity(17):toWorld(ang(0,0,0)),0)
    Body:soundPlay(1,5,"/mvm/giant_demoman/giant_demoman_grenade_shoot.wav")
    Prop:propGravity(0)
    Prop:setMass(50000)
    Prop:setTrails(10,0,1,"trails/smoke",vec(255,255,255),255)
    Prop:applyForce(Prop:up()*Prop:mass()*50000)
}


if(Alt==1){Prop:propBreak()}

rangerFilter(Prop)
rangerFilter(Body)
Ranger = rangerOffset(50,Prop:toWorld(vec(0,0,0)),Prop:up():rotate(Prop:angles()))
if(Ranger:hit()){Prop:propBreak()}



RP = (W-S)*40+(2.5*(Space==0))-abs(Body:vel():dot(Body:forward())/150+(entity():angles():roll()/-3))
RR = (A-D)*40
RY = (M1-M2)*10+(Body:vel():dot(Body:forward())/100+(entity():angles():roll()/-20))

Force = 40
Anglo = ang(-Body:angVel():pitch()+RR,-Body:angVel():yaw()/5+RY,-Body:angVel():roll()+RP) * Force * shiftL(ang(Body:inertia()))
Body:applyAngForce(Anglo)


Body:applyForce(Body:up()*Body:mass()*9.2*Active)
Body:applyForce(Body:up()*Body:mass()*(Space-Shift)*5)
Body:applyForce(Body:up()*Body:mass()*Body:vel():z()/-100*(abs(entity():angles():pitch())<90))

if(Active==1){Rot+=40} else {Rot+=0}

if(M2==1){Tilt=30} else {Tilt=0}
if(M1==1){Tilt=-30}

if(first()){
    #base
    holoCreate(-2)
    holoModel(-2,"hq_dome")
    holoScale(-2,vec(3,5,4.5)*Scale)
    holoPos(-2,Body:toWorld(vec(0,-15,0)*Scale)) 
    holoAng(-2,Body:toWorld(ang(0,0,0)))
    holoParent(-2,Body)
    
    holoCreate(-1)
    holoModel(-1,"hq_sphere")
    holoScale(-1,vec(2,3,2)*Scale)
    holoPos(-1,Body:toWorld(vec(0,35,10)*Scale)) 
    holoAng(-1,Body:toWorld(ang(0,0,0)))
    holoParent(-1,Body)
    
    holoCreate(0)
    holoModel(0,"hq_cylinder")
    holoScale(0,vec(2,2,3)*Scale)
    holoPos(0,Body:toWorld(vec(0,15,10)*Scale)) 
    holoAng(0,Body:toWorld(ang(0,0,90)))
    holoParent(0,Body)
    
    holoCreate(1)
    holoModel(1,"hq_sphere")
    holoScale(1,vec(4,8,1)*Scale)
    holoPos(1,Body:toWorld(vec(0,0,0)*Scale)) 
    holoAng(1,Body:toWorld(ang(0,0,0)))
    holoParent(1,Body)
    
    #arm back
    holoCreate(2)
    holoModel(2,"hq_rcube_thin")
    holoScale(2,vec(0.3,0.3,3.9)*Scale)
    holoPos(2,Body:toWorld(vec(0,60,0)*Scale))   
    holoAng(2,Body:toWorld(ang(0,0,90)))
    holoParent(2,Body)
    
    #arm left
    holoCreate(3)
    holoModel(3,"hq_rcube_thin")
    holoScale(3,vec(0.3,4,0.3)*Scale)
    holoPos(3,Body:toWorld(vec(30,-40,0)*Scale))   
    holoAng(3,Body:toWorld(ang(0,45,0)))
    holoParent(3,Body)
    
    #arm right
    holoCreate(4)
    holoModel(4,"hq_rcube_thin")
    holoScale(4,vec(0.3,4,0.3)*Scale)
    holoPos(4,Body:toWorld(vec(-30,-40,0)*Scale))   
    holoAng(4,Body:toWorld(ang(0,-45,0)))
    holoParent(4,Body)
    
    #arm left rotor ring
    holoCreate(5)
    holoModel(5,"hq_tube_thin")
    holoScale(5,vec(2.5,2.5,0.3)*Scale)
    holoPos(5,Body:toWorld(vec(57,-67,0)*Scale))   
    holoAng(5,Body:toWorld(ang(0,0,0)))
    holoParent(5,Body)
    
    #arm right rotor ring
    holoCreate(6)
    holoModel(6,"hq_tube_thin")
    holoScale(6,vec(2.5,2.5,0.3)*Scale)
    holoPos(6,Body:toWorld(vec(-57,-67,0)*Scale))   
    holoAng(6,Body:toWorld(ang(0,0,0)))
    holoParent(6,Body)
    
    #arm back rotor ring
    holoCreate(7)
    holoModel(7,"hq_tube_thin")
    holoScale(7,vec(2.5,2.5,0.3)*Scale)
    holoPos(7,Body:toWorld(vec(0,97,0)*Scale))   
    holoParent(7,Body)
    
    #arm back rotor center
    holoCreate(8)
    holoModel(8,"hq_cylinder")
    holoScale(8,vec(0.3,0.3,0.3)*Scale)
    holoPos(8,holoEntity(7):toWorld(vec(0,0,0)*Scale))   
    holoParent(8,7)
    
    #arm right rotor center
    holoCreate(9)
    holoModel(9,"hq_cylinder")
    holoScale(9,vec(0.3,0.3,0.3)*Scale)
    holoPos(9,holoEntity(6):toWorld(vec(0,0,0)*Scale))   
    holoParent(9,6)
    
    #arm left rotor center
    holoCreate(10)
    holoModel(10,"hq_cylinder")
    holoScale(10,vec(0.3,0.3,0.3)*Scale)
    holoPos(10,holoEntity(5):toWorld(vec(0,0,0)*Scale))   
    holoParent(10,5)    
    
    #arm back rotor blade
    holoCreate(11)
    holoModel(11,"hq_sphere")
    holoScale(11,vec(2.5,0.3,0.05)*Scale)
    holoPos(11,holoEntity(8):toWorld(vec(0,0,0)*Scale))   
    holoAng(11,holoEntity(8):toWorld(ang(0,0,0)))
    holoParent(11,8)
    
    #arm right rotor blade
    holoCreate(12)
    holoModel(12,"hq_sphere")
    holoScale(12,vec(2.5,0.3,0.05)*Scale)
    holoPos(12,holoEntity(9):toWorld(vec(0,0,0)*Scale))   
    holoAng(12,holoEntity(9):toWorld(ang(0,0,0)))
    holoParent(12,9)
    
    #arm left rotor blade
    holoCreate(13)
    holoModel(13,"hq_sphere")
    holoScale(13,vec(2.5,0.3,0.05)*Scale)
    holoPos(13,holoEntity(10):toWorld(vec(0,0,0)*Scale))   
    holoAng(13,holoEntity(10):toWorld(ang(0,0,0)))
    holoParent(13,10)
    
    #turret
    holoCreate(15)
    holoModel(15,"hq_sphere")
    holoScale(15,vec(1,1,1)*Scale)
    holoPos(15,Body:toWorld(vec(0,-45,0)*Scale))   
    holoParent(15,Body)
    
    holoCreate(16)
    holoModel(16,"hq_cylinder")
    holoScale(16,vec(0.7,0.7,1)*Scale)
    holoPos(16,holoEntity(15):toWorld(vec(5,0,0)*Scale))   
    holoAng(16,holoEntity(15):toWorld(ang(90,0,0)))
    holoParent(16,15)
    
    holoCreate(17)
    holoModel(17,"hq_tube")
    holoScale(17,vec(0.6,0.6,2)*Scale)   
    holoAng(17,holoEntity(15):toWorld(ang(90,0,0)))
    holoPos(17,holoEntity(15):toWorld(vec(8,0,0)*Scale))
    holoParent(17,15)
    
    #wingletts
    holoCreate(18)
    holoModel(18,"hq_cube")
    holoScale(18,vec(0.1,1,3)*Scale)
    holoPos(18,Body:toWorld(vec(15,30,20)*Scale)) 
    holoAng(18,Body:toWorld(ang(45,0,0)))
    holoParent(18,Body)
    
    holoCreate(19)
    holoModel(19,"hq_cube")
    holoScale(19,vec(0.1,1,3)*Scale)
    holoPos(19,Body:toWorld(vec(-15,30,20)*Scale)) 
    holoAng(19,Body:toWorld(ang(-45,0,0)))
    holoParent(19,Body)
    
    holoCreate(20)
    holoModel(20,"hq_cube")
    holoScale(20,vec(0.1,1,3)*Scale)
    holoPos(20,Body:toWorld(vec(14.3,23,19.3)*Scale)) 
    holoAng(20,Body:toWorld(ang(45,0,-22)))
    holoParent(20,Body)
    
    holoCreate(21)
    holoModel(21,"hq_cube")
    holoScale(21,vec(0.1,1,3)*Scale)
    holoPos(21,Body:toWorld(vec(-14.3,23,19.3)*Scale)) 
    holoAng(21,Body:toWorld(ang(-45,0,-22)))
    holoParent(21,Body)
    
    holoMaterial(-2,"models/shiny")
    holoColor(-2,vec(75,75,75))
    holoMaterial(-1,"models/gibs/metalgibs/metal_gibs")
    holoMaterial(0,"models/gibs/metalgibs/metal_gibs")
    holoMaterial(1,"models/gibs/metalgibs/metal_gibs")
    holoMaterial(2,"models/gibs/metalgibs/metal_gibs")
    holoMaterial(3,"models/gibs/metalgibs/metal_gibs")
    holoMaterial(4,"models/gibs/metalgibs/metal_gibs")
    holoMaterial(5,"models/gibs/metalgibs/metal_gibs")
    holoMaterial(6,"models/gibs/metalgibs/metal_gibs")
    holoMaterial(7,"models/gibs/metalgibs/metal_gibs")
    holoMaterial(8,"models/gibs/metalgibs/metal_gibs")
    holoMaterial(9,"models/gibs/metalgibs/metal_gibs")
    holoMaterial(10,"models/gibs/metalgibs/metal_gibs")
    holoMaterial(15,"models/gibs/metalgibs/metal_gibs")
    holoMaterial(16,"models/gibs/metalgibs/metal_gibs")
    holoMaterial(17,"models/gibs/metalgibs/metal_gibs")
    holoMaterial(18,"models/gibs/metalgibs/metal_gibs")
    holoMaterial(19,"models/gibs/metalgibs/metal_gibs")
    holoMaterial(20,"models/gibs/metalgibs/metal_gibs")
    holoMaterial(21,"models/gibs/metalgibs/metal_gibs")
}
#turret
holoAng(15,Driver:eyeAngles())

#arm back rotor ring pivot
holoAng(7,Body:toWorld(ang(Tilt,0,0)))

#Rotor rotation
holoAng(8,holoEntity(7):toWorld(ang(0,Rot,0)))
holoAng(9,holoEntity(6):toWorld(ang(0,Rot,0)))
holoAng(10,holoEntity(5):toWorld(ang(0,Rot,0)))

#Aim

Ranger2 = rangerOffset(9999999999,holoEntity(17):toWorld(vec(0,0,0)),holoEntity(17):up())
if(first()){
holoCreate(14)
holoAng(14,ang(0,0,0))
holoColor(14,vec(255,0,0))
holoModel(14,"hq_sphere")
holoScale(14,vec(3,3,3))
}
holoPos(14,Ranger2:position())
holoVisible(14,players(),0)
holoVisible(14,Driver,1)
if(dupefinished()){reset()}

##newb vtol protect
if(changed(owner():lastSaidWhen())){
   
    if(owner():lastSaid() == "!nop"){
         hideChat(1)
        LOL=propSpawn("models/props_phx/ww2bomb.mdl",Body:toWorld(vec(0,0,0)),entity():toWorld(ang(0,0,0)),0)
        LOL:propBreak()
    }
}
##newb vtol protect
