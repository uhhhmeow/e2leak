@name Snek?
@persist E:entity OldP:vector Pos:array Cnt

interval(10)

if(first()) {
    E = entity()
    
    OldP = E:pos()
    
    Cnt = 50
    
    for(I = 1, Cnt) {
        holoCreate(I)   
        holoModel(I, "hq_sphere")
        holoScale(I, vec(2))
        holoDisableShading(I, 1)
    }
}

if(E:pos():distance(OldP) > 20){
    Pos:pushVector(E:pos())
    OldP = E:pos()
    
    if(Pos:count() > Cnt){
        Pos:remove(1)
    }

}

for(K = 1, Cnt){
    holoPos(K, Pos[K, vector])
}

