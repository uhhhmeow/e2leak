@name Rocket Pod
@inputs Fire Reload
@outputs Ammo
@persist MaxAmmo Enabled Lock SInt Drop
@persist [Rockets]:array
@persist Temp:entity
@model models/hunter/blocks/cube025x1x025.mdl
if(first())
{
    MaxAmmo = 15
    Ammo = MaxAmmo
    Enabled = 1
    runOnTick(1)
    
    holoCreate(1,entity():toWorld(vec(-6,0,6)),vec(1.2,1.2,4),entity():toWorld(ang(0,0,90)),vec(150))
    holoModel(1,"hq_cylinder")
    holoParent(1,entity())
    holoMaterial(1,"models/shiny")
    
    holoCreate(2,entity():toWorld(vec(-6,-1,6)),vec(1,1,4),entity():toWorld(ang(0,0,90)),vec(100))
    holoModel(2,"hq_cylinder")
    holoParent(2,entity())
    holoMaterial(2,"models/shiny")
    
    SInt=1
Drop = 0
    
    entity():setColor(255,255,255,0)
    #selfDestruct()
}

if(Enabled)
{
    if(Fire&!Lock)
    {
        if(Ammo>0)
        {
            Temp = propSpawn("models/props_phx/ww2bomb.mdl",entity():toWorld(vec(-6,20,0)),entity():toWorld(ang(0,90,0)),1)
            Temp:setMaterial("models/debug/debugwhite")
            Temp:setColor(150,150,150,255)
            Temp:setTrails(25,3,2,"trails/smoke",vec(255),255)
            Rockets:pushEntity(Temp)
            Ammo--
            Lock = 1
            soundPlay(SInt,0.8,"/mvm/giant_demoman/giant_demoman_grenade_shoot.wav")
            SInt++
            timer("ub",400)
        }
    }
}

if(clk("ub"))
{
    stoptimer("ub")
    Lock = 0
}

AmmoNull = changed(Ammo)&Ammo==0&Ammo!=MaxAmmo
ReloadNull = changed(Reload)&Reload&Ammo!=MaxAmmo

if(AmmoNull||ReloadNull)
{
    Enabled = 0
    timer("reload",1500)
    soundPlay("reload",5,"/buttons/button8.wav")
}

if(clk("reload"))
{
    stoptimer("reload")
    Enabled = 1
    Ammo = MaxAmmo
    soundPlay("ready",3,"/buttons/button4.wav")
}

if(Rockets:count()>0)
{
    for(I=1,Rockets:count())
    {
        ForE = Rockets[I,entity]
        ForE:setPos(ForE:pos()+ForE:forward()*250)
ForE:setAng(ForE:toWorld(ang(-Drop,0,0)))
        ForR = rangerOffset(400,ForE:pos(),ForE:forward())
        if(ForR:hit()||!ForE:isValid())
        {
            ForE:propBreak()
            Rockets:remove(I)
        }
    }
}

if(duped()|dupefinished())
{
    reset()
}
