@name Magical Meteorite
@inputs 
@outputs 
@persist Reload Prop:entity 
@trigger
@model models/props_lab/bigrock.mdl

interval(10)

Reload = owner():keyReload()
A = random(255),random(666),random(777),(1000)

if(Reload&$Reload)
{
    soundPlay(1,10,"common/wpn_select.wav")
    Prop = propSpawn("models/props_forest/cliff_wall_02c.mdl",owner():pos()+vec(0,0,70)+owner():eye()*30,owner():eyeAngles(),0)
    Prop:setMass(50000)
    Prop:applyForce(Prop:forward()*5000*Prop:mass())
    Prop:setTrails(10,0,10,"trails/smoke",vec(A),255)
    Prop:setColor(random(255),random(666),random(777),(1000))
    Prop:setMaterial("models/debug/debugwhite")
}
