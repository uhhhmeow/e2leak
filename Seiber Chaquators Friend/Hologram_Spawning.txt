@persist TotalHolograms I E:entity

if(first()){
    runOnTick(1)
    E=entity()
    TotalHolograms = 2
}
while(perf()&holoCanCreate()&I<TotalHolograms){
    I++
    holoCreate(I)
    
    if(I>=TotalHolograms){

        holoPos(1,E:pos())
        holoParent(1,E)
        
        holoPos(2,E:pos()+vec(0,0,15))
        holoParent(2,E)
        
        interval(100)
        break
    }
}

