@name Quadruped_V2

@persist [Base]:entity OnGround Cycle Step_Height
@persist [FR_AP FL_AP BR_AP BL_AP]:vector #after step position
@persist [FR_FP FL_FP BR_FP BL_FP]:vector #foot position
@persist [Lead]:vector

@model models/maxofs2d/cube_tool.mdl

interval(50)

if(first()){
    Base = entity()
    Base:setMass(500)
    Base:propDrag(0)
    Base:propGravity(0)
    
    for(I = 1, 4){
        holoCreate(I)
    }
    
    Step_Height = 30
    
    #Functions#
    function vector bezier_curve(From:vector, Middle:vector, To:vector, Frac){
        return mix(mix(To, Middle, Frac), mix(Middle, From, Frac), Frac)
    }
    ##
}

Lead = clamp(Base:vel() * 0.2, 0, 500)

#Rangers#
rangerFilter(Base)
FR = rangerOffset(200, Base:toWorld(vec(50, -35, 80)) + Lead, -Base:up())
FL = rangerOffset(200, Base:toWorld(vec(50, 35, 80)) + Lead, -Base:up())
BR = rangerOffset(200, Base:toWorld(vec(-50, -35, 80)) + Lead, -Base:up())
BL = rangerOffset(200, Base:toWorld(vec(-50, 35, 80)) + Lead, -Base:up())
##

OnGround = FL:hit() & FR:hit() & BL:hit() & BR:hit() & abs(Base:angles():pitch()) < 70 & abs(Base:angles():roll()) < 70

if(OnGround){
    Cycle += 0.05
    Cycle = Cycle % 1
    
    Step1 = inrange(Cycle, 0, 0.25)
    Step2 = inrange(Cycle, 0.25, 0.5)
    Step3 = inrange(Cycle, 0.5, 0.75)
    Step4 = inrange(Cycle, 0.75, 1)
    
        if(Step1){
            BR_FP = bezier_curve(BR_AP, (BR_AP + BR:position()) / 2 + Base:up() * Step_Height, BR:position(), Cycle / 0.25)
        }
        if(changed(Step1) & !Step1){
            BR_AP = BR_FP   
        }
        
        if(Step2){
            FR_FP = bezier_curve(FR_AP, (FR_AP + FR:position()) / 2 + Base:up() * Step_Height, FR:position(), (Cycle - 0.25) / 0.25)
        }
        if(changed(Step2) & !Step2){
            FR_AP = FR_FP   
        }
        
        if(Step3){
            BL_FP = bezier_curve(BL_AP, (BL_AP + BL:position()) / 2 + Base:up() * Step_Height, BL:position(), (Cycle - 0.5) / 0.25)
        }
        if(changed(Step3) & !Step3){
            BL_AP = BL_FP   
        }
        
        if(Step4){
            FL_FP = bezier_curve(FL_AP, (FL_AP + FL:position()) / 2 + Base:up() * Step_Height, FL:position(), (Cycle - 0.75) / 0.25)
        }
        if(changed(Step4) & !Step4){
            FL_AP = FL_FP   
        }   
} 

holoPos(1, FR_FP)
holoPos(2, FL_FP)
holoPos(3, BR_FP)
holoPos(4, BL_FP)
