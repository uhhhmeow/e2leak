@name 
@inputs
@outputs 
@persist [O Ammo]:entity [Rockets Timers]:array Loaded T AmmoPos:vector Mix Wait
@trigger 
interval(10)
T++
if(first()){
    O = owner()
    
    Loaded = 0
    
}

if(!Loaded&!Wait&O:weapon():type() == "weapon_physgun"){
    findIncludeModel("models/weapons/w_missile_closed.mdl")
    findInSphere(O:pos(),75)
    
    if(find():owner() == owner()){
        Ammo = findClosest(O:pos())
    }
}
if(changed(Ammo:isValid())&Ammo:isValid()){
    AmmoPos = Ammo:pos()
}
if(Ammo:isValid()){
    Mix+=0.05
    
    Ammo:propFreeze(1)
    Ammo:propNotSolid(1)
    Ammo:setAng(ang(0,Mix*10,0)+Ammo:angles())
    Ammo:setPos(mix(O:attachmentPos("anim_attachment_RH"), AmmoPos, Mix))
    
    if(Mix>=1){
        Mix = 0
        O:soundPlay(4,0.162,"weapons/c4/c4_plant.wav")
        Ammo:propBreak()
        Loaded = 1
    }
}

Fire = O:keyUse() & O:keyDuck() & O:weapon():type() == "weapon_physgun"

if(changed(Fire)&Fire){
    if(Loaded){
        propSpawnEffect(0)
    
        Prop = propSpawn("models/props_phx/ww2bomb.mdl",O:attachmentPos("anim_attachment_RH")+(O:attachmentPos("anim_attachment_RH") - O:aimPos()):normalized()*-50,O:eyeAngles(),1)
        Prop:setTrails(5,150,2,"trails/smoke",vec(175,175,175),175)
        Prop:setAlpha(0)
        Prop:soundPlay(1,0,"thrusters/rocket04.wav")
    
        O:soundPlay(2,0.97,"weapons/ar2/npc_ar2_altfire.wav")
    
        Rockets:pushEntity(Prop)
        Timers:pushNumber(150)
    
        Loaded = 0
        
        Wait = 1
        timer("wait",1500)
    } else {
        O:soundPlay(3,0.188,"weapons/shotgun/shotgun_empty.wav")
    }
    
}
if(clk("wait")){
    Wait = 0
}

rangerFilter(Rockets)
rangerFilter(O)

foreach(I, Rocket:entity = Rockets){
    Rocket:setPos(Rocket:toWorld(vec(fromUnit("m",182*(10/1000)),0,0)))
    Rocket:setAng(clamp(Rocket:toWorld(ang(1/24, 0, 0)), ang(-90, -360, 0), ang(90, 360, 0))+ang(cos(T*10)*0.1, sin(T*10)*0.1, 0))
    
    holoCreate(I)
    holoPos(I,Rocket:toWorld(vec(0,0,0)))
    holoAng(I,Rocket:toWorld(ang(0,0,0)))
    holoParent(I,Rocket)
    holoModel(I,"models/weapons/w_missile.mdl")
    holoScale(I,vec(1.5,1.5,1.5))
    
    holoCreate(I+1)
    holoPos(I+1,holoEntity(I):toWorld(vec(-10,0,0)))
    holoAng(I+1,holoEntity(I):toWorld(ang(0,0,0)))
    holoParent(I+1,holoEntity(I))
    holoModel(I+1,"models/buildables/sentry3_rockets.mdl")
    holoScale(I+1,vec(1,0,0))
    holoAlpha(I+1,0)
    
    Timers[I,number] = Timers[I,number] - 1
    
    Hit = rangerOffset(150,Rocket:toWorld(vec(0,0,0)),Rocket:forward())
    if(Hit:hit() | Timers[I,number]<=0){
        Rockets:remove(I)
        Timers:remove(I)
        Blast = propSpawn("models/props_phx/mk-82.mdl",Rocket:pos(),ang(),1)
        Blast:setAlpha(0)
        Blast:propBreak()
        Rocket:propBreak()
    }
}

#removing non-existant rockets from array
for(Item = 1, Rockets:count()){
    if(!Rockets[Item,entity]:isValid()){
        Rockets:remove(Item)
        Timers:remove(Item)
    }   
}
