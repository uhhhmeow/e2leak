@name 
@inputs 
@outputs 
@persist LegCount Separation
@trigger 
if(first()){
    interval(100)
    
    LegCount = 3
    Separation = 360/LegCount
    
    for(Holo = 1, LegCount){
        holoCreate(Holo)
    }
    
}


