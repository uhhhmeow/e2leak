@name Cyber's Prop Protector
@persist [E O]:entity Iterator [Whitelist Props]:array PropCount Select SelectMode Door Open
if(first()){
    runOnTick(1)
    runOnChat(1)
    
    E = entity()
    O = owner()
    
    Whitelist:pushEntity(O)
    
#    E:propNotSolid(1)
#    E:setAlpha(0)
    
    print("Type \"select\" to enable selection, \"!select\" to disable selection, and \"setdoor\" to set the door!")

function number array:validate(E:entity)
{
if(!This:count())
{
return 0
}

Val = 0
foreach(I, TE:entity = This)
{
if(TE == E)
{
Val = I
}
}
return Val
}
}

if(O:lastSaid() == "select"){
    SelectMode = 1
    hideChat(1)
}
if(O:lastSaid() == "!select"){
    SelectMode = 0
    hideChat(1)
}
if(changed(SelectMode)&SelectMode){
    print("Press E to select props!")
}
if(changed(SelectMode)&!SelectMode){
    print("Selecting disabled!")
}

Select = changed(O:keyUse())&O:keyUse()
if(Select & SelectMode){
    Props:pushEntity(O:aimEntity())
    print(PropCount+1)
    PropCount = Props:count()
    
    holoCreate(PropCount,O:aimEntity():pos(),vec(1,1,1),O:aimEntity():angles())
    holoModel(PropCount,O:aimEntity():model())
    holoMaterial(PropCount,O:aimEntity():getMaterial())
    holoColor(PropCount,O:aimEntity():getColor())
    holoAlpha(PropCount,0)
}

Iterator++
if(Iterator > PropCount){Iterator = 1} 

if(PropCount>0&propCanCreate()&(!Props[Iterator, entity]:isValid()|!Props[Iterator, entity]:isFrozen())){
    Props[Iterator, entity]:propBreak()
    Props[Iterator, entity] = propSpawn(holoEntity(Iterator):model(),holoEntity(Iterator):pos(),holoEntity(Iterator):angles(),1)
    Props[Iterator, entity]:setMaterial(holoEntity(Iterator):getMaterial())    
}

Props[Iterator, entity]:setAlpha(255-(155*SelectMode))
Props[Iterator, entity]:setColor(holoEntity(Iterator):getColor()-vec(255,0,255)*SelectMode)
Props[Iterator, entity]:propNotSolid(SelectMode)


if(chatClk(O)){   
    if(Props:validate(O:aimEntity())&O:lastSaid() == "setdoor"){
        hideChat(1)
        Door = Props:validate(O:aimEntity())
    }
}
S = owner():lastSaid():explode(" ")
if(S[1,string]=="add")
{
local TE = findPlayerByName(S[2,string])
foreach(I, Ply:entity = Whitelist)
{
if(Ply==TE)
{
Validate = 0
break
}else
{
Validate = 1
}
}
if(Validate)
{
Whitelist:pushEntity(TE)
}
}

if(S[1,string]=="remove")
{
hideChat(1)
local TE = findPlayerByName(S[2,string])
foreach(I, Ply:entity = Whitelist)
{
if(Ply==TE)
{
Whitelist:remove(I)
}
}
}
foreach(I, TE:entity = Whitelist)
{
if(lastSpoke()==TE)
{
Validate = 1
break
}else
{
Validate = 0
}
}

if(Validate)
{
    if(lastSpoke():lastSaid()=="open"){Open = 1}
    if(lastSpoke():lastSaid()=="close"){Open = 0}
    
}
if(chatClk(O)){if(O:lastSaid()=="showlist"){hideChat(1), print(Whitelist)}}

if(Open == 1){
    Props[Door,entity]:propNotSolid(1)
    Props[Door,entity]:setAlpha(0)
}
if(Open == 0){
    Props[Door,entity]:propNotSolid(0)
    Props[Door,entity]:setAlpha(255)
}
