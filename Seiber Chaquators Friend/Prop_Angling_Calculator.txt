@name 
@inputs 
@outputs 
@persist P1:vector P2:vector O:entity
@trigger 
if(first()){
    runOnTick(1)
    O = owner()
}
if(changed(O:lastSaid() == "p1")&O:lastSaid() == "p1"){
    P1 = O:aimPos()
}
if(changed(O:lastSaid() == "p2")&O:lastSaid() == "p2"){
    P2 = O:aimPos()
}
if(changed(O:lastSaid() == "print")&O:lastSaid() == "print"){
    print((P1-P2):toAngle():pitch())
}
