@name Cyber's Replicator
@persist [Props Position Angle Model Material Color Alpha]:array SelectMode Select Paste Pasting I J Radius A
interval(10)

if(first()){
#    entity():setAlpha(0)
#    entity():propNotSolid(1)
    runOnChat(1)    
    
    Radius = 500
            
    holoCreate(9001)
    holoAlpha(9001, 0)
    
    holoCreate(9002)
    holoAlpha(9002, 0)
    holoParent(9002,9001)
}

if(chatClk(owner()))
{
local LS = owner():lastSaid():lower():explode(" ")
if(LS[1,string]=="radius")
{ 
    hideChat(1)
    Radius = LS[2,string]:toNumber()
    }
}
if(first() | changed(Radius) | changed(SelectMode)){
    for(K = 1, 20){
        local DegSep = 360/20
        holoCreate(K)
        holoPos(K,holoEntity(9002):toWorld(vec(Radius*cos(K*DegSep),Radius*sin(K*DegSep),0))) 
        holoAng(K,ang(0,(holoEntity(K):pos() - holoEntity(9001):pos()):toAngle():yaw(),0))
        holoParent(K,9002)
        holoAlpha(K,SelectMode*255)
        holoModel(K,"hq_sphere")    
        #holoScale(K,vec(1,1,4))   
        holoAnim(K,1)
    }
}

if(owner():lastSaid() == "clear"){
    Props:clear()
    Position:clear()
    Angle:clear()
    Model:clear()
    Material:clear()
    Color:clear()
    Alpha:clear()
    J = 0
    I = 0
    hideChat(1)
}

if(owner():lastSaid() == "dupe"){
    SelectMode = 1
    hideChat(1)
}
if(owner():lastSaid() == "!dupe"){
    SelectMode = 0
    hideChat(1)
}
if(SelectMode){
    Select = changed(owner():keyUse())&owner():keyUse()
    Paste = changed(owner():keyReload())&owner():keyReload()
}
if(Paste){
    Pasting = 1
    holoEntity(9001):soundPlay(1,2,"ambient/alarms/apc_alarm_loop1.wav")
    for(K = 1, 20){
        holoEntity(K):setTrails(50,50,5,"trails/laser",vec(0,255,0),255)   
    }
}
if(!Pasting){
    holoPos(9001,owner():aimPos())
    holoAng(9001,ang(0,owner():eyeAngles():yaw(),0))
}
if(Pasting){
    A++
    holoAng(9002,ang(0,cos(A)*180,0))
    holoPos(9002,holoEntity(9001):toWorld(vec(0,0,(Radius/2)+(cos(A)*(Radius/2)))))
}
if(changed(Pasting)&!Pasting){
    holoPos(9002,holoEntity(9001):pos()) 
    
    for(K = 1, 20){
        holoEntity(K):removeTrails()
    }
}

if(Select&findCanQuery()){
    findExcludeEntity(entity())
    findIncludeClass("prop_physics")
    findIncludeClass("acf_gun")
    findIncludeClass("acf_ammo")
    findIncludeClass("acf_engine")
    findIncludeClass("prop_vehicle_prisoner_pod")
    findInSphere(owner():aimPos(),Radius)
    Props = findToArray()
}


while(J < Props:count() & perf()){
    J++
    Position:pushVector(holoEntity(9001):toLocal(Props[J, entity]:pos()))
    Angle:pushAngle(holoEntity(9001):toLocal(Props[J, entity]:angles()))
    Model:pushString(Props[J, entity]:model())
    Material:pushString(Props[J, entity]:getMaterial())
    Color:pushVector(Props[J, entity]:getColor())
    Alpha:pushNumber(Props[J, entity]:getAlpha())
}



while(Pasting & propCanCreate() & perf() & I < Props:count()){
    I++
    Prop = propSpawn(Model[I, string], holoEntity(9001):toWorld(Position[I, vector]), holoEntity(9001):toWorld(Angle[I, angle]), 1)
    Prop:propNotSolid(1)
    Prop:setMaterial(Material[I, string])
    Prop:setColor(Color[I, vector])
    Prop:setAlpha(Alpha[I, number])
}
if(I >= Props:count()){
    Pasting = 0
    J = 0
    I = 0
    if(changed(I >= Props:count())){
    holoEntity(9001):soundPlay(1,3.413,"garrysmod/save_load"+randint(1,4)+".wav")
    }
}

