@name M197
@inputs Turret:entity M1
@outputs Fire
@persist Ang P

runOnTick(1)

if(first() || dupefinished()){
    P1 = 70
    P2 = 63
    P3 = 40

    holoCreate(1)
    holoPos(1,Turret:toWorld(vec(0,0,0)))
    holoAng(1,Turret:toWorld(ang(0,0,0)))
    holoAlpha(1,0)
    holoParent(1,Turret)
    ###spin###
    holoCreate(2)
    holoModel(2,"models/Holograms/hq_rcylinder_thin.mdl")
    holoPos(2,Turret:toWorld(vec(20,0,0)))
    holoAng(2,Turret:toWorld(ang(90,0,0)))
    holoScale(2,vec(1,1,1.3))
    holoMaterial(2,"phoenix_storms/mat/mat_phx_metallic")
    holoColor(2,vec(50,50,50))
    holoParent(2,1)
    ###spin###
    ###barrels###
    holoCreate(3)
    holoModel(3,"models/Holograms/hq_tube.mdl")
    holoPos(3,Turret:toWorld(vec(50,0,3)))
    holoAng(3,Turret:toWorld(ang(90,0,0)))
    holoScale(3,vec(0.2,0.2,4))
    holoMaterial(3,"phoenix_storms/mat/mat_phx_metallic")
    holoColor(3,vec(50,50,50))
    holoParent(3,2)
    
    holoCreate(4)
    holoModel(4,"models/Holograms/hq_tube.mdl")
    holoPos(4,Turret:toWorld(vec(50,2.5,-2)))
    holoAng(4,Turret:toWorld(ang(90,0,0)))
    holoScale(4,vec(0.2,0.2,4))
    holoMaterial(4,"phoenix_storms/mat/mat_phx_metallic")
    holoColor(4,vec(50,50,50))
    holoParent(4,2)
    
    holoCreate(5)
    holoModel(5,"models/Holograms/hq_tube.mdl")
    holoPos(5,Turret:toWorld(vec(50,-2.5,-2)))
    holoAng(5,Turret:toWorld(ang(90,0,0)))
    holoScale(5,vec(0.2,0.2,4))
    holoMaterial(5,"phoenix_storms/mat/mat_phx_metallic")
    holoColor(5,vec(50,50,50))
    holoParent(5,2)
    
    holoCreate(6)
    holoModel(6,"models/sprops/geometry/qring_12.mdl")
    holoPos(6,Turret:toWorld(vec(P1,0,4)))
    holoAng(6,Turret:toWorld(ang(45,90,0)))
    holoScale(6,vec(0.5,0.5,0.5))
    holoMaterial(6,"phoenix_storms/mat/mat_phx_metallic")
    holoColor(6,vec(50,50,50))
    holoParent(6,2)
    
    holoCreate(7)
    holoModel(7,"models/sprops/geometry/qring_12.mdl")
    holoPos(7,Turret:toWorld(vec(P1,3.8,-2.7)))
    holoAng(7,Turret:toWorld(ang(162.5,90,0)))
    holoScale(7,vec(0.5,0.5,0.5))
    holoMaterial(7,"phoenix_storms/mat/mat_phx_metallic")
    holoColor(7,vec(50,50,50))
    holoParent(7,2)
    
    holoCreate(8)
    holoModel(8,"models/sprops/geometry/qring_12.mdl")
    holoPos(8,Turret:toWorld(vec(P1,-3.8,-2.7)))
    holoAng(8,Turret:toWorld(ang(-72.5,90,0)))
    holoScale(8,vec(0.5,0.5,0.5))
    holoMaterial(8,"phoenix_storms/mat/mat_phx_metallic")
    holoColor(8,vec(50,50,50))
    holoParent(8,2)
    
    holoCreate(9)
    holoModel(9,"")
    holoPos(9,Turret:toWorld(vec(P1,0,-3.74)))
    holoAng(9,Turret:toWorld(ang(0,0,0)))
    holoScale(9,vec(0.13,0.47,0.14))
    holoMaterial(9,"phoenix_storms/mat/mat_phx_metallic")
    holoColor(9,vec(50,50,50))
    holoParent(9,2)
    
    holoCreate(10)
    holoModel(10,"")
    holoPos(10,Turret:toWorld(vec(P1,2.68,1.17)))
    holoAng(10,Turret:toWorld(ang(0,0,-61)))
    holoScale(10,vec(0.13,0.46,0.15))
    holoMaterial(10,"phoenix_storms/mat/mat_phx_metallic")
    holoColor(10,vec(50,50,50))
    holoParent(10,2)
    
    holoCreate(11)
    holoModel(11,"")
    holoPos(11,Turret:toWorld(vec(P1,-2.68,1.17)))
    holoAng(11,Turret:toWorld(ang(0,0,61)))
    holoScale(11,vec(0.13,0.46,0.15))
    holoMaterial(11,"phoenix_storms/mat/mat_phx_metallic")
    holoColor(11,vec(50,50,50))
    holoParent(11,2)
    
    holoCreate(12)
    holoModel(12,"models/sprops/geometry/qring_12.mdl")
    holoPos(12,Turret:toWorld(vec(P2,0,4)))
    holoAng(12,Turret:toWorld(ang(45,90,0)))
    holoScale(12,vec(0.5,0.5,0.5))
    holoMaterial(12,"phoenix_storms/mat/mat_phx_metallic")
    holoColor(12,vec(50,50,50))
    holoParent(12,2)
    
    holoCreate(13)
    holoModel(13,"models/sprops/geometry/qring_12.mdl")
    holoPos(13,Turret:toWorld(vec(P1,3.8,-2.7)))
    holoAng(13,Turret:toWorld(ang(162.5,90,0)))
    holoScale(13,vec(0.5,0.5,0.5))
    holoMaterial(13,"phoenix_storms/mat/mat_phx_metallic")
    holoColor(13,vec(50,50,50))
    holoParent(13,2)
    
    holoCreate(14)
    holoModel(14,"models/sprops/geometry/qring_12.mdl")
    holoPos(14,Turret:toWorld(vec(P2,-3.8,-2.7)))
    holoAng(14,Turret:toWorld(ang(-72.5,90,0)))
    holoScale(14,vec(0.5,0.5,0.5))
    holoMaterial(14,"phoenix_storms/mat/mat_phx_metallic")
    holoColor(14,vec(50,50,50))
    holoParent(14,2)
    
    holoCreate(15)
    holoModel(15,"")
    holoPos(15,Turret:toWorld(vec(P2,0,-3.74)))
    holoAng(15,Turret:toWorld(ang(0,0,0)))
    holoScale(15,vec(0.13,0.47,0.14))
    holoMaterial(15,"phoenix_storms/mat/mat_phx_metallic")
    holoColor(15,vec(50,50,50))
    holoParent(15,2)
    
    holoCreate(16)
    holoModel(16,"")
    holoPos(16,Turret:toWorld(vec(P2,2.68,1.17)))
    holoAng(16,Turret:toWorld(ang(0,0,-61)))
    holoScale(16,vec(0.13,0.46,0.15))
    holoMaterial(16,"phoenix_storms/mat/mat_phx_metallic")
    holoColor(16,vec(50,50,50))
    holoParent(16,2)
    
    holoCreate(17)
    holoModel(17,"")
    holoPos(17,Turret:toWorld(vec(P2,-2.68,1.17)))
    holoAng(17,Turret:toWorld(ang(0,0,61)))
    holoScale(17,vec(0.13,0.46,0.15))
    holoMaterial(17,"phoenix_storms/mat/mat_phx_metallic")
    holoColor(17,vec(50,50,50))
    holoParent(17,2)
    
    holoCreate(18)
    holoModel(18,"models/sprops/geometry/qring_12.mdl")
    holoPos(18,Turret:toWorld(vec(P3,0,4)))
    holoAng(18,Turret:toWorld(ang(45,90,0)))
    holoScale(18,vec(0.5,0.5,0.5))
    holoMaterial(18,"phoenix_storms/mat/mat_phx_metallic")
    holoColor(18,vec(50,50,50))
    holoParent(18,2)
    
    holoCreate(19)
    holoModel(19,"models/sprops/geometry/qring_12.mdl")
    holoPos(19,Turret:toWorld(vec(P3,3.8,-2.7)))
    holoAng(19,Turret:toWorld(ang(162.5,90,0)))
    holoScale(19,vec(0.5,0.5,0.5))
    holoMaterial(19,"phoenix_storms/mat/mat_phx_metallic")
    holoColor(19,vec(50,50,50))
    holoParent(19,2)
    
    holoCreate(20)
    holoModel(20,"models/sprops/geometry/qring_12.mdl")
    holoPos(20,Turret:toWorld(vec(P3,-3.8,-2.7)))
    holoAng(20,Turret:toWorld(ang(-72.5,90,0)))
    holoScale(20,vec(0.5,0.5,0.5))
    holoMaterial(20,"phoenix_storms/mat/mat_phx_metallic")
    holoColor(20,vec(50,50,50))
    holoParent(20,2)
    
    holoCreate(21)
    holoModel(21,"")
    holoPos(21,Turret:toWorld(vec(P3,0,-3.74)))
    holoAng(21,Turret:toWorld(ang(0,0,0)))
    holoScale(21,vec(0.13,0.47,0.14))
    holoMaterial(21,"phoenix_storms/mat/mat_phx_metallic")
    holoColor(21,vec(50,50,50))
    holoParent(21,2)
    
    holoCreate(22)
    holoModel(22,"")
    holoPos(22,Turret:toWorld(vec(P3,2.68,1.17)))
    holoAng(22,Turret:toWorld(ang(0,0,-61)))
    holoScale(22,vec(0.13,0.46,0.15))
    holoMaterial(22,"phoenix_storms/mat/mat_phx_metallic")
    holoColor(22,vec(50,50,50))
    holoParent(22,2)
    
    holoCreate(23)
    holoModel(23,"")
    holoPos(23,Turret:toWorld(vec(P3,-2.68,1.17)))
    holoAng(23,Turret:toWorld(ang(0,0,61)))
    holoScale(23,vec(0.13,0.46,0.15))
    holoMaterial(23,"phoenix_storms/mat/mat_phx_metallic")
    holoColor(23,vec(50,50,50))
    holoParent(23,2)
    ###barrels###
}

if(M1&Ang<20){
    Ang++
}elseif(!M1&Ang>0){
    Ang-=0.1
}

    Fire = P>0.75
    if(M1){
        P+=0.0125
    }else{
        P-=0.0125
    }
    P = clamp(P,0,1)

holoAng(2,holoEntity(2):toWorld(ang(0,Ang,0)))

if(changed(Fire)&Fire){Turret:soundPlay(1,0,"npc/combine_gunship/gunship_fire_loop1.wav")}
if(!Fire){soundStop(1)}
soundPitch(1,200)
