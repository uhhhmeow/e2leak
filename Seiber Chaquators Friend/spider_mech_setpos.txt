#Four legged mech with twin turrets and swiveling upper section
#based on a similar design from Supreme Commander 2
@name Spider Mech
@inputs [Controls Camera]:wirelink [Pod Core FL FR RL RR Torso]:entity
@outputs Fire
@persist [Campos Camvec Cpos Corepos Torpos]:vector Zoom Posture
@persist CoreP CoreY CoreR TorP TorY TorR Inc Parts:array
@persist [FLfootgo FLToepos FRfootgo FRToepos RLfootgo RLToepos RRfootgo RRToepos]:vector
@persist Prop:entity
interval(10)

if(first() | duped()){
Zoom = 200
Posture = 75    
Parts = entity():getConstraints()
FLfootgo = FL:pos()
FRfootgo = FR:pos()
RLfootgo = RL:pos()
RRfootgo = RR:pos()

        rangerFilter(Parts)
        rangerHitEntities(1)
        rangerHitWater(1)
        rangerPersist(1)

}

#start controls
W = Controls:number("W")
A = Controls:number("A")
S = Controls:number("S")
D = Controls:number("D")
M1 = Controls:number("Mouse1")
M2 = Controls:number("Mouse2")
Space = Controls:number("Space")
Shift = Controls:number("Shift")#
Mwheelup = Controls:number("NextWeapon")
Mwheeldown = Controls:number("PrevWeapon")
Active = Controls:number("Active")
Seat = Controls:entity("Entity")
#end controls
Camera:setNumber("Activated",Active)
Camera:setVector("Position",Campos)
Camera:setVector("Direction",Camvec)

if(M1){Fire = 1}else{Fire = 0}

if(Mwheelup){Zoom = Zoom + 10}
if(Mwheeldown){Zoom = Zoom - 10}
#Cang = (Cpos - Core:pos()):normalized()
#if((Core:pos() - Cpos):length() > 300){Cammul = 100}else{Cammul = 10}
#if((Core:pos() - Cpos):length() > 10){Cpos = Cpos - (Cang * Cammul)}
Campos = Torso:pos() + vec(0,0,100)-Seat:driver():eye()*Zoom
Camvec = Seat:driver():eye()

if(Space & Posture < 100){Posture = Posture + 1}
if(Space & Posture > 100){Posture = Posture - 1}
if(!Space & !Shift & Posture < 50){Posture = Posture + 1}
if(!Space & !Shift & Posture > 50){Posture = Posture - 1}
#hippos

CoreP = Core:angles():pitch()
CoreY = Core:angVel():yaw()
CoreR = Core:angles():roll()
CorePitch = (($CoreP)+(CoreP))
CoreYaw = (($CoreY)+(CoreY))
CoreRoll =(($CoreR)+(CoreR))
Core:applyAngForce(ang(CorePitch,CoreYaw,CoreRoll))

Corepos = Core:pos() - (((FL:pos() + FR:pos() + RL:pos() + RR:pos()))+vec(0,0,Posture))
Corevec = (($Corepos)+(Corepos))
Core:setPos(Corevec)

#torsopos
Torpos = Torso:pos() - (Core:pos()+vec(0,0,65))
Torvec = (($Torpos)+(Torpos))
Torso:setPos(Torvec)

if(Active){TorY = Torso:bearing(Seat:driver():eye())}else{TorY = Torso:bearing(Core:forward())}
TorP = Torso:angles():pitch()
TorR = Torso:angles():roll()
TorPitch = (($TorP)+(TorP))
TorYaw = (($TorY)+(TorY))
TorRoll =(($TorR)+(TorR))
Torso:applyAngForce(ang(TorPitch,TorYaw,TorRoll))


#Target = Seat:driver():eye()*500000
Tranger = rangerOffset(10000,Campos,Seat:driver():eye())
Target = Tranger:position()

#walk

FLToepos = FL:pos() - (FLfootgo + vec(0,0,FL:pos():distance(FLfootgo)))
FLToevec = (($FLToepos)+(FLToepos))
FL:applyOffsetForce(FLToevec,FL:pos())

FRToepos = FR:pos() - (FRfootgo + vec(0,0,FR:pos():distance(FRfootgo)))
FRToevec = (($FRToepos)+(FRToepos))
FR:applyOffsetForce(FRToevec,FR:pos())

RLToepos = RL:pos() - (RLfootgo + vec(0,0,RL:pos():distance(RLfootgo)))
RLToevec = (($RLToepos)+(RLToepos))
RL:applyOffsetForce(RLToevec,RL:pos())

RRToepos = RR:pos() - (RRfootgo + vec(0,0,RR:pos():distance(RRfootgo)))
RRToevec = (($RRToepos)+(RRToepos))
RR:applyOffsetForce(RRToevec,RR:pos())


#control
if(W == 1)
{
    if(Inc < 200){Inc = Inc + 5}
if(Inc >= 200){Inc = 0}
    if(Inc <= 50){Foot = 1}
    if(Inc <= 100 & Inc > 50){Foot = 3}
    if(Inc <= 150 & Inc > 100){Foot = 2}
    if(Inc <= 200 & Inc > 150){Foot = 4}
    
Vel = 60
}
if(S == 1 | A == 1 | D == 1)
{
    if(Inc < 200){Inc = Inc + 5}
if(Inc >= 200){Inc = 0}
    if(Inc <= 50){Foot = 1}
    if(Inc <= 100 & Inc > 50){Foot = 3}
    if(Inc <= 150 & Inc > 100){Foot = 2}
    if(Inc <= 200 & Inc > 150){Foot = 4}
    
Vel = -60
}


if(!W & !S & !A & !D)
{
    Foot = 0
    Vel = 0
    }


if(A)
{Dire = Torso:right()}

if(D)
{Dire = Torso:right()}

if(!A & !D)
{Dire = Seat:driver():eye()}

    
#if(changed(Foot))
#{
#    Core:soundPlay(1,500,"ambient/levels/labsmachine_stop1.wav")
#    soundPitch(1,300)
#    }
if(Foot == 1)
    {
    Foottar = rangerOffset(5000,Torso:toWorld(vec(75,75,0)) + Dire*Vel,vec(0,0,-1))
    FLfootgo = Foottar:position() + vec(0,0,12)   
    }
if(Foot == 2)
    {
    Foottar = rangerOffset(5000,Torso:toWorld(vec(75,-75,0)) + Dire*Vel,vec(0,0,-1))
    FRfootgo = Foottar:position() + vec(0,0,12)        
    }
if(Foot == 3)
    {
    Foottar = rangerOffset(5000,Torso:toWorld(vec(-75,75,0)) + Dire*Vel,vec(0,0,-1))
    RLfootgo = Foottar:position() + vec(0,0,12)
    }
if(Foot == 4)
    {
    Foottar = rangerOffset(5000,Torso:toWorld(vec(-75,-75,0)) + Dire*Vel,vec(0,0,-1))
    RRfootgo = Foottar:position() + vec(0,0,12)
    }

if(Active==0){Torso:propFreeze(1)}
if(Active==1){Torso:propFreeze(0)}

if(first()){
    holoCreate(1)
    holoAng(1,Core:toWorld(ang(0,0,0)))
    holoPos(1,Core:toWorld(vec(0,0,0)))
    holoParent(1,Core)
    holoModel(1,"hq_sphere")
    holoScale(1,vec(4.7,4.7,4.7))
    holoMaterial(1,"Phoenix_storms/mat/mat_phx_metallic")
    holoColor(1,vec(50,50,50))
    
    holoCreate(2)
    holoAng(2,Core:toWorld(ang(0,0,-10)))
    holoPos(2,Core:toWorld(vec(0,0,25)))
    holoParent(2,Core)
    holoModel(2,"hq_cylinder")
    holoScale(2,vec(2,2,5))
    holoMaterial(2,"Phoenix_storms/mat/mat_phx_metallic")
    holoColor(2,vec(50,50,50))
    
    holoCreate(3)
    holoPos(3,Core:toWorld(vec(0,0,50)))
    holoAng(3,Torso:toWorld(ang(0,0,0)))
    holoParent(3,Core)
    holoModel(3,"hq_rcube_thick")
    holoScale(3,vec(4,5,2))
    holoMaterial(3,"Phoenix_storms/mat/mat_phx_metallic")
    holoColor(3,vec(50,50,50))
    
    holoCreate(4)
    holoPos(4,holoEntity(3):toWorld(vec(0,0,0)))
    holoAng(4,holoEntity(3):toWorld(ang(0,0,0)))
    holoParent(4,holoEntity(3))
    holoModel(4,"hq_rcube_thick")
    holoScale(4,vec(4.01,5.01,0.1))
    holoMaterial(4,"Phoenix_storms/mat/mat_phx_metallic")
    holoColor(4,vec(255,50,50))
    #turret
    holoCreate(5)
    holoPos(5,holoEntity(3):toWorld(vec(20,0,-10)))
    holoParent(5,holoEntity(3))
    holoModel(5,"hq_rcylinder_thin")
    holoScale(5,vec(1,1,1))
    holoMaterial(5,"Phoenix_storms/mat/mat_phx_metallic")
    holoColor(5,vec(50,50,50))
    
    holoCreate(6)
    holoPos(6,holoEntity(5):toWorld(vec(8,0,0)))
    holoAng(6,holoEntity(5):toWorld(ang(90,0,0)))
    holoParent(6,holoEntity(5))
    holoModel(6,"hq_cubinder")
    holoScale(6,vec(0.5,0.5,0.5))
    holoMaterial(6,"Phoenix_storms/mat/mat_phx_metallic")
    holoColor(6,vec(50,50,50))
    
    holoCreate(7)
    holoPos(7,holoEntity(5):toWorld(vec(15,0,0)))
    holoAng(7,holoEntity(5):toWorld(ang(90,0,0)))
    holoParent(7,holoEntity(5))
    holoModel(7,"hq_tube_thin")
    holoScale(7,vec(0.5,0.5,0.7))
    holoMaterial(7,"Phoenix_storms/mat/mat_phx_metallic")
    holoColor(7,vec(50,50,50))
    
    #reddot
    holoCreate(8)
    holoScale(8,vec(2.5,2.5,2.5))
    holoColor(8,vec(255,0,0))
    holoModel(8,"hq_sphere")
}
    Aim=Pod:driver():eyeAngles():pitch()  
    holoAng(5,holoEntity(3):toWorld(ang(Aim,0,90)))
    
if(changed(Fire)&Fire&!Prop:isValid()){  
    propSpawnEffect(0)
    Prop = propSpawn("models/props_phx/ww2bomb.mdl",holoEntity(7):toWorld(vec(0,0,100)),holoEntity(7):toWorld(ang(-90,0,0)),0)
    Torso:soundPlay(1,5,"weapons/rpg/rocketfire1.wav")
    Prop:propGravity(0)
    Prop:setMass(50000)
    Prop:setTrails(10,0,1,"trails/smoke",vec(255,255,255),255)
    
}
Prop:setPos(Prop:forward())

Ranger = rangerOffset(100000,holoEntity(7):pos(),holoEntity(7):up())
holoPos(8,Ranger:position())

holoVisible(8,players(),0)
holoVisible(8,Pod:driver(),1)

