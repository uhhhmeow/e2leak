@name Cyber's Walking Thing
@inputs P:entity [FR RL FL RR]:entity
@outputs [FR RL FL RR LegMount1 LegMount2 LegMount3 LegMount4]:entity
@persist E:entity Inc Foot BaseHeight:vector Dri:entity [FRTar RLTar FLTar RRTar FRToepos FRToevec RLToepos RLToevec FLToepos FLToevec RRToepos RRToevec]:vector
if(first())
{
    runOnTick(1)
    
    E = entity():isWeldedTo()
    
    rangerFilter(array(entity(),E,FR,RL,FL,RR))
    rangerPersist(1)
    
    FR:propNotSolid(1)
    RL:propNotSolid(1)
    FL:propNotSolid(1)
    RR:propNotSolid(1)
    FR:propFreeze(0)
    RL:propFreeze(0)
    FL:propFreeze(0)
    RR:propFreeze(0)
    FR:setAlpha(0)
    RL:setAlpha(0)
    FL:setAlpha(0)
    RR:setAlpha(0)
    
    E:setMass(50000)
    
holoCreate(1)
holoParent(1,E)
holoPos(1,E:toWorld(vec(15,15,0)))
holoAng(1,E:toWorld(ang(0,225,0)))
#holoModel(1,"hq_sphere")
holoScale(1,vec(1.3,1.3,1.3))
holoMaterial(1,"phoenix_storms/mat/mat_phx_metallic")
#holoColor(1,vec(40,40,40))
holoAlpha(1,0)


holoCreate(2)
holoParent(2,E)
holoPos(2,E:toWorld(vec(-15,15,0)))
holoAng(2,E:toWorld(ang(0,315,0)))
#holoModel(2,"hq_sphere")
holoScale(2,vec(1.3,1.3,1.3))
holoMaterial(2,"phoenix_storms/mat/mat_phx_metallic")
#holoColor(2,vec(40,40,40))
holoAlpha(2,0)


holoCreate(3)
holoParent(3,E)
holoPos(3,E:toWorld(vec(-15,-15,0)))
holoAng(3,E:toWorld(ang(0,45,0)))
#holoModel(3,"hq_sphere")
holoScale(3,vec(1.3,1.3,1.3))
holoMaterial(3,"phoenix_storms/mat/mat_phx_metallic")
#holoColor(3,vec(40,40,40))
holoAlpha(3,0)


holoCreate(4)
holoParent(4,E)
holoPos(4,E:toWorld(vec(15,-15,0)))
holoAng(4,E:toWorld(ang(0,135,0)))
#holoModel(4,"hq_sphere")
holoScale(4,vec(1.3,1.3,1.3))
holoMaterial(4,"phoenix_storms/mat/mat_phx_metallic")
#holoColor(4,vec(40,40,40))
holoAlpha(4,0)

LegMount1 = holoEntity(1)
LegMount2 = holoEntity(2)
LegMount3 = holoEntity(3)
LegMount4 = holoEntity(4)
}


Dri = P:driver()
Active = Dri:isValid()
if(Active)
{
    W = Dri:keyForward()
    A = Dri:keyLeft()
    S = Dri:keyBack()
    D = Dri:keyRight()
    Spc = Dri:keyJump()
    Shift = Dri:keySprint()
    M1 = Dri:keyAttack1()
    M2 = Dri:keyAttack2()
    R = Dri:keyReload()
    Alt = Dri:keyWalk()
}


RangerFR = rangerOffset(220,E:toWorld(vec(75,-75,20)) + (clamp(E:vel()*0.3,vec(-50),vec(50))),vec(0,0,-1))
RangerRL = rangerOffset(220,E:toWorld(vec(-75,75,20)) + (clamp(E:vel()*0.3,vec(-50),vec(50))),vec(0,0,-1))
RangerFL = rangerOffset(220,E:toWorld(vec(75,75,20)) + (clamp(E:vel()*0.3,vec(-50),vec(50))),vec(0,0,-1))
RangerRR = rangerOffset(220,E:toWorld(vec(-75,-75,20)) + (clamp(E:vel()*0.3,vec(-50),vec(50))),vec(0,0,-1))
RangerBase = (RangerFR:position()+RangerRL:position()+RangerFL:position()+RangerRR:position())/4

BaseHeight = ((RangerBase + vec(0,0,50))-E:pos())*vec(0,0,1)
E:applyForce((((BaseHeight*5) + (E:right()*(S-W)*15) + (E:forward()*(R-Alt)*15))*10 -E:vel()*vec(1,1,4))*E:mass()/10)

E:applyAngForce((ang(0,(A-D)*100,0)-E:angles()*ang(1,0,1)-E:angVel())*E:mass())

if(Inc < 200){Inc = Inc + 0.5 + (E:angVelVector():length()/20)*!W*!S + E:vel():length()/25}
if(Inc >= 200){Inc = 0}
if(Inc <= 50){Foot = 1}
if(Inc <= 100 & Inc > 50){Foot = 2}
if(Inc <= 150 & Inc > 100){Foot = 3}
if(Inc <= 200 & Inc > 150){Foot = 4}

if(changed(Foot))
{
    switch(Foot)
    {
        case 1,
        FRTar = RangerFR:position()
        RLTar = RangerRL:position()
        break
        
        case 3,
        FLTar = RangerFL:position()
        RRTar = RangerRR:position()
        break
    }
}

FRToepos = FR:pos() - (FRTar + vec(0,0,FR:pos():distance(FRTar)/2))
FRToevec = (($FRToepos*150)+(FRToepos*20))*(FR:mass()/10*-1)
FR:applyOffsetForce(FRToevec,FR:pos())

RLToepos = RL:pos() - (RLTar + vec(0,0,RL:pos():distance(RLTar)/2))
RLToevec = (($RLToepos*150)+(RLToepos*20))*(RL:mass()/10*-1)
RL:applyOffsetForce(RLToevec,RL:pos())

FLToepos = FL:pos() - (FLTar + vec(0,0,FL:pos():distance(FLTar)/2))
FLToevec = (($FLToepos*150)+(FLToepos*20))*(FL:mass()/10*-1)
FL:applyOffsetForce(FLToevec,FL:pos())

RRToepos = RR:pos() - (RRTar + vec(0,0,RR:pos():distance(RRTar)/2))
RRToevec = (($RRToepos*150)+(RRToepos*20))*(RR:mass()/10*-1)
RR:applyOffsetForce(RRToevec,RR:pos())
