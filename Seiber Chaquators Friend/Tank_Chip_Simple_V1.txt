##NOT MADE BY CYBER POLICE :D

@name Tank
@inputs Pod:entity Gun:entity Active
@outputs XHVec:vector Debug CameraPOS:vector CameraDIR:vector
@persist Dran:ranger XH:ranger User:entity A
@persist Exclude
@persist [Trig Sam Sac Fac Trig3 ET ET2]:vector
@persist TrigTX TrigTY TrigTZ TrigY TrigX TrigZ TrigT XHVec:vector Vel:vector
@persist TurningRate ElevationRate ShootPos:vector Change
@inputs [FL FR Base]:entity W S A D Brake
@outputs Running EnginePitch TrackPitch Volume MPH
@persist RPMFL RPMFR EnginePowerReduced Stall EngineDestroyed
@persist [Pos Turningf Turningb Force]:vector
@inputs AmmoSwitch
@outputs AmmoType
@persist Aon Aimpower Torque TraverseRate ElevateRate
@inputs CalcTotalWeight
@persist State Owner:entity R:array Psh
@persist Tmass Tcom:vector
@trigger all
 
interval(20)
 
 
#Movement constants.
    Dfor = 0.2 #Forward friction.
    Dtur = 0.05 #turning friction. Raise this if it turns too fast.
    Torque = -750000 #Change the 2.7 multiplier to increase wheel torque. Affects steering speed too.
    Offsetfor = 200 # Ignore
    StaticPitch = 0.15 #Idle pitch
    DrivingPitch = 0.3
    TurningPitch = 0.45
    Volume = 30 #Ignore
    Ang = ang(0,0,1) #Wheel direction angle. If your wheels don't spin, change the position of the 1
    # to 1,0,0 or 0,0,1 etc
   
   #Aiming constants
    Aimpower = 200 #Amount of force on the gun, increase if gun is floppy, decrease if spazzes
    TraverseRate = 0.030 # Turret turn rate, increase to go faster
    ElevateRate = 0.020  #gun elevation rate
   
   
   
   
   
   
   
   
   
   
if(first()|dupefinished())
{
    Exclude = 1000 #Ignore
    XHExclude = 300 #Ignore
    TurningRate = TraverseRate
    ElevationRate = ElevateRate  
    rangerPersist(1)
    rangerFilter(entity():getConstraints())
   
    ######HOLO CREATION
    holoCreate(1,entity():pos(),vec(0.05,0.05,0.05))
    holoModel(1,"icosphere")
    holoColor(1,vec(0,255,0),255) #Holo sight color/alpha adjustment
    holoParent(1,Pod)
   
}
 
if(Pod:driver())
{
    Change = 1
    User= Pod:driver()
    Dran = rangerOffset(30000, User:shootPos() + User:eye() * Exclude, User:eye())
    ET = entity():forward()
    ET2 = $ET
    ET2X = ET2:x()
    ET2Y = ET2:y()
    ET2Z = ET2:z()
 
    if($Change){
        ShootPos = Pod:toLocal(User:shootPos())
    }
   
    if(Gun)
    {
        #Xhair
    XH = rangerOffset(30000, Gun:pos() + Gun:forward() *XHExclude, Gun:forward())
    XHVec = XH:position()# - User:shootPos()
    XHVec2 = XH:position() - Pod:toWorld(ShootPos)
        #Hologram stuff
    HoloPos = Pod:toWorld(ShootPos) + XHVec2:normalized() * 40 #Do not adjust
        holoPos(1,HoloPos)    
        holoAng(1,Gun:angles())
       
        Trig2 = (Dran:position() - Gun:pos())
        Trig3 = Trig2:normalized()
            #Clamp the speed at which the Normalized vector changes, making a delayed turning
            #Split into 3 Numbers
            TrigX = Trig3:x()
            TrigY = Trig3:y()
            TrigZ = Trig3:z()
            #Add where it wants to aim, to a "delayed" value, that it will slowly obtain.
            TrigTX = clamp(TrigX, TrigTX-TurningRate, TrigTX+TurningRate)+ET2X
            TrigTY = clamp(TrigY, TrigTY-TurningRate, TrigTY+TurningRate)+ET2Y
            TrigTZ = clamp(TrigZ, TrigTZ-ElevationRate, TrigTZ+ElevationRate)+ET2Z
           
            TrigT = TrigTX+TrigTY+TrigTZ
           
        Trig = vec(TrigTX,TrigTY,TrigTZ)
       
        Sam = (Trig) - Gun:forward()
        Sac = (Gun:forward():cross(Sam)):cross(Gun:forward())
        Fac = (Sac + $Sac * 5)*Gun:mass()
       
        Gun:applyOffsetForce(Fac*50,Gun:pos()+Gun:forward()*Aimpower) #Edit the last number here to change force
    }
 
}else{
    holoPos(1,entity():pos())
    Change = 0
}
 
 
 
   
 
#Friction Changer, To make it jerk to a stop.
    if((!W&!S)){
        Dfor = 4
        }
 
#Handbrake
    if(Brake){
        Dfor = 15 #Braking friction
    }
 
#Damage Inputs
 
    #Default
    if(!EnginePowerReduced&!EngineDestroyed&!Stall){
        Power = Torque #Adjust multiplier to adjust torque. This affects steering, adjust Dtur if it turns too fast
        Running = 1
    }
 
    #Reduced Engine Power
    if(EnginePowerReduced&!EngineDestroyed&!Stall){
        Power = 260000*2.7 #Don't change this
        Running = 1
    }
 
    #Engine Destroyed/Stall
    if(EngineDestroyed|Stall){
        Running = 0
        Power = 0
    }
 
 
 
 
 
#Rpm Limitation
    Limit = 350 #RPM limit
    if((Limit > RPMFL)&(-Limit < RPMFL)) {TorqueFL = Power}else{TorqueFL = 0}
 
 
    if((Limit > RPMFR)&(-Limit < RPMFR)) {TorqueFR = Power}else{TorqueFR = 0}
 
   
 
 
#Actual Movement Calculations.
    Total = (W-S)
    Right = (A-D)
    Left = -Right
    #Right/left side
    RHS = (Total*2+Right*4)/2
    LHS = (Total*2+Left*4)/2
   
#Applying the Forces
if(W|S|A|D){
    FL:applyTorque(vec(Ang)*-TorqueFL*LHS)
 
    FR:applyTorque(vec(Ang)*TorqueFR*RHS)
 
}
 
#Base Dampening Friction
#Main drag forces.
    Pos = Base:pos()
    Vel = $Pos
    Vel:setZ(0)
 
#Reset Variables
    Force = vec()
    Turningf = vec()
    Turningb = vec()
 
#Drag Forces
if(clk())
{
    Force = Force - Vel * Base:mass() * Dfor
    Turningf = Turningf - entity():angVel():yaw() * entity():right() * Dtur * Base:mass()
    Turningb = Turningb + entity():angVel():yaw() * entity():right() * Dtur * Base:mass()
 
}
#Applying the forces
if(clk())
{
    Force = Force:setZ(0)
    Base:applyForce(Force)
    Base:applyOffsetForce(entity():forward() * Offsetfor, Turningf)
    Base:applyOffsetForce(entity():forward() * -Offsetfor, Turningb)
}
 
##Sounds for engines (inbuilt smoother)
if(clk()){
    if(A|D){
        Pitch = TurningPitch
    }elseif(W|S){
        Pitch = DrivingPitch
    }else{
        Pitch = StaticPitch    
    }
    #Smoother
    EnginePitch = clamp(Pitch,EnginePitch-0.01,EnginePitch+0.01)
}
 
#Sounds for Tracks (Inbuilt speedo and Smoother)
if(clk()){
    MPH=toUnit("mph", entity():vel():length())
 
    if(W|A|S|D){
        TPitch = MPH/12
       
    }else{
        TPitch = 0
    }
   
    #Smoother
    TrackPitch = clamp(TPitch,TrackPitch-0.08,TrackPitch+0.08)
   
 
}
 
 
#RPM Calculation
RPMFL = FL:angVel():pitch()/6
 
 
RPMFR = FR:angVel():pitch()/6
# if it isn't working, change the
# :roll to :yaw or :pitch till it works
 
if(AmmoSwitch & ~AmmoSwitch) {Aon = !Aon}
if(Aon) {AmmoType = 2}
if(!Aon) {AmmoType = 1}
 
 
#Weight calculator by Sestze
Owner=entity():owner()
 
if(first())
{
    R = entity():getConstraints()
}
 
if (CalcTotalWeight & ~CalcTotalWeight & (State == 0))
{
    State = 1
    if(State)
    {
        print("calculating Center of Mass")
    }
    R = entity():getConstraints()
    Psh = 1
    Tcom = vec()
    Tmass = 0
}
if (State)
{
    if(R:entity(Psh))
    {
        Tcom += R:entity(Psh):massCenter() * R:entity(Psh):mass()
        Tmass += R:entity(Psh):mass()
        Psh++
    }
    else
    {
        Tcom = Tcom / ((Psh - 1) * Tmass)
        print("Center of Mass @ " + Tcom:toString())
        print("Total Mass @ " + Tmass)
        State = 0
    }
}
