@name Cyber's Mini C-RAM
@inputs 
@outputs Turret
@persist [Player PlayerTar Plate O]:entity Gyro:angle Ang Y2 Laser Defend Targetmode
@model models/props_phx/construct/metal_plate1.mdl

runOnTick(1)
runOnChat(1)

if(first()){
    Plate = entity()
    O = owner()
    Plate:setMass(50000)
    Plate:setAlpha(0)
    
    hint("Made By Cyber Police",10)
#Holograms# 
       holoCreate(-4)
    holoParent(-4,holoEntity(-2))
    holoScale(-4,vec(1.5,0.5,5))
    holoColor(-4,vec(130,125,105))
    holoModel(-4,"hq_rcube_thin")


    holoCreate(-3)
    holoParent(-3,holoEntity(-2))
    holoScale(-3,vec(1.5,0.5,5))
    holoColor(-3,vec(130,125,105))
    holoModel(-3,"hq_rcube_thin")
    
    holoCreate(-2)
    holoParent(-2,holoEntity(0))
    holoScale(-2,vec(0.3,0.3,0.3))
    holoColor(-2,vec(130,130,130))
    holoModel(-2,"hq_cube")

    holoCreate(-1)
    holoPos(-1,Plate:toWorld(vec(-10,0,20)))
    holoParent(-1,Plate)
    holoScale(-1,vec(1.98,1.98,3))
    holoAng(-1,Plate:toWorld(ang(0,0,0)))
    holoColor(-1,vec(200,200,200))
    holoModel(-1,"hq_cylinder")
    holoMaterial(-1,"models/debug/debugwhite")

    holoCreate(0)
    holoPos(0,Plate:toWorld(vec(-10,0,0)))
    holoParent(0,Plate)
    holoScale(0,vec(2,2,1.3))
    holoAng(0,Plate:toWorld(ang(0,0,0)))
    holoColor(0,vec(130,125,105))
    holoModel(0,"hq_rcube_thin")

    holoCreate(1)
    holoPos(1,Plate:toWorld(vec(7,0,0)))
    holoParent(1,Plate)
    holoScale(1,vec(1.3,2,1))
    holoColor(1,vec(130,125,105))
    holoModel(1,"pyramid")

    holoCreate(2)
    holoModel(2,"hq_cylinder")
    holoPos(2,holoEntity(1):toWorld(vec(0,0,1.85)))
    holoScale(2,vec(0.5,0.5,0.7))
    holoParent(2,holoEntity(1))
    holoColor(2,vec(60,60,60))
    holoMaterial(2,"models/debug/debugwhite")
    #Barrels
    holoCreate(3)
    holoModel(3,"hq_tube")
    holoPos(3,holoEntity(2):toWorld(vec(0,2,15)))
    holoAng(3,holoEntity(2):angles()+ang(0,0,0))
    holoScale(3,vec(0.1,0.1,3))
    holoParent(3,holoEntity(2))
    holoColor(3,vec(60,60,60))

    holoCreate(4)
    holoModel(4,"hq_tube")
    holoPos(4,holoEntity(2):toWorld(vec(0,-2,15)))
    holoAng(4,holoEntity(2):angles()+ang(0,0,0))
    holoScale(4,vec(0.1,0.1,3))
    holoParent(4,holoEntity(2))
    holoColor(4,vec(60,60,60))

    holoCreate(5)
    holoModel(5,"hq_tube")
    holoPos(5,holoEntity(2):toWorld(vec(-2,0,15)))
    holoAng(5,holoEntity(2):angles()+ang(0,0,0))
    holoScale(5,vec(0.1,0.1,3))
    holoParent(5,holoEntity(2))
    holoColor(5,vec(60,60,60))

    holoCreate(6)
    holoModel(6,"hq_tube")
    holoPos(6,holoEntity(2):toWorld(vec(2,0,15)))
    holoAng(6,holoEntity(2):angles()+ang(0,0,0))
    holoScale(6,vec(0.1,0.1,3))
    holoParent(6,holoEntity(2))
    holoColor(6,vec(60,60,60))

    holoCreate(7)
    holoModel(7,"hq_tube")
    holoPos(7,holoEntity(2):toWorld(vec(-1.4,1.4,15)))
    holoAng(7,holoEntity(2):angles()+ang(0,0,0))
    holoScale(7,vec(0.1,0.1,3))
    holoParent(7,holoEntity(2))
    holoColor(7,vec(60,60,60))

    holoCreate(8)
    holoModel(8,"hq_tube")
    holoPos(8,holoEntity(2):toWorld(vec(1.4,-1.4,15)))
    holoAng(8,holoEntity(2):angles()+ang(0,0,0))
    holoScale(8,vec(0.1,0.1,3))
    holoParent(8,holoEntity(2))
    holoColor(8,vec(60,60,60))

    holoCreate(9)
    holoModel(9,"hq_tube")
    holoPos(9,holoEntity(2):toWorld(vec(-1.4,-1.4,15)))
    holoAng(9,holoEntity(2):angles()+ang(0,0,0))
    holoScale(9,vec(0.1,0.1,3))
    holoParent(9,holoEntity(2))
    holoColor(9,vec(60,60,60))

    holoCreate(10)
    holoModel(10,"hq_tube")
    holoPos(10,holoEntity(2):toWorld(vec(1.4,1.4,15)))
    holoAng(10,holoEntity(2):angles()+ang(0,0,0))
    holoScale(10,vec(0.1,0.1,3))
    holoParent(10,holoEntity(2))
    holoColor(10,vec(60,60,60))
    #other
    #holoCreate(11)
    #holoModel(11,"hq_cylinder")
    #holoPos(11,holoEntity(1):toWorld(vec(0,0,20)))
    #holoAng(11,holoEntity(2):angles())
    #holoScale(11,vec(0.6,0.6,0.1))
    #holoParent(11,holoEntity(1))
    #holoColor(11,vec(80,80,80))
    #holoMaterial(11,"models/shiny")

    #holoCreate(12)
    #holoModel(12,"hq_tube_thin")
    #holoPos(12,holoEntity(1):toWorld(vec(0,0,38)))
    #holoAng(12,holoEntity(2):angles())
    #holoScale(12,vec(0.6,0.6,0.7))
    #holoParent(12,holoEntity(1))
    #holoColor(12,vec(80,80,80))
    #holoMaterial(12,"models/shiny")
    
    holoCreate(13)
    holoPos(13,Plate:toWorld(vec(-10,0,-15)))
    holoParent(13,Plate)
    holoScale(13,vec(1.5,1.5,2.7))
    holoAng(13,Plate:toWorld(ang(90,0,0)))
    holoColor(13,vec(125,120,100))
    holoModel(13,"hq_cylinder")
    
    holoCreate(14)
    holoPos(14,Plate:toWorld(vec(2.5,0,-15)))
    holoParent(14,Plate)
    holoScale(14,vec(1,0.7,0.7))
    holoAng(14,Plate:toWorld(ang(0,0,45)))
    holoColor(14,vec(120,115,95))
    holoModel(14,"hq_rcube_thin")
    
    holoCreate(15)
    holoPos(15,Plate:toWorld(vec(-10,15,17)))
    holoParent(15,Plate)
    holoScale(15,vec(0.5,0.5,1))
    holoAng(15,Plate:toWorld(ang(90,0,0)))
    holoColor(15,vec(150,150,150))
    holoModel(15,"hq_rcylinder_thin")
    
    holoCreate(16)
    holoPos(16,Plate:toWorld(vec(-10,12,14)))
    holoParent(16,Plate)
    holoScale(16,vec(0.3,0.5,0.3))
    holoAng(16,Plate:toWorld(ang(90,0,0)))
    holoColor(16,vec(150,150,150))
    holoModel(16,"hq_rcube_thin")
    
    holoCreate(17)
    holoPos(17,Plate:toWorld(vec(-5,15,17)))
    holoParent(17,Plate)
    holoScale(17,vec(0.49,0.49,0.4))
    holoAng(17,Plate:toWorld(ang(90,0,0)))
    holoColor(17,vec(65,65,65))
    holoModel(17,"hq_dome")
    holoMaterial(17,"models/shiny")
    
     holoCreate(18)
    holoModel(18,"hq_cylinder")
    holoPos(18,holoEntity(1):toWorld(vec(0,0,30.5)))
    holoAng(18,holoEntity(2):angles())
    holoScale(18,vec(0.45,0.45,0.6))
    holoParent(18,holoEntity(1))
    holoColor(18,vec(60,60,60))
    holoMaterial(18,"models/debug/debugwhite")
    
    holoCreate(19)
    holoPos(19,Plate:toWorld(vec(7,0,0)))
    holoAng(19,Plate:toWorld(ang(90,0,0)))
    holoParent(19,Plate)
    holoScale(19,vec(0.51,0.51,0.7))
    holoColor(19,vec(130,125,105))
    holoModel(19,"hq_rcube_thin")
    
    holoCreate(20)
    holoPos(20,Plate:toWorld(vec(-10,15,12)))
    holoParent(20,Plate)
    holoScale(20,vec(0.35,0.35,1))
    holoAng(20,Plate:toWorld(ang(90,0,0)))
    holoColor(20,vec(150,150,150))
    holoModel(20,"hq_rcylinder_thin")
    
    holoCreate(21)
    holoPos(21,Plate:toWorld(vec(-6.3,15,12)))
    holoParent(21,Plate)
    holoScale(21,vec(0.34,0.34,0.4))
    holoAng(21,Plate:toWorld(ang(90,0,0)))
    holoColor(21,vec(65,65,65))
    holoModel(21,"hq_cylinder")
    holoMaterial(21,"models/shiny")
    
    holoCreate(22)
    holoPos(22,Plate:toWorld(vec(-10,0,37.99)))
    holoParent(22,Plate)
    holoScale(22,vec(1.98,1.98,1.98))
    holoAng(22,Plate:toWorld(ang(0,0,0)))
    holoColor(22,vec(200,200,200))
    holoModel(22,"hq_dome")
    holoMaterial(22,"models/debug/debugwhite")
    #Holograms#
}
#User#

  S = owner():lastSaid():explode(" ")
    if(S:string(1)=="user"){
        hideChat(1)
        TempPlayer = findPlayerByName(S:string(2))
        if(TempPlayer:isPlayer()){
            Player=TempPlayer
        
        }
    
    
    }
#User#
    
#Target Player#
  S2 = Player:lastSaid():explode(" ")
    if(S2:string(1)=="tar"){
        hideChat(1)
        TempPlayer2 = findPlayerByName(S2:string(2))
        if(TempPlayer2:isPlayer()){
            PlayerTar=TempPlayer2
        
        }
    
    
    }
#Target Player#

#Chat Commands#

if(Player:lastSaid():left(5)=="las"){
    hideChat(1)
    Laser=1
    Targetmode=0
    Defend=0
}
if(Player:lastSaid():left(6)=="defend"){
    hideChat(1)
    Defend=1
    Targetmode=0
    Laser=0
}
if(Player:lastSaid():left(10)=="tarmode"){
    hideChat(1)
    Targetmode=1
    Defend=0
    Laser=0
}
#Chat Commands#

#Firing#
if(Player:keyReload()==1&Laser==1) {
    Turret=1,Ang+=40
     Plate:soundPlay(1,0,"mvm/giant_heavy/giant_heavy_gunspin.wav")
    } 
    else {
        Turret=0,Ang+=0
        soundStop(1)
}

#Target Explosives#
    findIncludeModel("models/props_phx/amraam.mdl")
    findIncludeModel("models/props_phx/torpedo.mdl")
    findIncludeModel("models/props_phx/ww2bomb.mdl")
    findIncludeModel("models/props_phx/mk-82.mdl")
    findIncludeModel("models/props_phx/misc/flakshell_big.mdl")
    findIncludeModel("models/props_c17/oildrum001_explosive.mdl")
    findIncludeModel("models/props_phx/oildrum001.mdl")
    findIncludeModel("models/props_phx/oildrum001_explosive.mdl")
    findIncludeModel("models/props_phx/misc/potato_launcher_explosive.mdl")
    findIncludeModel("models/props_explosive/explosive_butane_can.mdl")
    findIncludeModel("models/props_explosive/explosive_butane_can02.mdl")
    findIncludeModel("models/props_phx/ball.mdl")
    findIncludeModel("models/props_junk/propane_tank001a.mdl")
    findIncludeModel("models/props_junk/gascan001a.mdl")
    findIncludeModel("models/weapons/w_missile.mdl")
    
    findInSphere(Plate:pos(),99999999)
    Target = findClosest(Plate:pos())
#Target Explosives#
if(Defend==1&Target:pos():distance(Plate:pos())<6000|Targetmode==1&PlayerTar:pos():distance(Plate:pos())<3000){
    Turret=1,Ang+=40
     Plate:soundPlay(1,0,"mvm/giant_heavy/giant_heavy_gunspin.wav")}
#Firing#
#C-RAM Angles#
if(Laser==1){ 
P = Plate:elevation(Player:aimPos()) 
Y = Plate:bearing(Player:aimPos())
R = Plate:angles():roll()
}


if(Defend==1){
Plate:setAng((Target:shootPos()-Plate:pos()):toAngle())   
}


if(Targetmode==1){  
Plate:setAng((PlayerTar:shootPos()-Plate:pos()):toAngle())   
}

Y2 = Plate:angles():yaw()#For Holo Legs

holoAng(1,Plate:toWorld(ang(90,0,0)))
holoAng(2,holoEntity(1):toWorld(ang(0,Ang,0)))
holoAng(-2,ang(0,Y2,0))
holoPos(-2,holoEntity(0):toWorld(vec(0,-13,0)))
holoAng(-3,ang(0,Y2,0))
holoPos(-3,holoEntity(-2):toWorld(vec(0,0,-25)))
holoAng(-4,ang(0,Y2,0))
holoPos(-4,holoEntity(-2):toWorld(vec(0,26,-25)))
#C-RAM Angles#
if(dupefinished()){reset()}
