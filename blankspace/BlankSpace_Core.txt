@name BlankSpace Core
@inputs Scr:wirelink UserTrigger:entity
@persist Use Using User:entity Menu Draw SayColor:array
@persist Av1 Av2 Av3
#include "lib/gang/func"
#[
    BlankSpace was founded in 2010 by Scriptis, on a server named 'Nocturnal Gaming.'
    BlankSpace was a small community based on raiding and basing.
    Then, the server was shut down.
    BlankSpace was never heard from again.
    Until now.
]#
if (first()) {
    #include "lib/egp/func"
    SayColor=array(vec(0,255,255),vec(0,255,0),vec(255,0,0))
    function say(S:string, Mode) {
        printColor(SayColor[Mode,vector],"[BlankSpace] "+S)
    }
    say("Starting party",1)
    syncDown() #let's start this party
    Menu=-1
}
if (changed(Scr)) {
    #Number=entity:createWire(entity ent2,string inputname,string outputname)
    entity():createWire(Scr:entity(),"UserTrigger","User")
    say("Auto-wired UserTrigger",1)
}
if (changed(Menu)) {
    Scr:egpClear()
    local I=egpMaxObjects()
    Scr:egpBox(I,vec2(256,256),vec2(512,512))
    Scr:egpMaterial(I,"skybox/sky_dust_hdrrt.vmt")
    Draw=1
}
if (User) {
    if (!Using & User:keyUse() & User:aimEntity()==Scr:entity()) {
        Use=1
        Using=1
    }
    if (!User:keyUse()) {
        Using=0
    }
}
if (DownClk) {
    say("Party started.",2)
    Menu=0
}
if (Menu==-1) {
    if (Draw) {
        Scr:egpBox(1,vec2(256,256),vec2(512,50))
        Scr:egpColor(1,vec(172,0,0))
        Scr:egpText(2,"Loading...",vec2(256,256))
        Scr:egpAlign(2,1,1)
        Scr:egpSize(2,40)
    }
}
if (Menu==0) {
    if (Draw) {
        local Offset=50
        Scr:egpText(1,"B",vec2(256,256-Offset))
        Scr:egpSize(1,150)
        Scr:egpAlign(1,2,1)
        Scr:egpText(2,"S",vec2(256,256-Offset))
        Scr:egpSize(2,150)
        Scr:egpAlign(2,0,1)
        Scr:egpColor(1,vec(1,1,1))
        Scr:egpText(3,"Blank",vec2(256,280-Offset))
        Scr:egpAlign(3,2,0)
        Scr:egpSize(3,50)
        Scr:egpText(4,"Space",vec2(256,280-Offset))
        Scr:egpAlign(4,0,0)
        Scr:egpColor(4,vec(1,1,1))
        Scr:egpSize(4,50)
        Scr:egpText(5,"Core Access",vec2(256,280-Offset+35))
        Scr:egpColor(5,vec(255,255,0))
        Scr:egpSize(5,35)
        Scr:egpAlign(5,1,0)
        Scr:egpText(6,"",vec2(256,280-Offset+60))
        Scr:egpSize(6,30)
        Scr:egpAlign(6,1,0)
        Scr:egpColor(6,vec(255,0,0))
    }
    if (findCanQuery()) {
        findByClass("player")
        User=findClosest(Scr:entity():pos())
    }
    if (Use) {
        if (perm(User,"Read")) {
            Menu=1
        } else {
            Scr:egpSetText(6,"ACCESS DENIED; INSUFFICIENT RANK")
            timer("derp1",2000)
        }
    }
    if (clk("derp1")) {
        Scr:egpSetText(6,"")
    }
            
}
if (Menu!=0 & Draw) {
        Scr:egpText(1,"B",vec2(25,15))
        Scr:egpSize(1,30)
        Scr:egpAlign(1,2,1)
        Scr:egpText(2,"S",vec2(25,15))
        Scr:egpSize(2,30)
        Scr:egpAlign(2,0,1)
        Scr:egpColor(1,vec(1,1,1))
        Scr:egpText(3,"Blank",vec2(25,25))
        Scr:egpAlign(3,2,0)
        Scr:egpSize(3,10)
        Scr:egpText(4,"Space",vec2(25,25))
        Scr:egpAlign(4,0,0)
        Scr:egpColor(4,vec(1,1,1))
        Scr:egpSize(4,10)
        Scr:egpText(5,"BlankSpace Core",vec2(505,0))
        Scr:egpAlign(5,2,0)
        Scr:egpText(6,"Build 3",vec2(505,15))
        Scr:egpAlign(6,2,0)
        Scr:egpBox(7,vec2(30,512-20),vec2(54,27))
        Scr:egpColor(7,vec(24,24,24))
        Scr:egpAlpha(7,128)
        Scr:egpText(8,"Exit",Scr:egpPos(7))
        Scr:egpAlign(8,1,1)
        Scr:egpSize(8,30)
    }
    if (Menu!=0) {
        if (pIR(7)) {
            Scr:egpColor(7,vec(128,128,128))
            if (Use) {
                User=noentity()
                gmenu(0)
            }
        } else {
            Scr:egpColor(7,vec(24,24,24))
        }
    }
if (Menu==1) {
    if (Draw) {
        #print("one")
        #Write to database menu
        Scr:egpBox(10,vec2(256,256),vec2(55,55))
        Scr:egpBox(11,vec2(256,256),vec2(50,50))
        Scr:egpColor(10,vec(128,128,128))
        Scr:egpColor(11,vec(172,172,172))
        Scr:egpBox(12,Scr:egpPos(11),vec2(45,45))
        Scr:egpMaterial(12,"gui/silkicons/box")
        Scr:egpText(13,"Database",Scr:egpPos(11)+Scr:egpSize(11)*vec2(0,0.5))
        Scr:egpSize(13,12)
        Scr:egpAlign(13,1,2)
        
        
        #Global settings
        Scr:egpBox(14,vec2(256-100,256),vec2(55,55))
        Scr:egpBox(15,Scr:egpPos(14),vec2(50,50))
        Scr:egpBox(16,Scr:egpPos(15),Scr:egpSize(15))
        Scr:egpMaterial(16,"gui/silkicons/plugin")
        Scr:egpColor(14,vec(128,128,128))
        Scr:egpColor(15,vec(172,172,172))
        Scr:egpText(17,"Options",Scr:egpPos(15)+Scr:egpSize(15)*vec2(0,0.5))
        Scr:egpSize(17,15)
        Scr:egpAlign(17,1,2)
        
        
        #User List
        Scr:egpBox(18,vec2(256+100,256),vec2(55,55))
        Scr:egpBox(19,Scr:egpPos(18),vec2(50,50))
        Scr:egpBox(20,Scr:egpPos(19),Scr:egpSize(19))
        Scr:egpMaterial(20,"gui/silkicons/user")
        Scr:egpText(21,"Users",Scr:egpPos(20)+Scr:egpSize(20)*vec2(0,0.5))
        Scr:egpAlign(21,1,2)
        Scr:egpColor(18,vec(128,128,128))
        Scr:egpColor(19,vec(172,172,172))
        
        if (!perm(User,"Write")) {
            Scr:egpColor(13,vec(255,0,0))
            Scr:egpColor(12,vec(0,0,0))
            Scr:egpColor(11,vec(64,64,64))
            Scr:egpColor(10,vec(172/2,172/2,172/2))
            
            Scr:egpColor(17,vec(255,0,0))
            Scr:egpColor(14,vec(64,64,64))
            Scr:egpColor(15,vec(172/2,172/2,172/2))
            Scr:egpColor(16,vec(0,0,0))
            
            Av1=0
            Av2=0
        }
        Av3=perm(User,"Exile")        
    }
}
runOnChat(1)
if (chatClk() & perm(lastSpoke(),"Write")) {
    local CMD=lastSaid()
    local Ply=lastSpoke()
    if (CMD:sub(1,1)=="-") {
        local All=gTable(GangName)
        local CMD=CMD:sub(2)
        local Args=CMD:lower():explode(" ")
        if (Args[1,string]=="set" & perm(Ply,"*")) {
            local Players=players()
            for (I=1, Players:count()) {
                if (Players[I,entity]:name():lower():matchFirst(Args[2,string])) {
                    All[Players[I,entity]:steamID(),number]=Args[3,string]:toNumber()
                    #print(All[Players[I,entity]:steamID(),number])
                    print("Player "+(Players[I,entity]:name())+" ("+(Players[I,entity]:steamID())+") has rank "+Args[3,string])
                }
            }
        }
        
        if (Args[1,string]=="sync") {
            syncUp()
        }
    }
    
}

Draw=0
Use=0
interval(100)
