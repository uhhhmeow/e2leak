@name BlankSpace InfoGUI
@persist WasLooking
interval(100)
#include "lib/gang/func"
All=gTable("Thieves",1)
Target=owner():aimEntity()
if (Target:isPlayer() & getRank(Target)>1) {
    print(_HUD_PRINTCENTER,Target:name()+"\n"+rankName(getRank(Target))+" ("+getRank(Target)+")\n"+(Target:health()+" HP"))
    WasLooking=1
} elseif (WasLooking) {
    print(_HUD_PRINTCENTER," ")
    WasLooking=0
}
