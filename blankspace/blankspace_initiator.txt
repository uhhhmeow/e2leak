@name BlankSpace Initiator
@inputs Scr:wirelink User2:entity
@persist User:entity TXT:string
runOnFile(1)
runOnChat(1)

if (TXT:length()==0) { fileLoad("blankmembers.txt")}
Scr:egpAlign(1,1,2)
Scr:egpAlign(1,1,1)
interval(100)
fileLoad("blankmembers.txt")
if (first() | changed(Scr)) {
    Scr:egpClear()
    Scr:entity():setAlpha(256)
    Scr:entity():setColor(vec(255,255,255))
    Scr:entity():setMaterial("models/shiny")
    Scr:egpBox(2,vec2(256,256),vec2(512,512))
    Scr:egpMaterial(2,"maps/noicon")
    Scr:egpText(3,"BlankSpace Initiation",vec2(256,0))
    Scr:egpAlign(3,1)
    Scr:egpSize(3,30)
    Terms=array("You may never negatively affect another member under any circumstance.",
    "You will follow given orders while participating in a group event.",
    "There are no second chances. Once you're in, you can't get out.",
    "Breaking these terms will result in severe punishment unless accidental.")
    for (I=1, Terms:count()) {
        Scr:egpText(3+I,I+". "+Terms[I,string],vec2(3,I*20+75))
        Scr:egpSize(3+I,15)
    }
    Scr:egpText(11,"Press the screen to accept and join.",vec2(256,450))
    Scr:egpAlign(11,1,1)

    
}
    if (TXT) {
    Scr:egpText(10,"CURRENT MEMBERS: "+(TXT:explode("
"):count()),vec2(256,350))
Scr:egpAlign(10,1,1)
Scr:egpColor(10,vec(255,0,0))
Scr:egpSize(10,30)
}
if (fileLoaded()) {
if (first()) {
    print(fileRead())
    TXT=fileRead() 
    TXT=TXT+"
"}
if (User2) {
    User=User2
    if (!(TXT:matchFirst(User:steamID())) & User) {
        Scr:egpText(1,User:name()+", you have been initiated.",vec2(256,500))
        Scr:egpAlign(1,1,1)
         print(User:steamID())
    TXT=TXT+"
"+User:steamID()
    print("Added.")
}
    }
if (owner():lastSaid()=="-initiate" & chatClk(owner())) {
    hideChat(1)
    fileAppend("blankmembers.txt",TXT)
    print("Initiated.")
    selfDestructAll()
    
}
entity():setAlpha(256)
}
