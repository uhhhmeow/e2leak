@name Failcake Radar V1
@inputs 
@outputs I
@persist A:array T
@trigger all

interval(100)
RadarPos = entity():pos()
DisplayScala = 30
RRange = 5000

if(first()){
holoEntity(-2):soundPlay(1000,1000,"")
}

if(findCanQuery()){
findIncludeClass("player")
findInSphere(entity():pos(),5000)
A = findToArray()
}



holoCreate(-1)
holoModel(-1,"cylinder")
holoPos(-1,entity():pos()+vec(0,0,20))
holoColor(-1,vec(1,1,1))
holoAlpha(-1,100)
holoScaleUnits(-1,vec(DisplayScala*2,DisplayScala*2,0.1))
holoMaterial(-1,"")


for(I=1,A:count()){
    
PlPos = A[I,entity]:boxCenter() + A[I,entity]:pos()
Dist = PlPos:distance(RadarPos)
Dir = PlPos - RadarPos
Calculated = Dist*DisplayScala/RRange
Dir = Dir:normalized() * Calculated + RadarPos

holoCreate(I)  
holoModel(I,"icosphere3")
holoPos(I,Dir+vec(0,0,20))
holoColor(I,vec(255,1,1))
holoScaleUnits(I,vec(1,1,1))
}


