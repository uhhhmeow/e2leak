@name Gun Store Info
@inputs 
@outputs I1:string I2:string I3:string I4:string I5:string I6:string
@outputs P1 P2 P3 P4 P5 P6
@persist Prices:gtable Items:gtable 
@trigger 

if(first() | duped()){
Prices = gTable("Prices", 0)
Items = gTable("Items", 0)

}

runOnChat(1)
if(chatClk(owner())){
    Chat = owner():lastSaid():explode(" ")
    if(Chat:string(1)=="/i"){
        ioSetOutput("I"+Chat:string(2), Chat:string(3))
        ioSetOutput("P"+Chat:string(2), Chat:string(4):toNumber())
        Items[Chat:string(2):toNumber(), string] = Chat:string(3)
        Prices[Chat:string(2):toNumber(), number] = Chat:string(4):toNumber()
        print(Items[1, string])
        }
    
    }
