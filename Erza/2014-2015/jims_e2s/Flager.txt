@name Flager
@inputs Flag:entity C1:entity C2:entity
@outputs 
@persist Point:entity 
@trigger 

if(first() | duped()){
    Point = entity():isWeldedTo()
    Point:setMaterial("models/alyx/emptool_glow")
    entity():setAlpha(0)
    }

interval(10)

for(I=1, findInBox(C1:pos(),C2:pos())){
    if(findResult(I)==Flag){
        Point:setColor(vec(255,0,0))
    }else{
    Point:setColor(vec(255,255,255))
    }
    }
