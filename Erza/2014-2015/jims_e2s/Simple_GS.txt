@name Simple GS
@inputs EGP:wirelink
@outputs [U1 U2 U3 U4]
@outputs [I1 I2 I3 I4]:string
@outputs [P1 P2 P3 P4]
@persist [Items Prices]:array 


if(first() | duped()){
    EGP:egpClear()
    Items = array()
    Prices = array()
    for(I=1, 4){EGP:egpSize(I, 40)}
    }
runOnChat(1)
if(chatClk(owner())){
    Chat = owner():lastSaid():explode(" ")
    if(Chat:string(1)=="/i"){
        ioSetOutput("I"+Chat:string(2), Chat:string(3))  
        ioSetOutput("P"+Chat:string(2), Chat:string(4):toNumber()) 
        Items[Chat:string(2):toNumber(), string] = Chat:string(3)
        Prices[Chat:string(2):toNumber(), number] = Chat:string(4):toNumber()
        }
    for(I=1, 4){
        EGP:egpText(I, Items[I, string], vec2(30, 100+60*I))
        }
    }
