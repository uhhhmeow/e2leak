@name Spooky suit for Erza
@inputs 
@outputs 
@persist 
@trigger 

Pos = entity():pos() + vec(0,0,70)

holoCreate(1)
holoModel(1, "hq_rcylinder_thick")
holoPos(1, Pos) 

holoCreate(2)
holoModel(2, "hq_rcylinder_thick")
holoPos(2, Pos-vec(0,0,2))
holoScale(2, vec(1.3,1.3,0.5))

holoCreate(3,Pos)
holoModel(3,"hq_dome")
