@name EGP Basics
@inputs EGP:wirelink User:wirelink 
@outputs B1
@persist 

#The screen its self example: The text and green around the outside.
if(first() || duped()){
    EGP:egpClear()
    }
if(first() || duped()){
    #Creating The Box
    EGP:egpLine(1, vec2(0,0), vec2(10000, 0))
    EGP:egpLine(2, vec2(0,0), vec2(0,100000))
    EGP:egpLine(3, vec2(510,0), vec2(510, 10000))
    EGP:egpLine(4, vec2(0, 510), vec2(10000, 510))
    #Sizing the Lines
    EGP:egpSize(1, 40)
    EGP:egpSize(2, 30)
    EGP:egpSize(3, 30)
    EGP:egpSize(4, 170)
    for(I=1, 4){
        EGP:egpColor(I, vec(0,255,0))
        }
    }

EGP:egpText(5, "Welcome To Insanity's Gun Shop!", vec2(140,1))

EGP:egpColor(5,vec(0,0,0))

EGP:egpText(6, "~~~~~~Instructions~~~~~~", vec2(140,430))

EGP:egpColor(6,vec(65,0,255))

EGP:egpText(7, "Use up and down arrows to select gun!", vec2(20,450))

EGP:egpColor(7,vec(255,0,0))

EGP:egpText(8, "Then put money in money pot!", vec2(20,470))

EGP:egpColor(8,vec(255,0,0))

EGP:egpText(9, "Then push start!", vec2(20,490))

EGP:egpColor(9,vec(255,0,0))


