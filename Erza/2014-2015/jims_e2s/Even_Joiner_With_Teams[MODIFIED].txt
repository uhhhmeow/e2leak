@name Even Joiner With Teams[MODIFIED by Jim Dangle]
@persist Max1 Max2 Current1 Current2 Name:string Joined:array Red:vector Blue:vector

if(first() | duped()){hint("Created by the Dangler", 3)}
runOnChat(1)
Rand = vec(randint(200), randint(200), 30)

if(first()){
    Current1 = 1
    Current2 = 1
    Max1 = 16
    Max2 = 16
    RedN = "RED"
    BlueN = "BLUE"
    }
if(chatClk(lastSpoke())){
    if(Current1!=Max1){
    Join = lastSpoke():lastSaid():upper():explode(" ")
    if(Join:string(1)=="/EVENTJOIN" & Join:string(2)==RedN){
    hideChat(1)
    #lastSpoke():teleport(RedN+Rand)
    Current1++
    Joined:pushEntity(lastSpoke())
    print(Joined)
    }
}
}
