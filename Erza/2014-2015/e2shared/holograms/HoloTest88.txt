@name HoloTest88

if(first()) {

    holoCreate(1)
    holoPos(1, entity():toWorld(vec(0,0,0)))
    holoAng(1, entity():toWorld(ang(0,180,180)))
    holoModel(1, "models/beer/wiremod/gate_e2.mdl")
    holoParent(1, entity())

    holoCreate(3)
    holoPos(3, entity():toWorld(vec(20.509,0.781,12.257)))
    holoAng(3, entity():toWorld(ang(89.995,-177.544,180)))
    holoModel(3, "models/jaanus/wiretool/wiretool_grabber_forcer.mdl")
    holoColor(3, vec4(109,109,109,255))
    holoMaterial(3, "models/debug/debugwhite")
    holoParent(3, 1)

    holoCreate(4)
    holoPos(4, entity():toWorld(vec(8.002,0.257,12.273)))
    holoAng(4, entity():toWorld(ang(-0.005,2.456,-0.784)))
    holoModel(4, "models/mechanics/robotics/a1.mdl")
    holoColor(4, vec4(109,109,109,255))
    holoMaterial(4, "models/debug/debugwhite")
    holoParent(4, 1)

    holoCreate(6)
    holoPos(6, entity():toWorld(vec(0.557,-14.061,12.281)))
    holoAng(6, entity():toWorld(ang(0.023,2.452,0.006)))
    holoModel(6, "models/sprops/geometry/fhex_36.mdl")
    holoColor(6, vec4(109,109,109,255))
    holoMaterial(6, "models/debug/debugwhite")
    holoParent(6, 1)

    holoCreate(8)
    holoPos(8, entity():toWorld(vec(-0.649,13.888,12.283)))
    holoAng(8, entity():toWorld(ang(0.016,-177.553,0.025)))
    holoModel(8, "models/sprops/geometry/fhex_36.mdl")
    holoColor(8, vec4(109,109,109,255))
    holoMaterial(8, "models/debug/debugwhite")
    holoParent(8, 1)

    holoCreate(9)
    holoPos(9, entity():toWorld(vec(0.595,-15.92,12.287)))
    holoAng(9, entity():toWorld(ang(43.77,2.543,90.062)))
    holoModel(9, "models/sprops/misc/domes/size_1/dome_12x6.mdl")
    holoColor(9, vec4(109,109,109,255))
    holoMaterial(9, "models/debug/debugwhite")
    holoParent(9, 1)

}
