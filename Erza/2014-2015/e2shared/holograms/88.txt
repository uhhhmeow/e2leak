@name 88

if(first()) {

    holoCreate(2)
    holoPos(2, entity():toWorld(vec(-13.496,0.317,-0.037)))
    holoAng(2, entity():toWorld(ang(-39.655,88.693,-179.983)))
    holoModel(2, "models/sprops/misc/alphanum/alphanum_e.mdl")
    holoColor(2, vec4(109,109,109,255))
    holoMaterial(2, "models/debug/debugwhite")
    holoParent(2, 1)

    holoCreate(3)
    holoPos(3, entity():toWorld(vec(20.512,-0.487,-0.03)))
    holoAng(3, entity():toWorld(ang(89.987,178.706,180)))
    holoModel(3, "models/jaanus/wiretool/wiretool_grabber_forcer.mdl")
    holoColor(3, vec4(109,109,109,255))
    holoMaterial(3, "models/debug/debugwhite")
    holoParent(3, 1)

    holoCreate(5)
    holoPos(5, entity():toWorld(vec(7.997,-0.192,-0.01)))
    holoAng(5, entity():toWorld(ang(0.011,-1.294,-0.784)))
    holoModel(5, "models/mechanics/robotics/a1.mdl")
    holoColor(5, vec4(109,109,109,255))
    holoMaterial(5, "models/debug/debugwhite")
    holoParent(5, 1)

    holoCreate(7)
    holoPos(7, entity():toWorld(vec(-0.37,-13.979,0)))
    holoAng(7, entity():toWorld(ang(0.039,-1.341,0.017)))
    holoModel(7, "models/sprops/geometry/fhex_36.mdl")
    holoColor(7, vec4(109,109,109,255))
    holoMaterial(7, "models/debug/debugwhite")
    holoParent(7, 1)

    holoCreate(8)
    holoPos(8, entity():toWorld(vec(-0.019,0.027,12.281)))
    holoAng(8, entity():toWorld(ang(0,0,0)))
    holoModel(8, "models/beer/wiremod/gate_e2.mdl")
    holoParent(8, 1)

}