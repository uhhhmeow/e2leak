################
# > Help Holos: A guide to holograms for beginners.
# > How to use: Spawn it and look at the code for a tutorial on it.
################

@name Holograms Guide by Neo

# First, create the hologram with this command.
holoCreate(0)

# This command lets you set the size using the X,Y,Z axis.
holoScale(0, (vec(3,3,3)))

# This lets you change the color of the hologram.
holoColor(0, (vec(255,0,0)))

# This lets you set the position of the hologram.
holoPos(0, entity():toWorld(vec(0,0,0)))

# This lets you set the shape of the hologram.
holoModel(0, "hqsphere2")

# This lets you change the angle the hologram is at.
holoAng(0, ang(0,0,0))

# This lets you set what the hologram is attached to.
holoParent(0, entity())

# This lets you set the material used on the hologram.
# Note: To make the material perfectly clear, set the colour
# to 255,255,255
holoMaterial(0, "models/rendertarget")

# This makes the hologram see through. Noraml is 255.
holoAlpha(0, 200)

# Add a dropshadow onto it for extra realism.
holoShadow(0, 1)


#####################
#TO CREATE YOUR OWN:#
#####################
# You will notice that the number straight after the command
# is different. This is because it is a different hologram.
# The number is like a name for the hologram, so you can choose
# which hologram you want to affect.

holoCreate(1)
holoScale(1, (vec(1,2,3)))
holoColor(1, (vec(255,255,255)))
holoPos(1, entity():toWorld(vec(0,0,0)))
holoAng(1, (ang(0,0,0)))
holoParent(1, entity())
holoMaterial(1, "models/rendertarget")
holoAlpha(1, 200)
holoShadow(1, 1)

# Change any of the factors or remove the commands if its not needed.
# Materials can be found in the material tool.
# Models can be found with other help on gmodwiki.
