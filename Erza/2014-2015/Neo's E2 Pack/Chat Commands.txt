################
# > Chat Commands: Lets you run console commands from chat.
# > How to use: Type '!command'. Example: '!+forward' will make you walk forward.
################

@name Chat Commands
@outputs Cmd:string S2 S3 S4

runOnChat(1)

LS = owner():lastSaid():explode(" ")

if (LS:string(2) != "") { S2 = 1 }
if (LS:string(3) != "") { S3 = 1 }
if (LS:string(4) != "") { S4 = 1 }
if (LS:string(2) == "") { S2 = 0 }
if (LS:string(3) == "") { S3 = 0 }
if (LS:string(4) == "") { S4 = 0 }

if ((S2 == 1) & (S3 == 0)) {
    Cmd = LS:string(1):right(LS:string(1):length()-1)+" "+LS:string(2)
}
if ((S3 == 1) & (S4 == 0)) {
    Cmd = LS:string(1):right(LS:string(1):length()-1)+" "+LS:string(2)+" "+LS:string(3)
}
if (S4 == 1) {
    Cmd = LS:string(1):right(LS:string(1):length()-1)+" "+LS:string(2)+" "+LS:string(3)+" "+LS:string(4)
}
if ((S2+S3+S4) == 0) {
    Cmd = LS:string(1):right(LS:string(1):length()-1)
}

if (chatClk(owner())&(LS:string(1):left(1) == "!")) {
    concmd(Cmd)
}
