################
# > Jihad Bomb: The Jihad Bomb recreated in E2.
# > How to use: Press E for the taunt, press right click to explode!
################

@name Jihad Bomb by Runeneo
@outputs Taunt

interval(10)

if (first()) {
    concmd("gm_spawn models/props_phx/mk-82.mdl")
    print("You are a terrorist! People fear you!")
    print("Hold down E for your taunt!")
    print("Then press right click to explode!")
}

O = owner():pos()
Fire = (owner():keyUse())
Taunt = (owner():keyAttack2())
findIncludePlayerProps(owner())
findByClass("prop_physics")
OwnerProps=findToArray()

for(Index=1, OwnerProps:count()) {
    
Ex=OwnerProps[Index,entity]

if (Fire == 0) {
Ex:setAlpha(0)
Ex:applyForce(((O-Ex:pos()+vec(0,100,110))*10-Ex:vel())*Ex:mass())
}

if (Fire == 1) {
Ex:setMass(50000)
Ex:applyForce(((O-Ex:pos()+vec(0,100,-110))*10)*Ex:mass()) 
}

if ($Taunt & Taunt) {
owner():soundPlay("siege/jihad.wav",0,"siege/jihad.wav")
}
}
