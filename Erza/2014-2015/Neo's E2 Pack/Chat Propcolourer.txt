################
# > Chat PropColourer: Colours all of your props through chat.
# > How to use: Type '/list' to see all colours. Example of use: '/red' will colour all of your props red.
################

@name Prop Colorer
@outputs R G B
@persist OwnerProps:array R G B A

runOnChat(1)

S=lastSaid()

X=random(0,255)
Y=random(0,255)
Z=random(0,255)
  
if (first()) { R=255 G=255 B=255 A=255 }

# Colours

if (S=="/red") { R=255 G=0 B=0 }
if (S=="/blue") { R=0 G=0 B=255 }
if (S=="/green") { R=0 G=255 B=0 }
if (S=="/plain") { R=255 G=255 B=255 }
if (S=="/yellow") { R=255 G=255 B=0 }
if (S=="/black") { R=0 G=0 B=0 }
if (S=="/purple") { R=255 G=0 B=255 }
if (S=="/orange") { R=250  G=125 B=125 }
if (S=="/pink") { R=255 G=125 B=125 }
if (S=="/lightblue") { R=0 G=255 B=255 }
if (S=="/darkblue") { R=0 G=0 B=128 }
if (S=="/darkred") { R=128 G=0 B=0 }
if (S=="/darkgreen") { R=0 G=128 B=0 }
if (S=="/indigo") { R=75 G=0 B=130 }
if (S=="/crimson") { R=220 G=20 B=60 }
if (S=="/emerald") { R=0 G=201 B=87 }    
if (S=="/limegreen") { R=50 G=205 B=50 }                             
if (S=="/gold") { R=255 G=215 B=0 }             
if (S=="/dusk") { R=150 G=100 B=50 }
if (S=="/random") { R=X G=Y B=Z }
if (S=="/invisible") { R=R G=G B=B A=0 }
if (S=="/visible") { R=R G=G B=B A=255 }
if (S=="/list") {
    print(":::---Colours:---:::")
    print("red, blue, green, plain, yellow, black")
    print("purple, orange, pink, lightblue, darkblue, darkred")
    print("darkgreen, indigo, crimson, emerald, limegreen")
    print("gold, maroon, dusk, random, invisible, visible")
}    
else { R=R G=G B=B A=A }

findByClass("prop_physics")
OwnerProps=findToArray()

for(Index=1, OwnerProps:count()) {
    
    Prop=OwnerProps[Index,entity]
    Prop:setColor(vec(R,G,B))
    Prop:setAlpha(A)

}
