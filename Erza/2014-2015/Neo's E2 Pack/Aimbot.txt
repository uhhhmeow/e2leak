################
# > Aimbot: A simple aimbot in E2.
# > How to use: Spawn it and you will target the nearest player.
################

@name Aimbot
@persist Player:entity Angle:angle

interval(10)

if (first()|duped()) {
    holoCreate(1)
    holoAlpha(1,0)
    holoPos(1,owner():pos())
    owner():parentTo(holoEntity(1))
}

findExcludePlayer(owner())
findByClass("player*")
Players = findToArray()
Player = findClosest(owner():pos())
Pos = Player:pos()+vec(0,100,0)
holoPos(1,Pos)
Angle = (owner():pos()-(Player:pos()-vec(0,0,20))):toAngle()
holoAng(1,Angle+ang(90,0,0))
