################
# > Controlledball V2: Some balls that you can control.
# > How to use: Press E to make them follow your aim.
################

@name The BallZ
@persist T Blocka:entity Blockb:entity

F = entity()
O = owner()

runOnTick(10)

if (first()) {
    Blocka = propSpawn("models/hunter/blocks/cube025x025x025.mdl",owner():aimPos(),0)
    Blockb = propSpawn("models/hunter/blocks/cube025x025x025.mdl",owner():aimPos(),0)
    holoCreate(1)
    holoCreate(11)
    holoCreate(2)
    holoCreate(12)
    
    holoEntity(1):setTrails(10,0,0.5,"trails/electric",vec(0,0,255),255)
    holoEntity(2):setTrails(10,0,0.5,"trails/electric",vec(0,0,255),255)
}

for(Index=1, 2) {

Scale = 1
Speed = 2
Radius = 60

A = random(0,360)
B = random(0,360)
C = random(0,360)
D = random(0,255)
E = random(0,255)
G = random(0,255)
Fire = owner():keyUse()

T+=Speed
X=cos(T)*Radius
Y=sin(T)*Radius
Z=110

XA=0-X
YA=0-Y
ZA=Z

if (Fire == 1) {
    if (Index == 1) {
        Blocka:setMass(50000)
        Blocka:applyForce((O:aimPos() - Blocka:pos())*10000)
    }
    if (Index == 2) {
        Blockb:setMass(50000)
        Blockb:applyForce((O:aimPos() - Blockb:pos())*10000)    
    }
}
if (Fire == 0) {
    if (Index == 1) {
        Blocka:applyForce(((O:pos()-Blocka:pos()+vec(X,Y,Z))*10-Blocka:vel())*Blocka:mass())
    }
    if (Index == 2) {
        Blockb:applyForce(((O:pos()-Blockb:pos()+vec(XA,YA,ZA))*10-Blockb:vel())*Blockb:mass())
    }
}

# HoloBall

if (Index == 1) {

holoPos(Index, Blocka:pos())
holoParent(Index, Blocka)
holoModel(Index, "hqsphere2")
holoColor(Index, (vec(255,0,0)))
holoScale(Index, (vec(2,2,2)*Scale))

holoPos(Index+10, Blocka:pos())
holoParent(Index+10, Blocka)
holoModel(Index+10, "hqsphere2")
holoColor(Index+10, (vec(D,E,0)))
holoScale(Index+10, (vec(2.6,2.6,2.6)*Scale))
holoAlpha(Index+10, 200)


}

if (Index == 2) {
    
holoPos(Index, Blockb:pos())
holoParent(Index, Blockb)
holoModel(Index, "hqsphere2")
holoColor(Index, (vec(255,0,0)))
holoScale(Index, (vec(2,2,2)*Scale))

holoPos(Index+10, Blockb:pos())
holoParent(Index+10, Blockb)
holoModel(Index+10, "hqsphere2")
holoColor(Index+10, (vec(D,E,0)))
holoScale(Index+10, (vec(2.6,2.6,2.6)*Scale))
holoAlpha(Index+10, 200)

}

}
