################
# > Fireworks: A firework!
# > How to use: Spawn it and go 'oooh, aaah'.
################

@name zUmg! It'z a fIrEwUrK bY nEo!
@outputs Colour:vector Speed I Explode

interval(10)

if (first()|duped()) {
    holoCreate(1)
    holoModel(1,"hqcylinder2")
    holoScale(1,vec(0.5,0.5,1.5))
    holoMaterial(1,"phoenix_storms/stripes")
    holoPos(1,entity():pos()+vec(0,0,24))
    holoCreate(2)
    holoModel(2,"cone")
    holoScale(2,vec(0.7,0.7,0.5))
    holoMaterial(2,"phoenix_storms/stripes")
    holoPos(2,entity():pos()+vec(0,0,35))
    holoCreate(3)
    holoScale(3,vec(0.1,0.1,1.5))
    holoPos(3,entity():pos()+vec(2,0,9))
    holoMaterial(3,"phoenix_storms/wood")
    Colour=vec(random(255),random(255),random(255))
    holoColor(1,Colour)
    holoColor(2,Colour)
    holoEntity(3):setTrails(10,50,2,"trails/smoke",vec(100,100,100),255)
    holoParent(2,holoEntity(1))
    holoParent(3,holoEntity(1))
    holoEntity(1):soundPlay("launch",0,"weapons/rpg/rocket1.wav")
}

if (!Explode) {
    Speed+=0.3
    holoPos(1,holoEntity(1):pos()+vec(0,0,Speed))
}

if (!Explode&holoEntity(1):pos():z() > 6000) {
    soundPurge()
    holoDelete(1)
    holoDelete(2)
    holoDelete(3)
    Explode=1
}

if (changed(Explode)&Explode) {
    for (I=1,30) {
        holoCreate(I)
        holoAlpha(I,0)
        holoPos(I,entity():pos()+vec(0,0,6000))
        holoAng(I,ang(random(360),random(360),random(360)))
        holoEntity(I):setTrails(50,1,5,"trails/smoke",vec(random(255),random(255),random(255)),255)
    }
    holoEntity(1):soundPlay("explode",0,"weapons/explode3.wav")
}

if (Explode) {
    for (I=1,30) {
        holoPos(I,holoEntity(I):pos()+holoEntity(I):angles():forward()*30)
    }
}
