@name Echo Eye
@inputs E:wirelink
@outputs 
@persist G T ECHO JJ
@trigger 

if(first()){
    E:egpClear()
    E:egpBox(1,vec2(1000,1000),vec2(10000,10000))
E:egpPos(1,vec2(0,0))
E:egpBox(2,vec2(1000,1000),vec2(10000,10000))
E:egpPos(2,vec2(0,0))
E:egpColor(1,vec(82,105,187))
E:egpColor(2,vec(0,0,0))
E:egpOrder(1,2)
E:egpOrder(2,1)
E:egpMaterial(1,"gui/center_gradient")
E:egpMaterial(2,"gui/center_gradient")
E:egpAlpha(1,0)
E:egpAlpha(2,0)
JJ=0
E:egpBox(3,vec2(850,350),vec2(300,400))
#E:egpPos(3,vec2(850,350))
E:egpBox(4,vec2(850,350),vec2(320,420))
#E:egpPos(4,vec2(850,350))
E:egpMaterial(3,"")
E:egpColor(3,vec4(252,203,96,255))
E:egpColor(4,vec4(0,0,0,255))
E:egpAlpha(3,100)
E:egpAlpha(4,50)
E:egpOrder(3,3)
E:egpOrder(4,1)

T=0
G=0
ECHO=0
}

runOnTick(1)
if(changed(owner():keyZoom())){
    
T++
}

if(T>2){
      E:egpAlpha(1,150)
E:egpAlpha(2,100)

ECHO=1 }
if(T>4){
      E:egpAlpha(1,0)
E:egpAlpha(2,0)  
ECHO=0
T=0
}

if(changed(owner():keyZoom())){
    G++
}
if(G>2){
    if(ECHO==1){
        if(owner():aimEntity():isValid()){
            if(changed(owner():keyZoom())){
        
         H = owner():aimEntity()
        if(!changed(H)){
    E:egpAlpha(3,100)  
    E:egpAlpha(4,50)  
    E:egpAlpha(5,255)
    E:egpAlpha(6,255)
    E:egpAlpha(7,255)
    E:egpAlpha(8,255)
    if(H:isPlayer()){
        E:egpFont(5,"Coolvetica",30)
        E:egpFont(6,"Coolvetica",20)
        E:egpFont(7,"Coolvetica",20)
        E:egpFont(8,"Coolvetica",15)
        E:egpText(5,"TARGET: Player " + H:name(),vec2(725,175))
        E:egpText(6,"HP: " +H:health(),vec2(725,250))
        E:egpText(7,"ARMOR: " +H:armor(),vec2(725,275))
        if(H:name():find("Erza")){
            E:egpText(8,"Very Dangerous, report to Helios official",vec2(725,300))
        }
    
}
if(!H:isPlayer()){
    E:egpText(5,"TARGET: Entity " + H:type(),vec2(725,175))
     E:egpText(6,"MODEL: " +H:model(),vec2(725,250))
        E:egpText(7,"OWNER: " +H:owner():toString(),vec2(725,275))
}}}}}}
if(G>4 | ECHO==0){
    
    E:egpAlpha(3,0)  
    E:egpAlpha(4,0)  
    E:egpAlpha(5,0)
    E:egpAlpha(6,0)
    E:egpAlpha(7,0)
    E:egpAlpha(8,0)
    E:egpText(5,"",vec2(725,175)) 
    E:egpText(6,"",vec2(725,250))
    E:egpText(7,"",vec2(725,275))
    E:egpText(8,"",vec2(725,300))
G=0}
if(ECHO==0){
JJ=0}
if(clk("JJ")){JJ=0}
if(ECHO==1){
    if(JJ==0){
  owner():soundPlay("buttons/button19.wav",1000,"buttons/button19.wav") 
JJ=1      

}}
#[F = randint(1,100)
if(2<F<10){
E:egpColor(1,vec(82,105,187))}
if(10<F<40){
    E:egpColor(1,vec(78,115,167))
}
if(80>F<40){
    E:egpColor(1,vec(85,100,193))
}


]#
