@name The Bank
@inputs E:wirelink
@outputs 
@persist 
@trigger 

E:egpCircle(1,vec2(256,267),vec2(290,290))
E:egpColor(1,vec(0,0,45))
E:egpText(2,"N",vec2(45,136))
E:egpText(3,"A",vec2(105,205))
E:egpSize(2,255)
E:egpSize(3,255)

E:egpBox(4,vec2(200,256),vec2(5,450))
E:egpMaterial(4,"gui/center_gradient")

E:egpText(5,"New",vec2(250,130))
E:egpSize(5,100)
E:egpText(6,"  Age",vec2(250,230))
E:egpSize(6,100)

E:egpText(7," The future of poseidon",vec2(215,355))
E:egpSize(7,25)

E:egpText(8,"*New Age Gen. (R)",vec2(150,500))    

E:egpText(9,"",vec2(250,50))
