@name New Age Gen SteamID Finder

## COLOURS ##
Red = vec(200,0,0)
Amber = vec(200,100,0)
## END OF COLOURS ##

runOnChat(1)
if(first())
{
    printColor(Red,"[Steam ID Finder | Chat] ",Amber,"Zac Asixx - V5.0 Admin Assist (New Age Gen.)")
    printColor(Amber,"-steamid [Steam ID]  = This finds if a player is online or not by their Steam ID.")
    printColor(Amber,"-rpname [RPname]  = This finds if a player is online or not by their RPname.")
}
Chat = owner():lastSaid():explode(" ")
ChatWhen = owner():lastSaidWhen()

if(chatClk(owner()))
{
    if(Chat[1,string]=="-steamid" & changed(ChatWhen))
    {
        hideChat(1)
        Plys = players()
        for(I=1,Plys:count())
        {
            Ply = Plys[I,entity]:steamID():lower()
            if(Ply==Chat[2,string]:lower())
            {
                Target = Plys[I,entity]
                break
            }
            
        }
        if(Target:isPlayer())
        {
            printColor(Red,"[Steam ID Finder | Chat] ",Amber,"That Steam ID is currently Online.")
            printColor(Red,"Name: ",Amber,Target:name())
            printColor(Red,"Steam ID: ",Amber,Target:steamID())
            printColor(Red,"Job: ",Amber,Target:team():teamName())
        }else
        {
            printColor(Red,"[Steam ID Finder | Chat] ",Amber,"That Steam ID is currently Offline, sorry.")
        }
    }elseif(Chat[1,string]=="-rpname" & changed(ChatWhen))
    {
        hideChat(1)
        Player = findPlayerByName(Chat[2,string]:lower())
        Target = Player

        if(Target:isPlayer())
        {
            printColor(Red,"[Steam ID Finder | Chat] ",Amber,"That Steam ID is currently Online.")
            printColor(Red,"Name: ",Amber,Target:name())
            printColor(Red,"Steam ID: ",Amber,Target:steamID())
            printColor(Red,"Job: ",Amber,Target:team():teamName())
        }else
        {
            printColor(Red,"[Steam ID Finder | Chat] ",Amber,"That Steam ID is currently Offline, sorry.")
        }
    }
        
        
    
}
