@name QuadroCopterBotv3
@model models/hunter/plates/plate05x05.mdl
@persist Timer Hight Targer:entity
runOnTick(1) timer("1",100)

if(Targer:pos()!=vec(0,0,0)){Timer++
entity():applyForce((Targer:pos()-entity():pos()+vec(0,0,Hight))/20+vec(0,0,30+entity():isConstrained()*8)-vec(5,5,0)*(Targer:pos()-entity():pos())/entity():pos():distance(Targer:pos())-entity():vel()/20)
entity():applyAngForce(-ang(entity():angles():pitch(),entity():bearing(Targer:pos())/3,entity():angles():roll())-entity():angVel()/15)
}

if(clk("1")){
    findIncludeClass("player")
    findInSphere(entity():pos(),2000)
    findSortByDistance(entity():pos())
    Targer=find()
    
    Speed=toUnit("km/h",entity():vel():length())
    if(Speed<10){ColorMultiplier=(Speed+5)/15}else{ColorMultiplier=1}
    
    for(I=0,3){
        holoAng(10+I, entity():toWorld(ang(-90,5*Timer,0)))
        holoColor(10+I,vec(sin(Timer+90*I)*128+128,sin(Timer+120+90*I)*128+128,sin(Timer+240+90*I)*128+128)*ColorMultiplier) } }

if(first()|duped()){Hight=80
    entity():setColor(vec4(0,0,0,0)) entity():propFreeze(0)
    findIncludeClass("player")
    findInSphere(entity():pos(),2000)
    findSortByDistance(entity():pos())
    
    function holoAvd(Index,Model:string,Pos:vector,Ang:angle,Scale:vector,Color:vector){
        holoCreate(Index)
        holoPos(Index, entity():toWorld(Pos))
        holoAng(Index, entity():toWorld(Ang))
        holoModel(Index, Model)
        holoColor(Index, Color)
        holoMaterial(Index, "models/debug/debugwhite")
        holoScale(Index,Scale)
        holoParent(Index, entity()) }
        
    function holoAvd2(Index,Material:string,Model:string,Pos:vector,Ang:angle,Scale:vector,Color:vector,Parent){
        holoCreate(Index)
        holoPos(Index, holoEntity(Parent):toWorld(Pos))
        holoAng(Index, holoEntity(Parent):toWorld(Ang))
        holoModel(Index, Model)
        holoColor(Index, Color)
        holoMaterial(Index, Material)
        holoScale(Index,Scale)
        holoParent(Index, entity()) }
            
    holoAvd(1,"models/hunter/plates/plate.mdl",vec(0,0,-1),ang(0,0,0),vec(2,1.5,1),vec(50,50,50  holoAvd(2,"models/hunter/plates/plate05.mdl",vec(5,-3,0),ang(0,60,0),vec(0.4,0.6,0.4),vec(70,70,70))
    holoAvd(3,"models/hunter/plates/plate05.mdl",vec(-5,-3,0),ang(0,120,0),vec(0.4,0.6,0.4),vec(70,70,70))
    holoAvd(4,"models/hunter/plates/plate05.mdl",vec(-5,3,0),ang(0,60,0),vec(0.4,0.6,0.4),vec(70,70,70))
    holoAvd(5,"models/hunter/plates/plate05.mdl",vec(5,3,0),ang(0,120,0),vec(0.4,0.6,0.4),vec(70,70,70))
    holoAvd2(6,"models/debug/debugwhite","models/hunter/plates/plate.mdl",vec(0,-6.5,-1.2),ang(0,0,0),vec(0.4,0.4,0.4),vec(70,70,70),2 holoAvd2(7,"models/debug/debugwhite","models/hunter/plates/plate.mdl",vec(0,6.5,-1.2),ang(0,0,0),vec(0.4,0.4,0.4),vec(70,70,70),3 holoAvd2(8,"models/debug/debugwhite","models/hunter/plates/plate.mdl",vec(0,6.5,-1.2),ang(0,0,0),vec(0.4,0.4,0.4),vec(70,70,70),4 holoAvd2(9,"models/debug/debugwhite","models/hunter/plates/plate.mdl",vec(0,-6.5,-1.2),ang(0,0,0),vec(0.4,0.4,0.4),vec(70,70,70),5 holoAvd2(10,"models/debug/debugwhite","models/xqm/jetenginepropeller.mdl",vec(0,-6.5,1.5),ang(-90,0,0),vec(0.4,0.4,0.4),vec(20,20,20),2 holoAvd2(11,"models/debug/debugwhite","models/xqm/jetenginepropeller.mdl",vec(0,6.5,1.5),ang(-90,0,0),vec(0.4,0.4,0.4),vec(20,20,20),3 holoAvd2(12,"models/debug/debugwhite","models/xqm/jetenginepropeller.mdl",vec(0,6.5,1.5),ang(-90,0,0),vec(0.4,0.4,0.4),vec(20,20,20),4 holoAvd2(13,"models/debug/debugwhite","models/xqm/jetenginepropeller.mdl",vec(0,-6.5,1.5),ang(-90,0,0),vec(0.4,0.4,0.4),vec(20,20,20),5 }
