@name Holopad Export

#####
# Holograms authored by Erza on 25/10/2015
# Exported from Holopad 09/11/2012 (BETA 6.7) by Bubbus
# Thanks to Vercas for the original E2 export template!
#
# FOR AN EXPLANATION OF THE CODE BELOW, VISIT http://code.google.com/p/holopad/wiki/NewE2ExportFormatHOWTO
##### 

#####
# Hologram spawning data
@persist [Holos Clips]:table HolosSpawned HolosStep LastHolo TotalHolos
@persist E:entity
#####


if (first() | duped())
{
    E = entity()

    function number addHolo(Pos:vector, Scale:vector, Colour:vector4, Angles:angle, Model:string, Material:string, Parent:number)
    {
        if (holoRemainingSpawns() < 1) {error("This model has too many holos to spawn! (" + TotalHolos + " holos!)"), return 0}
        
        holoCreate(LastHolo, E:toWorld(Pos), Scale, E:toWorld(Angles))
        holoModel(LastHolo, Model)
        holoMaterial(LastHolo, Material)
        holoColor(LastHolo, vec(Colour), Colour:w())

        if (Parent > 0) {holoParent(LastHolo, Parent)}
        else {holoParent(LastHolo, E)}

        local Key = LastHolo + "_"
        local I=1
        while (Clips:exists(Key + I))
        {
            holoClipEnabled(LastHolo, 1)
            local ClipArr = Clips[Key+I, array]
            holoClip(LastHolo, I, holoEntity(LastHolo):toLocal(E:toWorld(ClipArr[1, vector])), holoEntity(LastHolo):toLocalAxis(E:toWorldAxis(ClipArr[2, vector])), 0)
            I++
        }
        
        return LastHolo
    }

    ##########
    # HOLOGRAMS
    

    #[   ]#    Holos[1, array] = array(vec(0.0000, 0.0000, 21.6408), vec(1.0000, 1.0000, 1.0000), vec4(255, 255, 255, 255), ang(0.0000, 0.0000, 0.0000), "hq_icosphere", "sprops/sprops_grid_orange_12x12", 0)
    #[   ]#    Holos[2, array] = array(vec(0.0000, 0.0000, 44.1778), vec(1.0000, 1.0000, 1.0000), vec4(255, 255, 255, 255), ang(0.0000, -35.0567, 0.0000), "models/props_combine/combine_mine01.mdl", "sprops/trans/misc/ls_m1", 0)
    #[   ]#    Holos[3, array] = array(vec(0.0000, 0.0000, 14.2816), vec(0.9000, 0.9000, 0.9000), vec4(255, 255, 255, 255), ang(0.0000, 0.0000, 0.0000), "hq_torus", "sprops/trans/misc/ls_m1", 0)
    #[   ]#    Holos[4, array] = array(vec(0.0000, 0.0000, 12.1737), vec(0.8000, 0.8000, 0.8000), vec4(255, 255, 255, 255), ang(0.0000, 0.0000, 0.0000), "hq_torus", "sprops/trans/misc/ls_m1", 0)
    #[   ]#    Holos[5, array] = array(vec(0.0000, 0.0000, 25.5014), vec(1.0000, 1.0000, 1.0000), vec4(255, 255, 255, 255), ang(0.0000, 0.0000, 0.0000), "hq_torus_thin", "sprops/trans/misc/ls_m1", 0)
    #[   ]#    Holos[6, array] = array(vec(0.0000, 0.0000, 24.5788), vec(1.0000, 1.0000, 1.0000), vec4(255, 255, 255, 255), ang(0.0000, 0.0000, 0.0000), "hq_torus", "sprops/trans/misc/ls_m1", 0)
    #[   ]#    Holos[7, array] = array(vec(0.0000, 0.0000, 16.7204), vec(1.0000, 1.0000, 1.0000), vec4(255, 255, 255, 255), ang(0.0000, 0.0000, 0.0000), "hq_torus", "sprops/trans/misc/ls_m1", 0)
    #[   ]#    Holos[8, array] = array(vec(0.0000, 0.0000, 10.1396), vec(0.7000, 0.7000, 0.7000), vec4(255, 255, 255, 255), ang(0.0000, 0.0000, 0.0000), "hq_torus", "sprops/trans/misc/ls_m1", 0)
    #[   ]#    Holos[9, array] = array(vec(0.0138, 0.0000, 35.8359), vec(1.0000, 1.0000, 1.0000), vec4(255, 255, 255, 255), ang(-0.1499, 180.0000, 180.0000), "models/sprops/misc/pyramids/rsize_1/rpyr_12x12.mdl", "sprops/sprops_grid_orange_12x12", 0)
    #[   ]#    Holos[10, array] = array(vec(0.0000, 0.0000, 40.6551), vec(1.0661, 1.0468, 1.0000), vec4(255, 255, 255, 255), ang(0.0000, 0.0000, 0.0000), "cube", "sprops/trans/misc/ls_m1", 0)
    #[   ]#    Holos[11, array] = array(vec(0.0000, 0.0000, 27.9848), vec(1.0000, 1.0000, 1.0000), vec4(255, 255, 255, 255), ang(0.0000, 0.0000, 0.0000), "hq_torus_thin", "sprops/trans/misc/ls_m1", 0)
    #[   ]#    Holos[12, array] = array(vec(0.0000, 0.0000, 24.5331), vec(0.8004, 0.8170, 1.0000), vec4(255, 255, 255, 255), ang(0.0000, 0.0000, 0.0000), "hq_rcylinder_thin", "sprops/trans/misc/ls_m1", 0)
    #[   ]#    Holos[13, array] = array(vec(5.0274, 2.3065, 35.3858), vec(0.7352, 0.3909, 0.4659), vec4(255, 255, 255, 255), ang(0.0000, 90.0000, 0.0000), "models/props_lab/tpplugholder_single.mdl", "sprops/trans/misc/ls_m1", 0)
    #[   ]#    Holos[14, array] = array(vec(0.0000, 0.0000, 26.7035), vec(1.0000, 1.0000, 1.0000), vec4(255, 255, 255, 255), ang(0.0000, 0.0000, 0.0000), "hq_torus_thin", "sprops/trans/misc/ls_m1", 0)
    #[   ]#    Holos[15, array] = array(vec(0.0000, 0.0000, 27.2849), vec(1.0000, 1.0000, 1.0000), vec4(255, 255, 255, 255), ang(0.0000, 0.0000, 0.0000), "hq_torus_thin", "sprops/trans/misc/ls_m1", 0)
    #[   ]#    Holos[16, array] = array(vec(0.0000, 0.0000, 26.0591), vec(1.0000, 1.0000, 1.0000), vec4(255, 255, 255, 255), ang(0.0000, 0.0000, 0.0000), "hq_torus_thin", "sprops/trans/misc/ls_m1", 0)
    #[   ]#    Holos[17, array] = array(vec(0.0000, 0.0000, 28.6708), vec(1.0000, 1.0000, 1.0000), vec4(255, 255, 255, 255), ang(0.0000, 0.0000, 0.0000), "hq_torus_thin", "sprops/trans/misc/ls_m1", 0)
    
    ##########
    
    TotalHolos = Holos:count()
    if (0 > holoClipsAvailable()) {error("A holo has too many clips to spawn on this server! (Max is " + holoClipsAvailable() + ")")}
}


#You may place code here if it doesn't require all of the holograms to be spawned.


if (HolosSpawned)
{
    #Your code goes here if it needs all of the holograms to be spawned!
}
else
{
    while (LastHolo <= Holos:count() & holoCanCreate() & perf())
    {
        local Ar = Holos[LastHolo, array]
        addHolo(Ar[1, vector], Ar[2, vector], Ar[3, vector4], Ar[4, angle], Ar[5, string], Ar[6, string], Ar[7, number])
        LastHolo++
    }
    
    if (LastHolo > Holos:count())
    {
        Holos:clear()
        Clips:clear()
        HolosSpawned = 1
        E:setAlpha(0)
    }

    interval(1000)
}
