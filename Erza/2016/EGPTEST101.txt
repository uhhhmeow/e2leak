@inputs EGP:wirelink
@outputs Ready Page
@persist Page Ready Pos:vector2
@trigger
 
interval(200)
 
if(findCanQuery()) {
findIncludeClass("player")
findInSphere(EGP:entity():pos(),250)
Player = findClosest(EGP:entity():pos())
}
if (Player != noentity()) {
Pos = round(EGP:egpCursor(Player))
Use = (changed(Player:keyUse()==1) &Player:keyUse()==1)
}
 
if(first()|duped()) {
    EGP:egpClear()
    Page = 1
 
    EGP:egpBox(1, vec2(256,256), vec2(512,512))
    EGP:egpMaterial(1, "console/background03_widescreen")
   
    EGP:egpRoundedBox(2, vec2(256,256), vec2(390,440))
    EGP:egpColor(2, vec4(50,50,50,150))
 
    EGP:egpRoundedBox(3, vec2(256,90), vec2(375,72))
    EGP:egpColor(3, vec4(120,25,0,200))
   
    EGP:egpText(4, "MrGuBBi AutoGunShop", vec2(80,70))
    EGP:egpSize(4, 38)
   
    #Do not remove
 timer("Timeout",100)
    Ready = 1
    #Do not remove
   
}
 
 
 
################Page system Below
 
 
 
 
if(Page==1&Ready==1) {
   
   
 
   
   
    EGP:egpRoundedBox(10, vec2(258,258), vec2(220,120))
    EGP:egpColor(10, vec4(0,255,0,120))
    EGP:egpText(11, "Start", vec2(200,228))
    EGP:egpSize(11, 55)
   
    #tag den altid under der hvor du laver boxen, du skifter farve paa
    if(inrange(Pos,vec2(150,200),vec2(370,315))) {
    EGP:egpColor(10, vec4(0,200,250,120))
    if(Use) {
    Page = 2
    Ready = 0#husk denne
}
    } else {
    EGP:egpColor(10, vec4(0,255,0,120))
    }
 
}
 
if(Page==2&Ready==1) {
 
 
    EGP:egpRoundedBox(10, vec2(220,165), vec2(215,45))
    EGP:egpColor(10, vec4(120,25,0,200))
    EGP:egpText(11, "   M4A1      380$", vec2(140,150))
    EGP:egpSize(11, 25)
    EGP:egpColor(11, vec(255,0,0))
 
    EGP:egpRoundedBox(12, vec2(220,220), vec2(215,45))
    EGP:egpColor(12, vec4(120,25,0,200))
    EGP:egpText(13, "   P90        450$", vec2(146,205))
    EGP:egpSize(13, 25)
    EGP:egpColor(13, vec(255,0,0))
 
    EGP:egpRoundedBox(14, vec2(220,275), vec2(215,45))
    EGP:egpColor(14, vec4(120,25,0,200))
    EGP:egpText(15, "   AWP       520$", vec2(141,260))
    EGP:egpColor(15, vec(255,0,0))
    EGP:egpSize(15, 25)
 
    EGP:egpRoundedBox(16, vec2(220,330), vec2(215,45))
    EGP:egpColor(16, vec4(120,25,0,200))
    EGP:egpText(17, "   AUG        380$", vec2(138,315))
    EGP:egpColor(17, vec(255,0,0))
    EGP:egpSize(17, 25)
 
    EGP:egpRoundedBox(18, vec2(220,385), vec2(215,45))
    EGP:egpColor(18, vec4(120,25,0,200))
    EGP:egpText(19, "  M249      150$", vec2(143,370))
    EGP:egpColor(19, vec(255,0,0))
    EGP:egpSize(19, 25)
   
    EGP:egpBox(20, vec2(380,165), vec2(83,52))
    EGP:egpMaterial(20, "VGUI/gfx/VGUI/m4a1")
    EGP:egpRoundedBoxOutline(21, vec2(380,165), vec2(90,52))
    EGP:egpColor(21, vec4(150,150,150,100))
   
    EGP:egpBox(22, vec2(380,220), vec2(83,52))
    EGP:egpMaterial(22, "VGUI/gfx/VGUI/ak47")
    EGP:egpRoundedBoxOutline(23, vec2(380,220), vec2(90,52))
    EGP:egpColor(23, vec4(150,150,150,100))
   
    EGP:egpBox(24, vec2(380,275), vec2(90,52))
    EGP:egpMaterial(24, "VGUI/gfx/VGUI/awp")
    EGP:egpRoundedBoxOutline(25, vec2(380,275), vec2(93,54))
    EGP:egpColor(25, vec4(150,150,150,100))
   
    EGP:egpBox(26, vec2(380,330), vec2(83,52))
    EGP:egpMaterial(26, "VGUI/gfx/VGUI/aug")
    EGP:egpRoundedBoxOutline(27, vec2(380,330), vec2(90,52))
    EGP:egpColor(27, vec4(150,150,150,100))
   
    EGP:egpBox(28, vec2(380,385), vec2(83,52))
    EGP:egpMaterial(28, "VGUI/gfx/VGUI/m249")
    EGP:egpRoundedBoxOutline(29, vec2(380,385), vec2(90,52))
    EGP:egpColor(29, vec4(150,150,150,100))
}
 
 
################ Cleaner
 
if(changed(Page)&!first()) {
   
    for( I = 10,100) {
 EGP:egpRemove(I)      
       
    }
   
    timer("Timeout",100)#saatter en timer paa 0.1 sec, og saa skifter den side
}
if(clk("Timeout")) {
 Ready = 1  
}
