@name example_egpSlidingText
@inputs Egp:wirelink EgpUser:entity
@outputs
@persist Direction
@trigger
 
if(first()){
    Egp:egpText(1,"[Thank You!]",vec2(256,0)),Egp:egpSize(1,50)
}
 
interval(50)
if(Direction){
    local Pos=Egp:egpPos(1)
    Egp:egpPos(1,Pos+vec2(5,0))
    if(Pos:x()>330){Direction=0}
}else{
    local Pos=Egp:egpPos(1)
    Egp:egpPos(1,Pos-vec2(5,0))
    if(Pos:x()<30){Direction=1}
}
