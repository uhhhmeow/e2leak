@name EGP Slider
@inputs EGP:wirelink User:entity
@outputs SliderOut
@persist POS
#[--------------------===============[|Created by IronManhood|]===============--------------------]#
if(first())
    {
        EGP:egpClear()
        EGP:egpDrawTopLeft(1)
        SliderOut=0
#------------------------------------------Background
        EGP:egpBox(1,vec2(0,0),vec2(512,512))
        EGP:egpColor(1,vec4(100,150,255,255))
        EGP:egpMaterial(1,"gui/center_gradient")
#------------------------------------------Slider Background
        EGP:egpBox(2,vec2(30,236),vec2(10,40))
        EGP:egpColor(2,vec4(255,191,0,200))
        EGP:egpBox(3,vec2(472,236),vec2(10,40))
        EGP:egpColor(3,vec4(255,191,0,200))
        EGP:egpBox(4,vec2(40,251),vec2(432,10))
        EGP:egpColor(4,vec4(255,191,0,255))
        EGP:egpMaterial(4,"gui/center_gradient")
        
        EGP:egpBox(5,vec2(144,241),vec2(4,30))
        EGP:egpColor(5,vec4(255,191,0,200))
        EGP:egpBox(6,vec2(254,231),vec2(4,50))
        EGP:egpColor(6,vec4(255,191,0,200))
        EGP:egpBox(7,vec2(361,241),vec2(4,30))
        EGP:egpColor(7,vec4(255,191,0,200))
#------------------------------------------Slider Box
        EGP:egpBox(8,vec2(35,231),vec2(4,50))
        EGP:egpColor(8,vec4(255,30,30,255))
#------------------------------------------Header
        EGP:egpText(9,"EGP Slider",vec2(254,80))
        EGP:egpFont(9,"Courier New",60)
        EGP:egpAlign(9,1)
        EGP:egpColor(9,vec4(0,0,0,155))
#------------------------------------------SliderOut
        EGP:egpText(10,"" + SliderOut + "%",vec2(226,316))
        EGP:egpFont(10,"Courier New",60)
        EGP:egpColor(10,vec4(255,30,30,255))
#------------------------------------------Cursor
        EGP:egpCircle(400,vec2(),vec2(6,6))
        EGP:egpColor(400,vec4(255,255,255,100))
        EGP:egpParentToCursor(400)
    }
Cursor=EGP:egpCursor(User)
if(User)
    {
        LeftTop=EGP:egpPos(2)
        RightBottom=EGP:egpPos(3) + EGP:egpSize(3)
        if(inrange(Cursor, LeftTop, RightBottom))
            {
                EGP:egpPos(8,vec2(Cursor:x(),231))
            }
        POS=round(Cursor:x())
        SliderOut=round(clamp(((POS-32)/440)*100,0,100))
        EGP:egpSetText(10,"" + SliderOut + "%")
    }
