@name Cheese
@inputs [E]:wirelink
@outputs [Pos]:array
@persist [Targets,PlyN,Ply]:array
@persist [Owner]:entity Count Visable
@trigger E

if(first()|dupefinished())
{
    #if(owner():steamID()!="STEAM_0:0:40286110")
    #{selfDestructAll(),hint(";-;",7),print(";-;")}
    if(E:entity():isValid()){entity():propNotSolid(1)}
    E:entity():propNotSolid(1)
    #entity():parentTo(entity():isWeldedTo())
    entity():setAlpha(0),E:entity():setAlpha(0)
    Ply = players()
    for(I=1,Ply:count()){PlyN[I,string] = Ply[I,entity]:name()}
    Owner = owner()
    Count = Targets:count()
    #entity():propNotSolid(1)
    E:egpClear()
    E:egpDrawTopLeft(1)
    
    #[dPanel("HUD Settings",vec2(12,200),vec2(300,310))
    dShow("HUD Settings",0)
    dTabHolder("Tabs","HUD Settings",vec2(5,30),vec2(290,275))
    dTab("Targets","Tabs")
    dTab("Settings","Tabs")
    dTab("Other","Tabs")]#
    
    #################
    ##  TAB THINGS ##
    #################
    ## Targets tab ##
    #################
    
    #[dLabel("Select what you want to be displayed on your HUD","Targets",vec2(5,5))
    dCheckBox("Players","Targets",vec2(5,30))
    dSetNval("Players",1)
    dCheckBox("Props","Targets",vec2(5,50))
    dSetNval("Props",0)
    dDropBox("Prop","Targets",vec2(5,70),265)
    dArray("Prop",PlyN)
    dCheckBox("Kino","Targets",vec2(5,90))
    dCheckBox("Expression 2","Targets",vec2(5,110))
    dCheckBox("EGPS","Targets",vec2(5,130))
    dButton("Update","Targets",vec2(5,210),vec2(265,25))]#
    
    #runOnChat(1)
    #dRunOnChange("Update",1)
    
    gSetGroup("Targeting")
    timer("force start",500)
}

if(chatClk(Owner))
{
    local LSE = lastSaid():lower():explode(" ")
    if(LSE:string(1) == "/ed")
    {
        #dShow("HUD Settings",1)
        F = 1
    }
    elseif(LSE:string(1) == "/cl")
    {
        #dShow("HUD Settings",0)
        F = 1
    }
    elseif(LSE:string(1) == "/up")
    {
        timer("force start",500)
        F = 1
    }
    hideChat(F ? 1 : 0)
}

#if(dClk("Update")|clk("force start"))
if(clk("force start")|(inputClk() & E:entity():type() == "gmod_wire_egp_hud"))
{
    #dShow("HUD Settings",0)
    E:egpClear()
    Targets = array()
    #[if(dNval("Players"))
    {]#
        Players = players()
        #for(I=1,Players:count()){if(Players[I,entity]==Owner){Players:removeEntity(I),break}}
        for(I=1,Players:count()){Targets:pushEntity(Players[I,entity])}
    #}
    #[if(dNval("Props")&dSval("Prop") != "")
    {
        for(I=1,PlyN:count())
        {
            if(dSval("Prop") == PlyN[I,string])
            {
                local Props_Owner = Ply[I,entity]
                findClearWhiteList()
                findClearBlackList()
                #findExcludeClass("player")
                #findIncludeClass("playx*")
                #findIncludePlayerProps(PlyN[I,string])
                findIncludeClass("prop_physics")
                findInSphere(vec(),100000)
                local Props = findToArray()
                for(I=1,Props:count()){Targets:pushEntity(Props[I,entity])}
                break
            }
        }
    }]#
    #[if(dNval("Kino"))
    {
        findClearWhiteList()
        findClearBlackList()
        findByClass("kino_ball")
        local Kinos = findToArray()
        for(I=1,Kinos:count()){Targets:pushEntity(Kinos[I,entity])}
    }]#
    #if(dNval("Expression 2"))
    #{
        findClearWhiteList()
        findClearBlackList()
        findByClass("gmod_wire_expression2")
        local E2 = findToArray()
        for(I=1,E2:count()){Targets:pushEntity(E2[I,entity])}
    #}
    
    #findClearWhiteList()
    #findClearBlackList()
    #findByClass("prop_vehicle_jeep")
    #local Cars = findToArray()
    #qqfor(I=1,Cars:count()){Targets:pushEntity(Cars[I,entity])}
    #if(dNval("EGPS"))
    #{
        findClearWhiteList()
        findClearBlackList()
        findByClass("gmod_wire_egp_hud")
        local EGP = findToArray()
        for(I=1,EGP:count()){Targets:pushEntity(EGP[I,entity])}
        findByClass("gmod_wire_egp")
        local EGP = findToArray()
        for(I=1,EGP:count()){Targets:pushEntity(EGP[I,entity])}
        findByClass("gmod_wire_egp_emitter")
        local EGP = findToArray()
        for(I=1,EGP:count()){Targets:pushEntity(EGP[I,entity])}
   # }
    
    findClearWhiteList()
    findClearBlackList()
    #findIncludeClass("k_printer_t")
    #findIncludeClass("printer")
    #findIncludeClass("keypad")
    #findIncludeClass("func_door")
    #findIncludeClass("func_button")
    #findIncludeClass("gmod_anchor")
    #findInSphere(vec(),100000)
    #local Printers = findToArray()
    #for(I=1,Printers:count())
    #{
    #    Targets:pushEntity(Printers[I,entity])
    #}
    
    Count = Targets:count()
    for(I=1,Count)
    {
        local Ent = Targets[I,entity]
        local Dist = 0
        local BoxCenter = Ent:boxCenter()
        E:egp3DTracker(I,BoxCenter)
        E:egpCircleOutline(I+Count,vec2(),vec2(7.5,7.5))
        #local Text = "["+Dist+"]"
        Text = "Unknown"
        local Col = vec(200,200,200)
        #[if(Ent:isPlayer()){Text = Ent:name()+" ["+Dist+"]", Col = teamColor(Ent:team())}
        elseif(Ent:type() == "kino_ball"){Text = "Kino ["+Dist+"]", Col = vec(200,40,40)}
        elseif(Ent:type() == "gmod_wire_expression2"){Text = "E2 ["+Dist+"]",Col = vec(200,40,40)}
        elseif(Ent:type() == "gmod_wire_egp_hud"){Text = "EGP Hud ["+Dist+"]",Col = vec(10,125,200)}
        elseif(Ent:type() == "gmod_wire_egp"){Text = "EGP Screen ["+Dist+"]",Col = vec(10,125,200)}
        elseif(Ent:type() == "gmod_wire_egp_emitter"){Text = "EGP Emitter ["+Dist+"]",Col = vec(10,125,200)}]#
        
        if(Ent:isPlayer()){Text = Ent:name(), Col = teamColor(Ent:team())}
        elseif(Ent:type() == "kino_ball"){Text = "Kino", Col = vec(255,132,0)}
        elseif(Ent:type() == "gmod_wire_expression2"){Text = "E2",Col = vec(200,40,40)}
        elseif(Ent:type() == "gmod_wire_egp_hud"){Text = "EGP Hud",Col = vec(10,125,200)}
        elseif(Ent:type() == "gmod_wire_egp"){Text = "EGP Screen",Col = vec(10,125,200)}
        elseif(Ent:type() == "gmod_wire_egp_emitter"){Text = "EGP Emitter",Col = vec(10,125,200)}
        else{Text = Ent:type() + " : " + Ent:owner():name()}
        #elseif(Ent:type() == "k_printer_t2"){Text = "EGP Emitter",Col = vec(10,125,200)}
        
        E:egpText(I+Count*2,Text,vec2(0,10)),E:egpColor(I+Count,Col),E:egpColor(I+Count*2,Col),E:egpSize(I+Count*2,15)
        
        E:egpAlign(I+Count*2,1,0)
        
        E:egpParent(I,Ent)
        E:egpParent(I+Count,I)
        E:egpParent(I+Count*2,I)
    }
    timer("force start",5000)
}
#[
interval(200)

OwnerPos = Owner:boxCenterW()
if(Count > 0)
{
    for(I=1,Count)
    {
        local Ent = Targets[I,entity]
        local Dist = round(OwnerPos:distance(Ent:boxCenterW()))
        local Text = "["+Dist+"]"
        if(Ent:isPlayer()){Text = Ent:name()+" ["+Dist+"]"}
        elseif(Ent:type() == "kino_ball"){Text = "Kino ["+Dist+"]"}
        elseif(Ent:type() == "gmod_wire_expression2"){Text = "E2 ["+Dist+"]"}
        elseif(Ent:type() == "gmod_wire_egp_hud"){Text = "EGP Hud ["+Dist+"]"}
        elseif(Ent:type() == "gmod_wire_egp"){Text = "EGP Screen ["+Dist+"]"}
        elseif(Ent:type() == "gmod_wire_egp_emitter"){Text = "EGP Emitter ["+Dist+"]"}

        E:egpSetText(I+Count*2,Text)
    }
}
]#
