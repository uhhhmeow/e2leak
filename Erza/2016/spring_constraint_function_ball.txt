@name spring_constraint_function_ball
@persist ConstrainHolo:array Data:table [Apos,Bpos]:vector

runOnTick(1)

if (first()) {
    for(I=1,6) {
        holoCreate(I)
        holoModel(I,"hqicosphere2")
        holoScale(I,vec(0.5,0.5,0.5))
        holoPos(I,entity():pos()+vec(0,(-7*20/2)+I*20,60))
    }
    ConstrainHolo:pushVector2(vec2(1,2))
    ConstrainHolo:pushVector4(vec4(25,1,1,1))
    ConstrainHolo:pushVector2(vec2(2,4))
    ConstrainHolo:pushVector4(vec4(25,1,1,1))
    ConstrainHolo:pushVector2(vec2(1,3))
    ConstrainHolo:pushVector4(vec4(25,1,1,1))
    ConstrainHolo:pushVector2(vec2(3,4))
    ConstrainHolo:pushVector4(vec4(25,1,1,1))
    ConstrainHolo:pushVector2(vec2(1,5))
    ConstrainHolo:pushVector4(vec4(25,1,1,1))
    ConstrainHolo:pushVector2(vec2(5,4))
    ConstrainHolo:pushVector4(vec4(25,1,1,1))
    ConstrainHolo:pushVector2(vec2(1,6))
    ConstrainHolo:pushVector4(vec4(25,1,1,1))
    ConstrainHolo:pushVector2(vec2(6,4))
    ConstrainHolo:pushVector4(vec4(25,1,1,1))
    ConstrainHolo:pushVector2(vec2(6,2))
    ConstrainHolo:pushVector4(vec4(25,1,1,1))
    ConstrainHolo:pushVector2(vec2(5,3))
    ConstrainHolo:pushVector4(vec4(25,1,1,1))
    ConstrainHolo:pushVector2(vec2(6,5))
    ConstrainHolo:pushVector4(vec4(25,1,1,1))
    ConstrainHolo:pushVector2(vec2(3,2))
    ConstrainHolo:pushVector4(vec4(25,1,1,1))
    ConstrainHolo:pushVector2(vec2(6,3))
    ConstrainHolo:pushVector4(vec4(25,1,1,1))
    ConstrainHolo:pushVector2(vec2(5,2))
    ConstrainHolo:pushVector4(vec4(25,1,1,1))
    ConstrainHolo:pushVector2(vec2(1,4))
    ConstrainHolo:pushVector4(vec4(50,1,1,1))
}

if (owner():keyAttack2()) {
    ConstrainHolo[31,vector4] = vec4(owner():aimPos()+owner():aimNormal()*20,1)
    ConstrainHolo[32,vector4] = vec4(10,4,4,1)
    holoColor(61,vec4(255,255,255,255))
} else {
    ConstrainHolo:remove(31)
    ConstrainHolo:remove(32)
    holoColor(61,vec4(255,255,255,0))
}

if (ops() < 4950) {
    for (I=1,ConstrainHolo:count(),2) {
         Data["Move"+I,vector] = vec()
    }
    for (I=1,ConstrainHolo:count(),2) {
        if (!ConstrainHolo[I,vector4]:w()) {
            Vel = Data["Move"+I,vector]
            Apos = holoEntity(ConstrainHolo[I,vector2]:x()):pos()
            Bpos = holoEntity(ConstrainHolo[I,vector2]:y()):pos()
            Dis = Apos - Bpos
            K = ConstrainHolo[I+1,vector4]:y()
            D = ConstrainHolo[I+1,vector4]:x()
            B = ConstrainHolo[I+1,vector4]:z()
            F = K * (D - Dis:length()) - B * Vel:length() / Dis:length()
            FV = (Dis * (F / Dis:length()))
            
            Data["Move"+I,vector] = Data["Move"+I,vector]+(FV/10)
            
        } else {
            Vel = Data["Move"+I,vector]
            Apos = holoEntity(ConstrainHolo[I,vector4]:w()):pos()
            Bpos = vec(ConstrainHolo[I,vector4]:x(),ConstrainHolo[I,vector4]:y(),ConstrainHolo[I,vector4]:z())
            Dis = Apos - Bpos
            K = ConstrainHolo[I+1,vector4]:y()
            D = ConstrainHolo[I+1,vector4]:x()
            B = ConstrainHolo[I+1,vector4]:z()
            F = K * (D - Dis:length()) - B * Vel:length() / Dis:length()
            FV = (Dis * (F / Dis:length()))
            
            Data["Move"+I,vector] = Data["Move"+I,vector]+(FV/10)
            
        }
    }
    for (I=1,ConstrainHolo:count(),2) {
        if (!ConstrainHolo[I,vector4]:w()) {
            PosX = holoEntity(ConstrainHolo[I,vector2]:x()):pos()
            PosY = holoEntity(ConstrainHolo[I,vector2]:y()):pos()
            if (!Data["Dholo_"+I,normal] & ConstrainHolo[I+1,vector4]:w()) {
                holoCreate(I+30)
                holoModel(I+30,"hqcylinder")
                holoColor(I+30,holoEntity(ConstrainHolo[I,vector2]:x()):getColor())
                holoEntity(I+30):setMaterial(holoEntity(ConstrainHolo[I,vector2]:x()):getMaterial())
                Data["Dholo_"+I,normal] = 1
            } elseif (ConstrainHolo[I+1,vector4]:w()) {
                holoAng(ConstrainHolo[I,vector2]:x(),ang(0,0,0))
                Bearing = holoEntity(ConstrainHolo[I,vector2]:x()):bearing(PosY)
                Elevation = holoEntity(ConstrainHolo[I,vector2]:x()):elevation(PosY)
                Distance = (PosX):distance(PosY)-Data["Move"+I,vector]:length()
                holoAng(I+30,ang(0,-Bearing+90,-Elevation+90))
                holoScaleUnits(I+30,vec(5,5,Distance))
                holoPos(I+30,PosX+holoEntity(I+30):angles():up()*(Distance/2))
            }
            PosX = holoEntity(ConstrainHolo[I,vector2]:x()):pos()+Data["Move"+I,vector]
            PosY = holoEntity(ConstrainHolo[I,vector2]:y()):pos()-Data["Move"+I,vector]
            holoPos(ConstrainHolo[I,vector2]:x(),PosX+Data["move_"+ConstrainHolo[I,vector2]:x(),vector])
            holoPos(ConstrainHolo[I,vector2]:y(),PosY+Data["move_"+ConstrainHolo[I,vector2]:y(),vector])
        } else {
            PosX = holoEntity(ConstrainHolo[I,vector4]:w()):pos()+Data["Move"+I,vector]
            PosY = vec(ConstrainHolo[I,vector4]:x(),ConstrainHolo[I,vector4]:y(),ConstrainHolo[I,vector4]:z())
            if (!Data["Dholo_"+I,normal] & ConstrainHolo[I+1,vector4]:w()) {
                holoCreate(I+30)
                holoModel(I+30,"hqcylinder")
                holoColor(I+30,holoEntity(ConstrainHolo[I,vector4]:w()):getColor())
                holoEntity(I+30):setMaterial(holoEntity(ConstrainHolo[I,vector4]:w()):getMaterial())
                Data["Dholo_"+I,normal] = 1
            } elseif (ConstrainHolo[I+1,vector4]:w()) {
                holoAng(ConstrainHolo[I,vector4]:w(),ang(0,0,0))
                Bearing = holoEntity(ConstrainHolo[I,vector4]:w()):bearing(PosY)
                Elevation = holoEntity(ConstrainHolo[I,vector4]:w()):elevation(PosY)
                Distance = (PosX):distance(PosY)
                holoAng(I+30,ang(0,-Bearing+90,-Elevation+90))
                holoScaleUnits(I+30,vec(5,5,Distance))
                holoPos(I+30,PosX+holoEntity(I+30):angles():up()*(Distance/2))
            }
            holoPos(ConstrainHolo[I,vector4]:w(),PosX+Data["move_"+ConstrainHolo[I,vector2]:x(),vector])
        }
    }
}
