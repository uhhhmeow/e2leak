@name Steering E2
@inputs [Base,Chair]:entity 
@outputs SteeringAngles TiltAngles Tilt
@persist SteeringAngles TiltAngles
@model models/hunter/plates/plate05x05.mdl
interval(20)
E=entity()
TurnSpeed=3
TiltSpeed=0.4
ReturnTurnSpeed=5
ReturnTiltSpeed=5
TurningLimit=45
TiltLimit=12


if(Chair:driver():keyPressed("A"))
{
SteeringAngles=clamp(SteeringAngles+TurnSpeed,-TurningLimit,TurningLimit)
}

if(Chair:driver():keyPressed("D"))
{
SteeringAngles=clamp(SteeringAngles-TurnSpeed,-TurningLimit,TurningLimit)
}


if(Chair:driver():keyPressed("A")==0&&Chair:driver():keyPressed("D")==0)
{
if(SteeringAngles>=0.0001)
{
SteeringAngles=clamp(SteeringAngles-ReturnTurnSpeed,0,TurningLimit)
}

if(SteeringAngles<=-0.0001)
{
SteeringAngles=clamp(SteeringAngles+ReturnTurnSpeed,-TurningLimit,0)
}
}



Tilt=-(SteeringAngles/TurningLimit)*TiltLimit


if(Base:isValid())
{
E:setPos(Base:toWorld(vec(0,0,100)))
E:setAng(Base:toWorld(ang(Tilt,SteeringAngles+90,0)))
E:propFreeze(1)
}
