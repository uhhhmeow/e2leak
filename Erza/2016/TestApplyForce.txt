@persist Plate:entity Constant
@model models/props_phx/construct/metal_plate1.mdl

if (first())    {
    Constant = 1500000
    Plate = entity():isWeldedTo()

    runOnTick(1)
} elseif (tickClk())    {

    Plate:applyForce( -Plate:up() * Constant )

}
