
@name Erza Auto Store v12
@inputs E:wirelink User:entity [R1 R2 R3 R4]:entity
@persist Index [Price Config Users]:table
@outputs [U1 U2 U3 U4]

interval(200)

if(first()){
    timer("advert",100)
    
    ################################
    #[
     Configurations
    
     Change these to how YOU please!
    
    Do Not Keep GunDealer and BlackMarket the same. You can keep them both 0. Just not 1. Or it will fuck up...
    
     ]#
    
    
    Config["Locked",number]=0 #<--- If changed to one, the screen will no longer get past Index 1. (Login Screen)
    
    Config["GunDealer",number]=1    
    
    Config["BlackMarket",number]=0
    
    Config["AutoAdvert",number]=0
    
    Config["AdvertText",string]="Come on down to Erza's GunShop! The Cheapest Weapons On The Server!" # The text it will say in the advert!
    
    
    ################################
    
    ###################
    ## Miscellaneous ##
    ###################
    
    
    
    ###################
    
    
    
    function number egpClick(Player:entity,ID){
        return inrange(E:egpCursor(Player),E:egpPos(ID)-(E:egpSize(ID)/2),E:egpPos(ID)+(E:egpSize(ID)/2))
    }
    function background(E:wirelink) {
        E:egpClear()
        E:egpBox(300,vec2(256),vec2(512)),E:egpColor(300,vec(255,0,0))
        local Circles=5
        local Distence=512/Circles
        for (I=1,Circles+2) {
            E:egpCircle(I+200,vec2(Distence*(I-2),-5),vec2(Distence*I,517))
            E:egpSize(I+200,30)
            E:egpMaterial(I+200,"gui/gradient_down")
            E:egpColor(I+200,vec(32))
        }
    }
    function void wirelink:cursorSpawn(Index){
        This:egpPoly(Index,vec2(),vec2(12,12),vec2(7,12),vec2(10,17),vec2(9,18),vec2(8,18),vec2(7,17),vec2(5,13),vec2(0,16))
        This:egpColor(Index,vec())
        This:egpParentToCursor(Index)
    }
    Index = 1,timer("Draw",100)
}

if(Index == 1){
    if(clk("Draw")){
        background(E)
        U1=0,U2=0,U3=0,U4=0
        E:egpBox(1,vec2(256),vec2(512,70)),E:egpColor(1,vec(128))
        E:egpText(2,"Erza's GunShop",vec2(256)),E:egpSize(2,40),E:egpAlign(2,1,2)
        E:egpText(3,Config["Locked",number] ? "System is unavailable" : "Press Anywhere to Begin",vec2(256)),E:egpAlign(3,1,0),E:egpSize(3,30)
    }
    if(~User&User){
        if(Config["Locked",number]==0){
            if(egpClick(User,1)){
                soundPlay("welcome",0.6,"vo/Breencast/br_welcome01.wav"),soundVolume("welcome",100)
                Index = 2,timer("Draw",100)
            }
        }
    }   
}


if(Index == 2){
    if(clk("Draw")){
        background(E)
        E:egpBox(1,vec2(256,100),vec2(512,70)),E:egpColor(1,vec(128))
        E:egpBox(2,vec2(256),vec2(512,70)),E:egpColor(2,vec(128))
        E:egpBox(3,vec2(256,420),vec2(512,70)),E:egpColor(3,vec(128))
        E:egpText(4,"Choose A Catergory",vec2(256,100)),E:egpSize(4,40),E:egpAlign(4,1,2)
        E:egpText(5,"Admins Get 50% Off Gun Dealer",vec2(256,100)),E:egpAlign(5,1,0),E:egpSize(5,30)
        E:egpText(6,"Gun Dealer",vec2(256)),E:egpSize(6,40),E:egpAlign(6,1,2)
        E:egpText(7,"Weapons That Can Be Legal",vec2(256)),E:egpAlign(7,1,0),E:egpSize(7,30)
        E:egpText(8,"Black Market Dealer",vec2(256,420)),E:egpSize(8,40),E:egpAlign(8,1,2)
        E:egpText(9,"Weapons Are Illegal!",vec2(256,420)),E:egpAlign(9,1,0),E:egpSize(9,30)
        
        E:egpBox(100,vec2(20,490),vec2(50)),E:egpMaterial(100,"gui/HTML/back")
    }
    if(~User&User){
        if(Config["GunDealer",number]==1){
            if(egpClick(User,2)){
                soundPlay("click",1,"buttons/button9.wav"),soundPitch("click",255)
                Index = 3,timer("Draw",100)
            }
        }
        if(Config["BlackMarket",number]==1){
            if(egpClick(User,3)){
                soundPlay("click",1,"buttons/button9.wav"),soundPitch("click",255)
                Index = 3.1,timer("Draw",100)
            }
        }
        if(egpClick(User,100)){
            Index = 1,timer("Draw",100)
            soundPlay("click",1,"buttons/button9.wav"),soundPitch("click",255)
        }
    }
}
            

if(Index == 3 & Config["GunDealer",number]==1){
    if(clk("Draw")){
        background(E)
        E:egpBox(4,vec2(256,70),vec2(512,60)),E:egpColor(4,vec(128)),E:egpText(5,"Buy "+R1:shipmentName()+" for $"+R1:shipmentPrice()/5,vec2(50,50)),E:egpSize(5,30)
        E:egpBox(25,vec2(256,160),vec2(512,60)),E:egpColor(25,vec(128)),E:egpText(26,"Buy "+R2:shipmentName()+" for $"+R2:shipmentPrice()/5,vec2(50,140)),E:egpSize(26,30)
        E:egpBox(27,vec2(256,260),vec2(512,60)),E:egpColor(27,vec(128)),E:egpText(28,"Buy "+R3:shipmentName()+" for $"+R3:shipmentPrice()/5,vec2(50,240)),E:egpSize(28,30)
        E:egpBox(29,vec2(256,360),vec2(512,60)),E:egpColor(29,vec(128)),E:egpText(30,"Buy "+R4:shipmentName()+" for $"+R4:shipmentPrice()/5,vec2(50,340)),E:egpSize(30,30)
        
        E:egpBox(100,vec2(20,490),vec2(50)),E:egpMaterial(100,"gui/HTML/back")
    }
    if(~User&User){
        if(egpClick(User,4)){
            moneyRequest(User,R1:shipmentPrice()/5,"opt1")
            soundPlay("click",1,"buttons/button9.wav"),soundPitch("click",255)
        }
        if(egpClick(User,25)){
            moneyRequest(User,R2:shipmentPrice()/5,"opt2")
            soundPlay("click",1,"buttons/button9.wav"),soundPitch("click",255)
        }
        if(egpClick(User,27)){
            moneyRequest(User,R3:shipmentPrice()/5,"opt3")
            soundPlay("click",1,"buttons/button9.wav"),soundPitch("click",255)
        }
        if(egpClick(User,29)){
            moneyRequest(User,R4:shipmentPrice()/5,"opt4")
            soundPlay("click",1,"buttons/button9.wav"),soundPitch("click",255)
        }
        if(egpClick(User,100)){
            Index = 1,timer("Draw",100)
            soundPlay("click",1,"buttons/button9.wav"),soundPitch("click",255)
        }
    }
}

if(Index == 3.1 & Config["BlackMarket",number]==1){
    if(clk("Draw")){
        background(E)
        E:egpBox(4,vec2(256,70),vec2(512,60)),E:egpColor(4,vec(128)),E:egpText(5,"Buy "+R1:shipmentName()+" for $"+R1:shipmentPrice()/5,vec2(50,50)),E:egpSize(5,30)
        E:egpBox(25,vec2(256,160),vec2(512,60)),E:egpColor(25,vec(128)),E:egpText(26,"Buy "+R2:shipmentName()+" for $"+R2:shipmentPrice()/5,vec2(50,140)),E:egpSize(26,30)
        E:egpBox(27,vec2(256,260),vec2(512,60)),E:egpColor(27,vec(128)),E:egpText(28,"Buy "+R3:shipmentName()+" for $"+R3:shipmentPrice()/5,vec2(50,240)),E:egpSize(28,30)
        E:egpBox(29,vec2(256,360),vec2(512,60)),E:egpColor(29,vec(128)),E:egpText(30,"Buy "+R4:shipmentName()+" for $"+R4:shipmentPrice()/5,vec2(50,340)),E:egpSize(30,30)
        #bottom right text
        
        E:egpBox(100,vec2(20,490),vec2(50)),E:egpMaterial(100,"gui/HTML/back")
    }
    if(~User&User){
        if(egpClick(User,4)){
            moneyRequest(User,R1:shipmentPrice()/5,"opt1")
            soundPlay("click",1,"buttons/button9.wav"),soundPitch("click",255)
        }
        if(egpClick(User,25)){
            moneyRequest(User,R2:shipmentPrice()/5,"opt2")
            soundPlay("click",1,"buttons/button9.wav"),soundPitch("click",255)
        }
        if(egpClick(User,27)){
            moneyRequest(User,R3:shipmentPrice()/5,"opt3")
            soundPlay("click",1,"buttons/button9.wav"),soundPitch("click",255)
        }
        if(egpClick(User,29)){
            moneyRequest(User,R4:shipmentPrice()/5,"opt4")
            soundPlay("click",1,"buttons/button9.wav"),soundPitch("click",255)
        }
        if(egpClick(User,100)){
            Index = 1,timer("Draw",100)
            soundPlay("click",1,"buttons/button9.wav"),soundPitch("click",255)
        }
    }
}

if(moneyClk("opt1")){
    U1 = 1
    timer("reset",100)
}elseif(moneyClk("opt2")){
    U2 = 1
    timer("reset",100)
}elseif(moneyClk("opt3")){
    U3 = 1
    timer("reset",100)
}elseif(moneyClk("opt4")){
    U4 = 1
    timer("reset",100)
}elseif(moneyNoClk()){
    Index = 1,timer("Draw",100)
}


if(clk("reset")){
    Index = 1,timer("Draw",100)
}



if(Config["AutoAdvert",number]==1 & clk("advert")){
    concmd("say /advert " +Config["AdvertText",string])
}


E:cursorSpawn(200)
