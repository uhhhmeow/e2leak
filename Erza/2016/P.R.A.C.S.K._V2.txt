@name P.R.A.C.S.K. V2
@persist [On Key Toggle WModeType Mode Armor Clip1 Clip2 Static Shooting HasArmor UsesArmor Fear]:normal [O OW]:entity [ModeType]:string [R]:array

interval(1)
runOnLast(1)

O = owner()
OW = O:weapon()
OWT = OW:type()
Swapped = changed(OWT)
S=O:vel():length()
Key = O:keyPressed("RCONTROL")

if(UsesArmor)
{
Armor = O:armor()
}
else
{
Armor = O:health()
}

ArmorShot = $Armor < 0 & Toggle
Clip1 = OW:clip1()
Clip2 = O:ammoCount(OW:secondaryAmmoType())
Shot1 = $Clip1 < 0 & !Swapped
Shot2 = $Clip2 < 0 & !Swapped
Crouching = O:isCrouch()


#Change this value from 1 or 0 to enable / disable armor requisite
UsesArmor = 1


if(UsesArmor == 1)
{
    HasArmor = Armor > 20
}
else
{
    HasArmor = 1
}

if(O:getMaterial() != ModeType | OW:getAlpha() != WModeType)
{
    O:setMaterial(ModeType)
    OW:setAlpha(WModeType)
}

    if(Mode == 0)
    {
        ModeType = ""
        WModeType = 255
    }
    elseif(Mode == 1)
    {
        ModeType = "models/shadertest/predator"
        WModeType = 0
    }
    elseif(Mode == 2)
    {
        ModeType = "models/blackout/blackout"
        WModeType = 0
    }
    
    if(Static & Toggle)
    {
        ModeType = "models/alyx/emptool_glow"
        WModeType = 255
        timer("static",500)
    }


if(changed(Key) & Key)
{
    Toggle = !Toggle
}

if(HasArmor)
{
    if(Toggle == 0)
    {
        Mode = 0
    }
    elseif(Toggle == 1 & !Crouching)
    {
        Mode = 1
    }
    elseif(Toggle == 1 & Crouching)
    {
        Mode = 2
    }
}
else
{
Mode = 0
}


if(OWT == "weapon_slam" | OWT == "m9k_m61_frag" | OWT == "m9k_harpoon" | OWT == "m9k_ied_detonator" | OWT == "m9k_nerve_gas" | OWT == "m9k_orbital_strike" | OWT == "m9k_sticky_grenade" | OWT == "m9k_suicide_bomb" | OWT == "bb_css_c4" | OWT == "bb_cssfrag" | OWT == "bb_cssfrag" | OWT == "bb_css_smoke" | OWT == "bb_css_c4_alt" | OWT == "bb_cssfrag_alt" | OWT == "bb_css_smoke_alt" | OWT == "bb_alliedfrag" | OWT == "bb_axisfrag" | OWT == "bb_alliedsmoke" | OWT == "bb_axissmoke" | OWT == "bb_dod_tnt")
{
Shooting = 0
}
else
{
    if(Shot1 | Shot2)
    {
        stoptimer("shoot")
        Shooting = 1
    }

    if(Shooting == 1)
    {
        timer("shoot",100)
        Mode = Mode - 1
    }
}

if(clk("shoot"))
{
    Shooting = 0
    Mode = Mode
}


if(ArmorShot)
{
    stoptimer("static")
    Static = 1
    O:soundPlay(1,0,"ambient/energy/zap" + floor(randint(1,9)):toString() + ".wav")
}

if(clk("static"))
{
    Static = 0
}

if(findCanQuery())
{
    findByClass("npc_*")
    
    findExcludeClass("npc_alyx")
    findExcludeClass("npc_barney")
    findExcludeClass("npc_citizen")
    findExcludeClass("npc_dog")
    findExcludeClass("npc_magnusson")
    findExcludeClass("npc_kleiner")
    findExcludeClass("npc_mossman")
    findExcludeClass("npc_eli")
    findExcludeClass("Medic")
    findExcludeClass("npc_odessa")
    findExcludeClass("Rebel")
    findExcludeClass("Refugee")
    findExcludeClass("VortigauntUriah")
    findExcludeClass("npc_vortigaunt")
    findExcludeClass("VortigauntSlave")
    findExcludeClass("npc_monk")
    findExcludeClass("npc_crow")
    findExcludeClass("npc_pigeon")
    findExcludeClass("npc_seagull")
    findExcludeClass("Spawn ID of a friendly NPC here.")
    
    R = findToArray()
}

for(I=1,R:count())
{
    NPC=R[I,entity]
    if(Mode > 0 & !Fear)
    {
        NPC:npcRelationship(O,"neutral",999)
    }
    if(removing() | Mode < 1 | O:pos():distance(NPC:pos()) > 1750 & !Fear)
    {
        NPC:npcRelationship(O,"hate",999)
    }
        
    if((O:pos():distance(NPC:pos()) < 100 & Toggle & NPC:npcGetTarget()) | (O:pos():distance(NPC:pos()) < 175 & (changed(Toggle) & Toggle == 0)))
    {
        Fear = 1
    }
    if(Fear == 1)
    {
        NPC:npcRelationship(O,"fear",999)
        timer("fear",3000)
    }
    if(clk("fear"))
    {
        Fear = 0
    }
}

if(changed(Toggle) & Toggle)
{
    print(_HUD_PRINTCENTER,"Activated")
    owner():soundPlay(0,0,"hl1/fvox/blip.wav")
    soundPitch(0,110)
}

elseif(changed(Toggle) & !Toggle)
{
    print(_HUD_PRINTCENTER,"Deactivated")
    owner():soundPlay(1,0,"hl1/fvox/blip.wav")
    soundPitch(1,80)
}

if(changed(Toggle) & Armor <= 20 & UsesArmor == 1)
{
    print(_HUD_PRINTCENTER,"Insufficient armor points. P.R.A.C.S.K. Disabled. Aquire more than 20 points of armor to enable.")    
}

if(changed(Toggle) & Toggle & Armor <= 20)
{
    O:soundPlay(1,2,"buttons/combine_button2.wav")
}

if(changed(Toggle) & !Toggle & Armor <= 20)
{
    print(_HUD_PRINTCENTER,"Deactivated")
}

if(UsesArmor == 1)
{
    if(changed(Armor > 20))
    {
        if(first() & changed(Armor) & Armor > 20 | changed(Armor) & Armor > 20)
        {
            hint("Sufficient armor points.",5)
            hint("P.R.A.C.S.K. enabled.",5)
            O:soundPlay(2,0.6,"buttons/button19.wav")
            soundPitch(2,125)
        }
     

        if(first() & changed(Armor) & Armor <= 21 | changed(Armor) & Armor <= 21)
        {
            hint("Insufficient armor points. P.R.A.C.S.K. Disabled.",5)
            hint("Aquire more than 20 points of armor to enable.",5)
            O:soundPlay(2,0.7,"buttons/button19.wav")
            soundPitch(2,80)
        }
    }
}

if(removing())
{
O:setMaterial("")
OW:setAlpha(255)
}
