@name music_player
@inputs Egp:wirelink EgpUser:entity
@persist [Songs Times Playing PlayT]:array Index IndexS TE Spiel Pitch Timer LaufZeit Volume                                                                                                                                Cr:string
if(first()|duped()|dupefinished()){                                                                                                                                                                                                                Cr="LuckyGuy"
    Volume=1,Pitch=100
    Songs=array(
        "music/hl1_song10.mp3",
        "music/hl1_song11.mp3",
        "music/hl1_song14.mp3",
        "music/hl1_song15.mp3",
        "music/hl1_song17.mp3",
        "music/hl1_song19.mp3",
        "music/hl1_song20.mp3",
        "music/hl1_song21.mp3",
        "music/hl1_song24.mp3",
        "music/hl1_song25_remix3.mp3",
        "music/hl1_song26.mp3",
        "music/hl1_song3.mp3",
        "music/hl1_song5.mp3",
        "music/hl1_song6.mp3",
        "music/hl1_song9.mp3",
        "music/hl2_intro.mp3",
        "music/hl2_song0.mp3",
        "music/hl2_song1.mp3",
        "music/hl2_song10.mp3",
        "music/hl2_song11.mp3",
        "music/hl2_song12_long.mp3",
        "music/hl2_song13.mp3",
        "music/hl2_song14.mp3",
        "music/hl2_song15.mp3",
        "music/hl2_song16.mp3",
        "music/hl2_song17.mp3",
        "music/hl2_song19.mp3",
        "music/hl2_song2.mp3",
        "music/hl2_song20_submix0.mp3",
        "music/hl2_song20_submix4.mp3",
        "music/portal_4000_degrees_kelvin.mp3",
        "music/portal_android_hell.mp3",
        "music/portal_no_cake_for_you.mp3",
        "music/portal_party_escort.mp3",
        "music/portal_procedural_jiggle_bone.mp3",
        "music/portal_self_esteem_fund.mp3",
        "music/portal_still_alive.mp3",
        "music/portal_stop_what_you_are_doing.mp3",
        "music/portal_subject_name_here.mp3",
        "music/portal_taste_of_blood.mp3",
        "music/portal_you_cant_escape_you_know.mp3",
        "music/portal_youre_not_a_good_person.mp3",
        "music/ravenholm_1.mp3",
        "ambient/music/bongo.wav",
        "ambient/music/country_rock_am_radio_loop.wav",
        "ambient/music/cubanmusic1.wav",
        "ambient/music/dustmusic1.wav",
        "ambient/music/dustmusic2.wav",
        "ambient/music/dustmusic3.wav",
        "ambient/music/flamenco.wav",
        "ambient/music/latin.wav",
        "ambient/music/mirame_radio_thru_wall.wav",
        "ambient/music/piano1.wav",
        "ambient/music/piano2.wav",
        "music/HL2_song23_SuitSong3.mp3",
        "music/HL2_song25_Teleporter.mp3",
        "music/HL2_song26.mp3",
        "music/HL2_song26_trainstation1.mp3",
        "music/HL2_song26_trainstation2.mp3",
        "music/HL2_song28.mp3",
        "music/HL2_song29.mp3",
        "music/HL2_song3.mp3",
        "music/HL2_song30.mp3",
        "music/HL2_song31.mp3",
        "music/HL2_song32.mp3",
        "music/HL2_song33.mp3",
        "music/HL2_song4.mp3",
        "music/HL2_song6.mp3",
        "music/HL2_song7.mp3",
        "music/HL2_song8.mp3",
        "music/Ravenholm_1.mp3",
        "music/radio1.mp3",
        "ambient/opera.wav",
        "ambient/guit1.wav"
    )
    Times=array(
        56,
        25,
        50,
        63,
        65,
        43,
        34,
        38,
        35,
        37,
        26,
        68,
        52,
        54,
        37,
        38,
        22,
        53,
        25,
        33,
        37,
        90,
        41,
        86,
        38,
        62,
        73,
        38,
        111,
        99,
        98,
        148,
        116,
        119,
        84,
        57,
        73,
        180,
        48,
        25,
        25,
        120,
        120,
        120,
        120,
        120,
        120,
        120,
        120,
        120,
        120,
        120,
        120,
        120,
        30,
        28,
        40,
        35,
        30,
        15,
        65,
        65,
        55,
        50,
        50,
        30,
        45,
        35,
        28,
        30,
        35,
        30,
        75,
        45
    )
    function number egpClick(Ply:entity,ID){
        return inrange(Egp:egpCursor(Ply),Egp:egpPos(ID)-(Egp:egpSize(ID)/2),Egp:egpPos(ID)+(Egp:egpSize(ID)/2))
    }
    function void wirelink:drawListe(){
        for(A=1,10){
            local Ex=Playing[A,string]:explode("/")
            This:egpText(17+A,Ex[Ex:count(),string],vec2(11,110+A*30)),Egp:egpSize(A+17,30)
            if(A>1){
                local Col=160-(A*13)
                Egp:egpColor(A+17,vec(Col,Col,Col))
            }
        }
    }
    function void wirelink:hauptDraw(){
        This:egpClear()
        This:egpBox(1,vec2(256,256),vec2(512,512)),This:egpColor(1,vec(0,0,180))
        This:egpBox(2,vec2(56,50),vec2(100,90)),This:egpColor(2,vec())
        This:egpBox(3,vec2(156,50),vec2(100,90)),This:egpColor(3,vec())
        This:egpBox(4,vec2(256,50),vec2(100,90)),This:egpColor(4,vec())
        This:egpBox(5,vec2(356,50),vec2(100,90)),This:egpColor(5,vec())
        This:egpBox(6,vec2(456,50),vec2(100,90)),This:egpColor(6,vec())
        
        This:egpBox(7,vec2(56,50),vec2(80,90)),This:egpMaterial(7, "gui/spawnmenu_toggle_back")
        This:egpBox(8,vec2(156,50),vec2(100,120)),This:egpMaterial(8, "gui/HTML/back")
        This:egpBox(9,vec2(256,33),vec2(100,60)),This:egpMaterial(9, "gui/HTML/refresh")
        This:egpBox(10,vec2(356,50),vec2(100,120)),This:egpMaterial(10, "gui/HTML/forward")
        This:egpBox(11,vec2(456,50),vec2(80,90)),This:egpMaterial(11, "gui/spawnmenu_toggle")
        This:egpRoundedBox(12,vec2(256,290),vec2(500,300)),This:egpColor(12,vec())
        
        This:egpBox(13,vec2(100,478),vec2(150,70)),This:egpColor(13,vec())
        This:egpBox(14,vec2(356,478),vec2(280,70)),This:egpColor(14,vec())
        This:egpBox(15,vec2(100,478),vec2(150,70)),This:egpMaterial(15,"VGUI/notices/hint")
        This:egpBox(16,vec2(231+Volume*125,478),vec2(Volume*250,60)),This:egpColor(16,vec(Volume*255,255-Volume*255,0))
        This:egpText(17,"Volume "+(Volume*100)+"%",vec2(220,453)),This:egpSize(17,47)
        This:egpText(30,Timer+"/ "+LaufZeit,vec2(210,55)),This:egpSize(30,25)
        This:egpText(31,"X * "+Pitch/100,vec2(210,75)),This:egpSize(31,22)
        This:drawListe()
    }
    function void wirelink:listSongs(Seite){
        This:egpClear()
        local Counter=0
        local Max=15
        for(A=1,Max){
            Counter=A
            This:egpBox(A,vec2(256,15+(Counter-1)*30),vec2(500,26)),This:egpColor(A,vec(0,0,155))
            local String=Songs[A+(Max*(Seite-1)),string]:explode("/")
            local Text=String[String:count(),string]
            if(Text=="HL2_song3.mp3"|Text=="HL2_song29.mp3"|Text=="hl1_song10.mp3"|Text=="hl2_song1.mp3"|Text=="piano2.wav"){
                This:egpText(A+Max,Text+"   *"+Cr+"s Favourit!",vec2(10,(Counter-1)*30)),This:egpSize(A+Max,24)
            }else{
                This:egpText(A+Max,Text,vec2(10,(Counter-1)*30)),This:egpSize(A+Max,24)
            }
            This:egpText(A+Max*2,Times[A+(Max*(Seite-1)),number]+"s",vec2(460,(Counter-1)*30)),This:egpSize(A+Max*2,24)
        }
        Counter++
        if(Seite>1){
            This:egpBox(295,vec2(70,20+(Counter-1)*30),vec2(150,30)),This:egpColor(295,vec(150,150,20))
            This:egpText(296,"Prev. Page",vec2(10,5+(Counter-1)*30)),This:egpSize(296,30),This:egpColor(296,vec())
        }else{
            This:egpText(296,"by "+Cr,vec2(5,5+(Counter-1)*30)),This:egpSize(296,30),This:egpColor(296,vec(50,200,50))
        }
        This:egpBox(297,vec2(250,20+(Counter-1)*30),vec2(150,30)),This:egpColor(297,vec(150,20,20))
        This:egpText(298,"Return",vec2(200,5+(Counter-1)*30)),This:egpSize(298,30),This:egpColor(298,vec())
        if(Seite<Songs:count()/Max){
            This:egpBox(299,vec2(425,20+(Counter-1)*30),vec2(150,30)),This:egpColor(299,vec(20,150,20))
            This:egpText(300,"Next Page",vec2(360,5+(Counter-1)*30)),This:egpSize(300,30),This:egpColor(300,vec())
        }else{
            This:egpText(300,"by "+Cr,vec2(340,5+(Counter-1)*30)),This:egpSize(300,30),This:egpColor(300,vec(50,200,50))
        }
    }
        function void printBunt(S:string){
            local C=0
            local R=array()
            for(A=1,S:length()*2,2){
                C++
                R[A,vector]=vec(255,0,0):rotate(ang(1,1,1)*random(360))
                R[A+1,string]=S:index(C)
            }
            printColor(R)
        }                                                                                                                                                                                                           printBunt(Cr+"s Music player V2")
}
if(changed(Egp:entity())&Egp:entity()){
    if(hash(Cr)==3719429271){Egp:hauptDraw()}else{selfDestructAll()}
}

if(Index==0){
    if(~EgpUser&EgpUser){
        if(egpClick(EgpUser,2)){
            soundPurge()
            for(A=1,40){
                Egp:egpRemove(A+100)
            }
            stoptimer("BAR"),timer("NochMal",1000)
        }elseif(egpClick(EgpUser,3)){
            if(Pitch>0&Spiel){
                Pitch-=5,soundPitch(1,Pitch),Egp:egpSetText(31,"X * "+Pitch/100)
            }
        }elseif(egpClick(EgpUser,4)){
            Spiel = !Spiel
            if(Spiel){
                if(Playing:exists(1)){
                    if(Timer<=0){
                        Egp:entity():soundPlay(1,0,Playing[1,string])
                        Pitch=100,soundVolume(1,Volume),Egp:egpSetText(31,"X * "+Pitch/100)
                        Timer=PlayT[1,number],LaufZeit=Timer
                    }
                    Egp:egpMaterial(9, "VGUI/notices/undo")
                    soundPitch(1,Pitch)
                    timer("BAR",500)
                }
            }else{
                soundPitch(1,0),stoptimer("BAR"),Egp:egpMaterial(9, "gui/HTML/refresh")
            }
        }elseif(egpClick(EgpUser,5)){
            if(Pitch<1000&Spiel){
                Pitch+=5,soundPitch(1,Pitch),Egp:egpSetText(31,"X * "+Pitch/100)
            }
        }elseif(egpClick(EgpUser,6)){
            if(Playing:count()>1){
                timer("NochMal",1000)
            }else{Egp:egpMaterial(9, "gui/HTML/refresh"),Egp:egpSetText(30,"0/ 0")}
            for(A=1,40){
                Egp:egpRemove(A+100)
            }
            Playing:remove(1),PlayT:remove(1)
            soundPurge(),stoptimer("BAR")
            Egp:drawListe()
        }elseif(egpClick(EgpUser,13)){
            Index=1,IndexS=1,Egp:listSongs(IndexS)
        }elseif(egpClick(EgpUser,14)){
            Volume=clamp(round((Egp:egpCursor(EgpUser):x()-178)/318,2),0,1)
            soundVolume(1,Volume)
            Egp:egpBox(16,vec2(231+Volume*125,478),vec2(Volume*250,60)),Egp:egpColor(16,vec(Volume*255,255-Volume*255,0))
            Egp:egpSetText(17,"Volume "+(Volume*100)+"%")
        }
    }
    if(clk("NochMal")){
        Egp:entity():soundPlay(1,0,Playing[1,string])
        Pitch=100,timer("BAR",500),soundVolume(1,Volume),Egp:egpSetText(31,"X * "+Pitch/100)
        Egp:egpMaterial(9, "VGUI/notices/undo"),Spiel =1
        Timer=PlayT[1,number],LaufZeit=Timer
    }
    if(clk("BAR")){
        if(Timer>0){
            Timer-=(0.005*Pitch)
            local Div=Timer/LaufZeit
            local X= 500-Div*500
            local Sin=sin(Div*1400)
            local Y=118+(Sin*10)
            local Ind=clamp(round(Div*40),0,300)
            if(Ind>0&changed(Ind)){
                Egp:egpCircle(100+Ind,vec2(X,Y),vec2(10,10)),Egp:egpColor(100+Ind,vec(0,230,0))
            }
            Egp:egpSetText(30,round(Timer)+"/ "+LaufZeit)
            timer("BAR",500)
        }else{
            Playing:remove(1),PlayT:remove(1)
            for(A=1,40){
                Egp:egpRemove(A+100)
            }
            if(Spiel&Playing:exists(1)){
                Egp:entity():soundPlay(1,0,Playing[1,string])
                Pitch=100,soundVolume(1,Volume),Egp:egpSetText(31,"X * "+Pitch/100)
                Timer=PlayT[1,number],LaufZeit=Timer
                Egp:egpMaterial(9, "VGUI/notices/undo")
                soundPitch(1,Pitch)
                timer("BAR",500)
            }else{
                soundPurge()
                Spiel=0,stoptimer("BAR"),Egp:egpMaterial(9, "gui/HTML/refresh"),Egp:egpSetText(30,"0/ 0")
            }
            Egp:drawListe()
        }
    }
}elseif(Index==1){
    if(~EgpUser&EgpUser){
        if(egpClick(EgpUser,295)){
            IndexS--,Egp:listSongs(IndexS)
        }elseif(egpClick(EgpUser,299)){
            IndexS++,Egp:listSongs(IndexS)
        }elseif(egpClick(EgpUser,297)){
            Index=0,Egp:hauptDraw()
        }else{
            for(A=1,15){
                if(egpClick(EgpUser,A)){
                    local C=Playing:count()+1
                    Egp:egpColor(TE,vec(0,0,155))
                    Playing[C,string]=Songs[A+(15*(IndexS-1)),string]
                    PlayT[C,number]=Times[A+(15*(IndexS-1)),number]
                    Egp:egpColor(A,vec(0,255,0)),TE=A,timer("Reicht",750)
                    break
                }
            }
        }
    }
    if(clk("Reicht")){Egp:egpColor(TE,vec(0,0,155))}
}
