@name Hovertank
@inputs 
@outputs 
@persist Inertia:angle Pod:wirelink
@model models/hunter/blocks/cube2x2x2.mdl
E=entity()
interval(50)
if(first()|dupefinished()){
    E:noCollideAll(1)
    
    Mats=array("models/gibs/metalgibs/metal_gibs")
    Inertia = shiftL(ang(E:inertia())):setYaw(0)
    
    holoCreate(1)
    holoModel(1,"models/hunter/plates/plate4x8.mdl")
    holoPos(1,E:toWorld(vec(0,0,-46)))
    holoAng(1,E:angles())
    holoMaterial(1,Mats[1,string])
    holoParent(1,E)
    holoEntity(1):holoPhysScale(vec(1,1,1))
    
    holoCreate(2)
    holoModel(2,"models/hunter/triangles/2x2x1.mdl")
    holoPos(2,E:toWorld(vec(142.35,0,0)))
    holoAng(2,holoEntity(1):toWorld(ang(0,-90,0)))
    holoScale(2,vec(8,1,1))
    holoMaterial(2,Mats[1,string])
    holoParent(2,E)
    holoEntity(2):holoPhysScale(vec(8,1,1))
    
    holoCreate(3)
    holoModel(3,"models/hunter/triangles/2x2x1.mdl")
    holoPos(3,E:toWorld(vec(-142.35,0,0)))
    holoAng(3,holoEntity(1):toWorld(ang(0,90,0)))
    holoScale(3,vec(8,1,1))
    holoMaterial(3,Mats[1,string])
    holoParent(3,E)
    holoEntity(3):holoPhysScale(vec(8,1,1))
    
    holoCreate(4)
    holoModel(4,"models/hunter/plates/plate4x8.mdl")
    holoPos(4,E:toWorld(vec(0,0,46)))
    holoAng(4,E:angles())
    holoMaterial(4,Mats[1,string])
    holoParent(4,E)
    holoEntity(4):holoPhysScale(vec(1,1,1))
    
    holoCreate(5)
    holoModel(5,"models/hunter/triangles/3x2x2.mdl")
    holoPos(5,E:toWorld(vec(0,246.8,0)))
    holoAng(5,E:toWorld(ang(0,0,0)))
    holoScale(5,vec(2,0.8,1))
    holoMaterial(5,Mats[1,string])
    holoParent(5,E)
    holoEntity(5):holoPhysScale(vec(2,0.8,1))
    
    holoCreate(6)
    holoModel(6,"models/hunter/blocks/cube4x4x025.mdl")    
    holoPos(6,holoEntity(4):toWorld(vec(0,95,0)))
    holoAng(6,E:toWorld(ang(0,0,-5.35)))
    holoScale(6,vec(1,1.02,1.5))
    holoMaterial(6,Mats[1,string])
    holoParent(6,E)
    holoEntity(6):holoPhysScale(vec(1,1,1.535))
    
    holoCreate(7)
    holoModel(7,"models/hunter/blocks/cube4x4x025.mdl")
    holoPos(7,holoEntity(4):toWorld(vec(0,-95,9)))
    holoAng(7,E:angles())
    holoScale(7,vec(1,1,1.5))
    holoMaterial(7,Mats[1,string])
    holoParent(7,E)
    holoEntity(7):holoPhysScale(vec(1,1,1.5))
    
    holoCreate(8)
    holoModel(8,"models/hunter/blocks/cube2x4x025.mdl")
    holoPos(8,E:toWorld(vec(0,-184,0)))
    holoAng(8,E:toWorld(ang(90,90,0)))
    holoMaterial(8,Mats[1,string])
    holoParent(8,E)
    holoEntity(8):holoPhysScale(vec(1,1,1))
    
    timer("fix",100)
    E:setMaterial(Mats[1,string])
}elseif(clk("fix")){E:noCollideAll(0),E:propFreeze(0)}

if(Pod["Active",number]==1) {
    rangerHitEntities(1)
    rangerHitWater(1) 
    rangerFilter(E)
    rangerFilter(holoEntity(1))
    Down=rangerOffset(500,E:pos(),vec(0,0,-1))
    Left=250/rangerOffset(250,E:toWorld(vec(50,50,0)),E:forward()-E:right()):distance() 
    Right=250/rangerOffset(250,E:toWorld(vec(50,-50,0)),E:forward()+E:right()):distance()

    Ta=vec()

    Height=vec(0,0,100-Down:distance()) 
    Force=-E:angles()*500-E:angVel()*15
    Counter = -E:vel()*0.25
    Forward=0
    Yaw=clamp((Left-Right)*110,-90,90)
    TA=(Ta-E:pos()):toAngle():yaw() 
    Dis=vec2(Ta):distance(vec2(E:pos()))
    Ang=ang(0,(-E:angVel():yaw()/15-Yaw+TA),0)
    V=E:toLocal(rotationVector(quat(Ang)/quat(E))+E:pos())

    E:applyForce((Height+Counter+Forward)*E:mass())
    E:applyAngForce(Force*Inertia)
    E:applyTorque((50*V-15*E:angVelVector())*E:inertia())
}
