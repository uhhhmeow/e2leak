@name Heat vision 3
@inputs Capture Solve Screen:wirelink Convection
@outputs 
@outputs Scan A Loop InitialC:array Display Ready Compute PreWrite Cell
@outputs I SubI Array:array ACount AMin I2 SArray:array Data Temp Colour:vector AIndex Background Data2
@trigger 
runOnTick(1)
if (Capture & ~Capture) {
    Screen[1048569] = 1
    Screen[1048574] = 1
    Screen[1048572] = 64
    Screen[1048573] = 64
    Scan = 1
    A = Ready = Compute = 0#32768
    Array = array()
    InitialC = array()
    SArray = array()
    
}
while (perf() & Scan) {
        A++
        Ranger = ranger(1000000, A % 64 / 32 - 1, floor(A / 64) / 32 - 1)
        if (Ranger:hitWorld() | Ranger:hitSky()) {Data = 0}
        else {
            E = Ranger:entity()
            if (E:getColor() == vec(255, 0, 0)) {Data = -E:mass()}
            else {Data = 1}
        }
        InitialC[A, number] = Data
        if (A >= 4096) {Scan = 0, Ready = 1}
}
if (Solve & ~Solve & Ready) {
    PreWrite = 1
    ACount = InitialC:count()
    AMin = InitialC:min()
}

while (PreWrite & perf()) {
    Cell++
    if (InitialC[Cell, number] >= 0) {Array[Cell, number] = 0}
    else {Array[Cell, number] = -InitialC[Cell, number]} #/ AMin
    if (Cell >= ACount) {
        PreWrite = 0
        Compute = 1
        #Display = 1
    }
}

while (Compute & perf()) {
    while (perf()) {
        SubI++
        if (Background) {Colour = vec(0, 35, 0)}
        else {        
        if (Data2 < 1/5) {
            Colour = vec(0, 0, round(Data2 * 5 * 255))
        }
        elseif (Data2 < 2/5) {
            Colour = vec(round((Data2 - 1/5) * 5 * 255), 0, 255)
        }
        elseif (Data2 < 3/5) {
            Colour = vec(255, 0, round(255 - (Data2 - 2/5) * 5 * 255))
        }
        elseif (Data2 < 4/5) {
            Colour = vec(255, round((Data2 - 3/5) * 5 * 255), 0)
        }
        else {
            Colour = vec(255, 255, round((Data2 - 4/5) * 5 * 255))
        }
        }
        SArray:pushNumber(Colour:x())
        SArray:pushNumber(Colour:y())
        SArray:pushNumber(Colour:z())
        if (SubI > ACount) {SubI = 0, I++, SArray = array(), break}
        if (InitialC[SubI, number] <= 0) {
            if (InitialC[SubI, number] == 0) {Background = 1} else {
                Background = 0
                Data2 = -InitialC[SubI, number] / 255
                }
            continue # dont change temp values of air or heaters
        }
        Background = 0
        T = Array[SubI - 64, number]
        L = Array[SubI - 1, number]
        R = Array[SubI + 1, number]
        B = Array[SubI + 64 , number]
        DivTot = 0
        if (!InitialC[SubI - 64, number]) {DivTot += Convection, T *= Convection} else {DivTot++}
        if (!InitialC[SubI - 1, number]) {DivTot += Convection, L *= Convection} else {DivTot++}
        if (!InitialC[SubI + 1, number]) {DivTot += Convection, R *= Convection} else {DivTot++}
        if (!InitialC[SubI + 64, number]) {DivTot += Convection, B *= Convection} else {DivTot++}
        Temp = (T + L + R + B) / DivTot
        Array[SubI, number] = Temp
        Data2 = Temp / 255
    }
    if (I == 1000) {Compute = 0}
}
if (I / 3 == round(I / 3) & I > 1) {Display = 1}
if (Display) {
    Screen:writeArray(0, SArray)
    Display = 0
}

