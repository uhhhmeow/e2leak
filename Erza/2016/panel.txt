@name panel
@inputs [Fuel RPM]:normal SpeedTarg:entity
@outputs
@persist [Arrows Others]:vector [RPM_PEAK Fuel_PEAK Speed_PEAK] [C_rpm C_fuel C_speed]
 
if(first()){
 
    Others      = vec(0,88,88)
    Arrows      = vec(0,255,0)
 
    RPM_PEAK   = 5500
    Fuel_PEAK  = 83.7
    Speed_PEAK = 180
 
    holoCreate(0, entity():toWorld(vec(-7,3,0)), vec(1.17,0.01,0.7), entity():toWorld(ang(0,-3,5))), holoParent(0, entity()), holoMaterial(0, "models/shiny"), holoColor(0, vec(0,50,50))
   
    holoCreate(1, entity():toWorld(vec(7,3,0)), vec(1.17,0.01,0.7), entity():toWorld(ang(0,-177,-5))), holoParent(1, entity()), holoMaterial(1, "models/shiny"), holoColor(1, vec(0,50,50))
   
    holoCreate(2, entity():toWorld(vec(0,3.25,0)), vec(0.7,0.7,0.15), entity():toWorld(ang(90,90,0))), holoModel(2, "models/holograms/hq_tube.mdl"), holoParent(2, entity()), holoMaterial(2, "models/shiny"), holoColor(2, vec(0,50,50))
 
    holoCreate(2e3, entity():toWorld(vec(0,2.5,0)), vec(1), entity():toWorld(ang(0,90,0))), holoModel(2e3, "models/sprops/trans/misc/gauge_2.mdl"), holoParent(2e3, entity())
       
        holoClip(2e3, 1, vec(0.72,0,0),vec(-1,0,0), 0), holoClipEnabled(2e3, 1, 1)
 
    holoCreate(3, entity():toWorld(vec(6.2,3.25,-1)), vec(0.35,0.35,0.15), entity():toWorld(ang(90,100,0))), holoModel(3, "models/holograms/hq_tube_thin.mdl"), holoParent(3, entity()), holoMaterial(3, "models/shiny"), holoColor(3, vec(0,50,50))
 
        holoCreate(3e3, entity():toWorld(vec(6.13,3.6,-1)), vec(0.46), entity():toWorld(ang(0,100,0))), holoModel(3e3, "models/sprops/trans/misc/gauge_2.mdl"), holoSkin(3e3, 5), holoParent(3e3, entity())
 
        holoCreate(5020, entity():toWorld(vec(6.0802,3.9,-1)), vec(0.24), entity():toWorld(ang(90,100,0))), holoModel(5020, "models/holograms/hq_torus_thin.mdl"), holoMaterial(5020, "lights/white"), holoParent(5020, entity())
 
            holoClip(5020, 1, vec(),vec(0,-1,0), 0), holoClipEnabled(5020, 1, 1)
 
        holoCreate(5021, entity():toWorld(vec(6.0802,3.9,-1)), vec(0.24), entity():toWorld(ang(90,100,7))), holoModel(5021, "models/holograms/hq_torus_thin.mdl"), holoMaterial(5021, "lights/white"), holoParent(5021, entity())
 
            holoClip(5021, 1, vec(),vec(-1,1,0), 0), holoClipEnabled(5021, 1, 1)
 
 
    holoCreate(4, entity():toWorld(vec(-6.2,3.25,-1)), vec(0.35,0.35,0.15), entity():toWorld(ang(90,80,0))), holoModel(4, "models/holograms/hq_tube_thin.mdl"), holoParent(4, entity()), holoMaterial(4, "models/shiny"), holoColor(4, vec(0,50,50))
   
        holoCreate(4e3, entity():toWorld(vec(-6.13,3.6,-1)), vec(0.46), entity():toWorld(ang(0,80,0))), holoModel(4e3, "models/sprops/trans/misc/gauge_2.mdl"), holoSkin(4e3, 6)   , holoParent(4e3, entity())
 
        holoCreate(5022, entity():toWorld(vec(-6.076,3.9,-1)), vec(0.24), entity():toWorld(ang(90,80,0))), holoModel(5022, "models/holograms/hq_torus_thin.mdl"), holoMaterial(5022, "lights/white"), holoParent(5022, entity())
 
            holoClip(5022, 1, vec(),vec(-0.77,-1,0), 0), holoClipEnabled(5022, 1, 1)
       
        holoCreate(5023, entity():toWorld(vec(-6.076,3.9,-1)), vec(0.24), entity():toWorld(ang(90,80,0))), holoModel(5023, "models/holograms/hq_torus_thin.mdl"), holoMaterial(5023, "lights/white"), holoParent(5023, entity())
 
            holoClip(5023, 1, vec(),vec(-0.82,1,0), 0), holoClipEnabled(5023, 1, 1)
 
    for(I = 0, 19){
     
        holoCreate(5e3 + I, entity():toWorld(vec(0.13+sin(I*18 + 3)*1.75,3.2,cos(I*18 + 3)*1.75)), vec(0.015,0.015,0.01), entity():toWorld(ang(90,90,0))), holoModel(5e3 + I, "models/holograms/hq_cylinder.mdl"), holoMaterial(5e3 + I, "lights/white"), holoParent(5e3 + I, entity())
       
    }
 
    # axles
   
    holoCreate(100, entity():toWorld(vec(0,3.2, 0)), vec(0.01), entity():toWorld(ang())), holoParent(100, entity())
       
        #ifdef entity:setdLight(vector, number, number)
           
            holoEntity(100):setdLight(Arrows, 3,10)
       
        #else
 
        #endif
 
    holoCreate(200, entity():toWorld(vec(6.13,3.9,-1)), vec(0.01), entity():toWorld(ang(0,100,0))), holoParent(200, entity())
 
    holoCreate(300, entity():toWorld(vec(-6.13,3.9,-1)), vec(0.01), entity():toWorld(ang(0,80,0))), holoParent(300, entity())
 
 
    holoCreate(5024, entity():toWorld(vec(0,3.2, -2.2)), vec(0.02), entity():toWorld(ang(0,90,0))), holoParent(5024, 100), holoModel(5024, "models/holograms/prism.mdl"), holoMaterial(5024, "lights/white")
 
    holoCreate(5025, entity():toWorld(vec(6.13,3.9,-2.58)), vec(0.015), entity():toWorld(ang(0,10,0))), holoParent(5025, 200), holoModel(5025, "models/holograms/prism.mdl"), holoMaterial(5025, "lights/white")
 
    holoCreate(5026, entity():toWorld(vec(-6.13,3.9,-2.58)), vec(0.015), entity():toWorld(ang(0,-10,0))), holoParent(5026, 300), holoModel(5026, "models/holograms/prism.mdl"), holoMaterial(5026, "lights/white")
 
    for(I = 0, 26){
 
        if(I<24){
 
            holoColor(5e3+I, Others)
 
        }
        if(I == 24 | I == 25 | I == 26){holoColor(5e3+I, Arrows)}
   
    }
   
    holoColor(5e3 + 10, vec(255))
    holoColor(5e3 + 12, vec(255))
    holoDelete(5e3 + 11)
 
}
 
interval(30)
 
E = entity()
 
C_rpm = -clamp(RPM,0,RPM_PEAK )/37
C_fuel = -40 - clamp(Fuel,0,Fuel_PEAK )
C_speed = clamp( (toUnit("km/h", SpeedTarg:vel():length())) ,0,Speed_PEAK )
 
holoAng(100, entity():toWorld(ang(0,90,-C_speed*1.75 )))
holoAng(200, entity():toWorld(ang(0,100,C_rpm * 2 )))
holoAng(300, entity():toWorld(ang(0,80,C_fuel * 2.6 )))
