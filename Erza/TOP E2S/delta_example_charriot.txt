@name FUN/charriot
@inputs [Pod Cam]:wirelink Charriot:entity
@outputs
@persist E:entity [WalkV Walk1 Epos V]:vector
@trigger all

W = Pod["W",number] 
SH= Pod["Shift",number]
En= Pod["Entity",entity]
A = Pod["A",number]
D = Pod["D",number]
S = Pod["S",number]
M1= Pod["Mouse1",number]

if(first()|dupefinished()|clk("ping")){ 
    findByClass("npc*")
    if(findClosest(entity():pos()):owner()==owner()){
        E = findResult(1)
    }
    timer("ping",5000)
}

if(changed(E)&E){ 
    stoptimer("ping")  
    timer("check",400) 
    holoCreate(2)
    holoAlpha(2,0)
    holoParent(2,E)
    
    Cam["Angle",angle] = E:angles()
    Cam["Position",vector] = E:toWorld(vec(-500,0,150))
    Cam["Parent",entity] = E
}

if(E){
    interval(10) 
    holoPos(2,E:toWorld(vec(200,50*(A-D),50)))
    Cam["Activated",number] = Pod["Active",number]
    
    Epos = E:pos()
    Walk1 = holoEntity(2):pos() - Epos
    WalkV = holoEntity(2):pos() - Walk1:normalized()*100
    
    E:npcRelationship("player","like",1)
    
    if(clk("check")){
        timer("check",500)
        if(W){
            if(SH){E:npcGoRun(WalkV)}
            else{E:npcGoWalk(WalkV)}
        }
    }
    
    if(changed(M1)&M1){
        E:npcAttack()
    }
    
    V = ((Epos+vec(0,0,50))-Charriot:pos())*Charriot:mass()
    Charriot:applyForce(V + $V*15)
}

if(changed(!E)&!E){
    reset() 
}