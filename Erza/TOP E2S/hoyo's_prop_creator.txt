@name hoyo's prop Creator
@persist [ENT,O,TENT]:entity AR:array ID 
@persist Dist KAT2 I SAVE POS:array HE:entity LOAD
@persist ON COL:array ALP:array MOD:array
@persist ANG:array AX M:string DBG2
@persist SL:array A R:array R2:array E:entity
interval(50)
if(first())
{
    ON = 1
    SNA = 1
    ID = 0
    runOnChat(1)
    runOnFile(1)
    O = owner()  
}
USE = O:keyUse()
KAT2 = O:keyAttack2()
if(chatClk(O))
{
    AR = lastSaid():explode(" ")
}
if(chatClk(O) & AR:string(1) == "create")
{
    if(AR:string(2))
    {
        ID++
        if(AR:string(2):find("*"))
        {
            M = "models/hunter/plates/plate"+AR:string(2):explode("*"):string(1)+"x"+AR:string(2):explode("*"):string(2)+".mdl"
        }
        else
        {
            M = AR:string(2)
        }
        E = propSpawn(M,O:aimPos(),ang(AR:number(3),AR:number(4),AR:number(5)),1)
        E:setPos(E:pos()+vec(0,0,E:boxSize():length()/3))
#        E:propNotSolid(1) 
        POS:setVector(ID,entity():toLocal(E:pos())+vec(0,0,E:boxSize():length()/3))
        COL:setVector(ID,E:getColor())
        ALP:setNumber(ID,255)
        ANG:setAngle(ID,ang(0,0,0))
        MOD:setString(ID,M)
        R:setEntity(ID,E)
        R2:setNumber(E:id(),ID)
        printTable(R2)
    }
}
if(chatClk(O) & AR:string(1) == "on")
{
    ON = 1
}
if(chatClk(O) & AR:string(1) == "showpos")
{
    print(entity(AR:number(2)):pos():toString())
}
if(chatClk(O) & AR:string(1) == "off")
{
    ON = 0
}
if(chatClk(O) & AR:string(1) == "save")
{
    I = 0
    if(fileCanLoad())
    {
        fileRemove("prop.txt")
        fileLoad("prop.txt")
        print("loading file... please wait")
    }
    else {print("can't load file now, please retry")}
    SAVE++
}
if(chatClk(O) & AR:string(1) == "load")
{
    I = 0
    if(fileCanLoad())
    {
        fileRemove("prop.txt")
        fileLoad("prop.txt")
        print("loading file... please wait")
    }
    else {print("can't load file now, please retry")}
    LOAD++
}
if(chatClk(O) & AR:string(1) == "axis")
{
    if(AR:string(2) == "x") 
    {
        AX = 1
    }
    elseif(AR:string(2) == "y")
    {
        AX = 2
    }
    elseif(AR:string(2) == "z")
    {
        AX = 3
    }
    elseif(AR:string(2) == "off")
    {
        AX = 0
    }
}
if(chatClk(O) & AR:string(1) == "angle")
{
    ENT:setAng(angnorm(ang(AR:number(2),AR:number(3),AR:number(4))))
    ANG:setAngle(R2:number(ENT:id()),ang(AR:number(2),AR:number(3),AR:number(4)))  
}
if(chatClk(O) & AR:string(1) == "addangle")
{
    HE = ENT
    HE:setAng(HE:angles()+angnorm(ang(AR:number(2),AR:number(3),AR:number(4))))
    ANG:setAngle(R2:number(ENT:id()),ang(AR:number(2),AR:number(3),AR:number(4)))  
}
if(chatClk(O) & AR:string(1) == "angle2")
{
    HE = entity(AR:number(2))
    HE:setAng(angnorm(ang(AR:number(3),AR:number(4),AR:number(5))))
    ANG:setAngle(AR:number(2),ang(AR:number(2),AR:number(3),AR:number(4)))   
}
if(chatClk(O) & AR:string(1) == "addangle2")
{
    HE = entity(AR:number(2))
    HE:setAng(HE:angles()+angnorm(ang(AR:number(3),AR:number(4),AR:number(5))))
    ANG:setAngle(AR:number(2),ang(AR:number(2),AR:number(3),AR:number(4)))   
}
if(chatClk(O) & AR:string(1) == "color")
{
    ENT:setColor(vec(AR:number(2),AR:number(3),AR:number(4)))
    COL:setVector(R2:number(ENT:id()),HE:getColor())
}
if(chatClk(O) & AR:string(1) == "color2")
{
    HE = entity(AR:number(2))
    HE:setColor(vec(AR:number(3),AR:number(4),AR:number(5)))
    COL:setVector(AR:number(2),HE:getColor())
}
if(chatClk(O) & AR:string(1) == "position")
{
    ENT:setPos(vec(AR:number(3),AR:number(4),AR:number(5)))
    timer("chk",100)
}
if(chatClk(O) & AR:string(1) == "position2")
{
    HE = entity(AR:number(2))
    HE:setPos(vec(AR:number(3),AR:number(4),AR:number(5)))
    timer("chk",100)
}
if(chatClk(O) & AR:string(1) == "move")
{
    HE = entity(AR:number(2))
    HE:setPos(HE:pos()+vec(AR:number(3),AR:number(4),AR:number(5)))
    timer("chk",100)
}
if(chatClk(O) & AR:string(1) == "clone" & ENT)
{
    if(AR:string(2))
    {
        ID++ 
        TENT = ENT
        if(AR:string(2) == "x")
        {
        E = propSpawn(ENT,ENT:pos()+vec(ENT:boxSize():x(),0,0),1)
        }
        if(AR:string(2) == "y")
        {
        E = propSpawn(ENT,ENT:pos()+vec(0,ENT:boxSize():y(),0),1)
        }
        if(AR:string(2) == "z")
        {
        E = propSpawn(ENT,ENT:pos()+vec(0,0,ENT:boxSize():z()),1)
        }     
        if(AR:string(2) == "-x")
        {
        E = propSpawn(ENT,ENT:pos()-vec(ENT:boxSize():x(),0,0),1)
        }
        if(AR:string(2) == "-y")
        {
        E = propSpawn(ENT,ENT:pos()-vec(0,ENT:boxSize():y(),0),1)
        }
        if(AR:string(2) == "-z")
        {
        E = propSpawn(ENT,ENT:pos()-vec(0,0,ENT:boxSize():z()),1)
        }        
        E:setAlpha(TENT:getAlpha())
        E:setColor(TENT:getColor())
        E:setAng(TENT:angles())
        HE = TENT
        POS:setVector(ID,entity():toLocal(HE:pos()))
        print(POS:vector(ID):toString())
        COL:setVector(ID,HE:getColor())
        print(COL:vector(ID):toString())
        ALP:setNumber(ID,HE:getAlpha())
        print(ALP:number(ID):toString())
        MOD:setString(ID,HE:model())
        print(MOD:string(ID))
        ANG:setAngle(ID,HE:angles())
        print(ANG:angle(ID):toString())
    }
}
if(chatClk(O) & AR:string(1) == "alpha2")
{
    AL = AR:number(3)
    ALP:setNumber(AR:number(2),HE:getAlpha())
}
if(chatClk(O) & AR:string(1) == "DEBUG")
{
    timer("chk",100)
}
if(chatClk(O) & AR:string(1) == "remove")
{
    POS:removeVector(AR:number(2))
    COL:removeVector(AR:number(2))
    ALP:removeNumber(AR:number(2))
    ANG:removeAngle(AR:number(2))
    R:removeEntity(AR:number(2))
    R2:removeNumber(AR:number(2))
    entity(AR:number(2)):propDelete()
}
if(chatClk(O) & AR:string(1) == "parent")
{
    I = 0
    if(AR:number(2))
    {
        print("prop Parented")
    }
    elseif(AR:string(2) == "all")
    { 
        timer("all",0)
        print("current prop Parented to all")
    }
    elseif(AR:string(2) == "aim")
    {
        if(AR:string(3) == "all")
        {
            I = 0
            timer("parent1",0)
            print("all prop Parented to Entity")
        }
        else
        {
            R:entity(R2:number(AR:number(3))):parentTo(O:aimEntity())
            print("current prop Parented to Entity")
        }
    }
    elseif(AR:string(2) == "e2")
    {
        if(AR:string(3) == "all")
        {
            I = 0
            timer("parent2",0)
            print("all prop Parented to E2")
        }
        else
        {
            R:entity(AR:number(3)):parentTo(entity())
            print("current prop Parented to E2")
        }
    }
    elseif(AR:string(2) == "owner")
    {
        if(AR:string(3) == "all")
        {
            I = 0
            timer("parent3",0)
            print("all prop Parented to "+O:name())
        }
        else
        {
            ENT:parentTo(O)
            print("current prop Parented to "+O:name())
        }
    }
}
if(chatClk(O) & AR:string(1) == "unparent")
{
    if(AR:string(2) == "all")
    {
        timer("unparent",0)
    }
    else
    {
        ENT:deparent()
    }
}
if(chatClk(O) & AR:string(1) == "solid")
{
    if(AR:string(2) == "all")
    {
        timer("solid",0)
    }
    else
    {
        R[AR:number(2),entity]:propNotSolid(AR:number(3))
    }
}   

if(chatClk(O) & AR:string(1) == "alpha")
{
    ENT:setAlpha(AR:number(2))
    ALP:setNumber(R2:number(ENT:id()),AR:number(2))
    print("alpha is set to "+AR:string(2))
}
if(KAT2)
{
    if($KAT2)
    {
        TENT = ENT
        ENT:propFreeze(1)
        Dist = O:shootPos():distance(TENT:pos())
    }    
    if(AX == 1)
    {
        ENT:setPos(ENT:pos():setX((O:shootPos()+O:eye()*Dist):x()))
    }
    if(AX == 2)
    {
        ENT:setPos(ENT:pos():setY((O:shootPos()+O:eye()*Dist):y()))
    }
    if(AX == 3)
    {
        ENT:setPos(ENT:pos():setZ((O:shootPos()+O:eye()*Dist):z()))
    }
    if(AX == 0)
    {
        ENT:setPos(O:shootPos()+O:eye()*Dist)
    }
}   
elseif(!KAT2 & $KAT2)
{
    print(round(TENT:pos()):toString())
    POS:setVector(R2:number(ENT:id()),ENT:pos())
    timer("chk",100)
}
if(ON & !KAT2)
{
    ENT = O:aimEntity()
    if(ENT)
    {
        A = 1
    }
    else
    {
        A = 0
    }
}
#end
if(ENT & $A)
{
    print(4,ENT:id():toString())
    hint("You are looking at prop "+ENT:id():toString(),3)
}
if(!ENT & $A)
{
    hint("You are not looking any props",3)
}
if(fileClk() & fileLoaded("prop.txt") & SAVE)
{
    SAVE = 0
    timer("save",0)
    print("load complete!")
}
if(fileClk() & fileLoaded("prop.txt") & LOAD)
{
    LOAD = 0
    I = 0
    R2 = array()
    R = array()
    SL = glonDecode(fileRead("prop.txt"))
    POS = glonDecode(SL:string(1))
    COL = glonDecode(SL:string(2))
    ALP = glonDecode(SL:string(3))
    MOD = glonDecode(SL:string(4))
    ANG = glonDecode(SL:string(5))
    print("load complete!")
    timer("loadloop",0)
}
if(clk("loadloop"))
{
    I++
    if(POS:vector(I))
    {
        E = propSpawn(MOD:string(I),entity():pos()+POS:vector(I),1)
        print(round((I/MOD:count())*100):toString()+"% complete")
        E:setColor(COL:vector(I))
        E:setAlpha(ALP:number(I))
        E:setAng(ANG:angle(I))
        R:setEntity(E:id(),E)
        R2:pushNumber(E:id())
#        E:propNotSolid(1)
    }
    if(SL:number(6)<I) {timer("load",0)}
    else {timer("loadloop",250)}
}

if(clk("save"))
{
    SL = array()
    SL:pushString(glonEncode(POS))
    SL:pushString(glonEncode(COL))
    SL:pushString(glonEncode(ALP))
    SL:pushString(glonEncode(MOD))
    SL:pushString(glonEncode(ANG))
    SL:pushNumber(ID)
    fileWrite("prop.txt",glonEncode(SL))
    print("save compelte!") 
}
if(clk("load"))
{
    ID = SL:number(6)+1
    I = 0
    print("load complete!")
    timer("R2",0)
}
if(clk("unparent"))
{
    while(1)
    {
        R:entity(R2[I,number]):deparent()
        if(I>ID) {I = 0 break}
        if(opcounter()+20000>maxquota()) {timer("unparent",0) break}
        I++
    }
}
if(clk("parent1"))
{
    while(1)
    {
        I++
        R:entity(R2[I,number]):parentTo(ENT)
        if(I>ID) {I = 0 break}
        if(opcounter()+20000>maxquota()) {timer("parent1",0) break}
    }
}
if(clk("parent2"))
{
    while(1)
    {
        R:entity(R2[I,number]):parentTo(entity())
        if(I>ID) {I = 0 break}
        if(opcounter()+20000>maxquota()) {timer("parent2",0) break}
        I++
    }
}
if(clk("parent3"))
{
    while(1)
    {
        R:entity(R2[I,number]):parentTo(O)
        if(I>ID) {I = 0 break}
        if(opcounter()+20000>maxquota()) {timer("parent3",0) break}
        I++
    }
}
if(clk("chk"))
{
    while(1)
    {
        HE = R:entity(I)
        if(HE)
        {
            POS:setVector(I,entity():toLocal(HE:pos()))
        }
        if(I>ID) {I = 0 break}
        if(opcounter()+20000>maxquota()) {timer("chk",100) break}
        I++
    }
}
if(clk("all"))
{
    while(1)
    {
        R:entity(R2[I,number]):parentTo(entity(AR:number(3)))
        entity(AR:number(3)):parentTo(R:entity(I))
        if(I>ID) {I = 0 break}
        if(opcounter()+20000>maxquota()) {timer("all",0) break}
        I++
    }
}
if(clk("solid"))
{
    while(1)
    {
        R[R2[I,number],entity]:propNotSolid(AR:number(3))
        if(I>ID) {I = 0 break}
        if(opcounter()+20000>maxquota()) {timer("solid",0) break}
        I++
    }
}

