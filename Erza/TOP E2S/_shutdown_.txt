@name EGP Drag and Unlock Screen
@inputs EGP:wirelink User:entity Shutoff Override
@outputs Pass
@persist [O E]:entity [Keys Points]:table Lines:array [Size Snap Index]:vector2 Passcode:string [Mode Countdown Cur Use Line Fails Failed Time Set]
@trigger User Shutoff Override

if( dupefinished() ){ reset() }

if( first() )
{
    # SET THE TIME TO REMAIN OPEN WHEN YOU PASS ( IN SECONDS )
    Time = 5
    
    runOnChat(1)
    
    EGP:egpClear()
    
    O = owner()
    E = entity():isWeldedTo() ?: entity()
    
    if( E["passcode",string] == "" )
    {
        E["passcode",string] = "abdegh"
    }
    
    runOnKeys( O, 1 )
    
    function number getID( N )
    {
        return EGP:egpNumObjects() + N
    }
    function resetScreen()
    {
        EGP:egpLoadFrame("default")
        Lines:clear()
        Points:clear()
        Passcode = ""
        Use = 0
    }
    function dropCircle( [V1 V2]:vector2 )
    {
        EGP:egpCircle( getID(1), Snap, Size/4 )
        EGP:egpColor( getID(0), hsv2rgb(120,1,1) )
            EGP:egpCircleOutline( getID(1), vec2(), Size/3.5 )
            EGP:egpColor( getID(0), hsv2rgb(120,1,1) )
            EGP:egpParent( getID(0), getID(-1) )
    }
    
    Keys = table( "1,1" = "a",
                "1,2" = "d",
                "1,3" = "g",
                "2,1" = "b",
                "2,2" = "e",
                "2,3" = "h",
                "3,1" = "c",
                "3,2" = "f",
                "3,3" = "i"
                )
    
    Screen = vec2(512,512)
    Dots = vec2(3,3)
    Size = Screen / Dots
    
    EGP:egpBox( getID(1), vec2(256,256), vec2(512) )
    EGP:egpMaterial( getID(0), "vgui/gradient-r" )
    EGP:egpAngle( getID(0), 180 )
    EGP:egpColor( getID(0), vec(150) )
    
    EGP:egpBox( getID(1), vec2(256,256), vec2(512) )
    EGP:egpMaterial( getID(0), "vgui/gradient-u" )
    EGP:egpColor( getID(0), vec(100) )
    
    EGP:egpBox( getID(1), vec2(256,256), vec2(512) )
    EGP:egpMaterial( getID(0), "vgui/gradient-d" )
    EGP:egpColor( getID(0), vec(50) )
    
    for( X = 0, 2 )
    {
        for( Y = 0, 2 )
        {
            EGP:egpCircle( getID(1), Size/2 + Size*vec2(X,Y), Size/3 )
            EGP:egpMaterial( getID(0), "vgui/gradient-r" )
            EGP:egpAngle( getID(0), 0 )
            EGP:egpColor( getID(0), hsv2rgb(0,0,0.5) )
            
            EGP:egpCircle( getID(1), Size/2 + Size*vec2(X,Y), Size/3 )
            EGP:egpMaterial( getID(0), "vgui/gradient-u" )
            EGP:egpColor( getID(0), hsv2rgb(0,0,0.15) )
            
            EGP:egpCircle( getID(1), Size/2 + Size*vec2(X,Y), Size/3.5 )
            EGP:egpColor( getID(0), vec(50) )
            
            EGP:egpCircle( getID(1), Size/2 + Size*vec2(X,Y), Size/3.5 )
            EGP:egpMaterial( getID(0), "vgui/gradient-r" )
            EGP:egpAngle( getID(0), 80 )
            EGP:egpColor( getID(0), vec(0) )
            
            EGP:egpCircle( getID(1), Size/2 + Size*vec2(X,Y), Size/3.5 )
            EGP:egpMaterial( getID(0), "vgui/gradient-r" )
            EGP:egpAngle( getID(0), -200 )
            EGP:egpColor( getID(0), vec(0) )
        }
    }
    
    EGP:egpBox( getID(1), vec2(), vec2() )
    Line = getID(0)
    
    EGP:egpCircle( getID(1), -Size/2, Size/4 )
    EGP:egpColor( getID(0), hsv2rgb(0,1,0.5) )
    Cur = getID(0)
    
        EGP:egpCircleOutline( getID(1), vec2(), Size/3.5 )
        EGP:egpColor( getID(0), hsv2rgb(0,1,1) )
        EGP:egpParent( getID(0), getID(-1) )
    
    EGP:egpText( getID(1), "LockCode", vec2(512-5) )
    EGP:egpAlign( getID(0), 2, 2 )
    EGP:egpAlpha( getID(0), 20 )
    
    EGP:egpSaveFrame("default")
}

if( ~User & User )
{
    runOnKeys(O,0)
    O = User
    runOnKeys(O,1)
}

if( ~Shutoff & Shutoff )
{
    Pass = 0
}

if( ~Override & Override )
{
    Pass = 1
    timer("pass_reset",Time*1000)
}

interval(90)

if( !Failed )
{
    local Range = inrange( Snap, vec2(), vec2(512) )
    if( keyClk(O) & Range )
    {
        Key = keyClkPressed()
        Pressed = O:keyPressed(Key)
        
        switch( Key )
        {
            default,
            break
            
            case "e",
                Use = Pressed
                
                if( Use )
                {
                    dropCircle( Snap, Size/4 )
                    
                    Lines:pushVector2( Snap )
                    Lines:pushVector2( Snap )
                    Points["p:"+Index[1]+","+Index[2],number] = 1
                    Passcode = Passcode + Keys[Index[1]+","+Index[2],string]
                }
                else
                {
                    Fails = Passcode == E["passcode",string] ? 0 : Lines:count() > 2 ? Fails + 1 : Fails
                    
                    if( Fails < 5 )
                    {
                        stoptimer("pass_reset")
                        if( Mode )
                        {
                            if( Lines:count() > 2 )
                            {
                                E["passcode",string] = Passcode
                                EGP:entity():soundPlay("newpass",1,"@garrysmod/content_downloaded.wav")
                            }
                            else
                            {
                                EGP:entity():soundPlay("newpass",1,"@buttons/button10.wav")
                            }
                            Mode = 0
                        }
                        else
                        {
                            Pass = Passcode == E["passcode",string]
                            EGP:entity():soundPlay("accept",0.637, Pass ? "@buttons/button9.wav" : "@buttons/button8.wav" )
                            timer("pass_reset",Time*1000)
                            if( !Pass )
                            {
                                concmd("ulx slap "+O:name():explode(" ")[1,string])
                            }
                        }
                        
                        resetScreen()
                    }
                    else
                    {
                        EGP:egpBox( getID(1), vec2(256,256), vec2(512,512) )
                        EGP:egpColor( getID(0), vec() )
                        
                        EGP:egpTextLayout( getID(1), "You have incorrectly drawn your\nunlock pattern 5 times.\n\nTry again in 30 seconds.", vec2(), vec2(512) )
                        EGP:egpAlign( getID(0), 1, 1 )
                        EGP:egpFont( getID(0), "Courier New", 40 )
                        Countdown = getID(0)
                        
                        EGP:entity():soundPlay("alarm",0,"@ambient/alarms/klaxon1.wav",1)
                        Failed = 30
                        timer("failcount",1000)
                        concmd("ulx slay "+O:name())
                    }
                }
            break
        }
    }
    
    if( egpCanSendUmsg() )
    {
        Cursor = EGP:egpCursor( O )
        
        Snap = floor( Cursor / Size ) * Size + Size / 2
        
        Index = Snap / Size + vec2(0.5)
        
        if( changed(Snap) | changed(Range) )
        {
            if( Range )
            {
                EGP:egpPos( Cur, Snap )
                
                if( Use )
                {
                    if( !Points["p:"+Index[1]+","+Index[2],number] )
                    {
                        dropCircle( Snap, Size/4 )
                        
                        Lines:pushVector2( Snap )
                        Points["p:"+Index[1]+","+Index[2],number] = 1
                        Passcode = Passcode + Keys[Index[1]+","+Index[2],string]
                        
                        EGP:entity():soundPlay("click",0,"buttons/button1.wav",0.05)
                        soundVolume("click",0.5)
                    }
                }
            }
            elseif( !Mode )
            {
                resetScreen()
            }
        }
    }
    
    if( changed(Lines:count()) )
    {
        EGP:egpLineStrip( Line, Lines )
        EGP:egpColor( Line, hsv2rgb(120-30,1,1) )
        EGP:egpMaterial( Line, "vgui/gradient-r" )
        EGP:egpSize( Line, 10 )
    }
    
    if( chatClk(owner()) & Range )
    {
        local Lsx = owner():lastSaid():lower():explode(" ")
        
        switch( Lsx[1,string] )
        {
            default,
            break
            
            case "set",
                if( Lsx[2,string] == "password" )
                {
                    if( Lsx[3,string] == "silent" )
                    {
                        E["passcode",string] = Lsx[4,string]
                    }
                    else
                    {
                        Mode = 1
                        
                        EGP:egpTextLayout( getID(1), "Set your new passcode now.", vec2(), vec2(512) )
                        EGP:egpAlign( getID(0), 1, 2 )
                        EGP:egpFont( getID(0), "coolvetica", 30 )
                        Set = getID(0)
                        
                        Fails = 0
                    }
                }
                hideChat(1)
            break
        }
    }
}

if( clk("failcount") )
{
    Failed--
    
    if( Failed == 0 )
    {
        Fails = 0
        EGP:egpLoadFrame("default")
        Lines:clear()
        Points:clear()
        Passcode = ""
    }
    else
    {
        EGP:egpSetText( Countdown, "You have incorrectly drawn your\nunlock pattern 5 times.\n\nTry again in "+Failed+" seconds." )
        timer("failcount",1000)
    }
}

if( clk("pass_reset") )
{
    Pass = 0
}
