@name PRP Gun Shop Control
@inputs EGPD1:wirelink EGPD2:wirelink EGPC:wirelink User:entity W1:entity W2:entity
@outputs 
@persist KUse CSR:vector2 E:entity O:entity
@persist D1C:vector D2C:vector Screen:string SP:array WindowI SongName:array SongIndex:array FContents:array Scrolling ScrollID
@trigger 
interval(100)
if(first()){
    ## Variable setups
    O=owner()
    E=entity()
    Screen="main"
    Scrolling=0
    ScrollID=0
    D1C=vec(100,100,100)
    D2C=vec(100,100,100)
    EGPD1:egpClear()    
    EGPD2:egpClear()
    EGPC:egpClear()
    EGPD1:egpDrawTopLeft(1)
    EGPD2:egpDrawTopLeft(1)
    EGPC:egpDrawTopLeft(1)
    function number ir(Cursor:vector2,Index){
        Vec1=EGPC:egpPos(Index)
        Vec2=EGPC:egpPos(Index)+EGPC:egpSize(Index)
        C1=0
        C2=0
        if(Cursor:x()>=Vec1:x() & Cursor:y()>=Vec1:y()){
            C1=1
        }
        if(Cursor:x()<=Vec2:x() & Cursor:y()<=Vec2:y()){
            C2=1
        }
        if(C1==1&C2==1)
        {
            return 1
        }
        else
        {
            return 0
        }    
    }
    ## File functions
    fileLoad(">e2shared/songs.txt")
    
    ##EGP Setup
    EGPC:egpText(1,"Control",vec2(0,0))
    
    EGPD1:egpBox(2,vec2(0,0),vec2(512,512))
    EGPD1:egpColor(2, vec(0,100,255))
    
    EGPD2:egpBox(2,vec2(0,0),vec2(512,512))
    EGPD2:egpColor(2, vec(0,100,255))
    
    EGPD1:egpRoundedBox(3, vec2(5,5),vec2(502,502))
    EGPD1:egpColor(3,D1C)
    
    
    EGPD2:egpRoundedBox(3, vec2(5,5),vec2(502,502))
    EGPD2:egpColor(3,D2C)
    
##EGP Control Declarations
    #Display 1 settings
    EGPC:egpBox(2, vec2(50,50),vec2(150,50))
    EGPC:egpColor(2, D1C)
    EGPC:egpText(3, "Display 1",vec2(-70,-25))
    EGPC:egpColor(3, vec(0,0,0))
    EGPC:egpParent(3,2)
    
    #Display 2 Settings
    EGPC:egpBox(4, vec2(50,150),vec2(150,50))
    EGPC:egpColor(4, D2C)
    EGPC:egpText(5, "Display 2",vec2(-70,-25))
    EGPC:egpColor(5, vec(0,0,0))
    EGPC:egpParent(5,4)
    
    #Base Control
    EGPC:egpBox(6, vec2(50,250),vec2(150,50))
    EGPC:egpColor(6, vec(255,0,50))
    EGPC:egpText(7, "Shop Control",vec2(-70,-25))
    EGPC:egpColor(7, vec(0,0,0))
    EGPC:egpParent(7,6)
}
KUse=User:keyUse()
CSR=EGPC:egpCursor(User)
EGPC:egpSetText(1, "Control."+Screen)
## TIMERS ##
if(clk("next")){
    if(WindowI<=10){   
        SP[WindowI,entity]=propSpawn(W1,W1:pos(),W1:angles(),1)
    }
    elseif(WindowI>=11){   
       SP[WindowI,entity]=propSpawn(W2,W2:pos(),W2:angles(),1)
    }
    EGPC:egpSetText(1,"Control."+Screen+"   -("+WindowI+"/20)") 
    WindowI++
    if(WindowI<20){timer("next",1500)}       
}    
    
if(changed(fileLoaded())&fileLoaded()){
    print("loaded and reading")
    if(fileRead()!=""){
        FContents=fileRead():explode(";")
        I1=1
        I2=1
        for(I=1,FContents:count()){
            if(I%2==0){
                SongIndex[I1,string]=FContents[I,string]
                I1++
            } 
            elseif(I%2!=0){
                SongName[I2,string]=FContents[I,string]
                I2++   
            }
            if(I==FContents:count()){
                print("File Loaded Succesfully")
            }
        }
    }
    else{
        print("file not loaded correctly!")
    }    
} 
if(KUse){
    if(Screen=="main"){
        if(inrange(CSR,EGPC:egpPos(2),EGPC:egpPos(2)+EGPC:egpSize(2))){
            EGPC:egpAlpha(2,0)
            EGPC:egpAlpha(4,0)  
            EGPC:egpAlpha(6,0)
            Screen="d1"      
        }
        if(inrange(CSR,EGPC:egpPos(4),EGPC:egpPos(4)+EGPC:egpSize(4))){
            EGPC:egpAlpha(2,0)
            EGPC:egpAlpha(4,0)  
            EGPC:egpAlpha(6,0)
            Screen="d2"      
        }
        if(inrange(CSR,EGPC:egpPos(6),EGPC:egpPos(6)+EGPC:egpSize(6))){
            EGPC:egpAlpha(2,0)
            EGPC:egpAlpha(4,0)  
            EGPC:egpAlpha(6,0)
            Screen="base"      
        }
    }
    elseif(Screen=="base"){
        #MAKE SHUTTER WINDOWS LIKE LUXARY HOUSE
        if(ir(CSR,10)){
            #winShut()
            #E:soundPlay(10,1,"ambient/alarms/warningbell1.wav")
            soundPurge()
        }
        if(ir(CSR,11)){
            for(I=101,132){
                if(ir(CSR,I)){
                    soundPurge()
                    entity():soundPlay(1,soundDuration(SongIndex[I-100,string]),SongIndex[I-100,string])   
                }    
            }    
        }    
    }
    elseif(Screen=="d1"){
        if(ir(CSR,10)){
            Scrolling=1
            ScrollID=10  
        }
        elseif(ir(CSR,12)){
            Scrolling=1
            ScrollID=12
        }
        elseif(ir(CSR,14)){
            Scrolling=1
            ScrollID=14
        }    
    }
    
    if(Screen!="main"){
        if(ir(CSR,9)){
            Screen="main"    
        }
    }  
}
if(Scrolling==1){
    if(CSR:y()>30 & CSR:y()<=200){
        EGPC:egpPos(ScrollID,vec2(EGPC:egpPos(ScrollID):x(),CSR:y()))    
    }    
}
if(changed(Screen)){
    for(I=8,30){
        EGPC:egpRemove(I)    
    }
    if(Screen=="d1"){
        EGPC:egpBox(8, vec2(20,20), vec2(472,256))
        EGPC:egpColor(8, D1C)
        
        EGPC:egpBox(9, vec2(472,20), vec2(20,20))
        
        EGPC:egpBox(10, vec2(30,25),vec2(20,30))
        
        EGPC:egpBox(11, vec2(30,30),vec2(200,20))  
        EGPC:egpColor(11, vec(D1C:x(),0,0))
        
        EGPC:egpBox(12, vec2(30,65),vec2(20,30))  
        
        EGPC:egpBox(13, vec2(30,70),vec2(200,20))  
        EGPC:egpColor(13, vec(0,D1C:y(),0))
        
        EGPC:egpBox(14, vec2(30,105),vec2(20,30))  
        
        EGPC:egpBox(15, vec2(30,110),vec2(200,20))      
        EGPC:egpColor(15, vec(0,0,D1C:z()))
        
        EGPC:egpOrder(10,11)
        EGPC:egpOrder(12,13)
        EGPC:egpOrder(14,15)
    
    }
    elseif(Screen=="d2"){
        EGPC:egpBox(8, vec2(20,20), vec2(472,256))
        EGPC:egpColor(8, D1C)
        
        EGPC:egpBox(9, vec2(472,20), vec2(20,20)) 
    
    }  
    elseif(Screen=="base"){
        EGPC:egpBox(8, vec2(20,20), vec2(472,472))
        EGPC:egpColor(8, vec(100,0,0))
        
        EGPC:egpBox(9, vec2(472,20), vec2(20,20))
        
        EGPC:egpBox(10, vec2(25,410), vec2(462,66))
        EGPC:egpColor(10, vec(255,0,0))
        
        EGPC:egpBox(11, vec2(25,41), vec2(462,360))
        EGPC:egpColor(11, vec(0,0,100))
        
        X=26
        C=0
        for(I=1,31){
            if(I<=24){
                X=26
                C=0    
            }
            else{
                X=26+(462/2)
                C=24*15    
            }
            EGPC:egpText(I+11,I+":"+SongName[I,string],vec2(X,25+((I*15)-C)))
            EGPC:egpBox(100+I,vec2(X,29+((I*15)-C)),vec2(462/2,10))
            EGPC:egpAlpha(100+I,0)    
        }    
    }
    elseif(Screen=="main"){
        EGPC:egpAlpha(2,255)
        EGPC:egpAlpha(4,255)
        EGPC:egpAlpha(6,255)
        for(I=1,31){
            EGPC:egpRemove(I+11)    
        }    
    }      
}
