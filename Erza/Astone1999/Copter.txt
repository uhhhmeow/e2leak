@name Copter
@inputs Chair:entity Fly Active 
@outputs 
@persist [E P]:entity FC FM
if(dupefinished())
{reset()}
if(first())
{
    runOnTick(1)
    
    entity():setColor(255,255,255)
    
    E = entity():isConstrainedTo()
    if(E:isValid())
    {
  
        
        if(E:isVehicle())
        {
            selfDestruct()
        }else
        {
            P = E:isConstrainedTo()
        }
    }else
    {
        selfDestruct()
    }
    
    FC = 9
    
    holoCreate(1) #Remove this holo stuff here if you don't want a holo showing how much force is being applied
    holoModel(1,"hq_cylinder")
    holoAng(1,E:toWorld(ang(0,0,5)))
    holoPos(1,E:toWorld(vec(20,40,5.5)))
    holoDisableShading(1,1)
    holoParent(1,E)
      holoScale(1,vec(0,0,0))
}
if(Fly==1&&Active==1)
{
Dri = Chair:driver()
Active = Chair:driver():isValid()
if(Active)
{
    W = Dri:keyForward()
    A = Dri:keyLeft()
    S = Dri:keyBack()
    D = Dri:keyRight()
    Spc = Dri:keyJump()
    Shift = Dri:keySprint()
    M1 = Dri:keyAttack1()
    M2 = Dri:keyAttack2()
    R = Dri:keyReload()
    
    FM+=0.01
}else
{
  
    FM-=0.0035
}
if(A)
{Left=1}
else
{Left=0}
if(D)
{Right=1}
else
{Right=0}
if(W|S)
{Pressed=1}
else
{Pressed=0}
FM = clamp(FM,0,1)
  holoScale(1,vec(0,0,0))
if(changed(FM)) #Remove this block of code here if you don't want to see the visual representation of the amount of force being applied
{
    local N = FM*255
    holoColor(1,vec(255-N,N,0))

    holoPos(1,E:toWorld(vec(20,40,0.5+FM*6)))
}
 
E:applyForce((E:up()*(FC+((M1-M2)*16))-(E:vel()*vec(0.0025,0.0025,0.005)))*E:mass()*FM)
E:applyAngForce((ang((Right-Left)*65,(Left-Right)*60,((S-W)*1.5)*30-((Pressed)*(Shift*15)))-(E:angles()*ang(1.5,0,1.5))-E:angVel()*0.75)*E:mass()*FM)
}

