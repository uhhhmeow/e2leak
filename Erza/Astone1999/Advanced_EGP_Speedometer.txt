@name Advanced EGP Speedometer
@inputs E:wirelink
@outputs MPH KMH Angle
@persist Axis Text UseMPH

if (first()) {    
    #Change this if you want it to display the 
    #speed in mph rather than km/h
    UseMPH = 0
    
    #Background
    X=1
    E:egpBox(X,vec2(256,256),vec2(512,512))
    E:egpColor(X,vec(70,70,70))
    
    #Circle background
    X++
    E:egpCircle(X,vec2(256,256),vec2(230,230))
    E:egpColor(X,vec(165,35,35))
    
    #Circle outline
    X++
    E:egpCircleOutline(X,vec2(256,256),vec2(240,240))
    E:egpSize(X,25)
    E:egpColor(X,vec())
    E:egpFidelity(X,36)
    X++
    E:egpCircleOutline(X,vec2(256,256),vec2(240,240))
    E:egpSize(X,25)
    E:egpAngle(X,45)
    E:egpColor(X,vec())
    E:egpFidelity(X,36)
    
    #Create all the numbers and lines
    J=0
    for(I=0,190,10) {
        J++
        
        #For making every third element slightly larger
        IsThird = (J%3) == 0
        
        #Calculate angle for position
        Angle = I/190*270-45
        
        #Give the larger texts a little offset
        Offset = 0
        if (IsThird) {
            if (I > 100) {Offset = 20} 
            else {Offset = 5}
        }
        Pos = vec2(256,256) - vec2(175-Offset,0):rotate(Angle)
        
        #Create text
        X++
        E:egpText(X,""+I,Pos)
        E:egpAlign(X,1,1)
        E:egpSize(X,( IsThird ? 30 : 10 ))
        E:egpColor(X,vec())
        
        #Create line
        X++
        LinePos1 = vec2(256,256) - vec2(240,0):rotate(Angle)
        LinePos2 = vec2(256,256) - vec2(190 + (!IsThird ? 20 : 0),0):rotate(Angle)
        E:egpLine(X,LinePos1,LinePos2)
        E:egpSize(X,(IsThird ? 10 : 5))
        E:egpColor(X,vec())
    }
    
    #Arrow
    X++
    E:egpLine(X,vec2(200,0),-vec2(20,0))
    E:egpSize(X,5)
    E:egpColor(X,vec())
    
    #Center axis circle
    X++
    Axis = X
    E:egpCircle(X,vec2(256,256),vec2(10,10))
    E:egpColor(X,vec())
    
    #Parent the arrow to the axis
    E:egpParent(X-1,X)
    
    X++
    Text = X
    E:egpText(X,(UseMPH ? "mph: 0" : "km/h: 0" ),vec2(160,400))
    E:egpColor(X,vec())
    E:egpSize(X,40)
}

#Update the E2 every 0.2 seconds
interval(200)

#Get miles and kilometers per hour
Vel = entity():vel():length()
MPH = toUnit("mph",Vel)
KMH = toUnit("km/h",Vel)

if (Vel > 0) {

    #Calculate angle of arrow
    if (UseMPH) {
        Vel2 = clamp(round(MPH),0,190)
        Angle = 225-Vel2/190*270
        E:egpAngle(Axis,Angle)
        
        E:egpSetText(Text,"mph: " + round(MPH))
    } else {
        Vel2 = clamp(round(KMH),0,190)
        Angle = 225-Vel2/190*270
        E:egpAngle(Axis,Angle)
        
        E:egpSetText(Text,"km/h: " + round(KMH))
    }

}
