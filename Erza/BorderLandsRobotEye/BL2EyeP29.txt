@name Holopad Export

#####
# Holograms authored by Erza on 14/09/2015
# Exported from Holopad 09/11/2012 (BETA 6.7) by Bubbus
# Thanks to Vercas for the original E2 export template!
#
# FOR AN EXPLANATION OF THE CODE BELOW, VISIT http://code.google.com/p/holopad/wiki/NewE2ExportFormatHOWTO
##### 

#####
# Hologram spawning data
@persist [Holos Clips]:table HolosSpawned HolosStep LastHolo TotalHolos
@persist E:entity
#####


if (first() | duped())
{
    E = entity()

    function number addHolo(Pos:vector, Scale:vector, Colour:vector4, Angles:angle, Model:string, Material:string, Parent:number)
    {
        if (holoRemainingSpawns() < 1) {error("This model has too many holos to spawn! (" + TotalHolos + " holos!)"), return 0}
        
        holoCreate(LastHolo, E:toWorld(Pos), Scale, E:toWorld(Angles))
        holoModel(LastHolo, Model)
        holoMaterial(LastHolo, Material)
        holoColor(LastHolo, vec(Colour), Colour:w())

        if (Parent > 0) {holoParent(LastHolo, Parent)}
        else {holoParent(LastHolo, E)}

        local Key = LastHolo + "_"
        local I=1
        while (Clips:exists(Key + I))
        {
            holoClipEnabled(LastHolo, 1)
            local ClipArr = Clips[Key+I, array]
            holoClip(LastHolo, I, holoEntity(LastHolo):toLocal(E:toWorld(ClipArr[1, vector])), holoEntity(LastHolo):toLocalAxis(E:toWorldAxis(ClipArr[2, vector])), 0)
            I++
        }
        
        return LastHolo
    }

    ##########
    # HOLOGRAMS
    

    #[   ]#    Holos[1, array] = array(vec(-26.3489, 0.0000, -39.5466), vec(2.1472, 1.7164, 1.0000), vec4(255, 191, 0, 255), ang(-88.5401, 0.0000, 0.0000), "models/sprops/rectangles/size_4/rect_36x60x3.mdl", "phoenix_storms/concrete1", 0)
    #[   ]#    Holos[2, array] = array(vec(19.5530, -33.6620, -102.3990), vec(1.2584, 1.4707, 1.4360), vec4(255, 191, 0, 255), ang(0.0000, 0.0000, 0.0000), "hq_rcube", "phoenix_storms/concrete1", 0)
    #[   ]#    Holos[3, array] = array(vec(22.4075, 0.3157, 29.2981), vec(1.0000, 1.0000, 1.0000), vec4(255, 191, 0, 255), ang(0.0000, -89.1923, 31.7521), "models/sprops/rectangles/size_2_5/rect_18x30x3.mdl", "phoenix_storms/concrete1", 0)
    #[   ]#    Holos[4, array] = array(vec(-11.7991, 0.0000, -111.9477), vec(1.0000, 1.0000, 1.0000), vec4(99, 99, 99, 255), ang(0.0000, 0.0000, 0.0000), "models/sprops/rectangles/size_5/rect_48x48x3.mdl", "phoenix_storms/concrete1", 0)
    #[   ]#    Holos[5, array] = array(vec(0.0090, -39.7730, -65.5790), vec(1.2210, 1.3190, 0.7980), vec4(255, 191, 0, 255), ang(-88.4080, -89.9870, 180.0000), "models/sprops/rectangles/size_4_5/rect_42x66x3.mdl", "phoenix_storms/concrete1", 0)
    #[   ]#    Holos[6, array] = array(vec(0.0000, -24.3880, 28.4920), vec(1.0000, 1.0000, 1.0000), vec4(255, 191, 0, 255), ang(0.0000, 0.0000, -25.4600), "models/sprops/rectangles/size_2_5/rect_18x30x3.mdl", "phoenix_storms/concrete1", 0)
    #[   ]#    Holos[7, array] = array(vec(0.0000, 24.3884, 28.4921), vec(1.0000, 1.0000, 1.0000), vec4(255, 191, 0, 255), ang(0.0000, 0.0000, 25.4600), "models/sprops/rectangles/size_2_5/rect_18x30x3.mdl", "phoenix_storms/concrete1", 0)
    #[   ]#    Holos[8, array] = array(vec(-21.2560, 24.9640, 30.3850), vec(1.2220, 1.7810, 1.0000), vec4(255, 191, 0, 255), ang(0.0000, 0.0000, 0.0000), "hq_rcube_thin", "phoenix_storms/concrete1", 0)
    #[   ]#    Holos[9, array] = array(vec(-0.0040, -36.5180, 4.2630), vec(0.9030, 1.3190, 0.7980), vec4(255, 191, 0, 255), ang(-81.5574, 90.0130, 0.0000), "models/sprops/rectangles/size_4_5/rect_42x66x3.mdl", "phoenix_storms/concrete1", 0)
    #[   ]#    Holos[10, array] = array(vec(0.0000, 21.1700, 34.2600), vec(1.0000, 1.0000, 1.0000), vec4(0, 0, 0, 255), ang(0.0000, 0.0000, 0.0000), "models/sprops/rectangles/size_1_5/rect_6x36x3.mdl", "phoenix_storms/concrete1", 0)
    #[   ]#    Holos[11, array] = array(vec(-30.2730, -0.0000, -53.8152), vec(1.0000, 1.0000, 3.6215), vec4(255, 191, 0, 255), ang(-88.2438, -180.0000, -180.0000), "hq_cylinder", "phoenix_storms/concrete1", 0)
    #[   ]#    Holos[12, array] = array(vec(22.8640, -24.9640, 33.4600), vec(1.2220, 1.7810, 1.0000), vec4(255, 191, 0, 255), ang(0.0000, 0.0000, 0.0000), "hq_rcube_thin", "phoenix_storms/concrete1", 0)
    #[   ]#    Holos[13, array] = array(vec(-26.4553, 0.3360, 30.5754), vec(1.2173, 2.5018, 1.0000), vec4(255, 191, 0, 255), ang(0.0000, 0.0000, 0.0000), "hq_rcube_thin", "phoenix_storms/concrete1", 0)
    #[   ]#    Holos[14, array] = array(vec(22.8640, 24.9640, 33.4600), vec(1.2220, 1.7810, 1.0000), vec4(255, 191, 0, 255), ang(0.0000, 0.0000, 0.0000), "hq_rcube_thin", "phoenix_storms/concrete1", 0)
    #[   ]#    Holos[15, array] = array(vec(-40.6637, 0.1098, -109.6884), vec(0.5475, 4.4544, 0.5913), vec4(255, 191, 0, 255), ang(0.0000, 0.0000, 0.0000), "hq_rcube_thin", "phoenix_storms/concrete1", 0)
    #[   ]#    Holos[16, array] = array(vec(0.0000, -21.1703, 34.2603), vec(1.0000, 1.0000, 1.0000), vec4(0, 0, 0, 255), ang(0.0000, 0.0000, 0.0000), "models/sprops/rectangles/size_1_5/rect_6x36x3.mdl", "phoenix_storms/concrete1", 0)
    #[   ]#    Holos[17, array] = array(vec(19.1870, -33.8070, -111.1190), vec(1.0000, 1.0000, 1.0000), vec4(255, 0, 0, 255), ang(-5.5487, -180.0000, -180.0000), "hq_dome", "phoenix_storms/concrete1", 0)
    #[   ]#    Holos[18, array] = array(vec(-8.8513, 0.0000, -107.2608), vec(1.7203, 2.6773, 1.0000), vec4(255, 191, 0, 255), ang(0.0000, 0.0000, 0.0000), "models/sprops/rectangles/size_3_5/rect_30x42x3.mdl", "phoenix_storms/concrete1", 0)
    #[   ]#    Holos[19, array] = array(vec(19.1872, 33.8072, -111.1193), vec(1.0000, 1.0000, 1.0000), vec4(255, 0, 0, 255), ang(-5.5487, -180.0000, -180.0000), "hq_dome", "phoenix_storms/concrete1", 0)
    #[   ]#    Holos[20, array] = array(vec(-22.4070, -0.3780, 29.2980), vec(1.0000, 1.0000, 1.0000), vec4(255, 191, 0, 255), ang(0.0000, -89.1920, -31.7520), "models/sprops/rectangles/size_2_5/rect_18x30x3.mdl", "phoenix_storms/concrete1", 0)
    #[   ]#    Holos[21, array] = array(vec(0.0000, 0.0000, 22.4380), vec(1.0546, 1.0876, 1.0000), vec4(255, 0, 0, 255), ang(0.0000, 0.0000, 0.0000), "models/XQM/Rails/trackball_1.mdl", "phoenix_storms/concrete1", 0)
    #[   ]#    Holos[22, array] = array(vec(-0.0423, -0.1217, 24.9378), vec(1.9049, 1.0000, 1.9687), vec4(255, 191, 0, 255), ang(0.0875, 0.6405, 91.0966), "models/sprops/geometry/t_fdisc_30.mdl", "phoenix_storms/concrete1", 0)
    #[   ]#    Holos[23, array] = array(vec(0.0087, 39.7725, -65.5794), vec(1.2210, 1.3190, 0.7980), vec4(255, 191, 0, 255), ang(-88.4080, 89.9870, 180.0000), "models/sprops/rectangles/size_4_5/rect_42x66x3.mdl", "phoenix_storms/concrete1", 0)
    #[   ]#    Holos[24, array] = array(vec(29.9949, 0.0000, -37.3422), vec(2.4330, 2.2330, 1.0000), vec4(255, 191, 0, 255), ang(-88.5401, 0.0000, 0.0000), "models/sprops/rectangles/size_4/rect_36x60x3.mdl", "phoenix_storms/concrete1", 0)
    #[   ]#    Holos[25, array] = array(vec(-0.0040, 36.5180, 4.2630), vec(0.9030, 1.3186, 0.7981), vec4(255, 191, 9, 255), ang(-81.5570, -90.0130, 0.0000), "models/sprops/rectangles/size_4_5/rect_42x66x3.mdl", "phoenix_storms/concrete1", 0)
    #[   ]#    Holos[26, array] = array(vec(0.0000, 0.1536, 24.0965), vec(3.2324, 3.1204, 10.8967), vec4(0, 0, 0, 255), ang(0.0000, 0.0000, 0.0000), "hq_torus", "phoenix_storms/concrete1", 0)
    #[   ]#    Holos[27, array] = array(vec(20.1245, 0.6940, -107.6072), vec(1.3887, 4.4544, 1.0000), vec4(255, 191, 0, 255), ang(0.0000, 0.0000, 0.0000), "hq_rcube_thin", "phoenix_storms/concrete1", 0)
    #[   ]#    Holos[28, array] = array(vec(19.5534, 33.6622, -102.3991), vec(1.2584, 1.4707, 1.4360), vec4(255, 191, 0, 255), ang(0.0000, 0.0000, 0.0000), "hq_rcube", "phoenix_storms/concrete1", 0)
    #[   ]#    Holos[29, array] = array(vec(-21.2557, -24.9640, 30.3855), vec(1.2220, 1.7810, 1.0000), vec4(255, 191, 0, 255), ang(0.0000, 0.0000, 0.0000), "hq_rcube_thin", "phoenix_storms/concrete1", 0)
    
    ##########
    
    TotalHolos = Holos:count()
    if (0 > holoClipsAvailable()) {error("A holo has too many clips to spawn on this server! (Max is " + holoClipsAvailable() + ")")}
}


#You may place code here if it doesn't require all of the holograms to be spawned.


if (HolosSpawned)
{
    #Your code goes here if it needs all of the holograms to be spawned!
}
else
{
    while (LastHolo <= Holos:count() & holoCanCreate() & perf())
    {
        local Ar = Holos[LastHolo, array]
        addHolo(Ar[1, vector], Ar[2, vector], Ar[3, vector4], Ar[4, angle], Ar[5, string], Ar[6, string], Ar[7, number])
        LastHolo++
    }
    
    if (LastHolo > Holos:count())
    {
        Holos:clear()
        Clips:clear()
        HolosSpawned = 1
        E:setAlpha(0)
    }

    interval(1000)
}
