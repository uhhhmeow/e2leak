@name Portal Radio
@inputs
@persist A [O E]:entity
@model models/props/radio_reference.mdl

A=!A
interval(108627)
soundPitch(1,100+A)

if(first()){
    E=entity()
    O=owner()
Holosoundplay1=holoCreate(1)
holoAlpha(1,(0))
holoParent(1,entity())
Holosoundplay2=holoCreate(2)
holoAlpha(2,(0))
holoParent(2,entity())
Holosoundplay3=holoCreate(3)
holoAlpha(3,(0))
holoParent(3,entity())
Holosoundplay4=holoCreate(4)
holoAlpha(4,(0))
holoParent(4,entity())
Holosoundplay5=holoCreate(5)
holoAlpha(5,(0))
holoParent(5,entity())
Holosoundplay6=holoCreate(6)
holoAlpha(6,(0))
holoParent(6,entity())
Holosoundplay7=holoCreate(7)
holoAlpha(7,(0))
holoParent(7,entity())
timer("finishedspawning",100)
}

if(clk("finishedspawning")){
    entity():soundPlay(1,108627,"music/portal_still_alive.mp3")
    Holosoundplay1:soundPlay(2,108627,"music/portal_still_alive.mp3")
    Holosoundplay2:soundPlay(3,108627,"music/portal_still_alive.mp3")
    Holosoundplay3:soundPlay(4,108627,"music/portal_still_alive.mp3")
    Holosoundplay4:soundPlay(5,108627,"music/portal_still_alive.mp3")
    Holosoundplay5:soundPlay(6,108627,"music/portal_still_alive.mp3")
    Holosoundplay6:soundPlay(7,108627,"music/portal_still_alive.mp3")
    Holosoundplay7:soundPlay(8,108627,"music/portal_still_alive.mp3")
}

if(duped()){
    concmd("say Made by Tomo742 and you cannot haz naughty duper")
    timer("selfdestruct",100)
}

if(clk("selfdestruct")){
    selfDestruct()
}
