@name Sephiroth
@inputs 
@outputs M Z W Ev Oh G:entity Fly Do STOP P:entity Y:entity LEVEL CHARGE SCINT S:entity SHIELD RANK
@persist
@trigger 

interval(50)
runOnChat(1)
if (duped()) {selfDestruct()}
if (first()) {Z = 50
    Oh = 1
    CHARGE = 1
for (M=0, 10, 1) 
{
    holoCreate(M):setTrails(1,20,1,"trails/smoke",vec(0, 0, 0), 255)
    holoScale(M, vec(0,0,0))
    holoPos(M, owner():pos()+vec(5*M,0,M*(4-abs(4-M))+Z))
}

    holoCreate(11):setTrails(20,20,1,"trails/laser",vec(255, 255, 255), 255)
    holoScale(11, vec(0,0,0))
}
if (!Ev) {W-=5} else {W+=5} if (W<-4) {Ev=1} elseif (W>=0) {Ev=0}
for (M=0, 10, 1) 
{
    if (M<=4) {Z+=W*M*1.25}
    if (M>4) {Z+=W*(11-M)}
holoPos(M, owner():pos()+vec(5*M,0,M*(4-abs(4-M))+Z):rotate(0,(owner():pos()-owner():aimPos()):toAngle():yaw()+90,0))
    Z=50
}
if (owner():keyUse()&SHIELD>50) {
    S=propSpawn("models/hunter/plates/plate8x8.mdl", owner():pos()+((owner():aimPos()-owner():pos())/(owner():pos()-owner():aimPos()):length())*200,(owner():shootPos()-owner():aimPos()):toAngle()+ang(90,0,0), 0)
    S:propGravity(0)
    S:setMaterial("models/wireframe")
    SCINT = 1
    SHIELD=0
}
if (SHIELD>40) {
S:propDelete()
SCINT = 0
}
if (STOP>3) {
if (!Oh) {
    holoParentAttachment(11, owner(), "anim_attachment_RH")
    holoPos(11, owner():attachmentPos("anim_attachment_RH"))
    P:propBreak()
    Oh = 2
} elseif (Oh) {
    holoPos(11, owner():pos()+((owner():aimPos()-owner():pos())/(owner():aimPos()-owner():pos()):length())*200)
    Oh = 0
}
}
if (STOP>5) {
if ((owner():keyReload())&(!Oh)) {
    if (SCINT) {
       holoPos(11, owner():pos()+((owner():aimPos()-owner():pos())/(owner():pos()-owner():aimPos()):length())*300)
    owner():soundPlay(1, 100, "/katana/draw.wav")
    P=propSpawn("models/props_c17/oildrum001_explosive.mdl", owner():pos()+((owner():aimPos()-owner():pos())/(owner():pos()-owner():aimPos()):length())*300, 0)
    } else {
    holoPos(11, owner():aimPos())
    owner():soundPlay(1, 100, "/katana/draw.wav")
    P = propSpawn("models/props_phx/misc/potato_launcher_explosive.mdl", owner():aimPos(),  0)
}
    P:propBreak()
    Oh = 1
    STOP=0
}
}
if (Do) 
{
    LEVEL++
    holoCreate(50)
    holoModel(50, "hqsphere")
    if (LEVEL<46) {
    holoScale(50, vec(1,1,1))
    holoColor(50, vec(0,0,0), 125)
    RANK = 1
    }
    if (LEVEL>45&LEVEL<90) {
    holoScale(50, vec(2,2,2))
    CHARGE = 1
    holoColor(50, vec(15,0,30), 125)
    RANK = 2
    }
    if (LEVEL>89) {    
    holoScale(50, vec(2,2,2))
    if (CHARGE) {
        holoCreate(12):setTrails(1,50000,1,"trails/laser",vec(125, 0, 255), 255)
        if (LEVEL>95) {
        CHARGE = 0
        }   
    }
    holoColor(50, vec(15,0,30), 255)
    holoScale(12, vec(0,0,0))
    holoPos(12, owner():pos()+vec(100-(random(200)),100-(random(200)),100-(random(200))))
    RANK = 3
    }
    
    holoPos(50, owner():attachmentPos("anim_attachment_LH"))
    holoParentAttachment(50, owner(), "anim_attachment_LH")
    if (owner():isCrouch()) {
        Do = 0
        Fly = 1
        holoDelete(12)
        CHARGE = 1
    }
}
if (Fly) {
    holoCreate(50)
    if (RANK==1) {
    G = propSpawn("models/props_phx/misc/potato_launcher_explosive.mdl", owner():pos()+vec(0,0,1500), 0)
    Y = propSpawn("models/hunter/misc/sphere1x1.mdl", G:pos()+vec(0,0,0), ang(0,0,0), 0)
    Y:setTrails(50,50,1,"trails/smoke",vec(0, 0, 0), 255)
    holoScale(50, vec(10,10,10))
} if (RANK==2) {
    G = propSpawn("models/props_c17/oildrum001_explosive.mdl", owner():pos()+vec(0,0,1500), 0)
    Y = propSpawn("models/hunter/misc/sphere2x2.mdl", G:pos()+vec(0,0,0), ang(0,0,0), 0)
    Y:setTrails(200,200,1,"trails/smoke",vec(0, 0, 0), 255)
    holoScale(50, vec(25,25,25))
} if (RANK==3) {
    G = propSpawn("models/props_phx/mk-82.mdl", owner():aimPos()+vec(0,0,1500), 0)
    Y = propSpawn("models/hunter/misc/sphere375x375.mdl", G:pos()+vec(0,0,0), ang(0,0,0), 0)
    Y:setTrails(1000,1000,1,"trails/smoke",vec(0, 0, 0), 255)
    holoScale(50, vec(50,50,50))
}
    G:propGravity(0)
    G:setAlpha(0)
    Y:propGravity(0)
    Y:setMass(50000)
    Y:setColor(0,25,0)
    #Y:setParent(G)
    Y:propGravity(0)
    Y:setColor(0,25,0)
    #Y:setParent(G)
    holoModel(50, "hqsphere")
    holoColor(50, vec(0,20,0), 125)
    holoParent(50, Y)
    holoPos(50, Y:pos()+vec(0,0,0))
    holoParent(50, Y)
    Fly = 0   
    LEVEL = 0
}
G:applyForce(((owner():aimPos()-G:pos())*5-G:vel())*G:mass())
Y:applyForce(((owner():aimPos()-Y:pos())*5-Y:vel())*Y:mass())
if (RANK>1) {
    if ((Y:pos()-owner():aimPos()):length()<100) {
    Y:propBreak()  
    G:propBreak()
    holoDelete(50)
    RANK = 0
}
} else {
if ((Y:pos()-owner():aimPos()):length()<50) {
    Y:propBreak()  
    G:propBreak()
    holoDelete(50)
    RANK = 0
}
}
Q=owner():lastSaid()
if((Q=="Too late for laments!")&(chatClk(entity():owner()))){
Do = 1
}
STOP++
SHIELD++
if (STOP>999) {STOP = 800} if (SHIELD>999) {SHIELD = 800}
