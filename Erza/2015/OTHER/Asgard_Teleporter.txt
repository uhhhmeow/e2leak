@name Asgard Teleporter
@inputs 
@outputs Teleport OriginX OriginY OriginZ DestX DestY DestZ
@persist Teled SavedSaid:string Timer
@trigger all

if (Teled) {
    Teleport = 0
    Teled = 0
}

Owner = owner()

Spoke = lastSpoke()

Said = lastSaid()

if (Spoke == Owner & SavedSaid != Said) {
    Tele = Said:find("Goto", 0)
    Length = Said:length()
    if (Tele) {
        Pos = Owner:pos()
        TargetName = Said:sub(Tele + 5, Length)
        Target = findPlayerByName(TargetName)
        DestPos = Target:pos()
        OriginX = Pos:x()
        OriginY = Pos:y()
        OriginZ = Pos:z()
        DestX = DestPos:x()
        DestY = DestPos:y()
        DestZ = DestPos:z()
        if (Timer) {
            Teleport = 1
            Teled++
            Timer--
        }
    }
} elseif (SavedSaid != Said) {
    Tele = Said:find("Goto", 0)
    Length = Said:length()
    if (Tele) {
        Pos = Spoke:pos()
        TargetName = Said:sub(Tele + 5, Length)
        Target = findPlayerByName(TargetName)
        DestPos = Target:pos()
        OriginX = Pos:x()
        OriginY = Pos:y()
        OriginZ = Pos:z()
        DestX = DestPos:x()
        DestY = DestPos:y()
        DestZ = DestPos:z()
    }
    if (Said == "Grant" & Spoke == Owner) {
        Teleport = 1
        Teled++
    }
}

SavedSaid = Said

Timer++

interval(100)
