@name Busy
@persist [O]:entity
interval(100)

if ( first() )
{
    O = owner()    
    findIncludeClass("player")
    findExcludePlayer(O)
}

if ( findCanQuery() )
{
    findInSphere(O:pos(),150)
    Players = findToArray()
    
    foreach ( K, Ply:entity = Players )
    {
        PlyA = Ply:isAlive()
        PlyN = Ply:inNoclip()
        if ( Ply & PlyA )
        {
            Ply:applyPlayerForce((Ply:boxCenterW() - O:pos())*10)
            chatPrint(O, vec(255,0,0),"["+O:name()+"]", vec(250,90,0)," is busy !")
        }
        if ( PlyA & PlyN )
        {
            Ply:plyNoclip(0)
            Ply:applyPlayerForce((Ply:boxCenterW() - O:pos())*10)
            chatPrint(O, vec(255,0,0),"["+O:name()+"]", vec(250,90,0)," is busy !")
        }
    }
}
