@name Tornado
@inputs
@outputs
@persist Speed Spread Sin Cos

interval(10)

if (first()) {
    Speed = 250
    Spread = 3
    for(I = 1, 20) {
        holoCreate(I)
        holoModel(I,"hqtorus")
        holoPos(I,entity():toWorld(vec(0,0,I*12)))
        holoScaleUnits(I,vec(I,I,12))
    }
}

Sin = sin(curtime() * Speed) * Spread
Cos = cos(curtime() * Speed) * Spread
for(I = 1, 20) {
    holoPos(I,entity():toWorld(vec(Sin*I,Cos*I,I*12)))
}
