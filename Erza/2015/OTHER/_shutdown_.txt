@name TF2 DispenserBy Ammut
@outputs [E Me Turret]:entity [Amodels Models]:array Phase:string [Ang Angle]:angle Pos:vector Upgrade ModelIndex
@model models/weapons/w_models/w_toolbox.mdl


if(first() | dupefinished())
{


    runOnTick(1)
    runOnChat(1)






    
    E = entity()
    Me = owner()   
    Amodels = array("models/buildables/dispenser.mdl","models/buildables/dispenser_lvl2.mdl","models/buildables/dispenser_lvl3.mdl")
    Models = array("models/buildables/dispenser_blueprint.mdl","models/buildables/dispenser_light.mdl","models/buildables/dispenser_lvl2_light.mdl","models/buildables/dispenser_lvl3_light.mdl")
    ModelIndex = 1
    
    Turret = holoCreate(1)
    holoParent(1, E)
    holoModel(1,Models[ModelIndex,string])
    holoCreate(0)

   


    Phase = "Deploy"
}

if(Phase == "Deploy")
{
    Ang = (Me:eyeTrace():hitNormal():toAngle() + ang(90,0,0)):setYaw((Me:aimPos() - Me:pos() ):toAngle():yaw())
    holoPos(1, Me:aimPos())
    holoAng(1, Ang)
    
    if(Me:aimPos():distance(Me:pos()) >= 100) #| !inrange(Ang, ang(-45,-360,-45), ang(45,360,45)))
    {
        holoAnim(1, 1)
    }else
    {
        holoAnim(1, 0)
        if(Me:keyAttack1())
        {
            Phase = "Build 1"
            Angle = Ang
            Pos = Me:aimPos()
            
            #holoModel(1, E:model())
            holoModel(1,Amodels[ModelIndex,string])
            holoAnim(1,1)
            timer("build",10500)   
            holoEntity(1):soundPlay(50,50,"vo/engineer_autobuildingdispenser01.wav")
            ModelIndex++
        }
    }    
}elseif(Phase == "Active")
{
   Ang = (Pos + vec(0,0,25) - Me:aimPos()):toAngle() + ang(0,90,0)

 holoSetPose(1,"aim_pitch", clamp(Ang:pitch(), 0, 0)) 
    holoSetPose(1,"aim_yaw", sin(curtime()*20)*90)     
    #soundPlay:("weapons/sentry_scan.wav")

#entity():soundPlay(1,50,"weapons/sentry_scan.wav")
 
    Upgrade = Me:keyAttack2()
    if(changed(Upgrade) & Upgrade & ModelIndex <= 3)
    {
        holoModel(1, Amodels[ModelIndex,string])
        holoAnim(1, "upgrade")
        timer("build",2000)    
        ModelIndex++
    }
}



if(clk("build"))
{
    holoModel(1, Models[ModelIndex,string])
    Phase = "Active"
}

timer("clk",13000)

if(clk("clk")){entity():soundPlay(1,50,"")}


