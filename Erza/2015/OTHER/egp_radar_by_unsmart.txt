@name EGP radar by Unsmart
@inputs EGP:wirelink Prop:entity
@outputs Players:array PCount Spawned MaxDist Ang Mul
@persist 
@trigger 

#forever alone SP NPC finder for testing
#findByClass("NPC_*")
#Players=findToArray()

Players=players()
MaxDist=2000
    
    
PCount=Players:count()

interval(100)
if(first()|duped())
{
    
    
    EGP:egpClear()
    
    LightGreen=vec(50,255,0)#chnage to a better color
    DarkGreen=vec(0,255,0)  #chnage to a better color
    Background=vec(37,73,28)
    
    #base color of really dark green
    EGP:egpBox(1,vec2(256,256),vec2(256,256))
    EGP:egpColor(1,Background)
    
    EGP:egpCircle(2,vec2(256,256),vec2(255,255))
    EGP:egpColor(2,vec(13,66,13))
    
    EGP:egpCircle(3,vec2(256,256),vec2(256/2,256/2))
    EGP:egpFidelity(3,180)#:V
    
    EGP:egpCircle(4,vec2(256,256),vec2(256/2-3,256/2-3))
    EGP:egpColor(4,vec(13,66,13))
    
    EGP:egpLine(5,vec2(0,256),vec2(512,256))
    EGP:egpLine(6,vec2(256,0),vec2(256,512))
    
    EGP:egpText(7,round(MaxDist/2)+"",vec2(200,230))
    EGP:egpText(8,round(MaxDist  )+"",vec2(60,230))
    
    
    
    #spawn and edit positions
    for(I=1,PCount)
    {
        Ent=Players[I,entity]
        
        GlobalI=I+8
        
        PosDiff=(Ent:pos()-Prop:pos())
        TAng=Prop:bearing(Ent:pos())#PosDiff:toAngle():yaw()
        
        Ang=(TAng+180)%360
        
        Mul=(256/MaxDist)*clamp(PosDiff:length(),0,MaxDist)

        EGP:egpCircle(GlobalI,vec2(256-sin(Ang)*Mul,256+cos(Ang)*Mul),vec2(7,7))
        EGP:egpColor(GlobalI,vec(255,0,0))
    }
    
    
    
    
    Spawned=PCount
    
    holoCreate(1,Prop:pos()+Prop:forward()*50)
    holoParent(1,Prop)
    
   
    
}
else #update
{
    
    if(PCount>Spawned)
    {
        for(I=Spawned+1,PCount)
        {
            Ent=Players[I,entity]
        
            GlobalI=I+8
        
            PosDiff=(Ent:pos()-Prop:pos())
            TAng=Prop:bearing(Ent:pos())#PosDiff:toAngle():yaw()
        
            Ang=(TAng+180)%360
        
            Mul=(256/MaxDist)*clamp(PosDiff:length(),0,MaxDist)

            EGP:egpCircle(GlobalI,vec2(256-sin(Ang)*Mul,256+cos(Ang)*Mul),vec2(7,7))
            EGP:egpColor(GlobalI,vec(255,0,0))
            
            print("adding"+I)
        }
        Spawned=PCount
    }
    elseif(Spawned>PCount)
    {
        
        for(I=PCount+1,Spawned)
        {
            Ent=Players[I,entity]
            GlobalI=I+8
            
            EGP:egpRemove(GlobalI)
            
            print("removing"+I)
        }
        
        Spawned=PCount
    }
    
    
    for(I=1,PCount)
    {
        Ent=Players[I,entity]
        if(!Ent){continue}
        
        GlobalI=I+8
        
        PosDiff=(Ent:pos()-Prop:pos())
        TAng=Prop:bearing(Ent:pos())#PosDiff:toAngle():yaw()
        
        Ang=(TAng+180)%360
        
        Mul=(256/MaxDist)*clamp(PosDiff:length(),0,MaxDist)

        EGP:egpPos(GlobalI,vec2(256-sin(Ang)*Mul,256+cos(Ang)*Mul))
        #EGP:egpColor(GlobalI,vec(255,0,0))
    }
}





