@name Player Model Color Changer
@inputs 
@outputs 
@persist 
@trigger 
runOnChat(1)

if(first()){
#owner():setColor(randint(255,0),randint(255,0),randint(255,0))
#owner():setMaterial("models/debug/debugwhite")
setName( owner():name() + "'s Player Model Color Changer" )
}

OLS=owner():lastSaid():explode(" ")

if(OLS[1, string]=="purple"){
    owner():setColor(vec(119,0,255))
    hideChat(1)
    owner():setMaterial("models/debug/debugwhite")
    entity():setColor( 119,0,255 )
    entity():setMaterial("models/debug/debugwhite")
    #printColor(vec(119,0,255),"Your Player Models Color Has Been Changed To: Purple")
}


if(OLS[1, string]=="white"){
    owner():setColor(vec(255,255,255))
    owner():setMaterial("models/debug/debugwhite") 
   hideChat(1)
    entity():setColor( 255,255,255 )
    entity():setMaterial("models/debug/debugwhite")
    #printColor(vec(255,255,255),"Your Player Models Color Has Been Changed To: White")
}


if(OLS[1, string]=="red"){
    owner():setColor(vec(255,0,0))
    owner():setMaterial("models/debug/debugwhite") 
    hideChat(1)
    entity():setColor( 255,0,0 )
    entity():setMaterial("models/debug/debugwhite")
   # printColor(vec(255,0,0),"Your Player Models Color Has Been Changed To: Red")
}

if(OLS[1, string]=="black"){
    owner():setColor(vec(0,0,0))
    owner():setMaterial("models/debug/debugwhite") 
    hideChat(1)
    entity():setColor( 0,0,0 )
    entity():setMaterial("models/debug/debugwhite")
    #printColor(vec(0,0,0),"Your Player Models Color Has Been Changed To: Black")
}



if(OLS[1, string]=="orange"){
    owner():setColor(vec(255,191,0))
    owner():setMaterial("models/debug/debugwhite") 
    hideChat(1)
    entity():setColor( 255,191,0 )
    entity():setMaterial("models/debug/debugwhite")
    #printColor(vec(255,191,0),"Your Player Models Color Has Been Changed To: Orange")
}

if(OLS[1, string]=="blue"){
    owner():setColor(vec(29,0,255))
    owner():setMaterial("models/debug/debugwhite") 
    hideChat(1)
    entity():setColor( 29,0,255 )
    entity():setMaterial("models/debug/debugwhite")
    #printColor(vec(29,0,255),"Your Player Models Color Has Been Changed To: Blue")
}


if(OLS[1, string]=="green"){
    owner():setColor(vec(0,255,63))
    owner():setMaterial("models/debug/debugwhite") 
    hideChat(1)
    entity():setColor( 0,255,63 )
    entity():setMaterial("models/debug/debugwhite")
    #printColor(vec(0,255,63),"Your Player Models Color Has Been Changed To: Green")
}


if(OLS[1, string]=="yellow"){
    owner():setColor(vec(255,255,0))
    owner():setMaterial("models/debug/debugwhite") 
    hideChat(1)
    entity():setColor( 255,255,0 )
    entity():setMaterial("models/debug/debugwhite")
    #printColor(vec(255,255,0),"Your Player Models Color Has Been Changed To: Yellow")
    
}


if(OLS[1, string]=="random"){
    interval(100)
    owner():setColor(randint(255,0),randint(255,0),randint(255,0))
    owner():setMaterial("models/debug/debugwhite") 
    hideChat(1)
    entity():setColor(randint(255,0),randint(255,0),randint(255,0))
    entity():setMaterial("models/debug/debugwhite")
        
}


if(OLS[1, string]=="brown"){
    interval(100)
    owner():setColor(vec(127,95,0))
    owner():setMaterial("models/debug/debugwhite") 
    hideChat(1)
    entity():setColor(vec(127,95,0))
    entity():setMaterial("models/debug/debugwhite")
    if(OLS[1, string]=="invis"){
    owner():setColor(vec4(0,0,0,0))
    #owner():setMaterial("models/debug/debugwhite") 
    hideChat(1)
    entity():setColor(vec(255,0,0))
    entity():setMaterial("models/debug/debugwhite")
        
        
    }
}
if(OLS[1, string]=="reset"){
    owner():setColor(255,255,255,255)
    owner():setMaterial("BlaH")
    entity():setMaterial("BlaH")
    entity():setColor(255,255,255)
    hideChat(1)
    printColor(vec(255,255,255),"Your Player Models Color Has Been Reset")
}
