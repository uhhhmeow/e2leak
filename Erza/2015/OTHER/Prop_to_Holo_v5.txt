@name Prop to Holo
@inputs 
@outputs 
@persist [Data]:table [O Hull Turret Gun CEnt]:entity [Name Used]:array [NameStr NewStr FStr T1 T2]:string K2 I Q Stage
@trigger all

if(first()){
    runOnTick(1)
    runOnChat(1)
    runOnLast(1)
    
    function string toVec(V:vector){
        return "vec("+round(V:x(),2)+","+round(V:y(),2)+","+round(V:z(),2)+")"
    }
    function string toAng(A:angle){
        return "ang("+round(A:pitch(),2)+","+round(A:yaw(),2)+","+round(A:roll(),2)+")"
    }
    
    function number array:hasValue( Ent:entity ) {
        for(I=1,This:count()) {
            if (This[I,entity] == Ent) {
                return 1
            }
        }
        return 0
    } 
    
    I=0
    holoCreate(I)
    holoScale(I,vec(1.01))
    holoAlpha(I,0)
    holoColor(I,vec(255,0,0))
    
    #fileWrite(">e2shared/hologram_details/current_editing.txt","")
    
    Stage = 1
    printColor(vec(200,0,110),"Please use Mouse2 to select your hull base prop. When you have selected it, type \">select\".")
    printColor(vec(200,0,110),"After you have selected the base prop, use Mouse2 for selecting the props you wish to convert.")
    printColor(vec(200,0,110),"To continue to the next step, either use \">turret\" to begin selecting your turret props or \">gun\" to select gun props.")
    printColor(vec(200,0,110),"You may go directly to the gun, but you can not go back to turret.")
    printColor(vec(200,0,110),"If you have finished selecting your props, use \">save [NAME HERE]\" to save to an E2 in your \"e2shared/hologram_details\" folder.")
}
CEnt = owner():aimEntity()
if(chatClk(owner())&owner():lastSaid():explode(""):string(1)==">"){
    hideChat(1)
}
if(Stage == 1){
    if(changed(owner():keyAttack2())&owner():keyAttack2()&CEnt:isValid()){
        holoPos(0,CEnt:pos())
        holoAng(0,CEnt:angles())
        holoAlpha(0,255)
        holoModel(0,CEnt:model())
        Hull = CEnt
    }
    if(chatClk(owner())&owner():lastSaid():lower()==">select"){
        holoColor(0,vec(0,255,0))
        holoParent(0,Hull)
        Used:pushEntity(Hull)
        Stage = 2
    }
}
elseif(Stage == 2){
    if(changed(owner():keyAttack2())&owner():keyAttack2()&CEnt:isValid()&!Used:hasValue(owner():aimEntity())){
        Used:pushEntity(CEnt)
        Data:pushArray(array(Hull:toLocal(CEnt:pos()),Hull:toLocal(CEnt:angles()),CEnt:model(),CEnt:getMaterial(),CEnt:getColor(),0,0))
        I++
        holoCreate(I,CEnt:pos(),vec(1.01),CEnt:angles(),vec(255,255,255))
        holoModel(I,CEnt:model())
        holoMaterial(I,"models/debug/debugwhite")
    }
    if(chatClk(owner())&owner():lastSaid():lower()==">undo"){
        Data:pop()
        Used:pop()
        holoDelete(I)
        I--
    }
    elseif(chatClk(owner())&owner():lastSaid():explode(" "):string(1):lower()==">turret"){
        Stage = 3
        holoColor(0,vec(255,0,0))
        holoAlpha(0,0)
    }
    elseif(chatClk(owner())&owner():lastSaid():explode(" "):string(1):lower()==">gun"){
        Stage = 5
        holoColor(0,vec(255,0,0))
        holoAlpha(0,0)
    }
    elseif(chatClk(owner())&owner():lastSaid():explode(" "):string(1):lower()==">save"){
        Stage = 7
    }
}
if(Stage == 3){
    if(changed(owner():keyAttack2())&owner():keyAttack2()&CEnt:isValid()){
        holoPos(0,CEnt:pos())
        holoAng(0,CEnt:angles())
        holoAlpha(0,255)
        holoModel(0,CEnt:model())
        Turret = CEnt
    }
    if(chatClk(owner())&owner():lastSaid():lower()==">select"){
        holoColor(0,vec(0,255,0))
        holoParent(0,Turret)
        Used:pushEntity(Turret)
        Stage = 4
    }
}
elseif(Stage == 4){
    if(changed(owner():keyAttack2())&owner():keyAttack2()&CEnt:isValid()&!Used:hasValue(owner():aimEntity())){
        Used:pushEntity(CEnt)
        Data:pushArray(array(Turret:toLocal(CEnt:pos()),Turret:toLocal(CEnt:angles()),CEnt:model(),CEnt:getMaterial(),CEnt:getColor(),1,0))
        I++
        holoCreate(I,CEnt:pos(),vec(1.01),CEnt:angles(),vec(255,255,255))
        holoModel(I,CEnt:model())
        holoMaterial(I,"models/debug/debugwhite")
    }
    if(chatClk(owner())&owner():lastSaid():lower()==">undo"){
        Data:pop()
        Used:pop()
        holoDelete(I)
        I--
    }
    elseif(chatClk(owner())&owner():lastSaid():explode(" "):string(1):lower()==">gun"){
        Stage = 5
        holoColor(0,vec(255,0,0))
        holoAlpha(0,0)
    }
    elseif(chatClk(owner())&owner():lastSaid():explode(" "):string(1):lower()==">save"){
        Stage = 7
    }
}
if(Stage == 5){
    if(changed(owner():keyAttack2())&owner():keyAttack2()&CEnt:isValid()){
        holoPos(0,CEnt:pos())
        holoAng(0,CEnt:angles())
        holoAlpha(0,255)
        holoModel(0,CEnt:model())
        Gun = CEnt
    }
    if(chatClk(owner())&owner():lastSaid():lower()==">select"){
        holoColor(0,vec(0,255,0))
        holoParent(0,Gun)
        Used:pushEntity(Gun)
        Stage = 6
    }
}
elseif(Stage == 6){
    if(changed(owner():keyAttack2())&owner():keyAttack2()&CEnt:isValid()&!Used:hasValue(owner():aimEntity())){
        Used:pushEntity(CEnt)
        Data:pushArray(array(Gun:toLocal(CEnt:pos()),Gun:toLocal(CEnt:angles()),CEnt:model(),CEnt:getMaterial(),CEnt:getColor(),1,1))
        I++
        holoCreate(I,CEnt:pos(),vec(1.01),CEnt:angles(),vec(255,255,255))
        holoModel(I,CEnt:model())
        holoMaterial(I,"models/debug/debugwhite")
    }
    if(chatClk(owner())&owner():lastSaid():lower()==">undo"){
        Data:pop()
        Used:pop()
        holoDelete(I)
        I--
    }
    elseif(chatClk(owner())&owner():lastSaid():explode(" "):string(1):lower()==">save"){
        Stage = 7
    }
}
elseif(changed(Stage)&Stage == 7){
    Name = owner():lastSaid():explode(" ")
    Name:shift()
    NameStr = Name[1,string]
    for(I=2,Name:count()){
        NameStr = NameStr +"_"+ Name[I,string]
    }
    print("\nBeginning to save to file: "+NameStr)
    T1 = "@name "+NameStr+"
@inputs [Hull Turret Gun]:entity
@persist Queue:table I
if(first()){
    runOnTick(1)"
    T2=
"\n}
while(perf()&I<Queue:count()&holoCanCreate()){
    if(I<Queue:count()){
        I++
        holoCreate(I,Queue[I,array][1,vector],vec(1),Queue[I,array][2,angle],Queue[I,array][5,vector])
        holoModel(I,Queue[I,array][3,string])
        holoMaterial(I,Queue[I,array][4,string])
        holoParent(I,Queue[I,array][6,number] ? Queue[I,array][7,number] ? Gun : Turret : Hull)
    }
    if(I==Queue:count()){
        runOnTick(0)
    }
}
if(dupefinished()){
    reset()
}
"
    Stage = 8
}
elseif(Stage == 8){
    if(Q<Data:count()){
        Q++
        Pos=Data[Q,array][1,vector]
        Ang=Data[Q,array][2,angle]
        NewStr="\n    Queue["+Q+",array]=array("+(Data[Q,array][6,number] ? Data[Q,array][7,number] ? "Gun" : "Turret" : "Hull")+":toWorld("+toVec(Pos)+"),"+(Data[Q,array][6,number] ? Data[Q,array][7,number] ? "Gun" : "Turret" : "Hull")+":toWorld("+toAng(Ang)+"),\""+Data[Q,array][3,string]+"\",\""+Data[Q,array][4,string]+"\","+toVec(Data[Q,array][5,vector])+","+Data[Q,array][6,number]+","+Data[Q,array][7,number]+")"
        FStr+=NewStr
        if(Q>=Data:count()){
            FStr=T1+FStr+T2
            print("Dupe \""+NameStr+"\": "+Data:count()+" prop(s)")
            if(!fileCanWrite()){error("nobo")}
            #fileWrite(">dupes/"+FName:replace(" ","_")+".txt",FStr)
            fileWrite(">e2shared/hologram_details/"+NameStr+".txt",FStr)
            holoDeleteAll()
            Stage=9
            #print(Q)
        }
    }
}
if(Stage == 9){
    runOnTick(0)
}
if(last()){
    holoDeleteAll()
}

#print(_HUD_PRINTCENTER,toString(Stage))
