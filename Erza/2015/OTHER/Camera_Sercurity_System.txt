@name Camera Sercurity System
@inputs [Cam1 Cam2 Cam3 Cam4 Cam5]:wirelink
@outputs 
@persist 
@trigger 
runOnTick(1)
runOnChat(1)
if(first()){
    print("Made by Ryno630")
    print("Type -cam1,-cam2,-cam3,-cam4,-cam5; to active cameras 1-5, and -off to turn them off")
    print("All you need is a wire Cam controler, that has an external camera. Place the camera where u want.")
}

if (chatClk(owner())) # chat commands
{
    Str = owner():lastSaid():explode(" ")
 #turning on camera
    if(Str[1,string]=="-cam1"){
        hideChat(1)
        print("viewing Camera #1")
        Cam1["Activated",number] = 1
           
    }
    if(Str[1,string]=="-cam2"){
        hideChat(1)
        print("viewing Camera #2")
        Cam2["Activated",number] = 1
        
    }
    if(Str[1,string]=="-cam3"){
        hideChat(1)
        print("viewing Camera #3")
        Cam3["Activated",number] = 1
        
    }
    if(Str[1,string]=="-cam4"){
        hideChat(1)
        print("viewing Camera #4")
        Cam4["Activated",number] = 1
        
    }
    if(Str[1,string]=="-cam5"){
        hideChat(1)
        print("viewing Camera #5")
        Cam5["Activated",number] = 1
        
    }
# turning off camera
    if(Str[1,string]=="-off"){
        hideChat(1)
        print("Turning off camera")
        Cam1["Activated",number] = 0
        Cam2["Activated",number] = 0
        Cam3["Activated",number] = 0
        Cam4["Activated",number] = 0
        Cam5["Activated",number] = 0
    }
}
