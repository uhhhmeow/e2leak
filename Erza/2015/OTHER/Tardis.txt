@name Tardis
@persist Timer
interval(10)
Me=owner()
Chip=entity()
Timer=Timer+1*1
T=Timer

if(first()) {holoCreate(1)}

holoModel(1,"cube")
holoPos(1,Chip:pos()+vec(0,0,25))
holoAng(1,ang(0,0,0))
holoColor(1,vec(0,20,155))
holoScale(1, vec(5.9,5.9,15.9))

holoCreate(2)
holoModel(2,"cube")
holoPos(2,Chip:pos()+vec(23,0,25))
holoAng(2,ang(0,0,0))
holoColor(2,vec(0,0,0))
holoScale(2, vec(7,0,15.9))
