@name Portal Turret Animated
@inputs 
@outputs 
@persist Magicnumber Zinit PickedUp
@trigger 
runOnChat(1)
Undeploysound = "npc/turret_floor/turret_retire_" + randint(1,7):toString() +".wav"
Pickupsound = "npc/turret_floor/turret_pickup_"+ randint(1,10):toString() + ".wav"
Deploysound = "npc/turret_floor/turret_deploy_"+ randint(1,6):toString()+".wav"
Tippedsound = "npc/turret_floor/turret_tipped_"+ randint(1,6):toString()+".wav"
if (first()){
Zinit = entity():pos():z()
hint("Welcome to E2 Portal Turret!",5)
hint("!undeploy - makes the turret undeploy",5)
hint("!redeploy - makes the turret start working again after undeploying or falling over",5)
holoCreate(1)
#########MODEL#############
holoModel(1,"models/player/alyx.mdl")
###########################
holoParent(1,entity())
holoAnim(1,"deploy")
entity():soundPlay(1,soundDuration(Deploysound),Deploysound)
timer("changeanim",holoAnimLength(1)*1000)
PickedUp =0
Magicnumber=0
}

if (clk("changeanim")&Magicnumber!=2){
    
    if (Magicnumber!=1){
        holoAnim(1,"aim_leftright",1,(1/16)/2)
        timer("changeanim",holoAnimLength(1)*10000)
         Magicnumber=1
    }
    if (Magicnumber==1){
        #hint("...",1)
        holoAnim(1,"aim_leftright",0,(1/16)/2)
        timer("changeanim",holoAnimLength(1)*36000)
    }
}
if(clk("changeanim") & Magicnumber==2){
    holoAnim(1,"idle")
    
}
if(chatClk() & (owner():lastSaid()=="!undeploy")& Magicnumber!=2){
    holoAnim(1,"undeploy")
    entity():soundPlay(1,soundDuration(Undeploysound),Undeploysound)
    timer("changeanim",holoAnimLength(1)*1000)
    Magicnumber=2
}
if(chatClk() & (owner():lastSaid()=="!redeploy") & Magicnumber==2){
entity():soundPlay(1,soundDuration(Deploysound),Deploysound)
holoAnim(1,"deploy")
timer("changeanim",holoAnimLength(1)*1000)
Magicnumber=0
}
if (entity():pos():z()>Zinit+20){
    PickedUp=1
    entity():soundPlay(1,soundDuration(Pickupsound),Pickupsound)
    
}
Eang = entity():angles()
#hint(entity():angles():pitch():toString(),1)
if ((Magicnumber!=2)&( PickedUp & abs(Eang:pitch()) > 35 | abs(Eang:roll()) > 35)){
    
    #hint(entity():angles():roll():toString(),2)
    entity():soundPlay(1,soundDuration(Tippedsound),Tippedsound)
    holoAnim(1,"undeploy")
    timer("changeanim",holoAnimLength(1)*1000)
    Magicnumber=2
}
