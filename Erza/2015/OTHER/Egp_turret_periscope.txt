@name Egp turret periscope
@inputs Active EGP:wirelink Fire Gunready
@persist 

if(Active==1){
EGP:egpBox(1, vec2(683,384), vec2(1,120))
EGP:egpColor(1,vec(255,0,0)) 

EGP:egpBox(2, vec2(683,384), vec2(1,100))
EGP:egpAngle(1,90)
EGP:egpColor(2,vec(255,0,0)) 


EGP:egpBox(3,vec2(683,0),vec2(256,1400))
EGP:egpColor(3,vec(36,36,36))
EGP:egpMaterial(3,"gui/center_gradient")
EGP:egpAngle(3,90)

EGP:egpBox(4,vec2(0,100),vec2(256,1400))
EGP:egpColor(4,vec(36,36,36))
EGP:egpMaterial(4,"gui/center_gradient")
EGP:egpAngle(4,0)

EGP:egpBox(5,vec2(1366,100),vec2(256,1400))
EGP:egpColor(5,vec(36,36,36))
EGP:egpMaterial(5,"gui/center_gradient")
EGP:egpAngle(5,0)

EGP:egpBox(6,vec2(683,768),vec2(256,1400))
EGP:egpColor(6,vec(36,36,36))
EGP:egpMaterial(6,"gui/center_gradient")
EGP:egpAngle(6,90)

EGP:egpBox(7,vec2(683,460),vec2(10,15))
EGP:egpColor(7,vec(150,0,0))

if(Fire==1&Gunready==1) {EGP:egpBox(7,vec2(683,460),vec2(150,15)) }
}
else{ EGP:egpClear()}
