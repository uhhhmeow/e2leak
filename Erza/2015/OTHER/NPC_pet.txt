@name NPC pet
@inputs Find
@outputs 
@persist NPC:entity
Play = players()
if(Find&~Find)
{
   findByClass("npc_*")
   NPC = findClosest(owner():pos())
   NPC:npcRelationship("player","hate",900)
   NPC:npcRelationship(owner(),"like",999)
}

if(NPC)
{

   interval(1000)

   OwnerPos = owner():pos()
   DPos = OwnerPos - NPC:pos()

   WalkPos = OwnerPos - DPos:normalized()*100

   if(DPos:length()>10)
   {
      NPC:npcGoRun( WalkPos )
   # NPC:npcGiveWeapon("weapon_357")
    NPC:npcShoot() Play
   }
   else
   {
      NPC:npcStop()
   }

}
