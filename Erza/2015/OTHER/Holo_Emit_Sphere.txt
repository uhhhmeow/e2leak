@name Holo Emit Sphere
@inputs 
@outputs 
@persist 
@trigger 

NCircleExpress IMul Timer OOX OY OZ X = sin(Mul) 20 Y = cos(Mul) 20 Z = cos(Timer) OZ = sin(Timer) 20 OX = Z X OY = Z * Y[]
