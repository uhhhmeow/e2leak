@name EGP Test
@inputs EGP:wirelink User:entity
@outputs A1
@persist [ID]:array
@trigger 
runOnChat(1)
A1 = 255
ID[1, string] = "STEAM_0:0:56021974"

findByClass("player")
User = findClosest(EGP:entity():massCenter())

if(first() | duped() | dupefinished()) {
    EGP:egpClear()
}
    EGP:egpBox(1, vec2(256,256), vec2(512,512))
    EGP:egpColor(1, vec(255))
    EGP:egpAlpha(1, A1)
    EGP:egpText(2, "Ruben's Bank System", vec2(256, 10))
    EGP:egpAlign(2, 1)
    EGP:egpSize(2, 40)
    EGP:egpColor(2, vec())
    EGP:egpAlpha(2, A1)
    EGP:egpText(3, "Log in using !login", vec2(256,256))
    EGP:egpAlign(3, 1)
    EGP:egpColor(3, vec())
    EGP:egpSize(3, 40)
    EGP:egpAlpha(3, A1)
    EGP:egpText(4, "* User has to be on the whitelist *", vec2(256, 306))
    EGP:egpAlign(4, 1)
    EGP:egpColor(4, vec())
    EGP:egpSize(4, 25)
    EGP:egpAlpha(4, A1)

for(I = 1,10) {
    if(User:lastSaid() =="!login" & User:steamID() == ID[I, string] & chatClk(owner())) {
        hideChat(1)
        A1 = 0
        hint("User "+User:name()+" logged in", 5)   
        EGP:egpClear()
        break
    }
}
