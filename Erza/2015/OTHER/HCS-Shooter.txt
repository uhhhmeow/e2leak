@name HCS-Shooter
@inputs 
@outputs 
@persist ID:string Range
@trigger 

if (first() | duped()) {
    gShare(1)
    
    Range = 1000
    ID = entity():id():toString()
    
    holoCreate(1)
    holoScaleUnits(1,vec(10,10,10))
    holoModel(1,"hqsphere")
    holoMaterial(1,"models/effects/splode_sheet")
    holoAlpha(1,0)
}

###### getting the variables I need to use ########
if (owner():keyAttack1()) {
    Mode = 1
    Time = curtime()
    Damage = 25
    Origin = owner():shootPos()
    OriginDirVec = owner():eye()
    
    DamageArray = array(Mode,Time,Damage,Origin,OriginDirVec,Range,ID)
    
    gSetStr("Damage",glonEncode(DamageArray))
}

if (owner():keyAttack2()) {
    Mode = 2
    Time = curtime()
    ExplosionDamage = 100
    Radius = 100
    Falloff = 0.1
    Pos = owner():aimPos()
    
    DamageArray = array(Mode,Time,ExplosionDamage,Radius,Falloff,Pos,ID)
    
    gSetStr("Damage",glonEncode(DamageArray))
}

if (owner():keyUse()) {
    Mode = 3
    Time = curtime()
    Damage = 10
    ExplosionDamage = 25
    Radius = 100
    Falloff = 0.01
    Origin = owner():shootPos()
    OriginDirVec = owner():eye()
    
    DamageArray = array(Mode,Time,Damage,ExplosionDamage,Radius,Falloff,Origin,OriginDirVec,Range,ID)
    
    gSetStr("Damage",glonEncode(DamageArray))
}

#Effect
HitPos = gGetVec(ID+"Hit")
if (HitPos) {
    #fx("explosion",entity():pos(),10)
    holoAlpha(1,100)
    holoPos(1,HitPos)
    gSetVec(ID+"Hit",vec(0,0,0))
} else {
    #holoAlpha(1,0)
}

interval(1000)
