@name Spectres Platforms

#Vars
@persist SDist ZDist PlatformModel:string DuckDist

#Startup
@persist FindArray:array Blocks:array B B2

#Enable
@persist Enable Etrigger

#PlayerVars
@persist Eye:angle LookBar P Y PlayerMove

#BlockMatrix
@persist BVA:array SupPos:vector

#ExtrapolateStep
@persist I2 Bear:vector ExtrapVec:array

#Movement
@persist Run Vec:vector B1Vec:vector B2Vec:vector SBlock:entity MBlock:entity Switch Delay Avoid

#===================


#Vars
SDist = 100
ZDist = 10
DuckDist = 50
PlatformModel = "models/hunter/tubes/circle2x2.mdl"


#General
runOnTick(1)

#Startup
if(first()) {
    
    Switch = 1 
    
    findByModel(PlatformModel)
    FindArray = findToArray()

    #Filtering Results
    while(B < FindArray:count()) {

        B++

        if(FindArray:entity(B):owner() == owner()) {               

            B2++
            Blocks[B2,entity] = FindArray[B,entity]
            Blocks[B2,entity]:setMass(5000)

        }   

    }
    
    SBlock = Blocks[1,entity]
    MBlock = Blocks[2,entity] 

    if(Blocks:count() < 2) {
        hint("Insufficient Plate Count: Missing ("+(2-Blocks:count())+") "+PlatformModel,5)
        selfDestruct()
    } else {
        hint("Hold 'R' and press 'Attack 2' to enable",8)
        hint("Remeber to No-Colide the platforms",7)
    }
    

}

#Enable
if(owner():keyAttack2() & owner():keyReload()) {Etrigger = 1} else {Etrigger = 0}

if(Etrigger & $Etrigger) {
    
    Enable++
    
    if(Enable == 1) {

        owner():soundPlay(1,3,"vehicles/junker/radar_ping_friendly1.wav")
        soundPitch(1,150)
        Blocks[1,entity]:setColor(255,0,0)
        Blocks[2,entity]:setColor(255,0,0)

    }
    
    if(Enable > 1) {

        Enable = 0
        owner():soundPlay(2,3,"vehicles/junker/radar_ping_friendly1.wav")
        soundPitch(2,100)
        Blocks[1,entity]:setColor(255,255,255)
        Blocks[2,entity]:setColor(255,255,255)

    }    

}

#PlayerVars

Eye = owner():eyeAngles()
P = owner():eyeAngles():pitch()
Y = owner():eyeAngles():yaw()
PlayerMove = owner():pos():x()

if(P < -10) {LookBar = 1} elseif (P > 10) {LookBar = -1} else {LookBar = 0}

#BlockVectorArray

if(Switch) {

    BVA[1,vector] = owner():pos() + vec(SDist,-SDist,0)
    BVA[2,vector] = owner():pos() + vec(SDist,0,0)
    BVA[3,vector] = owner():pos() + vec(SDist,SDist,0)
    BVA[4,vector] = owner():pos() + vec(0,-SDist,0)
    BVA[5,vector] = owner():pos()
    BVA[6,vector] = owner():pos() + vec(0,SDist,0)
    BVA[7,vector] = owner():pos() + vec(-SDist,-SDist,0)
    BVA[8,vector] = owner():pos() + vec(-SDist,0,0)
    BVA[9,vector] = owner():pos() + vec(-SDist,SDist,0)

    SupPos = owner():pos() - vec(0,0,6)

}

#ExtrapolateStep

if($Y) {

    Bear = owner():shootPos()+(vec(SDist,0,0):rotate(ang(0,Y,  0)))
    
    while(I2 < 9) {
        
        I2++ 
        ExtrapVec[I2,number] = Bear:distance(BVA[I2,vector])
        ExtrapVec[5,number] = 1000
        
    }
    
} else {I2 = 0}

#Movement
    
    #Block must be 6 units below player to prevent pushing

if(Enable) {
    
    if(owner():pos():distance(SBlock:massCenter()) > owner():pos():distance(MBlock:massCenter()) & !Switch & !Delay) {
    
        Switch = 1

    }
            
    if (Switch) {

        timer("switchoff",20)
    }
        
    if(clk("switchoff")) {

        Switch = 0

    }
    
    while(Enable) {
        
        Run++
    
        TarQ = quat(ang())
        CurQ = quat(Blocks[Run,entity]) 
        Q = TarQ/CurQ
        V = Blocks[Run,entity]:toLocal(rotationVector(Q)+Blocks[Run,entity]:pos())
        Blocks[Run,entity]:applyTorque((1000*V - 12*Blocks[Run,entity]:angVelVector())*Blocks[Run,entity]:inertia()) 
    
            if(Run > 3) {

            Run = 0
            break
            
            }
    
    }
    
    
    B1Vec = SupPos - SBlock:massCenter()
    SBlock:applyForce((B1Vec*10 - SBlock:vel())*SBlock:mass())
    
    B2Vec = BVA[ExtrapVec:minIndex(),vector]+vec(0,0,LookBar*ZDist)-vec(0,0,Avoid*DuckDist) - MBlock:massCenter()
    MBlock:applyForce((B2Vec*10 - MBlock:vel())*MBlock:mass())
    
        
    if(Switch & !Delay & SBlock == Blocks[1,entity]) {
            
        Delay = 1
        Avoid = 1   
        
        SBlock = Blocks[2,entity]
        MBlock = Blocks[1,entity]
    
    } elseif (Switch & !Delay & SBlock == Blocks[2,entity]) {
        
        Delay = 1
        Avoid = 1
        
        SBlock = Blocks[1,entity]
        MBlock = Blocks[2,entity]
        
    }

    if(Delay) {
        
        timer("Delayoff",50)
        
        if(clk("Delayoff")) {

            Delay = 0

        }

    } 
    
    if(Avoid) {
        
        timer("Avoid",200)
        
        if(clk("Avoid")) {

            Avoid = 0

        }

    } 

}

#SpectreCat
