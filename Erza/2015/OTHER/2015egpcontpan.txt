@name constructor_interface
@inputs EGP:wirelink User:entity
@persist TempFrame:string EGPMode X Y  
 
##MISSILE TARGETING
@inputs TarPos:vector R:entity Target:entity Distance  
@outputs TargetMode TargetClass:string Fire TargetClear TargetFind
@persist TargetClassArray:array TargetClassIndex
 
##NAVIGATION
@inputs WayPointIndex Mode:string
@outputs TempIndex ModeT:string Start Clear
 
##CONSTRUCTION
@outputs BuildStart BuildClear
@inputs Building
 
##TURRETS TARGETING
@outputs TargetPlayers:array TurretClass:string TurretOn
@persist TargetPlayerIndex TurretClassIndex Players:array
 
if(first()){
    ModeT = "bunker"
    EGP:egpClear()
 
    TargetMode = 1
    TargetClassIndex = 1
 
    TargetClassArray[1,string] = "sent_tardis"
    TargetClassArray[2,string] = "prop_vehicle*"
    TargetClassArray[3,string] = "player"
    TargetClassArray[4,string] = "wac*"
   
    TargetClass = TargetClassArray[TargetClassIndex,string]
   
    timer("frame1",200)
    timer("frame2",400)
    timer("frame3",600)
    timer("frame4",800)
    timer("main",1000)
    timer("logo",1200)
   
    X = 512
    Y = 512
}
 
if(dupefinished()){
    reset()
}
 
Color1 = vec4(0,0,150,15)
Color2 = vec4(0,0,100,55)
 
if(clk("frame1")){
    local Color1 = vec4(0,0,100,255)
    local Color2 = vec4(0,50,0,255)
   
    EGP:egpBox(1, vec2(X*0.4,Y*0.15),vec2(X*0.6,Y*0.1))
    EGP:egpBox(2, vec2(X*0.4,Y*0.27),vec2(X*0.6,Y*0.1))
    EGP:egpBox(3, vec2(X*0.4,Y*0.39),vec2(X*0.6,Y*0.1))
    EGP:egpBox(4, vec2(X*0.4,Y*0.51),vec2(X*0.6,Y*0.1))
   
    EGP:egpText(5, "> BUNKER DOWN", vec2(0,0))
    EGP:egpText(6, "> FOLLOW", vec2(0,0))
    EGP:egpText(7, "> ROUTE", vec2(0,0))
    EGP:egpText(8, "> PATROL", vec2(0,0))
   
    EGP:egpBox(9, vec2(X*0.24,Y*0.72),vec2(X*0.29,Y*0.26)), EGP:egpColor(9, Color1)
    EGP:egpBox(10, vec2(X*0.56,Y*0.72),vec2(X*0.29,Y*0.26)), EGP:egpColor(10, Color1)
    EGP:egpBoxOutline(11, vec2(X*0.56,Y*0.72),vec2(X*0.29,Y*0.26)), EGP:egpSize(11, 3)
   
    EGP:egpBox(12, vec2(X*0.24,Y*0.66),vec2(X*0.25,Y*0.07)), EGP:egpColor(12, Color2)
    EGP:egpBox(13, vec2(X*0.24,Y*0.78),vec2(X*0.25,Y*0.07)), EGP:egpColor(13, Color2)
    EGP:egpText(14, "[START]", vec2(-30,-10)), EGP:egpParent(14, 12)
    EGP:egpText(15, "[CLEAR]", vec2(-30,-10)), EGP:egpParent(15, 13)  
    EGP:egpText(16, " Waypoint count: ", -EGP:egpSize(11)/2), EGP:egpParent(16,11)
    EGP:egpText(17, (WayPointIndex-1):toString(), vec2(-15,-35)), EGP:egpParent(17,11), EGP:egpFont(17, "Arial", 70)
   
    for(I=1,4){
        EGP:egpColor(I, Color1)
        EGP:egpParent(I+4,I)
        EGP:egpFont(I+4,"ChatFont",25)
        EGP:egpPos(I+4,vec2(-X*0.28,-12))
    }
   
    TempFrame = "frame1"
    timer("saveFrame",10)
}
 
if(clk("frame2")){
    local Color1 = vec4(0,0,100,255)
    local Color2 = vec4(55,55,55,255)
   
    EGP:egpBox(1,vec2(X*0.4,Y*0.5),vec2(X*0.65,Y*0.85)), EGP:egpColor(1,Color2)
    EGP:egpBox(2, vec2(X*0.4,Y*0.15),vec2(X*0.6,Y*0.1)), EGP:egpColor(2,Color1)
    EGP:egpBox(3, vec2(X*0.4,Y*0.35),vec2(X*0.4,Y*0.1)), EGP:egpColor(3,Color1)
    EGP:egpBox(4, vec2(X*0.4,Y*0.47),vec2(X*0.4,Y*0.1)), EGP:egpColor(4,Color1)
    EGP:egpCircleOutline(5, EGP:egpPos(3) + vec2(-X*0.25,0), vec2(X*0.05,Y*0.05)), EGP:egpAngle(5, 180)
    EGP:egpCircleOutline(6, EGP:egpPos(3) + vec2(X*0.25,0), vec2(X*0.05,Y*0.05)), EGP:egpAngle(6, 0)
    EGP:egpCircleOutline(7, EGP:egpPos(4) + vec2(-X*0.25,0), vec2(X*0.05,Y*0.05)), EGP:egpAngle(7, 180)
    EGP:egpCircleOutline(8, EGP:egpPos(4) + vec2(X*0.25,0), vec2(X*0.05,Y*0.05)), EGP:egpAngle(8, 0)
   
    for(I=1,4){
        EGP:egpFidelity(I+4, 3)
        EGP:egpSize(I+4, 5)
        EGP:egpColor(I+4, Color1)
    }
   
    EGP:egpBox(9, vec2(X*0.25,Y*0.725), vec2(X*0.3, Y*0.35)),  EGP:egpColor(9,Color1)
    EGP:egpBox(10, vec2(X*0.56,Y*0.6), vec2(X*0.28, Y*0.1)),   EGP:egpColor(10,vec4(155,0,0,255))
    EGP:egpBox(11, vec2(X*0.56,Y*0.725), vec2(X*0.28, Y*0.1)), EGP:egpColor(11,Color1)
    EGP:egpBox(12, vec2(X*0.56,Y*0.85), vec2(X*0.28, Y*0.1)),  EGP:egpColor(12,vec4(0,155,0,255))
   
    EGP:egpText(13, "<MISSILE TARGETING INTERFACE>", -EGP:egpSize(2)/2 + vec2(15,15)), EGP:egpParent(13,2)
    EGP:egpText(14, "TARGET MODE", -EGP:egpSize(3)/2 + vec2(10,15)), EGP:egpParent(14,3)
    EGP:egpText(15, "TARGET CLASS", -EGP:egpSize(4)/2 + vec2(10,15)), EGP:egpParent(15,4)
    EGP:egpText(16, "TARGET ENTITY", -EGP:egpSize(9)/2 + vec2(0,0)), EGP:egpParent(16,9)
    EGP:egpText(17, "TARGET OWNER", -EGP:egpSize(9)/2 + vec2(0,20)), EGP:egpParent(17,9)
    EGP:egpText(18, "TARGET POS", -EGP:egpSize(9)/2 + vec2(0,40)), EGP:egpParent(18,9)
    EGP:egpText(19, "TARGET DIS", -EGP:egpSize(9)/2 + vec2(0,60)), EGP:egpParent(19,9)
    EGP:egpText(20, "[ FIRE ]", -EGP:egpSize(10)/2 + vec2(20,15)), EGP:egpParent(20,10)
    EGP:egpText(21, "[ TARGET ]", -EGP:egpSize(11)/2 + vec2(20,15)), EGP:egpParent(21,11)
    EGP:egpText(22, "[ CLEAR ]", -EGP:egpSize(12)/2 + vec2(20,15)), EGP:egpParent(22,12)
   
    EGP:egpLine(23, vec2(X*0.1,Y*0.25), vec2(X*0.7, Y*0.25)), EGP:egpSize(23,6), EGP:egpColor(23,Color1)
   
    TempFrame = "frame2"
    timer("saveFrame",10)
}
 
if(clk("frame3")){
    local Color1 = vec4(0,0,100,255)
    local Color2 = vec4(55,55,55,255)
   
    EGP:egpBox(1, vec2(X*0.4,Y*0.5),vec2(X*0.65,Y*0.85)), EGP:egpColor(1,Color2)
    EGP:egpBox(2, vec2(X*0.4,Y*0.15),vec2(X*0.6,Y*0.1)), EGP:egpColor(2,Color1)
    EGP:egpBox(3, vec2(X*0.4,Y*0.35),vec2(X*0.6,Y*0.1)), EGP:egpColor(3,Color1)
    EGP:egpBox(4, vec2(X*0.4,Y*0.47),vec2(X*0.4,Y*0.1)), EGP:egpColor(4,vec4(0,155,0,255))
    EGP:egpBox(5, vec2(X*0.4,Y*0.59),vec2(X*0.4,Y*0.1)), EGP:egpColor(5,vec4(155,0,0,255))
   
    EGP:egpBox(6, vec2(X*0.4,Y*0.775), vec2(X*0.6, Y*0.22)),  EGP:egpColor(6,Color1)
   
    EGP:egpText(7, "<CONSTRUCTION INTERFACE>", -EGP:egpSize(2)/2 + vec2(20,15)), EGP:egpParent(7,2)
    EGP:egpText(8, "> BUILDMODE: ", -EGP:egpSize(3)/2 + vec2(10,15)), EGP:egpParent(8,3)
    EGP:egpText(9, "[ BUILD ]", -EGP:egpSize(4)/2 + vec2(65,15)), EGP:egpParent(9,4)
    EGP:egpText(10, "[ CLEAR ]", -EGP:egpSize(5)/2 + vec2(65,15)), EGP:egpParent(10,5)
   
    EGP:egpLine(15, vec2(X*0.1,Y*0.25), vec2(X*0.7, Y*0.25)), EGP:egpSize(15,6), EGP:egpColor(15,Color1)
 
    TempFrame = "frame3"
    timer("saveFrame",10)
}
 
if(clk("frame4")){
    local Color1 = vec4(0,0,100,255)
    local Color2 = vec4(55,55,55,255)
   
    EGP:egpBox(26,vec2(X*0.4,Y*0.5),vec2(X*0.65,Y*0.85)), EGP:egpColor(26,Color2)
    EGP:egpBox(13, vec2(X*0.4,Y*0.15),vec2(X*0.6,Y*0.1)), EGP:egpColor(13,Color1)
   
    for(I=1,12){
        if(I<=6){ EGP:egpBox(I,vec2(X*0.25,(Y/20)*I + (Y/5)),vec2(X*0.3,Y/17.5)) }
        if(I >6){ EGP:egpBox(I,vec2(X*0.55,(Y/20)*(I-6) + (Y/5)),vec2(X*0.3,Y/17.5)) }
        EGP:egpSize(I,3)
        EGP:egpColor(I,Color1)
    }
   
   
    EGP:egpBox(27, vec2(X*0.4,Y*0.6), vec2(X*0.4,Y*0.1)), EGP:egpColor(27, Color1)
    EGP:egpCircleOutline(28, EGP:egpPos(27) + vec2(-X*0.25,0), vec2(X*0.05,Y*0.05)), EGP:egpAngle(28, 180)
    EGP:egpCircleOutline(29, EGP:egpPos(27) + vec2(X*0.25,0), vec2(X*0.05,Y*0.05)), EGP:egpAngle(29, 0)
    EGP:egpText(30, "> CLASS: ", -EGP:egpSize(27)/2 + vec2(10,15)), EGP:egpParent(30,27)
   
    for(I=1,2){
        EGP:egpFidelity(I+27, 3)
        EGP:egpSize(I+27, 5)
        EGP:egpColor(I+27, Color1)
    }
   
    EGP:egpBox(31, vec2(X*0.25,Y*0.725), vec2(X*0.3,Y/10))
    EGP:egpColor(31, vec4(155,55,55,100))
    EGP:egpText(32, "[ RESET ]", -EGP:egpSize(31)/2 + vec2(10,15)), EGP:egpParent(32,31)
    EGP:egpBox(33, vec2(X*0.55,Y*0.725), vec2(X*0.3,Y/10))
    EGP:egpColor(33, Color1)
    EGP:egpText(34, "[ ON ]", -EGP:egpSize(33)/2 + vec2(10,15)), EGP:egpParent(34,33)
   
    EGP:egpText(35, "<DEFENSE TARGETING INTERFACE>", -EGP:egpSize(13)/2 + vec2(15,15)), EGP:egpParent(35,13)
   
    TempFrame = "frame4"
    timer("saveFrame",10)
}
 
if(clk("main")){
    EGP:egpBox(51, vec2(X,Y)/2, vec2(X, Y)) , EGP:egpColor(51, Color1)
    EGP:egpBox(52, vec2(X*0.4,Y*0.5), vec2(X*0.7, Y*0.9))
   
    EGP:egpBox(53, vec2(X*0.875,Y*0.15), vec2(X*0.175, Y*0.2))
    EGP:egpBox(54, vec2(X*0.875,Y*0.3833), vec2(X*0.175, Y*0.2))
    EGP:egpBox(55, vec2(X*0.875,Y*0.6166), vec2(X*0.175, Y*0.2))
    EGP:egpBox(56, vec2(X*0.875,Y*0.8499), vec2(X*0.175, Y*0.2))
   
    for(I=1, 5){
        EGP:egpColor(I+51, Color2)
    }
   
    EGP:egpText(57, "[A]", vec2(0,0))
    EGP:egpText(58, "[B]", vec2(0,0))
    EGP:egpText(59, "[C]", vec2(0,0))
    EGP:egpText(60, "[D]", vec2(0,0))
   
    for(I=1, 4){
        EGP:egpParent(I+56, I+52)
        EGP:egpPos(I+56, vec2(-17,-15))
        EGP:egpFont(I+56, "ChatFont", 25)
    }
}
 
if(clk("logo")){
    local Color1 = vec4(255,191,0,255)
    local Color2 = vec4(55,55,55,255)
    local Color3 = vec4(155,25,25,255)
   
    EGP:egpBox(1,vec2(X*0.4,Y*0.5),vec2(X*0.205,Y*0.2))
    EGP:egpTriangle(2,vec2(X*0.3,Y*0.4),vec2(X*0.3,Y*0.6),vec2(X*0.2,Y*0.5))
    EGP:egpTriangle(3,vec2(X*0.5,Y*0.4),vec2(X*0.5,Y*0.6),vec2(X*0.6,Y*0.5))
    EGP:egpCircle(4,vec2(X*0.4,Y*0.5),vec2(X*0.1,Y*0.1)), EGP:egpColor(4, Color2)
    EGP:egpCircle(5,vec2(X*0.4,Y*0.5),vec2(X*0.075,Y*0.075)), EGP:egpColor(5, Color3)
   
    EGP:egpText(6,"C04STRCT-OR",vec2(X*0.15,Y*0.25)), EGP:egpFont(6,"Arial",45)
    EGP:egpText(7,"O.S. v2.0",vec2(X*0.17,Y*0.65)), EGP:egpFont(7,"Arial",65)
   
    for(I=1,3){
        EGP:egpColor(I, Color1)
    }
}
 
function void button(Index){
    Cursor = EGP:egpCursor(User)
       
    LT = EGP:egpPos(Index)-EGP:egpSize(Index)/2
    RB = EGP:egpPos(Index)+EGP:egpSize(Index)/2
       
    if(inrange(Cursor,LT,RB)){
        if(User == owner()){
            TempIndex = Index
            timer("resetIndex",5)
            User:soundPlay(1,1,"buttons/blip1.wav")  
        }      
    }
}
 
if(changed(EGPMode)){
    if(EGPMode == 2){
        EGP:egpSetText(14, array("TARGET MODE: ",TargetMode):concat())
        EGP:egpSetText(15, array("CLASS: [",TargetClass,"]"):concat())
        timer("update",1000)
       
        EGP:egpSetText(16, array("<",Target:toString():sub(0,15),">"):concat())
        EGP:egpSetText(17, array("<",Target:owner():toString():sub(0,15),">"):concat())
        EGP:egpSetText(18, array("<",round(TarPos):toString():sub(0,15),">"):concat())
        EGP:egpSetText(19, array("<",round(Distance ):toString():sub(0,15),">"):concat())
    }
    if(EGPMode == 4){
        if(TurretOn){ EGP:egpSetText(34, "[ ON ]") }
        if(!TurretOn){ EGP:egpSetText(34,  "[ OFF ]") }  
        EGP:egpSetText(30, array("CLASS: [",TurretClass,"]"):concat())
    }
}
   
if(~User & User){  
    if(!Building){  
        button(53)
        button(54)
        button(55)
        button(56)
    }
 
    if(TempIndex == 53){ EGP:egpLoadFrame("frame1"), timer("main",10), EGPMode = 1 }
    if(TempIndex == 54){ EGP:egpLoadFrame("frame2"), timer("main",10), EGPMode = 2 }
    if(TempIndex == 55){ EGP:egpLoadFrame("frame3"), timer("main",10), EGPMode = 3 }
    if(TempIndex == 56){ EGP:egpLoadFrame("frame4"), timer("main",10), EGPMode = 4 }
}
 
if(EGPMode == 1){
    if(changed(WayPointIndex)){
        EGP:egpSetText(17, (WayPointIndex-1):toString())  
    }
   
    if(~User & User){
        button(1)
        button(2)
        button(3)
        button(4)
        button(12)
        button(13)
       
        local Color1 = vec4(0,0,100,255)
        local Color2 = vec4(100,100,100,255)
       
        if(TempIndex == 1){ EGP:egpColor(1,Color2), ModeT ="bunker"}else{ EGP:egpColor(1,Color1) }
        if(TempIndex == 2){ EGP:egpColor(2,Color2),ModeT = "flight"}else{ EGP:egpColor(2,Color1) }
        if(TempIndex == 3){ EGP:egpColor(3,Color2),ModeT = "bunker",timer("route",3000) }else{ EGP:egpColor(3,Color1) }
        if(TempIndex == 4){ EGP:egpColor(4,Color2),ModeT = "bunker",timer("patrol",3000) }else{ EGP:egpColor(4,Color1) }
       
        if(TempIndex == 12){
            if(WayPointIndex > 1){
                User:soundPlay(1,1,"buttons/blip2.wav")
                Start = 1
                timer("resetStart",1000)      
            }
        }
       
        if(TempIndex == 13){
            if(WayPointIndex > 1){
                User:soundPlay(1,1,"buttons/blip2.wav")
                Clear = 1
                timer("resetClear",1000)        
            }
        }          
    }
   
    if(clk("resetStart")){ Start = 0 }
    if(clk("resetClear")){ Clear = 0 }  
   
    if(clk("route")){
        ModeT = "route"
    }
    if(clk("patrol")){
        ModeT = "patrol"
    }
}
 
if(EGPMode == 2){
    if(~User & User){
        button(5)
        button(6)
        button(7)
        button(8)
        button(10)
        button(11)
        button(12)
       
        if(TempIndex == 5){ TargetMode = clamp(TargetMode - 1,1,3), EGP:egpSetText(14, array("TARGET MODE: ",TargetMode):concat()) }
        if(TempIndex == 6){ TargetMode = clamp(TargetMode + 1,1,3), EGP:egpSetText(14, array("TARGET MODE: ",TargetMode):concat()) }
        if(TempIndex == 7){
            TargetClassIndex = clamp(TargetClassIndex - 1,1,TargetClassArray:count())
            TargetClass = TargetClassArray[TargetClassIndex,string]
            EGP:egpSetText(15, array("CLASS: [",TargetClass,"]"):concat())
        }
        if(TempIndex == 8){
            TargetClassIndex = clamp(TargetClassIndex + 1,1,TargetClassArray:count())
            TargetClass = TargetClassArray[TargetClassIndex,string]
            EGP:egpSetText(15, array("CLASS: [",TargetClass,"]"):concat())
        }
        if(TempIndex == 10){
            if(!R & TarPos != vec(0,0,0)){
                Fire = 1
                timer("reload",3000)
            }
        }
        if(TempIndex == 11){
            if(!R){ TargetFind = 1 }
        }
        if(TempIndex == 12){
            if(!TargetClear){ TargetClear = 1, timer("resetTargetClear",1000) }
        }
    }
   
    if(TargetMode == 1 | TargetMode == 2){
        if((changed(Target) & Target) | TargetClear){
            TargetFind = 0
            EGP:egpSetText(16, array("<",Target:toString():sub(0,15),">"):concat())
            EGP:egpSetText(17, array("<",Target:owner():toString():sub(0,15),">"):concat())
            EGP:egpSetText(18, array("<",round(Target:pos()):toString():sub(0,15),">"):concat())
            EGP:egpSetText(19, array("<",round(Distance ):toString():sub(0,15),">"):concat())
        }    
    }
    if(TargetMode == 3){
        if(changed(TarPos)  | TargetClear){
            TargetFind = 0
            EGP:egpSetText(16, array("<",Target:toString():sub(0,15),">"):concat())
            EGP:egpSetText(17, array("<",Target:owner():toString():sub(0,15),">"):concat())
            EGP:egpSetText(18, array("<",round(TarPos):toString():sub(0,15),">"):concat())
            EGP:egpSetText(19, array("<",round(Distance ):toString():sub(0,15),">"):concat())
        }    
    }
}
 
if(EGPMode == 3){
    if(~User & User){
        button(3)
        button(4)
        button(5)
       
        if(TempIndex == 3 & Mode == "shield"){ ModeT = "build" }
        if(TempIndex == 4){ timer("resetBuild",1000), BuildStart = 1 }
        if(TempIndex == 5){ timer("resetBuild",1000), BuildClear = 1 }
        if(ModeT == "build"){ EGP:egpSetText(8, "> BUILDMODE: ENABLED") }else{ EGP:egpSetText(8, "> BUILDMODE: DISABLED") }
    }
}
 
if(EGPMode == 4){
    function void addPlayer(Index){
        Cursor = EGP:egpCursor(User)
       
        LT = EGP:egpPos(Index)-EGP:egpSize(Index)/2
        RB = EGP:egpPos(Index)+EGP:egpSize(Index)/2
           
        if(inrange(Cursor,LT,RB)){
            if(User == owner()){
                if(Players[Index,entity]:isValid()){
                    TargetPlayerIndex++
                    TargetPlayers:insertEntity(TargetPlayerIndex,Players[Index,entity])  
                    EGP:egpColor(Index, vec4(155,0,0,255))
                    User:soundPlay(1,1,"buttons/button16.wav")
                }
                else{
                    User:soundPlay(1,1,"buttons/button10.wav")
                }
            }      
        }
    }
   
    if(~User & User){
        for(I=1,12){
            addPlayer(I)
        }
        button(28)
        button(29)
        button(31)
        button(33)
       
        if(TempIndex == 28){
            TurretClassIndex = clamp(TurretClassIndex - 1,1,TargetClassArray:count())
            TurretClass = TargetClassArray[TurretClassIndex,string]
            EGP:egpSetText(30, array("CLASS: [",TurretClass,"]"):concat())
        }
        if(TempIndex == 29){
            TurretClassIndex = clamp(TurretClassIndex + 1,1,TargetClassArray:count())
            TurretClass = TargetClassArray[TurretClassIndex,string]
            EGP:egpSetText(30, array("CLASS: [",TurretClass,"]"):concat())
        }
       
        if(TempIndex == 31){
            timer("resetIndex",100)
            TargetPlayers = array()
            TargetPlayerIndex = 0
           
            for(I=1,12){
                EGP:egpColor(I,vec4(0,0,100,255))
                EGP:egpRemove(I+13)
            }
           
            Players = players()
   
            for(I=1, Players:count()){
                Player = Players[I,entity]
                EGP:egpText(I+13, Player:name(), vec2(-EGP:egpSize(I):x()/2+10,-EGP:egpSize(I):y()/2.5))
                EGP:egpParent(I+13,I)
            }        
        }
        if(TempIndex == 33){
            if(!Building){
                TurretOn = !TurretOn
               
                if(TurretOn){ EGP:egpSetText(34, "[ ON ]") }
                if(!TurretOn){ EGP:egpSetText(34,  "[ OFF ]") }    
            }
        }
    }
}
 
if(clk("reload")){ entity():soundPlay(1,1,"vehicles/tank_turret_stop1.wav"), Fire = 0 }
if(clk("resetIndex")){ TempIndex = 0 }
if(clk("resetTargetClear")){ TargetClear = 0 }
if(clk("saveFrame")){ EGP:egpSaveFrame(TempFrame), timer("clear",10) }
if(clk("clear")){ EGP:egpClear() }
if(clk("resetBuild")){ BuildStart = 0, BuildClear = 0 }
 
if(changed(Building) & Building){
    TurretOn = 0
}