@name Solar System Holo E2
@persist T C C2 C3 C4 T2 T3 T4 T5 T6 T7 T8 T9 T10 R1 S
runOnTick(1)



#PLANET ORBITS
#Sun
#Yes i have the sun on a bit of an orbit
S=S*1+1
E=cos(S)
F=sin(S)
#P1
T=T*1+0.3
X=cos(T)
Y=sin(T)

#P2
T2=T2*1+0.5
X2=cos(T2)
Y2=sin(T2)

#P3
T3=T3*1+0.4 
X3=cos(T3)
Y3=sin(T3)

#P4
T4=T4*1+0.37
X4=cos(T4)
Y4=sin(T4)

#P5
T5=T5*1+0.3 
X5=cos(T5)
Y5=sin(T5)

#P6
T6=T6*1+0.1 
X6=cos(T6)
Y6=sin(T6)

#P7
T7=T7*1+0.09 
X7=cos(T7)
Y7=sin(T7)

#P8
T8=T8*1+0.08 
X8=cos(T8)
Y8=sin(T8)

#P9
T9=T9*1+0.07 
X9=cos(T9)
Y9=sin(T9)

#P10
T10=T10*1+0.06
X10=cos(T10)
Y10=sin(T10)

#MOON ORBITS
C=C*1+1
A=cos(C)
B=sin(C)

C2=C2*1+0.7
A2=cos(C2)
B2=sin(C2)

C3=C3*1+0.5
A3=cos(C3)
B3=sin(C3)

C4=C4*1+0.4
A4=cos(C4)
B4=sin(C4)

#Ring Orbits
#Saturn
R1=R1*1+0.7
D=cos(R1)
E=sin(R1)

#Sun
holoCreate(1)
holoScale(1, vec(20,20,20))
holoPos(1, entity():toWorld(vec(E*2,F*2,300)))
holoParent(1, entity())
holoColor(1, vec(255,205,0))
holoModel(1, "hqsphere")

#Mercury
holoCreate(2)
holoScale(2, vec(1.5,1.5,1.5))
holoPos(2, entity():toWorld(vec(X2*250,Y2*250,300)))
holoParent(2, entity())
holoColor(2, vec(137,113,89))
holoModel(2, "hqsphere")

#Venus
holoCreate(3)
holoScale(3, vec(3,3,3))
holoPos(3, entity():toWorld(vec(X3*360,Y3*360,300)))
holoParent(3, entity())
holoColor(3, vec(210,118,25))
holoModel(3, "hqsphere")

#Earth
holoCreate(4)
holoScale(4, vec(3.5,3.5,3.5))
holoPos(4, entity():toWorld(vec(X4*490,Y4*490,300)))
holoParent(4, entity())
holoColor(4, vec(51,153,255))
holoModel(4, "hqsphere")

#Mars
holoCreate(5)
holoScale(5, vec(2.8,2.8,2.8))
holoPos(5, entity():toWorld(vec(X5*640,Y5*640,300)))
holoParent(5, entity())
holoColor(5, vec(125,84,37))
holoModel(5, "hqsphere")

#Jupiter
holoCreate(6)
holoScale(6, vec(12,12,12))
holoPos(6, entity():toWorld(vec(X6*1050,Y6*1050,300)))
holoParent(6, entity())
holoColor(6, vec(201,172,139))
holoModel(6, "hqsphere")

#Saturn
holoCreate(7)
holoScale(7, vec(8,8,8))
holoPos(7, entity():toWorld(vec(X7*1550,Y7*1550,300)))
holoParent(7, entity())
holoColor(7, vec(236,222,20))
holoModel(7, "hqsphere")
holoAng(7, ang(vec(45,0,0)))


#SaturnsRing
holoCreate(8)
holoScale(8, vec(15,15,1))
holoPos(8, holoEntity(7):toWorld(vec(D*1,E*1,0)))
holoParent(8, holoEntity(7))
holoColor(8, vec(218,206,47))
holoModel(8, "hqtorus")
holoAng(8, ang(vec(45,0,0)))
holoAlpha(8, 100)

#Uranus
holoCreate(9)
holoScale(9, vec(5,5,5))
holoPos(9, entity():toWorld(vec(X8*2250,Y8*2250,300)))
holoParent(9, entity())
holoColor(9, vec(51,153,255))
holoModel(9, "hqsphere")
holoAng(9, ang(vec(80,0,0)))

#Uranus's Ring
holoCreate(10)
holoScale(10, vec(10,10,1))
holoPos(10, entity():toWorld(vec(X8*2250,Y8*2250,300)))
holoParent(10, entity())
holoColor(10, vec(0,76,153))
holoAng(10, ang(vec(80,0,0)))
holoModel(10, "hqtorus")
holoAlpha(10, 30)

#Neptune
holoCreate(11)
holoScale(11, vec(5.5,5.5,5.5))
holoPos(11, entity():toWorld(vec(X9*3050,Y9*3050,300)))
holoParent(11, entity())
holoColor(11, vec(0,0,255))
holoModel(11, "hqsphere")

#Neptune's Ring
holoCreate(12)
holoScale(12, vec(9,9,1))
holoPos(12, entity():toWorld(vec(X9*3050,Y9*3050,300)))
holoParent(12, entity())
holoColor(12, vec(192,192,192))
holoModel(12, "hqtorus")
holoAlpha(12, 20)

if(first()){ hint("Made By MrCilliman,3) runOnTick(1) timer("ColChange",0) R = vec(255,0,0)
}

#Pluto
holoCreate(13)
holoScale(13, vec(1.5,1.5,1.5))
holoPos(13, entity():toWorld(vec(X10*3750,Y10*3000,300)))
holoParent(13, entity())
holoColor(13, vec(96,96,96))
holoModel(13, "hqsphere")
holoAng(13, ang(vec(45,0,0)))

#THE MOOOOOOOOOOOOOOOOOOONS

#Earths Moon 
holoCreate(14)
holoScale(14, vec(0.6,0.6,0.6))
holoPos(14, holoEntity(4):toWorld(vec(A*40,B*40,0)))
holoParent(14, holoEntity(4))
holoColor(14, vec(96,96,96))
holoModel(14, "hqsphere")

#Plutos Moon
holoCreate(15)
holoScale(15, vec(1.4,1.4,1.4))
holoPos(15, holoEntity(13):toWorld(vec(A*40,B*40,0)))
holoParent(15, holoEntity(13))
holoColor(15, vec(96, 100, 80))
holoModel(15, "hqsphere")
holoAng(15, ang(vec(45,0,0)))

#Jupiters Moons
#Moon1
holoCreate(16)
holoScale(16, vec(1,1,1))
holoPos(16, holoEntity(6):toWorld(vec(A*100,B*100,0)))
holoParent(16, holoEntity(6))
holoColor(16, vec(255,154,0))
holoModel(16, "hqsphere")
#moon2
holoCreate(17)
holoScale(17, vec(1,1,1))
holoPos(17, holoEntity(6):toWorld(vec(A2*115,B2*115,0)))
holoParent(17, holoEntity(6))
holoColor(17, vec(0,0,255))
holoModel(17, "hqsphere")
#Moon3
holoCreate(18)
holoScale(18, vec(1,1,1))
holoPos(18, holoEntity(6):toWorld(vec(A3*150,B3*150,0)))
holoParent(18, holoEntity(6))
holoColor(18, vec(96,96,96))
holoModel(18, "hqsphere")
#moon4
holoCreate(19)
holoScale(19, vec(1,1,1))
holoPos(19, holoEntity(6):toWorld(vec(A4*180,B4*180,0)))
holoParent(19, holoEntity(6))
holoColor(19, vec(255,255,51))

