@name Flight_Simple
@inputs     W A S D Fly Hover
@outputs    Ve:vector
@persist    EE:entity
@trigger 
@model models/hunter/plates/plate1x1.mdl

#Downloaded from sicklebrick.com
#Code snippets based on Techni's quaternion based flight device here:
#http://www.wiremod.com/forum/finished-contraptions/14446-fully-3d-mouse-controlled-flight.html#post138320

runOnTick(1)
Speed = 200

if ( S ){ W = -1 }
if ( D ){ A = -1 }

Enabled = owner():keyReload() | Fly | Hover

if ( first() ){
    
    EE = entity()
    

    
    
    EE:setMass(5000)
    
    Ps = EE:getConstraints()
    rangerPersist(1)
    rangerHitWater(0)
    rangerDefaultZero(0)
    rangerFilter(Ps)    
        
}

    
    #Calculate ground distance for hover awesomeness

if (Enabled) {
    
    
    DownRange = rangerOffset(200, EE:pos(), vec(0,0,-1) )
    DNorm = DownRange:hitNormal()
    DPos = DownRange:position()
    
    GDist = EE:pos():distance(DPos)
        
    #Dampen movement    
    EE:applyForce( -EE:vel() * EE:mass() )
    
    if ( GDist < 50 ){ EE:applyForce( vec(0,0,1) * EE:mass() * 20  )  }
    
        
    if (Fly ){ EE:applyForce( EE:forward() * EE:mass() * Speed )  }
    
    #Rotation
        
    Delta = EE:forward()
        
    
    TarA = quat( Delta:toAngle() )
    CurA = quat(entity() )
    
    
    Ve = entity():toLocal( rotationVector(TarA/CurA) +entity():pos() ) * 150 
        
    EE:applyTorque( (Ve  -12*EE:angVelVector()) * entity():inertia()   )    
    
    EE:applyAngForce( ang( 50 * W * EE:mass() , 100 * A  * EE:mass() ,  100 * -A * EE:mass() ) )
    
 }  
    



