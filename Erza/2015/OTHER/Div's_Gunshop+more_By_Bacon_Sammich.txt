@name Div's Gunshop+more By Bacon Sammich
@inputs S:wirelink [User R1 R2 R3 R4]:entity
@outputs Shipment1 Shipment2 Shipment3 Shipment4 Door  
@persist [BGChange]:array Shipment1 Shipment2 Shipment3 Shipment4 
@persist Seconds Current Door Shop Selection1 Selection2 Ra1 Ra2 Ra3 Ra4
@persist Selection3 Selection4 Confirm Dist1 Dist2 Dist3 Dist4 Select:string
@persist SlotCheck Swap Return Gun1Price Gun2Price Gun3Price Gun4Price
@persist [Text1 Text2 Text3 Text4 Text5 Text6 Text7 AmountL]:string 
interval(400)
if(S&~S){reset()}
#[
BBBBBBBBBBBBBBBBB                                                                            
B::::::::::::::::B                                                                           
B::::::BBBBBB:::::B                                                                          
BB:::::B     B:::::B                                                                         
  B::::B     B:::::B  aaaaaaaaaaaaa       cccccccccccccccc   ooooooooooo   nnnn  nnnnnnnn    
  B::::B     B:::::B  a::::::::::::a    cc:::::::::::::::c oo:::::::::::oo n:::nn::::::::nn  
  B::::BBBBBB:::::B   aaaaaaaaa:::::a  c:::::::::::::::::co:::::::::::::::on::::::::::::::nn 
  B:::::::::::::BB             a::::a c:::::::cccccc:::::co:::::ooooo:::::onn:::::::::::::::n
  B::::BBBBBB:::::B     aaaaaaa:::::a c::::::c     ccccccco::::o     o::::o  n:::::nnnn:::::n
  B::::B     B:::::B  aa::::::::::::a c:::::c             o::::o     o::::o  n::::n    n::::n
  B::::B     B:::::B a::::aaaa::::::a c:::::c             o::::o     o::::o  n::::n    n::::n
  B::::B     B:::::Ba::::a    a:::::a c::::::c     ccccccco::::o     o::::o  n::::n    n::::n
BB:::::BBBBBB::::::Ba::::a    a:::::a c:::::::cccccc:::::co:::::ooooo:::::o  n::::n    n::::n
B:::::::::::::::::B a:::::aaaa::::::a  c:::::::::::::::::co:::::::::::::::o  n::::n    n::::n
B::::::::::::::::B   a::::::::::aa:::a  cc:::::::::::::::c oo:::::::::::oo   n::::n    n::::n
BBBBBBBBBBBBBBBBB     aaaaaaaaaa  aaaa    cccccccccccccccc   ooooooooooo     nnnnnn    nnnnnn
]#
BGChange = array("vgui/appchooser/background_ep2","vgui/appchooser/background_episodic","vgui/appchooser/background_hl2")
Seconds = 4000 #think of it like this 25 + 000 = 25 seconds (1000 for 1)


if(first()){
    Current=1
    Shop = 0
    S:egpClear()
        function number wirelink:pIR(Idx:number,Ply:entity,TopLeft:number)
    {
        TL = This:egpPos(Idx) - This:egpSize(Idx) / 2 * (TopLeft ? 0 : 1)
        BR = This:egpPos(Idx) + This:egpSize(Idx) / (TopLeft ? 1 : 2)
        Cur = This:egpCursor(Ply)
        return inrange(Cur, TL, BR)
    }}

#---- Do not Touch Boxes

S:egpBox(1,vec2(256,256),vec2(512,512))
S:egpMaterial(1,BGChange[Current,string])


#---- Do not Touch If's
timer("Changer",Seconds)
if(clk("Changer")){Current+=1}
if(Current>=4){Current=1}
#----

#---- Deleteables ----# (In sense of e2s)
S:egpCircle(2,vec2(256,256),vec2(100,100))
S:egpColor(2,vec4(0,0,255,100))
S:egpText(3,"Tap Anywhere to begin.",vec2(170,240))
S:egpBox(4,vec2(256,256),vec2(512,512))
S:egpColor(4,vec4(0,0,0,0))
#----

if(~User & User & !Shop){if( S:pIR(4, User, 0) ) {Shop = !Shop}}
if(~User & User & Shop){if( S:pIR(5, User, 0) ) {Selection1 = 1 Selection2 = 0 Selection3 = 0 Selection4 = 0}}
if(~User & User & Shop){if( S:pIR(6, User, 0) ) {Selection1 = 0 Selection2 = 1 Selection3 = 0 Selection4 = 0}}
if(~User & User & Shop){if( S:pIR(7, User, 0) ) {Selection1 = 0 Selection2 = 0 Selection3 = 1 Selection4 = 0}}
if(~User & User & Shop){if( S:pIR(8, User, 0) ) {Selection1 = 0 Selection2 = 0 Selection3 = 0 Selection4 = 1}}
if(~User & User & Shop){if( S:pIR(15, User, 0)){Confirm=1}}

if(Shop){
    Dist1 = 70
    Dist2 = 70
    Dist3 = 70
    Dist4 = 70

    S:egpRemove(3)
    S:egpRemove(4)
    
    S:egpBox(2, vec2(256,256),vec2(455,455))
    S:egpColor(2, vec4(20,20,20,220))
    
    #---- The 4 Selection Buttons ----#
    S:egpBox(5, vec2(120,80),vec2(125,40))#Box 1
    S:egpBox(6, vec2(120,130),vec2(125,40))#Box 2
    S:egpBox(7, vec2(120,180),vec2(125,40))#Box 3
    S:egpBox(8, vec2(120,230),vec2(125,40))#Box 4
    S:egpText(9,R1:shipmentName(),vec2(Dist1,70))#Box 1 Text
    S:egpText(10,R2:shipmentName(),vec2(Dist2,120))#Box 2 Text
    S:egpText(11,R3:shipmentName(),vec2(Dist3,170))#Box 3 Text
    S:egpText(12,R4:shipmentName(),vec2(Dist4,220))#Box 4 Text
    S:egpSize(9,15)
    S:egpSize(10,15)
    S:egpSize(11,15)
    S:egpSize(12,15)
    #---- ----#
    
    S:egpBox(13, vec2(325,155),vec2(250,188))
    S:egpColor(13, vec4(0,0,255,100))
    S:egpBox(14, vec2(255,350),vec2(390,180))
    S:egpColor(14, vec4(0,0,255,100))
    
    S:egpBox(15, vec2(256,410),vec2(300,30))#Purchase Button
    S:egpText(16,"Purchase a :",vec2(170,400))
    S:egpText(17, Select ,vec2(266,403))
    S:egpSize(17,15)
    
    S:egpText(18,Text1,vec2(70,265))
    S:egpText(19,Text2,vec2(70,280))
    S:egpText(20,Text3,vec2(70,295))
    S:egpText(21,Text4,vec2(70,310))
    S:egpText(22,Text5,vec2(70,325))
    S:egpText(23,Text6,vec2(70,340))
    S:egpText(24,Text7,vec2(70,355))
    S:egpText(25,"Amount left :",vec2(215,75))
    S:egpText(27,"Price :",vec2(215,100))
    S:egpSize(18,15)
    S:egpSize(19,15)
    S:egpSize(20,15)
    S:egpSize(21,15)
    S:egpSize(22,15)
    S:egpSize(23,15)
    S:egpSize(24,15)

    
}
if(Selection1 & !Selection2 & !Selection3 & !Selection4){
        Ra1=1 Ra2=0 Ra3=0 Ra4=0
        Gun1Price=R1:shipmentPrice()/7
        S:egpText(26,R1:shipmentAmount():toString(),vec2(320,75))
        S:egpText(28,round(Gun1Price):toString(),vec2(280,100))
        Select = R1:shipmentName()
        S:egpColor(5, vec4(0,255,0,100))
        S:egpColor(6, vec4(0,0,255,100))
        S:egpColor(7, vec4(0,0,255,100))
        S:egpColor(8, vec4(0,0,255,100))
}
if(!Selection1 & Selection2 & !Selection3 & !Selection4){
        Ra1=0 Ra2=1 Ra3=0 Ra4=0
        Gun2Price=R2:shipmentPrice()/7
        S:egpText(26,R2:shipmentAmount():toString(),vec2(320,75))
        S:egpText(28,round(Gun2Price):toString(),vec2(280,100))
        Select = R2:shipmentName()
        S:egpColor(5, vec4(0,0,255,100))
        S:egpColor(6, vec4(0,255,0,100))
        S:egpColor(7, vec4(0,0,255,100))
        S:egpColor(8, vec4(0,0,255,100))
}
if(!Selection1 & !Selection2 & Selection3 & !Selection4){
        Ra1=0 Ra2=0 Ra3=1 Ra4=0
        Gun3Price=R3:shipmentPrice()/7
        S:egpText(26,R3:shipmentAmount():toString(),vec2(320,75))
        S:egpText(28,round(Gun3Price):toString(),vec2(280,100))
        Select = R3:shipmentName()
        S:egpColor(5, vec4(0,0,255,100))
        S:egpColor(6, vec4(0,0,255,100))
        S:egpColor(7, vec4(0,255,0,100))
        S:egpColor(8, vec4(0,0,255,100))
}
if(!Selection1 & !Selection2 & !Selection3 & Selection4){
        Ra1=0 Ra2=0 Ra3=0 Ra4=1
        Gun4Price=R4:shipmentPrice()/7
        S:egpText(26,R4:shipmentAmount():toString(),vec2(320,75))
        S:egpText(28,round(Gun4Price):toString(),vec2(280,100))
        Select = R4:shipmentName()
        S:egpColor(5, vec4(0,0,255,100))
        S:egpColor(6, vec4(0,0,255,100))
        S:egpColor(7, vec4(0,0,255,100))
        S:egpColor(8, vec4(0,255,0,100))
}
if(Shop & !Selection1 & !Selection2 & !Selection3 & !Selection4){
        Ra1=0 Ra2=0 Ra3=0 Ra4=0
        S:egpColor(5, vec4(0,0,255,100))
        S:egpColor(6, vec4(0,0,255,100))
        S:egpColor(7, vec4(0,0,255,100))
        S:egpColor(8, vec4(0,0,255,100))
    }
if(Shop & !Confirm & !Ra1 & !Ra2 & !Ra3 & !Ra4){S:egpColor(15, vec4(255,0,0,100)) Confirm=0}
if(Shop & Confirm & !Ra1 & !Ra2 & !Ra3 & !Ra4){S:egpColor(15, vec4(255,0,0,100)) Confirm=0}
if(Shop & !Confirm & Ra1 | Shop & !Confirm & Ra2 | Shop & !Confirm & Ra3 | Shop & !Confirm & Ra4){S:egpColor(15, vec4(255,0,0,100))}
if(Shop & Confirm){S:egpColor(15, vec4(0,255,0,100)) SlotCheck=1}


if(R1:shipmentName()=="PUMP SHOTGUN" & Ra1 | R2:shipmentName()=="PUMP SHOTGUN" & Ra2 | R3:shipmentName()=="PUMP SHOTGUN" & Ra3 | R4:shipmentName()=="PUMP SHOTGUN" & Ra4)
{
    Text1 = "The Savage Arms/Stevens 350 Security Pump-Action"
    Text2 = "Shotgun is loaded with tactical features,"
    Text3 = "making it a
