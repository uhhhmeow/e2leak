@name Holo-Test
@persist Ang:angle ChipLoc:vector A

runOnTick(1)

if(first())
    {
        ChipLoc=entity():pos()
        holoCreate(1,ChipLoc+vec(0,0,150),vec(10,10,10),Ang,vec(255,0,0),"hq_rcylinder_thick")
    }
A++
holoPos(1,ChipLoc+vec(0,0,A*10)) #This teleports the holo to the specified vector. It seems to move smoothly because runOnTick(1)
#makes the chip execute 1000 times a second or something like that.
if(A>-100) {A=0}
