@name Flip it
@outputs Meh:entity
@persist [E,O]:entity Cnt V Size Done Start Ar:array

if(first()) {
    runOnChat(1)
    O = owner()
    E = entity()
    Cnt = 4
    Size = 2
    holoCreate(1,E:pos(),(vec(1,1,1)*Size):setZ(0.1),E:angles(),vec(0,255,0),"hqcylinder")
    holoMaterial(1,"models/shiny")
    holoParent(1,E)
    V = 1
}

if(!Done) {
    if(Cnt<=9) {
        interval(100)
    }
    else {
        interval(200)
    }
    
    V++
    
    if(V<=Cnt) {
        holoCreate(V,holoEntity(V-1):toWorld(vec(Size*12+1,0,0)),(vec(1,1,1)*Size):setZ(0.1),E:angles(),vec(0,255,0),"hqcylinder")
        holoMaterial(V,"models/shiny")
        holoParent(V,E)
    }
    elseif(V<=Cnt^2) {
        holoCreate(V,holoEntity(V-Cnt):toWorld(vec(0,Size*12+1,0)),(vec(1,1,1)*Size):setZ(0.1),E:angles(),vec(0,255,0),"hqcylinder")
        holoMaterial(V,"models/shiny")
        holoParent(V,E)
    }
    if(holoEntity(Cnt^2)!=noentity()) {
        Done = 1
        printColor(vec(255,255,255),"Press ",vec(255,0,0),"E",vec(255,255,255)," on a hologram to flip it and its surrounding blocks")
        runOnTick(1)
    }
}
if(Done) {
    if(changed(O:keyUse())&O:keyUse()) {
        for(I=1,Cnt^2) {
            Holo = holoEntity(I)
            Vec = (O:shootPos()+(Holo:up():dot(Holo:pos()-O:shootPos()))/(Holo:up():dot((O:shootPos()+O:eye())-O:shootPos()))*((O:shootPos()+O:eye())-O:shootPos()))
            if(Vec:distance(Holo:pos())<Size*6) {
                findIncludeClass("gmod_wire_hologram")
                findIncludePlayerProps(O)
                findInBox(Holo:toWorld(vec(Size*18+1,Size*18+1,5)),Holo:toWorld(-vec(Size*18+1,Size*18+1,5)))
                A = findToArray()
                
                for(K=1,A:count()) {
                    if(A[K,entity]:getColor():x()==255) {
                        holoColor(holoIndex(A[K,entity]),vec(0,255,0))
                    }
                    else {
                        holoColor(holoIndex(A[K,entity]),vec(255,0,0))
                    }
                }
                if(Start) {
                    for(K=1,Cnt^2) {
                        if(holoEntity(K):getColor()==vec(0,255,0)) {
                            Ar[K,number] = 1
                        }
                    }
                    if(Ar:sum()==Cnt^2) {
                        if(O == owner()) {
                            printColor(vec(200,0,0),"You won!")
                        }
                        else {
                            concmd("say You won "+O:name()+"!")
                        }
                        Start = 0
                    }
                    Ar = array()
                }
            }
        }
    }
    if(chatClk(O)|chatClk(owner())) {
        M = O:lastSaid():explode(" ")
        if(!Start) {
            if(M[1,string]=="start") {
                if(M[2,string]=="") {
                    printColor(vec(255,255,255),"Try to solve it!")
                    Start = 1
                }
                elseif(findPlayerByName(M[2,string]):isPlayer()) {
                    O = findPlayerByName(M[2,string])
                    concmd("say Try to solve it "+O:name()+"!")
                    Start = 1
                }
                else {
                    hint("Could not find player, playing by yourself..",7)
                    printColor(vec(255,255,255),"Try to solve it!")
                    Start = 1
                }
            }
            if(M[1,string]=="rand") {
                for(I=1,Cnt^2) {
                    Temp = randint(1,2)
                    holoColor(I,vec((Temp==1 ? 1 : 255),(Temp==2 ? 1 : 255),0))
                }
            }
        }
        else {
            if(M[1,string]=="end") {
                Start = 0
                O = owner()
            }
        }
    }
}
    
