@name 
@inputs Start Stop Dist LoopButton
@outputs A Rate Fire FadeStart FadeStop FireLoop RLength
@persist Pod:wirelink
@trigger 

RLength=30
Rate=25

runOnTick(1)
if(Start|LoopButton)
{A=100 FadeStart=1 FadeStop=0 timer("timer",10000)}
if(clk("timer"))
{A=0 FireLoop=0 timer("loop",10000)}
if(clk("loop"))
{FireLoop=1}

if(Stop)
{FadeStart=0 FadeStop=1 stoptimer("timer") stoptimer("loop") reset()}

if(Dist<=6)
{Fire=1}else{Fire=0}
