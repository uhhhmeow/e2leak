@name Tester
@inputs 
@outputs 
@persist 

interval(25)

E = entity()
C = E:isWeldedTo()
D = C:driver()

if(D:keyForward()){
    C:setPos(C:pos() + vec(10,0,0))
}
if(D:keyBack()){
    C:setPos(C:pos() - vec(10,0,0))
}
if(D:keyLeft()){
    C:setPos(C:pos() + vec(0,10,0))
}
if(D:keyRight()){
    C:setPos(C:pos() - vec(0,10,0))
}
if(D:keyAttack1()){
    C:setPos(C:pos() + vec(0,0,10))
}
if(D:keyAttack2()){
    C:setPos(C:pos() - vec(0,0,10))
}

if(owner():keyPressed("x")){
    C:setPos(owner():aimPos() + vec(0,0,40))
}




