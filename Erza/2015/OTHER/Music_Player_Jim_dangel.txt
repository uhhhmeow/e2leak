@name Music Player
@model models/props_lab/citizenradio.mdl
@inputs 
@outputs 
@persist Sel
@trigger 

runOnChat(1)

if(first()){
    hint("Type !latin for latin music",20)
     hint("Type !italian for latin music",20)
     hint("Type !country for latin music",20)
     hint("Type !cuban for latin music",20)
     hint("Type !french for latin music",20)
     hint("Made by Porkatron aka RuthlessSpork",20)
    }


Music = array("ambient/music/latin.wav","ambient/music/flamenco.wav","ambient/music/cubanmusic1.wav","ambient/music/country_rock_am_radio_loop.wav","ambient/music/mirame_radio_thru_wall.wav")

if(Sel>=6){Sel=1}
if(Sel<=0){Sel=5}


String1 = ""
if(Music[Sel, string]=="ambient/music/latin.wav"){String1="Latin   "}
if(Music[Sel, string]=="ambient/music/flamenco.wav"){String1="Italian  "}
if(Music[Sel, string]=="ambient/music/cubanmusic1.wav"){String1="Cuban    "}
if(Music[Sel, string]=="ambient/music/country_rock_am_radio_loop.wav"){String1="Rock     "}
if(Music[Sel, string]=="ambient/music/mirame_radio_thru_wall.wav"){String1="Mirame   "}

if(chatClk(owner())){
    Chat = owner():lastSaid():explode(" ")
    if(Chat:string(1) == "!latin" | Chat:string(1) == "!Latin"){soundPlay(0,0,Music[1,string])}
    if(Chat:string(1) == "!italian" | Chat:string(1) == "!Italian"){soundPlay(0,0,Music[2,string])}
    if(Chat:string(1) == "!cuban" | Chat:string(1) == "!Cuban"){soundPlay(0,0,Music[3,string])}
    if(Chat:string(1) == "!country" | Chat:string(1) == "!Country"){soundPlay(0,0,Music[4,string])}
    if(Chat:string(1) == "!french" | Chat:string(1) == "!French"){soundPlay(0,0,Music[5,string])}
    
    if(Chat:string(1)=="!stop"){soundStop(0,0)}
    if(Chat:string(1)=="!alarm"){soundPlay(0,0,"ambient/alarms/alarm1.wav")}
    }
