
@name RingsODoom-8489 HoloSaver 

@persist [LocalPos Model Color Angle Material]:array Holos I

interval(10)

Shade=0

function void doneOnce()

{

  

}

function void done()

{

  

}



if(first()|dupefinished()){

##Data

LocalPos[1,vector]=vec(-37.514,0.9668,-4.7236)
LocalPos[2,vector]=vec(-38.0277,0.5007,-4.2285)
LocalPos[3,vector]=vec(-38.3868,0.3582,-4.6692)
Model[1,string]="models/sprops/geometry/fring_72.mdl"
Model[2,string]="models/sprops/geometry/sphere_18.mdl"
Model[3,string]="models/sprops/geometry/fring_30.mdl"
Color[1,vector4]=vec4(145,145,145,255)
Color[2,vector4]=vec4(145,145,145,255)
Color[3,vector4]=vec4(145,145,145,255)
Angle[1,angle]=ang(-40.4209,90.0035,0.0028)
Angle[2,angle]=ang(-2.3002,90,-90)
Angle[3,angle]=ang(6.9801,88.0185,-89.5288)
Material[1,string]="debug/env_cubemap_model"
Material[2,string]="debug/env_cubemap_model"
Material[3,string]="debug/env_cubemap_model"
Holos=3

#Data

}

    if(I<Holos&holoCanCreate())

    {

        I++

        holoCreate(I)

        holoModel(I,Model[I,string])

        holoPos(I,entity():toWorld(LocalPos[I,vector]))

        holoAng(I,entity():toWorld(Angle[I,angle]))

        holoScale(I,vec(1,1,1))

        holoColor(I,Color[I,vector4])

        holoMaterial(I,Material[I,string])

        holoRenderFX(I,Shade)

        holoParent(I,entity())

    }elseif(changed(I)&I==Holos){

        doneOnce()

    }else{

        done()

    }
#HoloSaver / HSbase Script by gamerpaddy