@name Text Logger
@inputs A B String:string Using:entity
@outputs String2:string
@persist C Array:array LastUser:entity
@trigger 
interval(500)
if(changed(Using)&&Using!=noentity())
{
    LastUser = Using    
}
if(A==1&&changed(A))
{
    C+=1    
}

if(B==1&&changed(B))
{
    C-=1
}
if(C<=0){C=1}
if(C>Array:count()){C = Array:count()}

if(changed(String)&&String!="")
{
    Array:pushString(String+" -"+LastUser:name())    
}

if(changed(A) || changed(B))
{
    String2 = Array[C,string]    
}
