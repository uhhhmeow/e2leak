@name Hopefully a propcore door
@persist Door:entity A Origin:vector Open
if(first())
{
    A=0
    Open = 0
    Door = entity():isWeldedTo()
    entity():propFreeze(1)
    Origin = Door:pos()   
}

interval(500)
findInSphere(entity():pos(),15)
findClipToClass("player")
Target=findResult(1)
if(Target:isPlayer() && !Target:isAuthorisedPersonnel() && !Target:isMayorAssistant() && !Target:isPoliceDeputy()) {A=1}else{A=0}

if(A==1&&Open!=1)
{
    Door:setPos(Door:pos()+vec(0,0,Door:aabbSize():y()))
    Door:propNotSolid(1)
    Open = 1    
}
elseif(A==0)
{
    Door:setPos(Origin)
    Door:propNotSolid(0)
    Open = 0    
}
