@name Simple Button Door Delay
@inputs A B
@outputs Door
if(first()){Door=0}
if(A==1 || B == 1)
{
    Door = 1
    stoptimer("close")
    timer("close",8000)    
}
if(clk("close"))
{
    Door=0    
}
