@name NeoGunlist
@inputs E:wirelink SA1 SA2 SA3 SA4 SA5 SA6
@persist Item:array
@persist Price:array
@persist Stock1 Stock2 Stock3 Stock4 Stock5
@persist Name:string Player:entity

runOnChat(1)
##CREDITS TO DEAGLER/NEO##
##ALSO CREDITS TO CALLAN FOR MAKING BASE##
##Heavy Edit By Elrond Miles##
A = owner():lastSaid():explode(" ")
if(A[1,string]=="!slot"& chatClk(owner())){
    
hint("Slot: " + A[2,string] + " Player: " + A[3,string],45)
TSlot=A[2,string]:toNumber()
TItem=A[3,string]
TPrice=A[4,string]
Item[TSlot,string]= TItem
}
if(A[1,string]=="!remove"& chatClk(owner())){
print("Slot: " + A[2,string] + " has been cleared")
TSlot=A[2,string]:toNumber()
Item[TSlot,string]= ""
Price[TSlot,string] =""
}
if(A[1,string]=="!clear"& chatClk(owner())){
print("List has been cleared")

Item:clear()
Price:clear()
}
#E:egpClear()
E:egpResolution(vec2(0, 0), vec2(1000, 1000))
E:egpBox(1, vec2(350, 618), vec2(510, 505))
E:egpColor(1, vec(0, 0, 0))
E:egpBox(2, vec2(350, 645), vec2(500, 443))
E:egpBox(3, vec2(350, 396), vec2(500, 50))
E:egpColor(3, vec(255, 93, 0))
E:egpText(5, "Player", vec2(142,381))
E:egpSize(5, 15)
E:egpColor(5, vec(255, 255, 255))
E:egpBox(6, vec2(123, 396), vec2(30, 30))
E:egpMaterial(6,"gui/info")
E:egpText(7, "Cash", vec2(530,381))
E:egpSize(7, 15)
E:egpColor(7, vec(255, 255, 255))
E:egpBox(8, vec2(510, 396), vec2(30, 30))
E:egpMaterial(8,"gui/faceposer_indicator")


# Menu Start
###########################################
E:egpText(9, Item[1,string], vec2(112,430))
E:egpSize(9, 15)
E:egpColor(9, vec(0, 0, 0))
E:egpText(10, Price[1,string], vec2(505,430))
E:egpSize(10, 15)
E:egpColor(10, vec(0, 0, 0))
###########################################
E:egpText(11, Item[2,string], vec2(112,480))
E:egpSize(11, 15)
E:egpColor(11, vec(0, 0, 0))
E:egpText(12, Price[2,string], vec2(505,480))
E:egpSize(12, 15)
E:egpColor(12, vec(0, 0, 0))
###########################################
E:egpText(13, Item[3,string], vec2(112,530))
E:egpSize(13, 15)
E:egpColor(13, vec(0, 0, 0))
E:egpText(14, Price[3,string], vec2(505,530))
E:egpSize(14, 15)
E:egpColor(14, vec(0, 0, 0))
###########################################
E:egpText(15, Item[4,string], vec2(112,580))
E:egpSize(15, 15)
E:egpColor(15, vec(0, 0, 0))
E:egpText(16, Price[4,string], vec2(505,580))
E:egpSize(16, 15)
E:egpColor(16, vec(0, 0, 0))
###########################################
E:egpText(17, Item[5,string], vec2(112,630))
E:egpSize(17, 15)
E:egpColor(17, vec(0, 0, 0))
E:egpText(18, Price[5,string], vec2(505,630))
E:egpSize(18, 15)
E:egpColor(18, vec(0, 0, 0))
###########################################
E:egpText(19, Item[6,string], vec2(112,680))
E:egpSize(19, 15)
E:egpColor(19, vec(0, 0, 0))
E:egpText(20, Price[6,string], vec2(505,680))
E:egpSize(20, 15)
E:egpColor(20, vec(0, 0, 0))
###########################################
E:egpText(21, Item[7,string], vec2(112,730))
E:egpSize(21, 15)
E:egpColor(21, vec(0, 0, 0))
E:egpText(22, Price[7,string], vec2(505,730))
E:egpSize(22, 15)
E:egpColor(22, vec(0, 0, 0))
###########################################
E:egpText(23, Item[8,string], vec2(112,780))
E:egpSize(23, 15)
E:egpColor(23, vec(0, 0, 0))
E:egpText(24, Price[8,string], vec2(505,780))
E:egpSize(24, 15)
E:egpColor(24, vec(0, 0, 0))
###########################################
E:egpText(25, Item[9,string], vec2(112,830))
E:egpSize(25, 15)
E:egpColor(25, vec(0, 0, 0))
E:egpText(26, Price[9,string], vec2(505,830))
E:egpSize(26, 15)
E:egpColor(26, vec(0, 0, 0))
###########################################
# Menu End


E:egpBox(30, vec2(350, 470), vec2(500, 2))
E:egpColor(30, vec(190, 190, 190))
E:egpBox(31, vec2(350, 520), vec2(500, 2))
E:egpColor(31, vec(190, 190, 190))
E:egpBox(32, vec2(350, 570), vec2(500, 2))
E:egpColor(32, vec(190, 190, 190))
E:egpBox(33, vec2(350, 620), vec2(500, 2))
E:egpColor(33, vec(190, 190, 190))
E:egpBox(34, vec2(350, 670), vec2(500, 2))
E:egpColor(34, vec(190, 190, 190))
E:egpBox(35, vec2(350, 720), vec2(500, 2))
E:egpColor(35, vec(190, 190, 190))
E:egpBox(36, vec2(350, 770), vec2(500, 2))
E:egpColor(36, vec(190, 190, 190))
E:egpBox(37, vec2(350, 820), vec2(500, 2))
E:egpColor(37, vec(190, 190, 190))
E:egpBox(50, vec2(490, 620), vec2(5, 500))
E:egpColor(50, vec(0, 0, 0))

E:egpText(90,"$"+SA1:toString(),vec2(500,425))
    E:egpSize(90,20) E:egpColor(90,vec(256,256,256))

E:egpText(91,"$"+SA2:toString(),vec2(500,475))
    E:egpSize(91,20) E:egpColor(91,vec(256,256,256))

E:egpText(92,"$"+SA2:toString(),vec2(500,525))
    E:egpSize(92,20) E:egpColor(92,vec(256,256,256))

E:egpText(93,"$"+SA2:toString(),vec2(500,575))
    E:egpSize(93,20) E:egpColor(93,vec(256,256,256))

E:egpText(94,"$"+SA2:toString(),vec2(500,625))
    E:egpSize(94,20) E:egpColor(94,vec(256,256,256))

E:egpText(95,"$"+SA2:toString(),vec2(500,675))
    E:egpSize(95,20) E:egpColor(95,vec(256,256,256))

E:egpText(96,"$"+SA2:toString(),vec2(500,725))
    E:egpSize(96,20) E:egpColor(96,vec(256,256,256))

E:egpText(97,"$"+SA2:toString(),vec2(500,775))
    E:egpSize(97,20) E:egpColor(97,vec(256,256,256))
    
E:egpText(98,"$"+SA2:toString(),vec2(500,825))
    E:egpSize(98,20) E:egpColor(98,vec(256,256,256))
