@name M A A G
@inputs E:wirelink
@outputs Output1 Output2 Output3
@persist Cur:vector2 U:entity
@trigger 

interval(50)
COLOR1 = 1
COLOR2 = 1
COLOR3 = 1
COLOR4 = 1
COLOR5 = 1
COLOR6 = 1
COLOR7 = 1
COLOR8 = 1

Cur=E:egpCursor(U)
  if(findCanQuery())
{
    findByClass("player")
    U = findClosest(owner():pos())
}
#IF YOU WANT PLAYERS TO USE ASWELL REPLACE OWNER WITH ENTITY!
B1P1=E:egpPos(2)-vec2(75,50)
B1P2=E:egpPos(2)+vec2(75,50)
B1P3=E:egpPos(4)-vec2(75,50)
B1P4=E:egpPos(4)+vec2(75,50)

B1P5=E:egpPos(6)-vec2(75,50)
B1P6=E:egpPos(6)+vec2(75,50)
B1P7=E:egpPos(8)-vec2(75,50)
B1P8=E:egpPos(8)+vec2(75,50)

B1P9=E:egpPos(10)-vec2(75,50)
B1P10=E:egpPos(10)+vec2(75,50)
B1P11=E:egpPos(12)-vec2(75,50)
B1P12=E:egpPos(12)+vec2(75,50)

B1P13=E:egpPos(20)-vec2(70,35)
B1P14=E:egpPos(20)+vec2(70,35)
B1P15=E:egpPos(21)-vec2(70,35)
B1P16=E:egpPos(21)+vec2(70,35)

#Button1 Open
if(inrange(Cur, B1P1, B1P2) & U:keyUse()){
Output1 = 1 soundPlay(1,100,"garrysmod/save_load1.wav")
}

if(inrange(Cur, B1P1, B1P2)){
     COLOR1 = 255
}
#Button1 Close
if(inrange(Cur, B1P3, B1P4) & U:keyUse()){
Output1 = 0 soundPlay(2,100,"garrysmod/save_load1.wav")
}

if(inrange(Cur, B1P3, B1P4)){
     COLOR2 = 255
}
#Button2 Open
if(inrange(Cur, B1P5, B1P6) & U:keyUse()){
Output2 = 1 soundPlay(3,100,"garrysmod/save_load1.wav")
}

if(inrange(Cur, B1P5, B1P6)){
     COLOR3 = 255
}
#Button2 Close
if(inrange(Cur, B1P7, B1P8) & U:keyUse()){
Output2 = 0 soundPlay(4,100,"garrysmod/save_load1.wav")
}

if(inrange(Cur, B1P7, B1P8)){
     COLOR4 = 255
}
#Button3 Open
if(inrange(Cur, B1P9, B1P10) & U:keyUse()){
Output3 = 1 soundPlay(5,100,"garrysmod/save_load1.wav")
}

if(inrange(Cur, B1P9, B1P10)){
     COLOR5 = 255
}
#Button3 Close
if(inrange(Cur, B1P11, B1P12) & U:keyUse()){
Output3 = 0 soundPlay(6,100,"garrysmod/save_load1.wav")
}

if(inrange(Cur, B1P11, B1P12)){
     COLOR6 = 255
}
#Alarm ON
if(inrange(Cur, B1P13, B1P14) & U:keyUse()){
 soundPlay(10,100,"ambient/alarms/alarm_citizen_loop1.wav")
}

if(inrange(Cur, B1P13, B1P14)){
     COLOR7 = 255
}

#Alarm OFF
if(inrange(Cur, B1P15, B1P16) & U:keyUse()){
soundStop(10)
}

if(inrange(Cur, B1P15, B1P16)){
     COLOR8 = 255
}

#Background
E:egpBox(1,vec2(500,500),vec2(1000,1000))
E:egpColor(1,vec(171,0,131))
#Button1 Open
E:egpRoundedBox(2,vec2(80,60),vec2(150,100))
E:egpColor(2,vec(COLOR1,25,25))
E:egpText(3,"Open",vec2(40,40))
E:egpSize(3,40)

#Button1 Close
E:egpRoundedBox(4,vec2(240,60),vec2(150,100))
E:egpColor(4,vec(COLOR2,25,25))
E:egpText(5,"Close",vec2(190,40))
E:egpSize(5,40)

#Button2 Open
E:egpRoundedBox(6,vec2(80,180),vec2(150,100))
E:egpColor(6,vec(COLOR3,25,25))
E:egpText(7,"Open",vec2(40,155))
E:egpSize(7,40)

#Button2 Close
E:egpRoundedBox(8,vec2(240,180),vec2(150,100))
E:egpColor(8,vec(COLOR4,25,25))
E:egpText(9,"Close",vec2(190,160))
E:egpSize(9,40)

#Button3 Open
E:egpRoundedBox(10,vec2(80,380),vec2(150,100))
E:egpColor(10,vec(COLOR5,25,25))
E:egpText(11,"Open",vec2(40,355))
E:egpSize(11,40)

#Button3 Close
E:egpRoundedBox(12,vec2(240,380),vec2(150,100))
E:egpColor(12,vec(COLOR6,25,25))
E:egpText(13,"Close",vec2(190,360))
E:egpSize(13,40)

#Decoration/Titles
E:egpBox(14,vec2(350,225),vec2(50,400))
E:egpColor(14,vec(255,174,0))
E:egpBox(15,vec2(440,225),vec2(140,400))
E:egpColor(15,vec(0,196,29))
E:egpText(16,"M ain Door",vec2(335,30))
E:egpSize(16,35)
E:egpText(17," A rmory",vec2(330,140))
E:egpSize(17,35)
E:egpText(18,"G arage",vec2(340,340))
E:egpSize(18,35)
E:egpText(24,"A larm",vec2(340,260))
E:egpSize(24,35)
#ALARM ON
E:egpRoundedBox(20,vec2(80,280),vec2(140,70))
E:egpColor(20,vec(COLOR7,25,25))
E:egpText(22,"On",vec2(60,260))
E:egpSize(22,30)
#Alarm OFF
E:egpRoundedBox(21,vec2(240,280),vec2(140,70))
E:egpColor(21,vec(COLOR8,25,25))
E:egpText(23,"Off",vec2(210,265))
E:egpSize(23,30)
#Creator
E:egpText(19,"Created By Elrond Miles",vec2(120,470))
E:egpColor(19,vec(0,251,255))
E:egpSize(19,30)


