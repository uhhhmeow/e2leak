@name 
@inputs E:wirelink SA SA2
@outputs 
@persist Total
@trigger 

runOnTick(1)

E:egpText(98,"$"+SA:toString(),vec2(50,25))
E:egpSize(98,50)

E:egpText(99,"$"+SA2:toString(),vec2(50,75))
E:egpSize(99,50)


E:egpText(100,"Total: "+Total:toString(),vec2(50,125))
E:egpSize(100,50)

Total = SA+SA2
