# Foundation
Framework for Wiremod's Expression 2 chip.

### Getting Started
Download or clone the repository into:

    C:\Steam\SteamApps\common\GarrysMod\garrysmod\data\expression2\Foundation-Framework

Read the [documentation] (https://github.com/Benjiko99/Foundation-Framework/wiki) for more info about how to do specific things.

### Resources
Trello Board: https://trello.com/b/OX2RTHP7
