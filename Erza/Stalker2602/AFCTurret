@name general_acf/secondary_turret
@inputs [POD GUN CAM EGP]:wirelink Base:entity Ammo1 Ammo2
@outputs Latch
@persist Elevation Depression Yaw YawOffset RotateSpeed
@persist MultiAmmo [Ammo1Suffix Ammo2Suffix Suffix]:string Active1 Active2
@persist [CamOffset CamOffset_Zoom]:vector Zoom_FOV
@persist Inertia:angle HUD_weapon_info CamMode
 
interval(50)
 
if(first()|dupefinished()){
   
    HUD_weapon_info = 1
   
    Elevation   = 15
    Depression  = 5
    Yaw         = 360
    YawOffset   = 0
    RotateSpeed = 3  
   
    Latch = 1
   
    MultiAmmo = 0
    Ammo1Suffix = "APHE"
    Ammo2Suffix = "APHE"
    Suffix = Ammo1Suffix
    Active1 = 1  
   
    CamOffset=vec(0,0,25)
    CamOffset_Zoom=vec(120,0,0)
    Zoom_FOV=20
   
    rangerHitEntities(0)
    rangerIgnoreWorld(1)
    rangerPersist(1)
   
    holoCreate(1, GUN["Entity",entity]:toWorld(vec(9999,0,0)))
    holoAlpha(1,0)
    holoParent(1, GUN["Entity",entity])
   
    holoCreate(2, GUN["Entity",entity]:massCenter(), vec(1,1,1), GUN["Entity",entity]:angles())
    holoParent(2, GUN["Entity",entity])
    holoAlpha(2, 0)
   
    CAM["Position",vector]=holoEntity(2):toWorld(CamOffset)
    CAM["Parent",entity]=holoEntity(2)
    CAM["Angle",angle] = holoEntity(2):angles()
    CAM["FOV",number] = 90
   
    Inertia = shiftL(ang(GUN["Entity",entity]:inertia()))
}
 
Active = POD["Active",number]
R = POD["R",number]
En = POD["Entity",entity]
Zoom = POD["Shift",number]
 
##WEAPON SYSTEM##
 
function void calibrate(){
    if(GUN["Ready",number]){
        EGP:egpColor(100, vec4(0,255,0,255))    
    }
    else{
        EGP:egpColor(100, vec4(255,0,0,255))                        
    }    
}
 
if(MultiAmmo == 1){    
    if(changed(R) & R){
        timer("unload",500)
        Active1 = !Active1    
        Active2 = !Active2    
        if(Active2 == 1){ Suffix = Ammo2Suffix }else{ Suffix = Ammo1Suffix }
           
        hint(Suffix+" rounds selected",5)    
    }
}
if(clk("unload")){
    GUN["Unload",number] = 1    
}
 
Gun = GUN["Entity",entity]
 
if(Gun:isValid() & HUD_weapon_info){
    if(clk("hud")|changed(Zoom)&Zoom){
        CamMode = !CamMode  
    }
}
 
if (Active){
   
    if(Gun:isValid()){
        Mat     = transpose(matrix(Base))
        Vec     = Mat * En:driver():eye()
        Ang     = Vec:toAngle()
        holoAng(2, Ang)
    }
   
    if(HUD_weapon_info){
 
        Ranger = rangerOffset(9999999999,Gun:pos(),Ang:forward())
        GunAng = (Ranger:pos() - Gun:pos()):toAngle()
       
        Force = Gun:toWorld(clamp(Gun:toLocal(GunAng),ang(-RotateSpeed),ang(RotateSpeed)))
        Clamped = clamp(Base:toLocal(Force),ang(-Elevation,-Yaw+YawOffset,-1),ang(Depression,Yaw+YawOffset,1))
        LocalToGun = Gun:toLocal(Base:toWorld(Clamped))
        Gun:applyAngForce((LocalToGun * 250 - Gun:angVel() * 35 + entity():angVel() * 47 * (abs(Gun:toLocal(GunAng):yaw())>5)) * Inertia) #250, 47
    }
       
    if(HUD_weapon_info){
        if(Active1 == 1){ Ammo = Ammo1 }
        elseif(Active2 == 1){ Ammo = Ammo2 }
 
        EGP:egpSetText(99, "AMMO: "+Ammo+" ["+Suffix+"]")    
    }  
     
    if(changed(GUN["Ready",number])){
        calibrate()
    }
}
 
else{
    soundPitch(1,0)
   
    if(~Active){
        Latch = 1    
    }
}
 
##HUD SETUP##
 
function void hudnormal(){
    ScrSize = egpScrSize(En:driver())
    X = ScrSize:x()
    Y = ScrSize:y()
 
    EGP:egpClear()
 
    if(HUD_weapon_info){
        EGP:egpCircle(100, ScrSize/2, vec2(X/24,X/24))
        EGP:egpColor(100, vec4(0,0,0,255))
        EGP:egpMaterial(100, "gui/faceposer_indicator")
           
        EGP:egpCircle(1, ScrSize/2, vec2(X/25,X/25))
        EGP:egpColor(1, vec4(0,0,0,255))
        EGP:egpMaterial(1, "gui/faceposer_indicator")
 
        EGP:egp3DTracker(2, vec(0,0,0))
        EGP:egpParent(2, holoEntity(1))
       
        EGP:egpCircle(3, vec2(0,0), vec2(X/100,X/100))
        EGP:egpColor(3, vec4(0,0,0,155))
        EGP:egpParent(3, 2)
    }
   
    if(HUD_weapon_info){
        EGP:egpText(99, "AMMO: "+Ammo,vec2(X*0.06, Y*0.78))
        EGP:egpFont(99, "Arial", 40)
        EGP:egpColor(99, vec4(50, 155, 50, 255))  
    }
}
 
function void hudzoom(){
    ScrSize = egpScrSize(En:driver())
    X = ScrSize:x()
    Y = ScrSize:y()
 
    EGP:egpClear()
 
    EGP:egpCircle(100, ScrSize/2, vec2(X/3.95,X/3.95))
    EGP:egpColor(100, vec4(255,0,0,255))
    EGP:egpMaterial(100, "gui/faceposer_indicator")
 
    EGP:egpBox(1, ScrSize/2, ScrSize)
    EGP:egpMaterial(1, "vgui/zoom")
    EGP:egpBox(2, ScrSize/2, ScrSize)
    EGP:egpMaterial(2, "vgui/zoom")
    EGP:egpAngle(2, 180)    
 
    EGP:egpCircle(3, ScrSize/2, vec2(X/4,X/4))
    EGP:egpMaterial(3, "gui/faceposer_indicator")
   
    EGP:egpBox(4, vec2(X/2, Y*0.27), vec2(X*0.01,Y*0.09))
    EGP:egpBox(5, vec2(X/2, Y*0.73), vec2(X*0.01,Y*0.09))
    EGP:egpBox(6, vec2(X*0.373, Y/2), vec2(Y*0.09,X*0.01))
    EGP:egpBox(7, vec2(X*0.627, Y/2), vec2(Y*0.09,X*0.01))
   
    EGP:egpLine(8, vec2(X/2, Y*0.28), vec2(X/2, Y*0.72))
    EGP:egpSize(8, 2)
    EGP:egpLine(9, vec2(X*0.383, Y/2), vec2(X*0.617, Y/2))
    EGP:egpSize(9, 2)
       
    for(I=1, 7){
        EGP:egpColor(I+2, vec4(0,0,0,255))    
    }
   
    if(HUD_weapon_info){
        EGP:egpText(99, "AMMO: "+Ammo,vec2(X*0.06, Y*0.78))
        EGP:egpFont(99, "Arial", 40)
        EGP:egpColor(99, vec4(50, 155, 50, 255))  
    }
}
 
if(changed(CamMode)){
    if(!CamMode){
        CAM["Position",vector]=holoEntity(2):toWorld(CamOffset)
        CAM["Parent",entity]=holoEntity(2)
        CAM["Angle",angle] = holoEntity(2):angles()      
        CAM["FOV",number] = 90
       
        hudnormal()
        calibrate()
    }
    else{
        CAM["Position",vector]=Gun:toWorld(CamOffset_Zoom)
        CAM["Parent",entity]=Gun
        CAM["Angle",angle] = Gun:angles()      
        CAM["FOV",number] = 20  
       
        hudzoom()
        calibrate()      
    }
}
 
if(changed(Active)){
    if(Active){
        #Gun:soundPlay(1,0,"vehicles/tank_turret_loop1.wav")
        #soundPitch(1,0)
       
        User = En:driver()
        Inertia = shiftL(ang(Gun:inertia()))
         
        CAM["Activated",number] = 1  
        Latch = 0  
       
        CamMode = 0
        hudnormal()
        calibrate()    
    }
    else{
        CAM["Activated",number] = 0
        Latch = 1
       
        EGP:egpClear()
    }
}