@name Server Information
@inputs Sc:wirelink
@persist [What Info Prev Day]:array
@trigger 

if (first()|duped()) {
    Sc:writeCell(2041,1)
    Sc:writeCell(2042,333)
    #Title
    W = "Server Information (By Divran)"
    Sc:writeString(W,15-round(W:length()/2),0,6)
    Day[2,string] = "Monday"
    Day[3,string] = "Tuesday"
    Day[4,string] = "Wednesday"
    Day[5,string] = "Thursday"
    Day[6,string] = "Friday"
    Day[7,string] = "Saturday"
    Day[1,string] = "Sunday"
}
interval(1000) #Update
if (clk()|first()|duped()) {
    What[1,string] = "Name: "
    Info[1,string] = hostname()
    What[2,string] = "Map: "
    Info[2,string] = map()
    What[3,string] = "Gamemode: "
    Info[3,string] = gamemode()
    What[4,string] = "Players: "
    Info[4,string] = numPlayers():toString() + "/" + maxPlayers():toString()
    What[5,string] = "Time: "
    Info[5,string] = Day[time("day"),string] + " " + time("hour") + ":" + time("min") + ":" + time("sec")
    #Useless stuff. Remove them if you want to..
    What[6,string] = "Is Lan? "
    Info[6,string] = (isLan() ? "Yes" : "No")
    What[7,string] = "Is Dedicated? "
    Info[7,string] = (isDedicated() ? "Yes" : "No")
    What[8,string] = "Is Single Player? "
    Info[8,string] = (isSinglePlayer() ? "Yes" : "No")
    What[9,string] = "Your ping: "
    Info[9,string] = owner():ping():toString()
    
    #I hope you understand how to add your own stuff to the screen now :)
   
    #Main Code:
    for(I=1,Info:count()) {
        CurWhat = What[I,string]
        CurInfo = Info[I,string]
        #Check if it changed:
        if (CurInfo != Prev[I,string]) {
            Prev[I,string] = CurInfo
            #Cut off the word if it's too long:
            if ((CurWhat+CurInfo):length() > 30) {
                CurInfo = CurInfo:left(30-CurWhat:length()-2) + ".."
            }
            Sc:writeCell(2039,I)
            Sc:writeString(CurWhat,0,I,60)
            Sc:writeString(CurInfo,CurWhat:length(),I)
        }
    }
}
