@name Following Music Player
@persist [Tracks Tracks1 Tracks2 Length LengthMS LengthMS1 LengthMS2]:array Playing MaxDist Vol 

if (first()) {
    
    #Unfreeze/SetInvisible/DisableCollison
    entity():propFreeze(1)
    entity():setColor(vec(0,0,0),0)
    entity():propNotSolid(1)
    entity():setMass(50000)
    entity():setPos(vec(0,0,0))
    runOnChat(1)
    
    #Default Volume Level
    Vol = 0.6
    
    #Define Music
    Tracks = array()
    LengthMS = array()
    
    #Include Music Lists
    #include "stuff/musicset2"
    #include "stuff/musicset1"
    
    Tracks = Tracks:add(Tracks1)
    LengthMS = LengthMS:add(LengthMS1)
    Tracks = Tracks:add(Tracks2)
    LengthMS = LengthMS:add(LengthMS2)
    
    
    
    C = LengthMS:count()
    I = 0
    while ( I < C ) {
        I++
        LengthMSTemp = LengthMS[I,string]
        LengthTempArray = LengthMSTemp:explode(":")
        LengthTemp = (LengthTempArray[1,string]:toNumber()*60)+(LengthTempArray[2,string]:toNumber())
        Length:pushNumber(LengthTemp*1000)
    }
    if ( Tracks:count() != Length:count() ) {
        hint("ERROR: Different Track/Length Counts Aborting",3000)
        hint("Tracks: " + Tracks:count(),3000)
        hint("Lengths: " + Length:count(),3000)
        selfDestruct()
    }
    else {
        hint("Tracks: " + Tracks:count(),3000)
    }

}

if (chatClk(owner())) {
    if (lastSaid():explode(" ")[1,string]=="!skip") {
        Playing = 0
        hideChat(1)
        soundPurge()
        timer("done",0)
    }
    if (lastSaid():explode(" ")[1,string]=="!play") {
        hideChat(1)
        Song = lastSaid():explode(" ")[2,string]
        C = Tracks:count()
        I = 0
        Match = 0
        AllMatchs = ""
            while ( I != C ) {
            I++
            if ( Tracks[I,string]:find(Song)) {
                LastMatch = I
                AllMatchs = AllMatchs + ", " + Tracks[I,string]
                Match++
            }
            if ( I == (C - 1) ) {
                if ( !Match ) {
                    hint("No Matching Songs: " + Song,3000)
                }
                if ( Match == 1 ) {
                    soundPurge()
                    timer("done",0)
                    owner():soundPlay(1,0,Tracks[LastMatch,string])
                    soundVolume(1,Vol)
                    timer("done",Length[LastMatch,number])
                    hint("Now Playing: " + Tracks[LastMatch,string],1000)
                    print( _HUD_PRINTCONSOLE,"Now Playing: " + Tracks[LastMatch,string])
                }
                if ( Match > 1 ) {
                    hint("Multiply Matches, Check Console",3000)
                    print( _HUD_PRINTCONSOLE,AllMatchs)
                    
                }
            }
        }
    }
    if (lastSaid():explode(" ")[1,string]=="!vol") {
        hideChat(1)
        NewVol = lastSaid():explode(" ")[2,string]:toNumber()
        if ( inrange(Vol,0,1) ) {
            if ( NewVol <= 0 ) {
                Vol = 0
                soundVolume(1,Vol)
                hint("Music Muted",1000)
            }
            elseif ( NewVol >= 1 ) {
                Vol = 1
                soundVolume(1,Vol)
                hint("Volume At Max",1000)
            }
            else {
                Vol = NewVol
                soundVolume(1,Vol)
                hint("Volume At " + Vol:toString(),1000)
            }
        }
    }
}

#Set to play new song at end of last one
if (clk("done")) {
    Playing = 0
    soundPurge()
}

#Start new song
if (!Playing) {
    A = randint(Tracks:count())
    owner():soundPlay(1,0,Tracks[A,string])
    soundVolume(1,Vol)
    timer("done",Length[A,number])
    hint("Now Playing: " + Tracks[A,string],1000)
    print( _HUD_PRINTCONSOLE,"Now Playing: " + Tracks[A,string])
    Playing = 1
}

