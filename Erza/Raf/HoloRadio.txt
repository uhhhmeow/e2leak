@name Radio
@persist [I,H,D,D1,Alpha,Z,A,Q]:number [P,P1,P2,P3]:entity Deb:string Track:array
@model models/hunter/plates/plate025x05.mdl
#if(first()){selfDestructAll()}
runOnTick(1)
E=entity()

if(first()){
    E:setAlpha(0)
    Alpha=255
    Deb="models/debug/debugwhite"
    E:setMass(50000)
    E:propFreeze(0)
    A=0
    Z=1
    #print(_HUD_PRINTCENTER,"By Denz")
 
function entity holo(Parent:entity,Pos:vector,Ang:angle,Mdl:string,Scale:vector,Mat:string,Color:vector,Alpha,Shading,Shadow){
H++
holoCreate(H)
holoParent(H,Parent)
holoPos(H,Pos)
holoAng(H,Ang)
holoModel(H,Mdl)
holoScale(H,Scale)
holoMaterial(H,Mat)
holoColor(H,Color)
holoAlpha(H,Alpha)
holoDisableShading(H,!Shading)
holoShadow(H,Shadow)}
 
#BOX
holo(E,E:pos(),E:angles(),"",vec(0.85,2.6,0.34),Deb,vec(245,245,255),Alpha,1,1)
#POLOSKA
holo(holoEntity(1),holoEntity(1):toWorld(vec(-2.95,0,0)),holoEntity(1):toWorld(ang(0,0,0)),"",vec(0.02,2.61,0.341),Deb,vec(50,50,50),Alpha,1,1)
#3ADNAJA XUIN9
holo(holoEntity(1),holoEntity(1):toWorld(vec(0.7,-10,1.9)),holoEntity(1):toWorld(ang(0,0,0)),"models/holograms/hq_rcube_thin.mdl",vec(0.6,0.6,0.03),Deb,vec(255,250,250),0,1,1)
holo(holoEntity(1),holoEntity(1):toWorld(vec(0.7,10,1.9)),holoEntity(1):toWorld(ang(0,0,0)),"models/holograms/hq_rcube_thin.mdl",vec(0.6,0.6,0.03),Deb,vec(255,250,250),0,1,1)
#DERJALKI DLJA RU4KI
holo(holoEntity(1),holoEntity(1):toWorld(vec(-5.2,-8,-0.7)),holoEntity(1):toWorld(ang(0,0,90)),"models/holograms/hq_rcylinder.mdl",vec(0.05,0.03,0.02),Deb,vec(255,250,250),Alpha,1,1)
holo(holoEntity(1),holoEntity(1):toWorld(vec(-5.2,-8.5,-0.7)),holoEntity(1):toWorld(ang(0,0,90)),"models/holograms/hq_rcylinder.mdl",vec(0.05,0.03,0.02),Deb,vec(255,250,250),Alpha,1,1)
holo(holoEntity(1),holoEntity(1):toWorld(vec(-5.2,8,-0.7)),holoEntity(1):toWorld(ang(0,0,90)),"models/holograms/hq_rcylinder.mdl",vec(0.05,0.03,0.02),Deb,vec(255,250,250),Alpha,1,1)
holo(holoEntity(1),holoEntity(1):toWorld(vec(-5.2,8.5,-0.7)),holoEntity(1):toWorld(ang(0,0,90)),"models/holograms/hq_rcylinder.mdl",vec(0.05,0.03,0.02),Deb,vec(255,250,250),Alpha,1,1)
#DERJALKA
holo(holoEntity(1),holoEntity(1):toWorld(vec(-5.8,8.25,-0.45)),holoEntity(1):toWorld(ang(30,0,90)),"models/holograms/hq_cylinder.mdl",vec(0.13,0.03,0.02),Deb,vec(255,250,250),Alpha,1,1)
holo(holoEntity(1),holoEntity(1):toWorld(vec(-5.8,-8.25,-0.45)),holoEntity(1):toWorld(ang(30,0,90)),"models/holograms/hq_cylinder.mdl",vec(0.13,0.03,0.02),Deb,vec(255,250,250),Alpha,1,1)
#PALKA DERJALKI
holo(holoEntity(1),holoEntity(1):toWorld(vec(-6.3,0,-0.1)),holoEntity(1):toWorld(ang(30,0,90)),"models/holograms/hq_rcube_thin.mdl",vec(0.03,0.04,1.43),Deb,vec(255,250,250),Alpha,1,1)
holo(holoEntity(1),holoEntity(1):toWorld(vec(-6.2,0,-0.165)),holoEntity(1):toWorld(ang(30,0,90)),"models/holograms/hq_rcube_thin.mdl",vec(0.025,0.04,1),Deb,vec(70,70,70),Alpha,1,1)
#KOROB DLA KOLONOK
holo(holoEntity(1),holoEntity(1):toWorld(vec(0.7,10,-3)),holoEntity(1):toWorld(ang(0,0,0)),"models/hunter/misc/platehole4x4.mdl",vec(0.03,0.03,0.03),Deb,vec(255,250,250),Alpha,1,1)
holo(holoEntity(1),holoEntity(1):toWorld(vec(0.7,-10,-3)),holoEntity(1):toWorld(ang(0,0,0)),"models/hunter/misc/platehole4x4.mdl",vec(0.03,0.03,0.03),Deb,vec(255,250,250),Alpha,1,1)
holo(holoEntity(1),holoEntity(1):toWorld(vec(0.7,10,-2.44)),holoEntity(1):toWorld(ang(0,0,0)),"models/holograms/hq_stube_thin.mdl",vec(0.52,0.52,0.1),Deb,vec(255,250,250),Alpha,1,1)
holo(holoEntity(1),holoEntity(1):toWorld(vec(0.7,-10,-2.44)),holoEntity(1):toWorld(ang(0,0,0)),"models/holograms/hq_stube_thin.mdl",vec(0.52,0.52,0.1),Deb,vec(255,250,250),Alpha,1,1)
#KOLONKA  
holo(holoEntity(1),holoEntity(1):toWorld(vec(0.7,10,-1.9)),holoEntity(1):toWorld(ang(0,0,180)),"models/cheeze/wires/speaker.mdl",vec(0.49,0.49,0.3),"",vec(255,250,250),Alpha,1,1)
holo(holoEntity(1),holoEntity(1):toWorld(vec(0.7,-10,-1.9)),holoEntity(1):toWorld(ang(0,0,180)),"models/cheeze/wires/speaker.mdl",vec(0.49,0.49,0.3),"",vec(255,250,250),Alpha,1,1)
#Screen
holo(holoEntity(1),holoEntity(1):toWorld(vec(-0.6,0,-1.7)),holoEntity(1):toWorld(ang(0,0,0)),"",vec(0.3,0.70,0.1),Deb,vec(55,50,250),200,1,1)
#Kassets
holo(holoEntity(1),holoEntity(1):toWorld(vec(2.7,2.4,-1.7)),holoEntity(1):toWorld(ang(0,0,0)),"",vec(0.3/2,0.60/2,0.1),Deb,vec(55,50,50),200,1,1)
holo(holoEntity(1),holoEntity(1):toWorld(vec(2.7,-2.4,-1.7)),holoEntity(1):toWorld(ang(0,0,0)),"",vec(0.3/2,0.60/2,0.1),Deb,vec(55,50,50),200,1,1)
#KNOPKu
holo(holoEntity(1),holoEntity(1):toWorld(vec(-3.9,8,-1.7)),holoEntity(1):toWorld(ang(0,0,0)),"models/holograms/hq_rcylinder_thin.mdl",vec(0.14),Deb,vec(200,200,200),255,1,1)
holo(holoEntity(1),holoEntity(1):toWorld(vec(-3.9,8,-2)),holoEntity(1):toWorld(ang(0,0,0)),"models/holograms/hq_tube_thin.mdl",vec(0.15,0.15,0.02),Deb,vec(20,20,60),255,1,1)
holo(holoEntity(22),holoEntity(1):toWorld(vec(-3.9,7.3,-2.5)),holoEntity(1):toWorld(ang(0,0,0)),"",vec(0.01,0.02,0.02),Deb,vec(20,20,60),255,1,1)
#2
holo(holoEntity(1),holoEntity(1):toWorld(vec(-3.9,6,-1.7)),holoEntity(1):toWorld(ang(0,0,0)),"models/holograms/hq_rcylinder_thin.mdl",vec(0.14),Deb,vec(200,200,200),255,1,1)
holo(holoEntity(1),holoEntity(1):toWorld(vec(-3.9,6,-2)),holoEntity(1):toWorld(ang(0,0,0)),"models/holograms/hq_tube_thin.mdl",vec(0.15,0.15,0.02),Deb,vec(20,20,60),255,1,1)
holo(holoEntity(25),holoEntity(1):toWorld(vec(-3.9,6.7,-2.5)),holoEntity(1):toWorld(ang(0,0,0)),"",vec(0.01,0.02,0.02),Deb,vec(20,20,60),255,1,1)
#3
holo(holoEntity(1),holoEntity(1):toWorld(vec(-4.5,-5,-2)),holoEntity(1):toWorld(ang(0,0,0)),"models/holograms/hq_rcylinder_thin.mdl",vec(0.07),Deb,vec(200,200,200),255,1,1)
holo(holoEntity(1),holoEntity(1):toWorld(vec(-4.5,-5,-2)),holoEntity(1):toWorld(ang(0,0,0)),"models/holograms/hq_tube_thin.mdl",vec(0.08,0.08,0.02),Deb,vec(20,20,60),255,1,1)
#4
holo(holoEntity(1),holoEntity(1):toWorld(vec(-4.2,-6.3,-2)),holoEntity(1):toWorld(ang(0,0,0)),"models/holograms/hq_rcylinder_thin.mdl",vec(0.07),Deb,vec(200,200,200),255,1,1)
holo(holoEntity(1),holoEntity(1):toWorld(vec(-4.2,-6.3,-2)),holoEntity(1):toWorld(ang(0,0,0)),"models/holograms/hq_tube_thin.mdl",vec(0.08,0.08,0.02),Deb,vec(20,20,60),255,1,1)
#5
holo(holoEntity(1),holoEntity(1):toWorld(vec(-5.14,6,-1.7)),holoEntity(1):toWorld(ang(0,0,0)),"models/holograms/hq_rcube.mdl",vec(0.04,0.05,0.02),Deb,vec(200,200,200),255,1,1)
holo(holoEntity(1),holoEntity(1):toWorld(vec(-5.14,5,-1.7)),holoEntity(1):toWorld(ang(0,0,0)),"models/holograms/hq_rcube.mdl",vec(0.04,0.05,0.02),Deb,vec(200,200,200),255,1,1)
holo(holoEntity(1),holoEntity(1):toWorld(vec(-5.14,4,-1.7)),holoEntity(1):toWorld(ang(0,0,0)),"models/holograms/hq_rcube.mdl",vec(0.04,0.05,0.02),Deb,vec(200,200,200),255,1,1)
holo(holoEntity(1),holoEntity(1):toWorld(vec(-5.14,3,-1.7)),holoEntity(1):toWorld(ang(0,0,0)),"models/holograms/hq_rcube.mdl",vec(0.04,0.05,0.02),Deb,vec(200,200,200),255,1,1)
holo(holoEntity(1),holoEntity(1):toWorld(vec(-5.14,2,-1.7)),holoEntity(1):toWorld(ang(0,0,0)),"models/holograms/hq_rcube.mdl",vec(0.04,0.05,0.02),Deb,vec(200,200,200),255,1,1)
holo(holoEntity(1),holoEntity(1):toWorld(vec(-5.14,1,-1.7)),holoEntity(1):toWorld(ang(0,0,0)),"models/holograms/hq_rcube.mdl",vec(0.04,0.05,0.02),Deb,vec(200,200,200),255,1,1)
#NIJNIJA HUINJA
holo(holoEntity(1),holoEntity(1):toWorld(vec(4,11,0)),holoEntity(1):toWorld(ang(0,90,0)),"models/holograms/hq_rcube_thin.mdl",vec(0.4,0.2,0.2),Deb,vec(60,60,90),255,1,1)
holo(holoEntity(1),holoEntity(1):toWorld(vec(4,-11,0)),holoEntity(1):toWorld(ang(0,90,0)),"models/holograms/hq_rcube_thin.mdl",vec(0.4,0.2,0.2),Deb,vec(60,60,90),255,1,1)
#37 HOLO
 
#Propi dlja ypravlenija
P=propSpawn("models/hunter/plates/plate.mdl",holoEntity(1):toWorld(vec(-3.9,5.5,-2.4)),0)
P:setMass(200)
P:weldTo(E,1)
P:setAlpha(0)
P:noCollide(P1)
P:noCollide(E)
 
P1=propSpawn("models/hunter/plates/plate.mdl",holoEntity(1):toWorld(vec(-3.9,8.5,-2.4)),0)
P1:setMass(200)
P1:weldTo(E,1)
P1:setAlpha(0)
P1:noCollide(E)
 
P2=propSpawn("models/hunter/plates/plate.mdl",holoEntity(1):toWorld(vec(2.7,2.4,-2.4)),0)
P2:setMass(200)
P2:weldTo(E,1)
P2:setAlpha(0)
P2:noCollide(E)
P2:noCollide(P3)
P3:propFreeze(0)
 
P3=propSpawn("models/hunter/plates/plate.mdl",holoEntity(1):toWorld(vec(2.7,-2.4,-2.4)),0)
P3:setMass(200)
P3:weldTo(E,1)
P3:setAlpha(0)
P3:noCollide(E)
P3:propFreeze(0)
#P3=propSpawn("models/hunter/plates/plate.mdl",holoEntity(1):toWorld(vec(2.7,-2.4,-2.4)),0)
#P3:setMass(200)
#P3:weldTo(E,1)
#P3:setAlpha(255)
 
#StartUp
E:soundPlay(0,0.4,"friends/friend_join.wav")
 
#Track array
Track=array("http://pub7.di.fm:80/di_dubstep","http://pub1.di.fm:80/di_clubsounds",
"http://pub1.di.fm:80/di_drumandbass","http://pub1.di.fm:80/di_djmixes","http://pub1.di.fm:80/di_chiptunes",
"http://pub2.di.fm:80/di_liquiddubstep","http://pub1.di.fm:80/di_techno","http://pub1.di.fm:80/di_house",
"http://online.mytracklist.com/radio-50.htm")
 
#For holo emiter
for(Q=200,215){
    holoCreate(Q)
    holoPos(Q,E:toWorld(vec(-0.4,-103.8+Q*0.5,-0.4)))
    holoParent(Q,E)
    holoColor(Q,vec(100,randint(0,255),160))
    holoScale(Q,vec(0.2,0.04,0.3))}}
 
timer("C",70)
I++
 
if(clk("C")){
    
if(P3:inUse()){
    A++
    E:soundPlay(2,1,"friends/friend_join.wav")
    soundURLload(1,Track:string(A),1,0,entity())
    if(A==1){print("dubstep")},if(A==2){print("club")},if(A==3){print("drum'n'bass")},if(A==4){print("dj mix")},if(A==5){print("retro music")}
    if(A==6){print("liquid dubstep")},if(A==7){print("techno")},if(A==8){print("house")},if(A==9){print("idk")}}
   
if(P2:inUse()){
    A--
    E:soundPlay(2,1,"friends/friend_join.wav")
    soundURLload(1,Track:string(A),1,0,entity())
    if(A==1){print("dubstep")},if(A==2){print("club")},if(A==3){print("drum'n'bass")},if(A==4){print("dj mix")},if(A==5){print("retro music")}
    if(A==6){print("liquid dubstep")},if(A==7){print("techno")},if(A==8){print("house"),if(A==9){print("idk")}}}

if(P:inUse()){Z+=0.1,D++}
if(P1:inUse()){Z-=0.1,D1++}
 
if(Z==1.1){Z=1}
if(Z==-0.1){Z=0}
 
soundURLvolume(1,Z)
holoAng(22,E:toWorld(ang(0,60-D1*30,0)))
holoAng(25,E:toWorld(ang(0,D*60,0)))
 
if(A==10){A=0,soundURLdelete(1)}
if(A==-1){A=0,soundURLdelete(1)}
 
if(A!=0){
   
for(Q=200,215){holoScale(Q,vec(0.2+sin(I/6*Q^5)*0.05,0.04,0.3))}
 
###
holoScale(17,vec(0.49,0.49,0.21+sin(I*25)*0.05))
holoPos(17,holoEntity(1):toWorld(vec(0.7,10,-2-sin(I*30)*0.14)))
holoScale(18,vec(0.49,0.49,0.21+sin(I*25)*0.05))
holoPos(18,holoEntity(1):toWorld(vec(0.7,-10,-2-sin(I*30)*0.14)))}}
###
 
#if(duped()){selfDestructAll(),error("nope c:")}
 
if(owner():keyPressed("P")){selfDestructAll()}
