@name Moto_Cross
@inputs
@outputs RC RCW CorAng:angle A:angle V Speed:string
@persist EC:entity       PC:vector    CorAng:angle  CorPos:vector Siege:entity                    #Chassis,e2,selle,TabdeBord
@persist EG:entity                                           #Guidon,Haut_fourche
@persist EAv:entity   RAv   AAv:angle PAv:vector   RoueAv:entity                     #Roue,moyeux,Bas_fourche Avant
@persist EAr:entity   RAr  AAr:angle  PAr:vector   RoueAr:entity
@persist EMAr:entity   EMAv:entity   PEMAr:vector AMAr:angle AMAv:angle                        #Roue,moyeux Arriere
@persist Vitesse VChange V
 
@persist HoloIndex
 
@trigger
 
 
 
if(first()){
   
   
   
    Siege=entity():isWeldedTo()
    Siege:setMass(500)
    Siege:setAng(Siege:angles()-ang(0,0,Siege:angles():roll())-ang(0,0,26.56505118))
    Siege:setAlpha(150)
   
    Vitesse=1
    V=0
    VChange=0
   
    soundPlay(1,2370,"acf_engines/l6_special.wav")
    soundPitch(1,Vitesse*240/6)
    timer("Sound",2370)
   
   
    runOnKeys(Siege:driver(),1)
   
   
    function void createHolo(ModelHolo:string,HoloEntity:entity,PosHolo:vector,AngHolo:angle,ParentHolo:entity,ScaleHolo:vector,HoloAlpha:number,HoloMaterial:string,HoloColor:vector){
           
        holoCreate(HoloIndex)
        holoModel(HoloIndex,ModelHolo)
        holoPos(HoloIndex,HoloEntity:toWorld(PosHolo))
        holoAng(HoloIndex,HoloEntity:toWorld(AngHolo))
        holoScaleUnits(HoloIndex,ScaleHolo)
        holoParent(HoloIndex,ParentHolo)
        holoAlpha(HoloIndex,HoloAlpha)
        holoMaterial(HoloIndex,HoloMaterial)
        holoColor(HoloIndex,HoloColor)
           
        HoloIndex++
    }
   
   
   
   
   
   
    runOnTick(1)
   
    entity():setMass(0)
       
    rangerPersist(1)
    rangerHitWater(1)
    rangerFilter(Siege)
   
    rangerFilter(players())
    findExcludePlayer(owner())
   
   
           
    function createHolobrut(){
   
 
## CADRE    
        HoloIndex = 10
        createHolo("",Siege,vec(),ang(0,0,26.56505118),Siege,vec(),255,"",vec())
        EC = holoEntity(10)
       
        createHolo("hq_dome",EC,vec(0,10,0),ang(0,0,40),EC,vec(10,30,15),255,"phoenix_storms/plastic",vec())
        createHolo("hq_dome",EC,vec(0,10,0),ang(0,0,-140),EC,vec(10,30,40),255,"phoenix_storms/plastic",vec(255))
        createHolo("hq_dome",EC,vec(0,-10,0),ang(0,0,-20),EC,vec(10,55,23),255,"phoenix_storms/plastic",vec(0))
        createHolo("hq_dome",EC,vec(0,-10,0),ang(0,0,-200),EC,vec(10,55,13.5),255,"phoenix_storms/plastic",vec(255))
       
       
       
## Guidon        
        HoloIndex = 20
        createHolo("",EC,vec(0,25,15),ang(0,0,26.56505118),noentity(),vec(1),255,"",vec())
        EG = holoEntity(20)
       
        createHolo("hq_rcylinder_thin",EG,vec(0,0,0),ang(90,0,0),EG,vec(2,2,30),255,"phoenix_storms/plastic",vec(255))
       
        createHolo("hq_rcylinder_thin",EG,vec(5,0,-15),ang(0,0,0),EG,vec(4,4,30),255,"",vec(255))
        createHolo("hq_rcylinder_thin",EG,vec(-5,0,-15),ang(0,0,0),EG,vec(4,4,30),255,"",vec(255))
        createHolo("hq_rcylinder_thin",EG,vec(0,-5,-10),ang(0,0,0),EG,vec(3,3,10),255,"",vec(255))
 
        createHolo("hq_rcylinder_thin",EG,vec(-2.5,-2.5,-6.5),ang(0,45,90),EG,vec(3,3,7.07106),255,"",vec(255))
        createHolo("hq_rcylinder_thin",EG,vec(+2.5,-2.5,-6.5),ang(0,-45,90),EG,vec(3,3,7.07106),255,"",vec(255))
        createHolo("hq_rcylinder_thin",EG,vec(-2.5,-2.5,-13.5),ang(0,45,90),EG,vec(3,3,7.07106),255,"",vec(255))
        createHolo("hq_rcylinder_thin",EG,vec(+2.5,-2.5,-13.5),ang(0,-45,90),EG,vec(3,3,7.07106),255,"",vec(255))
   
       
       
## ROUE AVANT        
        HoloIndex = 30
        createHolo("",EC,vec(0,45,-25),ang(),noentity(),vec(1),255,"",vec())
        EAv = holoEntity(30)
               
        createHolo("hq_torus",EAv,vec(0,0,0),ang(90,0,0),EAv,vec(32.5,32.5,35),255,"phoenix_storms/plastic",vec(50))
       
        createHolo("hq_rcylinder_thin",EAv,vec(0,0,0),ang(90,0,0),EAv,vec(7.5,7.5,3),255,"",vec(255))
        createHolo("hq_rcylinder_thin",EAv,vec(5,0,10),ang(0,0,0),EAv,vec(2,2,22.35),255,"",vec(255))
        createHolo("hq_rcylinder_thin",EAv,vec(-5,0,10),ang(0,0,0),EAv,vec(2,2,22.35),255,"",vec(255))
           
        createHolo("hq_cylinder",EAv,vec(0,0,0),ang(90,0,0),EAv,vec(3,3,10),255,"",vec(255))
           
        createHolo("hq_rcylinder",EAv,vec(0,0,0),ang(0,0,90),EAv,vec(0.5,0.5,25),255,"",vec(255))
        createHolo("hq_rcylinder",EAv,vec(0,0,0),ang(0,0,45),EAv,vec(0.5,0.5,25),255,"",vec(255))
        createHolo("hq_rcylinder",EAv,vec(0,0,0),ang(0,0,0),EAv,vec(0.5,0.5,25),255,"",vec(255))
        createHolo("hq_rcylinder",EAv,vec(0,0,0),ang(0,0,-45),EAv,vec(0.5,0.5,25),255,"",vec(255))
       
        createHolo("hq_tube_thin",EAv,vec(0,0,0),ang(90,0,0),EAv,vec(25,25,4),255,"",vec(100))
           
 
 
## ROUE ARRIERE        
 
        HoloIndex = 50
        createHolo("",EC,vec(0,0,-15),ang(0,0,26.9258),noentity(),vec(1),255,"",vec())
        EAr = holoEntity(50)
           
        createHolo("hq_rcylinder_thin",EAr,vec(0,-25,0),ang(90,0,0),EAr,vec(7.5,7.5,3),255,"",vec(255))
           
        createHolo("hq_torus",EAr,vec(0,-25,0),ang(90,0,0),EAr,vec(32.5,32.5,35),255,"phoenix_storms/plastic",vec(50))
           
        createHolo("hq_cylinder",EAr,vec(5,-10,0),ang(0,0,90),EAr,vec(2.5,6.5,30),255,"",vec(255))
        createHolo("hq_cylinder",EAr,vec(-5,-10,0),ang(0,0,90),EAr,vec(2.5,6.5,30),255,"",vec(255))
       
        createHolo("hq_cylinder",EAr,vec(0,-25,0),ang(90,0,0),EAr,vec(3,3,10),255,"",vec(255))
       
        createHolo("hq_rcylinder",EAr,vec(0,-25,0),ang(0,0,90),EAr,vec(0.5,0.5,25),255,"",vec(255))
        createHolo("hq_rcylinder",EAr,vec(0,-25,0),ang(0,0,45),EAr,vec(0.5,0.5,25),255,"",vec(255))
        createHolo("hq_rcylinder",EAr,vec(0,-25,0),ang(0,0,0),EAr,vec(0.5,0.5,25),255,"",vec(255))
        createHolo("hq_rcylinder",EAr,vec(0,-25,0),ang(0,0,-45),EAr,vec(0.5,0.5,25),255,"",vec(255))
       
        createHolo("hq_tube_thin",EAr,vec(0,-25,0),ang(90,0,0),EAr,vec(25,25,4),255,"",vec(100))
   
   
   
##Vitesse
        HoloIndex=70
   
        createHolo("models/sprops/misc/alphanum/alphanum_1.mdl",EG,vec(-12.5,5,2.5),ang(0,180,30),EG,vec(4,1,4),255,"",vec(255))
       
        createHolo("models/sprops/misc/alphanum/alphanum_0.mdl",EG,vec(7.5,5,2.55),ang(0,180,30),EG,vec(4,1,4),255,"",vec(255))
        createHolo("models/sprops/misc/alphanum/alphanum_0.mdl",EG,vec(10,5,2.5),ang(0,180,30),EG,vec(4,1,4),255,"",vec(255))
        createHolo("models/sprops/misc/alphanum/alphanum_0.mdl",EG,vec(12.5,5,2.5),ang(0,180,30),EG,vec(4,1,4),255,"",vec(255))
    }
   
    timer("CREATE",100)
 
}
   
 
if(clk("CREATE")){createHolobrut()}
 
 
 
RC=rangerOffset(10000,EC:pos(),EC:toWorld(vec(0,0,-1))-EC:pos()):distance()
RCW=rangerOffset(10000,EC:pos(),vec(0,0,-1)):distance()
RAv=rangerOffset(10000,EG:toWorld(vec(0,0,-sqrt(40*40+20*20))),vec(0,0,-1)):distance()
RAr=rangerOffset(10000,EAr:toWorld(vec(0,-sqrt(25*25+100),0)),vec(0,0,-1)):distance()
 
 
    F=vec(0,0,0)
    A=ang(0,0,0)
   
    PC=vec(0,0,40)-vec(0,0,RC)
    CorPos=EC:pos()
   
   
    if(clk("Sound")){
        soundPlay(1,2370,"acf_engines/l6_special.wav")
        timer("Sound",2370)
       
    }
   
    soundPitch(1,V)
   
   
   
   
    if(Siege:driver():keySprint()&!VChange&Vitesse<6){
        Vitesse++
       
        holoModel(70,"models/sprops/misc/alphanum/alphanum_"+toString(Vitesse)+".mdl")
           
        }
       
    if(Siege:driver():keyJump()&!VChange&Vitesse>1){
        Vitesse--
        holoModel(70,"models/sprops/misc/alphanum/alphanum_"+toString(Vitesse)+".mdl")
 
    }
   
    if(Siege:driver():keySprint()|Siege:driver():keyJump()){
        VChange=1
        timer("VChange",100)      
    }
    elseif(clk("VChange")){VChange=0}
   
 
       
       
       
       
        if(V&Siege:driver():keyLeft()&EC:angles():pitch()>-45){        
            A=A+ang(-sin(26.565)*10,cos(26.565)*5,0)
            holoAng(20,EC:toWorld(ang(0,40-40*V/240,26.56505118)))
           # EC:angles():pitch()<15 ? A=A :
        }
        elseif(V&Siege:driver():keyRight()&EC:angles():pitch()<45){    
            A=A+ang(sin(26.565)*10,-cos(26.565)*5,0)
           
            holoAng(20,EC:toWorld(ang(0,-40+40*V/240,26.56505118)))
           
             }
        else{
            A=A-ang(Siege:angles():pitch(),0,0)
            holoAng(20,EC:toWorld(ang(0,0,26.56505118)))
        }
       
        holoPos(20,EC:toWorld(vec(0,25+V*0.1,15)))
       
           
           
       
       
       
        if(RC<40){
            F=F+(PC+$PC*5-vec($CorPos:x()*5,$CorPos:y()*5,0))*Siege:mass()*5
        }
   
   
   
        if(V<Vitesse*240/6&Siege:driver():keyForward()){
            V+=((40+V)/(40*Vitesse))^2
               
        }
        elseif(V>0){
            V--
           
           
        }
        if(V>-240/6&Siege:driver():keyBack()){
            V--
        }
        elseif(V<0){
            V++
           
        }
   
   
    if(RAr<=15){
        F=F+EC:toWorld(vec(0,50000*V/40,0))-EC:pos()
    }
       
    Siege:applyForce(F)
   
    A=A+ang(0,0,(asin((RAr-RAv)/(holoEntity(51):pos()-holoEntity(31):pos()):length())))
   
    TarQ=quat(Siege:toWorld(A))
    CurQ=quat(Siege)
    C = Siege:toLocal(rotationVector(TarQ/CurQ)+Siege:pos())
    Siege:applyTorque((500*C - 30*Siege:angVelVector())*Siege:inertia())
 
 
 
 
 
#holoAng(10,Siege:angles():pitch(),Siege:angles:yaw(),asinr((RAv-RAr)**2/70))
 
 
 
 
holoAng(30,EG:angles())
if(RAv<15){
holoPos(30,EG:toWorld(vec(0,0,-sqrt(40*40+20*20)-RAv*cos(26.56505118)+15*cos(26.56505118))))
}
else{
    holoPos(30,EG:toWorld(vec(0,0,-sqrt(40*40+20*20))))
}
 
holoPos(50,EC:toWorld(vec(0,0.1*V,-15)))
if(RAr<15){
    holoAng(50,EC:toWorld(ang(0,0,26.9258-(15-RAr)*26.9258/15)))
}
else{
    holoAng(50,EC:toWorld(ang(0,0,26.9258)))
}
 
 
Speed=toString(round(convertUnit("u/s","km/h",Siege:vel():length())))
if(Speed:length()==1){Speed="0"+Speed}
if(Speed:length()==2){Speed="0"+Speed}
holoModel(71,"models/sprops/misc/alphanum/alphanum_"+Speed:index(1)+".mdl")
holoModel(72,"models/sprops/misc/alphanum/alphanum_"+Speed:index(2)+".mdl")
holoModel(73,"models/sprops/misc/alphanum/alphanum_"+Speed:index(3)+".mdl")