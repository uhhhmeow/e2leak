@name Turret Sequencer
@inputs Button TTarget:entity
@outputs  Active Target:entity
@persist Array:array X
@trigger 
interval(50)
if(first()){
    Active = 1
    findIncludePlayerProps(owner())
    findByClass("sc_weapon_base")
    local Weapons = findToArray()
    
    foreach(K,V:entity = Weapons)
    {
        Array[Array:count()+1,wirelink] = V:wirelink()
        V:createWire(entity(),"Target","Target")
        V:createWire(entity(),"Active","Active")
        V:setPos(entity():toWorld(vec(0,0,10)))
        V:setAng(entity():angles()+ang(0,0,0))
        V:weld(entity():isWeldedTo())
    }
    
    X = 1
}
Target = TTarget


Array[X,wirelink]["Fire",number] = Button
if(X==1){
    Array[Array:count(),wirelink]["Fire",number] = Button
}else{
    Array[X-1,wirelink]["Fire",number] = Button
}
X = X + 1
if(X == Array:count()+1){X = 1}
