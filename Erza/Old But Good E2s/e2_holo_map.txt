@name E2 Holo Map
@inputs [Ent1 Ent2]:entity [Egp Asguard]:wirelink
@outputs Map:string PA:array Close:entity [BeamTar BeamOrigin]:vector
@outputs Target:entity Target2:entity [Point1 Point2]:vector R S:entity II1
@outputs GrobCount PCOUNT HCOUNT GCOUNT OLCOUNT CORECOUNT ShieldCount IceRoidCount StargateCount EntCount EntCount2 Num I3 I4
@persist MA Num Draw [Ar Ar2 ColorA GA]:array I3 I4 I5 I6 I2 Offs Blink TargetPos:vector
@persist R Target:entity Scale PrintCol TeamColorOn ShowHDrive ShieldsOn
@persist ShowGPod Ping MinerPos E2LocOn ShowCredit GroundOn I20 ShieldCount
@persist PCOUNT HCOUNT GCOUNT OLCOUNT CORECOUNT StargateCount IceRoidCount 
@persist GrobCount EntCount Ent:entity ShowCore Targeting EgpOn IceOn Stargate
@persist BackGround Grobs SphereOn FactionsOn [TeleA TeleR]:array
@persist II1 TeleSystem Target2:entity EntCount2 SingCount
@trigger 

interval(10)
runOnChat(1)

timer("TerrainReset",5000)
if(first()){
    stoptimer("TerrainReset")
timer("TerrainReset",1000)
E2LocOn = 1
Ent1On = 0
Ent2On = 0
MapXyzOn = 0
Scale = 1  #Less than 1 will make map bigger while larger numbers will make it smaller
TeamColorOn = 0 #E2 Gets Player Colors From Scoreboard, otherwize, random colors
ShowCredit = 0 #only works with spaceage, also have to edit some code at bottem
Ping = 0
FactionsOn = 1 #Teamnames
MinerPos = 0 #another spaceage thing
ShowHDrive = 1 #hovordrives
ShowGPod = 1  # Gyropods
ShowOreLaser = 0 #Spaceage Ore Lasers
ShowCore = 1 #Cores
Targeting = 0
EgpOn = 1 #The Egp Extra
Stargate = 0 #stargates (durr)
GroundOn = 0
SphereOn = 1
Grobs = 0 #spaceage
Singularity = 0 #Spaceage, dont think i ever made it work
ShieldsOn = 0
BackGround = 0 #broken
IceOn = 0 #not spaceage ice
Krystallos = 0
DarkGlitter = 0
Gelidus = 0
}

#Gvars
CurPos =gGetVec("CurPos")
TarPos =gGetVec("TarPos")


timer("Blink",1000)

if(clk("Blink")) {Blink++} 
if(Blink > 1){Blink = 0}

Test = Ent1:pos():distance(Ent2:pos())/100
E1Pos = mix(Ent1:pos(),Ent2:pos(),0.5)

if(first()){I4 = 20, I2 = 1}
Map = map()
if(Map == "gm_flatgrass_airport"){MA = 1, Offs = 200/Scale}
if(Map == "gm_flatgrass"){MA = 2,Offs = 0/Scale}
if(Map == "sb_forlorn_sb3_r2l"){MA = 3,Offs = 200/Scale}
if(Map == "sb_gooniverse"){MA = 4,Offs = 200/Scale}
if(Map == "sb_wuwgalaxy_fix"){MA = 5,Offs = 200/Scale}
if(Map == "sb_astria_beta01"){MA = 6,Offs = 200/Scale}
if(Map == "sb_omen"){MA = 7,Offs = 200/Scale}
Base = entity():pos()+vec(0,0,Offs)



#lokis stuff


#  S = Sphere entity/holo
#  O = the player
#  Sc = Spheres position
#  P1 = the players shoot pos, or head, start of line segment
#  P2 = the players aim pos, end of line segment.
#  Di = Distance between the player shootpos and the sphere
#  R  = the radius of the sphere

#Close = owner()


if(PA[I3,entity]:pos():distance(Base) <300){
Close = PA[I3,entity]}


if(TeleSystem)
{
S = TeleA[II1,entity]
P = Close
R = TeleR[II1,number]/Scale


Sc = S:pos()
P1 = P:shootPos()
P2 = P:aimPos()
Di = P1 - Sc 

V = P2 - P1

A = (V:x()^2) + (V:y()^2) + (V:z()^2)

B = 2 * (V:x()*Di:x() + V:y()*Di:y() + V:z()*Di:z())

C = (Sc:x()^2) + (Sc:y()^2) + (Sc:z()^2)
C+= (P1:x()^2) + (P1:y()^2) + (P1:z()^2)
C-= 2 * (Sc:x()*P1:x() + Sc:y()*P1:y() + Sc:z()*P1:z())
C-= (R^2)

BB4AC = (B*B-4*A*C)
True = BB4AC >= 0

if (True)
{
  # first intersection
  Point1 = P1 + ((-B -sqrt(BB4AC)) / (2 * A))*(P2-P1)

  # second intersection
  Point2 = P1 + ((-B +sqrt(BB4AC)) / (2 * A))*(P2-P1)
}

if(TeleA[II1,entity]:pos():distance(Point1)<R+1 &
TeleA[II1,entity]:pos():distance(Point2)<R+1)
{Target = TeleA[II1,entity]}
#else{Target = noentity()}

II1++

if(II1 > TeleA:count()){II1 = 0}

if(!Target2){
for(II2 = 1,TeleA:count()){
TeleA[II2,entity]:setColor(vec(255,255,255))}
}

if(Target & !Target2){Target:setColor(vec(0,0,0))}

if(Close:keyUse()){
  #printColor(vec(0,200,0),"Selection Made")  
    Target2 = Target}

}



if(Targeting)
{

S = Ar2[I3,entity]
P =owner()
R = 1

Sc = S:pos()
P1 = P:shootPos()
P2 = P:aimPos()
Di = P1 - Sc 

V = P2 - P1

A = (V:x()^2) + (V:y()^2) + (V:z()^2)

B = 2 * (V:x()*Di:x() + V:y()*Di:y() + V:z()*Di:z())

C = (Sc:x()^2) + (Sc:y()^2) + (Sc:z()^2)
C+= (P1:x()^2) + (P1:y()^2) + (P1:z()^2)
C-= 2 * (Sc:x()*P1:x() + Sc:y()*P1:y() + Sc:z()*P1:z())
C-= (R^2)

BB4AC = (B*B-4*A*C)
True = BB4AC >= 0

if (True)
{
  # first intersection
  Point1 = P1 + ((-B -sqrt(BB4AC)) / (2 * A))*(P2-P1)

  # second intersection
  Point2 = P1 + ((-B +sqrt(BB4AC)) / (2 * A))*(P2-P1)
}
if(owner():keyUse()){Target = Ent}
}




#chat cmds
if(chatClk(owner())){
   Lastsaid = owner():lastSaid()
Msg = Lastsaid:explode(" ")
if(Msg:string(1) == "reset"){
    printColor(vec(0,200,0),"Reseting Expression2")
reset()    }

if(Msg:string(1) == "colors"){
    printColor(vec(0,200,0),"Printing Colors")
for(I7 = 1,PCOUNT){
    
printColor(ColorA[I7,vector],""+PA[I7,entity]:name())}
    
    }
}
#all cmds
if(chatClk()){
   Lastsaid = lastSpoke():lastSaid()
Msg = Lastsaid:explode(" ")
if(Msg:string(1) == "clear"){
    printColor(vec(0,200,0),"Clearing Selection")
 Target2 = noentity()}
if(Msg:string(1) == "beam"){
    printColor(vec(0,200,0),"Activating Selection System")
TeleSystem = 1
}
if(Msg:string(1) == "beamme"){
    
if(Msg:string(2) == "earth"){Sel = 1,timer("send",10)}
if(Msg:string(2) == "lava"){Sel = 2,timer("send",10)}
if(Msg:string(2) == "desert"){Sel = 3,timer("send",10)}
if(Msg:string(2) == "toxic"){Sel = 4,timer("send",10)}
if(Msg:string(2) == "ice"){Sel = 5,timer("send",10)}
if(Msg:string(2) == "icecaves"){Sel = 6,timer("send",10)}
if(Msg:string(2) == "moon"){Sel = 7,timer("send",10)}

BeamTar = (((TeleA[Sel,entity]:pos()-Base)*Scale)*100)+vec(0,0,500)
BeamOrigin = Close:pos()
}

if(Msg:string(1) == "confirm" & Target2){
    printColor(vec(0,200,0),"Beaming Person")
 BeamTar = (((Target:pos()-Base)*Scale)*100)+vec(0,0,500)
BeamOrigin = Close:pos()
Target2 = noentity()
TeleSystem = 0
for(II2 = 1,TeleA:count()){
TeleA[II2,entity]:setColor(vec(255,255,255))}

timer("send",10)
}
if(Msg:string(1) == "!return"){
    printColor(vec(0,200,0),"Beaming To Map")
 BeamTar = Base
BeamOrigin = lastSpoke():pos()
timer("send",10)
}
}


EE = entity()

if(MA == 1){
if(first()){ 
    holoCreate(100)
     holoCreate(110)
     holoCreate(120)
     holoCreate(130)
     holoCreate(140)
}
if(clk("TerrainReset"))  {
    holoPos(100,Base+(vec(0,550,-4367)/100)/Scale)
    holoScaleUnits(100,vec(290,290,1)/Scale)
    holoModel(100,"hqcylinder2")
    holoColor(100,vec4(0,100,0,150))
    
    
    holoPos(110,Base+(vec(0,550,-4367)/100)/Scale)
    holoScaleUnits(110,vec(90,90,2)/Scale)
    holoModel(110,"hqcylinder2")
    holoColor(110,vec4(0,0,0,200))
    
    holoPos(120,Base+(vec(0,550,-13580)/100)/Scale)
    holoScaleUnits(120,vec(90,90,56)/Scale)
    holoModel(120,"hqcylinder2")
    holoColor(120,vec4(100,100,100,255))

    holoPos(130,Base+(vec(0,80,-13580)/100)/Scale)
    holoScaleUnits(130,vec(340,340,55)/Scale)
    holoModel(130,"cube")
    holoColor(130,vec4(0,0,150,150))
    
    holoPos(140,Base+(vec(0,80,-80)/100)/Scale)
    holoScaleUnits(140,vec(340,340,340)/Scale)
    holoModel(140,"cube")
    holoColor(140,vec4(100,100,100,150))
holoParent(100,EE)
holoParent(110,EE)
holoParent(120,EE)
holoParent(130,EE)
holoParent(140,EE)

}
}
if(MA == 2){
if(first()){ 
    holoCreate(100)
    holoCreate(101)
     holoCreate(110)
}
if(clk("TerrainReset"))  {
    holoPos(100,Base+(vec(-1600,-1600,-100)/100)/Scale)
    holoScaleUnits(100,vec(280,280,2)/Scale)
    holoModel(100,"cube")
    holoColor(100,vec4(0,100,0,255))
#    holoMaterial(100,"models/props_foliage/oak_tree01")
    
    holoPos(110,Base+(vec(1020,0,-50)/100)/Scale)
    holoScaleUnits(110,vec(5,10,2)/Scale)
    holoModel(110,"cube")
    holoColor(110,vec4(255,255,255,255))
    holoMaterial(110,"phoenix_storms/plastic")
    
        holoPos(101,Base)
    holoScaleUnits(101,vec(1,1,1)/Scale)
    holoModel(101,"sphere")
    holoColor(101,vec4(255,255,255,255))
    
    holoParent(100,EE)
holoParent(110,EE)
holoParent(101,EE)


}
}

if(MA == 3){
if(first()){ 
    holoCreate(100)
    holoCreate(110)    
    holoCreate(120)
    holoCreate(101)
    holoCreate(102)
    holoCreate(103)

}
if(clk("TerrainReset"))  {
    holoPos(100,Base+(vec(9334,9878.5,843.5)/100)/Scale)
    holoScaleUnits(100,vec(50,50,50)/Scale)
    holoModel(100,"hqicosphere2")
    holoColor(100,vec4(200,200,200,255))
    holoMaterial(100,"models/wireframe")

    holoPos(110,Base+(vec(8564,-7737,-9231)/100)/Scale)
    holoScaleUnits(110,vec(100,100,100)/Scale)
    holoModel(110,"hqicosphere2")
    holoColor(110,vec4(0,150,0,255))
    holoMaterial(110,"models/wireframe")
    
        holoPos(120,Base+(vec(9082.4,9305.5,-8872)/100)/Scale)
    holoScaleUnits(120,vec(80,80,80)/Scale)
    holoModel(120,"hqicosphere2")
    holoColor(120,vec4(0,100,0,255))
    holoMaterial(120,"models/wireframe")
    
        holoPos(101,Base+(vec(5801,-4072,6568.1)/100)/Scale)
    holoScaleUnits(101,vec(35,35,35)/Scale)
    holoModel(101,"hqicosphere2")
    holoColor(101,vec4(0,0,200,255))
    holoMaterial(101,"models/wireframe")
    
            holoPos(102,Base+(vec(-8184.087,8962.654,7088.5)/100)/Scale)
    holoScaleUnits(102,vec(60,60,60)/Scale)
    holoModel(102,"hqicosphere2")
    holoColor(102,vec4(0,200,0,255))
    holoMaterial(102,"models/wireframe")
    
    
    holoPos(103,Base+(vec(-8815.50,-9146.049,220)/100)/Scale)
    holoScaleUnits(103,vec(60,60,60)/Scale)
    holoModel(103,"hqicosphere2")
    holoColor(103,vec4(150,0,0,255))
    holoMaterial(103,"models/wireframe")
    
    holoParent(100,EE)
holoParent(110,EE)
holoParent(120,EE)
holoParent(101,EE)
holoParent(102,EE)
holoParent(103,EE)

}
}

if(MA == 4){
if(first()){ 
    holoCreate(100)
    holoCreate(110)    
    holoCreate(120)
    holoCreate(101)
    holoCreate(102)
    holoCreate(103)
    holoCreate(104)

}
if(clk("TerrainReset"))  {
    #term
    holoPos(100,Base+(vec(3.4,-0.7,4608)/100)/Scale)
    holoScaleUnits(100,vec(22,22,22)/Scale)
    holoModel(100,"hqicosphere2")
    holoColor(100,vec4(200,200,200,255))
    holoMaterial(100,"models/wireframe")

#spawn
    holoPos(110,Base+(vec(-9734.98,-6161.99,-8189.96)/100)/Scale)
    holoScaleUnits(110,vec(100,100,100)/Scale)
    holoModel(110,"hqicosphere2")
    holoColor(110,vec4(0,150,0,255))
    holoMaterial(110,"models/wireframe")
#tiber  
     holoPos(120,Base+(vec(8154.55,-10129.47,-1939.29)/100)/Scale)
    holoScaleUnits(120,vec(90,90,90)/Scale)
    holoModel(120,"hqicosphere2")
    holoColor(120,vec4(100,100,0,255))
    holoMaterial(120,"models/wireframe")
  #other habitable one  
        holoPos(101,Base+(vec(9217.09,8858,4586.58)/100)/Scale)
    holoScaleUnits(101,vec(80,80,80)/Scale)
    holoModel(101,"hqicosphere2")
    holoColor(101,vec4(0,200,50,255))
    holoMaterial(101,"models/wireframe")
    #Saturn
    holoPos(102,Base+(vec(-8195.45,8707.8,10117.5)/100)/Scale)
    holoScaleUnits(102,vec(70,70,70)/Scale)
    holoModel(102,"hqicosphere2")
    holoColor(102,vec4(100,100,0,255))
    holoMaterial(102,"models/wireframe")
   #Saturn rings    
    holoPos(103,Base+(vec(-8195.45,8707.8,10117.5)/100)/Scale)
    holoScaleUnits(103,vec(100,100,6)/Scale)
    holoModel(103,"hqtorus2")
    holoColor(103,vec4(150,150,150,255))
    holoMaterial(103,"models/wireframe")
    holoAng(103,ang(15,30,5))
   #lava planet   
    holoPos(104,Base+(vec(1537.92,7686.27,-10236.98)/100)/Scale)
    holoScaleUnits(104,vec(60,60,60)/Scale)
    holoModel(104,"hqicosphere2")
    holoColor(104,vec4(150,0,0,255))
    holoMaterial(104,"models/wireframe")

    holoParent(100,EE)
holoParent(110,EE)
holoParent(120,EE)
holoParent(101,EE)
holoParent(102,EE)
holoParent(103,EE)
holoParent(104,EE)
}
}

if(MA == 5){
if(first()){ 
    holoCreate(100)
    holoCreate(110)    
    holoCreate(120)
    holoCreate(101)
    holoCreate(102)
    holoCreate(103)
    holoCreate(104)
    holoCreate(105)
    holoCreate(106)
    holoCreate(107)
    

}
if(clk("TerrainReset"))  {
    #asteroid base
    holoPos(100,Base+(vec(-8550.27,-9994.79,-10722.65)/100)/Scale)
    holoScaleUnits(100,vec(85,55,85)/Scale)
    holoModel(100,"hqicosphere2")
    holoColor(100,vec4(200,200,200,255))
    holoMaterial(100,"models/wireframe")

#spawn room
    holoPos(110,Base+(vec(-73.85,-212.55,16134.48)/100)/Scale)
    holoScaleUnits(110,vec(8,20,5)/Scale)
    holoModel(110,"cube")
    holoColor(110,vec4(150,150,150,255))
    holoMaterial(110,"models/wireframe")
#shakuras
     holoPos(120,Base+(vec(9759.32,-7731.16,-10125)/100)/Scale)
    holoScaleUnits(120,vec(100,100,100)/Scale)
    holoModel(120,"hqicosphere2")
    holoColor(120,vec4(0,100,0,255))
    holoMaterial(120,"models/wireframe")
  #kobol  
    holoPos(101,Base+(vec(-9179.32,9211.63,-950)/100)/Scale)
    holoScaleUnits(101,vec(80,80,80)/Scale)
    holoModel(101,"hqicosphere2")
    holoColor(101,vec4(0,200,50,255))
    holoMaterial(101,"models/wireframe")
    #vimana
    holoPos(102,Base+(vec(-8816.55,-9138.35,220)/100)/Scale)
    holoScaleUnits(102,vec(60,60,60)/Scale)
    holoModel(102,"hqicosphere2")
    holoColor(102,vec4(200,0,0,255))
    holoMaterial(102,"models/wireframe")
   #magmite    
    holoPos(103,Base+(vec(-6260.48,-2769.72,9846)/100)/Scale)
    holoScaleUnits(103,vec(60,60,60)/Scale)
    holoModel(103,"hqicosphere2")
    holoColor(103,vec4(50,0,0,255))
    holoMaterial(103,"models/wireframe")
    holoAng(103,ang(15,30,5))
   #shoosh   
    holoPos(104,Base+(vec(11811.65,12558.59,12433.41)/100)/Scale)
    holoScaleUnits(104,vec(40,40,40)/Scale)
    holoModel(104,"hqicosphere2")
    holoColor(104,vec4(0,0,200,255))
    holoMaterial(104,"models/wireframe")
    
       #Endgame  
    holoPos(105,Base+(vec(1545.28,7674.64,-10237.39)/100)/Scale)
    holoScaleUnits(105,vec(60,60,60)/Scale)
    holoModel(105,"hqicosphere2")
    holoColor(105,vec4(100,0,0,255))
    holoMaterial(105,"models/wireframe")

       #space station 
    holoPos(107,Base+(vec(8399.11,5700.85,7231.54)/100)/Scale)
    holoScaleUnits(107,vec(16,20,6)/Scale)
    holoModel(107,"cube")
    holoColor(107,vec4(150,150,150,255))
    holoMaterial(107,"models/wireframe")

       #Sun 
    holoPos(106,Base+(vec(-13500,-13500,13500)/100)/Scale)
    holoScaleUnits(106,vec(30,30,30)/Scale)
    holoModel(106,"hqicosphere2")
    holoColor(106,vec4(255,255,0,255))
    holoMaterial(106,"models/wireframe")

    holoParent(100,EE)
holoParent(110,EE)
holoParent(120,EE)
holoParent(101,EE)
holoParent(102,EE)
holoParent(103,EE)
holoParent(104,EE)
holoParent(105,EE)
holoParent(106,EE)
holoParent(107,EE)
}
}

if(MA == 6){
if(first()){ 
    if(SphereOn){
    holoCreate(100)
    holoCreate(110)    
    holoCreate(120)
    holoCreate(101)
    holoCreate(102)
    holoCreate(103)
    holoCreate(104)
holoCreate(105)}
  if(GroundOn){  
    holoCreate(106)
    holoCreate(107)
    holoCreate(108)
    holoCreate(109)
    holoCreate(111) 
    holoCreate(112)
holoCreate(113) }
    
if(BackGround)
{
    holoCreate(114)
    holoPos(114,Base+(vec(0,0,0)/100)/Scale)
    holoScaleUnits(114,vec(-305,-305,-384)/Scale)
    holoModel(114,"cube")
    holoColor(114,vec4(150,150,150,255))  
    holoParent(114,EE) 
} 

}



if(clk("TerrainReset"))  {
   A1 = 255
A2 = 255

if(SphereOn){
 #spawn
    holoPos(100,Base+(vec(-2.35,9216.58,-3072)/100)/Scale)
    holoScaleUnits(100,vec(-82,-82,-82)/Scale)
    holoModel(100,"hqicosphere2")
    holoColor(100,vec4(255,255,255,A1))
    holoMaterial(100,"Zup/planets/earthlike_001")
    
    TeleA[1,entity] = holoEntity(100)
    TeleR[1,number] =  41

#spawn room
    holoPos(110,Base+(vec(1908.57,9216.35,-3937.37)/100)/Scale)
    holoScaleUnits(110,vec(-12.5,-4,-1.3)/Scale)
    holoModel(110,"cube")
    holoColor(110,vec4(200,200,200,255))
    #holoMaterial(110,"models/wireframe")
#Lava Planet
     holoPos(120,Base+(vec(9208.7,3070.82,10249.38)/100)/Scale)
    holoScaleUnits(120,vec(-61,-61,-61)/Scale)
    holoModel(120,"hqicosphere2")
    holoColor(120,vec4(250,0,0,A1))
    holoMaterial(120,"Zup/planets/lava_001")
    TeleA[2,entity] = holoEntity(120)
    TeleR[2,number] =  30
    
  #desert 
    holoPos(101,Base+(vec(9220.68,-9216.89,2058.91)/100)/Scale)
    holoScaleUnits(101,vec(-92,-92,-92)/Scale)
    holoModel(101,"hqicosphere2")
    holoColor(101,vec4(255,255,255,A1))
    holoMaterial(101,"Zup/planets/desert_001")
    TeleA[3,entity] = holoEntity(101)
    TeleR[3,number] =  46
    
    #Toxic
    holoPos(102,Base+(vec(-9216.11,-9215.66,-504.95)/100)/Scale)
    holoScaleUnits(102,vec(-82,-82,-82)/Scale)
    holoModel(102,"hqicosphere2")
    holoColor(102,vec4(255,255,255,A1))
    holoMaterial(102,"Zup/planets/swamp_001")
    TeleA[4,entity] = holoEntity(102)
    TeleR[4,number] =  41
    
   #Normal Ice  
    holoPos(103,Base+(vec(0.94,-4091.87,-8190.4)/100)/Scale)
    holoScaleUnits(103,vec(-61,-61,-61)/Scale)
    holoModel(103,"hqicosphere2")
    holoColor(103,vec4(255,255,255,A1))
    holoMaterial(103,"Zup/planets/mineral_001")
    TeleA[5,entity] = holoEntity(103)
    TeleR[5,number] =  30
    
   #Cave Ice  
    holoPos(104,Base+(vec(-9217.9,2046.26,-11256.65)/100)/Scale)
    holoScaleUnits(104,vec(-61,-61,-61)/Scale)
    holoModel(104,"hqicosphere2")
    holoColor(104,vec4(255,255,255,A1))
    holoMaterial(104,"Zup/planets/ice_001")
    TeleA[6,entity] = holoEntity(104)
    TeleR[6,number] =  30
    
       #Moon 
    holoPos(105,Base+(vec(-8192.09,11264.41,1027.09)/100)/Scale)
    holoScaleUnits(105,vec(-30,-30,-30)/Scale)
    holoModel(105,"hqicosphere2")
    holoColor(105,vec4(255,255,255,A1))
    holoMaterial(105,"Zup/planets/moon_001")
    TeleA[7,entity] = holoEntity(105)
    TeleR[7,number] =  15
}
if(GroundOn){
#spawn
    holoPos(106,Base+(vec(-2.35,9216.58,-3072)/100)/Scale)
    holoScaleUnits(106,vec(82,82,1)/Scale)
    holoModel(106,"hqicosphere2")
    holoColor(106,vec4(255,255,255,A2))
    holoMaterial(106,"Zup/planets/earthlike_001")
#Lava Planet
     holoPos(107,Base+(vec(9208.7,3070.82,10249.38)/100)/Scale)
    holoScaleUnits(107,vec(61,61,1)/Scale)
    holoModel(107,"hqicosphere2")
    holoColor(107,vec4(255,255,255,A2))
    holoMaterial(107,"Zup/planets/lava_001")
  #desert 
    holoPos(108,Base+(vec(9220.68,-9216.89,2058.91)/100)/Scale)
    holoScaleUnits(108,vec(92,92,1)/Scale)
    holoModel(108,"hqicosphere2")
    holoColor(108,vec4(255,255,255,A2))
   holoMaterial(108,"Zup/planets/desert_001")
    #Toxic
    holoPos(109,Base+(vec(-9216.11,-9215.66,-504.95)/100)/Scale)
    holoScaleUnits(109,vec(82,82,1)/Scale)
    holoModel(109,"hqicosphere2")
    holoColor(109,vec4(255,255,255,A2))
    holoMaterial(109,"Zup/planets/swamp_001")
   #Normal Ice  
    holoPos(111,Base+(vec(0.94,-4091.87,-8190.4)/100)/Scale)
    holoScaleUnits(111,vec(61,61,1)/Scale)
    holoModel(111,"hqicosphere2")
    holoColor(111,vec4(255,255,255,A2))
    holoMaterial(111,"Zup/planets/mineral_001")
   #Cave Ice  
    holoPos(112,Base+(vec(-9217.9,2046.26,-11256.65)/100)/Scale)
    holoScaleUnits(112,vec(61,61,1)/Scale)
    holoModel(112,"hqicosphere2")
    holoColor(112,vec4(255,255,255,A2))
    holoMaterial(112,"Zup/planets/ice_001")
    #Moon 
    holoPos(113,Base+(vec(-8192.09,11264.41,1027.09)/100)/Scale)
    holoScaleUnits(113,vec(30,30,1)/Scale)
    holoModel(113,"hqicosphere2")
    holoColor(113,vec4(255,255,255,A2))
    holoMaterial(113,"Zup/planets/moon_001")

   
}




    holoParent(100,EE)
holoParent(110,EE)
holoParent(120,EE)
holoParent(101,EE)
holoParent(102,EE)
holoParent(103,EE)
holoParent(104,EE)
holoParent(105,EE)
holoParent(106,EE)
holoParent(107,EE)
holoParent(108,EE)
holoParent(109,EE)
holoParent(111,EE)
holoParent(112,EE)
holoParent(113,EE)


}
}

if(MA == 7){
if(first()){ 
    holoCreate(100)
    holoCreate(110)    
    holoCreate(120)
    holoCreate(101)
    holoCreate(102)
    holoCreate(103)
    holoCreate(104)

    

}
if(clk("TerrainReset"))  {
    #Spawn Planet
    holoPos(100,Base+(vec(7010.68,7241.2,-11907.93)/100)/Scale)
    holoScaleUnits(100,vec(135,135,135)/Scale)
    holoModel(100,"hqicosphere2")
    holoColor(100,vec4(0,100,0,255))
    holoMaterial(100,"models/wireframe")

#mars
    holoPos(110,Base+(vec(5090.75,-8613.1,222.68)/100)/Scale)
    holoScaleUnits(110,vec(75,75,75)/Scale)
    holoModel(110,"hqicosphere2")
    holoColor(110,vec4(255,136,0,255))
    holoMaterial(110,"models/wireframe")
#Jupiter
     holoPos(120,Base+(vec(-10492.42,-3684.48,4793.96)/100)/Scale)
    holoScaleUnits(120,vec(80,80,80)/Scale)
    holoModel(120,"hqicosphere2")
    holoColor(120,vec4(255,100,0,255))
    holoMaterial(120,"models/wireframe")
  #Moon  
    holoPos(101,Base+(vec(-854.01,8362.8,-4924.63)/100)/Scale)
    holoScaleUnits(101,vec(30,30,30)/Scale)
    holoModel(101,"hqicosphere2")
    holoColor(101,vec4(200,200,200,255))
    holoMaterial(101,"models/wireframe")
    #neptune
    holoPos(102,Base+(vec(-10121.03,-254.07,-9292.37)/100)/Scale)
    holoScaleUnits(102,vec(45,45,45)/Scale)
    holoModel(102,"hqicosphere2")
    holoColor(102,vec4(0,0,200,255))
    holoMaterial(102,"models/wireframe")
   #forest   
    holoPos(103,Base+(vec(-9034.26,9646.67,7699.6)/100)/Scale)
    holoScaleUnits(103,vec(65,65,65)/Scale)
    holoModel(103,"hqicosphere2")
    holoColor(103,vec4(0,150,0,255))
    holoMaterial(103,"models/wireframe")
    holoAng(103,ang(15,30,5))
   #magma 
    holoPos(104,Base+(vec(9919.77,11369.83,12671.91)/100)/Scale)
    holoScaleUnits(104,vec(40,40,40)/Scale)
    holoModel(104,"hqicosphere2")
    holoColor(104,vec4(150,0,0,255))
    holoMaterial(104,"models/wireframe")
    
    

    holoParent(100,EE)
holoParent(110,EE)
holoParent(120,EE)
holoParent(101,EE)
holoParent(102,EE)
holoParent(103,EE)
holoParent(104,EE)


}
}

if(E2LocOn)
{if(first()){holoCreate(200)}
  if(Blink){ Col1 = vec(255,0,0)}
else{ Col1 = vec(50,50,50)}
    holoPos(200,Base+(entity():pos()/100)/Scale)
    holoModel(200,"hqicosphere2")
    holoScaleUnits(200,vec(3,3,3)/Scale)
    holoColor(200,Col1)
}
if(Ent1On)
{if(first()){holoCreate(210)}
    holoPos(210,Base+(Ent1:pos()/100)/Scale)
    holoModel(210,"sphere")
    holoScaleUnits(210,vec(3,3,3)/Scale)
    holoColor(210,vec(0,0,0))
}
if(Ent2On)
{if(first()){holoCreate(220)}
    holoPos(220,Base+(Ent2:pos()/100)/Scale)
    holoModel(220,"sphere")
    holoScaleUnits(220,vec(3,3,3)/Scale)
    holoColor(220,vec(0,0,0))
}
if(MinerPos)
{if(first()){
    holoCreate(221)
    holoCreate(222)
    holoCreate(223)
    }
    holoPos(221,Base+(CurPos/100)/Scale)
    holoModel(221,"sphere")
    holoScaleUnits(221,vec(4,4,4)/Scale)
    holoColor(221,vec(255,0,0))
    
        holoPos(222,Base+(TarPos/100)/Scale)
    holoModel(222,"sphere")
    holoScaleUnits(222,vec(5,5,5)/Scale)
    holoColor(222,vec(0,255,0))

MinerTar =gGetEnt("Target")

     holoPos(223,Base+((MinerTar:pos()+vec(0,0,100))/100)/Scale)
    holoModel(223,"sphere")
    holoScaleUnits(223,((MinerTar:boxSize()/100)*1.2)/Scale)
    holoColor(223,vec(250,0,0))
    holoMaterial(223,"ce_ls3additional/asteroids/asteroid_small")
}


#colors Array
ColorA[1,vector] = vec(200,0,0)
ColorA[2,vector] = vec(0,100,0)
ColorA[3,vector] = vec(0,0,255)
ColorA[4,vector] = vec(200,200,0)
ColorA[5,vector] = vec(200,0,200)
ColorA[6,vector] = vec(100,100,100)
ColorA[7,vector] = vec(0,200,200)
ColorA[8,vector] = vec(200,200,200)
ColorA[9,vector] = vec(255,75,0)

findIncludeClass("player")
if(ShowHDrive){findIncludeClass("gmod_wire_hoverdrivecontroler")}
if(ShowGPod){findIncludeClass("gyropod_advanced")}
if(ShowOreLaser){
findIncludeClass("sa_mining_laser")
findIncludeClass("sa_mining_laser_ii")
findIncludeClass("sa_mining_laser_iii")
findIncludeClass("sa_mining_laser_iv")
findIncludeClass("sa_mining_laser_v")
findIncludeClass("sa_mining_laser_vi")
}
if(ShowCore){
    findIncludeClass("ship_core_caldari")
    findIncludeClass("ship_core_amarr")
    findIncludeClass("ship_core_minmatar")
    findIncludeClass("ship_core_station")
    }
if(IceOn){
    if(DarkGlitter){
findIncludeClass("iceroid_dark_glitter")}
    if(Gelidus){
findIncludeClass("iceroid_gelidus")}
    if(Krystallos){
findIncludeClass("iceroid_krystallos")}

}
if(Stargate){
    findIncludeModel("models/zup/stargate/stargate_base.mdl")
    findIncludeModel("models/zup/stargate/sga_base.mdl")
}
if(Grobs){
    findIncludeClass("grob_fighter")
    findIncludeClass("grob_overlord")
}
if(ShieldsOn){findIncludeClass("sga_shield")}

if(Singularity){findIncludeClass("singularity_cannon_blackhole_01")}



findInSphere(Base,100000)
Ar = findToArray()
Num = Ar:count()
if(changed(Num)){
I3 = 0
Draw = 1
EntCount = 1
PCOUNT = 0
HCOUNT = 0
GCOUNT = 0
OLCOUNT = 0
CORECOUNT = 0
IceRoidCount = 0
StargateCount = 0
GrobCount = 0
ShieldCount = 0
SingCount = 0


#for(Del = Num+1,50){holoDelete(Del)
#   PA[Del,entity] = noentity() }

for(Del = Num+1,50){holoDelete(Del)
   PA[Del,entity] = noentity() 

#reset()
}
}


if(Draw)
{if(first()){I2 = 1, I3 = 1}
holoCreate(I2)
holoScaleUnits(I2,vec(0.1,0.1,0.1)) 
    Ar2[I2,entity] =holoEntity(I2)
    I2++
}
if(I2 > Num){I2 = 1,Draw = 0}

I3++

Ent = Ar[I3,entity]

if(first()){PCOUNT = 1}
if(EntCount== 1){

if(Ent:type() == "player")
{PCOUNT++}
if(Ent:type() == "gmod_wire_hoverdrivecontroler")
{HCOUNT++}
if(Ent:type() == "gyropod_advanced")
{GCOUNT++}

if(Ent:type() == "sa_mining_laser" |
Ent:type() == "sa_mining_laser_ii" |
Ent:type() == "sa_mining_laser_iiI" |
Ent:type() == "sa_mining_laser_iv" |
Ent:type() == "sa_mining_laser_v" |
Ent:type() == "sa_mining_laser_vi")
{OLCOUNT++}

if(Ent:type() == "ship_core_caldari" |
Ent:type() == "ship_core_amarr" | 
Ent:type() == "ship_core_minmatar" |
Ent:type() == "ship_core_station" )
{CORECOUNT++}

if(Ent:type() == "iceroid_dark_glitter"|
Ent:type() == "iceroid_gelidus"|
Ent:type() == "iceroid_krystallos"

){
IceRoidCount++}

if(Ent:model() == "models/zup/stargate/stargate_base.mdl" |
Ent:model() =="models/zup/stargate/sga_base.mdl")
{StargateCount++}

if(Ent:type() == "grob_fighter" |
Ent:type() == "grob_overlord")
{GrobCount++}

if(Ent:type() == "sga_shield")
{ShieldCount++}

if(Ent:type() == "singularity_cannon_blackhole_01")
{SingCount++}


}






if(first()) {I4 = 1}
if(Ent:type() == "player"& EntCount2 == 1)
{PA[I4,entity] = Ent
I4++}
if(I4 > PCOUNT){I4 = 1,EntCount2 = 0}

if(first()) {I20 = 1}
if(Ent:type() == "grob_fighter" |
Ent:type() =="grob_overlord")
{GA[I20,entity] = Ent
I20++}
if(I20 > GrobCount){I20 = 1}

if(Grobs & GrobCount > 0)
{
    for(I21 = 1,GrobCount){
        GEnt =  GA[I21,entity]
        holoCreate(I21+1000)
     holoPos(I21+1000,Base+((GEnt:pos()+vec(0,0,0))/100)/Scale)
holoModel(I21+1000,"hqicosphere2")
holoScaleUnits(I21+1000,(GEnt:boxSize()*2/100)/Scale)
#holoAng(I21+1000,GEnt:angles()+ang(0,0,0))
holoColor(I21+1000,vec(255,255,255))
holoParent(I21+1000,EE) 
holoMaterial(I21+1000,"models/XQM/LightLinesRed")
        
}        
}


if(I3 > Num){I3 = 0, EntCount = 0,EntCount2 = 1}



for(I5 = 1,PA:count()){
if(Ent:owner() == PA[I5,entity]){I6 = I5}
elseif(Ent == PA[I5,entity]){I6 = I5}
}

Ent2 = PA[I6,entity]

if(Ent:type() == "player")
{
holoPos(I3,Base+((Ent:pos()+vec(0,0,50))/100)/Scale)
holoModel(I3,"tetra")

holoScaleUnits(I3,vec(5,5,5)/Scale)
holoAng(I3,Ent:eyeAngles()+ang(90,0,0))
if(TeamColorOn)
{holoColor(I3,teamColor(Ent2:team()))}
#elseif(Point1:distance(Ar2[I3,entity]:pos())<2)
#{holoColor(I3,vec(255,255,255))}
else{holoColor(I3,ColorA[I6,vector])}
holoParent(I3,EE)


}

if(ShowHDrive){
if(Ent:type() == "gmod_wire_hoverdrivecontroler")
{
holoPos(I3,Base+((Ent:pos()+vec(0,0,50))/100)/Scale)
holoModel(I3,"hqcylinder")
holoScaleUnits(I3,vec(3,3,5)/Scale)
holoAng(I3,ang(0,0,0))
if(TeamColorOn)
{holoColor(I3,teamColor(Ent2:team()))}
else{holoColor(I3,ColorA[I6,vector])}
holoParent(I3,EE)
}}

if(ShowGPod){
if(Ent:type() == "gyropod_advanced")
{
holoPos(I3,Base+((Ent:pos()+vec(0,0,50))/100)/Scale)
holoModel(I3,"cube")
holoScaleUnits(I3,vec(5,3,2)/Scale)
holoAng(I3,Ent:angles()+ang(0,0,0))
if(TeamColorOn)
{holoColor(I3,teamColor(Ent2:team()))}
else{holoColor(I3,ColorA[I6,vector])}
holoParent(I3,EE)
}}


if(ShowOreLaser){
if(Ent:type() == "sa_mining_laser" |
Ent:type() == "sa_mining_laser_ii" |
Ent:type() == "sa_mining_laser_iiI" |
Ent:type() == "sa_mining_laser_iv" |
Ent:type() == "sa_mining_laser_v" |
Ent:type() == "sa_mining_laser_vi"
)
{
holoPos(I3,Base+((Ent:pos()+vec(0,0,0))/100)/Scale)
holoModel(I3,"cone")
holoScaleUnits(I3,vec(2,2,4)/Scale)
holoAng(I3,Ent:angles()+ang(0,0,0))
if(TeamColorOn)
{holoColor(I3,teamColor(Ent2:team()))}
else{holoColor(I3,ColorA[I6,vector])}
holoParent(I3,EE)
}}

if(ShowCore){
if(Ent:type() == "ship_core_caldari" |
Ent:type() == "ship_core_amarr" | 
Ent:type() == "ship_core_minmatar" |
Ent:type() == "ship_core_station" )
{
holoPos(I3,Base+((Ent:pos()+vec(0,0,0))/100)/Scale)
holoModel(I3,"cube")
#holoModel(I3,Ent:model())
holoScaleUnits(I3,vec(4,4,4)/Scale)
#holoScaleUnits(I3,(Ent:boxSize()/10)/Scale)
holoAng(I3,Ent:angles()+ang(0,0,0))
if(TeamColorOn)
{holoColor(I3,teamColor(Ent2:team()))}
else{holoColor(I3,ColorA[I6,vector])}    
holoParent(I3,EE)


}}

if(IceOn){
if(Ent:type() == "iceroid_dark_glitter"|
Ent:type() == "iceroid_gelidus"|
Ent:type() == "iceroid_krystallos"

)
{
holoPos(I3,Base+((Ent:pos()+vec(0,0,0))/100)/Scale)
#holoModel(I3,"hqicosphere2")
holoModel(I3,Ent:model())
holoScaleUnits(I3,vec(8,8,8))
#holoScaleUnits(I3,Ent:boxSize()*5/100)
holoAng(I3,Ent:angles()+ang(0,0,0))
#holoColor(I3,vec(255,255,255))
holoColor(I3,Ent:getColor())
holoParent(I3,EE)
holoMaterial(I3,Ent:getMaterial())
}}

if(Stargate){
if(Ent:model() == "models/zup/stargate/stargate_base.mdl" |
Ent:model() =="models/zup/stargate/sga_base.mdl")
{
holoPos(I3,Base+((Ent:pos()+vec(0,0,0))/100)/Scale)
holoModel(I3,"hqtorus2")
holoScaleUnits(I3,vec(10,10,2)/Scale)
holoAng(I3,Ent:angles()+ang(90,0,0))
holoColor(I3,vec(255,255,255))
holoParent(I3,EE)
}}
if(ShieldsOn){
if(Ent:type() == "sga_shield")
{
holoPos(I3,Base+((Ent:pos()+vec(0,0,0))/100)/Scale)
holoModel(I3,"hqicosphere2")
holoScaleUnits(I3,(Ent:boxSize()/100)/Scale)
holoAng(I3,Ent:angles()+ang(90,0,0))
holoColor(I3,vec(70,70,70))
#holoColor(I3,Ent:getColor())
holoMaterial(I3,"Space_Combat/Shield/sga_shield_01")
holoParent(I3,EE)    

}
}  
if(Singularity){
if(Ent:type() == "singularity_cannon_blackhole_01")
{
holoPos(I3,Base+((Ent:pos()+vec(0,0,0))/100)/Scale)
#holoModel(I3,"hqicosphere2")
holoModel(I3,Ent:model())
holoScaleUnits(I3,(Ent:boxSize()*1/100)/Scale)
holoAng(I3,Ent:angles()+ang(90,0,0))
#holoColor(I3,vec(255,255,255))
holoColor(I3,Ent:getColor())
#holoMaterial(I3,"Space_Combat/Shield/sga_shield_01")
holoParent(I3,EE)    

}
}





if(EgpOn){
#Credit = Ent:credits()
Credit = 0 #for spaceage
if (Credit > 0){Credits = Credit, Cred = ""}
if (Credit > 1000){Credits = Credit/1000, Cred = "K"}
if (Credit > 1000000){Credits = Credit/1000000, Cred = "M"}
if (Credit > 1000000000){Credits = Credit/1000000000, Cred = "B"}
if (Credit > 1000000000000){Credits = Credit/1000000000000, Cred = "T  OMG GIVE ME SOME"}

#egp code
if(Ent:type() == "player")
{
 
if(Ping){PingV = "   Ping"+" "+Ent:ping()}
else{PingV = ""}

if(ShowCredit){Creditsv = ""+round(Credits,3)}
else{Creditsv = ""}

if(FactionsOn){
TeamName = teamName(Ent2:team())}else{TeamName = ""}
    
Egp:egpText(30,"Expression2 HoloMap ColorCode",vec2(0,5))
Egp:egpSize(30,30)
Egp:egpColor(30,vec(150,150,150))
Egp:egpText(I4,""+Ent2:name(),vec2(0,(I6*20)+20))

Egp:egpText(I4+20,""+Creditsv+" "+Cred+PingV+" "+TeamName,vec2(230,(I6*20)+20))


if(TeamColorOn){
Egp:egpColor(I4,teamColor(Ent:team()))
Egp:egpColor(I4+20,teamColor(Ent:team()))}
else{
Egp:egpColor(I4,ColorA[I6,vector])
Egp:egpColor(I4+20,ColorA[I6,vector])}
}

if(changed(Ar:count())){Egp:egpClear()}
}


if(clk("send")){Asguard["Send",number] = 1}
else{Asguard["Send",number] = 0}

Asguard["OriginX",number] = BeamOrigin:x()
Asguard["OriginY",number] = BeamOrigin:y()
Asguard["OriginZ",number] = BeamOrigin:z()

Asguard["DestX",number] = BeamTar:x()
Asguard["DestY",number] = BeamTar:y()
Asguard["DestZ",number] = BeamTar:z()
