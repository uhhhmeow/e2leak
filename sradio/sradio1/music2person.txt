@name Music Selection Menu v2
@model models/hunter/plates/plate.mdl
#Data in
@inputs Scr:wirelink User2:entity AMT
#EGP and Money
@persist Menu User:entity Use Credit:array
@persist Current Using RollOver Draw Credits
@persist Page Songcost
#Alarm
@persist Chat OutText:string Alarm Larmtext:string
@persist Stocking
#Music
@persist Music:array Musics:gtable Length:gtable Name:gtable Path:gtable
#Loading
@persist Cur Need Loaded FileName:string
#Timeout
@persist MaxTime TimeLeft Select Loadsonglist
#Data Out
@outputs Buy
#Trigger
@trigger all
#Loading
Songcost=0 ##Price/Second
if (first()) {
Length=gTable("slength")
Name=gTable("sname")
Path=gTable("spath")
Musics=gTable("sfile")
if (Length:count()) {
    Loaded=1 
    hint("Loaded from GTables.",5)
    for (I=1, Musics:count()) {
        Music[I,string]=Musics[I,string]
    }
    #print(Music:concat(", "))
    #print(Musics:toTable():concat(", "))
    }
}
if (duped()) {reset()}
Draw=0
interval(100)

if (first() & Loaded & Length:count()==0) {
    #print("Default test settings")
    for (I=1, 50) {
        Path[I,string]="PATH"+I
        Name[I,string]="NAME"+I
        Length[I,number]=I*10
    }
}
if (first()) {
    Page=1
    Cur=1
    function string getTime(Num:number) {
    Min=floor(Num/60)
    Sec=Num-(Min*60)
    if (Sec>=10) {
    return ""+Min+":"+Sec
} else {
    return ""+Min+":0"+Sec
}
}
}
    
runOnFile(1)
runOnChat(1)
if (!Loaded) {

    
    
    
if (first() |!Music) {
    if (!Loaded) {
    if (first()) {#print("Loading main list")}
    Load=fileCanLoad()
    Write=fileCanWrite()
    timer("timeout",10000)
    if (Load) {
        fileLoad("music/songlist.txt")
        interval(1000)
} else {print("Waiting for songlist...") interval(1000)}
}
}
if (clk("timeout")) {
    Loading=0 
    if (fileStatus()==_FILE_OK) {
        print("It was alright. Fix your code.")
    }
    if (fileStatus()==_FILE_404) {
        print("Four, oh four, error.")
    }
    if (fileStatus()==_FILE_TIMEOUT) {
        print("TIME OUT! Error.")
    }
    if (fileStatus()==_FILE_UNKNOWN) {
        print("File does not exist")
    }
    if (fileStatus()==_FILE_TRANSFER_ERROR) {
        print("Transfer error")
    }
    }
}
    
    
    
    if (fileClk("music/songlist.txt") & fileStatus()==_FILE_OK) {
        Music=fileRead():explode(":")
        for (I=1, Music:count()) {
            Musics[I,string]=Musics[I,string]
        }
        Need=Music:count()
        #print("Tracks: "+Music:count())
        
    }
        if (Need & changed(Cur)) {
        FileName=Music[Cur,string]
        setName("Songs: "+Need+"
        Loading "+FileName+"
        Loaded: "+Cur+"/"+Need)
    }
        if (FileName & !Loaded & fileCanLoad() & changed(Cur)) {
            hint("Loading "+FileName,5)
            fileLoad("music/"+FileName)
        }
        if (fileClk("music/"+FileName)) {
            Songn=fileRead()
            hint("Loaded "+FileName,5)
            if (Songn:length()==0) {hint("ERROR: NO TEXT",5)}
            Songinfo=Songn:explode(":")
            Timem=Songinfo[1,string]:toNumber()
            Namem=Songinfo[2,string]
            Pathm=Songinfo[3,string]
            Length[Cur,number]=Timem
            Path[Cur,string]=Pathm
            Name[Cur,string]=Namem
            Musics[Cur,string]=FileName
            hint("Added song "+Namem,5)
            hint(Songinfo:concat(", "),5)
            Cur++
            if (Cur==Need) {Loaded=1 hint("Loaded",5)}
            
        }
        if (!Loaded) {Scr:egpClear()}
        if (Loaded) {setName("SMus1") reset()}
        
    }

#1 Time
#2 Left
#3 Name
#4 Path
#5 Station
#6 Station Name
#7 Song list
if (Loaded) {
if (User:keyUse()) {TimeLeft=MaxTime}
Draw=changed(Menu)
ID=ceil((User:steamID():sub(11):toNumber())/100000)
Credits=Credit[ID,number]
timer("TimeDown",1000)
if (Menu==-1) {TimeLeft=0}
if (clk("TimeDown")) {
    Draw=1
    if (Menu!=6) {
    if (TimeLeft<(MaxTime/3)+1 & TimeLeft) {
    Scr:egpText(60,"You will be logged off in " +TimeLeft+" seconds.",vec2(256,500))
    Scr:egpAlign(60,1,1)
    Scr:egpColor(60,vec(255,0,0))
} else {Scr:egpRemove(60)}
    if (TimeLeft & Menu!=4 & Menu!=4.1) {
    TimeLeft-=1
} elseif (Menu!=4 & Menu!=4.1) {User=noentity() Menu=-1}
}

timer("TimeDown",1000)  
}

#MAIN#######################################################################
runOnChat(1)
runOnFile(1)
MaxTime=30
W=Scr
O=User
OutText="/advert [SMusics] The Jukebox is being robbed!"
interval(100)
Use=(User:keyUse() & !Using)
if (Use) {Using=1}
if (Using & !User:keyUse()) {Using=0}
if (changed(Menu)) {
    Scr:egpClear()
    Use=0
    Scr:egpBox(40,vec2(256,256),vec2(512,512))
    Scr:egpMaterial(40,"maps/noicon")
}



if (!User & !Alarm) {Menu=-1}
if (User & User:aimEntity()!=Scr:entity()) {
    if (Menu!=4 & Menu!=4.1 & Menu==11111333314124) {
    User=noentity() Menu=-1
} elseif (User:pos():distance(Scr:entity():pos())>200) {
    User=noentity()
    Menu=-1
}
    }
#STARTUP#####################################################################
if (first() | duped()) {
    Menu=-1
    Scr:egpClear()
    
    
        function number pIR(Num:number) {
        if(inrange(W:egpCursor(O):x(),W:egpPos(Num):x()-(W:egpSize(Num):x()/2),W:egpPos(Num):x()+(W:egpSize(Num):x()/2))&
        inrange(W:egpCursor(O):y(),W:egpPos(Num):y()-(W:egpSize(Num):y()/2),W:egpPos(Num):y()+(W:egpSize(Num):y()/2))) {
            RollOver=Num
            return 1
        } else {
            return 0
        }
    }
    
    
}


#START MENU###############################################################
if (Menu==0) {
    if (Draw) {
    Scr:egpText(1,"Music Machine",vec2(256,15))
    Scr:egpAlign(1,1,1)
    Scr:egpSize(1,50)
    Scr:egpRoundedBox(2,vec2(256,256),vec2(150,30))
    Scr:egpText(3,"Start",vec2(256,256))
    Scr:egpColor(2,120,120,120,255)
    Scr:egpAlign(3,1,1)
    Scr:egpRoundedBoxOutline(4,vec2(256,256),vec2(150,30))
    Scr:egpAlpha(4,0)
}
    if (pIR(2)) {
        Scr:egpAlpha(4,255)
        if (Use) {
            Menu=1
            Use=0
    }
} else {Scr:egpAlpha(4,0)}
}
#LOGIN#####################################################################
if (Menu==-1) {
    if (Draw) {
    Scr:egpBox(2,vec2(256,256),vec2(512,100))
    Scr:egpColor(2,80,80,80,255)
    Scr:egpText(1,"Press Anywere to Begin",vec2(256,256))
    Scr:egpAlign(1,1,1)
    Scr:egpSize(1,50)
}
    if (User2) {User=User2}
    if (User) {Menu=0 Use=0}
}

#SELECTION#################################################################

if (changed(Menu) & Menu==1) {
}

if (Menu==1) {
    Pages=ceil(Path:count()/5)

    for (I=1, 5) {
        ID=I+(5*(Page-1))
        if (Draw & Path[ID,string]) {
        Namem=Name[ID,string]
        Lengthm=Length[ID,number]
        Pathm=Path[ID,string]
        Scr:egpBox(I*5,vec2(0,512/5*I-(512/5/2)),vec2(1024,512/5))
        Scr:egpBoxOutline(I*5+1,vec2(0,512/5*I-(512/5/2)),vec2(1024,512/5))
        Scr:egpColor(I*5,vec(100,100,255))
        Scr:egpColor(I*5+1,vec(0,0,0))
        Scr:egpText(I*5+2,Namem,vec2(10,512/5*I-(512/5/2)))
        Scr:egpSize(I*5+2,25)
        Scr:egpAlign(I*5+2,0,1)
        Scr:egpText(I*5+3,Pathm+", "+getTime(Lengthm),vec2(512,(512/5*I-(512/5/2))+30))
        Scr:egpAlign(I*5+3,2,0)
    }
        if (!pIR(30) & !pIR(32) & Path[ID,string]) {
        if (pIR(I*5)) {Scr:egpColor(I*5,vec(50,50,50))} else {Scr:egpColor(I*5,vec(100,100,255))}
        if (pIR(I*5) & Use) {Select=ID Menu=4}
    }
    }
    if (Draw) {
        if (Pages>Page) {
        Scr:egpRoundedBox(30,vec2(512-25,25),vec2(25,25)) #
        Scr:egpAlign(30,2,2)
        Scr:egpRoundedBoxOutline(31,vec2(512-25,25),vec2(25,25))
        Scr:egpAlign(31,2,2)
        Scr:egpColor(30,vec(120,120,120))
        Scr:egpColor(31,vec(0,0,0))
        Scr:egpText(34,">",Scr:egpPos(30))
        Scr:egpAlign(34,1,1)
        Scr:egpSize(34,25)
                Scr:egpColor(34,vec(0,255,0))
    }
    if (Page>1) {
        Scr:egpRoundedBox(32,vec2(512-75,25),vec2(25,25)) #
        Scr:egpAlign(32,2,2)
        Scr:egpRoundedBoxOutline(33,vec2(512-75,25),vec2(25,25))
        Scr:egpAlign(33,2,2)
        Scr:egpColor(32,vec(120,120,120))
        Scr:egpColor(33,vec(0,0,0))
        Scr:egpText(35,"<",Scr:egpPos(32))
        Scr:egpAlign(35,1,1)
        Scr:egpSize(35,25)
        Scr:egpColor(35,vec(255,0,0))
    }
    Scr:egpText(36,"Page "+Page+" of "+Pages,vec2(((512-75)+(512-25))/2,45))
    Scr:egpAlign(36,1,1)
    }
        if (pIR(30)) {Scr:egpAlpha(31,255)} else {Scr:egpAlpha(31,0)}
    if (pIR(30) & Use & Pages>Page) {
        Page++
        Scr:egpClear()
        Draw=1
    }
    if (pIR(32)) {Scr:egpAlpha(33,255)} else {Scr:egpAlpha(33,0)}
    if (pIR(32) & Use & Page>1) {Page-- Scr:egpClear() Draw=1}
    
}
#PURCHASE MENU############################################################
if (Menu==4) {
        Credits=Credit[ID,number]
        Price=ceil(Length[Select,number]*Songcost)
        Needed=Price-Credits



    if (Draw) {

    Scr:egpText(10,"Confirmation and Payment",vec2(256,10))
    Scr:egpSize(10,30)
    Scr:egpAlign(10,1,1)
    Scr:egpText(20,"Would you like to play "+Name[Select,string],vec2(256,40))
    Scr:egpText(30,"For $"+Price+".00?",vec2(256,70))
    Scr:egpText(31,"Credits: "+Credits,vec2(256,100))
    Scr:egpAlign(20,1,1)
    Scr:egpAlign(30,1,1)
    Scr:egpSize(20,30)
    Scr:egpSize(30,25)
    Scr:egpAlign(31,1,1)
    Scr:egpSize(31,25)
    Scr:egpRoundedBox(2,vec2(256,200),vec2(200,40))
    Scr:egpColor(2,120,120,120,255)
    if (Needed>0) {
        Scr:egpAlpha(2,0)
} else {Scr:egpAlpha(2,255)}
    if (Needed>0) {
        Text="Insert $"+Needed+" to machine"
} else {Text="Confirm"}
    Scr:egpText(3,Text,vec2(256,200))
    Scr:egpAlign(3,1,1)
    Scr:egpRoundedBox(4,vec2(256,400),vec2(200,40))
    Scr:egpText(5,"Cancel",vec2(256,400))
    Scr:egpAlign(5,1,1)
    Scr:egpColor(4,120,120,120,255)
    Scr:egpRoundedBoxOutline(7,vec2(256,200),vec2(200,40))
    Scr:egpRoundedBoxOutline(8,vec2(256,400),vec2(200,40))
    Scr:egpAlpha(7,0)
    Scr:egpAlpha(8,0)

    
}
        
    if (pIR(2) & Needed<=0) {
        Scr:egpAlpha(7,255)
        if (Use & Needed<=0) {
            Buy=Select
            #print(Buy,Music[Buy,string],Path[Buy,string],Length[Buy,number],Name[Buy,string])
            Menu=4.1 Use=0
            R=gTable("Radio")
            Credit[ID,number]=Credit[ID,number]-Price
            #Re=Name[Select,string]+":::"+Path[Select,string]+":::"+Length[Select,number]
            Re=Music[Buy,string]
            #R[8,string]=Re
            User:soundPlay(User:id()/10,0,Re)
            #print(Music[Buy,string],R[8,string],Re)
            hint("Removed "+Price+" from "+User:name()+"'s account via purchase",5)
        }
} else {Scr:egpAlpha(7,0)}
    if (pIR(4)) {
        Scr:egpAlpha(8,255)
        if (Use) {Menu=1}
} else {Scr:egpAlpha(8,0)}

    
}




if (Menu==4.1) {
    Buy=Select
    Scr:egpText(1,"Playing song "+Name[Select,string],vec2(256,256))
    Scr:egpSize(1,40)
    Scr:egpAlign(1,1,1)
    Scr:egpText(2,"Thanks for SShopping!",vec2(256,300))
    Scr:egpAlign(2,1,1)
    Scr:egpSize(2,30)

    #Name,path,length

    timer("Finish",4000)
}
if (clk("Finish")) {
    User=noentity()
    R=gTable("Radio")
    Menu=-1
}


#CREDITS, ALARM, ROLLOVER
if (changed(Menu)) {Scr:egpClear()}
if (AMT>Current) {
    if (!User) {
        findByClass("player")
        findSortByDistance(Scr:entity():pos())
        User=find()
    }
    Add=AMT-Current
    ID=ceil((User:steamID():sub(11):toNumber())/100000)
    Credit[ID,number]=Credit[ID,number]+Add
    Current=AMT
    hint("[SMus1] Added "+Add+" to "+User:name()+"'s account.",5)
}
if (AMT<Current) {
    Removed=Current-AMT
    Current=AMT
    hint("[SMus1] $"+Removed+" removed. | Say /alarm to signal alarm.",5)
}


############ALARM FUNCTIONS#################################################
if (chatClk(owner()) & owner():lastSaid()=="/alarm") {
    hideChat(1)
    Scr:entity():soundPlay(0,0,"/ambient/alarms/alarm1.wav")
    Chat=1
    Alarm=1
    Menu=6
    timer("ThanksAnyway",1000)
}
if (clk("ThanksAnyway")) {
    Chat=0     
    hint("[SMus1] Signalled police. Say /off to halt alarm.",5)
}
if (Chat) {
    concmd("say "+OutText)
}
if (chatClk(owner()) & owner():lastSaid()==(OutText)) {
    Chat=0
    hint("[SMus1] Signalled police. Say /off to halt alarm.",5)
}

if (chatClk(owner()) & owner():lastSaid()=="/off") {
    hideChat(1)
    hint("[SMus1] Alarm deactivated.",5)
    soundPurge()
    Alarm=0
    Menu=-1
}

if (chatClk(owner()) & owner():lastSaid()=="/stock" & !Alarm) {
    hideChat(1)
    if (Stocking) {
    hint("[SMus1] Enabled Shopping",5)
    soundPurge()
    Stocking=0
    Menu=-1
} else {
    hint("[SMus1] Disabled Shopping",5)
    Stocking=1
    Menu=6
}
}
#LOCK MENU##################################################################
if (Menu==6) {
    if (Alarm) {Larmtext="Robbery!"} else {Larmtext="Locked"}
    Scr:egpBox(3,vec2(256,256),vec2(512,100))
    Scr:egpColor(3,80,80,80,255)
    Scr:egpText(2,Larmtext,vec2(256,256))
    Scr:egpSize(2,50)
    Scr:egpAlign(2,1,1)
    Scr:egpColor(2,vec(255,0,0))

}
#AUTOSHUTOFF
if (Menu==4 & !Select) {Menu=1}

#Rollover
if (changed(RollOver)) {
    soundVolume(0,200)
    Scr:entity():soundPlay(0,0,"/ui/buttonrollover.wav")
}
if (Stocking) {Menu=6}


}

