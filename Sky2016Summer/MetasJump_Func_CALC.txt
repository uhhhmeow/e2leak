@name MetasJump Func CALC
 function ranger finalRanger(Pos:vector) {
        return rangerOffset(999999999, Pos, vec(0, 0, -1))
    }
    
    function table findJumpPos(Origin:vector) {
        local AimPos = Aim:pos()
        local AimDist = min(AimPos:distance(Origin), 1000)
        local AimF = Origin + (AimPos - Origin):normalized() * AimDist
        
        local Dist = Origin:distance(AimF)
        local Mid = (Origin + AimF) / 2
        
        local AimNormal = 1 - Aim:hitNormal():dot(vec(0, 0, 1))
        local ZDiff = max(abs(AimF:z() - Origin:z()) / 4, 200 * AimNormal)
        
        local Perp = vec(0, 0, 1) #( ( AimF - Origin ):toAngle() - ang( 90, 0, 0 ) ):forward()
        local MidPerp = Mid + Perp * (Dist / 2 + ZDiff)
        
        local End = AimF + vec(0, 0, 1) * ZDiff
                
        local RG1 = rangerOffset(Origin:distance(MidPerp), Origin, MidPerp - Origin)
        local FinalPos = Origin
        local FinalRanger = noranger()
        
        if (RG1:hit()) {
            local Dot = 1 - RG1:hitNormal():dot(vec(0, 0, 1))
            FinalRanger = finalRanger(RG1:pos() + RG1:hitNormal() * 150 * Dot)
            FinalPos = FinalRanger:pos()
        }
        else {
            local RD2 = rangerOffset(End:distance(MidPerp) * 1.5, MidPerp, End - MidPerp)
            local Dot = 1 - RD2:hitNormal():dot( vec( 0, 0, 1 ) )
            
            if (RD2:hit()) {
                FinalRanger = finalRanger(RD2:pos() + RD2:hitNormal() * 150 * Dot)
                FinalPos = FinalRanger:pos()
            } else {
                FinalRanger = finalRanger(RD2:pos())
                FinalPos = FinalRanger:pos()
            }
        }
        
        
        return table(FinalPos + vec(0, 0, Height), FinalRanger)
    }
