@name CT-V1
@inputs Egp:wirelink Infos:wirelink
@outputs 
@persist [Red Blue]:array   SpawnRed SpawnBlue C Gotya1 Gotya2 RedPoint BluePoint Dropped
@trigger 
runOnChat(1)
interval(40)
if(first()){
    Infos:egpClear()
    Egp:egpClear()
}

if(changed(Egp:entity())|changed(RedPoint)|changed(BluePoint)){
        Egp:egpBox(132,vec2(255,255),vec2(515,515))
Egp:egpColor(132,vec(0,155,255))
Egp:egpMaterial(132, "gui/gradient_up")  
Egp:egpAlpha(132,150)
Egp:egpBox(133,vec2(255,450),vec2(515,390))
Egp:egpColor(133,vec(50,255,100))
Egp:egpMaterial(133,"gui/gradient_up")  
Egp:egpAlpha(133,150)

 Egp:egpLine(1,vec2(255,0),vec2(255,510))   
 Egp:egpLine(2,vec2(0,60),vec2(510,60)) 
Egp:egpColor(1,vec4(255,125,25,250))
Egp:egpColor(2,vec4(255,125,25,250))

}
Egp:egpText(3,"Blue Team : "+BluePoint,vec2(80,20))
Egp:egpColor(3,vec(0,0,255))

Egp:egpText(4,"Red Team : "+RedPoint,vec2(340,20))
Egp:egpColor(4,vec(255,0,0))
 O = owner()   
if(chatClk(O)){
local Last = O:lastSaid():explode(" ")

if(chatClk(O)&Last:string(1)=="/ar"){
        hideChat(1)
local Player = findPlayerByName(Last:string(2))
   printColor(vec(0,255,0),"[E2]",vec(255,0,0)," "+Player:name() + " has been added to team red!")
if(Player:isPlayer()){
 Red:pushEntity(Player)   
}



}
if(chatClk(O)&Last:string(1)=="/ab"){
        hideChat(1)
local Player = findPlayerByName(Last:string(2))
    printColor(vec(0,255,0),"[E2]",vec(0,0,255)," "+Player:name() + " has been added to team Blue!")
if(Player:isPlayer()){
 Blue:pushEntity(Player)   
}

}
if(chatClk(O)&Last:string(1)=="/red"){
    printColor(vec(0,255,0),"[E2]",vec(255,0,0)," Position of the red base has been setted!")
    hideChat(1)
    SpawnRed = 1
}
if(chatClk(O)&Last:string(1)=="/blue"){
        printColor(vec(0,255,0),"[E2]",vec(0,0,255)," Position of the blue base has been setted!")
        hideChat(1)
    SpawnBlue = 1
}
if(chatClk(O)&Last:string(1)=="/reset"){
    hideChat(1)
Reset = Last:string(2)
}
if(chatClk(O)&Last:string(1)=="/flag"){
    hideChat(1)
           printColor(vec(0,255,0),"[E2]",vec(0,0,255)," Flag position has been setted!")
    rangerFilter(O)
    local DsR = rangerOffset(99999,O:pos(),vec(0,0,-1)):position()
 holoCreate(3,DsR+vec(0,0,2),vec(4,4,0.1),ang(),vec(0,255,0),"hqcylinder")
}
}
if(Reset == "pos"){
holoDelete(1)
holoDelete(2)
       printColor(vec(0,255,0),"[E2]",vec(255,255,0)," Positions of bases has been reseted !")
}
if(Reset == "all"){
         printColor(vec(0,255,0),"[E2]",vec(255,255,0)," Everthing has been reseted!")
reset()
holoDeleteAll()
}
if(SpawnRed == 1){
      rangerFilter(O)
    local Pos = rangerOffset(99999,O:pos(),vec(0,0,-1)):position()
if(!holoEntity(1)){
 holoCreate(1,Pos+vec(0,0,2),vec(3,3,0.1),ang(),vec(255,0,0),"hqcylinder")   
}
if(holoEntity(1)){
 SpawnRed = 0   
}
}
if(SpawnBlue == 1){
      rangerFilter(O)
    local Pos = rangerOffset(99999,O:pos(),vec(0,0,-1)):position()
if(!holoEntity(2)){
 holoCreate(2,Pos+vec(0,0,2),vec(3,3,0.1),ang(),vec(0,0,255),"hqcylinder")   
}
if(holoEntity(2)){
 SpawnBlue = 0   
}
}

if(changed(Blue:count())){
    for(B = 1,Blue:count()){
        for(R = 1,Red:count()){
#     if(Blue[B,entity]:name() == Red[R,entity]:name()){
    Egp:egpText(B+40,B + " : "+Blue[B,entity]:name(),vec2(20,35+B*40))  
    Egp:egpSize(B+40,30)
    Egp:egpColor(B+40,vec(0,0,255))
    
    #else{
     #Egp:egpRemove(B+40)   
#}   
    }
holoCreate(10 + B , Blue[B,entity]:pos()+vec(0,0,90),vec(1,1,1),ang(),vec(0,0,255),"hq_rcube")
holoParentAttachment(10+B,Blue[B,entity],"head")
}
}

if(changed(Red:count())){
for(R = 1,Red:count()){
        for(B = 1,Blue:count()){
#    if(Blue[B,entity]:name() == Red[R,entity]:name()){
    Egp:egpText(R+Blue:count()+5,R + " : "+Red[R,entity]:name(),vec2(280,35+R*40))
    Egp:egpSize(R+Blue:count()+5,30)
    Egp:egpColor(R+Blue:count()+5,vec(255,0,0))
 #   }else{
  #      Egp:egpRemove(R+Blue:count()+5)
#}
holoCreate(Blue:count() + R , Red[R,entity]:pos()+vec(0,0,90),vec(1,1,1),ang(),vec(255,0,0),"hq_rcube")
holoParentAttachment(Blue:count() + R,Red[R,entity],"head")
    }
}
}

if(Infos:entity()&Red:count()>=1&Blue:count()>=1){
      Infos:egpText(12345,"OPS : "+toString(ops()),vec2(450,400))   
    Infos:egpColor(12345,vec4(255,125,25,250))

if(clk("resetred")|changed(Red:count())){  
    for(R=1,Red:count()){ 
    Infos:egpText(Red:count()*2+50,R + " : "+Red[R,entity]:name(),vec2(150,250+R*30))

}
    stoptimer("resetred")
}
if(clk("resetblue")|changed(Blue:count())){
        for(B=1,Blue:count()){ 
   Infos:egpText(Blue:count()*3+50,B + " : "+Blue[B,entity]:name(),vec2(350,250+B*30))
}
    stoptimer("resetblue")
}

for(R = 1,Red:count()){
if(!Red[R,entity]:isAlive()){
    Infos:egpColor(Red:count()*2+50,vec(0,0,0))
timer("resetred",300)
}else{
 Infos:egpColor(Red:count()*2+50,vec(255,0,0))
}
}
for(B = 1,Blue:count()){
if(!Blue[B,entity]:isAlive()){
timer("resetblue",300)
  Infos:egpColor(Blue:count()*3+50,vec(0,0,0))
}else{
  Infos:egpColor(Blue:count()*3+50,vec(0,0,255))

}
}

}
if(!holoEntity(1)&!holoEntity(2)&holoEntity(3)){
holoDelete(3)
holoDelete(4)
holoDelete(5)
}
if(holoEntity(3)&!holoEntity(4)){
holoCreate(4,holoEntity(3):pos()+vec(0,0,50),vec(0.3,0.3,4),ang(),vec(80),"hqcylinder")
    holoCreate(5,holoEntity(3):pos()+vec(0,-5,68),vec(1,0.1,1),ang(90,0,90),vec(0,0,0),"hqcone")
    holoParent(5,4)
  
}
if(holoEntity(3)&holoEntity(4)&Gotya1 == 0&Gotya2 == 0&Dropped == 0){
 C++   
holoAng(4,ang(25,cos(C*2)*210,0))
}
if(Red:count()>=1&Blue:count()>=1){
for(R = 1,Red:count()){
    RP = Red[R,entity] 
local Bd = RP:pos():distance(holoEntity(4):pos())   
if(changed(Bd < 50)&Bd < 50&Dropped == 0){
Gotya1 = 1
}
if(Gotya1 == 1&Gotya2 == 0&Dropped == 0){
    holoPos(4,RP:pos()+vec(0,0,30))
    holoAng(4,ang(0,0,0))
}
if(changed(!RP:isAlive())&!RP:isAlive()|(changed(Dropped == 0)&Dropped == 0&Gotya1 == 0&Gotya2==0)){
 Gotya2 = 0
Gotya = 0   
Infos:egpText(500,"Flag returned !",vec2(500,500))
Infos:egpColor(500,vec(0,255,255))
timer("egpr",1000)
}
if(changed(RP:keyPressed("G"))&RP:keyPressed("G")){
 Dropped =1
timer("returnfd",5000)   
}


if(changed(RP:pos():distance(holoEntity(1):pos())<20)&RP:pos():distance(holoEntity(1):pos())<20){
 RedPoint += 1
 Gotya1 = 0
Gotya2 = 0
}
}
for(B=1,Blue:count()){
BP = Blue[B,entity]
if(changed(BP:keyPressed("G"))&BP:keyPressed("G")){
 Dropped =1
timer("returnfd",5000)   
}
local Rd = BP:pos():distance(holoEntity(4):pos())
if(changed(Rd < 50)&Rd < 50&Dropped == 0){
Gotya2 = 1
}
if(Gotya2 == 1&Gotya1 == 0&Dropped == 0){
    holoPos(4,BP:pos()+vec(0,0,30))
        holoAng(4,ang(0,0,0))
}
if(changed(RP:pos():distance(holoEntity(2):pos())<20)&RP:pos():distance(holoEntity(2):pos())<20){
 BluePoint += 1
Gotya1 = 0
Gotya2 = 0
}
if(changed(!BP:isAlive())&!BP:isAlive()|(changed(Dropped == 0)&Dropped == 0&Gotya1 == 0&Gotya2==0)){
 Gotya2 = 0
Gotya = 0   
Infos:egpText(500,"Flag returned !",vec2(500,500))
Infos:egpColor(500,vec(0,255,255))
timer("egpr",1000)
}
}
if(clk("egpr")){
    Infos:egpRemove(500)
stoptimer("egpr")
}
if(clk("returnfd")){
 if(Gotya == 0&Gotya2 == 0){
 Dropped = 0   
}   
stoptimer("returnfd")
}
}
