@name GreatCar 5 new
@model models/hunter/plates/plate2x3.mdl
@inputs [Chair1 Chair2]:entity
@outputs Speed
@persist TickRate WheelAng:vector2 Wheel_Vel
@persist Adometer Adometer_OldPos:vector #Adometer
@persist Holo_Rulb Holo_WheelOffset Holo_Bonnet Holo_Trunk Holo_Subwoofer Holo_Update #Holo
@persist Mode_Trunk Mode_Bonnet Mode_MegaBoost Mode_Fly Mode_LowHeight Mode_Music #Modes
@persist Data:gtable [Entity Holo_Entity]:entity #Const
@persist Stay_Pos:vector #Pos sray

interval(10)
#Hologramms
if(first()|dupefinished()){
    
    #Run on ...
#    runOnTick(1)
    runOnChat(1)
    
    
    #Adometer
    timer("adometer",1)
    
    
    #Constants
    Data = gTable("greatcar")
    if(!Data["Valid",number]){
        Data["Valid",number] = 1
        
        #Paremeteres
        Data["Height",number] = 19
        Data["Mass",number] = 800
        Data["Side",number] = -1
        #ifdef soundURLload(string,string,number,number,entity)
            Data["Mode_Subwoofer",number] = 1
        #endif
        Data["Mode_Water",number] = 0
        
        #Color
        #Data["Color",vector] = vec(32,64,128) #Blue
        #Data["Color",vector] = vec(35) #Black
        #Data["Color",vector] = vec(255,200,50) #Yellow
        Data["Color",vector] = vec(255,93,0) #Orange
        #Data["Color",vector] = vec(77,112,157) #White-Blue
        #Data["Color",vector] = vec(96,29,52) #Dark-Red
        Entity:setColor(Data["Color",vector])
    }
    Entity = entity()
    
    #Persist
    TickRate = 1
    Mode_LowHeight = 1
    Mode_Fly = 1
    Holo_Update = 1
    
    
    #Settings
    Entity:setAlpha(0)
    Entity:setMass(Data["Mass",number])
    
    Chair1:setMass(0)
    Chair2:setMass(0)
    #Chair1:setPos(Entity:toWorld(vec( 18,8,0)))
    #Chair2:setPos(Entity:toWorld(vec(-18,8,0)))
    #Entity:setPos(Chair2:toWorld(vec(-18,8,0)))
}
    
if(Holo_Update){
    
    Holo_Update = 0
    
    #Holo colors
    Color = Data["Color",vector]
    Color_Inside = vec(60)
    Color_Interior = vec(220,195,160)
    #Color_Interior = vec(110)
    Color_Red = vec(255,0,0)
    Color_Disk = vec(150)
    Color_Wheels = vec(20)
    Color_Lights = vec(255)
    Color_Glass = vec4(150,180,150,50)
    Color_Speaker = Color_Interior*0.9
    
    Material_Shiny = "models/shiny"
    Material_Mirror = "debug/env_cubemap_model"
    Material_Glass = "debug/env_cubemap_model"
    
    Chair1:setColor(Color_Interior)
    Chair2:setColor(Color_Interior)
    
    H = 0
    
    
    #Main
    H++
    holoCreate(H)
    holoPos(H, Entity:toWorld(vec(0,-2,0)))
    holoAng(H, Entity:angles())
    holoScale(H, vec(4,11.5,0.25))
    holoParent(H, Entity)
    holoColor(H, Color_Inside)
    holoClipEnabled(H,1)
    holoClip(H, vec(0,-63,0),vec(0,1,0),0)
    
    Holo_Entity = holoEntity(H)

    H++
    holoCreate(H)
    holoPos(H, Holo_Entity:toWorld(vec(0,1,0)))
    holoAng(H, Holo_Entity:angles())
    holoScale(H, vec(5.9,4,0.25))
    holoParent(H, 1)
    holoColor(H, Color_Inside)

    H++
    holoCreate(H)
    holoPos(H, holoEntity(H-1):toWorld(vec(0,0,1.6)))
    holoAng(H, holoEntity(H-1):angles())
    holoScale(H, holoScale(H-1))
    holoParent(H, H-1)
    holoColor(H, Color_Interior)
    holoModel(H, "plane")
    
    
    
    #Back
    H++
    holoCreate(H)
    holoPos(H, Holo_Entity:toWorld(vec(0,70.2,14)))
    holoAng(H, Holo_Entity:toWorld(ang(0,0,85)))
    holoScale(H, vec(6,2.6,0.25))
    holoParent(H, 1)
    holoColor(H, Color)
    holoMaterial(H, Material_Shiny)

    H++
    holoCreate(H)
    holoPos(H, holoEntity(H-1):toWorld(vec(0,0,1.6)))
    holoAng(H, holoEntity(H-1):angles())
    holoScale(H, holoScale(H-1))
    holoParent(H, H-1)
    holoColor(H, Color_Inside)
    holoModel(H, "plane")
    
    H++
    holoCreate(H)
    holoPos(H, Holo_Entity:toWorld(vec(27,47.5,26.2)))
    holoAng(H, Holo_Entity:toWorld(ang(0,0,5)))
    holoScale(H, vec(1,3.9,0.25))
    holoParent(H, 1)
    holoColor(H, Color)
    holoMaterial(H, Material_Shiny)
    
    H++
    holoCreate(H)
    holoPos(H, Holo_Entity:toWorld(vec(-27,47.5,26.2)))
    holoAng(H, Holo_Entity:toWorld(ang(0,0,5)))
    holoScale(H, vec(1,3.9,0.25))
    holoParent(H, 1)
    holoColor(H, Color)
    holoMaterial(H, Material_Shiny)
    
    H++
    holoCreate(H)
    holoPos(H, Holo_Entity:toWorld(vec(0,25.5,6*2.25-1.4)))
    holoAng(H, Holo_Entity:toWorld(ang(0,0,90)))
    holoScale(H, vec(5.98,2.25,0.25))
    holoParent(H, 1)
    holoColor(H, Color_Inside)

    H++
    holoCreate(H)
    holoPos(H, holoEntity(H-1):toWorld(vec(0,0,1.6)))
    holoAng(H, holoEntity(H-1):angles())
    holoScale(H, holoScale(H-1))
    holoParent(H, H-1)
    holoColor(H, Color_Interior)
    holoModel(H, "plane")
    
    H++
    holoCreate(H)
    holoPos(H, Holo_Entity:toWorld(vec(24-1.6,48.5,16.5)))
    holoAng(H, Holo_Entity:toWorld(ang(90,0,0)))
    holoScale(H, vec(3,3.6,0.25))
    holoParent(H, 1)
    holoColor(H, Color_Inside)
    holoClipEnabled(H,1)
    holoClip(H, vec(-11,0,0),vec(1,0.08,0),0)
    
    H++
    holoCreate(H)
    holoPos(H, Holo_Entity:toWorld(vec(-24+1.6,48.5,16.5)))
    holoAng(H, Holo_Entity:toWorld(ang(90,0,0)))
    holoScale(H, vec(3,3.6,0.25))
    holoParent(H, 1)
    holoColor(H, Color_Inside)
    holoClipEnabled(H,1)
    holoClip(H, vec(-11,0,0),vec(1,0.08,0),0)
    
    
    
    #Subwoofer
    if(Data["Mode_Subwoofer",number]){
        H++
        holoCreate(H)
        holoPos(H, Holo_Entity:toWorld(vec(0,43.2,25)))
        holoAng(H, Holo_Entity:toWorld(ang(0,0,5)))
        Sin45 = (2^-0.5)
        holoScale(H, vec(4,4,0.25*Sin45)/Sin45)
        holoParent(H, 1)
        holoColor(H, Color_Inside)
        holoModel(H, "hq_tube_thick")
        for(I=1,4){holoClipEnabled(H,I,1)}
        holoClip(H,1, holoScale(H)*Sin45*vec(6,0,0),vec(-1,0,0),0)
        holoClip(H,2, holoScale(H)*Sin45*vec(-6,0,0),vec(1,0,0),0)
        holoClip(H,3, holoScale(H)*Sin45*vec(0,7,0),vec(0,-1,0),0)
        holoClip(H,4, holoScale(H)*Sin45*vec(0,-4.5,0),vec(0,1,0),0)
        
        H++
        Holo_Subwoofer = H
        holoCreate(H)
        holoPos(H, holoEntity(H-1):toWorld(vec(0,0,holoScale(H-1):z()*6)))
        holoAng(H, holoEntity(H-1):toWorld(ang(180,0,0)))
        holoScale(H, holoScale(H-1)*vec(0.425,0.425,2))
        holoParent(H, H-1)
        holoColor(H, Color_Inside)
        holoModel(H, "hq_hdome_thin")
        
        H++
        holoCreate(H)
        holoPos(H, holoEntity(H-2):toWorld(vec(0,0,holoScale(H-2):z()*4)))
        holoAng(H, holoEntity(H-2):toWorld(ang(180,0,0)))
        holoScale(H, holoScale(H-2)*vec(0.5,0.5,8))
        holoParent(H, H-2)
        holoColor(H, Color_Inside)
        holoModel(H, "hq_torus")
    }
    
    
    
    #Back lights
    H++
    holoCreate(H)
    holoPos(H, Holo_Entity:toWorld(vec(0,72.7,22)))
    holoAng(H, Holo_Entity:toWorld(ang(0,0,-95)))
    holoScale(H, vec(5,0.25,0.25))
    holoParent(H, 1)
    holoColor(H, Color_Red)
    holoModel(H, "plane")
    
    H++
    holoCreate(H)
    holoPos(H, Holo_Entity:toWorld(vec(7.5,71.5,10.5)))
    holoAng(H, Holo_Entity:toWorld(ang(0,0,-95)))
    holoScale(H, vec(0.25,2,0.25))
    holoParent(H, 1)
    holoColor(H, Color_Lights)
    holoModel(H, "plane")
    
    H++
    holoCreate(H)
    holoPos(H, Holo_Entity:toWorld(vec(-7.5,71.5,10.5)))
    holoAng(H, Holo_Entity:toWorld(ang(0,0,-95)))
    holoScale(H, vec(0.25,2,0.25))
    holoParent(H, 1)
    holoColor(H, Color_Lights)
    holoModel(H, "plane")
    
    
    
    #Front
    H++
    holoCreate(H)
    holoPos(H, Holo_Entity:toWorld(vec(0,-66.6,6.24)))
    holoAng(H, Holo_Entity:toWorld(ang(65,90,0)))
    holoScale(H, vec(2,6,0.25))
    holoParent(H, 1)
    holoColor(H, Color)
    holoMaterial(H, Material_Shiny)

    H++
    holoCreate(H)
    holoPos(H, holoEntity(H-1):toWorld(vec(0,0,1.6)))
    holoAng(H, holoEntity(H-1):angles())
    holoScale(H, holoScale(H-1))
    holoParent(H, H-1)
    holoColor(H, Color_Inside)
    holoModel(H, "plane")
    
    H++
    Holo_Bonnet = H
    holoCreate(H)
    if(Mode_Bonnet){
        holoPos(Holo_Bonnet, Holo_Entity:toWorld(vec(0,-40,40)))
                holoAng(Holo_Bonnet, Holo_Entity:toWorld(ang(0,0,-50)))
    }else{
        holoPos(Holo_Bonnet, Holo_Entity:toWorld(vec(0,-46.5,18.5)))
        holoAng(Holo_Bonnet, Holo_Entity:toWorld(ang(0,0,5)))
    }
    holoScale(H, vec(6,4,0.25))
    holoParent(H, 1)
    holoColor(H, Color)
    holoMaterial(H, Material_Shiny)
    
    H++
    holoCreate(H)
    holoPos(H, Holo_Entity:toWorld(vec(22.5,-46.5,7)))
    holoAng(H, Holo_Entity:toWorld(ang(0,0,5)))
    holoScale(H, vec(0.25,4,2))
    holoParent(H, 1)
    holoColor(H, Color_Inside)
    holoClipEnabled(H,1,1)
    holoClip(H,1, vec(0,0,-7.5),vec(0,0.08,1),0)
    holoClipEnabled(H,2,1)
    holoClip(H,2, vec(0,-20,0),vec(0,1,0.35),0)
    
    H++
    holoCreate(H)
    holoPos(H, Holo_Entity:toWorld(vec(-22.5,-46.5,7)))
    holoAng(H, Holo_Entity:toWorld(ang(0,0,5)))
    holoScale(H, vec(0.25,4,2))
    holoParent(H, 1)
    holoColor(H, Color_Inside)
    holoClipEnabled(H,1,1)
    holoClip(H,1, vec(0,0,-7.5),vec(0,0.08,1),0)
    holoClipEnabled(H,2,1)
    holoClip(H,2, vec(0,-20,0),vec(0,1,0.35),0)
    
    
    
    #Lights
    H++
    holoCreate(H)
    holoPos(H, Holo_Entity:toWorld(vec(-23,-71,12)))
    holoAng(H, Holo_Entity:toWorld(ang(90+25,-90,0)))
    holoScale(H, vec(0.25,2,0.25))
    holoParent(H, 1)
    holoColor(H, Color_Lights)
    holoModel(H, "plane")

    H++
    holoCreate(H)
    holoPos(H, Holo_Entity:toWorld(vec(23,-71,12)))
    holoAng(H, Holo_Entity:toWorld(ang(90+25,-90,0)))
    holoScale(H, vec(0.25,2,0.25))
    holoParent(H, 1)
    holoColor(H, Color_Lights)
    holoModel(H, "plane")
    
    H++
    holoCreate(H)
    holoPos(H, Holo_Entity:toWorld(vec(-20,-66.5,0.5)))
    holoAng(H, Holo_Entity:toWorld(ang(90+25,-90,0)))
    holoScale(H, vec(0.25,1,0.25))
    holoParent(H, 1)
    holoColor(H, Color_Lights)
    holoModel(H, "plane")
    
    H++
    holoCreate(H)
    holoPos(H, Holo_Entity:toWorld(vec(20,-66.5,0.5)))
    holoAng(H, Holo_Entity:toWorld(ang(90+25,-90,0)))
    holoScale(H, vec(0.25,1,0.25))
    holoParent(H, 1)
    holoColor(H, Color_Lights)
    holoModel(H, "plane")
    
    
    
    #Center longs
    
    #Left
    H++
    holoCreate(H)
    holoPos(H, Holo_Entity:toWorld(vec(36-1.5,1.5,21)))
    holoAng(H, Holo_Entity:toWorld(ang(0,0,5)))
    holoScale(H, vec(0.25,12,0.5))
    holoParent(H, 1)
    holoColor(H, Color)
    holoMaterial(H, Material_Shiny)
    holoClipEnabled(H,1,1)
    holoClip(H,1, vec(0,-25,0),vec(0,1,0),0)
    holoClipEnabled(H,2,1)
    holoClip(H,2, vec(0,69.5,0),vec(0,-1,0.2),0)
    
    #Right
    H++
    holoCreate(H)
    holoPos(H, Holo_Entity:toWorld(vec(-36+1.5,1.5,21)))
    holoAng(H, Holo_Entity:toWorld(ang(0,0,5)))
    holoScale(H, vec(0.25,12,0.5))
    holoParent(H, 1)
    holoColor(H, Color)
    holoMaterial(H, Material_Shiny)
    holoClipEnabled(H,1,1)
    holoClip(H,1, vec(0,-25,0),vec(0,1,0),0)
    holoClipEnabled(H,2,1)
    holoClip(H,2, vec(0,69.5,0),vec(0,-1,0.2),0)
    
    
    
    #Center
    
    #Up
    H++
    holoCreate(H)
    holoPos(H, Holo_Entity:toWorld(vec(0,20,46)))
    holoAng(H, Holo_Entity:toWorld(ang(0,0,7)))
    holoScale(H, vec(4,3,0.1))
    holoParent(H, 1)
    holoColor(H, Color)
    holoMaterial(H, Material_Shiny)
    
    H++
    holoCreate(H)
    holoPos(H, holoEntity(H-1):toWorld(vec(0,0,-0.7)))
    holoAng(H, holoEntity(H-1):toWorld(ang(180,0,0)))
    holoScale(H, holoScale(H-1))
    holoParent(H, H-1)
    holoColor(H, Color_Interior)
    holoModel(H, "plane")
    
    #Panel
    H++
    holoCreate(H)
    holoPos(H, Holo_Entity:toWorld(vec(0,-19,15.8)))
    holoAng(H, Holo_Entity:toWorld(ang(90,-90,0)))
    holoScale(H, vec(1,5.9,0.6))
    holoParent(H, 1)
    holoColor(H, Color_Interior)
    
    #Front
    H++
    holoCreate(H)
    holoPos(H, Holo_Entity:toWorld(vec(0,-22,6*1.9-1.4)))
    holoAng(H, Holo_Entity:toWorld(ang(90,-90,180)))
    holoScale(H, vec(1.9,5.9,0.25))
    holoParent(H, 1)
    holoColor(H, Color_Inside)

    H++
    holoCreate(H)
    holoPos(H, holoEntity(H-1):toWorld(vec(0,0,1.6)))
    holoAng(H, holoEntity(H-1):angles())
    holoScale(H, holoScale(H-1))
    holoParent(H, H-1)
    holoColor(H, Color_Interior)
    holoModel(H, "plane")
    
    
    #Steering wheel
    H++
    Holo_Rulb = H
    holoCreate(H)    
    holoPos(H, holoEntity(H-2):toWorld(vec(-7,17*Data["Side",number],12)))
    holoParent(H, H-2)
    holoColor(H, Color_Interior)
    holoModel(H,"torus3")
    
    H++
    holoCreate(H)
    holoPos(H, holoEntity(H-1):toWorld(vec(1,0,0)))
    holoAng(H, holoEntity(H-1):angles())
    holoScale(H, vec(0.4,1,0.2))
    holoParent(H, H-1)
    holoColor(H, Color_Interior)
    holoModel(H, "rcube")
    
    
    
    #Glass
    
    #Left
    H++
    holoCreate(H)
    holoPos(H, Holo_Entity:toWorld(vec(29,18,35)))
    holoAng(H, Holo_Entity:toWorld(ang(90-25,13,14)))
    holoScale(H, vec(2,7.1,0.05))
    holoParent(H, 1)
    holoColor(H, Color_Glass)
    holoMaterial(H, Material_Glass)
    holoClipEnabled(H,1,1)
    holoClip(H,1, vec(0,-32,0),vec(1.35,1,0),0)
    holoClipEnabled(H,2,1)
    holoClip(H,2, vec(0,31.3,0),vec(0.83,-1,0),0)
    
    #Right
    H++
    holoCreate(H)
    holoPos(H, Holo_Entity:toWorld(vec(-29.5,18,35)))
    holoAng(H, Holo_Entity:toWorld(ang(90+25,-13,-14)))
    holoScale(H, vec(2,7.1,0.05))
    holoParent(H, 1)
    holoColor(H, Color_Glass)
    holoMaterial(H, Material_Glass)
    holoClipEnabled(H,1,1)
    holoClip(H,1, vec(0,-32,0),vec(1.35,1,0),0)
    holoClipEnabled(H,2,1)
    holoClip(H,2, vec(0,31.3,0),vec(0.83,-1,0),0)
    
    #Forward
    H++
    holoCreate(H)
    holoPos(H, Holo_Entity:toWorld(vec(0,-11,33)))
    holoAng(H, Holo_Entity:toWorld(ang(-40,90,0)))
    holoScale(H, vec(2.9,6,0.05))
    holoParent(H, 1)
    holoColor(H, Color_Glass)
    holoMaterial(H, Material_Glass)
    holoClipEnabled(H,1,1)
    holoClip(H,1, vec(0,-29,0),vec(-0.25,1,0),0)
    holoClipEnabled(H,2,1)
    holoClip(H,2, vec(0,29,0),vec(-0.25,-1,0),0)
    
    #Back
    H++
    Holo_Trunk = H
    holoCreate(H)
    if(Mode_Trunk){
        holoPos(Holo_Trunk, Holo_Entity:toWorld(vec(0,52,53)))
        holoAng(Holo_Trunk, Holo_Entity:toWorld(ang(-20,90,0)))
    }else{
        holoPos(Holo_Trunk, Holo_Entity:toWorld(vec(0,49,38)))
        holoAng(Holo_Trunk, Holo_Entity:toWorld(ang(42,90,0)))
    }
    holoScale(H, vec(2.5,6,0.05))
    holoParent(H, 1)
    holoColor(H, Color_Glass)
    holoMaterial(H, Material_Glass)
    holoClipEnabled(H,1,1)
    holoClip(H,1, vec(0,-29.5,0),vec(0.32,1,0),0)
    holoClipEnabled(H,2,1)
    holoClip(H,2, vec(0,29.5,0),vec(0.32,-1,0),0)
    
    #Trunk
    
    H++
    holoCreate(H)
    holoPos(H, Holo_Entity:toWorld(vec(0,65,27.8)))
    holoAng(H, Holo_Entity:toWorld(ang(0,0,5)))
    holoScale(H, vec(3.5,1,0.25))
    holoParent(H, H-1)
    holoColor(H, Color)
    holoMaterial(H, Material_Shiny)
    
    
    #Left
    
    #Outside
    H++
    holoCreate(H)
    holoPos(H, Holo_Entity:toWorld(vec(36-1.5,1.6,6*2.3-1.4)))
    holoAng(H, Holo_Entity:toWorld(ang(-90,0,0)))
    holoScale(H, vec(2.3,4.18,0.25))
    holoParent(H, 1)
    holoColor(H, Color)
    holoMaterial(H, Material_Shiny)
    holoClipEnabled(H,1)
    holoClip(H, vec(11,0,0),vec(-1,tan(5),0),0)
    
    #Inside
    H++
    holoCreate(H)
    holoPos(H, holoEntity(H-1):toWorld(vec(0,0,1.6)))
    holoAng(H, holoEntity(H-1):angles())
    holoScale(H, holoScale(H-1))
    holoParent(H, H-1)
    holoColor(H, Color_Interior)
    holoModel(H, "plane")
    holoClipEnabled(H,1)
    holoClip(H, vec(11,0,0),vec(-1,tan(5),0),0)
    
    H++
    holoCreate(H)
    holoPos(H, holoEntity(H-1):toWorld(vec(-4,-10,0.1)))
    holoAng(H, holoEntity(H-1):angles())
    holoParent(H, H-1)
    holoColor(H, Color_Speaker)
    holoModel(H, "cplane")
    
    #Mirror
    H++
    holoCreate(H)
    holoPos(H, Holo_Entity:toWorld(vec(40,-18,25)))
    holoAng(H, Holo_Entity:toWorld(ang(0,20,90)))
    holoScale(H, vec(0.8,0.5,0.05))
    holoParent(H, 1)
    holoColor(H, Color)
    holoMaterial(H, Material_Shiny)
    
    H++
    holoCreate(H)
    holoPos(H, holoEntity(H-1):toWorld(vec(0,0,-0.4)))
         holoAng(Holo_Bonnet, Holo_Entity:toWorld(ang(0,0,-50)))
    }else{
        holoPos(Holo_Bonnet, Holo_Entity:toWorld(vec(0,-46.5,18.5)))
        holoAng(Holo_Bonnet, Holo_Entity:toWorld(ang(0,0,5)))
    }
    holoScale(H, vec(6,4,0.25))
    holoParent(H, 1)
    holoColor(H, Color)
    holoMaterial(H, Material_Shiny)
    
    H++
    holoCreate(H)
    holoPos(H, Holo_Entity:toWorld(vec(22.5,-46.5,7)))
    holoAng(H, Holo_Entity:toWorld(ang(0,0,5)))
    holoScale(H, vec(0.25,4,2))
    holoParent(H, 1)
    holoColor(H, Color_Inside)
    holoClipEnabled(H,1,1)
    holoClip(H,1, vec(0,0,-7.5),vec(0,0.08,1),0)
    holoClipEnabled(H,2,1)
    holoClip(H,2, vec(0,-20,0),vec(0,1,0.35),0)
    
    H++
    holoCreate(H)
    holoPos(H, Holo_Entity:toWorld(vec(-22.5,-46.5,7)))
    holoAng(H, Holo_Entity:toWorld(ang(0,0,5)))
    holoScale(H, vec(0.25,4,2))
    holoParent(H, 1)
    holoColor(H, Color_Inside)
    holoClipEnabled(H,1,1)
    holoClip(H,1, vec(0,0,-7.5),vec(0,0.08,1),0)
    holoClipEnabled(H,2,1)
    holoClip(H,2, vec(0,-20,0),vec(0,1,0.35),0)
    
    
    
    #Lights
    H++
    holoCreate(H)
    holoPos(H, Holo_Entity:toWorld(vec(-23,-71,12)))
    holoAng(H, Holo_Entity:toWorld(ang(90+25,-90,0)))
    holoScale(H, vec(0.25,2,0.25))
    holoParent(H, 1)
    holoColor(H, Color_Lights)
    holoModel(H, "plane")

    H++
    holoCreate(H)
    holoPos(H, Holo_Entity:toWorld(vec(23,-71,12)))
    holoAng(H, Holo_Entity:toWorld(ang(90+25,-90,0)))
    holoScale(H, vec(0.25,2,0.25))
    holoParent(H, 1)
    holoColor(H, Color_Lights)
    holoModel(H, "plane")
    
    H++
    holoCreate(H)
    holoPos(H, Holo_Entity:toWorld(vec(-20,-66.5,0.5)))
    holoAng(H, Holo_Entity:toWorld(ang(90+25,-90,0)))
    holoScale(H, vec(0.25,1,0.25))
    holoParent(H, 1)
    holoColor(H, Color_Lights)
    holoModel(H, "plane")
    
    H++
    holoCreate(H)
    holoPos(H, Holo_Entity:toWorld(vec(20,-66.5,0.5)))
    holoAng(H, Holo_Entity:toWorld(ang(90+25,-90,0)))
    holoScale(H, vec(0.25,1,0.25))
    holoParent(H, 1)
    holoColor(H, Color_Lights)
    holoModel(H, "plane")
    
    
    
    #Center longs
    
    #Left
    H++
    holoCreate(H)
    holoPos(H, Holo_Entity:toWorld(vec(36-1.5,1.5,21)))
    holoAng(H, Holo_Entity:toWorld(ang(0,0,5)))
    holoScale(H, vec(0.25,12,0.5))
    holoParent(H, 1)
    holoColor(H, Color)
    holoMaterial(H, Material_Shiny)
    holoClipEnabled(H,1,1)
    holoClip(H,1, vec(0,-25,0),vec(0,1,0),0)
    holoClipEnabled(H,2,1)
    holoClip(H,2, vec(0,69.5,0),vec(0,-1,0.2),0)
    
    #Right
    H++
    holoCreate(H)
    holoPos(H, Holo_Entity:toWorld(vec(-36+1.5,1.5,21)))
    holoAng(H, Holo_Entity:toWorld(ang(0,0,5)))
    holoScale(H, vec(0.25,12,0.5))
    holoParent(H, 1)
    holoColor(H, Color)
    holoMaterial(H, Material_Shiny)
    holoClipEnabled(H,1,1)
    holoClip(H,1, vec(0,-25,0),vec(0,1,0),0)
    holoClipEnabled(H,2,1)
    holoClip(H,2, vec(0,69.5,0),vec(0,-1,0.2),0)
    
    
    
    #Center
    
    #Up
    H++
    holoCreate(H)
    holoPos(H, Holo_Entity:toWorld(vec(0,20,46)))
    holoAng(H, Holo_Entity:toWorld(ang(0,0,7)))
    holoScale(H, vec(4,3,0.1))
    holoParent(H, 1)
    holoColor(H, Color)
    holoMaterial(H, Material_Shiny)
    
    H++
    holoCreate(H)
    holoPos(H, holoEntity(H-1):toWorld(vec(0,0,-0.7)))
    holoAng(H, holoEntity(H-1):toWorld(ang(180,0,0)))
    holoScale(H, holoScale(H-1))
    holoParent(H, H-1)
    holoColor(H, Color_Interior)
    holoModel(H, "plane")
    
    #Panel
    H++
    holoCreate(H)
    holoPos(H, Holo_Entity:toWorld(vec(0,-19,15.8)))
    holoAng(H, Holo_Entity:toWorld(ang(90,-90,0)))
    holoScale(H, vec(1,5.9,0.6))
    holoParent(H, 1)
    holoColor(H, Color_Interior)
    
    #Front
    H++
    holoCreate(H)
    holoPos(H, Holo_Entity:toWorld(vec(0,-22,6*1.9-1.4)))
    holoAng(H, Holo_Entity:toWorld(ang(90,-90,180)))
    holoScale(H, vec(1.9,5.9,0.25))
    holoParent(H, 1)
    holoColor(H, Color_Inside)

    H++
    holoCreate(H)
    holoPos(H, holoEntity(H-1):toWorld(vec(0,0,1.6)))
    holoAng(H, holoEntity(H-1):angles())
    holoScale(H, holoScale(H-1))
    holoParent(H, H-1)
    holoColor(H, Color_Interior)
    holoModel(H, "plane")
    
    
    #Steering wheel
    H++
    Holo_Rulb = H
    holoCreate(H)    
    holoPos(H, holoEntity(H-2):toWorld(vec(-7,17*Data["Side",number],12)))
    holoParent(H, H-2)
    holoColor(H, Color_Interior)
    holoModel(H,"torus3")
    
    H++
    holoCreate(H)
    holoPos(H, holoEntity(H-1):toWorld(vec(1,0,0)))
    holoAng(H, holoEntity(H-1):angles())
    holoScale(H, vec(0.4,1,0.2))
    holoParent(H, H-1)
    holoColor(H, Color_Interior)
    holoModel(H, "rcube")
    
    
    
    #Glass
    
    #Left
    H++
    holoCreate(H)
    holoPos(H, Holo_Entity:toWorld(vec(29,18,35)))
    holoAng(H, Holo_Entity:toWorld(ang(90-25,13,14)))
    holoScale(H, vec(2,7.1,0.05))
    holoParent(H, 1)
    holoColor(H, Color_Glass)
    holoMaterial(H, Material_Glass)
    holoClipEnabled(H,1,1)
    holoClip(H,1, vec(0,-32,0),vec(1.35,1,0),0)
    holoClipEnabled(H,2,1)
    holoClip(H,2, vec(0,31.3,0),vec(0.83,-1,0),0)
    
    #Right
    H++
    holoCreate(H)
    holoPos(H, Holo_Entity:toWorld(vec(-29.5,18,35)))
    holoAng(H, Holo_Entity:toWorld(ang(90+25,-13,-14)))
    holoScale(H, vec(2,7.1,0.05))
    holoParent(H, 1)
    holoColor(H, Color_Glass)
    holoMaterial(H, Material_Glass)
    holoClipEnabled(H,1,1)
    holoClip(H,1, vec(0,-32,0),vec(1.35,1,0),0)
    holoClipEnabled(H,2,1)
    holoClip(H,2, vec(0,31.3,0),vec(0.83,-1,0),0)
    
    #Forward
    H++
    holoCreate(H)
    holoPos(H, Holo_Entity:toWorld(vec(0,-11,33)))
    holoAng(H, Holo_Entity:toWorld(ang(-40,90,0)))
    holoScale(H, vec(2.9,6,0.05))
    holoParent(H, 1)
    holoColor(H, Color_Glass)
    holoMaterial(H, Material_Glass)
    holoClipEnabled(H,1,1)
    holoClip(H,1, vec(0,-29,0),vec(-0.25,1,0),0)
    holoClipEnabled(H,2,1)
    holoClip(H,2, vec(0,29,0),vec(-0.25,-1,0),0)
    
    #Back
    H++
    Holo_Trunk = H
    holoCreate(H)
    if(Mode_Trunk){
        holoPos(Holo_Trunk, Holo_Entity:toWorld(vec(0,52,53)))
        holoAng(Holo_Trunk, Holo_Entity:toWorld(ang(-20,90,0)))
    }else{
        holoPos(Holo_Trunk, Holo_Entity:toWorld(vec(0,49,38)))
        holoAng(Holo_Trunk, Holo_Entity:toWorld(ang(42,90,0)))
    }
    holoScale(H, vec(2.5,6,0.05))
    holoParent(H, 1)
    holoColor(H, Color_Glass)
    holoMaterial(H, Material_Glass)
    holoClipEnabled(H,1,1)
    holoClip(H,1, vec(0,-29.5,0),vec(0.32,1,0),0)
    holoClipEnabled(H,2,1)
    holoClip(H,2, vec(0,29.5,0),vec(0.32,-1,0),0)
    
    #Trunk
    
    H++
    holoCreate(H)
    holoPos(H, Holo_Entity:toWorld(vec(0,65,27.8)))
    holoAng(H, Holo_Entity:toWorld(ang(0,0,5)))
    holoScale(H, vec(3.5,1,0.25))
    holoParent(H, H-1)
    holoColor(H, Color)
    holoMaterial(H, Material_Shiny)
    
    
    #Left
    
    #Outside
    H++
    holoCreate(H)
    holoPos(H, Holo_Entity:toWorld(vec(36-1.5,1.6,6*2.3-1.4)))
    holoAng(H, Holo_Entity:toWorld(ang(-90,0,0)))
    holoScale(H, vec(2.3,4.18,0.25))
    holoParent(H, 1)
    holoColor(H, Color)
    holoMaterial(H, Material_Shiny)
    holoClipEnabled(H,1)
    holoClip(H, vec(11,0,0),vec(-1,tan(5),0),0)
    
    #Inside
    H++
    holoCreate(H)
    holoPos(H, holoEntity(H-1):toWorld(vec(0,0,1.6)))
    holoAng(H, holoEntity(H-1):angles())
    holoScale(H, holoScale(H-1))
    holoParent(H, H-1)
    holoColor(H, Color_Interior)
    holoModel(H, "plane")
    holoClipEnabled(H,1)
    holoClip(H, vec(11,0,0),vec(-1,tan(5),0),0)
    
    H++
    holoCreate(H)
    holoPos(H, holoEntity(H-1):toWorld(vec(-4,-10,0.1)))
    holoAng(H, holoEntity(H-1):angles())
    holoParent(H, H-1)
    holoColor(H, Color_Speaker)
    holoModel(H, "cplane")
    
    #Mirror
    H++
    holoCreate(H)
    holoPos(H, Holo_Entity:toWorld(vec(-40,-18,25)))
    holoAng(H, Holo_Entity:toWorld(ang(0,-20,90)))
    holoScale(H, vec(0.8,0.5,0.05))
    holoParent(H, 1)
    holoColor(H, Color)
    holoMaterial(H, Material_Shiny)
    
    H++
    holoCreate(H)
    holoPos(H, holoEntity(H-1):toWorld(vec(0,0,-0.4)))
    holoAng(H, holoEntity(H-1):angles())
    holoScale(H, holoScale(H-1))
    holoParent(H, H-1)
    holoMaterial(H, Material_Mirror)
    holoModel(H, "plane")
    
    
    
    #Wheels
    Holo_WheelOffset = H
    #Left up
    H++
    holoCreate(H)    
    holoPos(H, Entity:toWorld(vec(33,-45,0)))
    holoAng(H, Entity:toWorld(ang(90,0,0)))
    holoScale(H, vec(2.5,2.5,1))
    holoParent(H, 1)
    holoColor(H, Color_Wheels)
    holoModel(H,"hq_tube")
    
    #Right up
    H++
    holoCreate(H)    
    holoPos(H, Entity:toWorld(vec(-33,-45,0)))
    holoAng(H, Entity:toWorld(ang(-90,0,0)))
    holoScale(H, vec(2.5,2.5,1))
    holoParent(H, 1)
    holoColor(H, Color_Wheels)
    holoModel(H,"hq_tube")
    
    #Left back
    H++
    holoCreate(H)    
    holoPos(H, Entity:toWorld(vec(31.5,45,0)))
    holoAng(H, Entity:toWorld(ang(90,0,0)))
    holoScale(H, vec(3,3,1.25))
    holoParent(H, 1)
    holoColor(H, Color_Wheels)
    holoModel(H,"hq_tube")
    
    #Right back
    H++
    holoCreate(H)
    holoPos(H, Entity:toWorld(vec(-31.5,45,0)))
    holoAng(H, Entity:toWorld(ang(-90,0,0)))
    holoScale(H, vec(3,3,1.25))
    holoParent(H, 1)
    holoColor(H, Color_Wheels)
    holoModel(H,"hq_tube")
    
    local H3 = H-3
    
    for(I=0,3){
        local H2 = H3+I
        
        H++
        holoCreate(H)    
        holoPos(H, holoEntity(H2):pos())
        holoAng(H, holoEntity(H2):angles())
        holoScale(H, holoScale(H2)*vec(0.9,0.9,0.95))
        holoParent(H, H2)
        holoColor(H, Color_Disk)
        holoMaterial(H, Material_Shiny)
        holoModel(H,"hq_tube")
        
        #[H++
        holoCreate(H)    
        holoPos(H, holoEntity(H2):toWorld(vec(0,0,-4.2)))
        holoAng(H, holoEntity(H2):angles())
        holoScale(H, holoScale(H2)*vec(0.95,0.95,0)+vec(0,0,0.25))
        holoParent(H, H2)
        holoColor(H, ColorDisk)
        holoModel(H,"hq_cylinder")]#
        
        for(J=1,5){
            local Size = holoScale(H2):x()
            H++
            holoCreate(H)    
            holoPos(H, holoEntity(H2):toWorld(vec(sin(J*72)*Size*3,cos(J*72)*Size*3*0.9,0)))
            holoAng(H, holoEntity(H2):toWorld(ang(0,90-J*72,0)))
            holoScale(H, vec(Size*0.48,0.5,0.25))
            holoParent(H, H2)
            holoColor(H, Color_Disk)
            holoModel(H, "plane")
            holoMaterial(H, Material_Shiny)
            holoClipEnabled(H,1,1)
            holoClip(H,1, vec(0,-1.8,0),vec(-0.1,1,0),0)
            holoClipEnabled(H,2,1)
            holoClip(H,2, vec(0,1.8,0),vec(-0.1,-1,0),0)
        }
    }
    
    if(first()){print("This car use "+H+" hologramms. Type \"!greatcar help\" for get commands list.")
}



#Chat commands
if(chatClk(owner())){
    LastSaid_Raw = lastSaid()
    LastSaid_Raw=LastSaid_Raw:replaceRE(" +", " ")
    if(LastSaid_Raw[1]==" "){LastSaid_Raw=LastSaid_Raw:right(LastSaid_Raw:length()-1)}
    
    LastSaid = LastSaid_Raw:explode(" ")
    if(LastSaid[1,string]:lower()=="!greatcar"){
        hideChat(1)
        switch(LastSaid[2,string]:lower()){
            case "height",
                Data["Height",number] = LastSaid[3,string]:toNumber()
            break
            case "megaboost",
                Mode_MegaBoost=!Mode_MegaBoost
            print("Mega boost " + (Mode_MegaBoost ? "on" : "off"))
            break
            case "bonnet",
                Mode_Bonnet=!Mode_Bonnet
                if(Mode_Bonnet){
                    holoPos(Holo_Bonnet, Holo_Entity:toWorld(vec(0,-40,40)))
                    holoAng(Holo_Bonnet, Holo_Entity:toWorld(ang(0,0,-50)))
                }else{
                    holoPos(Holo_Bonnet, Holo_Entity:toWorld(vec(0,-46.5,18.5)))
                    holoAng(Holo_Bonnet, Holo_Entity:toWorld(ang(0,0,5)))
                }
            break
            case "trunk",
                Mode_Trunk=!Mode_Trunk
                if(Mode_Trunk){
                    holoPos(Holo_Trunk, Holo_Entity:toWorld(vec(0,52,53)))
                    holoAng(Holo_Trunk, Holo_Entity:toWorld(ang(-20,90,0)))
                }else{
                    holoPos(Holo_Trunk, Holo_Entity:toWorld(vec(0,49,38)))
                    holoAng(Holo_Trunk, Holo_Entity:toWorld(ang(42,90,0)))
                }
            break
            case "adometer",
                print(
                    "Total distance:\n"+
                    Adometer+" units\n"+
                    toUnit("km",Adometer)+" kilometers"
                )
            break
            case "side",
                Data["Side",number] = sign(LastSaid[3,string]:toNumber())
                if(!Data["Side",number]){
                    Data["Side",number] = -1
                    print("Size not choosed. Left side set automaticly")
                }
                holoPos(Holo_Rulb, holoEntity(Holo_Rulb-2):toWorld(vec(-7,17*Data["Side",number],12)))
            break
            case "mass",
                Data["Mass",number] = LastSaid[3,string]:toNumber()
                Entity:setMass(Data["Mass",number])
            break
            case "music",
                #ifdef soundURLload(string,string,number,number,entity)
                    switch(LastSaid[3,string]:lower()){
                        case "url",
                            local URL = LastSaid[4,string]
                            soundURLload("music",URL,1000,0,Entity)
                            soundURLparent("music",Entity)
                            print("Now playing: "+URL)
                            Mode_Music = 1
                        break
                        case "stop",
                            soundURLdelete("music")
                            print("Music stoped")
                            Mode_Music = 0

                        break
                        case "resume",
                            soundURLplay("music")
                            print("Music resumed")
                            Mode_Music = 1
                        break
                        case "pause",
                            soundURLpause("music")
                            print("Music paused")
                            Mode_Music = 0
                        break
                    }
                #endif
            break
            case "subwoofer",
                Data["Mode_Subwoofer",number]=!Data["Mode_Subwoofer",number]
                if(Data["Mode_Subwoofer",number]){
                    Holo_Update = 1
                }else{
                    for(I=Holo_Subwoofer-1,Holo_Subwoofer+1){holoDelete(I)}
                }
            break
            case "water",
                Data["Mode_Water",number]=!Data["Mode_Water",number]
            break
            case "color",
                local Color = vec(LastSaid[3,string]:toNumber(),LastSaid[4,string]:toNumber(),LastSaid[5,string]:toNumber())
                Data["Color",vector] = Color
                Entity:setAlpha(0)
                Holo_Update = 1
            break
            case "help",
                local Add = ""
                #ifdef soundURLload(string,string,number,number,entity)
                    Add = "\n"+array(
                        "music url [string]",
                        "music stop",
                        "music resume",
                        "music pause"
                    ):concat("\n    ")
                #endif
                
                print(
                    "!greatcar [commang] [argument]\n"
                    +"Commands:\n    "
                    +array(
                        "height [number]",
                        "megaboost",
                        "bonnet",
                        "trunk",
                        "adometer",
                        "side [number]",
                           "mass [number]",
                        "subwoofer",
                        "water",
                        "color [vector]"
                    ):concat("\n    ")
                    +Add
                    +"\nKeys:\n    "
                    +array(
                        "V - fly mode",
                        "R - rotate car to normal angles",
                        "T - low height mode"
                    ):concat("\n    ")
                )
            break
        }
    }
}


soundPlay
#Adometer
if(clk("adometer")){
    timer("adometer",1000)
    local Pos = Entity:pos()
    if(Adometer_OldPos){
        Adometer+=Pos:distance(Adometer_OldPos)
    }
    Adometer_OldPos = Pos
}



#Movement
if(tickClk()){
    
    #Pod control
    switch(Data["Side",number]){
        case -1, Driver=Chair1:driver() break
        case  1, Driver=Chair2:driver() break
        case  0, 
            Data["Side",number] = -1
            print("Size not choosed. Left side set automaticly")
            Driver=Chair1:driver()
        break
    }
    Active=Driver:isValid()
    W=Driver:keyForward()
    A=Driver:keyLeft()
    S=Driver:keyBack()
    D=Driver:keyRight()
    R=Driver:keyReload()
    T=Driver:keyPressed("T")
    V=Driver:keyPressed("V")
    Shift=Driver:keySprint()
    Space=Driver:keyJump()
    
    
    
    #Detectors
    Speed = toUnit("km/h",Entity:vel():length())
    Entity_VelL = Entity:velL()
    Move = -Entity_VelL:y()
    Multipler = clamp(toUnit("km/h",abs(Move))^0.4+4,0,14)
    MoveSign = sign(Move)
    
    
    
    #Change color
    if(changed(Entity:getColor())&!changed(Holo_Update)){
        Data["Color",vector] = Entity:getColor()
        Entity:setAlpha(0)
        Holo_Update = 1
    }
    
    
    
    #Rulb
    holoAng(Holo_Rulb,holoEntity(Holo_Rulb-1):toWorld(ang(0,MoveSign*Entity:angVel():yaw(),0)))
    
    
    
    #Low height
    if(changed(T)&!T){
        Mode_LowHeight=!Mode_LowHeight
    }
    
    
    
    #Fly mode
    if(changed(V)&!V){
        Mode_Fly=!Mode_Fly
    }
    
    
    
    #Subwoofer
    if(Mode_Music){
        #ifdef entity:soundFFT(string)
            local FFT = Entity:soundFFT("music")
            local Multiplier = 0
            for(I=1,10){Multiplier+=FFT[I,number]}
            holoPos(Holo_Subwoofer, holoEntity(Holo_Subwoofer-1):toWorld(vec(0,0,holoScale(Holo_Subwoofer-1):z()*6+sin(systime()*3000)*Multiplier/200)))
        #else
            holoPos(Holo_Subwoofer, holoEntity(Holo_Subwoofer-1):toWorld(vec(0,0,holoScale(Holo_Subwoofer-1):z()*6+sin(systime()*3000)*2)))
        #endif
    }
    
    
    
    #Persist
    Water = Data["Mode_Water",number]
    Mass = Data["Mass",number]
    Height = Data["Height",number]
    
    
    
    #Rangers
    AddHeight = (20+80)/(Multipler+20)
    NowHeight = Mode_LowHeight ? Height*0.8 : Height*(1-0.2*clamp(Speed,0,200)/200)
    
    rangerHitWater(Water) RangerLU=rangerOffset(NowHeight+AddHeight,Entity:toWorld(vec( 33,-45,-1.6)),-Entity:up())
    rangerHitWater(Water) RangerRU=rangerOffset(NowHeight+AddHeight,Entity:toWorld(vec(-33,-45,-1.6)),-Entity:up())
    rangerHitWater(Water) RangerLD=rangerOffset(NowHeight+AddHeight,Entity:toWorld(vec( 33, 47,-1.6)),-Entity:up())
    rangerHitWater(Water) RangerRD=rangerOffset(NowHeight+AddHeight,Entity:toWorld(vec(-33, 45,-1.6)),-Entity:up())
    
    DistLU=RangerLU:distance()
    DistRU=RangerRU:distance()
    DistLD=RangerLD:distance()
    DistRD=RangerRD:distance()
    DistL=(DistLU+DistLD)/2
    DistR=(DistRU+DistRD)/2
    DistU=(DistLU+DistRU)/2
    DistD=(DistLD+DistRD)/2
    DistC=(DistL +DistR )/2
    
    OnGround = RangerLU:hit()+RangerRU:hit()+RangerLD:hit()+RangerRD:hit()+Mode_Fly*5
    
    
    #Stay state
    Stay_Event = (!Active|Space)&Speed<5
    if(changed(Stay_Event)&Stay_Event){
        Stay_Pos = Entity:pos()
    }
    if(Stay_Event){
        Entity_VelL -= 5*Entity:toLocal(Stay_Pos)
    }
    
    
    #Movement
    if(OnGround>=2){
        Entity:applyForce((
            #Movenent
            Entity:right()*(10*(W-S)+20*((Shift|Mode_Fly)&W&!S))
            #Wheels
            +Entity:up()*(NowHeight-DistC)*Multipler*10/OnGround*(OnGround>=5|!Mode_Fly)*(1-0.9*(Water&Entity:isUnderWater()))
            #Vel
            -Entity_VelL:z()*Entity:up()*(0.1*Multipler+0.1*Mode_Fly)
            -Entity_VelL:x()*Entity:forward()*0.025*Multipler
            +Entity_VelL:y()*Entity:right()*(0.015*(!Active|Space)+0.085*((Space&Shift)|!Active))*!Mode_Fly
        )*TickRate*Mass)
        
        Entity:applyAngForce((
            #Movement
            ang(0,90,0)*16*(A-D)/(abs(toUnit("km/h",Move))^0.2+4)*((abs(Move)/300)/(abs(Move)/300+1))*MoveSign
            +ang(0,0,90)*16*(Shift-Space)/(abs(toUnit("km/h",Move))^0.2+4)*((abs(Move)/300)/(abs(Move)/300+1))*Mode_Fly
            -Entity:angles()*ang(20,0,0)*Mode_Fly
            #Wheel
            +ang((DistL-DistR)*15,0,(DistU-DistD)*25)
            #Vel
            -Entity:angVel()*ang(1.5,1.2+1*(!Active|Space),2.5)
        )*TickRate*Mass)
    }
    
    
    if(R|(Water&Entity:isUnderWater())){
        Entity:applyAngForce(-Entity:angles()*ang(1,0,1)*5*(1+5*(Water&Entity:isUnderWater()))*TickRate*Mass)
    }
    
    if(Mode_MegaBoost&Shift&W&!S){
        print(Speed)
        while(perf()){
            #Entity:applyForce(Entity:right()*2^1023)
            Entity:applyForce(Entity:right()*2^10)
        }
    }
    
    
    
    #Wheels
    foreach(I,N:ranger=array(RangerLU,RangerRU,RangerLD,RangerRD)){
        local H = Holo_WheelOffset+I
        holoPos(H, Entity:toWorld(Entity:toLocal(holoEntity(H):pos())*vec(1,1,0))+Entity:up()*(holoScale(H):x()*6-N:distance()))
    }
    
    if(OnGround>=2){
        WheelAng[1]=WheelAng[1]+Move/80*3
        WheelAng[2]=WheelAng[2]+Move/80*2.5
        Wheel_Vel=Move/80
    }
    else{
        Wheel_Vel+=0.1*(W-S)+0.2*(Shift&W&!S)-0.02*sign(Wheel_Vel)
        WheelAng[1]=WheelAng[1]+Wheel_Vel*3
        WheelAng[2]=WheelAng[2]+Wheel_Vel*2.5
    }
    
    holoAng(Holo_WheelOffset+1, Entity:toWorld(ang(-WheelAng[1],MoveSign*Entity:angVel():yaw()/5+90,90)))
    holoAng(Holo_WheelOffset+2, Entity:toWorld(ang( WheelAng[1],MoveSign*Entity:angVel():yaw()/5-90,90)))
    holoAng(Holo_WheelOffset+3, Entity:toWorld(ang(-WheelAng[2], 90,90)))
    holoAng(Holo_WheelOffset+4, Entity:toWorld(ang( WheelAng[2],-90,90)))
}   
    
  
