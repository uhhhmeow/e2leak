@name Egp Spiel Game Bewegung
@inputs EScreen:wirelink EHud:wirelink Seat:entity
@persist W H Arr:array User:entity Won Tab:table
setName("Game")
entity():propNotSolid(0)
#[
/////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////FIRST//////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////
]#
if(first()) {
    runOnTick(1)    
    W=512*2
    H=512*2
    Tab=table()
#    Tab[1,array]=array("1","2","3","4","5","6","7","8"," ")
    Tab[1,array]=array("3","6","2"," ","8","5","7","1","4")
    Tab[2,array]=array("5","2","7","3"," ","1","4","6","8")
    Tab[3,array]=array("7","3","6","8","1"," ","5","2","4")
    Arr=Tab[randint(Tab:count()),array]
    Arr=array("1","2","3","4","5","6","7"," ","8")
}
#[
/////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////FUNCTIONS//////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////
]#
function array replaceArr(Array:array,First:number,Second:number) {
if(First!=-1&&Second!=-1&&Array[First,string]!=" ") {
    local One=Array[First,string]
    local Two=Array[Second,string]
    local NewA = array()
        foreach(K,V:string=Array) {
            if(K==First) {
                NewA[K,string]=Two
            }elseif(K==Second) {
                NewA[K,string]=One
            }else{
                NewA[K,string]=V
            }
        }
        return NewA
}
return Array
}

function createGrid(E:wirelink,Index,Start:vector2,End:vector2,X,Y) {
E:egpClear()
local Count = 1
    for(J=1,Y,1) {
        for(I=1,X,1) {
            local Size = vec2(W/X-1,H/Y-1)
            local VEC = vec2(W/X*(I-1),H/Y*(J-1))+Size/2
            Index = Index+Count
            E:egpBox(Index,VEC,Size)
            E:egpColor(Index,vec(20,20,20))
            E:egpText(9+Index,Arr[Index,string],VEC-vec2(20,40))
            E:egpFont(9+Index,"Default",W/16)
            print(W/16)

        }
    }
    E:egpCircle(1000,vec2(50,50),vec2(4,4))
    E:egpParentToCursor(1000)
}

function number getSelected(U:entity,Cursor:vector2) {
    local Sel = -1
    for(I=1,9,1) {
        if(inrange(Cursor,EScreen:egpPos(I)-EScreen:egpSize(I)/2,EScreen:egpPos(I)+EScreen:egpSize(I)/2)) {
            Sel = I
            break
        }
    }
    return Sel
}

function number getD(K:number) {
        if(K==1||K==2||K==4||K==5||K==7||K==8) {
            return K+1
        }else {
            return -1
        }
}

function number getA(K:number) {
        if(K==2||K==3||K==5||K==6||K==8||K==9) {
            return K-1
        }else {
            return -1
        }
}

function number getW(K:number) {
        if(K==4||K==5||K==6||K==7||K==8||K==9) {
            return (K-3)
        }else {
            return -1
        }
}

function number getS(K:number) {
        if(K==1||K==2||K==3||K==4||K==5||K==6) {
            return (K+3)
        }else {
            return -1
        }
}

function number canChange(A:array,Fir:number,Sec:number) {
if(Fir!=-1&&A[Fir,string]!=" ") {
    if(A[Sec,string]==" ") {
        return 1
    }else{
        return 0
    }
}
return 0
}

function number checkArr(A:array) {
        if(A[1,string]=="1"&&A[2,string]=="2"&&A[3,string]=="3"&&
           A[4,string]=="4"&&A[5,string]=="5"&&A[6,string]=="6"&&
           A[7,string]=="7"&&A[8,string]=="8"&&A[9,string]==" ") {
            return 1
        }else {
            return 0
        }
}

function array generate() {
local A = array("1","2","3","4","5","6","7","8"," ")
local B = array()
for(I=1,8,1) {
B[I,string]==A[I,number]:toString()
}
return A
}

#[
/////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////Variable//////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////
]#
Key_W=User:keyPressed("W")
Key_D=User:keyPressed("D")
Key_S=User:keyPressed("S")
Key_A=User:keyPressed("A")
#[
/////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////UPDATE STUFF///////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////
]#
if(changed(Seat:driver())) {
User=Seat:driver()
}

if(changed(->EScreen)) {
Seat:driver()
EScreen:egpSize(W,H)
EScreen:egpClear()
createGrid(EScreen,0,vec2(),vec2(),3,3)
}

if(changed(User)) {
EHud:egpClear()
}
#[
/////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////IN-GAME//////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////
]#

if(User) {
    if(Won==0) {
local Selected = getSelected(User,EScreen:egpCursor(User)) 
    if(Selected!=-1&&changed(Selected)) {
        local Pos = EScreen:egpToWorld(EScreen:egpPos(Selected))
        EHud:egp3DTracker(20,Pos)
        EHud:egpPoly(21,array(vec2(0,-200),vec2(20,-160),vec2(10,-160),vec2(10,-110),vec2(-10,-110),vec2(-10,-160),vec2(-20,-160),vec2(0,-200)))
        EHud:egpPoly(22,array(vec2(200,0),vec2(160,20),vec2(160,10),vec2(110,10),vec2(110,-10),vec2(160,-10),vec2(160,-20),vec2(200,0)))
        EHud:egpPoly(23,array(vec2(0,200),vec2(20,160),vec2(10,160),vec2(10,110),vec2(-10,110),vec2(-10,160),vec2(-20,160),vec2(0,200)))
        EHud:egpPoly(24,array(vec2(-200,0),vec2(-160,20),vec2(-160,10),vec2(-110,10),vec2(-110,-10),vec2(-160,-10),vec2(-160,-20),vec2(-200,0)))

        EHud:egpText(25,"W",vec2(-8,-170))
        EHud:egpText(26,"D",vec2(155,-10))
        EHud:egpText(27,"S",vec2(-5,150))
        EHud:egpText(28,"A",vec2(-165,-10))
        for(I=21,28,1) {
            EHud:egpParent(I,20)
            if(I>24) {
                    EHud:egpColor(I,vec(100,255,100))
            }else {
                if(Key_W&&I==21) {
                    EHud:egpColor(I,vec(100,100,100))
                }elseif(Key_D&&I==22) {
                    EHud:egpColor(I,vec(100,100,100))
                }elseif(Key_S&&I==23) {
                    EHud:egpColor(I,vec(100,100,100))
                }elseif(Key_A&&I==24) {
                    EHud:egpColor(I,vec(100,100,100))
                }else{
                    EHud:egpColor(I,vec(0,0,0))
                }
            }
        }
    }


    for(I=1,9,1) {
    
        if(changed(Key_D)&&Key_D&&Selected!=-1) {
            if(canChange(Arr,Selected,getD(Selected))) {
                Arr = replaceArr(Arr,Selected,getD(Selected))
                createGrid(EScreen,0,vec2(),vec2(),3,3)
            }
        }
    
        if(changed(Key_A)&&Key_A&&Selected!=-1) {
            if(canChange(Arr,Selected,getA(Selected))) {
                Arr = replaceArr(Arr,Selected,getA(Selected))
                createGrid(EScreen,0,vec2(),vec2(),3,3)
            }
        }
    
    
        if(changed(Key_W)&&Key_W&&Selected!=-1) {
            if(canChange(Arr,Selected,getW(Selected))) {
                Arr = replaceArr(Arr,Selected,getW(Selected))
                createGrid(EScreen,0,vec2(),vec2(),3,3)
            }
        }
    
    
        if(changed(Key_S)&&Key_S&&Selected!=-1) {
            if(canChange(Arr,Selected,getS(Selected))) {
                Arr = replaceArr(Arr,Selected,getS(Selected))
                createGrid(EScreen,0,vec2(),vec2(),3,3)
            }
        }
    }
}
    if(checkArr(Arr)&&Won==0) {
    EScreen:egpClear()
    EHud:egpClear()
    Won=1
    }

if(Won) {
    EScreen:egpText(1,"YOU WIN",vec2(W/2-125,H/2-30))
    EScreen:egpColor(1,vec(0,255,0))
    EScreen:egpFont(1,"Default",W)
    EScreen:egpPos(1,vec2(W/2-125,H/2-30)+vec2(0,sin(realtime()*100)*20))
EScreen:egpBox(2,vec2(W/2,H/5*4),vec2(300,50))
EScreen:egpText(3,"Restart",EScreen:egpPos(2)+vec2(-65,-26))
EScreen:egpFont(3,"Default",W)
    EScreen:egpCircle(1000,vec2(50,50),vec2(4,4))
    EScreen:egpParentToCursor(1000)
if(inrange(EScreen:egpCursor(User),EScreen:egpPos(2)-EScreen:egpSize(2)/2,EScreen:egpPos(2)+EScreen:egpSize(2)/2)) {
EScreen:egpColor(2,vec(90,90,90))
    if(User:keyAttack1()) {
        Won=0
        EScreen:egpClear()
        Arr=Tab[randint(Tab:count()),array]
#        Arr=array("1","2","3","4","5","6","7"," ","8")
        createGrid(EScreen,0,vec2(),vec2(),3,3)
    }
}else{
EScreen:egpColor(2,vec(50,50,50))
}

}
}else {
Won=0
}










