@name Inverse Kinematics test 2
@inputs Prop:entity

E=entity()
interval(1)
if(first()){
    holoCreate(1,E:pos())
    holoCreate(2,holoEntity(1):toWorld(vec(0,0,25)))
    holoCreate(3,holoEntity(2):toWorld(vec(0,0,25)))
    
    holoParent(1,E)
    holoParent(2,1)
    holoParent(3,2)
function number cosin(A,B,C){
return acos((A^2 + B^2 - C^2) / (2*A*B))
}

function angle ik(H1,H2,Length:number,Length2:number,Hip:entity,End:entity){
local Dist = holoEntity(H1):pos()-End:pos()
local Acos = cosin(Dist:length(),Length,Length2)
holoAng(H1,(End:pos()-holoEntity(H1):pos()):toAngle()+ang(Acos,0,0))
holoAng(H2,holoEntity(H1):toWorld(ang(cosin(Length2,Length,Dist:length())+0,0,0)))
}
}
#[
Ent1Pos=holoEntity(1):pos()
Ent2Pos=Prop:pos()

Dist = Ent1Pos - Ent2Pos
Leng = Dist:length()
Ang1 = Dist:toAngle()
Ang2 = ang(-asin(Leng / 50), 0, 0)

holoAng(1, E:toWorld(Ang1 + Ang2) )
holoAng(2, E:toWorld(Ang1 - Ang2) )
]#
ik(1,2,25,25,holoEntity(1),Prop)
