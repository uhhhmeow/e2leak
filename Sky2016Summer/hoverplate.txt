@name hoverplate
@inputs 
@outputs 
@persist B:entity
@trigger 
runOnTick(1)
runOnLast(1)
E=entity()
if(!B){
B=propSpawn("models/hunter/tubes/circle4x4.mdl",(entity():pos()+vec(0,0,15)),0)    
}
B:setMass(50000)
B:propGravity(0)  
B:applyAngForce((ang(0,B:angles():yaw(),0)-B:angles())*B:mass()*100)
B:applyForce(-B:vel()*B:mass()*0.3)
B:applyAngForce(-B:angVel()*B:mass()*1)
B:applyAngForce(ang(0,1,0)*B:mass()*50)
if(last()){
 propDeleteAll()   
}
