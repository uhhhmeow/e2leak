@name Airstrike by leo
@inputs  
@outputs 
@persist Last:array T:entity B:entity
runOnTick(1)
runOnChat(1)
if(chatClk(owner()))
{
    Last = owner():lastSaid():explode(" ")
    if(Last[1,string] == "/strike")
    {
        hideChat(1)
        T = findPlayerByName(Last[2,string])
        if(T:isValid())
        {
            B = propSpawn("models/props_phx/torpedo.mdl",T:pos()+vec(0,0,5000),0)
            B:setAng(ang(90,0,0))
            B:setTrails(15,0,7,"trails/smoke",vec(75,75,75),255)
        }
    }
}
if(B:isValid())
{
    B:applyForce((T:pos()-B:pos())*999)
}
