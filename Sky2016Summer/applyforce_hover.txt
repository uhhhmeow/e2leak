@name applyforce hover
@inputs  
@outputs 
@persist Thrust Dist
@trigger 
interval(10)
E=entity():isWeldedTo()
rangerHitWater(1)
rangerFilter(E)
Ranger=rangerOffset(60,E:toWorld(vec(30,40,0)),-E:up())
Dist=Ranger:distance()
Distanz=59
Dist=Ranger:distance()
Thrust=Distanz-Dist-($Dist*1) #dollar zeichen ist delta
E:applyForce(E:up()*Thrust*E:mass()*1) #zahl erhoehen wenn hover kraft zu klein

E:applyAngForce(-E:angVel()*E:mass()/0.1) #soll das kippen verhindern
entity():propGravity(0)
E:applyForce(-E:vel()*E:mass()*0.3)
