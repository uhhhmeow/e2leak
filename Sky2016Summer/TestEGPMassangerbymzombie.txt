@name TestEGPMassangerbymzombie
@inputs EGP:wirelink Keyboards:wirelink
@persist [Data]:gtable Str:string X Count Okey Keyboard User:entity
interval(50)
runOnChat(1)

User = EGP["User",entity]
Cursor = EGP:egpCursor(User)
Keyboard = Keyboards["Memory",number]
User = Keyboards["User",entity]

local Data = gTable("Massanger",0)

if(first()){
    EGP:egpClear()
}

EGP:egpBoxOutline(1,vec2(512)/2,vec2(500))
EGP:egpColor(1,vec(200,0,0))
EGP:egpBoxOutline(2,vec2(512)/2,vec2(480))
EGP:egpColor(2,vec(0,0,200))

EGP:egpText(3,"Chat Massanger Test",vec2(255,80))
EGP:egpAlign(3,1,1)
EGP:egpFont(3,"arail",30)

EGP:egpBoxOutline(4,vec2(255,470),vec2(480,50))
EGP:egpColor(4,vec(0,0,255))

EGP:egpText(5,Str,EGP:egpPos(4))
EGP:egpAlign(5,1,1)


if(changed(User) & User){
    Str = ""
}

if(changed(Keyboard) & Keyboard & Count != 50){
Str += toChar(Keyboard)
Count++
}
if(changed(Keyboard == 127) & Keyboard == 127 & Count != 0  & Okey){
    Count--
    Str = Str:left(Str:length()-2)
    
}
if(Keyboard != 127 & Keyboard != 154 & Keyboard != 144 & Keyboard != 9 & Keyboard != 158 & Keyboard != 159){
    Okey = 1
}
else{
    Okey = 0
}

if(changed(Keyboard == 13) & Keyboard == 13 & !Data[X,string]){
    Data[X,string] = User:name()+": "+Str
    Str = ""
    X++
    Count = 0
}
elseif(changed(Data[X,string]) & Data[X,string]){
    X++
}

for(I=1,Data:count()){
    if(Data[I,string]){
    EGP:egpText(I+20,Data[I,string],vec2(30,100+I*25))
    #EGP:egpAlign(I+20,1,1)
}
}

if(owner():lastSaid() == "!clear" & chatClk(owner())){
    Data:clear()
    X = 0
    Str = ""
    Count = 0
    EGP:egpClear()
}
