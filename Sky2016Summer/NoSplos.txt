@name NoSplos
@inputs 
@outputs 
@persist T A:array
@trigger 
interval(10)
T++
Range = 1400
Prop = "models/hunter/blocks/cube2x2x2.mdl"

if (first()|duped()) {
    holoCreate(1)
    holoAlpha(1,0)
    holoCreate(2)
    holoAlpha(2,0)
    holoCreate(3)
    holoAlpha(3,0)
    holoEntity(1):setTrails(300,300,2,"trails/laser",vec(0,255,0),255)
    holoEntity(2):setTrails(300,300,2,"trails/laser",vec(0,255,0),255)
    holoEntity(3):setTrails(300,300,2,"trails/laser",vec(0,255,0),255)
}

if (first()) {
    findByModel("models/hunter/blocks/cube1x1x1.mdl")
    findIncludeModel("models/props_phx/amraam.mdl")
    findIncludeModel("models/props_phx/ball.mdl")
    findIncludeModel("models/props_phx/oildrum001_explosive.mdl")
    findIncludeModel("models/props_phx/torpedo.mdl")
    findIncludeModel("models/props_phx/ww2bomb.mdl")
    findIncludeModel("models/props_phx/mk-82.mdl")
    findIncludeModel("models/props_phx/misc/flakshell_big.mdl")
    findIncludeModel("models/props_phx/misc/potato_launcher_explosive.mdl")
    findIncludeModel("models/props_c17/oildrum001_explosive.mdl")
    findIncludeModel("models/props_junk/gascan001a.mdl")
    findIncludeClass("npc_grenade_frag")
    findIncludeClass("rpg_missile")
    findIncludeClass("prop_combine_ball")
    findIncludeClass("ent_awccs_hegrenade")
    findExcludePlayerProps(owner())
}

findInSphere(entity():pos(),Range)
F = findToArray()

for (I=1,F:count()) {
    if (!A[I,entity]:isValid()) {
        A[I,entity] = propSpawn(Prop,1)    
    } elseif (F[I,entity]:pos():distance(entity():pos())<Range) {
        A[I,entity]:setPos(F[I,entity]:boxCenterW())
    }
}
for (I=1,A:count()) {
    if (!F[I,entity]:isValid()) {
        A[I,entity]:propDelete()
        A:remove(I)
        F:remove(I)    
    }
}

holoPos(1,entity():pos()+vec(sin(T*1)*Range,cos(T*1)*Range,0))
holoPos(2,entity():pos()+vec(0,sin(T*1)*-Range,cos(T*1)*-Range))
holoPos(3,entity():pos()+vec(sin(T*1)*Range,0,cos(T*1)*Range))
