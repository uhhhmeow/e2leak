@name flying holo stuff
@inputs 
@outputs 
@persist S
@trigger 
runOnTick(1)
 S=S*1+1
 E=cos(S)
 F=sin(S)
 holoCreate(1)
 holoScale(1, vec(20,20,20))
 holoPos(1, entity():toWorld(vec(E*150,F*150,150)))
 holoParent(1, entity())
 holoColor(1, vec(255,205,0))
 holoModel(1, "hqsphere")
