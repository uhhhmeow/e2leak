@name ExoSuit-V2
@inputs Cam:wirelink
@outputs 
@persist [LegR LegL Mech]:table Mode:string
@model models/hunter/blocks/cube05x05x05.mdl    

#interval(80)  
if(first()){ #include "Sky2016Summer/GeneralMechFunctions" timer("run",1)}

if(clk("run")){
timer("run",80)
E = entity()
RightLeft = 40

LegL1 = 70
LegL2 = 70
 
StepSpeed = 2

RunDelay = 4
StepDelay = 7
MaxStepHeight = 2

MaxDist = 10 
StepVel = 3
WorldDownVec = vec(0,0,-1)

HoverHeight = 90

Cam:checkSeat(E:toWorld(vec(0,0,10)),E:toWorld(ang(0,-90,0)),E,Seat)
Mech:locked(owner():keyPressed("0"))
}
if(first()){
    E:setMass(50000)
E:propGravity(0)
function number c(I,V:vector,S:vector,A:angle,Col:vector,Model:string,Alpha:number){ holoCreate(I,V,S,A,Col,Model) holoAlpha(I,Alpha) }
function number c(I,V:vector,S:vector,A:angle,Col:vector,Model:string,Parent:entity,Alpha:number){ holoCreate(I,V,S,A,Col,Model) holoAlpha(I,Alpha) holoParent(I,Parent) }
function number c(I,V:vector,S:vector,A:angle,Col:vector,Model:string,Parent:number,Alpha:number){ holoCreate(I,V,S,A,Col,Model) holoAlpha(I,Alpha) holoParent(I,Parent) }
function entity h(N){ return holoEntity(N) }
function setMode(Name:string){ Mode = Name }
#######################################################################################################################
#######################################################################################################################
#######################################################################################################################

 function table:anim(Speed,StartAnim,EndCounter,Sheight,EndVector:vector,Glength,MaxDist,Vel,Base:entity,VecDown:vector,FeetHolo){
 local T = This
#[-----------Counter & AnimFix----------]#
if(T["Parts",string]!="Setupanim"&T["Parts",string]!="Doanim"){ T["Parts",string] = "Setupanim" }

local CountUp = 1

T["Count.data",number] = (T["Count.data",number] + CountUp) % EndCounter
#[---------------Ranger-----------------]#

 rangerFilter(Base)
 rangerFilter(Seat)
 rangerFilter(PropLock["Locker",entity])
 local TempA = rangerOffset(Glength,EndVector+Base:vel():setZ(0)/Vel,VecDown)
 T["RangerPos",vector]=TempA:position()

#[------------------Anim----------------]#

 switch (T["Parts",string]){
    case "Setupanim",
    if(TempA:hit()){
     if(T["Count.data",number]==StartAnim){
      if(T["StartPos.data",vector]:distance(  T["RangerPos",vector] )>=MaxDist){ 
       T["StartPos.data",vector] = T["Bezier",vector]
       T["Distance.data",number] = (T["StartPos.data",vector]- T["RangerPos",vector]):length()
       T["Middle.data",vector] = mix( T["RangerPos",vector],T["StartPos.data",vector],0.99) + vec(0,0,max(T["Distance.data",number] / Sheight,MaxDist/10)) 
       T["Parts",string] = "Doanim"
      }  
     }  
    }
    break  
    case "Doanim",
    
      T["GaitVelocity",number] = T["GaitVelocity",number]*0.1 + (Base:vel():length() + abs(Base:angVelVector():length()))
      T["GC",number] = T["GC",number] + (0.02 + 0.02*T["GaitVelocity",number]/(10 * Speed))
      T["Move_Count.data",number] = min(T["Move_Count.data",number] + T["GC",number],1)
     
       T["Bezier",vector] = bezier(T["StartPos.data",vector],T["Middle.data",vector], T["RangerPos",vector],T["Move_Count.data",number])
    
          if(T["Move_Count.data",number]>=1){
            holoEntity(FeetHolo):soundPlay(FeetHolo*randint(999),1.3,"npc/dog/dog_footstep_run"+randint(1,8)+".wav")
           
            T["Move_Count.data",number] = T["GaitVelocity",number] = T["GC",number] = Multiply = 0
            T["Parts",string] = "Setupanim"
          }
        
    break
 }
#[-----------------Foot-----------------]#
  if(Base){
    local HT = TempA:hitNormal()
    local Cross = HT:cross(Base:forward()):normalized():cross(HT)
    holoAng(FeetHolo,slerp(quat(holoEntity(FeetHolo):angles()), quat(Cross:toAngle()),0.99):toAngle())
  } 
#[--------------------------------------]#
 }
function table:fixIt(TempV:vector){
   This["Middle.data", vector] = This["StartPos.data", vector] = This["RangerPos",vector] = This["Bezier", vector] = TempV
This["Parts",string]="Doanim"
}

#######################################################################################################################
#######################################################################################################################
#######################################################################################################################

function vector entity:inverse(A, B, C, D, End:vector,Myaw){
    local Length_1 = abs(holoEntity(B):toLocal(holoEntity(C):pos()):z())
    local Length_2 = abs(holoEntity(C):toLocal(holoEntity(D):pos()):z())
    local Vector = (holoEntity(B):pos()-End)
    local AxisA = This:toLocalAxis(Vector)
    local AxisB = holoEntity(A):toLocalAxis(Vector)
    local Distance = min(Vector:length(),Length_1 + Length_2)
    local Hip_Acos = acos( (Distance^2 + Length_1^2 - Length_2^2) / ((Length_1*2)*Distance))
    local Knee_Acos = acos( (Length_1^2 + Length_2^2 - Distance^2) / ((Length_1*2)*Length_1))
    local Inverse = atan(AxisB[1],AxisB[3])
    holoAng(A,This:toWorld(ang(0,0,90+atan(AxisA[3],AxisA[2]))))
    holoAng(B,holoEntity(A):toWorld(ang(Hip_Acos-180+Inverse,0,0)))
    holoAng(C,holoEntity(B):toWorld(ang(180+Knee_Acos,0,0)))
}
function ranger ran(Vec:vector){
rangerFilter(E)
rangerFilter(Seat)
return rangerOffset(HoverHeight*1.5,Vec,WorldDownVec)
}
#######################################################################################################################
#######################################################################################################################
#######################################################################################################################

function ranger aim(){
return rangerOffset(99999,Driver:shootPos(),Seat:toLocalAxis(Driver:eye()))
}
function number table:getFH(){
return ((This["Bezier",vector]-This["RangerPos",ranger]:pos()):z())
}
function number bearingRotate2(Aim:vector,NR,M){
return (-bearing(E:pos(),ang(E:angles():pitch(),E:angles():yaw(), E:angles():roll()), Aim) / NR * M) *!Lock
}
#######################################################################################################################
#######################################################################################################################
#######################################################################################################################
#######################################################################################################################
#######################################################################################################################
#######################################################################################################################

setMode("walking") 
function walking(){
   local Hran = ran(E:toWorld(vec(0,0,0)))
   local Ground = Hran:hit()
#[--------------------------------------]#   
 
   local FeetH = clamp(abs(LegR:getFH()-LegL:getFH()),-50,50)

   local Speed = max(1,toUnit("mph",E:vel():setZ(0):length()))
   local CurTime = curtime()*100
   local SinTime = sin(CurTime)/Speed * 2
   local CosTime = cos(CurTime)/Speed * 2    

#[--------------------------------------]#
   local Mass = E:mass()
#[--------------------------------------]#  

   local Hovering = CosTime + FeetH
   local MVel = (-E:vel()*(1/3))

   local ForceUp = ((Hran:pos() + vec(0,0,(HoverHeight + Hovering)))-E:pos())
   local ForceM = (E:forward()*(50-(25*S))*(W-S) + E:right()*25*(D-A))
   
   E:applyForce(((ForceUp + ForceM) + MVel)  * Mass)

#[--------------------------------------]#  

   local AddPitch = SinTime + FeetH/2
   local Mavel = (-E:angVel()*(1/3))

   local Bearning = bearingRotate2(aim():position(),4,Driver:isValid())
   local Angle = ang(AddPitch,E:angles():yaw()+Bearning,0)
 
   E:applyAngForce((E:toLocal(Angle) + Mavel) * Mass)

#[--------------------------------------]#
LegR:anim(StepSpeed,1,StepDelay * 2,MaxStepHeight,E:toWorld(vec(0,RightLeft,0)),LegL1+LegL2,MaxDist,StepVel,E,WorldDownVec,7)
LegL:anim(StepSpeed,1+StepDelay,StepDelay * 2,MaxStepHeight,E:toWorld(vec(0,-RightLeft,0)),LegL1+LegL2,MaxDist,StepVel,E,WorldDownVec,8)
h(1000):inverse(1,3,5,7,LegR["Bezier",vector],180)
h(1000):inverse(2,4,6,8,LegL["Bezier",vector],180)
#[--------------------------------------]#

E:propGravity(0)

  if(changed(Shift)&Shift){
   setMode("running")
  }
  if(changed(Alt)&Alt){
   setMode("crouch")
  }
  if(!Ground){ setMode("fall") }

}
#######################################################################################################################
#######################################################################################################################
#######################################################################################################################

function crouch(){
   local Hran = ran(E:toWorld(vec(0,0,0)))
   local Ground = Hran:hit() 
#[--------------------------------------]#   
 
   local FeetH = clamp(abs(LegR:getFH()-LegL:getFH()),-50,50)

   local Speed = max(1,toUnit("mph",E:vel():setZ(0):length()))
   local CurTime = curtime()*100
   local SinTime = sin(CurTime)/Speed * 2
   local CosTime = cos(CurTime)/Speed * 2    

#[--------------------------------------]#
   local Mass = E:mass()
#[--------------------------------------]#  

   local Hovering = CosTime + FeetH
   local MVel = (-E:vel()*(1/3))

   local ForceUp = ((Hran:pos() + vec(0,0,(HoverHeight/2 + Hovering)))-E:pos())
   local ForceM = (E:forward()*25*(W-S) + E:right()*12.5*(D-A))
   
   E:applyForce(((ForceUp + ForceM) + MVel)  * Mass)

#[--------------------------------------]#  

   local AddPitch = SinTime + FeetH/2
   local Mavel = (-E:angVel()*(1/3))

   local Bearning = bearingRotate2(aim():position(),4,Driver:isValid())
   local Angle = ang(AddPitch,E:angles():yaw()+Bearning,0)
 
   E:applyAngForce((E:toLocal(Angle) + Mavel) * Mass)

#[--------------------------------------]#
LegR:anim(StepSpeed,1,StepDelay*2,MaxStepHeight,E:toWorld(vec(0,RightLeft,0)),LegL1+LegL2,MaxDist,StepVel,E,WorldDownVec,7)
LegL:anim(StepSpeed,1+StepDelay,StepDelay * 2,MaxStepHeight,E:toWorld(vec(0,-RightLeft,0)),LegL1+LegL2,MaxDist,StepVel,E,WorldDownVec,8)
h(1000):inverse(1,3,5,7,LegR["Bezier",vector],180)
h(1000):inverse(2,4,6,8,LegL["Bezier",vector],180)
#[--------------------------------------]#

E:propGravity(0)
 if(!Alt){
  setMode("walking")  
  }   
  if(!Ground){ setMode("fall") }
}

#######################################################################################################################
#######################################################################################################################
#######################################################################################################################


function running(){


   Mech["SpeedCounter",number] = Mech["SpeedCounter",number] + E:vel():length()
   local CosRun = cosr(Mech["SpeedCounter",number]/15)*30
   local SinRun = sinr(Mech["SpeedCounter",number]/15)*20

   local Hran = ran(E:toWorld(vec(0,0,0)))
   local Ground = Hran:hit()  
#[--------------------------------------]#
   local Mass = E:mass()
#[--------------------------------------]#  

   local Hovering = CosRun
   local MVel = (-E:vel()*(1/3))

   local ForceUp = ((Hran:pos() + vec(0,0,(HoverHeight + Hovering)))-E:pos())
   local ForceM = (E:forward()*120*Shift)
   
   E:applyForce(((ForceUp + ForceM) + MVel)  * Mass)

#[--------------------------------------]#  

   local AddPitch = SinRun
   local Mavel = (-E:angVel()*(1/4))

   local Bearning = bearingRotate2(aim():position(),10,Driver:isValid())
   local Angle = ang(AddPitch,E:angles():yaw()+Bearning,0)
 
   E:applyAngForce((E:toLocal(Angle)*8 + Mavel) * Mass)

#[--------------------------------------]#
LegR:anim(StepSpeed*4,1,RunDelay * 2,MaxStepHeight,E:toWorld(vec(0,RightLeft,0)),LegL1+LegL2,MaxDist,StepVel*1.5,E,WorldDownVec,7)
LegL:anim(StepSpeed*4,1+RunDelay,RunDelay * 2,MaxStepHeight,E:toWorld(vec(0,-RightLeft,0)),LegL1+LegL2,MaxDist,StepVel*1.5,E,WorldDownVec,8)
h(1000):inverse(1,3,5,7,LegR["Bezier",vector],180)
h(1000):inverse(2,4,6,8,LegL["Bezier",vector],180)
#[--------------------------------------]#

E:propGravity(0)
  if(!Shift){
   setMode("walking")
  }
  if(!Ground){ setMode("fall") }
}
#######################################################################################################################
#######################################################################################################################
#######################################################################################################################

function fall(){
    
   LegR:fixIt(E:toWorld(vec(0,RightLeft,-HoverHeight/1.2))+E:vel():setZ(-1)/10)
   LegL:fixIt(E:toWorld(vec(0,-RightLeft,-HoverHeight/1.2))+E:vel():setZ(-1)/10)
   
   h(1000):inverse(1,3,5,7,LegR["Bezier",vector],180)
   h(1000):inverse(2,4,6,8,LegL["Bezier",vector],180)

    
    
   E:propGravity(1)
   local Hran = ran(E:toWorld(vec(0,0,0)))
   local Ground = Hran:hit()  

  if(Ground){
   LegR:fixIt(E:toWorld(vec(0,RightLeft,-HoverHeight))+E:vel():setZ(1)/10)
   LegL:fixIt(E:toWorld(vec(0,-RightLeft,-HoverHeight))+E:vel():setZ(1)/10)
   
  setMode("walking")
  }
}

#######################################################################################################################
#######################################################################################################################
#######################################################################################################################
#######################################################################################################################
#######################################################################################################################
#######################################################################################################################
}
Last = 8

if(clk("run")&!holoEntity(Last)&holoCanCreate()&perf()){
#skell#

c(1000,E:toWorld(vec(0,0,0)),vec(1,1,1),E:toWorld(ang(0,180,0)),vec(0,0,255),"cube",E,255)


c(1,E:toWorld(vec(0,RightLeft,0)),vec(1,1,1),E:toWorld(ang()),vec(0,255,0),"cube",1000,255)
c(2,E:toWorld(vec(0,-RightLeft,0)),vec(1,1,1),E:toWorld(ang()),vec(0,255,0),"cube",1000,255)
c(3,h(1):toWorld(vec(0,0,0)),vec(1,1,1),h(1):toWorld(ang()),vec(255,0,255),"cube",1,255)
c(4,h(2):toWorld(vec(0,0,0)),vec(1,1,1),h(2):toWorld(ang()),vec(255,0,255),"cube",2,255)

c(5,h(3):toWorld(vec(0,0,LegL1)),vec(1,1,1),h(3):toWorld(ang()),vec(255,100,255),"cube",3,255)
c(6,h(4):toWorld(vec(0,0,LegL1)),vec(1,1,1),h(4):toWorld(ang()),vec(255,100,255),"cube",4,255)

c(7,h(5):toWorld(vec(0,0,LegL2)),vec(1,1,1),h(5):toWorld(ang()),vec(100,255,255),"cube",5,255)
c(8,h(6):toWorld(vec(0,0,LegL2)),vec(1,1,1),h(6):toWorld(ang()),vec(100,255,255),"cube",6,255)


#######  
}

if(clk("run")){ Mode() }


