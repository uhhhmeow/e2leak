@name Combine MiniGun
@inputs Fire Turret:wirelink Turret2:wirelink Turret3:wirelink Turret4:wirelink
@outputs 
@persist Moveup Roll
@trigger 
@model models/hunter/misc/sphere075x075.mdl
runOnTick(1)
E=entity()
E:setAlpha(0)
if(first()){
                       holoCreate(12,E:toWorld(vec(0,0,0)),vec(1,1,1),E:toWorld(ang(0,0,180)),vec(255),"models/combine_scanner.mdl")
    holoParent(12,E)
    holoCreate(1,E:toWorld(vec(0,0,0)),vec(1,1,1),E:toWorld(ang()),vec(255),"models/combine_scanner.mdl")
    holoParent(1,E)
    
            holoCreate(2,E:toWorld(vec(10,0,0)),vec(3,3,1),E:toWorld(ang(90,0,0)),vec(255),"models/items/combine_rifle_ammo01.mdl")
    holoParent(2,E)
        holoCreate(3,E:toWorld(vec(10,0,0)),vec(1,1,3),E:toWorld(ang(90,0,0)),vec(255),"models/items/combine_rifle_ammo01.mdl")
    holoParent(3,E)
            holoCreate(4,E:toWorld(vec(10,0,0)),vec(3,3,1),E:toWorld(ang(90,0,0)),vec(255),"models/items/combine_rifle_ammo01.mdl")
    holoParent(4,E)
            holoCreate(5,E:toWorld(vec(30,0,0)),vec(3,3,1),E:toWorld(ang(90,0,0)),vec(255),"models/items/combine_rifle_ammo01.mdl")
    holoParent(5,E)
    
                holoCreate(6,E:toWorld(vec(20,0,0)),vec(1,1,1),E:toWorld(ang(90,0,0)),vec(255),"models/props_combine/combine_mine01.mdl")
    holoParent(6,E)
    
                holoCreate(7,E:toWorld(vec(40,0,0)),vec(1,1,1),E:toWorld(ang(90,0,0)),vec(255),"models/props_combine/combine_mine01.mdl")
    holoParent(7,E)
    
                holoCreate(8,E:toWorld(vec(20,0,10)),vec(1,1,1),E:toWorld(ang(0,0,0)),vec(255),"models/props_combine/combine_emitter01.mdl")
    holoParent(8,E)
    
                holoCreate(9,E:toWorld(vec(20,0,-10)),vec(1,1,1),E:toWorld(ang(180,180,0)),vec(255),"models/props_combine/combine_emitter01.mdl")
    holoParent(9,E)
    
                    holoCreate(10,E:toWorld(vec(20,-10,0)),vec(1,1,1),E:toWorld(ang(0,0,90)),vec(255),"models/props_combine/combine_emitter01.mdl")
    holoParent(10,E)
    
                holoCreate(11,E:toWorld(vec(20,10,0)),vec(1,1,1),E:toWorld(ang(0,0,-90)),vec(255),"models/props_combine/combine_emitter01.mdl")
    holoParent(11,E)
    
 
    
   holoCreate(13,E:toWorld(vec(45,-5,0)),vec(1,1,1),E:toWorld(ang(90,0,0)),vec(255),"models/items/combine_rifle_ammo01.mdl")
    holoParent(13,7)
       holoCreate(14,E:toWorld(vec(45,5,0)),vec(1,1,1),E:toWorld(ang(90,0,0)),vec(255),"models/items/combine_rifle_ammo01.mdl")
    holoParent(14,7)
       holoCreate(15,E:toWorld(vec(45,0,5)),vec(1,1,1),E:toWorld(ang(90,0,0)),vec(255),"models/items/combine_rifle_ammo01.mdl")
    holoParent(15,7)
   holoCreate(16,E:toWorld(vec(45,0,-5)),vec(1,1,1),E:toWorld(ang(90,0,0)),vec(255),"models/items/combine_rifle_ammo01.mdl")
    holoParent(16,7)
    
       holoCreate(17,E:toWorld(vec(105,-6,0)),vec(1,1,6),E:toWorld(ang(180,90,-90)),vec(255),"models/props_combine/breenlight.mdl")
    holoParent(17,7)
       holoCreate(18,E:toWorld(vec(105,6,0)),vec(1,1,6),E:toWorld(ang(180,-90,90)),vec(255),"models/props_combine/breenlight.mdl")
    holoParent(18,7)
       holoCreate(19,E:toWorld(vec(105,0,6)),vec(1,1,6),E:toWorld(ang(-90,0,0)),vec(255),"models/props_combine/breenlight.mdl")
    holoParent(19,7)
   holoCreate(20,E:toWorld(vec(105,0,-6)),vec(1,1,6),E:toWorld(ang(90,0,180)),vec(255),"models/props_combine/breenlight.mdl")
    holoParent(20,7)
    soundPlay(1,999999,"weapons/airboat/airboat_gun_loop2.wav")
}
if(Fire==1){
Moveup=Moveup+0.8
Roll=Roll*1+Moveup
if(Moveup>80){Shooting=1}
if(Moveup>100){Moveup=100 }    
}
if(Fire==0){
    Shooting=0
 Moveup=Moveup-0.8
if(Moveup>0){Roll=Roll*1+Moveup}
if(Moveup<0){Moveup=0}   
}
if(Shooting==1){
    Turret["Fire",number]=1
      Turret2["Fire",number]=1
      Turret3["Fire",number]=1
      Turret4["Fire",number]=1
}else{
Turret["Fire",number]=0
Turret2["Fire",number]=0
Turret3["Fire",number]=0
Turret4["Fire",number]=0
}
if(Shooting==1){
 soundPlay(2,2,"weapons/airboat/airboat_gun_energy1.wav")   
}
soundPitch(1,Moveup*3)
holoAng(3,holoEntity(2):toWorld(ang(0,Roll,0)))
holoAng(4,holoEntity(2):toWorld(ang(0,Roll,0)))
holoAng(5,holoEntity(2):toWorld(ang(0,-Roll,0)))
holoAng(6,holoEntity(2):toWorld(ang(0,-Roll,0)))
holoAng(7,holoEntity(2):toWorld(ang(0,Roll,0)))

Turret:entity():setPos(holoEntity(17):toWorld(vec(-8,0,-40)))
Turret:entity():setAng(holoEntity(17):toWorld(ang(90,0,0)))

Turret2:entity():setPos(holoEntity(18):toWorld(vec(-8,0,-40)))
Turret2:entity():setAng(holoEntity(18):toWorld(ang(90,0,0)))

Turret3:entity():setPos(holoEntity(19):toWorld(vec(-8,0,-40)))
Turret3:entity():setAng(holoEntity(19):toWorld(ang(90,0,0)))

Turret4:entity():setPos(holoEntity(20):toWorld(vec(-8,0,-40)))
Turret4:entity():setAng(holoEntity(20):toWorld(ang(90,0,0)))
