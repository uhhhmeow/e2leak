@name Airstrike by Hotarie
@persist Ent:entity Prop:entity EntPos:vector Vec:vector Primed Counter Missile:entity

runOnTick(1)
if (first()|duped()) {
    Primed = 0
    Fire = 0
    Counter = 0
    Ent = entity()
    EntPos = Ent:pos()
    Prop = propSpawn("models/props_junk/PopCan01a.mdl", Ent:pos(), 0)
    Prop:setTrails(1000, 1000, 1, "trails/laser", vec(255, 255, 255), 255, 0, 0)
    ######################################################################
    holoCreate(1, Prop:pos())
    holoCreate(2, Prop:pos())
    holoParent(1, Prop)
    holoParent(2, Prop)
    holoDisableShading(1, 1)
    holoDisableShading(2, 1)
    holoAlpha(1, 115)
    holoModel(1, "hq_sphere")
    holoModel(2, "hq_sphere")
    holoScale(2, vec(0.7, 0.7, 0.7))
}
Vec = (EntPos + vec(0, 0, 2000) - Prop:pos()) * 10 * Prop:mass()
Prop:applyForce(Vec + $Vec * 2.5)

if (Prop:pos():z() >= entity():pos():z() + 1999) {
    Prop:propFreeze(1)
    Primed = 1
    if (changed(Primed) & $Primed == 1) {
        holoColor(1, vec(255, 0, 0))
        holoScale(1, vec(7, 7, 7))
        holoEntity(1):soundPlay(1, 0, "npc/vort/health_charge.wav")
        timer("count", 3500)
    }
}
if (clk("count")) {
    Explosive = propSpawn("models/props_phx/ww2bomb.mdl", Prop:pos(), 1)
    Explosive:setAlpha(0)
    Explosive:propBreak()
    holoCreate(3, Prop:pos())
    holoAlpha(3, 0)
    timer("airstrike", 100)
}
if (clk("airstrike") & Counter < 500) {
    Counter += 1
    timer("airstrike", 300)
    Missile = propSpawn("models/props_phx/ww2bomb.mdl", holoEntity(3):pos() + vec(random(1050), random(1050), 0), ang(90, 0, 0), 0)
    Missile:setMaterial("models/props_combine/portalball001_sheet")
    Missile:setTrails(1000, 1000, 1, "trails/laser", vec(255, 131, 0), 255, 0, 0)
    Missile:applyForce(Missile:up() * -1 * 10000)
    Missile:setMass(50000)
    Prop:setPos(Missile:pos())
}
if (changed(Counter) & Counter == 100) {
    Prop:propDelete()
    timer("del", 200)
}
if (clk("del")) {
    entity():propDelete()
}
