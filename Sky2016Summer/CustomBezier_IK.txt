@name CustomBezier_IK
@inputs 
@outputs 
@persist T1:table T2:table
@model models/hunter/blocks/cube05x05x05.mdl
E = entity()

interval(60)
if(first()) {
    E:propGravity(0)
#runOnTick(1)
function number c(I,V:vector,S:vector,A:angle,Col:vector,Model:string,Alpha:number){ holoCreate(I,V,S,A,Col,Model) holoAlpha(I,Alpha) }
function number c(I,V:vector,S:vector,A:angle,Col:vector,Model:string,Parent:entity,Alpha:number){ holoCreate(I,V,S,A,Col,Model) holoAlpha(I,Alpha) holoParent(I,Parent) }
function number c(I,V:vector,S:vector,A:angle,Col:vector,Model:string,Parent:number,Alpha:number){ holoCreate(I,V,S,A,Col,Model) holoAlpha(I,Alpha) holoParent(I,Parent) }

function number co(A, B, C){ return acos((A^2 + B^2 - C^2) / (2*A*B)) }
function number cosine (One, Two, Three){ return acos( (Three^2 + Two^2 - One^2) / (2*Two*Three) ) }

function ik(L1,L2,Hip:vector,End:vector,I1,I2,Bs:entity){
local Axis=Bs:toLocalAxis(End-Hip)
local Angle=Axis:toAngle():setRoll(-bearing(Hip,Bs:angles(),End))
local Dist=min(Axis:length(),L1+L2)
local Quat=quat(Angle)*qRotation(vec(0,90+co(Dist,L1,L2),0))
holoAng(I1,Bs:toWorld(Quat:toAngle()))
holoAng(I2,holoEntity(I1):toWorld(ang(co(L2, L1, Dist)+180,0,0)))
}
 function kinemize ([Ent Hip Knee Ankle]:entity, FootPos:vector,L1,L2,L3)  {
local        Total = L1 + L2 +L3
local        Bearing = -bearing(Hip:pos(), Ent:angles(), FootPos)
local        Angle = Ent:toLocalAxis(FootPos - Hip:pos()):toAngle():setRoll(Bearing)
local        Offset = Total - abs(min(Hip:pos():distance(FootPos), Total))
local        Quat = quat(Angle) * qRotation(vec(0, -Offset + 0, 0))
        holoAng(holoIndex(Hip), Ent:toWorld(slerp(quat(Hip), Quat, 1):toAngle()))
local        Distance = min(Knee:pos():distance(FootPos), L2+L3)
local        Axis = Hip:toLocalAxis(Knee:pos() - FootPos)
local        Atan = atan(-Axis:z(), Axis:x()) + cosine(L3, L2, Distance) + 90
        holoAng(holoIndex(Knee), Hip:toWorld(ang(Atan,0,0)))
        holoAng(holoIndex(Ankle), Knee:toWorld(ang(cosine(Distance, L2, L3), 0, 0)))
    }


function number table:setMoveVector(StartCounter,BaseEntity:entity,End:vector,EndCounter,StartMove,CounterSpeed,FixSpeed,SaveString:string){
    RoundNR = 0
if(changed(BaseEntity)&BaseEntity){
This["VecC"+SaveString,vector] = BaseEntity:pos()    
}elseif(changed(BaseEntity)){
This["VecC"+SaveString,vector] = BaseEntity:pos()      
}
local C = This["Counter"+SaveString,number]
if(changed(C==0)){  This["Counter"+SaveString,number] = StartCounter }

This["Counter"+SaveString,number] = This["Counter"+SaveString,number] + 
floor(clamp(BaseEntity:vel():length()/CounterSpeed,1,20),RoundNR)


if(This["Counter"+SaveString,number] >= EndCounter){
     This["Counter"+SaveString,number]=0
    } 
local StepC = This["Counter"+SaveString,number]

local StepSpeed = round(clamp(BaseEntity:vel():length()/2,12,20)) * FixSpeed

This["VecC"+SaveString,vector] = This["VecC"+SaveString,vector] + (tickInterval()*StepSpeed)
 * clamp(This["EndPos"+SaveString,vector] - This["VecC"+SaveString,vector],-25,50)
clamp(BaseEntity:vel():length()/CounterSpeed,1,15)

if(This["Counter"+SaveString,number]>=StartMove
&End:distance(This["FinalVec"+SaveString,vector] )>10&This["StartIt"+SaveString,number]==0){ 
    
This["StartIt"+SaveString,number] = 1
This["EndPos"+SaveString,vector]  =  End   
This["Dist"+SaveString,number]  =  End:distance(This["VecC"+SaveString,vector])
     }
    local DS = This["VecC"+SaveString,vector]:distance(This["EndPos"+SaveString,vector] )<=1
if(changed(DS)&DS&This["StartIt"+SaveString,number] == 1){
This["StartIt"+SaveString,number] = 0
This["Counter"+SaveString,number]= EndCounter

#BaseEntity:soundPlay(random(255),1,"plats/crane/vertical_stop.wav")


}

if(This["StartIt"+SaveString,number] == 1){
This["FinalVec"+SaveString,vector] = This["VecC"+SaveString,vector] + BaseEntity:up() *
 -sin(This["EndPos"+SaveString,vector]:distance(This["VecC"+SaveString,vector]) / This["Dist"+SaveString,number] 
* 180 - 180) * min(This["Dist"+SaveString,number]/3,30)
}

}


c(1,E:toWorld(vec(0,20,0)),vec(1),E:angles(),vec(255,255,0),"hqsphere",E,255)
c(2,holoEntity(1):toWorld(vec(0,0,70)),vec(1),E:angles(),vec(255,255,0),"hqsphere",1,255)
c(3,holoEntity(2):toWorld(vec(0,0,70)),vec(1),E:angles(),vec(255,255,0),"hqsphere",2,255)
c(4,holoEntity(1):toWorld(vec(0,0,35)),vec(1,1,5),E:angles(),vec(255,255,0),"hqcylinder",1,255)
c(5,holoEntity(2):toWorld(vec(0,0,35)),vec(1,1,5),E:angles(),vec(255,255,0),"hqcylinder",2,255)

c(6,E:toWorld(vec(0,-20,0)),vec(1),E:angles(),vec(255,255,0),"hqsphere",E,255)
c(7,holoEntity(6):toWorld(vec(0,0,70)),vec(1),E:angles(),vec(255,255,0),"hqsphere",6,255)
c(8,holoEntity(7):toWorld(vec(0,0,70)),vec(1),E:angles(),vec(255,255,0),"hqsphere",7,255)
c(9,holoEntity(6):toWorld(vec(0,0,35)),vec(1,1,5),E:angles(),vec(255,255,0),"hqcylinder",6,255)
c(10,holoEntity(7):toWorld(vec(0,0,35)),vec(1,1,5),E:angles(),vec(255,255,0),"hqcylinder",7,255)

}
rangerFilter(E)
Ran1 = rangerOffset(100,E:toWorld(vec(0,20,0)),vec(0,0,-1))
rangerFilter(E)
Ran2 = rangerOffset(100,E:toWorld(vec(0,-20,0)),vec(0,0,-1))

ik(70,70,holoEntity(1):pos(),T1["FinalVec"+"l",vector],1,2,E)
T1:setMoveVector(1,E,Ran1:pos(),10,5,100,2.5,"l")
ik(70,70,holoEntity(6):pos(),T2["FinalVec"+"r",vector],6,7,E)
T2:setMoveVector(10,E,Ran2:pos(),20,15,100,2.5,"r")
