@name Sky private vehicle
@inputs Egp:wirelink
@outputs 
@persist [Model Position Angles Props]:array XY Counter Forward [R1 R2 R3 R4 M1 M2 Ro2 Ro1 Ta]:entity 
@trigger 
runOnTick(1)
runOnChat(1)
runOnLast(1)

Counter++

E=entity()
Seat=E:isWeldedTo()
Driver=Seat:driver()

Space=Driver:keyJump()
Shift=Driver:keySprint()
W=Driver:keyForward()
A=Driver:keyLeft()
S=Driver:keyBack()
D=Driver:keyRight()
Mouse1=Driver:keyAttack1()
Mouse2=Driver:keyAttack2()
R=Driver:keyReload()

Seat:propFreeze(1)
####################################3
##############props###############3
######################################
if(first()|duped()|dupefinished()){
    Seat:setAng(ang())
    holoCreate(2,Seat:toWorld(vec(0,0,0)),vec(1,1,1),ang(),vec(1,1,1),"hqsphere")
    
Model=array(
"models/hunter/tubes/tube2x2x1.mdl","models/hunter/tubes/tube2x2x1d.mdl","models/hunter/tubes/tube2x2x1d.mdl",
"models/xqm/jetenginemedium.mdl","models/props_phx/construct/metal_angle180.mdl","models/props_phx/construct/metal_angle180.mdl",
"models/hunter/tubes/tube2x2x025.mdl","models/props_junk/sawblade001a.mdl","models/props_junk/sawblade001a.mdl",
"models/mechanics/wheels/wheel_spike_48.mdl","models/mechanics/wheels/wheel_spike_48.mdl","models/props_phx/construct/metal_angle360.mdl"
,"models/props_phx/construct/metal_angle360.mdl","models/xqm/jetengine.mdl","models/xqm/jetengine.mdl"
,"models/props_phx/construct/plastic/plastic_angle_90.mdl","models/props_phx/construct/plastic/plastic_angle_90.mdl",""
,"","",""
,"","","",
"","",""
,"","",""
)

Position=array(
vec(0,0,0),vec(-10,0,-10),vec(-10,0,10),
vec(-70,0,0),vec(5,30,0),vec(5,-26,0),
vec(-130,0,0),vec(-20,50,0),vec(-20,-50,0),
vec(-20,80,0),vec(-20,-80,0),vec(-20,-80,0)
,vec(-20,80,0),vec(-20,80,-20),vec(-20,-80,-20)
,vec(-30,-100,0),vec(-30,100,0),vec(0,0,0)
,vec(0,0,0),vec(0,0,0),vec(0,0,0)
,vec(0,0,0),vec(0,0,0),vec(0,0,0)
,vec(0,0,0),vec(0,0,0),vec(0,0,0)
,vec(0,0,0),vec(0,0,0),vec(0,0,0)
,vec(0,0,0),vec(0,0,0),vec(0,0,0)
)
Angles=array(
ang(0,0,90),ang(0,0,90),ang(0,0,270),
ang(0,0,0),ang(0,0,90),ang(0,0,90),
ang(0,90,90),ang(0,0,90),ang(0,0,90),
ang(0,0,0),ang(0,0,0),ang(0,0,0),
ang(0,0,0),ang(90,0,0),ang(90,0,0),
ang(180,0,0),ang(180,0,180),ang(0,0,0),
ang(0,0,0),ang(0,0,0),ang(0,0,0),
ang(0,0,0),ang(0,0,0),ang(0,0,0),
ang(0,0,0),ang(0,0,0),ang(0,0,0),
ang(0,0,0),ang(0,0,0),ang(0,0,0)
)
}
holoAng(2,Driver:eyeAngles())
holoParent(2,Seat)
timer("spawn",250)
if(changed(E)&E|clk("spawn")){
for(I=1,Model:count()){
 if(!Props[I,entity]){
    Pos=Seat:toWorld(vec(0,0,10))
 Props[I,entity]=propSpawn(Model[I,string],Pos+Position[I,vector],Angles[I,angle],1) 
Props[I,entity]:parentTo(holoEntity(2))
break
}   

Props[I,entity]:setMaterial("phoenix_storms/gear")
Props[1,entity]:setMaterial("models/vortigaunt/pupil")
Props[1,entity]:setColor(vec(0,255,255))
Props[I,entity]:setColor(vec(100,100,100))
}
}


if(last()){
 propDeleteAll()   
}

##########################
########################
##########################
if(first()){
 holoCreate(3,holoEntity(2):toWorld(vec(-130,0,10)),vec(5,2,2),ang(),vec(255,0,0),"hqsphere")   
 holoCreate(4,holoEntity(2):toWorld(vec(-20,80,-20)),vec(5,2,2),ang(90,0,0),vec(255,0,0),"hqsphere")   
 holoCreate(5,holoEntity(2):toWorld(vec(-20,-80,-20)),vec(5,2,2),ang(90,0,0),vec(255,0,0),"hqsphere")   
holoEntity(3):setTrails(30,0,3,"trails/smoke",vec(25,25,25),255)


 holoCreate(10,holoEntity(2):toWorld(vec(0,0,9)),vec(6,6,3),ang(0,0,90),vec(25,150,25),"hqcylinder")  
 holoCreate(11,holoEntity(2):toWorld(vec(0,0,9)),vec(5,5,4),ang(0,0,90),vec(1,1,1),"hqcylinder")   




holoCreate(12,holoEntity(2):toWorld(vec(0,-35,70)),vec(1,1,1),ang(0,90,90),vec(35,35,35),"hqcylinder")  
holoCreate(13,holoEntity(2):toWorld(vec(0,35,70)),vec(1,1,1),ang(0,90,90),vec(35,35,35),"hqcylinder")  
holoCreate(14,holoEntity(13):toWorld(vec(0,0,15)),vec(0.3,0.3,2),ang(0,90,90),vec(100,100,100),"hqtube")
holoCreate(15,holoEntity(12):toWorld(vec(0,0,15)),vec(0.3,0.3,2),ang(0,90,90),vec(100,100,100),"hqtube")


 holoCreate(6,holoEntity(2):toWorld(vec(10,-20,-50)),vec(2,2,2),ang(0,0,0),vec(255,255,255),"models/weapons/w_missile.mdl")  
 holoCreate(7,holoEntity(2):toWorld(vec(10,20,-50)),vec(2,2,2),ang(0,0,0),vec(255,255,255),"models/weapons/w_missile.mdl")  

 holoCreate(8,holoEntity(2):toWorld(vec(10,-10,-60)),vec(2,2,2),ang(0,0,0),vec(255,255,255),"models/weapons/w_missile.mdl")  
 holoCreate(9,holoEntity(2):toWorld(vec(10,10,-60)),vec(2,2,2),ang(0,0,0),vec(255,255,255),"models/weapons/w_missile.mdl")    
}
holoParent(3,2)
holoParent(4,2)
holoParent(5,2)

if(Driver){
holoAlpha(3,255)
    holoAlpha(4,255)
    holoAlpha(5,255)
}else{
holoAlpha(3,0)
    holoAlpha(4,0)
    holoAlpha(5,0)

}


holoParent(6,2)
holoParent(7,2)
holoParent(8,2)
holoParent(9,2)
holoParent(10,2)
holoParent(11,2)
holoParent(12,2)
holoParent(13,2)
holoParent(14,12)
holoParent(15,13)
holoAng(3,holoEntity(2):toWorld(ang(0,0,90+Counter*5.5)))
holoMaterial(3,"models/alyx/emptool_glow")
holoMaterial(4,"models/alyx/emptool_glow")
holoMaterial(5,"models/alyx/emptool_glow")
XY=XY*1+3 X=cos(XY) Y=sin(XY)
if(first()){
    
    Speed=15
 holoCreate(1,Seat:toWorld(vec(0,0,50)),vec(1,1,1),ang(),vec(255,0,0),"hqsphere")   
}
holoParent(1,Seat)
holoPos(1,Seat:toWorld(vec(X*150,Y*150,0)))

######controlls#######
 Forward=15  
Seat:setAlpha(1)
if(W){Seat:setPos(Seat:pos()+Driver:eye()*Forward)}
if(S){Seat:setPos(Seat:pos()+Driver:eye()*-Forward)}
if(Space){
 Seat:setPos(Seat:pos()+vec(0,0,Forward))   
}
if(Shift){
 Seat:setPos(Seat:pos()+vec(0,0,-Forward))   
}

########gun1##########
if(changed(R)&R){
holoAlpha(6,0) 
holoAlpha(7,0) 
holoAlpha(8,0) 
holoAlpha(9,0) 
   soundPlay(1,999,"weapons/shotgun/shotgun_dbl_fire.wav")
R1=propSpawn("models/props_phx/ww2bomb.mdl",(holoEntity(6):toWorld(vec(0,0,0))),0)
R2=propSpawn("models/props_phx/ww2bomb.mdl",(holoEntity(7):toWorld(vec(0,0,0))),0)
R3=propSpawn("models/props_phx/ww2bomb.mdl",(holoEntity(8):toWorld(vec(0,0,0))),0)
R4=propSpawn("models/props_phx/ww2bomb.mdl",(holoEntity(9):toWorld(vec(0,0,0))),0)
R4:setTrails(10,0,5,"trails/smoke",vec(75,75,75),255)
R3:setTrails(10,0,5,"trails/smoke",vec(75,75,75),255)
R2:setTrails(10,0,5,"trails/smoke",vec(75,75,75),255)
R1:setTrails(10,0,5,"trails/smoke",vec(75,75,75),255)
R4:applyForce(holoEntity(6):forward()*R4:mass()*2000)
R3:applyForce(holoEntity(7):forward()*R4:mass()*2000)
R2:applyForce(holoEntity(8):forward()*R4:mass()*2000)
R1:applyForce(holoEntity(9):forward()*R4:mass()*2000)
R1:propGravity(0)
R2:propGravity(0)
R3:propGravity(0)
R4:propGravity(0)
}
Ran=rangerOffset(75,R1:pos(),R1:pos())
if(Ran:hitWorld()){
R1:propBreak() 
R2:propBreak() 
R3:propBreak() 
R4:propBreak()    
}

if(!R4|!R3|!R2|!R1){
holoAlpha(6,255) 
holoAlpha(7,255) 
holoAlpha(8,255) 
holoAlpha(9,255) 
R1:propBreak() 
R2:propBreak() 
R3:propBreak() 
R4:propBreak()    

}


R4:applyForce(R4:forward()*R4:mass()*2000)
R4:setAng(Driver:eyeAngles())
R3:applyForce(R3:forward()*R4:mass()*2000)
R3:setAng(Driver:eyeAngles())
R2:applyForce(R2:forward()*R4:mass()*2000)
R2:setAng(Driver:eyeAngles())
R1:applyForce(R1:forward()*R4:mass()*2000)
R1:setAng(Driver:eyeAngles())
##################################
########gun2############
timer("reload",350)
if(Mouse1&clk("reload")){
    soundPlay(1,999,"weapons/ak47/ak47_fire1.wav")
M1=propSpawn("models/props_junk/garbage_glassbottle003a.mdl",(holoEntity(12):toWorld(vec(0,0,0))),0)
M2=propSpawn("models/props_junk/garbage_glassbottle003a.mdl",(holoEntity(13):toWorld(vec(0,0,0))),0)
M1:setTrails(10,0,5,"trails/smoke",vec(75,75,75),255)
M2:setTrails(10,0,5,"trails/smoke",vec(75,75,75),255)
M1:propGravity(0)
M2:propGravity(0)
M2:setMass(150)
M1:setMass(150)
M1:applyForce(holoEntity(12):up()*M1:mass()*6500)
M2:applyForce(holoEntity(13):up()*M2:mass()*6500)
}
##########################
########GUN3@###########

if(first()){
 holoCreate(20,holoEntity(2):toWorld(vec(-20,-50,-20)),vec(0.8,0.8,0.8),ang(),vec(255,255,255),"models/props_phx/mk-82.mdl")  
 holoCreate(21,holoEntity(2):toWorld(vec(-20,50,-21)),vec(0.8,0.8,0.8),ang(),vec(255,255,255),"models/props_phx/mk-82.mdl")  
}
holoParent(20,2)
holoParent(21,2)



findByClass("player")
findExcludeEntity(owner())
findExcludeEntity(Driver)
Ta=findClosest(Seat:pos())



if(changed(Mouse2)&Mouse2){
    soundPlay(1,999,"weapons/rpg/rocketfire1.wav")
holoAlpha(20,0)
holoAlpha(21,0)
print("Your target is "+Ta:name()) 
Ro2=propSpawn("models/props_phx/mk-82.mdl",(holoEntity(21):toWorld(vec(0,0,-30))),0)  
Ro2:propGravity(0)

Ro1=propSpawn("models/props_phx/mk-82.mdl",(holoEntity(20):toWorld(vec(0,0,-30))),0)  
Ro1:propGravity(0)
}


A2=(Ta:pos()-holoEntity(20):pos()):toAngle()
A1=(Ta:pos()-holoEntity(21):pos()):toAngle()

Ro2:setAng(A1)
Ro1:setAng(A1)



Ro1:applyForce(Ro1:forward()*Ro1:mass()*2000)
Ro2:applyForce(Ro2:forward()*Ro2:mass()*2000)
if(!Ro1){
    holoAlpha(20,255)
}
if(!Ro2){
    holoAlpha(21,255)
}
#######################333
########egp#########3


#####anti ueberlastung egp thing##########
timer("egp",250)
if(changed(E)&E|clk("egp")){
    ####################
    
for(P=1,players():count()){
Ply=players()[P,entity] 
Egp:egp3DTracker(P+100,Ply:pos())
Egp:egpSize(P+5,25)
if(Ply:isAlive()){Egp:egpColor(P+5,vec(255,255,0))}else{Egp:egpColor(P+5,vec(255,0,0))}
Egp:egpText(P+5,P+" "+Ply:name(),vec2())
Egp:egpParent(P+5,P+100)
}
   #############



}
#########anti noob sicherung###########3 
if(Driver:name()!=owner():name()){
 Seat:killPod()   
}  
########################
