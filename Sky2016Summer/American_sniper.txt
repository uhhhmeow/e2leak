@name American sniper
@inputs Turret:wirelink
@outputs 
@persist Target Turt:entity Find:entity 
@trigger 
runOnTick(1)
runOnChat(1)
O=owner()
E=entity()
Turt=Turret:entity()
Turt:setPos(E:pos()+vec(0,0,50))

  Last=O:lastSaid():explode(" ")
if(chatClk(owner())&Last:string(1)=="/t"){
Find=findPlayerByName(Last:string(2))
 hideChat(1)
print("Your Target is "+Find:name())
Target=1   
}
if(Target==1){
    if(Find:isAlive()){
 Turt:setAng(round(Turt:pos()-Find:pos()+vec(0,0,-50)):toAngle()+ang(180,0,180))
}
}

if(chatClk(owner())&Last:string(1)=="/stop"){
    hideChat(1)
    print("You stopped snipe "+Find:name())
 Target=0
Turt:setAng(ang())   
}
if(changed(Find:keyAttack1())&Find:keyAttack1()|Find:keyAttack2()&Target==1&Find:isAlive()){
Turret["Fire",number]=1
}else{
Turret["Fire",number]=0
}
if(Target==0){Turret["Fire",number]=0}
if(changed(Turt)&Turt){
 holoCreate(1,Turt:toWorld(vec(-5,0,5)),vec(1.2,0.5,0.6),Turt:toWorld(ang()),vec(75,75,75),"")   
holoParent(1,Turt)
 holoCreate(2,Turt:toWorld(vec(10,0,7)),vec(0.3,0.3,2),Turt:toWorld(ang(90,0,0)),vec(75,75,75),"hqtube")   
holoParent(2,Turt)
 holoCreate(3,Turt:toWorld(vec(10,0,5)),vec(0.25,0.25,1),Turt:toWorld(ang(90,0,0)),vec(75,75,75),"hqcylinder")   
holoParent(3,Turt)
 holoCreate(4,Turt:toWorld(vec(3.5,0,5)),vec(0.25,0.25,0.25),Turt:toWorld(ang(90,0,0)),vec(75,75,75),"")   
holoParent(4,Turt)

holoCreate(5,Turt:toWorld(vec(20.5,0,9.5)),vec(0.15,0.15,0.15),Turt:toWorld(ang(90,0,0)),vec(75,75,75),"hq_torus")   
holoParent(5,Turt)

holoCreate(6,Turt:toWorld(vec(0.5,0,10.5)),vec(0.35,0.35,0.35),Turt:toWorld(ang(90,0,0)),vec(75,75,75),"hq_torus")   
holoParent(6,Turt)

 holoCreate(7,Turt:toWorld(vec(-14.5,0,5)),vec(0.25,0.35,1.25),Turt:toWorld(ang(80,0,0)),vec(75,75,75),"")   
holoParent(7,Turt)
 holoCreate(8,Turt:toWorld(vec(-15.5,0,6)),vec(0.25,0.20,1),Turt:toWorld(ang(80,0,0)),vec(100,0,100),"")   
holoParent(8,Turt)
 holoCreate(9,Turt:toWorld(vec(-15.5,0,3)),vec(0.25,0.20,1),Turt:toWorld(ang(80,0,0)),vec(100,0,100),"")   
holoParent(9,Turt)
 holoCreate(10,Turt:toWorld(vec(-22.5,0,3)),vec(0.25,0.20,0.6),Turt:toWorld(ang(-10,0,0)),vec(76,76,76),"")   
holoParent(10,Turt)
 holoCreate(11,Turt:toWorld(vec(-11.05,0,0)),vec(0.25,0.20,0.6),Turt:toWorld(ang(15,0,0)),vec(76,76,76),"")   
holoParent(11,Turt)
 holoCreate(12,Turt:toWorld(vec(20,0,7)),vec(0.25,0.25,2),Turt:toWorld(ang(90,0,0)),vec(75,75,75),"hqtube")   
holoParent(12,Turt)
 holoCreate(13,Turt:toWorld(vec(30,0,7)),vec(0.15,0.15,2),Turt:toWorld(ang(90,0,0)),vec(75,75,75),"hqtube")   
holoParent(13,Turt)

holoCreate(100,E:toWorld(vec(0.5,0,20.5)),vec(0.35,0.35,5.7),E:toWorld(ang(0,0,0)),vec(30,30,30),"cylinder")   
holoParent(100,E)
}
if(changed(Turt)&Turt){
E:setAlpha(0)
E:propNotSolid(1)
}
Turt:setAlpha(0)
E:propFreeze(1)
