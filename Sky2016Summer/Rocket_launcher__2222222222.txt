@name Rocket launcher 
@persist [E]:entity
@persist Feuer2 Ran2:ranger
if(first()) {
runOnTick(1)
E=entity()
holoCreate(1)
holoPos(1,E:pos()+vec(0,0,15))   
holoColor(1,vec(100,100,100))
holoModel(1,"hqcylinder")
holoScale(1,vec(3,3,5))

holoCreate(2)
holoPos(2,E:pos()+vec(0,0,35))
holoModel(2,"hqcylinder")
holoScale(2,vec(1,1,5))
holoAng(2,ang(90,0,0))
holoColor(2,vec(255,0,0))

holoCreate(3)
holoPos(3,E:pos()+vec(32,0,35))
holoModel(3,"hqsphere")
holoScale(3,vec(0.7,0.7,0.7))
holoAng(3,ang(90,0,0))
holoColor(3,vec(0,0,0))

holoCreate(4)
holoPos(4,E:pos()+vec(-32,0,35))
holoModel(4,"hqsphere")
holoScale(4,vec(0.7,0.7,0.7))
holoAng(4,ang(90,0,0))
holoColor(4,vec(0,0,0))

holoCreate(5)
holoScale(5,vec(0.1,0.1,0.1))
holoColor(5,vec(255,0,0))
holoModel(5,"hq_torus_thin")
holoMaterial(5,"models/vortigaunt/pupil")

holoCreate(6)
holoPos(6,E:pos()+vec(-32,12,35))
holoModel(6,"cube")
holoScale(6,vec(1.5,1.5,1.5))
holoAng(6,ang(0,0,0))
holoColor(6,vec(100,100,100))

holoCreate(7)
holoPos(7,E:pos()+vec(32,12,35))
holoModel(7,"cube")
holoScale(7,vec(1.5,1.5,1.5))
holoAng(7,ang(0,0,0))
holoColor(7,vec(100,100,100))

holoCreate(8)
holoPos(8,E:pos()+vec(32,30,35))
holoModel(8,"hqcylinder")
holoScale(8,vec(1.5,1.5,1.5))
holoAng(8,ang(0,0,90))
holoAlpha(8,233)
holoColor(8,vec(50,50,50))

holoCreate(9)
holoPos(9,E:pos()+vec(23,33,39))
holoModel(9,"models/props_phx/amraam.mdl")
holoScale(9,vec(1,1,1))
holoAng(9,ang(90,0,0))
holoColor(9,vec(255,255,255))

holoCreate(10)
holoPos(10,E:pos()+vec(-38.5,29,40))
holoModel(10,"models/props_phx/ww2bomb.mdl")
holoScale(10,vec(1,1,1))
holoAng(10,ang(90,0,0))
holoColor(10,vec(255,255,255))

holoCreate(11)
holoPos(11,E:pos()+vec(-38.5,44,40))
holoModel(11,"models/props_phx/ww2bomb.mdl")
holoScale(11,vec(1,1,1))
holoAng(11,ang(90,0,0))
holoColor(11,vec(255,255,255))

holoCreate(12)
holoPos(12,E:pos()+vec(0,0,45))
holoAlpha(12,80)
holoModel(12,"hq_hdome")
holoScale(12,vec(3,3,7))
holoAng(12,ang(0,0,0))
holoColor(12,vec(50,50,255))

holoCreate(13)
holoPos(13,E:pos()+vec(-32.5,38,35))
holoAlpha(13,252)
holoModel(13,"hqcylinder")
holoScale(13,vec(1,1,3))
holoAng(13,ang(90,90,0))
holoColor(13,vec(50,50,50))

holoParent(12,E)
holoParent(13,8)
holoParent(11,E)
holoParent(11,8)
holoParent(10,8)
holoParent(9,8)
holoParent(8,7)
holoParent(7,3)
holoParent(6,4)
holoParent(2,E)
holoParent(1,E)
holoParent(3,2)
holoParent(4,2)
}

findByClass("prop_vehicle_prisoner_pod")
findSortByDistance(E:pos())
    if(find():model() == "models/nova/jeep_seat.mdl"){
        Seat=find()
        Seat:setPos(E:pos()+vec(1,-13,25))
    }
    Driver=Seat:driver()
    Seat:setAng(ang())
    Seat:setAlpha(0)
    
    Mouse1=Driver:keyAttack1()
    Mouse2=Driver:keyAttack2()
if(Driver){
holoPos(5,Driver:shootPos()+Driver:eye()*4)   
holoAng(5,Driver:eyeAngles()+ang(90,0,0))
holoAng(2,Driver:eyeAngles()+ang(0,0,90))   
}
if(Mouse2&Feuer2==0){
Feuer2=1
}
if(Feuer2) {
holoUnparent(9)
holoPos(9,holoEntity(9):pos()+holoEntity(9):forward()*25)
Ran2=rangerOffset(1000,holoEntity(9):pos(),holoEntity(9):forward())
}
if(Ran2:distance()<50&Feuer2) {
    A=propSpawn("models/props_phx/amraam.mdl",holoEntity(9):pos()+vec(1,1,1),0)
    A:propBreak()
    holoDelete(9)
    timer("Reload2",2000)
    Feuer2=0
}
if(clk("Reload2")) {
holoCreate(9,holoEntity(8):pos()+vec(0,0,-5),vec(1),Driver:eyeAngles(),vec(255),"models/props_phx/amraam.mdl")
holoParent(9,7)
}


