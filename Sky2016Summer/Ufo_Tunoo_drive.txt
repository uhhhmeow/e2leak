@name Ufo Tunoo drive
@inputs Egp:wirelink
@persist Timer P1:entity P2:entity
 
E=entity()
Seat=E:isWeldedTo()
Driver=Seat:driver()
Timer=Timer+1*3
T=Timer

runOnTick(1)
if(first()){
################Haupkeorper###########################################

holoCreate(1)
holoModel(1,"models/hunter/misc/sphere2x2.mdl")
holoScale(1,vec(1,1,1))
holoPos(1,Seat:pos()+vec(0,0,10))
holoAng(1,ang())
#holoColor(1,vec(0,0,0))
holoMaterial(1,"sprops/trans/misc/ls_m1")

####################Haupkanone Rechts##################################

holoCreate(2)
holoModel(2,"models/props_phx/misc/smallcannon.mdl")
holoScale(2,vec(1,1,1))
holoPos(2,Seat:pos()+vec(-10,56,3))
holoAng(2,ang(90,90,90))
holoColor(2,vec(70,70,70))
holoMaterial(2,"")

#############Haupkanone Links########################################

holoCreate(3)
holoModel(3,"models/props_phx/misc/smallcannon.mdl")
holoScale(3,vec(1,1,1))
holoPos(3,Seat:pos()+vec(-10,-56,3))
holoAng(3,ang(90,90,90))
holoColor(3,vec(70,70,70))
holoMaterial(3,"")

#################Fenster############################

holoCreate(4)
holoModel(4,"models/hunter/plates/plate025x075.mdl")
holoScale(4,vec(1,1,2.3))
holoPos(4,Seat:pos()+vec(44.4,0,15))
holoAng(4,ang(90,90,90))
holoColor(4,vec(0,0,0))
holoMaterial(4,"sprops/trans/misc/ls_m1")

############Fenster Rahmen Oben######################################

holoCreate(5)
holoModel(5,"models/hunter/plates/plate025x075.mdl")
#holoScale(5,vec(1,1,1))
holoPos(5,Seat:pos()+vec(45,0,22))
holoAng(5,ang(0,0,0))
#holoColor(5,vec(0,0,0))
holoMaterial(5,"sprops/trans/misc/ls_m1")

##################Fenster Rahmen rechts################################

holoCreate(6)
holoModel(6,"models/hunter/plates/plate025x025.mdl")
holoScale(6,vec(1,1.3,1))
holoPos(6,Seat:pos()+vec(45,19,16))
holoAng(6,ang(0,0,90))
#holoColor(6,vec(0,0,0))
holoMaterial(6,"sprops/trans/misc/ls_m1")

##################Fenster Rahmen links######################################################

holoCreate(7)
holoModel(7,"models/hunter/plates/plate025x025.mdl")
holoScale(7,vec(1,1.3,1))
holoPos(7,Seat:pos()+vec(45,-19,16))
holoAng(7,ang(0,0,90))
#holoColor(7,vec(0,0,0))
holoMaterial(7,"sprops/trans/misc/ls_m1")

##########Haupkanonen Ring Rechts########################################

holoCreate(8)
holoModel(8,"models/hunter/tubes/tube2x2x025c.mdl")
holoScale(8,vec(0.10,0.30,0.30))
holoPos(8,Seat:pos()+vec(-5,65,13))
holoAng(8,ang(0,-90,0))
holoColor(8,vec(250,250,250))
holoMaterial(8,"models/props_combine/masterinterface01c")

########Haupkanonen Ring Links############################################

holoCreate(9)
holoModel(9,"models/hunter/tubes/tube2x2x025c.mdl")
holoScale(9,vec(0.10,0.30,0.30))
holoPos(9,Seat:pos()+vec(-5,-65,13))
holoAng(9,ang(0,90,0))
holoColor(9,vec(250,250,250))
holoMaterial(9,"models/props_combine/masterinterface01c")

################Turbine rechts#######################################

holoCreate(10)
holoModel(10,"models/xqm/jetenginepropeller.mdl")
holoScale(10,vec(0.8,0.8,0.8))
holoPos(10,Seat:pos()+vec(-45,56,13))
holoAng(10,ang(0,0,T))
#holoColor(10,vec(0,0,0))
#holoMaterial(10,"phoenix_storms/car_tire")

################Turbine Links##########################################

holoCreate(11)
holoModel(11,"models/xqm/jetenginepropeller.mdl")
holoScale(11,vec(0.8,0.8,0.8))
holoPos(11,Seat:pos()+vec(-45,-56,13))
holoAng(11,ang(0,0,T))
#holoColor(11,vec(0,0,0))
#holoMaterial(11,"phoenix_storms/car_tire")

##########Auspuf Kanone rechts###########################################

holoCreate(12)
holoModel(12,"models/sprops/trans/exhaust/tip_6.mdl")
holoScale(12,vec(1,1,1))
holoPos(12,Seat:pos()+vec(-45,-56,33))
holoAng(12,ang(0,0,0))
#holoColor(12,vec(0,0,0))
#holoMaterial(12,"phoenix_storms/car_tire")

###########Auspuf Links#####################################################

holoCreate(13)
holoModel(13,"models/sprops/trans/exhaust/tip_6.mdl")
holoScale(13,vec(1,1,1))
holoPos(13,Seat:pos()+vec(-45,56,33))
holoAng(13,ang(0,0,0))
#holoColor(13,vec(0,0,0))
#holoMaterial(13,"phoenix_storms/car_tire")

#########Rechter Pufet#####################################

holoCreate(14)
holoModel(14,"models/xqm/cylinderx1medium.mdl")
holoScale(14,vec(1,1,1))
holoPos(14,Seat:pos()+vec(30,56,13))
holoAng(14,ang(0,0,T))
#holoColor(14,vec(0,0,0))
#holoMaterial(14,"phoenix_storms/car_tire")

#############Linker Pufet#####################################

holoCreate(15)
holoModel(15,"models/xqm/cylinderx1medium.mdl")
holoScale(15,vec(1,1,1))
holoPos(15,Seat:pos()+vec(30,-56,13))
holoAng(15,ang(0,0,T))
#holoColor(15,vec(0,0,0))
#holoMaterial(15,"phoenix_storms/car_tire")
}
#######controlls#########
W=Driver:keyForward()
S=Driver:keyBack()
Shift=Driver:keySprint()
Mouse1=Driver:keyAttack1()
Mouse2=Driver:keyAttack2()
I=Driver:keyPressed("I")
###########
holoParent(1,Seat)
holoParent(2,1)
holoParent(3,1)
holoParent(4,1)
holoParent(5,1)
holoParent(6,1)
holoParent(7,1)
holoParent(8,1)
holoParent(9,1)
holoParent(10,1)
holoParent(11,1)
holoParent(12,1)
holoParent(13,1)
holoParent(14,1)
holoParent(15,1)
#########
if(W){
 Seat:setPos(Seat:pos()+Driver:eye()*15)   
}
if(Shift&W){
 Seat:setPos(Seat:pos()+Driver:eye()*45)   
}
if(S){
 Seat:setPos(Seat:pos()+Driver:eye()*-15)   
}
if(Shift&S){
 Seat:setPos(Seat:pos()+Driver:eye()*-45)   
}
holoAng(1,Driver:eyeAngles())

if(I){
 holoMaterial(1,"models/shadertest/predator")
holoMaterial(2,"models/shadertest/predator")   
holoMaterial(3,"models/shadertest/predator")   
holoMaterial(4,"models/shadertest/predator")  
holoMaterial(5,"models/shadertest/predator")   
holoMaterial(6,"models/shadertest/predator")   
holoMaterial(7,"models/shadertest/predator")   holoMaterial(8,"models/shadertest/predator")   
holoMaterial(9,"models/shadertest/predator")   
holoMaterial(10,"models/shadertest/predator")    
holoMaterial(11,"models/shadertest/predator")   
holoMaterial(12,"models/shadertest/predator")   
holoMaterial(13,"models/shadertest/predator")   
holoMaterial(14,"models/shadertest/predator")   
holoMaterial(15,"models/shadertest/predator")   
E:setMaterial("models/shadertest/predator")
Egp:entity():setMaterial("models/shadertest/predator")
Driver:setMaterial("effects/ar2_altfire1b")
}else{
 holoMaterial(1,"sprops/trans/misc/ls_m1")
holoMaterial(2,"")   
holoMaterial(3,"")   
holoMaterial(4,"sprops/trans/misc/ls_m1")  
holoMaterial(5,"sprops/trans/misc/ls_m1")   
holoMaterial(6,"sprops/trans/misc/ls_m1")   
holoMaterial(7,"sprops/trans/misc/ls_m1")   
holoMaterial(8,"models/props_combine/masterinterface01c")   
holoMaterial(9,"models/props_combine/masterinterface01c")   
holoMaterial(10,"")    
holoMaterial(11,"")   
holoMaterial(12,"")   
holoMaterial(13,"")   
holoMaterial(14,"")   
holoMaterial(15,"")   
E:setMaterial("")
Egp:entity():setMaterial("")
Driver:setMaterial("")
}
Seat:setMaterial("models/shadertest/predator")
####################
######GUNS (NED ANFASSEN)#########
Ang=(Driver:aimPos()-P1:pos()):toAngle()
Ang2=(Driver:aimPos()-P2:pos()):toAngle()
if(changed(Mouse1)&Mouse1){
    P1=propSpawn("models/props_phx/ww2bomb.mdl",holoEntity(2):toWorld(vec(0,50,0)),0)
    P1:propGravity(0)
     P1:setTrails(10,50,0.5,"trails/smoke",vec(75,75,75),255)
}
 P1:applyForce(P1:forward()*P1:mass()*5000)
if(changed(Mouse2)&Mouse2){
        P2=propSpawn("models/props_phx/ww2bomb.mdl",holoEntity(3):toWorld(vec(0,-50,0)),0)
    P2:propGravity(0)
    P2:setTrails(10,50,0.5,"trails/smoke",vec(75,75,75),255)
}
   P2:applyForce(P2:forward()*P2:mass()*5000)

P1:setAng(Ang)
P2:setAng(Ang2)
#######################
##egp#########
timer("egp",150)
if(clk("egp")){
    for(P=1,players():count()){
     Ply=players()[P,entity]
    Egp:egp3DTracker(P+15,Ply:pos())
    Egp:egpText(P+5,P+" "+Ply:name(),vec2(0,-15))  
    Egp:egpParent(P+5,P+15) 
    if(Ply:isAlive()){Egp:egpColor(P+5,vec(0,0,255))}else{Egp:egpColor(P+5,vec(255,0,0))}
    
    Egp:egpCircleOutline(123,vec2(800,450),vec2(10,10))
    }
    
    
}
