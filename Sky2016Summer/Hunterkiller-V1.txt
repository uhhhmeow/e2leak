@name Hunterkiller-V1
@inputs Cam:wirelink
@outputs 
@persist Seat:entity  Lock [T T1 T2 T3 T4 E2]:table  Names:array PropLock:table Par:array Mode:string MidH
@model models/hunter/blocks/cube05x05x05.mdl

interval(60)
E = entity()

WorldDownVec = E:up()*-1

if(first()){
E:setMass(50000)
function number c(I,V:vector,S:vector,A:angle,Col:vector,Model:string,Alpha:number){ holoCreate(I,V,S,A,Col,Model) holoAlpha(I,Alpha) }
function number c(I,V:vector,S:vector,A:angle,Col:vector,Model:string,Parent:entity,Alpha:number){ holoCreate(I,V,S,A,Col,Model) holoAlpha(I,Alpha) holoParent(I,Parent) }
function number c(I,V:vector,S:vector,A:angle,Col:vector,Model:string,Parent:number,Alpha:number){ holoCreate(I,V,S,A,Col,Model) holoAlpha(I,Alpha) holoParent(I,Parent) }
function number spawnHolos(Start,End){ return (holoCanCreate()&perf()&holoEntity(Start)&!holoEntity(End)) }
function number spawnHolos(End){ return (holoCanCreate()&perf()&!holoEntity(End)) }

function setMode(Name:string){ Mode = Name }
#######################################################################################################################
#######################################################################################################################
#######################################################################################################################

function table:getPropLock(Key,PropModel:string,Entity:entity){
  if(changed(Key)&Key){
  This["PropLock",number] = This["PropLock",number] + 1
  }
  if(This["PropLock",number]>=2){ This["PropLock",number] = 0 }
  if(changed(This["PropLock",number])&This["PropLock",number]){
      printColor(vec(255),"[",vec(255,0,0),"E2",vec(255),"]",vec(255,255,0),"Prop protection deactivated "+owner():name()+".")
      This["Locker",entity]:propDelete()
    soundPlay(randint(1,100),soundDuration("buttons/button14.wav"),"buttons/button14.wav")
  }
  if(changed(!This["PropLock",number])&!This["PropLock",number]){
    soundPlay(randint(1,100),soundDuration("buttons/button14.wav"),"buttons/button14.wav")
      printColor(vec(255),"[",vec(255,0,0),"E2",vec(255),"]",vec(255,255,0),"Prop protection activated "+owner():name()+".")
    if(!This["Locker",entity]){ This["Locker",entity] = propSpawn(PropModel,Entity:toWorld(vec(0,0,0)),Entity:angles(),1) 
     This["Locker",entity]:parentTo(Entity)
     This["Locker",entity]:setMass(0)
     This["Locker",entity]:setAlpha(0)
#     This["Locker",entity]:propDraw(0)
}
  }

}
#######################################################################################################################
#######################################################################################################################
#######################################################################################################################

function wirelink:checkSeat(SetPos:vector,SetAngle:angle,Entity:entity,CamEnt:entity){
 if(!Seat){
    SeatFinder = "Seat is not valid"
   }elseif(changed(Seat)&Seat){
    SeatFinder = "Valid"
   }elseif(changed(Seat:pos():distance(SetPos)<=5)&Seat:pos():distance(SetPos)<=5){
    SeatFinder = "Parent"
   }elseif(SeatFinder=="Parent"|Seat:pos():distance(SetPos)<=5){
    SeatFinder = "RunKeys"
 }
 if(Seat){
 Driver = Seat:driver()
   if(Driver&!Lock){
    W=Driver:keyForward()
    A=Driver:keyLeft()
    S=Driver:keyBack()
    D=Driver:keyRight()
    Space=Driver:keyJump()
    Alt=Driver:keyWalk()
    M1=Driver:keyAttack1()
    M2=Driver:keyAttack2()
    R=Driver:keyReload()
    Shift=Driver:keySprint()
   }   
 
}
 switch (SeatFinder){
  case "Seat is not valid",
   findByClass("prop_vehicle_prisoner_pod")
   findSortByDistance(Entity:pos()) 
    if(find():owner()==owner()){ 
      Seat = find()  
      Seat:setMass(50000)
      print("Seat found!")
    }   
   break       

   case "Valid", 
    print("Second step Check!")
     Seat:setPos(SetPos)
     Seat:setAng(SetAngle)
     Seat:propFreeze(1)
      SeatFinder = "Parent"
   break   

   case "Parent",
    Seat:parentTo(Entity)
    print("Seat parented!")
   break

   case "RunKeys",
     if(This:entity():isValid()){
       if(changed(This:entity())&This:entity()){
        This["Parent",entity] = CamEnt
        This["Distance",number] = 100
      }
      This["Activated",number] = Driver ? 1 : 0
     }
   break
 }
}
#######################################################################################################################
#######################################################################################################################
#######################################################################################################################

function number table:locked(Key:number){
#runOnChat(1)
if(changed(Key)&Key){ This["LockT",number] = This["LockT",number] + 1 }
if(This["LockT",number]>=2){  This["LockT",number] = 0 }
    if(changed(This["LockT",number])&This["LockT",number]){
    printColor(vec(255),"[",vec(255,0,0),"E2",vec(255),"]",vec(255,255,0),"Mech has been unlocked "+owner():name()+".")
    soundPlay(randint(1,100),soundDuration("buttons/button3.wav"),"buttons/button3.wav")
    }elseif(changed(!This["LockT",number])&!This["LockT",number]){
    printColor(vec(255),"[",vec(255,0,0),"E2",vec(255),"]",vec(255,255,0),"Mech has been locked "+owner():name()+".")
    soundPlay(randint(1,100),soundDuration("buttons/button3.wav"),"buttons/button3.wav")
    }
if(changed(!Driver)){
This["K.timer",number] = 0     
}

if(!This["LockT",number]){
   
    if((Driver:name()!=owner():name())){
    Lock = 1  
    if(changed(Driver)&Driver){
    printColor(vec(255),"[",vec(255,0,0),"E2",vec(255),"]",vec(255,255,0),Driver:name()+" has entered your Mech messages beeing sended.")
    Seat:printColorDriver(vec(255),"[",vec(255,0,0),"E2",vec(255),"]",vec(255,255,0),Driver:name()+" please get out of the Mech or you'ill get killed in few seconds!")
    Seat:printColorDriver(vec(255),"[",vec(255,0,0),"E2",vec(255),"]",vec(255,255,0),"If you want to drive it ask "+owner():name()+".")
    Seat:hintDriver(Driver:name()+" please get out of the Mech or you'ill get killed in few seconds!",20)
    }
        This["K.timer",number]=min(This["K.timer",number]+0.2,10)
        if(changed(This["K.timer",number]>=10)&This["K.timer",number]>=10){
        Seat:printColorDriver(vec(255),"[",vec(255,0,0),"E2",vec(255),"]",vec(255,255,0),Driver:name()+" i warned you :D rekt!1!!!1")
        Seat:killPod()
        This["K.timer",number] = 0
        This["Checked",number] = 0
        }

    }else{
    Lock = 0 
    This["K.timer",number] = 0      
   }

########################################
 }elseif(This["LockT",number]){
    Lock = 0
    This["Checked",number] = 0
 }
}
#######################################################################################################################
#######################################################################################################################
#######################################################################################################################
function number pitchRot(Aim:vector,NR,M,MaxPitch){
return clamp((-elevation(E:pos(),ang(E:angles():pitch(),E:angles():yaw(), E:angles():roll()), Aim) / NR * M) *!Lock ,-MaxPitch,MaxPitch)
}
#[--------------------------------------]#
function number yawRot(Aim:vector,NR,M){
return (-bearing(E:pos(),ang(E:angles():pitch(),E:angles():yaw(), E:angles():roll()), Aim) / NR * M) *!Lock
} 
#[--------------------------------------]#
function vector entity:setF(V:vector,Vel){
This:applyForce((V)*This:mass()-(This:vel()*This:mass()*Vel))
}
#[--------------------------------------]#
function angle entity:setA(A:angle,Vel){
This:applyAngForce((A)*This:mass()-(This:angVel()*This:mass()*Vel))
}
#[--------------------------------------]#
function ranger ran(NR,Vec:vector){
rangerFilter(E)
rangerFilter(Seat)
return rangerOffset(NR,Vec,WorldDownVec)
}
#[--------------------------------------]#
function ranger aim(){
return rangerOffset(99999,Driver:shootPos(),Seat:toLocalAxis(Driver:eye()))
}
#######################################################################################################################
#######################################################################################################################
}
Cam:checkSeat(E:toWorld(vec(0,0,10)),E:toWorld(ang(0,-90,0)),E,Seat)
E2:locked(owner():keyPressed("0"))

#######################################################################################################################
#######################################################################################################################
#######################################################################################################################
#######################################################################################################################
#######################################################################################################################
#######################################################################################################################

if(first()){
setMode("hover")

function hover(){
  E:propGravity(0)
#[--------------------------------------]#
  local Mass = E:mass()
  local AngVel = (-E:angVel()*0.4)  
  local Vel = (-E:vel()*0.2)
#[--------------------------------------]#
  local Angle = ang(E:angles():pitch()+pitchRot(aim():position(),5,Driver:isValid(),360),E:angles():yaw()+yawRot(aim():position(),5,Driver:isValid()),0)

  local Movement = E:forward()*100*(W-S) + E:right()*100*(D-A) + E:up()*50*(Space-Shift)
  local Vector = Movement  
#[--------------------------------------]#
 
  E:applyForce((Vector + Vel) * Mass)
  E:applyAngForce((E:toLocal(Angle)*5 + AngVel) * Mass)

  if(Driver:keyPressed("2")){
  setMode("fly")
  }
}
#######################################################################################################################
#######################################################################################################################
#######################################################################################################################

function fly(){
  E:propGravity(0)

 if(W|A|S|D){
  E2["Speed",number] = min(E2["Speed",number]+10,1000)
  }elseif(!(W|A|S|D)){
  E2["Speed",number] = max(E2["Speed",number]-100,0)
 }
#[--------------------------------------]#
  local Mass = E:mass()
  local AngVel = (-E:angVel()*0.4)  
  local Vel = (-E:vel()*0.2)
#[--------------------------------------]#
  local Angle = ang(E:angles():pitch()+pitchRot(aim():position(),5,Driver:isValid(),360),E:angles():yaw()+yawRot(aim():position(),5,Driver:isValid()),0)

  local Movement = E:forward()*E2["Speed",number]*(W-S) + E:right()*E2["Speed",number]/2*(D-A)
  local Vector = Movement  
#[--------------------------------------]#
 
  E:applyForce((Vector + Vel) * Mass)
  E:applyAngForce((E:toLocal(Angle)*5 + AngVel) * Mass)

  if(Driver:keyPressed("1")){
  setMode("hover")
  E2["Speed",number] = 0
  }

}

}
#######################################################################################################################
#######################################################################################################################
#######################################################################################################################
#######################################################################################################################
#######################################################################################################################
#######################################################################################################################
if(spawnHolos(MidH)){


MidH 
}
#######################################################################################################################
#######################################################################################################################
#######################################################################################################################



Mode()


