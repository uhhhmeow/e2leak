@name Normalized PropCore Shield
@inputs 
@outputs 
@persist A:array Shields:array Up Alpha
@trigger 
timer("Spawn",100)
runOnTick(1)
Space = 300
Find = 600
if(clk("Spawn")&Up<40)
{
Up=Up+5
Shields[Up,entity] = propSpawn("models/props_phx/construct/glass/glass_angle360.mdl",1)

 


}
if(findCanQuery())
{
findExcludePlayerProps(owner())
findExcludeEntity(owner())
findExcludeEntities(Shields)
findIncludeClass("turret")
findIncludeClass("smg")
findIncludeClass("slam")
findIncludeClass("player")
findIncludeClass("pistol")
findIncludeClass("357")
findIncludeClass("knife")
findIncludeClass("flechette")
findIncludeClass("npc")
findIncludeClass("turret")
findIncludeClass("prop")
findIncludeClass("npc_grenade")
findIncludeClass("expression")
findIncludeClass("magn")
findIncludeClass("ball")
findIncludeClass("rpg")
findIncludeClass("crossbow_bolt")
findInSphere(owner():pos(),Space+Find)
A = findToArray()
}

for(I=1,A:count())
{if(ops()>4500){break}
Ent = A[I,entity]
Shield = Shields[I,entity]
Alpha = -Ent:pos():distance(owner():shootPos())+510

if(Ent:pos():distance(owner():shootPos())>300)
{
Alpha = 0
Shield:propNotSolid(1)    
}
else{
Shield:propNotSolid(0)  
if(Ent:isPlayer())
{
Aim = Ent:shootPos()
}
else{Aim = Ent:pos()}
}
Shoot = owner():shootPos()
Angle = (Aim-Shoot):toAngle()

V = (Shoot-Aim):normalized()*Space  
Shield:setPos(Shoot-V)    
Shield:setAng(Angle+ang(0,90,0))
Shields[I,entity]:setAlpha(Alpha)
}
