@name gunSniperBase
@persist CamAvailable CamP:entity
@trigger none
@model models/maxofs2d/cube_tool.mdl

 #include "functionsSSS"
#----------------------
#-- Setup
if (first()) {
    #include "hologramsCCC"

    ID = "GunSniper_V1"

    Chip = entity()
    ScaleFactor = 1
    ToggleColMat = 1
    ToggleShading = 0
}


#----------------------
#-- Load the hologram and clip data arrays.
elseif (CoreStatus == "InitSpawn") {
    loadContraption()
}


#----------------------
#-- This is like if (first()) { }, code here is run only once.

elseif (CoreStatus == "InitPostSpawn") {
    CoreStatus = "RunThisCode"

   

    Driver = owner()
    Owner = owner()
    Entity = entity()

    init( Entity, 5000 )

    #- Setup custom functions
    #- Tail
    #holoCreate(500, vec(), vec())
    #Tail:newLink(7, 1, 8, vec(-34, 0, 3), Entity)
    #Tail:newLink(8, 7, 9, vec(-36, 0, 0), Entity)
    #Tail:newLink(9, 8, 10, vec(-32, 0, 0), Entity)
    #Tail:newLink(10, 9, 500, vec(-28, 0, 0), Entity)
    #Tail:newLink(500, 10, 0, vec(-28, 0, 0), Entity)

    #- Neck
    NeckA = holoEntity(2)
    NeckB = holoEntity(3)
    NeckC = holoEntity(4)
    Head = holoEntity(5)

    #- Arms
    ArmLA = holoEntity(19)
    ArmLB = holoEntity(20)
    ArmRA = holoEntity(22)
    ArmRB = holoEntity(23)
    
    holoDisableShading(140, 1)
    for (I = 197, 206) { holoDisableShading(I, 1) }

    #- Legs
    LegL = mechCreateLeg(Entity, vec(-10, -35, 0), 8, 0, 5, 3, 7):addIK(Entity, 11, 12, 13, 14)
    LegR = mechCreateLeg(Entity, vec(-10, 35, 0), 8, 50, 5, 3, 7):addIK(Entity, 15, 16, 17, 18)

    #8 5, 15
    #10, 7, 25
    #6, 3, 6

    CamP = holoEntity(5000),holoUnparent(5000)# holoCreate(5000, vec(), vec())
}


#----------------------
#-- This is where executing code goes
elseif (CoreStatus == "RunThisCode") {
   if (clk("RunPhysics")) {
        timer("RunPhysics", 60)

        #- Detect a pilot change
        local Driver = Vehicle:driver()
        if (changed(Driver)) {
            Driver = Driver ?: Owner
            AimType = Driver ? (CamAvailable ? "Cam" : "Normal") : "Normal"
            Cam["Activated", number] = Driver ? 1 : 0

            W = A = S = D = Snipe = Shift = Space = Mouse1 = Mouse2 = 0
        }

        if (changed(Driver)) {
            runOnKeys(PrevDriver, 0)
            runOnKeys(Driver, 1)
            PrevDriver = Driver
        }

        Modes[Mode, string]()

        holoPos(5000, mix(Entity:pos(), CamP:pos(), 0.33))
        #Cam["FOV", number] = 90 + 45*Data["Jump.T", number]


    }

    if (clk("PodCamCheck")) {
        timer("PodCamCheck", 60)

        if (!Vehicle) {
            timer("PodCamCheck", 100)

            findIncludePlayerProps(Owner)
            findByClass("prop_vehicle_prisoner_pod")

            local Temp = find()

            if (Temp) {
                Temp:setPos( holoEntity(3):pos() )
                Temp:setAng( holoEntity(3):toWorld(ang(0, -90, 0)) )
                Temp:propFreeze(1)

                if ((Temp:pos() - holoEntity(3):pos()):length() < 0.5) {
                    Temp:parentTo(holoEntity(3))
                    Vehicle = Temp
                }
            }
        }
        elseif (!Cam) {
            timer("PodCamCheck", 300)

            findIncludePlayerProps(Owner)
            findByClass("gmod_wire_cameracontroller")

            local Temp = find()

            if (Temp) {
                Cam = Temp:wirelink()
                Cam["Parent", entity] = Vehicle #Vehicle
                Cam["Position", vector] = vec(0, -3, 33)
                Cam["Distance", number] = 0

                CamAvailable = 1
            }
        } else {
            stoptimer("PodCamCheck")
        }
    }

    if (keyClk(Driver)) {
        switch (AimType) {
            case "Cam",
                W = Driver:keyForward()
                A = Driver:keyLeft()
                S = Driver:keyBack()
                D = Driver:keyRight()

                Shift = Driver:keySprint()
                Snipe = Driver:keyPressed("H")
                Space = Driver:keyJump()

                Mouse1 = Driver:keyAttack1()
                Mouse2 = Driver:keyAttack2()

                if (Driver:keyPressed("B")) { Vehicle:ejectPod() }
            break

            case "Normal",
                W = Driver:keyAttack2()
                Space = Driver:keyReload()
                Shift = Driver:keySprint()
                Snipe = Driver:keyPressed("H")
                Mouse1 = Driver:keyAttack1()
                Mouse2 = Driver:keyAttack2()
            break
        }
    }

    if (last()) {
        if (!Owner) {
            selfDestruct()
        }
        Vehicle:deparent()
        Projectiles:propDelete()
    }
}
