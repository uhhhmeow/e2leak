@name models/zoid_liger

#local Name = "mech/zoids_liger_zero"
local Name = "mech/zoids_liger_zero_schneider"

local HT = gTable(Name + "_model")
local CT = gTable(Name + "_clip")

HT:clear()
CT:clear()


#- HT = table(index, parentid, scale/scaleunits, pos, ang, scale, model, mat, color)
#- CT = table(index, clipid, pos, dir)


local HN = 0
local CN = 0

local Vec = vec()
local Ang = ang()

#-----------
#-Bones
local ShowJoints = 0

local BoneM = ""
local BoneS = vec(9 / 12) / 1.25
local BoneC = vec4(30,30,30,255 * ShowJoints)

#- Body
HN++, HT[HN, table] = table(1,0,0,Vec,Ang,BoneS,BoneM,"",BoneC)              #- Frontbody
HN++, HT[HN, table] = table(2,0,0,Vec,Ang,BoneS,BoneM,"",BoneC)              #- Rearbody

#- Head
HN++, HT[HN, table] = table(3,1,0,vec(35,0,10),Ang,BoneS,BoneM,"",BoneC)              #- Vertebra
HN++, HT[HN, table] = table(4,3,0,vec(10,0,0),Ang,BoneS,BoneM,"",BoneC)               #- Head
HN++, HT[HN, table] = table(5,4,0,vec(17,0,-7.5),ang(10,0,0),BoneS,BoneM,"",BoneC)    #- Mouth

#- FrontLegs
HN++, HT[HN, table] = table(6,1,0,vec(15.245,-16.935,6.422),Ang,BoneS,BoneM,"",BoneC) #- Shoulder1(right)
HN++, HT[HN, table] = table(7,6,0,vec(0,-9,0),Ang,BoneS,BoneM,"",BoneC)               #- Shoulder2
HN++, HT[HN, table] = table(8,7,0,Vec,ang(25,0,0),BoneS,BoneM,"",BoneC)               #- Shoulder3
HN++, HT[HN, table] = table(9,8,0,vec(0,0,-34),ang(-50,0,0),BoneS,BoneM,"",BoneC)     #- Elbow
HN++, HT[HN, table] = table(10,9,0,vec(0,0,-36),ang(25,0,0),BoneS,BoneM,"",BoneC)     #- Wrist

HN++, HT[HN, table] = table(11,1,0,vec(15.245,16.935,6.422),Ang,BoneS,BoneM,"",BoneC) #- Shoulder1(left)
HN++, HT[HN, table] = table(12,11,0,vec(0,9,0),Ang,BoneS,BoneM,"",BoneC)              #- Shoulder2
HN++, HT[HN, table] = table(13,12,0,Vec,ang(25,0,0),BoneS,BoneM,"",BoneC)             #- Shoulder3
HN++, HT[HN, table] = table(14,13,0,vec(0,0,-34),ang(-50,0,0),BoneS,BoneM,"",BoneC)   #- Elbow
HN++, HT[HN, table] = table(15,14,0,vec(0,0,-36),ang(25,0,0),BoneS,BoneM,"",BoneC)    #- Wrist

#- RearLegs
HN++, HT[HN, table] = table(16,2,0,vec(-34,-16.935,6.422),Ang,BoneS,BoneM,"",BoneC)   #- Hip1(right)
HN++, HT[HN, table] = table(17,16,0,vec(0,-9,0),Ang,BoneS,BoneM,"",BoneC)             #- Hip2
HN++, HT[HN, table] = table(18,17,0,Vec,Ang,BoneS,BoneM,"",BoneC)                     #- Hip3
HN++, HT[HN, table] = table(19,18,0,vec(0,0,-34),ang(90,0,0),BoneS,BoneM,"",BoneC)    #- Knee1
HN++, HT[HN, table] = table(20,19,0,vec(0,0,-20),ang(-110,0,0),BoneS,BoneM,"",BoneC)  #- Knee2
HN++, HT[HN, table] = table(21,20,0,vec(0,0,-32),ang(20,0,0),BoneS,BoneM,"",BoneC)    #- Ankle

HN++, HT[HN, table] = table(22,2,0,vec(-34,16.935,6.422),Ang,BoneS,BoneM,"",BoneC)    #- Hip1(left)
HN++, HT[HN, table] = table(23,22,0,vec(0,9,0),Ang,BoneS,BoneM,"",BoneC)              #- Hip2
HN++, HT[HN, table] = table(24,23,0,Vec,Ang,BoneS,BoneM,"",BoneC)                     #- Hip3
HN++, HT[HN, table] = table(25,24,0,vec(0,0,-34),ang(90,0,0),BoneS,BoneM,"",BoneC)    #- Knee1
HN++, HT[HN, table] = table(26,25,0,vec(0,0,-20),ang(-110,0,0),BoneS,BoneM,"",BoneC)  #- Knee2
HN++, HT[HN, table] = table(27,26,0,vec(0,0,-32),ang(20,0,0),BoneS,BoneM,"",BoneC)    #- Ankle

#- Tail
HN++, HT[HN, table] = table(28,2,0,vec(-45,0,15),ang(5,0,0),BoneS,BoneM,"",BoneC)
HN++, HT[HN, table] = table(29,28,0,vec(-20,0,0),ang(5,0,0),BoneS,BoneM,"",BoneC)
HN++, HT[HN, table] = table(30,29,0,vec(-20,0,0),ang(5,0,0),BoneS,BoneM,"",BoneC)
HN++, HT[HN, table] = table(31,30,0,vec(-20,0,0),ang(5,0,0),BoneS,BoneM,"",BoneC)
HN++, HT[HN, table] = table(32,31,0,vec(-20,0,0),ang(5,0,0),BoneS,BoneM,"",BoneC)
HN++, HT[HN, table] = table(33,32,0,vec(-20,0,0),ang(5,0,0),BoneS,BoneM,"",BoneC)

if (ShowJoints) {
    
local BoneM = ""
local BoneC = vec4(225,225,255,255)

#-Tail
HN++, HT[HN, table] = table(34,28,2,vec(-10,0,0),ang(-90,0,0),vec(4,4,20),BoneM,"",BoneC)
HN++, HT[HN, table] = table(35,29,2,vec(-10,0,0),ang(-90,0,0),vec(4,4,20),BoneM,"",BoneC)
HN++, HT[HN, table] = table(36,30,2,vec(-10,0,0),ang(-90,0,0),vec(4,4,20),BoneM,"",BoneC)
HN++, HT[HN, table] = table(37,31,2,vec(-10,0,0),ang(-90,0,0),vec(4,4,20),BoneM,"",BoneC)
HN++, HT[HN, table] = table(38,32,2,vec(-10,0,0),ang(-90,0,0),vec(4,4,20),BoneM,"",BoneC)
HN++, HT[HN, table] = table(39,33,2,vec(-10,0,0),ang(-90,0,0),vec(4,4,20),BoneM,"",BoneC)

#-Rear Legs
HN++, HT[HN, table] = table(40,16,2,vec(0,-4.5,0),ang(0,0,90),vec(4,4,9),BoneM,"",BoneC)
HN++, HT[HN, table] = table(41,18,2,vec(0,0,-17),ang(180,0,0),vec(4,4,34),BoneM,"",BoneC)
HN++, HT[HN, table] = table(42,19,2,vec(0,0,-10),ang(180,0,0),vec(4,4,20),BoneM,"",BoneC)
HN++, HT[HN, table] = table(43,20,2,vec(0,0,-16),ang(180,0,0),vec(4,4,32),BoneM,"",BoneC)
HN++, HT[HN, table] = table(44,21,2,vec(4,0,0),ang(90,0,0),vec(4,4,8),BoneM,"",BoneC)

HN++, HT[HN, table] = table(45,22,2,vec(0,4.5,0),ang(0,0,-90),vec(4,4,9),BoneM,"",BoneC)
HN++, HT[HN, table] = table(46,24,2,vec(0,0,-17),ang(180,0,0),vec(4,4,34),BoneM,"",BoneC)
HN++, HT[HN, table] = table(47,25,2,vec(0,0,-10),ang(180,0,0),vec(4,4,20),BoneM,"",BoneC)
HN++, HT[HN, table] = table(48,26,2,vec(0,0,-16),ang(180,0,0),vec(4,4,32),BoneM,"",BoneC)
HN++, HT[HN, table] = table(49,27,2,vec(4,0,0),ang(90,0,0),vec(4,4,8),BoneM,"",BoneC)

#-Front Legs
HN++, HT[HN, table] = table(50,6,2,vec(0,-4.5,0),ang(0,0,90),vec(4,4,9),BoneM,"",BoneC)
HN++, HT[HN, table] = table(51,8,2,vec(0,0,-17),ang(180,0,0),vec(4,4,34),BoneM,"",BoneC)
HN++, HT[HN, table] = table(52,9,2,vec(0,0,-18),ang(180,0,0),vec(4,4,36),BoneM,"",BoneC)
HN++, HT[HN, table] = table(53,10,2,vec(4,0,0),ang(90,0,0),vec(4,4,8),BoneM,"",BoneC)

HN++, HT[HN, table] = table(54,11,2,vec(0,4.5,0),ang(0,0,-90),vec(4,4,9),BoneM,"",BoneC)
HN++, HT[HN, table] = table(55,13,2,vec(0,0,-17),ang(180,0,0),vec(4,4,34),BoneM,"",BoneC)
HN++, HT[HN, table] = table(56,14,2,vec(0,0,-18),ang(180,0,0),vec(4,4,36),BoneM,"",BoneC)
HN++, HT[HN, table] = table(57,15,2,vec(4,0,0),ang(90,0,0),vec(4,4,8),BoneM,"",BoneC)

HN++, HT[HN, table] = table(58,1,2,vec(15.86, 0, 4.55),ang(90 - 16,0,0),vec(4,4,35),BoneM,"",BoneC)
HN++, HT[HN, table] = table(59,3,2,vec(5,0,0),ang(90,0,0),vec(4,4,10),BoneM,"",BoneC)
   
} else {
	
local Default = vec(1,1,1)

local BMat1 = "" #"models/props_debris/plasterwall040c"
local BMat2 = "" #"models/props_debris/plasterwall040c"

local BCol1 = vec4(255,255,255,255)
local BCol2 = vec4(45,45,45,255)
local BCol3 = vec4(125,125,125,255)
local BCol4 = vec4(255,0,0,255)
local BCol5 = vec4(255,200,0,255)

#-- Head
HN++, HT[HN, table] = table(34,3,0,Vec,ang(3,0,0),vec(1.25,0.65,2.5),"models/props_c17/tv_monitor01.mdl",BMat2,BCol2)

HN++, HT[HN, table] = table(35,4,0,vec(31.06,0,3),ang(27.1,0,0),Default,"models/sprops/rectangles/size_3/rect_24x24x3.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(36,4,0,vec(30.38,5.89,4.87),ang(0,-12.45,0),Default,"models/sprops/triangles/right/size_1/rtri_12x24.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(37,4,0,vec(30.38,-5.89,4.87),ang(0,12.43,0),Default,"models/sprops/triangles/right/size_1/rtri_12x24.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(38,4,0,vec(30.44,6.18,-2.63),ang(0,-12.45,0),Default,"models/sprops/rectangles/size_1_5/rect_6x24x3.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(39,4,0,vec(30.44,-6.18,-2.63),ang(0,12.43,0),Default,"models/sprops/rectangles/size_1_5/rect_6x24x3.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(40,4,0,vec(30.77,-7.65,0.37),ang(0,12.43,0),Default,"models/sprops/triangles/right/size_0/rtri_3x24.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(41,4,0,vec(30.77,7.65,0.37),ang(0,-12.45,0),Default,"models/sprops/triangles/right/size_0/rtri_3x24.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(42,4,0,vec(39.81,0,-2.63),ang(0,-90,0),vec(1.05,1,1),"models/sprops/rectangles/size_1_5/rect_6x12x3.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(43,4,0,vec(40.1,0,-1.13),ang(-90,-180,0),vec(1.1,2,1.1),"models/sprops/geometry/qhex_12.mdl",BMat2,BCol2)
HN++, HT[HN, table] = table(44,4,0,vec(30,0,-2.9),ang(0,90,90),vec(1.85,1.85,4.5),"models/sprops/geometry/t_hhex_12.mdl",BMat2,BCol2)
HN++, HT[HN, table] = table(45,4,0,vec(-8.17,0,5.66),ang(16.99,0,0),Default,"models/sprops/misc/sq_holes/t_qsqhole_d0_60.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(46,4,0,vec(-8.17,2.25*1.25,5.66),ang(16.99,0,0),Default,"models/sprops/misc/sq_holes/t_qsqhole_d0_60.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(47,4,0,vec(-8.17,-2.25*1.25,5.66),ang(16.99,0,0),Default,"models/sprops/misc/sq_holes/t_qsqhole_d0_60.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(48,4,0,vec(-8.17,4.5*1.25,5.66),ang(16.99,0,0),Default,"models/sprops/misc/sq_holes/t_qsqhole_d0_60.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(49,4,0,vec(-8.17,-4.5*1.25,5.66),ang(16.99,0,0),Default,"models/sprops/misc/sq_holes/t_qsqhole_d0_60.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(50,4,0,vec(9.99,0,4.87),ang(),vec(1,1.66,1),"models/sprops/cuboids/height12/size_1/cube_12x18x12.mdl",BMat2,BCol2)
HN++, HT[HN, table] = table(51,4,0,vec(12.9,0,7.47),ang(28.99,0,0),vec(1,1.25,1),"models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl",BMat2,BCol2)
HN++, HT[HN, table] = table(52,4,0,vec(10,0,12),ang(0,0,0),vec(1,5,0.6),"models/sprops/geometry/qdisc_36.mdl",BMat2,BCol2)
HN++, HT[HN, table] = table(53,4,0,vec(-1.99,0,11.4),ang(90,180,0),vec(1,1.25,1),"models/sprops/cuboids/height06/size_2/cube_12x12x6.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(54,4,0,vec(-10.1,0,10.63),ang(-8,0,0),vec(1,4*1.25,1),"models/sprops/misc/sq_holes/qsqhole_d0_24.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(55,4,0,vec(2.49,-10.95,0.37),ang(90,180,0),Default,"models/sprops/geometry/t_qhex_30.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(56,4,0,vec(2.49,10.95,0.37),ang(90,180,0),Default,"models/sprops/geometry/t_qhex_30.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(57,4,0,vec(14.18,-10.2,1.87),ang(-90,180,0),Default,"models/sprops/geometry/qhex_24.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(58,4,0,vec(14.18,10.2,1.87),ang(-90,-180,0),Default,"models/sprops/geometry/qhex_24.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(59,4,0,vec(14.98*0.92,10.95,-5.63),ang(0,0,-180),vec(0.8,1,1),"models/sprops/triangles_thin/right/size_0/t_rtri_3x12.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(60,4,0,vec(14.98*0.92,-10.95,-5.63),ang(0,0,-180),vec(0.8,1,1),"models/sprops/triangles_thin/right/size_0/t_rtri_3x12.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(61,4,0,vec(16.86,9.29,-7.78),ang(0,0,-180),Default,"models/sprops/geometry/t_qhex_24.mdl",BMat2,BCol2)
HN++, HT[HN, table] = table(62,4,0,vec(16.86,-9.29,-7.78),ang(0,0,-180),Default,"models/sprops/geometry/t_qhex_24.mdl",BMat2,BCol2)
HN++, HT[HN, table] = table(63,4,0,vec(24,8.75,1.95),ang(-97.07,77.552+90,0),vec(0.35,0.5,1.65),"models/sprops/geometry/t_qhex_12.mdl",BMat1,vec4(255,105,0,255))
HN++, HT[HN, table] = table(64,4,0,vec(24,-8.75,1.95),ang(-97.07,102.448+90,0),vec(0.35,0.5,1.65),"models/sprops/geometry/t_qhex_12.mdl",BMat1,vec4(255,105,0,255))
HN++, HT[HN, table] = table(65,4,0,vec(39,3.5,-5.5),ang(90,10,0),vec(1,0.75,0.75),"models/Gibs/Antlion_gib_small_1.mdl",BMat1,BCol5)
HN++, HT[HN, table] = table(66,4,0,vec(39,1,-4.5),ang(90,0,0),vec(0.75,0.5,0.5),"models/Gibs/Antlion_gib_small_1.mdl",BMat1,BCol5)
HN++, HT[HN, table] = table(67,4,0,vec(36,4,-4.5),ang(90,90,0),vec(0.65,0.65,0.65),"models/Gibs/Antlion_gib_small_1.mdl",BMat1,BCol5)
HN++, HT[HN, table] = table(68,4,0,vec(33,5,-4.5),ang(90,90,0),vec(0.6,0.6,0.6),"models/Gibs/Antlion_gib_small_1.mdl",BMat1,BCol5)
HN++, HT[HN, table] = table(69,4,0,vec(30,6,-4.5),ang(90,90,0),vec(0.65,0.65,0.65),"models/Gibs/Antlion_gib_small_1.mdl",BMat1,BCol5)
HN++, HT[HN, table] = table(70,4,0,vec(27,7,-4.5),ang(90,90,0),vec(0.6,0.6,0.6),"models/Gibs/Antlion_gib_small_1.mdl",BMat1,BCol5)
HN++, HT[HN, table] = table(71,4,0,vec(39,-3.5,-5.5),ang(90,-10,0),vec(1,0.75,0.75),"models/Gibs/Antlion_gib_small_1.mdl",BMat1,BCol5)
HN++, HT[HN, table] = table(72,4,0,vec(39,-1,-4.5),ang(90,0,0),vec(0.75,0.5,0.5),"models/Gibs/Antlion_gib_small_1.mdl",BMat1,BCol5)
HN++, HT[HN, table] = table(73,4,0,vec(36,-4,-4.5),ang(90,-90,0),vec(0.65,0.65,0.65),"models/Gibs/Antlion_gib_small_1.mdl",BMat1,BCol5)
HN++, HT[HN, table] = table(74,4,0,vec(33,-5,-4.5),ang(90,-90,0),vec(0.6,0.6,0.6),"models/Gibs/Antlion_gib_small_1.mdl",BMat1,BCol5)
HN++, HT[HN, table] = table(75,4,0,vec(30,-6,-4.5),ang(90,-90,0),vec(0.65,0.65,0.65),"models/Gibs/Antlion_gib_small_1.mdl",BMat1,BCol5)
HN++, HT[HN, table] = table(76,4,0,vec(27,-7,-4.5),ang(90,-90,0),vec(0.6,0.6,0.6),"models/Gibs/Antlion_gib_small_1.mdl",BMat1,BCol5)

HN++, HT[HN, table] = table(77,5,2,vec(0,0,0),ang(0,0,90),vec(5,5,15),"models/holograms/cylinder.mdl",BMat2,BCol2)
HN++, HT[HN, table] = table(78,77,0,vec(9,-2.5,0),ang(90,0,0),vec(1.4,1.25,4),"models/sprops/geometry/hhex_12.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(79,77,0,vec(9,-2.1,0),ang(90,0,0),vec(1.25,1.1,3.75),"models/sprops/geometry/hhex_12.mdl",BMat2,BCol2)
HN++, HT[HN, table] = table(80,77,0,vec(7,0,4.7),ang(-20,90,0),vec(0.5,0.5,0.5),"models/Gibs/Antlion_gib_small_1.mdl",BMat1,BCol5)
HN++, HT[HN, table] = table(81,77,0,vec(11,0,3.9),ang(-20,90,0),vec(0.5,0.5,0.5),"models/Gibs/Antlion_gib_small_1.mdl",BMat1,BCol5)
HN++, HT[HN, table] = table(82,77,0,vec(15,0,3.1),ang(-20,90,0),vec(0.5,0.5,0.5),"models/Gibs/Antlion_gib_small_1.mdl",BMat1,BCol5)
HN++, HT[HN, table] = table(83,77,0,vec(7,0,-4.7),ang(20,90,180),vec(0.5,0.5,0.5),"models/Gibs/Antlion_gib_small_1.mdl",BMat1,BCol5)
HN++, HT[HN, table] = table(84,77,0,vec(11,0,-3.9),ang(20,90,180),vec(0.5,0.5,0.5),"models/Gibs/Antlion_gib_small_1.mdl",BMat1,BCol5)
HN++, HT[HN, table] = table(85,77,0,vec(15,0,-3.1),ang(20,90,180),vec(0.5,0.5,0.5),"models/Gibs/Antlion_gib_small_1.mdl",BMat1,BCol5)

CN++, CT[CN, table] = table(35,1,vec(0, -7.1, 0),vec(-0.205, 1, 0))
CN++, CT[CN, table] = table(35,2,vec(0, 7.1, 0),vec(-0.205, -1, 0))


#-- Body 
HN++, HT[HN, table] = table(86,1,0,vec(15,0,0),ang(0,-90,90),vec(3,1.5,1.1),"models/props_junk/metalgascan.mdl",BMat2,BCol2)
HN++, HT[HN, table] = table(87,1,0,vec(25.03,-17.74,-1.41),ang(80.32,180,180),Default,"models/sprops/triangles/right/size_1/rtri_12x36.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(88,1,0,vec(25.03,17.74,-1.41),ang(80.32,180,180),Default,"models/sprops/triangles/right/size_1/rtri_12x36.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(89,1,0,vec(28.51,0,19.04),ang(0,90,9.66),Default,"models/sprops/rectangles/size_2/rect_12x30x3.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(90,1,0,vec(31.19,16.67,-5.81),ang(61.89,180,-172),Default,"models/sprops/geometry/hhex_36.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(91,1,0,vec(31.19,-16.67,-5.81),ang(61.89,-180,172),Default,"models/sprops/geometry/hhex_36.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(92,1,0,vec(28.23,16.06,17.38),ang(-9.68,180,45),Default,"models/sprops/rectangles/size_1_5/rect_6x12x3.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(93,1,0,vec(28.23,-16.06,17.38),ang(-9.68,180,-45),Default,"models/sprops/rectangles/size_1_5/rect_6x12x3.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(94,1,0,vec(30.61,0,-16.61),ang(0,90,-2.29),Default,"models/sprops/rectangles_thin/size_1_5/rect_6x30x1_5.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(95,1,0,vec(34.37,0,-14.23),ang(0,90,-62.29),Default,"models/sprops/rectangles_thin/size_1_5/rect_6x30x1_5.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(96,1,0,vec(13,0,16),ang(90,0,0),vec(1.25,1.25,2.25),"models/props_lab/monitor01b.mdl",BMat2,BCol2)
HN++, HT[HN, table] = table(97,1,0,vec(49.72,0,-13.48),ang(23.09,0,-180),vec(1.25,4,1.25),"models/sprops/geometry/qdisc_30.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(98,1,2,vec(50,0,-10),ang(-25,0,90),vec(5,5,19),"models/holograms/cylinder.mdl",BMat2,BCol2)
HN++, HT[HN, table] = table(99,98,0,vec(0,10,-8.5),ang(0,0,90),vec(0.75,1,3.6),"models/sprops/geometry/t_hhex_12.mdl",BMat2,BCol2)
HN++, HT[HN, table] = table(100,98,0,vec(0,10,8.5),ang(0,0,90),vec(0.75,1,3.6),"models/sprops/geometry/t_hhex_12.mdl",BMat2,BCol2)
HN++, HT[HN, table] = table(101,98,0,vec(0,10,0),ang(0,0,90),vec(0.5,7,2.5),"models/sprops/geometry/hhex_12.mdl",BMat1,BCol1)

HN++, HT[HN, table] = table(102,1,2,vec(8,0,17),ang(0,0,90),vec(4,4,21),"models/holograms/cylinder.mdl",BMat1,BCol3)
HN++, HT[HN, table] = table(103,102,0,vec(0,5,10),ang(0,0,90),vec(0.65,1,3),"models/sprops/geometry/t_hhex_12.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(104,102,0,vec(0,5,-10),ang(0,0,90),vec(0.65,1,3),"models/sprops/geometry/t_hhex_12.mdl",BMat1,BCol1)

HN++, HT[HN, table] = table(105,102,0,vec(1.21,11.5,13),ang(0,90,90),vec(1,2,1),"models/sprops/geometry/qhex_24.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(106,105,2,vec(-2.25,0,-6),ang(0,0,180),vec(3,3,24),"models/holograms/cylinder.mdl",BMat2,BCol2)
HN++, HT[HN, table] = table(107,105,0,vec(3,0,-8.19-6),ang(90,0,0),vec(3,2,1),"models/sprops/geometry/qdisc_12.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(108,105,0,vec(-5.25,0,-7.79-5),ang(0,90,-180),vec(0.5,1,3),"models/sprops/geometry/t_hhex_12.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(109,105,0,vec(-5.25,0,7.79+2.5),ang(0,90,0),vec(0.5,1,2),"models/sprops/geometry/t_hhex_12.mdl",BMat1,BCol1)

HN++, HT[HN, table] = table(110,102,0,vec(1.21,11.5,-13),ang(0,90,90),vec(1,2,1),"models/sprops/geometry/qhex_24.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(111,110,2,vec(-2.25,0,-6),ang(0,0,180),vec(3,3,24),"models/holograms/cylinder.mdl",BMat2,BCol2)
HN++, HT[HN, table] = table(112,110,0,vec(3,0,-8.19-6),ang(90,0,0),vec(3,2,1),"models/sprops/geometry/qdisc_12.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(113,110,0,vec(-5.25,0,-7.79-5),ang(0,90,-180),vec(0.5,1,3),"models/sprops/geometry/t_hhex_12.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(114,110,0,vec(-5.25,0,7.79+2.5),ang(0,90,0),vec(0.5,1,2),"models/sprops/geometry/t_hhex_12.mdl",BMat1,BCol1)

HN++, HT[HN, table] = table(115,2,0,vec(-20,0,3),ang(0,90,90),vec(2.75,1.25,1.75),"models/props_junk/metalgascan.mdl",BMat2,BCol2)
HN++, HT[HN, table] = table(116,2,0,vec(-40,0,3),ang(0,-90,90),vec(2.7,1.2,1),"models/props_junk/metalgascan.mdl",BMat2,BCol2)
HN++, HT[HN, table] = table(117,2,0,vec(-28,0,14),ang(90,0,0),vec(1,1,3.5),"models/props_lab/monitor01b.mdl",BMat2,BCol2)
HN++, HT[HN, table] = table(118,2,0,vec(-3.34,0,11.76),ang(0,90,0),vec(1.5,1,0.8),"models/sprops/geometry/fhex_18.mdl",BMat1,BCol3)
HN++, HT[HN, table] = table(119,2,0,vec(-7.84,0,11.76),ang(0,90,0),vec(1.5,1,0.8),"models/sprops/geometry/fhex_18.mdl",BMat1,BCol3)
HN++, HT[HN, table] = table(120,2,0,vec(-12.34,0,11.76),ang(0,90,0),vec(1.5,1,0.8),"models/sprops/geometry/fhex_18.mdl",BMat1,BCol3)
HN++, HT[HN, table] = table(121,2,0,vec(-27.88,0,13.98),ang(-90,-90,0),vec(0.5,1,0.8),"models/sprops/geometry/t_fhex_36.mdl",BMat1,BCol3)
HN++, HT[HN, table] = table(122,2,0,vec(-30.38,0,13.98),ang(-90,-90,0),vec(0.5,1,0.8),"models/sprops/geometry/t_fhex_36.mdl",BMat1,BCol3)
HN++, HT[HN, table] = table(123,2,0,vec(-32.88,0,13.98),ang(-90,-90,0),vec(0.5,1,0.8),"models/sprops/geometry/t_fhex_36.mdl",BMat1,BCol3)
HN++, HT[HN, table] = table(124,2,0,vec(-35.38,0,13.98),ang(-90,-90,0),vec(0.5,1,0.8),"models/sprops/geometry/t_fhex_36.mdl",BMat1,BCol3)
HN++, HT[HN, table] = table(125,2,0,vec(-37.88,0,13.98),ang(-90,-90,0),vec(0.5,1,0.8),"models/sprops/geometry/t_fhex_36.mdl",BMat1,BCol3)


#-- Tail
HN++, HT[HN, table] = table(126,28,2,vec(-10,0,0),ang(90,0,0),vec(16,16,37),"models/props_phx2/garbage_metalcan001a.mdl",BMat2,BCol2)
HN++, HT[HN, table] = table(127,29,2,vec(-10,0,0),ang(90,0,0),vec(15,15,37),"models/props_phx2/garbage_metalcan001a.mdl",BMat2,BCol2)
HN++, HT[HN, table] = table(128,30,2,vec(-10,0,0),ang(90,0,0),vec(14,14,37),"models/props_phx2/garbage_metalcan001a.mdl",BMat2,BCol2)
HN++, HT[HN, table] = table(129,31,2,vec(-10,0,0),ang(90,0,0),vec(13,13,37),"models/props_phx2/garbage_metalcan001a.mdl",BMat2,BCol2)
HN++, HT[HN, table] = table(130,32,2,vec(-10,0,0),ang(90,0,0),vec(12,12,37),"models/props_phx2/garbage_metalcan001a.mdl",BMat2,BCol2)
HN++, HT[HN, table] = table(131,33,2,vec(-10,0,0),ang(90,0,0),vec(11,11,37),"models/props_phx2/garbage_metalcan001a.mdl",BMat2,BCol2)
HN++, HT[HN, table] = table(132,33,0,vec(-10,0,0),ang(0,180,0),vec(0.5,0.25,0.3),"models/props_combine/combine_emitter01.mdl",BMat2,BCol2)
HN++, HT[HN, table] = table(133,33,0,vec(-10,0,0),ang(0,180,180),vec(0.5,0.25,0.3),"models/props_combine/combine_emitter01.mdl",BMat2,BCol2)



#-- Front Right Leg
HN++, HT[HN, table] = table(134,6,2,Vec,Ang,vec(18,12,12),"models/holograms/rcube_thick.mdl",BMat2,BCol2)
HN++, HT[HN, table] = table(135,8,2,Vec,ang(90,0,0),vec(14,16,26),"models/holograms/rcube_thick.mdl",BMat2,BCol2)
HN++, HT[HN, table] = table(136,135,0,vec(2.88,-8.19,-2.35),ang(86.16,-174.51,-85.54),Default,"models/sprops/rectangles_thin/size_1/rect_3x12x1_5.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(137,135,0,vec(-7.54,-3.16,-6.67),ang(-83.72,178.31,-177.28),Default,"models/sprops/rectangles_thin/size_2/rect_12x18x1_5.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(138,135,0,vec(-0.7,-8.21,6.74),ang(10.62,0.98,89.69),Default,"models/sprops/rectangles_thin/size_2/rect_12x12x1_5.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(139,135,0,vec(-7.43,-3.08,7.95),ang(-79.38,2.62,-1.63),Default,"models/sprops/rectangles_thin/size_2/rect_12x12x1_5.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(140,135,0,vec(-1.75,-8.32,-10.76),ang(-36.09,1.25,89.62),Default,"models/sprops/rectangles_thin/size_1/rect_3x12x1_5.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(141,135,0,vec(0.53,-2.88,17.85),ang(-0.3,-88.97,-10.62),vec(1,4.5,1),"models/sprops/geometry/qhex_24.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(142,135,0,vec(16,0,0),ang(180,0,0),Default,"models/piledriver/piledriver_75mm.mdl",BMat2,BCol2)
HN++, HT[HN, table] = table(143,135,2,vec(18,0,8),ang(20,0,0),vec(40,12,12),"models/holograms/rcube_thick.mdl",BMat2,BCol1)
HN++, HT[HN, table] = table(144,135,0,vec(18,0,-2),ang(-15,0,90),vec(2,0.5,0.75),"models/props_phx/wheels/magnetic_small.mdl",BMat2,BCol2)
HN++, HT[HN, table] = table(145,135,0,vec(8,0,3.5),ang(0,0,90),vec(1.75,1.75,2.25),"models/props_phx2/garbage_metalcan001a.mdl",BMat1,BCol4)
HN++, HT[HN, table] = table(146,135,0,vec(34,0,0),ang(0,0,90),vec(1.5,1.5,1),"models/props_lab/jar01a.mdl",BMat2,BCol2)
HN++, HT[HN, table] = table(147,135,0,vec(34,0,0),ang(0,0,90),vec(1.75,1.75,3),"models/props_phx2/garbage_metalcan001a.mdl",BMat1,BCol4)
HN++, HT[HN, table] = table(148,9,0,vec(0,0,-14),Ang,vec(0.5,0.5,0.75),"models/props_junk/TrashBin01a.mdl",BMat2,BCol2)
HN++, HT[HN, table] = table(149,9,0,vec(0,0,-14),ang(0,90,0),vec(1.5,2,1.5),"models/sprops/misc/alphanum/alphanum_h.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(150,9,0,vec(-1,0,-28),ang(0,0,90),vec(1.25,1.25,0.85),"models/props_lab/jar01a.mdl",BMat2,BCol2)
HN++, HT[HN, table] = table(151,9,0,vec(-1,0,-28),ang(0,0,90),vec(1.75,1.75,3),"models/props_phx2/garbage_metalcan001a.mdl",BMat1,BCol4)

HN++, HT[HN, table] = table(152,10,0,vec(0,0,1),ang(0,0,90),vec(1.1,1.1,0.9),"models/props_lab/jar01a.mdl",BMat2,BCol2)
HN++, HT[HN, table] = table(153,10,0,vec(4,0,0),ang(0,180,0),vec(1,4.5,1),"models/sprops/geometry/qhex_18.mdl",BMat1,BCol1)

HN++, HT[HN, table] = table(154,10,0,vec(14, 0, 0):rotate(0,26,0),ang(0,15,0),vec(1.5,0.75,0.9),"models/sprops/geometry/qhex_18.mdl",BMat1,BCol5)
HN++, HT[HN, table] = table(155,10,0,vec(14, 0, 0):rotate(0,9,0),ang(0,5,0),vec(1.5,0.75,0.9),"models/sprops/geometry/qhex_18.mdl",BMat1,BCol5)
HN++, HT[HN, table] = table(156,10,0,vec(14, 0, 0):rotate(0,-26,0),ang(0,-15,0),vec(1.5,0.75,0.9),"models/sprops/geometry/qhex_18.mdl",BMat1,BCol5)
HN++, HT[HN, table] = table(157,10,0,vec(14, 0, 0):rotate(0,-9,0),ang(0,-5,0),vec(1.5,0.75,0.9),"models/sprops/geometry/qhex_18.mdl",BMat1,BCol5)



#-- Front Left Leg
HN++, HT[HN, table] = table(158,11,2,Vec,Ang,vec(18,12,12),"models/holograms/rcube_thick.mdl",BMat2,BCol2)
HN++, HT[HN, table] = table(159,13,2,Vec,ang(90,0,0),vec(16,14,26),"models/holograms/rcube_thick.mdl",BMat2,BCol2)
HN++, HT[HN, table] = table(160,159,0,vec(2.88,8.19,-2.35),ang(86.16,174.49,85.52),Default,"models/sprops/rectangles_thin/size_1/rect_3x12x1_5.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(161,159,0,vec(-7.54,3.16,-6.67),ang(-83.72,-178.33,177.26),Default,"models/sprops/rectangles_thin/size_2/rect_12x18x1_5.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(162,159,0,vec(-0.7,8.21,6.74),ang(10.62,-1,-89.71),Default,"models/sprops/rectangles_thin/size_2/rect_12x12x1_5.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(163,159,0,vec(-7.43,3.08,7.95),ang(-79.38,-2.64,1.61),Default,"models/sprops/rectangles_thin/size_2/rect_12x12x1_5.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(164,159,0,vec(-1.75,8.32,-10.76),ang(-36.09,-1.27,-89.64),Default,"models/sprops/rectangles_thin/size_1/rect_3x12x1_5.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(165,159,0,vec(0.53,2.88,17.85),ang(-0.3,88.95,10.6),vec(1,4.5,1),"models/sprops/geometry/qhex_24.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(166,159,0,vec(16,0,0),ang(180,0,0),Default,"models/piledriver/piledriver_75mm.mdl",BMat2,BCol2)
HN++, HT[HN, table] = table(167,159,2,vec(18,0,8),ang(20,0,0),vec(40,12,12),"models/holograms/rcube_thick.mdl",BMat2,BCol1)
HN++, HT[HN, table] = table(168,159,0,vec(18,0,-2),ang(-15,0,90),vec(2,0.5,0.75),"models/props_phx/wheels/magnetic_small.mdl",BMat2,BCol2)
HN++, HT[HN, table] = table(169,159,0,vec(8,0,3.5),ang(0,0,90),vec(1.75,1.75,2.25),"models/props_phx2/garbage_metalcan001a.mdl",BMat1,BCol4)
HN++, HT[HN, table] = table(170,159,0,vec(34,0,0),ang(0,180,90),vec(1.5,1.5,1),"models/props_lab/jar01a.mdl",BMat2,BCol2)
HN++, HT[HN, table] = table(171,159,0,vec(34,0,0),ang(0,0,90),vec(1.75,1.75,3),"models/props_phx2/garbage_metalcan001a.mdl",BMat1,BCol4)

HN++, HT[HN, table] = table(172,14,0,vec(0,0,-14),Ang,vec(0.5,0.5,0.75),"models/props_junk/TrashBin01a.mdl",BMat2,BCol2)
HN++, HT[HN, table] = table(173,14,0,vec(0,0,-14),ang(0,90,0),vec(1.5,2,1.5),"models/sprops/misc/alphanum/alphanum_h.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(174,14,0,vec(-1,0,-28),ang(0,0,90),vec(1.25,1.25,0.85),"models/props_lab/jar01a.mdl",BMat2,BCol2)
HN++, HT[HN, table] = table(175,14,0,vec(-1,0,-28),ang(0,0,90),vec(1.75,1.75,3),"models/props_phx2/garbage_metalcan001a.mdl",BMat1,BCol4)

HN++, HT[HN, table] = table(176,15,0,vec(0,0,1),ang(0,0,90),vec(1.1,1.1,0.9),"models/props_lab/jar01a.mdl",BMat2,BCol2)
HN++, HT[HN, table] = table(177,15,0,vec(4,0,0),ang(0,180,0),vec(1,4.5,1),"models/sprops/geometry/qhex_18.mdl",BMat1,BCol1)

HN++, HT[HN, table] = table(178,15,0,vec(14, 0, 0):rotate(0,26,0),ang(0,15,0),vec(1.5,0.75,0.9),"models/sprops/geometry/qhex_18.mdl",BMat1,BCol5)
HN++, HT[HN, table] = table(179,15,0,vec(14, 0, 0):rotate(0,9,0),ang(0,5,0),vec(1.5,0.75,0.9),"models/sprops/geometry/qhex_18.mdl",BMat1,BCol5)
HN++, HT[HN, table] = table(180,15,0,vec(14, 0, 0):rotate(0,-26,0),ang(0,-15,0),vec(1.5,0.75,0.9),"models/sprops/geometry/qhex_18.mdl",BMat1,BCol5)
HN++, HT[HN, table] = table(181,15,0,vec(14, 0, 0):rotate(0,-9,0),ang(0,-5,0),vec(1.5,0.75,0.9),"models/sprops/geometry/qhex_18.mdl",BMat1,BCol5)


#-- Rear Right Leg
HN++, HT[HN, table] = table(182,16,2,Vec,Ang,vec(18,12,12),"models/holograms/rcube_thick.mdl",BMat2,BCol2)
HN++, HT[HN, table] = table(183,18,2,Vec,ang(90,0,0),vec(14,16,26),"models/holograms/rcube_thick.mdl",BMat2,BCol2)
HN++, HT[HN, table] = table(184,183,0,vec(2.88,-8.19,-2.35),ang(86.16,-174.51,-85.54),Default,"models/sprops/rectangles_thin/size_1/rect_3x12x1_5.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(185,183,0,vec(-7.54,-3.16,-6.67),ang(-83.72,178.31,-177.28),Default,"models/sprops/rectangles_thin/size_2/rect_12x18x1_5.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(186,183,0,vec(-0.7,-8.21,6.74),ang(10.62,0.98,89.69),Default,"models/sprops/rectangles_thin/size_2/rect_12x12x1_5.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(187,183,0,vec(-7.43,-3.08,7.95),ang(-79.38,2.62,-1.63),Default,"models/sprops/rectangles_thin/size_2/rect_12x12x1_5.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(188,183,0,vec(-1.75,-8.32,-10.76),ang(-36.09,1.25,89.62),Default,"models/sprops/rectangles_thin/size_1/rect_3x12x1_5.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(189,183,0,vec(0.53,-2.88,17.85),ang(-0.3,-88.97,-10.62),vec(1,4.5,1),"models/sprops/geometry/qhex_24.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(190,183,0,vec(16,0,0),ang(180,0,0),Default,"models/piledriver/piledriver_75mm.mdl",BMat2,BCol2)
HN++, HT[HN, table] = table(191,183,2,vec(18,0,8),ang(20,0,0),vec(40,12,12),"models/holograms/rcube_thick.mdl",BMat2,BCol1)
HN++, HT[HN, table] = table(192,183,0,vec(18,0,-2),ang(-15,0,90),vec(2,0.5,0.75),"models/props_phx/wheels/magnetic_small.mdl",BMat2,BCol2)
HN++, HT[HN, table] = table(193,183,0,vec(8,0,3.5),ang(0,0,90),vec(1.75,1.75,2.25),"models/props_phx2/garbage_metalcan001a.mdl",BMat1,BCol4)
HN++, HT[HN, table] = table(194,183,0,vec(34,0,0),ang(0,0,90),vec(1.5,1.5,1),"models/props_lab/jar01a.mdl",BMat2,BCol2)
HN++, HT[HN, table] = table(195,183,0,vec(34,0,0),ang(0,0,90),vec(1.75,1.75,3),"models/props_phx2/garbage_metalcan001a.mdl",BMat1,BCol4)

HN++, HT[HN, table] = table(196,19,0,vec(0,0,-10),ang(0,0,0),vec(0.5,0.5,0.5),"models/props_junk/TrashBin01a.mdl",BMat2,BCol2)
HN++, HT[HN, table] = table(197,19,0,vec(0,0,-20),ang(0,0,90),vec(1.5,1.5,1),"models/props_lab/jar01a.mdl",BMat2,BCol2)
HN++, HT[HN, table] = table(198,19,0,vec(0,0,-20),ang(0,0,90),vec(1.75,1.75,3),"models/props_phx2/garbage_metalcan001a.mdl",BMat1,BCol4)

HN++, HT[HN, table] = table(199,20,0,vec(0,0,-10),ang(0,0,0),vec(0.5,0.5,0.6),"models/props_junk/TrashBin01a.mdl",BMat2,BCol2)
HN++, HT[HN, table] = table(200,20,0,vec(0,0,-12),ang(0,90,0),vec(1.5,2,1),"models/sprops/misc/alphanum/alphanum_h.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(201,20,0,vec(-1,0,-24),ang(0,0,90),vec(1.25,1.25,0.6),"models/props_lab/jar01a.mdl",BMat2,BCol2)
HN++, HT[HN, table] = table(202,20,0,vec(-1,0,-24),ang(0,0,90),vec(1.75,1.75,2),"models/props_phx2/garbage_metalcan001a.mdl",BMat1,BCol4)

HN++, HT[HN, table] = table(203,21,0,vec(0,0,1),ang(0,0,90),vec(1.1,1.1,0.9),"models/props_lab/jar01a.mdl",BMat2,BCol2)
HN++, HT[HN, table] = tablee(204,21,0,vec(4,0,0),ang(0,180,0),vec(1,4.5,1),"models/sprops/geometry/qhex_18.mdl",BMat1,BCol1)

HN++, HT[HN, table] = table(205,21,0,vec(14, 0, 0):rotate(0,26,0),ang(0,15,0),vec(1.5,0.75,0.9),"models/sprops/geometry/qhex_18.mdl",BMat1,BCol5)
HN++, HT[HN, table] = table(206,21,0,vec(14, 0, 0):rotate(0,9,0),ang(0,5,0),vec(1.5,0.75,0.9),"models/sprops/geometry/qhex_18.mdl",BMat1,BCol5)
HN++, HT[HN, table] = table(207,21,0,vec(14, 0, 0):rotate(0,-26,0),ang(0,-15,0),vec(1.5,0.75,0.9),"models/sprops/geometry/qhex_18.mdl",BMat1,BCol5)
HN++, HT[HN, table] = table(208,21,0,vec(14, 0, 0):rotate(0,-9,0),ang(0,-5,0),vec(1.5,0.75,0.9),"models/sprops/geometry/qhex_18.mdl",BMat1,BCol5)



#-- Rear Left Leg
HN++, HT[HN, table] = table(209,22,2,Vec,Ang,vec(18,12,12),"models/holograms/rcube_thick.mdl",BMat2,BCol2)
HN++, HT[HN, table] = table(210,24,2,Vec,ang(90,0,0),vec(14,16,26),"models/holograms/rcube_thick.mdl",BMat2,BCol2)
HN++, HT[HN, table] = table(211,210,0,vec(2.88,8.19,-2.35),ang(86.16,174.49,85.52),Default,"models/sprops/rectangles_thin/size_1/rect_3x12x1_5.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(212,210,0,vec(-7.54,3.16,-6.67),ang(-83.72,-178.33,177.26),Default,"models/sprops/rectangles_thin/size_2/rect_12x18x1_5.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(213,210,0,vec(-0.7,8.21,6.74),ang(10.62,-1,-89.71),Default,"models/sprops/rectangles_thin/size_2/rect_12x12x1_5.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(214,210,0,vec(-7.43,3.08,7.95),ang(-79.38,-2.64,1.61),Default,"models/sprops/rectangles_thin/size_2/rect_12x12x1_5.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(215,210,0,vec(-1.75,8.32,-10.76),ang(-36.09,-1.27,-89.64),Default,"models/sprops/rectangles_thin/size_1/rect_3x12x1_5.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(216,210,0,vec(0.53,2.88,17.85),ang(-0.3,88.95,10.6),vec(1,4.5,1),"models/sprops/geometry/qhex_24.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(217,210,0,vec(16,0,0),ang(180,0,0),Default,"models/piledriver/piledriver_75mm.mdl",BMat2,BCol2)
HN++, HT[HN, table] = table(218,210,2,vec(18,0,8),ang(20,0,0),vec(40,12,12),"models/holograms/rcube_thick.mdl",BMat2,BCol1)
HN++, HT[HN, table] = table(219,210,0,vec(18,0,-2),ang(-15,0,90),vec(2,0.5,0.75),"models/props_phx/wheels/magnetic_small.mdl",BMat2,BCol2)
HN++, HT[HN, table] = table(220,210,0,vec(8,0,3.5),ang(0,0,90),vec(1.75,1.75,2.25),"models/props_phx2/garbage_metalcan001a.mdl",BMat1,BCol4)
HN++, HT[HN, table] = table(221,210,0,vec(34,0,0),ang(0,180,90),vec(1.5,1.5,1),"models/props_lab/jar01a.mdl",BMat2,BCol2)
HN++, HT[HN, table] = table(223,210,0,vec(34,0,0),ang(0,0,90),vec(1.75,1.75,3),"models/props_phx2/garbage_metalcan001a.mdl",BMat1,BCol4)

HN++, HT[HN, table] = table(224,25,0,vec(0,0,-10),ang(0,0,0),vec(0.5,0.5,0.5),"models/props_junk/TrashBin01a.mdl",BMat2,BCol2)
HN++, HT[HN, table] = table(225,25,0,vec(0,0,-20),ang(0,0,90),vec(1.5,1.5,1),"models/props_lab/jar01a.mdl",BMat2,BCol2)
HN++, HT[HN, table] = table(226,25,0,vec(0,0,-20),ang(0,0,90),vec(1.75,1.75,3),"models/props_phx2/garbage_metalcan001a.mdl",BMat1,BCol4)

HN++, HT[HN, table] = table(227,26,0,vec(0,0,-10),ang(0,0,0),vec(0.5,0.5,0.6),"models/props_junk/TrashBin01a.mdl",BMat2,BCol2)
HN++, HT[HN, table] = table(228,26,0,vec(0,0,-12),ang(0,90,0),vec(1.5,2,1),"models/sprops/misc/alphanum/alphanum_h.mdl",BMat1,BCol1)
HN++, HT[HN, table] = table(229,26,0,vec(-1,0,-24),ang(0,0,90),vec(1.25,1.25,0.6),"models/props_lab/jar01a.mdl",BMat2,BCol2)
HN++, HT[HN, table] = table(230,26,0,vec(-1,0,-24),ang(0,0,90),vec(1.75,1.75,2),"models/props_phx2/garbage_metalcan001a.mdl",BMat1,BCol4)

HN++, HT[HN, table] = table(231,27,0,vec(0,0,1),ang(0,0,90),vec(1.1,1.1,0.9),"models/props_lab/jar01a.mdl",BMat2,BCol2)
HN++, HT[HN, table] = table(232,27,0,vec(4,0,0),ang(0,180,0),vec(1,4.5,1),"models/sprops/geometry/qhex_18.mdl",BMat1,BCol1)

HN++, HT[HN, table] = table(233,27,0,vec(14, 0, 0):rotate(0,26,0),ang(0,15,0),vec(1.5,0.75,0.9),"models/sprops/geometry/qhex_18.mdl",BMat1,BCol5)
HN++, HT[HN, table] = table(234,27,0,vec(14, 0, 0):rotate(0,9,0),ang(0,5,0),vec(1.5,0.75,0.9),"models/sprops/geometry/qhex_18.mdl",BMat1,BCol5)
HN++, HT[HN, table] = table(235,27,0,vec(14, 0, 0):rotate(0,-26,0),ang(0,-15,0),vec(1.5,0.75,0.9),"models/sprops/geometry/qhex_18.mdl",BMat1,BCol5)
HN++, HT[HN, table] = table(236,27,0,vec(14, 0, 0):rotate(0,-9,0),ang(0,-5,0),vec(1.5,0.75,0.9),"models/sprops/geometry/qhex_18.mdl",BMat1,BCol5)

}


