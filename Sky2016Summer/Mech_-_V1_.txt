@name Mech - V1 
@inputs
@outputs
@persist None:entity S:table
@trigger none
E = entity()
interval(65)

if(first()){
    
    function number c(I,V:vector,S:vector,A:angle,C:vector,M:string,P:entity,Al){
     holoCreate(I,V,S,A,C,M)
     holoParent(I,P)
     holoAlpha(I,Al)   
    }
 c(1,E:toWorld(vec(0,0,0)),vec(255),E:toWorld(ang()),vec(255,0,0),"hqsphere",None,255)  
    
    function vector bez(Start:vector,Middle:vector,End:vector,Speed:number){
     return mix(mix(Start,Middle,Speed),mix(Middle,End,Speed),Speed)   
    }
    S["B",vector] = E:pos()
    function table:setStep(End:vector,StartCycle,EndCycle,CycleSpeed){
    This["Cycle",number] = This["Cycle",number] + CycleSpeed
    if( This["Cycle",number]>= EndCycle){  This["Cycle",number] = 0 }
    local Cyc = This["Cycle",number]
        if(Cyc >= StartCycle & Cyc <= EndCycle&This["StartMove",number]==0){    
        if(changed(This["B2",vector]:distance(End)>10)&This["B2",vector]:distance(End)>10){
        This["StartMove",number] = 1    
        This["B2",vector] =   This["B",vector]
        }  
        }
        if(This["StartMove",number] == 1){
            local Mid = ((This["B2",vector]+End)/2)+vec(0,0,30)
        This["B",vector] = bez(This["B2",vector],Mid,End,0.5)
        }
        if(This["B",vector]:distance(End)<1){
        This["StartMove",number]==0
        }
        
    }
    
}
S:setStep(E:pos(),1,10,1)
holoPos(1,S["B",vector])
