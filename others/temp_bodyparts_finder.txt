@inputs Skull:entity Trunk:entity Pelvis:entity
@inputs Arm1:entity Arm2:entity Leg1:entity Leg2:entity
@outputs Bodyparts:array
Bodyparts=array()
Bodyparts:pushEntity(Skull)
Bodyparts:pushEntity(Trunk)
Bodyparts:pushEntity(Pelvis)
Bodyparts:pushEntity(Arm1)
Bodyparts:pushEntity(Arm2)
Bodyparts:pushEntity(Leg1)
Bodyparts:pushEntity(Leg2)
