@name HitHelper V2
@inputs S:wirelink User:entity E:wirelink Moneypot
@outputs MoneypotUser NoteDoor
@persist First BeginPage GovernmentBegin GovernmentPurchase GovernmentPurchasedScreen CrimeBegin CrimePurchase CrimePurchasedScreen CivilianBegin CivilianPurchase CivilianPurchasedScreen StartupCommands

runOnChat(1)

S:egpClear()
interval(1000)
S:egpDrawTopLeft(1)
KUse=User:keyUse()
CSR=S:egpCursor(User)

E:egpDrawTopLeft(1)

A = owner():lastSaid():explode(" ")
if(A[1,string]=="!Lock" & chatClk(owner())){
LockScreen = 1
}

A = owner():lastSaid():explode(" ")
if(A[1,string]=="!Unlock" & chatClk(owner())){
reset()
}

A = owner():lastSaid():explode(" ")
if(A[1,string]=="!Reset" & chatClk(owner())){
MoneypotUser = 1

reset()

}



Shopname = "Mornin's HitHelper" #Change this

if
(first())



{
    
    First = 1
}


if
(First)
{


    S:egpRoundedBox(1,vec2(20,20),vec2(475,450))
    S:egpColor(1,vec(182,182,182))
    S:egpRoundedBox(2,vec2(15,75),vec2(485,10))
    S:egpColor(2,vec(0,0,0))
    
    S:egpRoundedBox(3,vec2(175,175),vec2(150,75))
    S:egpColor(3,vec(36,36,36))
    S:egpBoxOutline(4,vec2(175,175),vec2(150,75))
    S:egpColor(4,vec(0,0,0))
    S:egpText(51,"Begin",vec2(210,195))
    S:egpSize(51,30)
    
    S:egpText(50,+Shopname,vec2(100,30)) #This value may need to be changed if your text is off
    S:egpSize(50,40) #The second value is size and may need to be changed to fit


if(KUse){
    if(inrange(CSR,S:egpPos(3),S:egpPos(3)+S:egpSize(3))){
        BeginPage = 1
        
        soundPlay(0, 0, "buttons/button14.wav")
        
        
        
    }
}
}

#----------------------------------------After Start Page------------------

if
(BeginPage)
{
    
    First = 0
    
    S:egpClear()
    
    S:egpRoundedBox(1,vec2(20,20),vec2(475,450))
    S:egpColor(1,vec(182,182,182))
    S:egpRoundedBox(2,vec2(15,75),vec2(485,10))
    S:egpColor(2,vec(0,0,0))
    
    S:egpText(50,+Shopname,vec2(100,30)) #This value may need to be changed if your text is off
    S:egpSize(50,40) #The second value is size and may need to be changed to fit


    S:egpBox(3,vec2(18,100),vec2(250,75))
    S:egpColor(3,vec(109,109,109))
    S:egpBox(4,vec2(275,100),vec2(75,75))
    S:egpColor(4,vec(109,109,109))
    S:egpRoundedBox(5,vec2(305,100),vec2(75,75))
    S:egpColor(5,vec(109,109,109))
    S:egpText(51,"Government",vec2(70,120))
    S:egpSize(51,30)
    S:egpText(52,"Buy",vec2(300,120))
    S:egpSize(52,30)
    
    if(KUse){
    if(inrange(CSR,S:egpPos(5),S:egpPos(5)+S:egpSize(5))){
        
        soundPlay(0, 0, "buttons/button14.wav")
        
        GovernmentBegin = 1
        
        
        
    }
}

    if(KUse){
    if(inrange(CSR,S:egpPos(4),S:egpPos(4)+S:egpSize(5))){
        
        soundPlay(0, 0, "buttons/button14.wav")
        
        GovernmentBegin = 1
        
        
        
    }
}

    S:egpBox(6,vec2(18,200),vec2(250,75))
    S:egpColor(6,vec(109,109,109))
    S:egpBox(7,vec2(275,200),vec2(75,75))
    S:egpColor(7,vec(109,109,109))
    S:egpRoundedBox(8,vec2(305,200),vec2(75,75))
    S:egpColor(8,vec(109,109,109))
    S:egpText(53,"Crime Family",vec2(70,220)) 
    S:egpSize(53,30)
    S:egpText(54,"Buy",vec2(300,220)) 
    S:egpSize(54,30)
    
    if(KUse){
    if(inrange(CSR,S:egpPos(7),S:egpPos(7)+S:egpSize(7))){
        
        soundPlay(0, 0, "buttons/button14.wav")
        
        CrimeBegin = 1
        
        
        
    }
}

    if(KUse){
    if(inrange(CSR,S:egpPos(8),S:egpPos(8)+S:egpSize(8))){
        
        soundPlay(0, 0, "buttons/button14.wav")
        
        CrimeBegin = 1
        
        
        
    }
}


    S:egpBox(9,vec2(18,300),vec2(250,75))
    S:egpColor(9,vec(109,109,109))
    S:egpBox(10,vec2(275,300),vec2(75,75))
    S:egpColor(10,vec(109,109,109))
    S:egpRoundedBox(11,vec2(305,300),vec2(75,75))
    S:egpColor(11,vec(109,109,109))
    S:egpText(55,"Civilian Family",vec2(70,320))
    S:egpSize(55,30)
    S:egpText(56,"Buy",vec2(300,320))  
    S:egpSize(56,30)
    
    if(KUse){
    if(inrange(CSR,S:egpPos(11),S:egpPos(11)+S:egpSize(11))){
        
        soundPlay(0, 0, "buttons/button14.wav")
        
        GovernmentBegin = 1
        
        
        
    }
}

    if(KUse){
    if(inrange(CSR,S:egpPos(10),S:egpPos(10)+S:egpSize(10))){
        
        soundPlay(0, 0, "buttons/button14.wav")
        
        CivilianBegin = 1
        
        
        
    }
}




}
    
    


#----------------------------------------Government Page--------------------

if
(GovernmentBegin)
{
    
    BeginPage = 0
    
    First = 0
    
    S:egpClear()
    
    S:egpRoundedBox(1,vec2(20,20),vec2(475,450))
    S:egpColor(1,vec(182,182,182))
    S:egpRoundedBox(2,vec2(15,75),vec2(485,10))
    S:egpColor(2,vec(0,0,0))
    
    S:egpText(50,"Government",vec2(150,30))
    S:egpSize(50,40)
    S:egpColor(50,vec(29,0,255))
    
    S:egpRoundedBox(3,vec2(60,110),vec2(390,175))
    S:egpColor(3,vec(36,36,36))
    
    S:egpText(51,"The Government Is The Class That",vec2(75,130))
    S:egpSize(51,25)
    S:egpText(52,"Enforces or Creates Laws.",vec2(75,155))
    S:egpSize(52,25)
    S:egpText(53,"Ex. Police, Mayor, SWAT",vec2(115,190))
    S:egpSize(53,25)
    S:egpText(54,"Price: $2,000",vec2(175,245))
    S:egpSize(54,25)
    
    S:egpRoundedBox(4,vec2(165,350),vec2(150,75))
    S:egpColor(4,vec(36,36,36))
    S:egpBoxOutline(5,vec2(165,350),vec2(150,75))
    S:egpColor(5,vec(0,0,0))
    S:egpText(55,"Purchase",vec2(180,370))
    S:egpSize(55,30)
    
    S:egpRoundedBox(6,vec2(350,385),vec2(115,75))
    S:egpColor(6,vec(36,36,36))
    S:egpBoxOutline(7,vec2(350,385),vec2(115,75))
    S:egpColor(7,vec(0,0,0))
    S:egpText(56,"Back",vec2(375,405))
    S:egpSize(56,30)
    
        if(KUse){
    if(inrange(CSR,S:egpPos(6),S:egpPos(6)+S:egpSize(6))){
        
        soundPlay(0, 0, "buttons/button3.wav")
        
        BeginPage = 1
        
        GovernmentBegin = 0
        
        
        
    
        }   
} 


        

    
    
    if(KUse){
    if(inrange(CSR,S:egpPos(4),S:egpPos(4)+S:egpSize(4))){
        GovernmentPurchase = 1
}        


if
(Moneypot >= 2000 & GovernmentPurchase)
{
    GovernmentPurchasedScreen = 1
    
    soundPlay(0, 0, "buttons/button4.wav")
    
}


        

    }
}


if
(GovernmentPurchasedScreen == 1)
{
    
    MoneypotUser = 1
    
    NoteDoor = 1
    
    GovernmentBegin = 0
    
    Begin = 0
    
    S:egpClear()
    
    S:egpRoundedBox(1,vec2(20,20),vec2(475,450))
    S:egpColor(1,vec(182,182,182))
    S:egpRoundedBox(2,vec2(15,75),vec2(485,10))
    S:egpColor(2,vec(0,0,0))
    
    S:egpText(50,"Government Purchased",vec2(65,30))
    S:egpSize(50,40)
    S:egpColor(50,vec(29,0,255))
    
    S:egpRoundedBox(3,vec2(60,90),vec2(390,150))
    S:egpColor(3,vec(36,36,36))
    
    S:egpRoundedBox(4,vec2(185,250),vec2(100,75))
    S:egpColor(4,vec(36,36,36))
    S:egpBoxOutline(5,vec2(185,250),vec2(100,75))
    S:egpColor(5,vec(0,0,0))
    S:egpText(55,"Done",vec2(200,270))
    S:egpSize(55,30)
    
    S:egpText(51,"You Have Successfuly Purchased",vec2(75,100))
    S:egpSize(51,25)
    S:egpText(52,"Government. Please Write A Note",vec2(75,130))
    S:egpSize(52,25)
    S:egpText(53,"With Your Victim's Name And Drop",vec2(75,160))
    S:egpSize(53,25)
    S:egpText(54,"It In The Compartment Below.",vec2(75,190))
    S:egpSize(54,25)
    
        if(KUse){
    if(inrange(CSR,S:egpPos(4),S:egpPos(4)+S:egpSize(4))){
        reset()
        
        
        
        
    
}
}
}

#----------------------------------------Crime Page--------------------

if
(CrimeBegin)
{
    BeginPage = 0
    
    First = 0
    
    S:egpClear()
    
    S:egpRoundedBox(1,vec2(20,20),vec2(475,450))
    S:egpColor(1,vec(182,182,182))
    S:egpRoundedBox(2,vec2(15,75),vec2(485,10))
    S:egpColor(2,vec(0,0,0))
    
    S:egpText(50,"Crime Family",vec2(150,30))
    S:egpSize(50,40)
    S:egpColor(50,vec(72,72,72))
    
    S:egpRoundedBox(3,vec2(60,110),vec2(390,175))
    S:egpColor(3,vec(36,36,36))
    
    S:egpText(51,"The Crime Family Is The Group ",vec2(75,130))
   S:egpSize(51,25)
    S:egpText(52,"That Kidnaps, Mugs, or Does",vec2(75,155))
    S:egpSize(52,25)
    S:egpText(53,"Anything Crime Related",vec2(75,180))
    S:egpSize(53,25)
    S:egpText(54,"Ex. Mob Boss, Gangster",vec2(115,210))
    S:egpSize(54,25)
    S:egpText(55,"Price: $1,500",vec2(175,245))
    S:egpSize(55,25)
    
    S:egpRoundedBox(4,vec2(165,350),vec2(150,75))
    S:egpColor(4,vec(36,36,36))
    S:egpBoxOutline(5,vec2(165,350),vec2(150,75))
    S:egpColor(5,vec(0,0,0))
    S:egpText(56,"Purchase",vec2(180,370))
    S:egpSize(56,30)
    
    S:egpRoundedBox(6,vec2(350,385),vec2(115,75))
    S:egpColor(6,vec(36,36,36))
    S:egpBoxOutline(7,vec2(350,385),vec2(115,75))
    S:egpColor(7,vec(0,0,0))
    S:egpText(57,"Back",vec2(375,405))
    S:egpSize(57,30)
    
        if(KUse){
    if(inrange(CSR,S:egpPos(6),S:egpPos(6)+S:egpSize(6))){
        
        soundPlay(0, 0, "buttons/button3.wav")
        
        CrimeBegin = 0
        
        BeginPage = 1
    
        }   
} 
        

    
    
    if(KUse){
    if(inrange(CSR,S:egpPos(4),S:egpPos(4)+S:egpSize(4))){
        CrimePurchase = 1
}        


if
(Moneypot >= 1500 & CrimePurchase)
{
    CrimePurchasedScreen = 1
    
    soundPlay(0, 0, "buttons/button4.wav")
}


        

    }
}


if
(CrimePurchasedScreen == 1)
{
    MoneypotUser = 1
    
    NoteDoor = 1
    
    CrimeBegin = 0
    
    Begin = 0
    
    S:egpClear()
    
    S:egpRoundedBox(1,vec2(20,20),vec2(475,450))
    S:egpColor(1,vec(182,182,182))
    S:egpRoundedBox(2,vec2(15,75),vec2(485,10))
    S:egpColor(2,vec(0,0,0))
    
    S:egpText(50,"Crime Contract Purchased",vec2(40,30))
    S:egpSize(50,40)
    S:egpColor(50,vec(72,72,72))
    
    S:egpRoundedBox(3,vec2(60,90),vec2(390,150))
    S:egpColor(3,vec(36,36,36))
    
    S:egpRoundedBox(4,vec2(185,250),vec2(100,75))
    S:egpColor(4,vec(36,36,36))
    S:egpBoxOutline(5,vec2(185,250),vec2(100,75))
    S:egpColor(5,vec(0,0,0))
    S:egpText(55,"Done",vec2(200,270))
    S:egpSize(55,30)
    
    S:egpText(51,"You Have Successfuly Purchased",vec2(75,100))
    S:egpSize(51,25)
    S:egpText(52,"Government. Please Write A Note",vec2(75,130))
    S:egpSize(52,25)
    S:egpText(53,"With Your Victim's Name And Drop",vec2(75,160))
    S:egpSize(53,25)
    S:egpText(54,"It In The Compartment Below.",vec2(75,190))
    S:egpSize(54,25)
    
        if(KUse){
    if(inrange(CSR,S:egpPos(4),S:egpPos(4)+S:egpSize(4))){
        reset()
        
        
        
        
    
}
}
}

#----------------------------------------Civilian Page--------------------

if
(CivilianBegin)
{
    BeginPage = 0
    
    First = 0
    
    S:egpClear()
    
    S:egpRoundedBox(1,vec2(20,20),vec2(475,450))
    S:egpColor(1,vec(182,182,182))
    S:egpRoundedBox(2,vec2(15,75),vec2(485,10))
    S:egpColor(2,vec(0,0,0))
    
    S:egpText(50,"Civilian",vec2(190,30))
    S:egpSize(50,40)
    S:egpColor(50,vec(4,186,33))
    
    S:egpRoundedBox(3,vec2(60,110),vec2(390,175))
    S:egpColor(3,vec(36,36,36))
    
    S:egpText(51,"The Civilians Work For Neither ",vec2(75,130))
    S:egpSize(51,25)
    S:egpText(52,"The Government, or The Crime",vec2(75,155))
    S:egpSize(52,25)
    S:egpText(53,"Family.",vec2(75,180))
    S:egpSize(53,25)
    S:egpText(54,"Ex. Citizen, Hobo, Cook",vec2(115,210))
    S:egpSize(54,25)
    S:egpText(55,"Price: $850",vec2(175,245))
    S:egpSize(55,25)
    
    S:egpRoundedBox(4,vec2(165,350),vec2(150,75))
    S:egpColor(4,vec(36,36,36))
    S:egpBoxOutline(5,vec2(165,350),vec2(150,75))
    S:egpColor(5,vec(0,0,0))
    S:egpText(56,"Purchase",vec2(180,370))
    S:egpSize(56,30)
    
    S:egpRoundedBox(6,vec2(350,385),vec2(115,75))
    S:egpColor(6,vec(36,36,36))
    S:egpBoxOutline(7,vec2(350,385),vec2(115,75))
    S:egpColor(7,vec(0,0,0))
    S:egpText(57,"Back",vec2(375,405))
    S:egpSize(57,30)
    
        if(KUse){
    if(inrange(CSR,S:egpPos(6),S:egpPos(6)+S:egpSize(6))){
        
        soundPlay(0, 0, "buttons/button3.wav")
        
        CivilianBegin = 0
        
        BeginPage = 1
    
        }   
} 
        

    
    
    if(KUse){
    if(inrange(CSR,S:egpPos(4),S:egpPos(4)+S:egpSize(4))){
        CivilianPurchase = 1
}        


if
(Moneypot >= 850 & CivilianPurchase)
{
    CivilianPurchasedScreen = 1
    
    soundPlay(0, 0, "buttons/button4.wav")

}


        

    }
}


if
(CivilianPurchasedScreen == 1)
{
    MoneypotUser = 1
    
    NoteDoor = 1
    
    CivilianBegin = 0
    
    Begin = 0
    
    S:egpClear()
    
    S:egpRoundedBox(1,vec2(20,20),vec2(475,450))
    S:egpColor(1,vec(182,182,182))
    S:egpRoundedBox(2,vec2(15,75),vec2(485,10))
    S:egpColor(2,vec(0,0,0))
    
    S:egpText(50,"Civilian Contract Purchased",vec2(50,30))
    S:egpSize(50,35)
    S:egpColor(50,vec(4,186,33))
    
    S:egpRoundedBox(3,vec2(60,90),vec2(390,150))
    S:egpColor(3,vec(36,36,36))
    
    S:egpRoundedBox(4,vec2(185,250),vec2(100,75))
    S:egpColor(4,vec(36,36,36))
    S:egpBoxOutline(5,vec2(185,250),vec2(100,75))
    S:egpColor(5,vec(0,0,0))
    S:egpText(55,"Done",vec2(200,270))
    S:egpSize(55,30)
    
    S:egpText(51,"You Have Successfuly Purchased",vec2(75,100))
    S:egpSize(51,25)
    S:egpText(52,"Civilian. Please Write A Note",vec2(75,130))
    S:egpSize(52,25)
    S:egpText(53,"With Your Victim's Name And Drop",vec2(75,160))
    S:egpSize(53,25)
    S:egpText(54,"It In The Compartment Below.",vec2(75,190))
    S:egpSize(54,25)
    
        if(KUse){
    if(inrange(CSR,S:egpPos(4),S:egpPos(4)+S:egpSize(4))){
        reset()
        
        
        
        
    
}
}
}
