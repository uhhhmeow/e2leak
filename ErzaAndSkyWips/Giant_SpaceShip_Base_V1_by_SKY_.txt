@name Giant SpaceShip Base V1 by SKY 
@inputs 
@outputs 
@persist [E P1 P2 P3 P4 P5 P6 P7 P8 P9 P10 P11 P12 P13 P14 P15 P16 P17 P18 P19 P20 P21 P22 P23 P24 P25 P26 P27 P28 P29 P30]:entity Build
@persist [ P31 P32 P33 P34 P35 P36 P37 P38 P39 P40 P41 P42 P43 P44 P45 P46 P47 P48 P49 P50 P51 P52 P53 P54 P55 P56 P57 P58 P59 P60]:entity
@persist 
@persist 
@trigger 

runOnTick(1)
runOnChat(1)
runOnLast(1)
if(last()){propDeleteAll()}
Ent=entity()

if(first()){
 Build=1
}

if(Build==1){
if(!E){E=propSpawn("models/hunter/plates/plate8x8.mdl",Ent:pos()+vec(0,0,150),ang(0,0,0),1) E:setMaterial("phoenix_storms/gear") }

if(!P1){P1=propSpawn("models/hunter/plates/plate8x8.mdl",E:pos()+vec(370,0,0),ang(0,0,0),1) P1:setMaterial("phoenix_storms/gear") P1:parentTo(E)}
if(!P2){P2=propSpawn("models/hunter/plates/plate8x8.mdl",E:pos()+vec(-370,0,0),ang(0,0,0),1) P2:setMaterial("phoenix_storms/gear") P2:parentTo(E)}
if(!P3){P3=propSpawn("models/hunter/plates/plate8x8.mdl",E:pos()+vec(-370,0,0),ang(0,0,0),1) P3:setMaterial("phoenix_storms/gear") P3:parentTo(E)}

if(!P4){P4=propSpawn("models/hunter/plates/plate4x8.mdl",E:pos()+vec(-370,-257,67),ang(45,90,0),1) P4:setMaterial("phoenix_storms/gear"),P4:parentTo(E)}
if(!P5){P5=propSpawn("models/hunter/plates/plate4x8.mdl",E:pos()+vec(-370,257,67),ang(-45,90,0),1) P5:setMaterial("phoenix_storms/gear"),P5:parentTo(E)}
if(!P6){P6=propSpawn("models/hunter/plates/plate4x8.mdl",E:pos()+vec(0,-257,67),ang(45,90,0),1) P6:setMaterial("phoenix_storms/gear"),P6:parentTo(E)}
if(!P7){P7=propSpawn("models/hunter/plates/plate4x8.mdl",E:pos()+vec(0,257,67),ang(-45,90,0),1) P7:setMaterial("phoenix_storms/gear"),P7:parentTo(E)}
if(!P8){P8=propSpawn("models/hunter/plates/plate4x8.mdl",E:pos()+vec(370,-257,67),ang(45,90,0),1) P8:setMaterial("phoenix_storms/gear"),P8:parentTo(E)}
if(!P9){P9=propSpawn("models/hunter/plates/plate4x8.mdl",E:pos()+vec(370,257,67),ang(-45,90,0),1) P9:setMaterial("phoenix_storms/gear"),P9:parentTo(E)}


if(!P10){P10=propSpawn("models/hunter/plates/plate4x8.mdl",E:pos()+vec(-370,-257,135),ang(0,90,0),1) P10:setMaterial("phoenix_storms/gear"),P10:parentTo(E)}
if(!P11){P11=propSpawn("models/hunter/plates/plate4x8.mdl",E:pos()+vec(-370,257,135),ang(0,90,0),1) P11:setMaterial("phoenix_storms/gear"),P11:parentTo(E)}
if(!P12){P12=propSpawn("models/hunter/plates/plate4x8.mdl",E:pos()+vec(0,-257,135),ang(0,90,0),1) P12:setMaterial("phoenix_storms/gear"),P12:parentTo(E)}
if(!P13){P13=propSpawn("models/hunter/plates/plate4x8.mdl",E:pos()+vec(0,257,135),ang(0,90,0),1) P13:setMaterial("phoenix_storms/gear") P13:parentTo(E)}
if(!P14){P14=propSpawn("models/hunter/plates/plate4x8.mdl",E:pos()+vec(370,-257,135),ang(0,90,0),1) P14:setMaterial("phoenix_storms/gear"),P14:parentTo(E) }
if(!P15){P15=propSpawn("models/hunter/plates/plate4x8.mdl",E:pos()+vec(370,257,135),ang(0,90,0),1) P15:setMaterial("phoenix_storms/gear"),P15:parentTo(E)}


if(!P16){P16=propSpawn("models/hunter/plates/plate4x8.mdl",E:pos()+vec(-370,-327,230),ang(90,90,0),1) P16:setMaterial("phoenix_storms/gear"),P16:parentTo(E)}
if(!P17){P17=propSpawn("models/hunter/plates/plate4x8.mdl",E:pos()+vec(-370,327,230),ang(90,90,0),1) P17:setMaterial("phoenix_storms/gear") P17:parentTo(E)}
if(!P18){P18=propSpawn("models/hunter/plates/plate4x8.mdl",E:pos()+vec(0,-327,230),ang(90,90,0),1) P18:setMaterial("phoenix_storms/gear"),P18:parentTo(E) }
if(!P19){P19=propSpawn("models/hunter/plates/plate4x8.mdl",E:pos()+vec(0,327,230),ang(90,90,0),1) P19:setMaterial("phoenix_storms/gear"),P19:parentTo(E)}
if(!P20){P20=propSpawn("models/hunter/plates/plate4x8.mdl",E:pos()+vec(370,-327,230),ang(90,90,0),1) P20:setMaterial("phoenix_storms/gear"),P20:parentTo(E)}
if(!P21){P21=propSpawn("models/hunter/plates/plate4x8.mdl",E:pos()+vec(370,327,230),ang(90,90,0),1) P21:setMaterial("phoenix_storms/gear"),P21:parentTo(E) }

if(!P22){P22=propSpawn("models/hunter/misc/stair1x1.mdl",E:pos()+vec(90,138,0),ang(0,0,0),1) P22:setMaterial("phoenix_storms/gear"),P22:parentTo(E)}
if(!P23){P23=propSpawn("models/hunter/misc/stair1x1.mdl",E:pos()+vec(45,138,45),ang(0,0,0),1) P23:setMaterial("phoenix_storms/gear"),P23:parentTo(E)}
if(!P24){P24=propSpawn("models/hunter/misc/stair1x1.mdl",E:pos()+vec(0,138,90),ang(0,0,0),1) P24:setMaterial("phoenix_storms/gear"),P24:parentTo(E) }

if(!P25){P25=propSpawn("models/hunter/misc/stair1x1.mdl",E:pos()+vec(70,138,23),ang(90,180,0),1) P25:setMaterial("phoenix_storms/gear"),P25:parentTo(E)}
if(!P26){P26=propSpawn("models/hunter/misc/stair1x1.mdl",E:pos()+vec(26,138,78),ang(90,180,0),1) P26:setMaterial("phoenix_storms/gear"),P26:parentTo(E)}
if(!P27){P27=propSpawn("models/hunter/misc/stair1x1.mdl",E:pos()+vec(-10,138,113),ang(90,180,0),1) P27:setMaterial("phoenix_storms/gear"),P27:parentTo(E) }

if(!P28){P28=propSpawn("models/props_phx/construct/metal_plate4x4_tri.mdl",E:pos()+vec(-550,-280,85),ang(-90,180,0),1) P28:setMaterial("phoenix_storms/gear"),P28:parentTo(E)}
if(!P29){P29=propSpawn("models/props_phx/construct/metal_plate4x4_tri.mdl",E:pos()+vec(-550,280,85),ang(-90,0,0),1) P29:setMaterial("phoenix_storms/gear"),P29:parentTo(E) }
if(!P30){P30=propSpawn("models/hunter/plates/plate6x8.mdl",E:pos()+vec(-550,0,140),ang(-90,0,0),1) P30:setMaterial("phoenix_storms/gear"),P30:parentTo(E) }

}

