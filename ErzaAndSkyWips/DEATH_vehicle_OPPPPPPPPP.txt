@name DEATH_vehicle_OPPPPPPPPP
@inputs    Radio:entity
@outputs  Lock Eject
@persist Ang:angle Speed J:array [Prop Driver E Seat Target Layer]:entity Add Counter Max Props:array
@persist Players:array TA:array Holos:array Cli ACount Dir:array DFA DFAA DFAAA DFAAAA XXER XXERA
#Made by Linus [ger] Hllm and sky # 
O = owner()
Active = Seat:driver()
if(Active){
Radio:setPos(Seat:pos()+vec(40,0,10))
Radio:setAng(ang(0,180,0))
}else{
Radio:setAng(ang())
}
E2 = 999
########################################################
SEX = 128418241231
SEX2 = 19392480238511341234525
 XXER=XXER*1+15
 Z45=cos(XXER)
 Y45=sin(XXER)

 XXERA=XXERA*1+-15
 Z46=cos(XXERA)
 Y46=sin(XXERA)



holoCreate(SEX)
holoModel(SEX,"cube")

holoPos(SEX, entity():toWorld(vec(Y45*50,Y45*100,Z45*100)))
holoMaterial(SEX,"effects/ar2_altfire1b")
holoAng(SEX,(Driver:eyeAngles()))
if(first()){
   holoEntity(SEX):setTrails(25,1,1,"trails/laser",vec(255,255,0),255) 
}


holoCreate(SEX2)
holoModel(SEX2,"cube")

holoPos(SEX2, entity():toWorld(vec(Y46*-50,Y46*100,Z46*100)))
holoMaterial(SEX2,"effects/ar2_altfire1b")
holoAng(SEX2,(Driver:eyeAngles()))
if(first()){
   holoEntity(SEX2):setTrails(25,1,1,"trails/laser",vec(255,255,0),255) 
}


if(first()|duped()|dupefinished()) {
    Seat:setAng(ang())
    SEX=18483243274
   CV=123567723
CG=1212121
FGG=32234
TH=914924
runOnTick(1)
runOnChat(1)
Speed = 8   
E = entity()
SEAT=entity():isWeldedTo()
Seat=entity():isWeldedTo()
holoCreate(CV,Seat:pos()+vec(0,60,0),vec(1,1,1),ang(),vec(255,255,255),"models/props_phx/mk-82.mdl")
holoCreate(CG,Seat:pos()+vec(0,-60,0),vec(1,1,1),ang(),vec(255,255,255),"models/props_phx/mk-82.mdl")
holoCreate(FGG,Seat:pos()+vec(0,0,-60),vec(1,1,1),ang(),vec(255,255,255),"models/props_phx/mk-82.mdl")
holoCreate(TH,Seat:pos()+vec(0,0,60),vec(1,1,1),ang(),vec(255,255,255),"models/props_phx/mk-82.mdl")

holoCreate(100000,Seat:pos()+vec(0,0,5),vec(3,3,3),ang(),vec(123,5,255),"hqsphere")
holoParent(100000,Seat)

holoCreate(89)
holoCreate(45)
holoParent(100,89)
holoParent(100,45)

holoAng(89,(ang(45,1,1)))
holoAng(45,(ang(-45,1,1)))
holoModel(45,"hq_torus_thin")
holoModel(89,"hq_torus_thin")
holoScale(89,vec(4,4,4))
holoScale(45,vec(4,4,4))

holoCreate(21,Seat:pos()+vec(0,0,10),vec(9,9,9),ang(),vec(255,255,255),"hq_torus_thin")
holoCreate(22,Seat:pos()+vec(0,0,10),vec(9,9,9),ang(),vec(),"hq_torus_thin")
holoCreate(23,Seat:pos()+vec(0,0,10),vec(9,9,9),ang(),vec(),"hq_torus_thin")

holoCreate(10,Seat:pos()+vec(0,0,10),vec(6,6,6),ang(),vec(255,255,255),"icosphere3")
holoCreate(100,Seat:pos()+vec(0,0,80),vec(2,2,2),ang(),vec(255,0,0),"icosphere3")

holoMaterial(21,"models/vortigaunt/pupil")
holoMaterial(22,"models/vortigaunt/pupil")
holoMaterial(23,"models/vortigaunt/pupil")
Prop=propSpawn("models/props_phx/construct/metal_plate1.mdl",entity():pos()+vec(0,0,50),1)

Prop:propNotSolid(1)
timer("color",100)
Max=100
holoParent(CV,10)
holoParent(CG,10)
holoParent(FGG,10)
holoParent(TH,10)
}

########################################################
Seat:setMaterial("effects/ar2_altfire1b")
if(first()){
Seat:setTrails(50,1,40,"trails/electric",vec(150,50,1),255)
E:setTrails(50,1,50,"trails/laser",vec(0,255,255),255)  
holoEntity(CV):setTrails(50,1,1,"trails/laser",vec(150,100,255),255)
holoEntity(CG):setTrails(50,1,1,"trails/laser",vec(150,0,255),255)
holoEntity(FGG):setTrails(50,1,1,"trails/laser",vec(150,0,255),255)
holoEntity(TH):setTrails(50,1,1,"trails/laser",vec(50,100,255),255)
holoEntity(SEX):setTrails(50,1,1,"trails/laser",vec(50,100,255),255)

Seat:setMaterial("effects/ar2_altfire1b")
E:setMaterial("effects/ar2_altfire1b")
 
}
########################################################
Driver =entity():isWeldedTo():driver()
Ang=(Driver:eyeAngles())
W=Driver:keyForward()
S=Driver:keyBack()
D=Driver:keyRight()
A=Driver:keyLeft()
Space=Driver:keyJump()
Shift=Driver:keySprint()
Mouse1=Driver:keyAttack1()
Mouse2=Driver:keyAttack2()
Alt=Driver:keyPressed("F")  
B=Driver:keyPressed("B")  
R=Driver:keyReload()
L1=Driver:keyPressed("1")
L2=Driver:keyPressed("2")
L3=Driver:keyPressed("3")
L4=Driver:keyPressed("4")
Cli = Driver:keyPressed("5")
L6=Driver:keyPressed("6")
L7=Driver:keyPressed("7")
L8=Driver:keyPressed("8")
L9=Driver:keyPressed("9")
L0=Driver:keyPressed("0")
N=Driver:keyPressed("N")
########################################################
if(Driver) {
    holoAng(CV,Ang)
    
    Seat:propNotSolid(1)  
holoAng(CG,Ang)
holoAng(FGG,Ang)
holoAng(TH,Ang)
holoAng(10,Ang+ang(0,0,90+Counter))
holoAng(10,Ang+ang(0,0,90+Counter*2.5))

holoAng(21,Ang+ang(0,0,-90))
holoAng(22,Ang)
Counter++
holoPos(89,(Driver:aimPos()))
holoPos(45,(Driver:aimPos()))
holoAng(89,Ang+ang(0,0,90+Counter*-2.5))
holoAng(45,Ang+ang(0,0,90+Counter*2.5))

 Seat:setMaterial("effects/ar2_altfire1b")  

holoPos(100,Driver:aimPos())
holoPos(21,Seat:pos()+vec(0,0,10))
holoPos(22,Seat:pos()+vec(0,0,10))
holoPos(23,Seat:pos()+vec(0,0,10))
holoPos(10,Seat:pos()+vec(0,0,10))
holoPos(100000,Seat:pos()+vec(0,0,10000000))
    
    }else{
holoPos(89,vec(1,1,999999999))
holoPos(45,vec(1,1,9999999999))
Seat:propNotSolid(0)  
holoPos(100,Driver:aimPos())
holoPos(21,Seat:pos()+vec(0,0,10000000000))
holoPos(22,Seat:pos()+vec(0,0,1000000000))
holoPos(23,Seat:pos()+vec(0,0,1000000000))
holoPos(10,Seat:pos()+vec(0,0,10000000))
holoPos(100000,Seat:pos()+vec(0,0,10))

owner():setMaterial("default")
}
J = Driver:lastSaid():explode(" ") 
Prop:setAng(Ang)
if(chatClk(Driver)&J:string(1)=="speed") {
Speed=J:string(2):toNumber()
Seat:hintDriver("Speed="+toString(Speed),10)
}

########################################################
if(Shift==1) {Add=3}else{Add=1}
if(W==1){Seat:setPos(Seat:pos()+Prop:forward()*(Speed*Add))}
if(S==1){Seat:setPos(Seat:pos()+Prop:forward()*-(Speed*Add))}
if(A==1){Seat:setPos(Seat:pos()+Prop:right()*-(Speed*Add))}
if(D==1){Seat:setPos(Seat:pos()+Prop:right()*(Speed*Add))}
if(Space==1){Seat:setPos(Seat:pos()+vec(0,0,Speed*Add))}
if(Alt==1){Seat:setPos(Driver:aimPos()+vec(1,1,50))}
########################################################

holoColor(21,vec(random(255),random(255),random(255)))
holoColor(22,vec(random(255),random(255),random(255)))
holoColor(23,vec(random(255),random(255),random(255)))
########################################################
if(Counter==Max) {
holoColor(10,vec(random(255),random(255),random(255)))
holoColor(89,vec(random(255),random(255),random(255)))
holoColor(45,vec(random(255),random(255),random(255)))
Max=Max+500
}
########################################################
findInSphere(holoEntity(100):pos(),400)
findIncludeClass("player")
findExcludeEntity(Driver)
if(changed(Mouse2)&Mouse2){
C=propSpawn("models/hunter/blocks/cube2x2x2.mdl",find():pos()+vec(0,0,65),1)
C:setMaterial("models/alyx/emptool_glow")
}
########################################################

if(changed(Mouse1)&Mouse1){
P = propSpawn("models/props_phx/cannonball_solid.mdl",(Driver:aimPos()),1)   
P:propBreak()
P:soundPlay(1,999,"mm/wtf.mp3")
}
########################################################
if(B){Prop:propDelete()}
########################################################
if(R){
Seat:setPos(Driver:aimPos()+vec(0,0,20))   
Driver:soundPlay(1,999,"st/console/denied01.wav")}
########################################################

if(Driver:name()!=owner():name()) {
#Seat:killPod()
}

########################################################

Eject
########################################################
if(owner():lastSaid()=="lock"){
Seat:lockPod(1)
}
########################################################
if(owner():lastSaid()=="unlock"){
Seat:lockPod(0)
}
########################################################
if(chatClk(owner())&J:string(1)=="eject"){
Seat:ejectPod()
}
########################################################


if(changed(Active)){
    Seat:hintDriver("Made by Linus and sky",5)
    Seat:hintDriver("Type !help in chat for help",15)
    Seat:hintDriver("(x) = not working power !",20)
}

########################################################

if (chatClk(Driver)&J:string(1)=="goto"){
Layer = findPlayerByName(J:string(2)) 
if (Layer:isPlayer()){ 
Player = Layer 
SEAT:setPos(Player:pos() + vec(50,0,0)) 
} 
} 
if(chatClk(Driver)&J:string(1)=="more-power"){


}


    
    ########################################################
if(Driver:lastSaid()=="normal-power"){
     holoPos(1000,(vec(1,1,10000000)))   
    }
########################################################


if(chatClk(Driver)&J:string(1)=="speed-reset"){
    Speed = 8
    
}

########################################################
if(chatClk(Driver)&J:string(1)=="!help"){
     Seat:hintDriver("more-power = more guns!(x)",20)
    Seat:hintDriver("speed-reset = reset speed !",20)
    Seat:hintDriver("B to break you spawned props!",20)
     Seat:hintDriver("goto <name> to teleport!(x)",20)
    Seat:hintDriver("speed <number> = faster / slower",20)
    Seat:hintDriver("normal-power = normal guns :D(x)",20)
    Seat:hintDriver("CLick L to shoot a rocket :D",20)
    Seat:hintDriver("/kill <name> to kill a player :D(x)",20)
    Seat:hintDriver("Number 1-9 and 0 to shoot rockets ",20)
}
########################################################
if(changed(Active)){
    print(Driver,"Made by Linus [ger] and Sky[ger]")
}
########################################################
if(changed(Active)){
hint("someone enter your vehicle say ,eject, to kick him",10)   
}
holoParent(1000000000,10)
holoParent(1011,10)
holoParent(1022,10)
holoParent(1033,10)

holoAng(1000,Ang)
holoAng(1011,Ang)
holoAng(1022,Ang)

holoAng(1033,Ang)

holoAng(1044,Ang)

########################################################
if(changed(L1)&L1) {

 Lol = propSpawn("models/hunter/misc/sphere1x1.mdl",(Driver:pos() + vec(0,0,25)) + Driver:eye() * 1,Driver:eyeAngles(),0)   
}
Lol:setMass(50000)
Lol:propGravity(0)
Lol:setTrails(15,1,50,"trails/smoke",vec(0,255,255),255)
Lol:setMaterial("models/alyx/emptool_glow")
        FG=entity():pos()-entity():pos() + vec(0,0,60) + vec(0,0,60)
        Lol:applyForce(Driver:eye()*99999999+ FG)
        Lol:setAlpha(255)
        Props:pushEntity(Lol)
########################################################

if(changed(L2)&L2){
 L2Q = propSpawn("models/props_phx/mk-82.mdl",(E:pos() + vec(50,0,0)) + Driver:eye() * 5,Driver:eyeAngles(),0)   
}
L2Q:setMass(1000)
L2Q:applyForce(Driver:eye()*100000000)
L2Q:setTrails(50,0,1, "trails/laser", vec(random(255),random(255),random(255)),255)
L2Q:propGravity(0)


if(changed(L2)&L2){
 L22a = propSpawn("models/props_phx/mk-82.mdl",(E:pos() + vec(-50,0,0)) + Driver:eye() * 5,Driver:eyeAngles(),0)   

}
Dir:pushEntity(L2Q)
Dir:pushEntity(L22a)
L22a:propGravity(0)
L22a:setMass(1000)
L22a:applyForce(Driver:eye()*100000000)
L22a:setTrails(50,0,1, "trails/laser", vec(random(255),random(255),random(255)),255)
########################################################
if(changed(L3)&L3){
 L2Qe = propSpawn("models/props_phx/mk-82.mdl",(Driver:pos() + vec(0,0,125)) + Driver:eye() * 25,Driver:eyeAngles(),0)   
}
L2Qe:setMass(1000)
L2Qe:applyForce(Driver:eye()*100000000)
L2Qe:setTrails(50,0,1, "trails/laser", vec(random(255),random(255),random(255)),255)
L2Qe:propGravity(0)


if(changed(L3)&L3){
 L22b = propSpawn("models/props_phx/mk-82.mdl",(Driver:pos() + vec(0,0,-125)) + Driver:eye() * 25,Driver:eyeAngles(),0)   
}
L22b:setMass(1000)
L22b:applyForce(Driver:eye()*100000000)
L22b:setTrails(50,0,1, "trails/laser", vec(random(255),random(255),random(255)),255)
L22b:propGravity(0)


if(changed(L3)&L3){
 L2Q1 = propSpawn("models/props_phx/mk-82.mdl",(Driver:pos() + vec(0,125,0)) + Driver:eye() * 25,Driver:eyeAngles(),0)   
}
L2Q1:setMass(1000)
L2Q1:applyForce(Driver:eye()*100000000)
L2Q1:setTrails(50,0,1, "trails/laser", vec(random(255),random(255),random(255)),255)
L2Q1:propGravity(0)


if(changed(L3)&L3){
 L24 = propSpawn("models/props_phx/mk-82.mdl",(Driver:pos() + vec(0,-125,0)) + Driver:eye() * 25,Driver:eyeAngles(),0)   
}
L24:setMass(1000)
L24:applyForce(Driver:eye()*100000000)
L24:setTrails(50,0,1, "trails/laser", vec(random(255),random(255),random(255)),255)
L24:propGravity(0)
########################################################

if (chatClk(Driver)&J:string(1)=="kill"){
Layer = findPlayerByName(J:string(2)) 
if (Layer:isPlayer()){ 
Player = Layer 
Ql=propSpawn("models/props_c17/oildrum001.mdl",(E:pos()+vec(1,1,1)),1)
Ql:setPos(Player:pos() + vec(50,0,0)) 

} 
} 

########################################################

if(Active){
E:propNotSolid(1)
}else{
E:propNotSolid(0)
}





if(B){
 propDeleteAll()   
    
}
timer("break",1000)
T =5
if(changed(L4)&L4){
Missle = propSpawn("models/props_phx/amraam.mdl",(Driver:pos() + vec(0,0,-150)) + Driver:eye() * 25,Driver:eyeAngles(),0) 
}
if(clk("break")){
 Missle:propBreak()   
}


timer("col",500)




XY = 50000000000000000000000000000000001
holoCreate(XY)
holoPos(XY,Driver:shootPos()+Driver:eye()*5)
holoAng(XY,Driver:eyeAngles()+ang(90,1,1))
holoModel(XY,"hq_torus_thin")
holoScale(XY,vec(0.1,0.1,0.1))
holoParent(XY,Seat)
if(Active&clk("col")){
 holoColor(XY,vec(240,255,0)) 


}


runOnTick(1)
FI = 14392
Find = findInSphere(Seat:pos(),150)
if(Find>0){
Obey = findClosest(Seat:pos())    
    holoCreate(FI)
    holoPos(FI,(Obey:pos()+vec(1,1,100)))
    holoModel(FI,"hqsphere")
    holoParent(FI,Obey)
holoScale(FI,vec(-10,-10,-10))
holoColor(FI,vec(1,1,1))
}else{
holoDelete(FI)
}


Fgg=findInSphere(holoEntity(100):pos(),300)
findIncludeClass("player")
findExcludeEntity(Driver)
if(Fgg>0){
 holoColor(XY,vec(0,255,0))   
    
    
}else{
holoColor(XY,vec(240,255,0)) 

}
if(Active){
 if(!Prop) {Prop=propSpawn("models/props_phx/construct/metal_plate1.mdl",entity():pos()+vec(0,0,50),1)}   
   Prop:setMaterial("effects/ar2_altfire1b") 
}
Seat:propFreeze(1)


Ball = 128471239
if(Active){
holoCreate(Ball)
holoPos(Ball,(Seat:pos()+vec(1,1,10)))
holoAng(Ball,(Driver:eyeAngles()))
holoModel(Ball,"hqsphere")
holoScale(Ball,vec(7.5,7.5,7.5))
holoAlpha(Ball,7000)
holoMaterial(Ball,"models/props_combine/tprings_globe")
}else{

holoPos(Ball,vec(1,1,999999999999))

}

Seat:setMaterial("effects/ar2_altfire1b")
XXY = 9384911199349
if(Seat&Active){
holoCreate(XXY)
holoPos(XXY,(Seat:pos()+vec(1,1,382423842)))
holoModel(XXY,"hqsphere")
holoColor(XXY,vec(0,0,0))
holoAlpha(XXY,255)
holoScale(XXY,vec(1,1,1))
holoColor(XXY,vec(random(255),random(255),random(255)))
}else{

holoPos(XXY,(E:pos()))
}
holoMaterial(23,"models/props_combine/com_shield001a")

if(changed(Cli)&Cli){
 Locky = propSpawn("models/hunter/blocks/cube4x4x4.mdl",(Driver:aimPos()+vec(1,1,50)),1)   
    Locky:propFreeze(1)
    Locky:setMaterial("models/props_combine/com_shield001a")
     Driver:hintDriver("you have spawned a big wall",5)
}

if(Active){
 DFA=DFA*1+1
 Z=cos(DFA)
 Y=sin(DFA)

 DFAA=DFAA*1+2
 Z2=cos(DFAA)
 Y2=sin(DFAA)

 DFAAA=DFAAA*1+1
 Z33=cos(DFAAA)
 Y33=sin(DFAAA)

 
 DFAAAA=DFAAAA*1+2
 Z44=cos(DFAAAA)
 Y44=sin(DFAAAA)

 


YYYYYY=2342342352345435
YYYYY=24234
YYYY=9324832
YYY = 12314854739642375341
if(N){
 Locky:propBreak()  
}
holoCreate(YYY)
holoModel(YYY,"models/hunter/blocks/cube025x025x025.mdl")
holoAng(YYY,Ang+ang(0,0,90+Counter*2.5))
holoPos(YYY, entity():toWorld(vec(0,Z*-100,Y*150)))
holoMaterial(YYY,"models/props_combine/com_shield001a")

holoCreate(YYYY)
holoModel(YYYY,"models/hunter/blocks/cube025x025x025.mdl")
holoAng(YYYY,Ang+ang(0,0,90+Counter*2.5))
holoPos(YYYY, entity():toWorld(vec(0,Z2*100,Y2*100)))
holoMaterial(YYYY,"models/props_combine/com_shield001a")

holoCreate(YYYYY)
holoModel(YYYYY,"models/hunter/blocks/cube025x025x025.mdl")
holoAng(YYYYY,Ang+ang(0,0,90+Counter*2.5))
holoPos(YYYYY, entity():toWorld(vec(0,Z33*100,Y33*-150)))
holoMaterial(YYYYY,"models/props_combine/com_shield001a")

holoCreate(YYYYYY)
holoModel(YYYYYY,"models/hunter/blocks/cube025x025x025.mdl")
holoAng(YYYYYY,Ang+ang(0,0,90+Counter*2.5))
holoPos(YYYYYY, entity():toWorld(vec(0,Z44*-100,Y44*-100)))
holoMaterial(YYYYYY,"models/props_combine/com_shield001a")


}else{
holoPos(YYY,(E:pos()))
holoPos(YYYY,(E:pos()))
holoPos(YYYYY,(E:pos()))
holoPos(YYYYYY,(E:pos()))
}

if(first()){
holoCreate(SEX)
holoModel(SEX,"cube")
holoAng(SEX,Ang+ang(0,0,90+Counter*2.5))
holoPos(SEX, entity():toWorld(vec(0,Z45*-100,Y45*-100)))
holoMaterial(SEX,"effects/ar2_altfire1b")
 holoEntity(SEX):setTrails(50,1,5,"trails/laser",vec(50,100,255),255) 
holoScale(SEX,vec(0,0,0))
    
 holoEntity(SEX):setTrails(50,1,5,"trails/laser",vec(50,100,255),255)   
}

if(changed(L6)&L6){
Wall=propSpawn("models/hunter/blocks/cube2x2x2.mdl",(Driver:aimPos()+vec(1,1,30)),1)    
    Wall:setMaterial("models/props_combine/com_shield001a")
     Driver:hintDriver("you have spawned a small block",5)
}

FKC = 214123122399
if(changed(L7)&L7){
 Door=propSpawn("models/hunter/blocks/cube2x2x2.mdl",(Driver:aimPos()+vec(1,1,40)),1)   
    Door:setMaterial("models/props_combine/tprings_globe")
     Seat:hintDriver("you have spawned a small window",5)
  
}
 if(owner():aimEntity()==Door&owner():keyReload()){
}
#############################
if(changed(L8)&L8){
 PLates = propSpawn("models/hunter/plates/plate24x24.mdl",(Driver:aimPos()),1)   
       Seat:hintDriver("you have spawned a big plate",5)
    PLates:setAng(ang(0,0,0))
    PLates:setColor(vec(1,1,1)) 
}
#############################
if(changed(L9)&L9){
 PLate = propSpawn("models/hunter/plates/plate8x8.mdl",(Driver:aimPos()),1)   
    Seat:hintDriver("you have spawned a medium plate",5)
    PLate:setAng(ang(0,0,0))
    
}
#############################
if(owner():lastSaid() == "inv"){
 
        owner():weapon():setMaterial("effects/ar2_altfire1b")
        owner():setMaterial("effects/ar2_altfire1b")
  }
if(owner():lastSaid() == "deinv"){
 
        owner():weapon():setMaterial("default")
        owner():setMaterial("default")
  }
#############################

if(owner():lastSaid()=="/1"){
owner():plyNoclip(1)
}
if(owner():lastSaid()=="/0"){
owner():plyNoclip(0)
}
#############################
if(owner():lastSaid()=="col"){
    
 PLates:setColor(vec(1,1,1))   
}
