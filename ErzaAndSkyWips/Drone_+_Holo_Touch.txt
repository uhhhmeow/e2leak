@name Drone + Holo Touch
@inputs  
@outputs AP:vector [LS Find]:array [Laser Power Aim] Vel Ang:vector Target:entity Height_Adjust  Turret2:entity
@outputs  Fire
@persist OP:vector Ang:vector O:entity E:entity 
@persist [Laser Power Aim Counter Scan Height_Adjust] [TempPos]:vector [Target]:entity
@trigger
#menupersist 
@persist Mode Temp Count Holo:entity [USE_KEY OPEN_MENU_KEY]:string Menu OO:entity Test:vector Holo_Aim:entity Key Active [TempVec]:vector [Inside Outside Hover]:vector4
@persist  Turret1:entity
runOnLast(1)
E:propMakePersistent(1)
E:propNotSolid(1)
Turret1:setPos(entity():pos())
Turret2:setPos(entity():pos())
Turret1:parentTo(noentity())
Turret2:parentTo(noentity())
Turret1:setPos(entity():pos())
Turret2:setPos(entity():pos())
if(owner():keyAttack2()){
Fire=1
    }
else{
Fire=0
    }
if(!Turret1){
if(owner():aimEntity()&owner():keyAttack2()&owner():aimEntity():type()=="gmod_wire_turret"){#&owner():aimEntity():owner()==owner()){
Turret1=owner():aimEntity()   
Turret1:createWire(entity(),"Fire","Fire")
Turret1:propNotSolid(1)

}
}
elseif(!Turret2){
if(owner():aimEntity()&owner():keyAttack2()&owner():aimEntity():type()=="gmod_wire_turret"&owner():aimEntity():owner()==owner()){
Turret2=owner():aimEntity()   
Turret2:createWire(entity(),"Fire","Fire")
Turret2:propNotSolid(1)

    }
}
if(Turret1){
Turret1:setPos(E:toWorld(+vec(0,10,0)))
Turret1:setAng(E:angles())


}
if(Turret2){
Turret2:setPos(E:toWorld(+vec(0,-10,0)))
Turret2:setAng(E:angles())


}
#E:setAng(ang(0,0,0))
if(first()|duped()){
    

    
    
    function void entity:angforce(Yaw){
       local Ang  = This:angles():setYaw(Yaw) + This:angles():setPitch(0)
       local AngV = This:angVel()
        Inertia = shiftL(ang(This:inertia()))
        This:applyAngForce(-(Ang*1000  + AngV*25)*Inertia)
    }
  

    function void entity:force(Location:vector,Mul) {

        This:applyForce(((Location-This:pos())*Mul-This:vel())*This:mass())

    }
	
function void entity:moveTo(Pos:vector){
        Vec = (Pos - This:pos())*50
        This:applyForce((Vec-This:vel())*This:mass())
    }

function entity:face(Pos:vector,Ang:angle){
    TarQ = quat((Pos - This:pos() ):toAngle()+Ang)
    CurQ = quat(This)
    Q = TarQ/CurQ
    V = This:toLocal(rotationVector(Q)+This:pos())
    #This:applyTorque((V*30 - This:angVelVector())*25*This:inertia())
    This:applyTorque((V*10 - This:angVelVector())*25*This:inertia())
}
    
function void entity:angforce2(Ang:angle,STRENGTH,DAMPING){
    
    INERTIA = shiftL(ang(This:inertia()))
    This:applyAngForce( (Ang*STRENGTH - This:angVel()*DAMPING) * INERTIA )
    
}
function void ikProp(HipHoloId,GoTo:vector,KneeHoloId,Prop:entity){
        Base = entity()
        HipHoloPos = holoEntity(HipHoloId):pos()
        
        Dist = HipHoloPos - Prop:pos()
        Length = Dist:length()
        Ang1 = Dist:toAngle()
        Ang2 = ang(-asin(Length / 50), 0 ,0)
        
        holoAng(HipHoloId,Base:toWorld(Ang1 + Ang2 + ang(0,0,0)))
        holoAng(KneeHoloId,Base:toWorld(Ang1 - Ang2 + ang(0,0,0)))    
    }

    
    
    runOnTick(1)
    runOnChat(1)
    runOnLast(1)
#    #include "/force_functions"
    E=propSpawn("models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl",0)
    E:propNotSolid(1)
    #ifdef E:noCollideAll(1) 
    #endif
    
    #entity():propNotSolid(1)
    OP = entity():pos() + vec(0,0,60)
    O = owner()
    holoCreate(1)
    #holoModel(1,"models/sprops/misc/pyramids/rsize_1/rpyr_12x144.mdl")
    holoMaterial(1,"models/debug/debugwhite")
    holoPos(1,E:pos()+E:forward()*10)
    holoDisableShading(1,1)
    holoAlpha(1,190)
    holoColor(1,vec(255,0,0))
    
    E:setAng(ang(0,-90,90))
    holoCreate(10,E:pos(),vec(0.95),ang(0,0,0),vec(255),"models/sprops/geometry/fring_42.mdl") 
E:setAlpha(0)
    holoAng(10,ang(0,90,90))
    holoParent(10,E)
    holoAlpha(10,0)
    holoCreate(500,E:pos(),vec(1.4,1.1,1.1),ang(0,0,0),vec(75),"hqicosphere") 
    holoParent(500,E)
    
    holoCreate(501,E:pos()+vec(-15,0,3),vec(0.4,0.4,3),ang(90,0,0),vec(75),"hq_rcylinder") 
    holoParent(501,E)
    
    
    
    holoCreate(502,E:pos()+vec(-9,0,3.9),vec(2.5,0.7,0.6),ang(0,0,0),vec(75),"hq_icosphere") 
    holoParent(502,E)
     
    holoCreate(503,E:pos()+vec(-7,0,-8),vec(1.6,1.8,0.2),ang(-40,0,0),vec(70),"hq_tube_thin") 
    holoParent(503,E)
    holoClipEnabled(503,1)
    holoClip(503,vec(1,0,0.9),vec(1,0,0.9),0)
    
    
    holoCreate(504,E:pos()+vec(-17,0,3.9),vec(2.5,0.5,0.4),ang(0,0,0),vec(75),"hq_icosphere") 
    holoParent(504,E)
    
    holoCreate(505,E:pos()+vec(7.9,0,0),vec(0.3,0.3,0.1),ang(90,0,0),vec(20),"hq_tube_thin") 
    holoParent(505,E)
    
    
    holoCreate(506,E:pos()+vec(7.9,0,0),vec(0.3,0.3,0.1),ang(90,0,0),vec(25,0,25),"hq_dome") 
    holoParent(506,E)
    
    holoCreate(507,E:pos()+vec(8,0,0),vec(0.3,0.3,0.1),ang(90,0,0),vec(255),"hq_dome") 
    holoParent(507,E),holoMaterial(507,"phoenix_storms/glass")
    
    
    
    holoCreate(508,E:pos()+vec(-31,0,3),vec(1.3,0.6,0.5),ang(0,0,0),vec(75),"hq_icosphere") 
    holoParent(508,E)
    
    
    holoCreate(509,E:pos()+vec(-9,0,1),vec(0.7,0.5,0.1),ang(0,0,90),vec(95,5,5),"hq_rcylinder_thin") 
    holoParent(509,E)
    
    holoCreate(510,E:pos()+vec(-9,0,1),vec(1.5,0.8,0.8),ang(0,0,90),vec(45),"hq_torus") 
    holoParent(510,E)
    
    
    #holoCreate(511,E:pos()+vec(-20.5,0,2.9),vec(2.5,0.52,0.42),ang(0,0,0),vec(75),"hq_icosphere") ######## holoEntity(504)
    #holoParent(511,E)
    
    holoCreate(512,E:pos()+vec(1.1,3.7,4.5),vec(0.4,0.6,0.4),ang(-40,60,50),vec(40),"models/segment.mdl") 
    holoParent(512,E)
    holoCreate(513,E:pos()+vec(1.1,-3.7,4.5),vec(0.4,0.6,0.4),ang(-40,-60,-50),vec(40),"models/segment.mdl") 
    holoParent(513,E)
    
    
    holoCreate(514,E:pos()+vec(-5,-10,-7),vec(0.10,0.2,1),ang(109,11,0),vec(70),"hq_cylinder") 
    holoParent(514,E)
    holoCreate(515,E:pos()+vec(-5,10,-7),vec(0.10,0.2,1),ang(109,-11,0),vec(70),"hq_cylinder") 
    holoParent(515,E)
   
    
    holoCreate(516,E:pos()+vec(-10,0,6),vec(0.15,1.8,0.07),ang(0,0,0),vec(70),"cube") 
    holoParent(516,E)
    
    holoCreate(517,E:pos()+vec(-10,16,6),vec(1.1,1.1,0.1),ang(0,0,0),vec(70),"hq_tube") 
    holoParent(517,E)
    holoCreate(518,E:pos()+vec(-10,-16,6),vec(1.1,1.1,0.1),ang(0,0,0),vec(70),"hq_tube") 
    holoParent(518,E)
    
    
    
    holoCreate(519,E:pos()+vec(-10,16,6),vec(1.1,1.1,0.1),ang(0,0,180),vec(0,0,255),"models/holograms/hq_hdome_thin.mdl") 
    holoParent(519,E)
    holoCreate(520,E:pos()+vec(-10,-16,6),vec(1.1,1.1,0.1),ang(0,0,180),vec(0,0,255),"models/holograms/hq_hdome_thin.mdl") 
    holoParent(520,E) 
    
    
    rangerPersist(1)
    rangerFilter(entity():getConstraints())
    ##Turret Check##
    findByModel("models/weapons/w_smg_mac10.mdl")
    Turrets = findToArray()
    for(I=1,Turrets:count()){
        if(Turrets[I,entity]:owner() == owner()){
            #NewTurrets:pushEntity(Turrets[I,entity])
        }
    }
    
    findIncludeClass("player")
    findExcludeEntity(O)
   
    Power = 1
    Aim   = 1
    Laser = 1
    Height_Adjust = 120
    
    
}
if(last()){E:propDelete()}

#Chat Commands
if(chatClk(O)){
    LS = O:lastSaid():lower():explode(" ")
    if(O:lastSaid()[1] == "/"){hideChat(1)}else{hideChat(0)}
    
    if(LS[1,string] == "/laser"){
        Laser = LS[2,string]:toNumber()
    }
    if(LS[1,string] == "/power"){
        Power = LS[2,string]:toNumber()
        holoScale(1,vec(0))
    }
    if(LS[1,string] == "/aim"){
        
        if(LS[2,string]:toNumber() != 5){
            Aim = LS[2,string]:toNumber()
        }
        elseif(LS[2,string]:toNumber() == 5){
            if(findCanQuery()){
                TempEnt = findPlayerByName(LS[3,string])
                print(LS[3,string])
                print(TempEnt)
                if(TempEnt:isValid()){  
                    Aim = 5
                    Target = TempEnt 
               }else{print("Invalid Entity - Try again")}
            }else{print("ERROR - Try again")}
        }
        
    }
        
    if(LS[1,string] == "/scan"){
        Laser = 1
        Aim = LS[2,string]:toNumber()
        Scan = 1
        TempPos = E:pos()
        if(LS[3,string]:toNumber()!= 0){
            timer("Scan",LS[3,string]:toNumber()*1000)
            print("Scanning -- " + LS[3,string] + " Seconds.")
        }
        else{
            timer("Scan",6000)
            print("Scanning -- 6 Seconds.")
        }
            
        
    }
}


if(Power)
{
    Counter+=1
    if(clk("Off")){
        Power = 0
    }
    if(clk("Scan")){
        print("-Scan Finished-")
        Scan = 0
    }
    
    if(findCanQuery() & Aim == 2){
        findInSphere(owner():pos(),300)
        Find = findToArray()
    }
    if(Find[1,entity]:isValid() & Aim == 2){
        OP = (Find[1,entity]:toWorld(vec(0,0,200 + Height_Adjust + sin(curtime()*40)*10)) + O:pos())/2 + owner():right()*0 + O:forward()*130
    }else{
        #OP = O:toWorld(vec(0,0,100 + sin(curtime()*40)*10 + Height_Adjust) + O:right()*0 + O:forward()*130)
        OP = O:toWorld(sin(curtime()*10)*10 + Height_Adjust + O:forward()*130)
        #if(O:aimPos():distance(O:pos())<1000){
         #   OP = (O:pos() + O:aimPos())/2 + vec(0,0,0 + sin(curtime()*10)*10 + Height_Adjust)
        #}
        #VecTemp = O:aimPos()
        #OP = O:toWorld(O:aimNormal()*100 + vec(0,0,Height_Adjust))
        
    }
    
    
    
    if(Aim==1){
        #Ang = -ang(E:elevation(O:aimPos()),E:bearing(O:aimPos()),E:angles():roll())
        Ang = O:aimPos()
    }elseif(Aim==2 & Find[1,entity]:isValid()){
        Ang = Find[1,entity]:toWorld(vec(0,0,50))
        #Ang = -ang(E:elevation(Find[1,entity]:toWorld(vec(0,0,40))),E:bearing(Find[1,entity]:pos()),E:angles():roll())
    }elseif(Aim==2 & !Find[1,entity]:isValid()){
        #Ang = -ang(E:elevation(O:aimPos()),E:bearing(O:aimPos()),E:angles():roll())
        Ang = O:aimPos()
    }elseif(Aim==4){
        Ang = entity():pos()
    }elseif(Aim==5){
       Ang = Target:toWorld(vec(0,0,50)-vec(0,0,15))
        OP = Target:toWorld(sin(curtime()*20)*10 + Height_Adjust + Target:forward()*130)
    }
    if(!Scan){
        E:force(OP,2)
        Ranger = rangerOffset(500,E:pos(),(E:forward()*10))
    }
    elseif(Scan){
        Ranger = rangerOffset(500,E:pos(),+vec(0,0,sin(Counter)*3)+(E:forward()*10))
        E:force(TempPos,2)
    }
    E:face(Ang,ang(0,0,0))
    
    
    AP = Ranger:pos()
    if(Laser){
        holoPos(1,(AP + E:pos())/2)
        if(Scan){
            holoScale(1,vec(AP:distance(E:pos())/12,3,0.00001))
        }else{holoScale(1,vec(AP:distance(E:pos())/12,0.1,0.1))}
        
        holoAng(1,(AP - E:pos()):toAngle())
        holoAng(10,holoEntity(1):angles()+ang(0,0,90))
        
        if(!Ranger:entity():isPlayer()){
            if(owner():trusts(Ranger:entity():owner()) | Ranger:entity():owner() == owner()){
                holoColor(1,vec(0,255,0))
            }
            else{
                holoColor(1,vec(255,0,0))
            }
        }
        elseif(owner():trusts(Ranger:entity())){
                    holoColor(1,vec(0,255,0))
                }
                else{
                    holoColor(1,vec(255,0,0))
                }   
    }
    else{
        holoScale(1,vec(0))
    }


}
elseif(!Power){

    holoScale(1,vec(0))

}



#holoAng(10,ang(0,90,90))

        
        
        
        
        
        
    
    
    
###Menu####
if(first()){
    
    runOnTick(1)
    
    Menu = 0
    
    #Colors
    Inside  = vec4(75,75,75,255)
    Outside = vec4(175,175,175,255)
    Hover   = vec4(230,230,230,255)
    
    #Settings
    Mode    = 2 # 1 = The Menu Will Follow You || 2 = The Menu Will Stay Where You Opened It
    Model   = "models/sprops/geometry/fhex_18.mdl" #models/sprops/geometry/fhex_18.mdl - octagon
    Count   = 6 # 1 - 9
    TMat    = "models/debug/debugwhite" #Material
    Radius  = 14.75 #Radius
    Scale_Mul = 0.6 #Scaling Mul
    Outline = 1.05 #outline size
    USE_KEY = "ATTACK" #Use key
    OPEN_MENU_KEY = "LALT" #Use key
    Rotation = 180 #Rotates outerprops around the center
    Center  = 0 #1 = prop in center || 0 = no prop
    CenterS = 1 #The size of the center propb
    
    ####These Will Need Changing Depending On The Model You Use
    Scaling = vec(1.5,0.3,1.5)*Scale_Mul
    Center_Scaling = Scaling*CenterS
    Angle_Offset = ang(0,90,0)
        
    
    
    
    
    #Cursor
    holoCreate(0,vec(0),vec(1),ang(0),vec(0),"models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl")
        holoDisableShading(0,1)
        holoMaterial(900,TMat)
        holoScaleUnits(0,vec(1))
    #
         
        
    #Menu
    HCenter = entity():pos() + vec(0,0,60) #+ owner():forward()*50
    holoCreate(900,HCenter) holoAlpha(900,0) holoScaleUnits(900,vec(1)) # Slave
    holoAng(900,ang(90,0,0)) holoMaterial(900,TMat)
    if(Center){
        holoCreate(200+Count+1,HCenter,Center_Scaling,Angle_Offset,vec(0),Model) holoColor(200+Count+1,Inside)
        holoCreate(101,HCenter,-Center_Scaling*Outline,Angle_Offset,vec(0),Model) holoColor(101,Outside)
        holoMaterial(200+Count+1,TMat) holoMaterial(101,TMat) holoDisableShading(200+Count+1,1) holoDisableShading(101,1) 
        holoParent(200+Count+1,900) holoParent(101,900)
    }
     
    
    #holoScale(0, vec(1,1,0.1))
    #holoAng(0, entity():toWorld(ang(-40,0,0)))   
    for(I=201,200+Count){
        Temp = Temp + 1
        #print(I)
        local TempVec = Radius * vec(cos(I*(360/Count)),sin(I*(360/Count)),0):rotate(holoEntity(900):angles()+ang(Rotation,-90,-90)) + holoEntity(900):pos()
        holoCreate(I)
        holoColor(I,Inside)
        holoPos(I,TempVec)
        holoAng(I,Angle_Offset)
        holoModel(I,Model)
        holoScale(I,Scaling)
        holoMaterial(I,TMat)
        holoDisableShading(I,1)
        holoParent(I,900)
        
        holoCreate(I+50) 
        holoMaterial(I+50,TMat)
        holoPos(I+50,holoEntity(I):pos()) 
        holoAng(I+50,holoEntity(I):angles())
        holoModel(I+50,holoEntity(I):model())
        holoScale(I+50,-Scaling*Outline)
        holoColor(I+50,Outside)
        holoParent(I+50,I)
        holoDisableShading(I+50,1)
        
        String = "models/sprops/misc/alphanum/alphanum_"+(Temp):toString()+".mdl"
        #print(Temp)0
        #print(String)
        #holoCreate(I+100,holoEntity(I):pos(),vec(1),holoEntity(I):angles(),vec(255),"models/sprops/misc/alphanum/alphanum_"+Temp:toString()+".mdl") 
        #holoParent(I+100,I)
        
    }
    
    #Make Your Icons Base Here With Index 301 - 310 And Use holoEntity(201-210) For Positioning And Parent Them To holoEntity(I,900)
    #Any Aditional Icon Detail Use Index 400+
    
    #Example#
    #holoCreate(301,holoEntity(201):pos(),vec(0.5),holoEntity(900):angles(),vec(255),"models/sprops/cuboids/height12/size_1/cube_12x12x12.mdl") 
        #holoParent(301,900)
        
    #Power Button 
        #holoCreate(260,holoEntity(208):pos() + FO,vec(1),HAng,vec(255),"models/cheeze/buttons2/pwr_green.mdl")
        holoCreate(301,holoEntity(201):pos(),vec(0.4),ang(90,0,90),vec(0),"models/sprops/geometry/fring_18.mdl")
            holoColor(301,Outside)
        holoCreate(401,holoEntity(201):pos()+vec(0,0,3),vec(0.35),ang(90,0,0),vec(0),"models/sprops/rectangles/size_1/rect_3x12x3.mdl")
            holoColor(401,Outside)
            holoClipEnabled(301,1,1)
            holoClip(301,1,vec(-2.2,0,0),vec(1,0,0),0)
            holoDisableShading(301,1) holoDisableShading(401,1)
            holoParent(301,900) holoParent(401,900)
            holoMaterial(301,TMat) holoMaterial(401,TMat)
            
    #Laser Button 
        holoCreate(302,holoEntity(202):pos()-vec(0,2,0),vec(0.4),ang(-90,0,0),vec(0),"models/sprops/cuboids/height06/size_1/cube_6x6x6.mdl")
            holoColor(302,Outside)
        holoCreate(402,holoEntity(202):pos()+vec(0,2,0),vec(0.45,0.4,0.1),ang(0,90,0),vec(255,0,0),"models/sprops/rectangles/size_1/rect_3x12x3.mdl")
            holoDisableShading(302,1) holoMaterial(302,TMat)
            holoDisableShading(402,1) holoMaterial(402,TMat)
            holoParent(302,900) holoParent(402,900)
            
     #Scan Button
        holoCreate(303,holoEntity(203):pos()-vec(0,2,0),vec(0.4),ang(0,0,0),vec(0),"models/sprops/cuboids/height06/size_1/cube_6x6x6.mdl")
            holoColor(303,Outside)
        holoCreate(403,holoEntity(203):pos()+vec(0,2,0),vec(0.45,0.4,0.6),ang(0,90,0),vec(255,0,0),"models/sprops/rectangles/size_1/rect_3x12x3.mdl")
            holoDisableShading(303,1) holoMaterial(303,TMat)
            holoDisableShading(403,1) holoMaterial(403,TMat)
            holoParent(303,900)
            holoParent(403,900)
    #Plus Button
        holoCreate(305,holoEntity(205):pos()-vec(0,0,0),vec(0.5),ang(0,90,0),vec(0),"models/sprops/misc/alphanum/alphanum_plu.mdl")
            holoColor(305,Outside)
            holoDisableShading(305,1) holoMaterial(305,TMat) holoParent(305,900)
    
    #Negate Button
        holoCreate(306,holoEntity(206):pos()-vec(0,0,0),vec(0.65),ang(0,90,0),vec(0),"models/sprops/misc/alphanum/alphanum_min.mdl")
            holoColor(306,Outside)
            holoDisableShading(306,1) holoMaterial(306,TMat) holoParent(306,900)
    #
    
###############################################################################################

function void button(Button){
    
    switch(Button){
        
        #Button 1 is pressed
        case 1,
            Power = !Power
            holoScale(1,vec(0)) 
        break
        
        #Button 2 is pressed
        case 2,
            Laser = !Laser
        break
        
        #Button 3 is pressed
        case 3,
        Laser = 1
        Aim = 1
        Scan = 1
        TempPos = E:pos()
        timer("Scan",6000)
        print("Scanning -- 6 Seconds.")
        break
        
        #Button 4 is pressed
        case 4,
            print(4)
        break
        
        #Button 5 is pressed
        case 5,
            Height_Adjust+=15
            print(Height_Adjust)
        break
        
        #Button 6 is pressed
        case 6,
            Height_Adjust-=15
            print(Height_Adjust)
        break
        
        #Button 7 is pressed
        case 7,
            print(7)
        break
        
        #Button 8 is pressed
        case 8,
            print(8)
        break
        
        #Button 9 is pressed
        case 9,
            print(9)
        break
        
        #Button 10 is pressed
        case 10,
            print(10)
        break
        
    }
           
}    
    
    

    
    rangerPersist(1)
    rangerFilter(owner())
    Holo = holoEntity(900) 
}
#interval(100)



Key=owner():keyPressed(OPEN_MENU_KEY)
if(Key & $Key){
        Active = !Active
        if(!Active){
            holoPos(900,vec(0))
            Counter = 0
        }
        TempVec = owner():pos() + vec(0,0,40) + owner():forward()*80
        TempAng = holoEntity(900):angles()
}

if(changed(Active) & Active == 1){
AimR=rangerOffset(100, owner():shootPos(), owner():eye())
holoPos(900, AimR:position())
}

if(Active){
      
    if(Mode==1){
    
    holoPos(900,owner():pos() + vec(0,0,40) + owner():forward()*80)
    holoAng(900,ang(40,0,0) + (owner():pos() - holoEntity(900):pos()):toAngle())
    holoAng(0,holoEntity(900):angles())
    
    }elseif(Mode==2){
        holoPos(900,TempVec)
        holoAng(900,ang(40,0,0) + (owner():pos() - holoEntity(900):pos()):toAngle())
        holoAng(0,holoEntity(900):angles())
        
    }
        
    
    ShootPos = owner():shootPos()
    EyeDir = owner():eye()

    #How does line-plane intersection work?
    PlanePoint = Holo:pos() #Get a point in the plane
    Normal = Holo:up() #Get the normal (a vector perpendicular to the surface) of the plane
    LinePoint1 = ShootPos #Get a point on the line
    LinePoint2 = ShootPos+EyeDir #Get a point on the line "after" point 1#
    X = (Normal:dot(PlanePoint-LinePoint1))/(Normal:dot(LinePoint2-LinePoint1)) #Not really sure how, but it returns how many times the distance from point 1 to point 2 you need to go from point 1 to reach the intersection
    Vec = LinePoint1+X*(LinePoint2-LinePoint1) #Get the intersections position using f(X) = LinePoint1+X*(LinePoint2-LinePoint1)
    #
    
    holoPos(0,Vec)
    
    for(I=201,200+Count+1){
        if(Vec:distance(holoEntity(I):pos())<8){
            holoColor(I,Hover)
            if(changed(owner():keyAttack1())&owner():keyAttack1()){
                button(I-200)
            }
        }else{
            holoColor(I,Inside)
        }
    
}
    
}else{holoPos(0,vec(0))}




#[ ####OLD####
OFB = holoEntity(208):right()*0.4
        OLR = holoEntity(208):up()*0.4
        OUD = holoEntity(208):forward()*0.4
        
        #Power Button - 260
        #holoCreate(260,holoEntity(208):pos() + FO,vec(1),HAng,vec(255),"models/cheeze/buttons2/pwr_green.mdl")
        holoCreate(261,holoEntity(208):pos() - OFB,vec(0.4),HAng + ang(0,0,90),vec(0),"models/sprops/geometry/fring_18.mdl")
        holoCreate(262,holoEntity(208):pos() - OFB + OUD*5,vec(0.4),HAng + ang(0,0,0),vec(0),"models/sprops/rectangles/size_1/rect_3x12x3.mdl")
            holoClipEnabled(261,1,1)
            holoClip(261,1,vec(2.2,0,0),vec(-1,0,0),0)
            holoDisableShading(260,1)
            holoParent(261,208) holoParent(262,208)
            
        #Laser Button  - 265
        holoCreate(265,holoEntity(207):pos() - OFB*1.5,vec(0.4),HAng+ang(-90,0,0),vec(0),"models/sprops/cuboids/height06/size_1/cube_6x6x6.mdl")
        holoCreate(266,holoEntity(207):pos() - OFB*1.5 + OLR*5.5,vec(0.45,0.1,0.1),HAng+ang(70,90,30),vec(255,0,0),"models/sprops/rectangles/size_1/rect_3x12x3.mdl")
            holoDisableShading(265,1) holoMaterial(265,TMat)
            holoDisableShading(266,1) holoMaterial(266,TMat)
            holoParent(265,208) holoParent(266,208)
            
        #Scan Button
        holoCreate(270,holoEntity(206):pos() - OFB*1.5,vec(0.4),HAng+ang(-90,0,0),vec(0),"models/sprops/cuboids/height06/size_1/cube_6x6x6.mdl")
        holoCreate(271,holoEntity(206):pos() - OFB*1.5 + OLR*5.5,vec(0.45,0.1,0.5),HAng+ang(70,90,30),vec(255,0,0),"models/sprops/rectangles/size_1/rect_3x12x3.mdl")
            holoDisableShading(270,1) holoMaterial(270,TMat)
            holoDisableShading(271,1) holoMaterial(271,TMat)]#
