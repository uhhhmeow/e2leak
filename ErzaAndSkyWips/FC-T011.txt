@name FC-T011
@inputs [Radio Th1 Th2 Th3 Th4 AP_HE3 AP_HE4]:wirelink Run SNST_RA_F SNST_RA [Base]:entity [WPA Options]:array TargetZ YawOffsetF_L YawOffsetF_R YawOffsetR_R YawOffsetR_L FDist Dist Dist_F_L Dist_F_R Dist_R_L Dist_R_R Strength Lock_XY Lock_Vec:vector [CCAMAP_0 CCAMAP_1 CCAMAP_2]:vector
@outputs ZThrust_Left ZThrust_Right ZThrust_Forward ZThrust_Rear Z_Left Z_Right Z_Forward Z_Rear XS YS S_Mul XN YN N_Mul OLX OLY OLX_F OLY_F OLT_Mult OLT_Mult_F 
@persist Duped XS_P YS_P XN_P YN_P T_LX T_LY TF_LX TF_LY  WaitDF [L1 L2 L3 L4]:vector [TT1 TT2 TT3 TT4]:vector2 PGenSpec LCT EnergyConsumers:array
@trigger none

if (duped() ) {  
    WaitDF = 1
    exit()
} else {
    if ( WaitDF ) {
        if ( !dupefinished() ) {
            exit()   
        } else {
            local E = entity()
            E["i_duped", number] = 1
        }
        #print("Resetting " + entity():getName())
        local E = entity()
        E["l_reset", number] = systime()
        reset()      
    }
}

if ( first() ) {
    
    
    print("Initializing " + entity():getName())
    
    #include "lib/energy"
    #include "lib/he"
    
    PGenSpec = 335000000
    
    Base["Power", number] = 10
    
    local E = entity()
    E["CID", number] = 3
    
    function clamp_wpod(P:entity) {
        if ( !P ) { return }

        
        if ( P["SIGLOCK", number] ) {      
                   
            if ( P["lock_do", number] ) {       
                P:setPos(Base:toWorld(P["Rel_Pos", vector]))  
                P:setAng(Base:toWorld(ang(0,0,0)) - P["Angular_Offset", angle])
                if ( systime() - P["lock_do_t", number] > 1 ) {                        
                    P["SIGLOCK", number] = 0  
                                  
                    P:propNotSolid(1)
                    #weld(P, Base)
                    P:parentTo(Base)
                    unregister_consumer(Base, EnergyConsumers, P)
                    
                    if ( P["weld_lock", number] ) {
                        P:propFreeze(1)
                        local R = P:getConstraints()
                        local I = R:count()

                        while (I & R[I, entity] != Base ) {
                            R[I,entity]:propFreeze(1)         
                            R[I,entity]:propNotSolid(1)                   
                            R[I,entity]:parentTo(P)
                            I--
                        }
                    }
                    
                    
                    P["lock_do", number] = 0
                    P["lock_do_t", number] = 0
                    P["Locked", number] = 1
                } 
            } else {
                P:setPos(Base:toWorld(P["Rel_Pos", vector]))  
                P:setAng(Base:toWorld(ang(0,0,0)) - P["Angular_Offset", angle])
                P["lock_do", number] = 1
                P["lock_do_t", number] = systime()
                P["Locked", number] = 1
            }
            
        } elseif ( P["SIGUNLOCK", number]  ) {          
            if ( !is_power_available(Base, 10000) ) { 
                P["SIGUNLOCK", number] = 0
                return 
            }
            P["Locked", number] = 0
            P["SIGUNLOCK", number] = 0
            P:propNotSolid(0)
            #P:constraintBreak(Base)
            P:deparent()
            register_consumer(Base, EnergyConsumers, P, 10000)
            
            if ( P["weld_lock", number] ) {
                P:propNotSolid(0)
                local R = P:getConstraints()
                local I = R:count()

                while (I & R[I, entity] != Base) {                
                       
                    R[I,entity]:deparent()
                    R[I,entity]:setPos(Base:toWorld(P["Rel_Pos", vector]))  
                    R[I,entity]:propNotSolid(0)
                    R[I,entity]:propFreeze(0)
                    
                    
                    I--
                }                
            }
            
        }
        if ( !P["Locked", number] ) {       
           
            #local BaseI = 3 * P["GIndex", number]
            local I = P["WP_Manual", number]
            
            if ( I ) {                                      
                
                local V = P["WP_M_Cam", wirelink]["HitPos", vector]
                
                #select(I, CCAMAP_0, CCAMAP_1, CCAMAP_2 )
                                
                local TA = (P:boxCenterW() - V):toAngle() 
                        
                local LA = Base:toLocal(TA)
                                        
                if ( LA:pitch() < 0 ) { 
                    P:setAng(TA - P["Angular_Offset", angle]) 
                } else {
                    P:setAng(Base:toWorld(LA:setPitch(0)) - P["Angular_Offset", angle]) 
                }
                
                
                
                #Radio[(BaseI + 1):toString(), number] = V:x()
                #Radio[(BaseI + 2):toString(), number] = V:y()
                #Radio[(BaseI + 3):toString(), number] = V:z()
            } else {                
                if ( !P["WPIndex", number] | !P["WPTarget", entity] ) {
                    P:setAng(Base:toWorld(ang(0,0,0)) - P["Angular_Offset", angle])
                } else {                 
                    if ( P["WPTarget", entity]:vehicle() ) {
                        CTarget = P["WPTarget", entity]:vehicle()
                    } else {
                        CTarget = P["WPTarget", entity]
                    }  
                    local TA = (P:boxCenterW() - CTarget:boxCenterW()):toAngle() 
                                                      
                    local LA = Base:toLocal(TA)
                                            
                    if ( LA:pitch() < 0 ) { 
                        P:setAng(TA - P["Angular_Offset", angle]) 
                    } else {
                        P:setAng(Base:toWorld(LA:setPitch(0)) - P["Angular_Offset", angle]) 
                    }
                    
                }
                #Radio[(BaseI + 1):toString(), number] = P["WPTarget", entity]:boxCenterW():x()
                #Radio[(BaseI + 2):toString(), number] = P["WPTarget", entity]:boxCenterW():y()
                #Radio[(BaseI + 3):toString(), number] = P["WPTarget", entity]:boxCenterW():z()
            }    
                                    
            P:setPos(Base:toWorld(P["Rel_Pos", vector]))            
        }     
    }    
    
    function thruster_create(HBase:entity, Index, Mat:string, N, M) {
        #HBase:propNotSolid(0)
        
        holoCreate(Index, HBase:boxCenterW(), vec(1, 1, 1), HBase:toWorld(ang(0,0,0)))
        holoModel(Index, "cube")
        holoMaterial(Index, Mat)
        #holoParent(Index, Base)  
        local E = holoEntity(Index)        
        E["thruster_assembly", number] = 1
        
        local PIndex = Index + (50 * Index)
        
        holoCreate(PIndex, HBase:toWorld(vec(-20,0,0)), vec(2.5, 2.5, 5.0), HBase:toWorld(ang(0,90,90)))
        holoModel(PIndex, "hq_stube")
        holoColor(PIndex, vec(100,100,100))
        holoMaterial(PIndex, Mat)
        holoParent(PIndex, Index)
        local E = holoEntity(PIndex)        
        E["thruster_assembly", number] = 1
        #holoShadow(PIndex, 1)
        
        #local E = holoEntity(PIndex)        
        #E["cloak_target", number] = 1
        
        PIndex++
        
        holoCreate(PIndex, HBase:toWorld(vec(-20,0,0)), vec(2.75, 2.75, 3.0), HBase:toWorld(ang(0,90,90)))
        holoModel(PIndex, "hq_stube")
        holoMaterial(PIndex, "sprops/textures/sprops_cfiber2")
        holoColor(PIndex, vec(75,75,75))
        holoParent(PIndex, Index)
        local E = holoEntity(PIndex)        
        E["thruster_assembly", number] = 1
        
        
        PIndex++
        
        holoCreate(PIndex, HBase:toWorld(vec(-47.5,0,0)), vec(2.95, 2.95, 1.0), HBase:toWorld(ang(0,90,90)))
        holoModel(PIndex, "hq_stube")
        #holoMaterial(PIndex, "sprops/textures/sprops_cfiber2")
        holoColor(PIndex, vec(145,45,45))
        holoParent(PIndex, Index)
        local E = holoEntity(PIndex)        
        E["thruster_assembly", number] = 1
        
        
        PIndex++
        
        holoCreate(PIndex, HBase:toWorld(vec(0,0,0)), vec(4.1, 4.1, 4.1), HBase:toWorld(ang(0,0,90)))
        holoModel(PIndex, "hq_sphere")
        holoMaterial(PIndex, "")
        holoColor(PIndex, vec(75,75,75))
        holoParent(PIndex, Index)
        local E = holoEntity(PIndex)        
        E["thruster_assembly", number] = 1
        
        #hq_hdome_thin
        
        PIndex++
        
        holoCreate(PIndex, HBase:toWorld(vec(0,0,0)), vec(4.65, 4.65, 4.65), HBase:toWorld(ang(45 * N,45 * M,180)))
        holoModel(PIndex, "hq_hdome_thin")
        holoColor(PIndex, vec(50,50,50))
        holoMaterial(PIndex, Mat)
        holoParent(PIndex, HBase)
        local E = holoEntity(PIndex)        
        E["thruster_assembly", number] = 1
        
        PIndex++
        
        holoCreate(PIndex, holoEntity(PIndex - 1):toWorld(vec(37 * -N,0 ,93)), vec(2.5, 2.5, 15.0), holoEntity(PIndex - 1):toWorld(ang(-N * 25,0,180)))
        holoModel(PIndex, "hq_stube")
        holoColor(PIndex, vec(75,75,75))
        holoParent(PIndex, PIndex - 1)
        local E = holoEntity(PIndex)        
        E["thruster_assembly", number] = 1
 
        E["cloak_target", number] = 1
        
        PIndex++
        
        holoCreate(PIndex, holoEntity(PIndex - 1):toWorld(vec(10 * N,0 ,15)), vec(2.5, 2.4, 22.0), holoEntity(PIndex - 1):toWorld(ang(N * 31,0,0)))
        holoModel(PIndex, "prism")
        holoColor(PIndex, vec(60,60,60))
        holoParent(PIndex, PIndex - 1)
        local E = holoEntity(PIndex)        
        E["thruster_assembly", number] = 1
               
        E["cloak_target", number] = 1
        
        PIndex++
        
        holoCreate(PIndex, holoEntity(PIndex - 2):toWorld(vec(20 * -N ,0 ,25)), vec(6, 2.4, 7.5), holoEntity(PIndex - 2):toWorld(ang(N * -170,0,180)))
        holoModel(PIndex, "prism")
        holoColor(PIndex, vec(75,75,75))
        holoParent(PIndex, PIndex - 2)
        
        local E = holoEntity(PIndex)        
        E["thruster_assembly", number] = 1
        
        PIndex+=3

        
        holoCreate(PIndex, holoEntity(PIndex - 5):toWorld(vec(8 * -N,0 ,-120)), vec(1.9, 1.9, 7.0), holoEntity(PIndex - 5):toWorld(ang(-N * -15,0,180)))
        holoModel(PIndex, "hq_tube")
        holoColor(PIndex, vec(88,88,88))
        holoParent(PIndex, PIndex - 5)
        
        local E = holoEntity(PIndex)        
        E["thruster_assembly", number] = 1

        PIndex++
        
        holoCreate(PIndex, holoEntity(PIndex - 6):toWorld(vec(109.755 * -N,0.5 ,-209)), vec(2.5, 2.4, 15.0), holoEntity(PIndex - 6):toWorld(ang(-N * -25,0,180)))
        holoModel(PIndex, "hq_stube")
        holoColor(PIndex, vec(75,75,75))
        holoParent(PIndex, PIndex - 6)
        
        local E = holoEntity(PIndex)        
        E["thruster_assembly", number] = 1
        
        PIndex++
        
        holoCreate(PIndex, holoEntity(PIndex - 2):toWorld(vec(0,0,0)), vec(2.3, 2.3, 1.2), holoEntity(PIndex - 2):toWorld(ang(0,0,0)))
        holoModel(PIndex, "hq_stube_thick")
        holoColor(PIndex, vec(68,68,68))
        holoParent(PIndex, PIndex - 2) 
        
        local E = holoEntity(PIndex)        
        E["thruster_assembly", number] = 1       
        
        PIndex++
        
        holoCreate(PIndex, holoEntity(PIndex - 3):toWorld(vec(0,0,0)), vec(2.15, 2.15, 2.2), holoEntity(PIndex - 3):toWorld(ang(0,0,0)))
        holoModel(PIndex, "hq_tube")
        holoColor(PIndex, vec(55,55,55))
        holoParent(PIndex, PIndex - 3)
        
        local E = holoEntity(PIndex)        
        E["thruster_assembly", number] = 1
        
        PIndex++
        
        holoCreate(PIndex, holoEntity(PIndex - 4):toWorld(vec(0,0,40)), vec(2.15, 2.15, 0.3), holoEntity(PIndex - 4):toWorld(ang(0,0,0)))
        holoModel(PIndex, "hq_tube")
        holoColor(PIndex, vec(55,55,55))
        holoParent(PIndex, PIndex - 4)
        
        local E = holoEntity(PIndex)        
        E["thruster_assembly", number] = 1
        
        PIndex++
        
        holoCreate(PIndex, holoEntity(PIndex - 5):toWorld(vec(0,0,-31)), vec(2.25, 2.25, 0.7), holoEntity(PIndex - 5):toWorld(ang(0,0,0)))
        holoModel(PIndex, "hq_stube_thick")
        holoColor(PIndex, vec(55,55,55))
        holoParent(PIndex, PIndex - 5)    
        
        local E = holoEntity(PIndex)        
        E["thruster_assembly", number] = 1    
            
        PIndex++
        
        holoCreate(PIndex, holoEntity(PIndex - 6):toWorld(vec(0,0,0.4)), vec(0.68, 0.68, 2.65), holoEntity(PIndex - 6):toWorld(ang(0,0,90)))
        holoModel(PIndex, "hq_tube")
        holoColor(PIndex, vec(55,55,55))
        holoParent(PIndex, PIndex - 6)  
        
        local E = holoEntity(PIndex)        
        E["thruster_assembly", number] = 1 
        
        #holoShadow(PIndex, 1)
        
        #holoEntity(PIndex):setSubMaterial(1, "sprops/textures/sprops_cfiber2")
        #holoRenderFX(PIndex, 3)
        #holoSkin(PIndex, 2)
        #entity():parentTo(Base)

        #HBase:propNotSolid(1)
               
    }
    
    Base["skip_holo_create", number] = Options[102, number]
    
    
    if ( !Base["skip_holo_create", number] ) {
        
        thruster_create(Th1:entity(), 1, "", 1, -1)
        thruster_create(Th2:entity(), 2, "", -1, 1)
        thruster_create(Th3:entity(), 3, "", -1, -1)
        thruster_create(Th4:entity(), 4, "", 1, 1)
        
    } else {
        Base:setAlpha(255)
    }
        
    Base["ctrl_id", number] = 1    
    
    Base["_Th4", entity] = Base["_Th3", entity] = Base["_Th2", entity] = Base["_Th1", entity] = Base
    Base["_Th1_mlf", number] = Base["_Th4_mlf", number] = Base["_Th2_mlf", number] = Base["_Th3_mlf", number] = 0
    
    init_ap_he(AP_HE3)
    init_ap_he(AP_HE4)
    
    init_energy_timers()
    register_consumer(Base, EnergyConsumers, entity(), 150)
    
    local E = entity()
    
    if ( !E["i_duped", number] ) {
        signalSend("SIGGPL", 1)        
    } else {
        E["i_duped", number] = 0
    }
    
    runOnTick(1)
    runOnLast(1)    

} elseif ( last() ) {
    holoDeleteAll()
    #signalSend("SIGRT", 1)
    exit()
}

if ( !perf(50) ) {
    exit()
}
        

if ( Base["engine", number] == 1 ) {
    if (abs(Base:angles():roll()) >= 90 ) {    
    
        
        Dist_R_L = Dist_R_R = Dist_F_L = Dist_F_R = 10000000
        
        YawOffsetF_L = -90
        YawOffsetF_R = 90
        YawOffsetR_L = -90
        YawOffsetR_R = 90
    } 
    if ( Lock_XY ) {
        if ( Lock_XY == 2 ) {
            if ( Base["lxy_ent", entity]) {
                Lock_Vec = Base["lxy_ent", entity]:pos()  
            } else {
                Lock_Vec = Base:boxCenterW()
            }   
            #local ZDiff = Base:pos():z() - Lock_Vec:z()
            if ( Base["lock_z", number] ) {
                TargetZ = Lock_Vec:z() + Base["lock_z_off", number]
            }
               
        }
        V_TT1 = V_TT2 = V_TT3 = V_TT4 = Lock_Vec
    } else {
        V_TT1 = Base["_Th1", entity]:boxCenterW()
        V_TT2 = Base["_Th4", entity]:boxCenterW()
        V_TT3 = Base["_Th2", entity]:boxCenterW()
        V_TT4 = Base["_Th3", entity]:boxCenterW()
    }
    
    local BYawRC_L = cos(Base:toWorld(ang(0,180 + YawOffsetR_L,0)):yaw()) * Dist_R_L
    local BYawRC_R = cos(Base:toWorld(ang(0,180 + YawOffsetR_R,0)):yaw()) * Dist_R_R
    local BYawRS_L = sin(Base:toWorld(ang(0,180 + YawOffsetR_L,0)):yaw()) * Dist_R_L
    local BYawRS_R = sin(Base:toWorld(ang(0,180 + YawOffsetR_R,0)):yaw()) * Dist_R_R
    
    TT3 = vec2(V_TT3:x() + BYawRC_L, V_TT3:y() + BYawRS_L )
    TT4 = vec2(V_TT4:x() + BYawRC_R, V_TT4:y() + BYawRS_R )
    
    BYawRC_L = cos(Base:toWorld(ang(0,180 + YawOffsetF_L,0)):yaw()) * Dist_F_L
    BYawRC_R = cos(Base:toWorld(ang(0,180 + YawOffsetF_R,0)):yaw()) * Dist_F_R
    BYawRS_L = sin(Base:toWorld(ang(0,180 + YawOffsetF_L,0)):yaw()) * Dist_F_L
    BYawRS_R = sin(Base:toWorld(ang(0,180 + YawOffsetF_R,0)):yaw()) * Dist_F_R
    
    TT1 = vec2(V_TT1:x() + BYawRC_L, V_TT1:y() + BYawRS_L )
    TT2 = vec2(V_TT2:x() + BYawRC_R ,V_TT2:y() + BYawRS_R )
    
    
    Z_Forward = Th1:entity():pos():z()
    local T = (TargetZ - Z_Forward) - ($Z_Forward * Strength )
    Th1["Mul", number] = (abs(T) + Base["_Th1_mlf", number] )
    L1 = vec(TT1:x() ,TT1:y(),Z_Forward + T * 4)
    
    holoPos(1, Th1:entity():pos())
    holoAng(1, (L1 - Th1:entity():pos()):toAngle())
    
    Th1["Vector", vector] = Th1:entity():pos() - L1
    
    Z_Right = Th4:entity():pos():z()
    T = (TargetZ - Z_Right) - ($Z_Right * Strength )   
    Th4["Mul", number] = (abs(T) + Base["_Th4_mlf", number]) 
    L2 = vec(TT2:x() ,TT2:y(),Z_Right + T * 4)
    
    holoPos(4, Th4:entity():pos())
    holoAng(4, (L2 - Th4:entity():pos()):toAngle())
    
    Th4["Vector", vector] = Th4:entity():pos() - L2
    
    Z_Rear = Th2:entity():pos():z()
    T = ((TargetZ - Z_Rear) - ($Z_Rear * Strength ))   
    Th2["Mul", number] =  (abs(T) + Base["_Th2_mlf", number]) 
    L3 = vec(TT3:x() ,TT3:y(),Z_Rear + T * 4)
    
    holoPos(2, Th2:entity():pos())
    holoAng(2, (L3 - Th2:entity():pos()):toAngle())
    
    Th2["Vector", vector] = Th2:entity():pos() - L3
    
    Z_Left = Th3:entity():pos():z()
    T = ((TargetZ - Z_Left) - ($Z_Left * Strength ))   
    Th3["Mul", number] = (abs(T) + Base["_Th3_mlf", number]) 
    L4 = vec(TT4:x() ,TT4:y(),Z_Left + T * 4)
    
    holoPos(3, Th3:entity():pos())
    holoAng(3, (L4 - Th3:entity():pos()):toAngle())
    
    Th3["Vector", vector] = Th3:entity():pos() - L4
    
    local TCC = clamp(systime() - LCT, 0, 1)
    #print("Delivering " + deliver_energy(Base, PGenSpec * TCC))
    deliver_energy(Base, (PGenSpec * TCC) * 
        (1 - clamp(((Base["_Th1_mlf", number] + Base["_Th2_mlf", number] + Base["_Th3_mlf", number]+ Base["_Th4_mlf", number] + Base["ZHHOff", number]) / 5) / 1000, 0, 1) )        
        )
    
    LCT = systime()

} elseif ( Base["engine", number] == 2 & Base["CCAM_0_WL", wirelink] ) {

    local BC = Base:pos()
    
    
    Base["last_z_", number] = BC:z()

    local AL = clamp(Base:toLocal((Base["CCAM_0_WL", wirelink]["CamAng", angle] - ang(180,0,180))) / 2.5, ang(-30,-35,-37), ang(30,35,37)) 
    local AO = ( (AL - (AL - Base["last_a_", angle] )) + ((AL ) * (1 / ang(abs(AL:pitch()), abs(AL:yaw()), abs(AL:roll()) )) )):setRoll(AL:yaw() / 1.25 )
    Base["last_a_", angle] = AL

    if ( !Base["aux_fforce", number] ) {
        FU = (((TargetZ - BC:z()) - (BC:z() - Base["last_z_", number])) * (1000 * (Base["aux_z_strength", number])) ) 
            
    } else {
        FU =  ((Base:mass() * 10) * (Base["aux_z_strength", number])) * 
            (400000/(clamp(Base["aux_fforce", number], 400000, 100000000000)))

    }

    local GE =  abs(FU ) + abs(Base:angles():roll() * 1000) + abs(AO:pitch()) + abs(AO:yaw()) + abs(AO:roll())
    
    if ( !is_power_available(Base, GE) ) {
        exit()
    }
    grab_energy(Base, GE)
   
    
    Base:applyForce(vec(0,0,FU) )  
    
    local BME = Base:mass() / 6.7

    if ( !Base["aux_ang_rot_off", number ] ) {

        local APF = (AO * 125000 )
        Base:applyAngForce( APF )       
     
            local AALY = abs(AL:yaw())
            local CFY = ((-APF:yaw() - (APF:yaw() - Base["last_al_y", number ]  ))  ) * 
                (1/clamp(AALY,1,AALY ) ^ 2 )
            Base["last_al_y", number ] = APF:yaw()

            Base:applyAngForce( ang(0 , 
                clamp( 2 * CFY * ( 1 - clamp(abs(CFY / APF:yaw()), 0, 1) ), abs(APF:yaw()) / -1.1, abs(APF:yaw()) / 1.1) ,
                0 ) )
    
        
        Base:applyAngForce( ang(-Base:angles():pitch() * (abs(Base:angles():pitch()) ) * BME  / 8  , 0, 0 ) )
    } else {
        Base:applyAngForce( ang(-Base:angles():pitch() * (abs(Base:angles():pitch()) ) * BME  , 0, 0 ) )
    }
    
   
    
    Base:applyAngForce( ang(0, 0, -Base:angles():roll() * (abs(Base:angles():roll()) ) * (BME )  )  )
    
    
    
    project_ap_he_hov(AP_HE3, AP_HE4, clamp(FU / 6500, 15,20))

} else {
    
    
    Th1["Mul", number] = 0
    Th2["Mul", number] = 0
    Th3["Mul", number] = 0
    Th4["Mul", number] = 0
    
    holoPos(1, Th1:entity():pos())
   
    holoPos(4, Th4:entity():pos())
       
    holoPos(2, Th2:entity():pos())       
    
    holoPos(3, Th3:entity():pos())
    
}

proc_energy_clk(Base, EnergyConsumers)



clamp_wpod(WPA[1, entity])
clamp_wpod(WPA[2, entity])
clamp_wpod(WPA[3, entity])
clamp_wpod(WPA[4, entity])
clamp_wpod(WPA[5, entity])
clamp_wpod(WPA[6, entity])
clamp_wpod(WPA[7, entity])
clamp_wpod(WPA[8, entity])

if ( Base["at_jail_1", number] ) {
    Base["at_jail_e1", entity]:setPos(Base["at_jail_u1", entity]["Ctl_Target", entity]:pos() )
}

if ( Base["at_jail_2", number] ) {
    Base["at_jail_e2", entity]:setPos(Base["at_jail_u2", entity]["Ctl_Target", entity]:pos() )
}

if ( Base["at_jail_3", number] ) {
    Base["at_jail_e3", entity]:setPos(Base["at_jail_u3", entity]["Ctl_Target", entity]:pos() )
}

