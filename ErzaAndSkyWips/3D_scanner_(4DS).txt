@name 3D scanner (4DS)
@inputs DS1:wirelink DS2:wirelink DS3:wirelink DS4:wirelink On
@outputs X Y Resolution Scale Pos:angle X1 Y1 Ranger:ranger Color:vector Ace
@outputs OldT NewT Secs Mins AvgRate TimeRemain TotalTime TimeStr:string
@outputs TimeRStr:string TimerStarted Distance Clr GreyScale RC:vector RN
@persist RE:vector PercentDone Done Remaining Time Height
@trigger 

interval(50)

if(first()){
    #Only Change this things
    
    #Resolution
    Resolution = 256
    
    #Change it Depending on the thing to scan
    Scale = 8
    
    #Change this Depending on the thing to scan
    Distance = 600
    
    #Recomended GreyScale 1000
    GreyScale = 2000
}




#DONT CHANGE NOTHING FROM HERE UNLESS YOU KNOW WHAT YOU ARE DOING!!!!



function wirelink:initScreen(Resolution){
        This[1048574]=0 #Reset Screen and whatever was on it will be cleared.
        This[1048569]=3 #Set color mode to 3
        This[1048575]=1 #Apply changes
        This[1048572]=Resolution #Set new resolution on Y (Height)
        This[1048573]=Resolution #Set new resolution on X (Width)
        This["PixelG",number] = 999
}   

if(first()){
    
    TimerStarted = 0
    Height = 0.5
    
    holoCreate(1)
    holoCreate(2)
    holoModel(1,"models/dav0r/camera.mdl")
    holoModel(2,"hq_sphere")
    #holoCreate(3)
    #holoCreate(4)
    #holoCreate(5)
    holoPos(1,entity():toWorld(vec(0,0,60)))
    
    holoCreate(3)
    holoCreate(4)
    holoPos(4,holoEntity(1):pos()+vec(0,20,10))
    holoPos(3,holoEntity(1):pos()+vec(0,20,-12)+vec(0,0,PercentDone/8.5))
    holoAng(3,holoEntity(1):angles())
    holoAng(4,holoEntity(1):angles())
    holoScale(3,vec(0.75,0.75,PercentDone/50))
    holoScale(4,vec(1,1,2.25))
    holoAlpha(4,100)
    holoModel(3,"hqcylinder")
    holoModel(4,"hqcylinder")
    holoColor(3,vec(0,255,0))
    holoColor(4,vec(200,200,200))
    holoParent(3,4)    

    DS1:initScreen(Resolution/2)
    DS2:initScreen(Resolution/2)
    DS3:initScreen(Resolution/2)
    DS4:initScreen(Resolution/2)
}

    holoScale(3,vec(0.75,0.75,PercentDone/50))
    holoPos(3,holoEntity(4):pos()-vec(0,0,12)+vec(0,0,PercentDone/8.5))
    
    holoColor(3,hsl2rgb(PercentDone,0.5,0.5))
    
if(On){

while(perf()){
for (A = 1, 18) {
    
    Pos = entity():toWorld(ang((Y*(Scale/(Resolution/12.8)))-(6*Scale),(X*(Scale/(Resolution/12.8)))-(Resolution/(Resolution/(Scale*6))),0))
    Ranger = rangerOffset(Distance,holoEntity(1):toWorld(vec()),holoEntity(1):forward())
    RN = (holoEntity(1):up():distance(Ranger:hitNormal())) / 2
    Clr = GreyScale - Ranger:distance()
    if(Ranger:hitSky()){
        RC=vec(0,161,255)
        Color=RC*(vec(Clr,Clr,Clr)/GreyScale)
    }
    if(!Ranger:hit()){
        RC=vec(0,161,255)
        Color=RC
    }
    if(Ranger:hitWorld()&!Ranger:hitSky()){
        RC=vec(0,random(200,255),random(0,30))
        Color=RC*(vec(Clr,Clr,Clr)/GreyScale)
    }
    if(Ranger:hit()&!Ranger:hitWorld()){
        RC=Ranger:entity():getColor()
        Color = round((RN*(vec(Clr,Clr,Clr)/GreyScale))*RC)
    }
    #if(Ranger:rangerHitEntities(1)){
    #    RC=Ranger:entity():getColor()
    #}
    
    #if(Ranger:hitWorld()&!Ranger:hitSky()){
    #    print("hey")
    #    RC=vec(0,random(200,255),random(0,30))
    #}
    
    holoAng(1,Pos)
    
    holoPos(2,Ranger:pos())
    
    
    if(Y < Resolution&On){
        X += 1
    }
    
    if (X > Resolution) {
        X = 0
        Y += 1
    }
    
    if (Y > Resolution) {
        X = 0
        Y = 0
    }
    
    #if(Ranger:hit()){
        X1=abs(X-Resolution)
        Y1=Y
    #}
    
    
    
    function wirelink:drawPixel(X,Y,Color:vector,Resolution){
            This[X+Y*Resolution]=rgb2digi(Color,3)
    }
    
    if(X1<=Resolution/2&Y1<=Resolution/2){
        DS1:drawPixel(X1,Y1,Color,Resolution/2)
    }
    
    if(X1>=Resolution/2&Y1<=Resolution/2){
        DS2:drawPixel(X1,Y1-1,Color,Resolution/2)
    }
    
    if(X1<=Resolution/2&Y1>=Resolution/2){
        DS3:drawPixel(X1,Y1-Resolution/2,Color,Resolution/2)
    }
    
    if(X1>=Resolution/2&Y1>=Resolution/2){
        DS4:drawPixel(X1,Y1-Resolution/2-1,Color,Resolution/2)
    }
}


PercentDone = round((((Y*Resolution)+X)/(Resolution*Resolution))*100)
Done = round((Y*Resolution)+X)
Remaining = round(Resolution*Resolution)

setName("3D Scanner
"+PercentDone+"%
"+Done+"/"+Remaining+"
"+TimeRStr)

if (!TimerStarted) {
    TimerStarted = 1
    timer("clock",300)
    
    if (On) {
        Time += 0.1
        AvgRate = Done / Time
        TotalTime = Resolution^2 / AvgRate
        TimeRemain = TotalTime - Time
        
        Mins = floor(Time / 60)
        Secs = floor(Time % 60)
        if (Secs >= 10) { TimeStr = Mins + ":" + Secs } else { TimeStr = Mins + ":0" + Secs }
        
        Mins = floor(TimeRemain / 60)
        Secs = floor(TimeRemain % 60)
        if (Secs >= 10) { TimeRStr = "Time Remaining : " + Mins + ":" + Secs } else { TimeRStr = "Time Remaining : " + Mins + ":0" + Secs }
    }
}
if (clk("clock")) {
    TimerStarted = 0
}
}
if(changed(On)&On){timer("clock",100)}
}
