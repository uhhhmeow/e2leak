@name HoloUniverse
@inputs 
@outputs
@persist  R X Y Z X1 Y1 X2 Y2 X3 Y3 X4 Y4 X5 Y5 X6 Y6 X7 Y7 X8 Y8 Z8 T
@model models/sprops/cylinders/size_5/cylinder_12x18.mdl
runOnTick(1)

Y = 50
entity():setColor(random(255),random(255),random(255))
runOnTick(1)



findIncludeClass("prop")
findIncludeClass("player")
Sphere = findInSphere(entity():pos(), 10000)
if(Sphere > 0){
O = findClosest(entity():pos())  
O:propGravity(0)
O:plySetGravity(Y)
}
if (first())
{
       Prop1 = propSpawn("models/props_phx/construct/glass/glass_angle360.mdl",(entity():pos() + vec(0,0,25)),1)
    Prop1:propFreeze(1)
    Prop1:setMaterial("models/alyx/emptool_glow")
     Prop2 = propSpawn("models/sprops/cylinders/size_5/cylinder_12x18.mdl",(entity():pos() + vec(0,0,35)),1)
    Prop2:propFreeze(1)
    Prop2:setColor(vec(150,50,10))

    findExcludeEntity(Prop1)
    findExcludeEntity(Prop2)   
    findIncludeClass("prop")
findIncludeClass("player")
Sphere = findInSphere(entity():pos(), 10000)
if(Sphere > 0){
O = findClosest(entity():pos())  
O:propGravity(0)
O:plySetGravity(Y)
}

    #SPAZIO
    holoCreate(1) #SPAZIO (CUPOLA) - ENTITY: models/props_phx/construct/metal_dome360.mdl
    holoCreate(2) #SPAZIO (PAVIMENTO) - ENTITY: models/props_phx/construct/metal_angle360.mdl
    
    #PIANETI
    holoCreate(3) #SOLE
    holoCreate(4) #MERCURIO
    holoCreate(5) #VENERE
    holoCreate(6) #TERRA
    holoCreate(7) #MARTE
    holoCreate(8) #GIOVE
    holoCreate(9) #SATURNO
    holoCreate(10) #URANO
    holoCreate(11) #NETTUNO
    
    #SETTAGGIO SPAZIO
    holoModel(1, "models/props_phx/construct/metal_dome360.mdl")
    holoModel(2, "models/props_phx/construct/metal_angle360.mdl")
    
    holoScale(1, vec(100,100,100))
    holoScale(2, vec(100,100,0))
    
    #SETTAGGIO MATERIALE OLOGRAMMI
    for (I=1,12) {holoMaterial(I, "holograms/hologram")}
    
    #CODA PIANETI (ESTETICO)
    for(TRAILS=3,12) {holoEntity(TRAILS):setTrails(3,1,100,"trails/laser",vec(255,255,255),255)}
    
    holoPos(1, entity():pos()+vec(0,0,1))
    holoPos(2, entity():pos()+vec(0,0,0.5))
    
    #MODELLI PIANETI
    holoModel(3, "models/XQM/Rails/gumball_1.mdl")
    holoModel(4, "hq_sphere")
    holoModel(5, "hq_sphere")
    holoModel(6, "hq_sphere")
    holoModel(7, "hq_sphere")
    holoModel(8, "hq_sphere")
    holoModel(9, "hq_sphere")
    holoModel(10, "hq_sphere")
    holoModel(11, "hq_sphere")
    
    #SCALA PIANETI
    holoScale(3,vec(10,10,10))
    holoScale(4,vec(0.5,0.5,0.5)) #MERCURIO
    holoScale(5, vec(1.6,1.6,1.6)) #VENERE
    holoScale(6, vec(1.3,1.3,1.3)) #TERRA
    holoScale(7, vec(0.7,0.7,0.7)) #MARTE
    holoScale(8, vec(5,5,5)) #GIOVE
    holoScale(9, vec(3.5,3.5,3.5)) #SATURNO
    holoScale(10, vec(1.3,1.3,1.3)) #URANO
    holoScale(11, vec(1.1,1.1,1.1)) #NETTUNO
    
    #SETTAGGIO COLORI
    holoColor(1,vec(0,0,0))
    holoColor(2,vec(0))
    
    holoColor(3,vec(255,50,0)) #SOLE
    holoColor(4, vec(100,70,30)) #MERCURIO
    holoColor(5,vec(90,90,50)) #VENERE
    holoColor(6,vec(40,40,255)) #TERRA
    holoColor(7, vec(120,30,20)) #MARTE
    holoColor(8,vec(100,90,50)) #GIOVE
    holoColor(9,vec(130,100,60)) #SATURNO
    holoColor(10,vec(70,70,255)) #URANO
    holoColor(11,vec(80,80,255)) #NETTUNO
    
    for (SHADOW = 3,11){holoDisableShading(SHADOW, 0)}
    
    #PLANET THINGS...
    holoCreate(12) #ANELLO DI SATURNO
    holoModel(12, "hq_torus")
    holoScale(12, vec(7.7,7.7,1))
    holoColor(12, vec(50,50,50))
    
    holoCreate(13) #ANELLO URANO
    holoModel(13, "hq_torus")
    holoScale(13, vec(5,5,0.3))
    holoColor(13, vec(50,50,60))
    holoAlpha(13, 10)
    
}

#SETTAGGIO TEMPO E ROTAZIONE OLOGRAMMI
T+=0.05
Speed = 3
Size = 300
P=holoEntity(3):pos() #SOLE
P0=holoEntity(8):pos() #GIOVE
P1=holoEntity(9):pos() #SATURNO
P2=holoEntity(10):pos() #URANO
P3=holoEntity(11):pos() #NETTUNO

R+=0.5

#MERCURIO
X = P:x()+cos(T*Speed)*Size
Y = P:y()+sin(T*Speed)*Size
Z = P:z()

#VENERE
X1 = P:x()+cos(T*1.8)*400
Y1 = P:y()+sin(T*1.8)*400

#TERRA
X2 = P:x()+cos(T*1)*510
Y2 = P:y()+sin(T*1)*510

#MARTE
X3 = P:x()+cos(T*1.1)*560
Y3 = P:y()+sin(T*1.1)*560

#GIOVE
X4 = P:x()+cos(T*0.5)*700
Y4 = P:y()+sin(T*0.5)*700

#SATURNO
X5 = P:x()+cos(T*0.8)*870
Y5 = P:y()+sin(T*0.8)*870

#URANO
X6 = P:x()+cos(T*0.3)*920
Y6 = P:y()+sin(T*0.3)*920

#NETTUNO
X7 = P:x()+cos(T*0.1)*1030
Y7 = P:y()+sin(T*0.1)*1030


#ROTAZIONE OLOGRAMMI
holoPos(3, entity():pos()+vec(0,0,250)) #SOLE
holoAng(3, ang(vec(0,R,0)))

#MERCURIO
holoPos(4, vec(X,Y,Z))

#VENERE
holoPos(5, vec(X1,Y1,Z))

#TERRA
holoPos(6, vec(X2,Y2,Z))

#MARTE
holoPos(7, vec(X3,Y3,Z))

#GIOVE
holoPos(8, vec(X4,Y4,Z))

#SATURNO
holoPos(9, vec(X5,Y5,Z))

#URANO
holoPos(10, vec(X6,Y6,Z))

#NETTUNO
holoPos(11, vec(X7,Y7,Z))

#ANELLO DI SATURNO
holoPos(12, P1)
holoAng(12, ang(vec(-40,-20,0)))

#ANELLO DI URANO
holoPos(13, P2)
holoAng(13, ang(vec(-40,20,0)))


if(first()){
 soundPlay(1,999,"holyhandgrenade.wav")   
}
