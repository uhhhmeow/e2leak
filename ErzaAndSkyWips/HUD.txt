@name HUD
@inputs EGP:wirelink
@outputs Player:entity
@persist Size:vector2 Players:array Count Player:entity BoxSize:vector2
@trigger all
interval(50)
if(first()){
    Count=0
    Size=vec2(4,4)
    BoxSize=vec2(100,100)
    Players=players()
}

if(changed(players():count())) {
    EGP:egpClear() #Setzt alle Eintrge zurueck, werden durch Forloop neu gesetzt
}


for(I=1,players():count()) {
    EGP:egp3DTracker(I,Players[I,entity]:attachmentPos("chest"))
    EGP:egpText(I+30,Players[I,entity]:name(),vec2())
    EGP:egpBoxOutline(I+60,vec2(),BoxSize)
    #EGP:egpCircle(I+60,vec2(),Size)
    EGP:egpColor(I+60,vec(60,120,0))
    EGP:egpParent(I+30,I)
    EGP:egpParent(I+60,I)
}
