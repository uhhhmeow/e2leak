@name ECOMM-033
@inputs [Base]:entity
@outputs 
@persist [WaitDF Run] [Chip Owner]:entity RTXM [IN_DATA IN_DATA_MSG OUT_DATA MSG_Q]:table [Running Mode ONLINE] [VMS_MA VMS_MI VMS_R]
@trigger 

# modes:
#
#  0 - normal
#  1 - pipe

if (duped() ) {    
    WaitDF = 1
    exit()
} else {
    if ( WaitDF ) {
        if ( !dupefinished() ) {
            exit()   
        }
        #print("Resetting " + entity():getName())
        reset()      
    }
}


if ( first() ) {
    
    runOnSignal("SIGREEC", 1, 1)
    
    Chip = entity()
    Owner = owner()
    VMS_MA = 0
    VMS_MI = 4
    VMS_R = 30
        
    Chip["URL_HOST", string] = "http://nixnodes.net/gmod/ecomm"
    Chip["HUB_PASS", string] = "password"

    Mode = 0
    
    function reset_rev() {
        Chip["VER_REV", number] = VMS_R
    }
    
    function increment_version(K:string, D) {
        if ( Chip[K, number] ) {
            Chip[K, number] =  Chip[K, number] + 1   
        } else {
            Chip[K, number] = D
        }
    }
    
    function set_current_version() {
        Chip["VER_MAJOR", number] = VMS_MA
        Chip["VER_MINOR", number] = VMS_MI
        increment_version("VER_REV", VMS_R)
    }
    
    function string assemble_ver_string(){
        return Chip["VER_MAJOR", number] + "." + Chip["VER_MINOR", number] + 
                    "." + Chip["VER_REV", number]   
    }
    
    function number send_message(URL:string, M, Data:string, PO) {  
        if ( httpCanRequest()) {     
            httpRequest(URL + "/?d="+ Data ) 
            return 1
        } else {
            return 0
        }
    }
    
    function table assemble_package(S:string, O, PO, P0, S2:string, R:string) {
        local T = table( )
        T["data", string] = (S)
        T["data2", string] = (S2)
        T["mode", string] = O:toString()
        T["opt", string] = PO:toString()
        T["vi0", string] = systime():toString()
        T["steamid", string] = (owner():steamID())
        T["recipient", string] = R
                
        T["name", string] = (owner():name())
        T["map", string] = (map())
        T["server", string] = (hostname()) 
        
        T["p", string] = Chip["HUB_PASS", string]
        
        return T:clone()
    }
    
    function string assemble_json_block(T:table) {
        return "[" + jsonEncode(T) + "]"  
    }
    
    function push_to_sendq(S:string, O, PO, VI0, S2:string, R:string) {     
           
        OUT_DATA:pushTable(assemble_package(S,O,PO, VI0, S2, R))         
    }
    
    function push_to_msgq(S:string) {
        local T = table()
        T["msg", string] = S
        T["col", vector] = vec(255,255,255)
        MSG_Q:pushTable(T)   
    }
    
    function ds_hud_send_message_p(S:string, Opt, E:entity) {
        local R = array( S , Opt, E )
        
        if ( !dsSend("SIGMSG", "HUD", R) ) {
            print("ds_hud_send_message: failed sending data signal")
        }
    }
    
    function message_print_b(Color:vector, Msg:string) {
        printColor(Color, Msg)     
        ds_hud_send_message_p(Msg,  1, owner())            
    }
    
    function message_print(Color:vector, Name:string, Msg:string) {
        printColor(Color, "<" + Name + "> ", vec(255,255,255), Msg)     
        ds_hud_send_message_p("<" + Name + "> " +  Msg,  1, owner())            
    }
    
    function message_print_in(Color:vector, Name:string, Chan:string , Msg:string) {
        printColor(Color,"#"+Chan+" <" + Name + "> ", vec(255,255,255), Msg)     
        ds_hud_send_message_p("#"+Chan+" <" + Name + "> " +  Msg,  1, owner())            
    }
    
    function message_print_out_p(Color:vector, Name:string, Target:string , Msg:string) {
        printColor(Color,"<"+Name+"-> " + Target + "> ", vec(255,255,255), Msg)     
        ds_hud_send_message_p("<"+Name+"->" + Target + "> " +  Msg,  1, owner())            
    }
    
    
    function notify_ecomm_mode(N) {       
        if ( Owner["chat_mode", number] == 2 ) {
            printColor(vec(25,175,4), "$> ", vec(255,255,255), "Entering hybrid chat mode") 
        } elseif ( Owner["chat_mode", number] ) {
            printColor(vec(25,175,4), "$> ", vec(255,255,255), "Entering ECCOM chat mode") 
        } else {
            if ( !N ) {
                printColor(vec(175,25,4), "$> ", vec(255,255,255), "Exiting ECCOM chat mode") 
            }
        }   
    }
    
    function process_package(NameS:string, T:table) {
        if ( T[1, string] == "1" ) {
            message_print(vec(25,175,2), NameS , "logging on from " + T[6, string] +
                " [ " + T[2, string] + " ] [" + assemble_ver_string() + "]")     
        } elseif ( T[1, string] == "3" | 
                (T[1, string] == "2" & T[4, string] == owner():name() )) {
            push_to_sendq("PONG: [" + assemble_ver_string() + "]" , 
                    1, 10, T[8, string]:toNumber(), "pvdummy", T[5, string])
            message_print(vec(25,45,165), "NOTICE", T[5, string] + " pinged us") 
        } elseif ( T[4, string]:length()) {
           if ( T[1, string] == "10" ) {
                message_print(vec(25,175,2), NameS, "[ " + T[6, string] + 
                    " ] [ " + T[2, string] + " ]: " + T[4, string] + " (" + 
                    round(systime() - T[8, string]:toNumber(), 3) + "s)" )                                        
            }  else {
                message_print_in(vec(25,175,2), NameS, T[9, string], T[4, string])             
            }                    
        }       
    }
    
    function join_channel(C:string) {
        Chip["channel", string] = C  
        push_to_sendq(C,10, 0, 0, "", "") 
    }
    
    
    function print_help() {
        push_to_msgq("ECOMM - External Communications Module")     
        push_to_msgq("  .<text>   -   Send <text> to the current channel")   
        push_to_msgq("  -<nick> <text>   -   Send <text> to <nick> (private message)")  
        push_to_msgq("  !chat   -   Toggle implicit chat mode (typed text will be sent to current channel and muted on the server)")    
        push_to_msgq("  !ping <nick>   -   Sends a packet to <nick> and awaits reply, timing its round-trip")
        push_to_msgq("  !gping   -   Broadcast ping (pings everyone)")
    }
        
    set_current_version()
    reset_rev()

    runOnHTTP(1)
    runOnChat(1)
    
    push_to_sendq("LOGON",1, 1, 0, "global", "")
    join_channel("global")
    push_to_sendq("pvdummy",10, 0, 0, "", "") 
    
    printColor(vec(25,170,170), Chip:getName(), vec(255,255,255), 
        " v" + assemble_ver_string() + " connecting.." )
        
    notify_ecomm_mode(1)
    
    push_to_msgq("Type !cmdlist for command list")
    
    timer("poll_rcvd", 400)
    
    RTXM = 1
    
} 

interval(250)

if ( RTXM  & ONLINE) {    
    if ( !Running ) {
        if  ( httpCanRequest() ) {
            Running = 2
            stoptimer("rtimeout")
            timer("rtimeout", 45000)
            #print(assemble_json_block(assemble_package("",2,0,0, "", "")))
            httpRequest(Chip["URL_HOST", string]  + "/?d="+ 
                httpUrlEncode(assemble_json_block(assemble_package("",2,0,0, "", ""))) ) 
            
            RTXM = bXor(RTXM, 1)
            
        }      
    } else {
        RTXM = bXor(RTXM, 1)
    }
} else {
    if ( OUT_DATA:count() ) {
        if ( !Running & httpCanRequest()  ) {
            
            O = "["
            while ( OUT_DATA:count() ) {
                local JSON = jsonEncode(OUT_DATA[1, table])
                O += JSON 
                if ( OUT_DATA:count() > 1 ) {
                    O+=","
                }
                OUT_DATA:remove(1)
            }
            O += "]"
            #print(O)
            #print(httpUrlEncode(O))
            httpRequest(Chip["URL_HOST", string]  + "/?d="+ httpUrlEncode(O) ) 
            RTXM = bXor(RTXM, 1)

        } 
    } else {
        RTXM = bXor(RTXM, 1)
    }
}

if ( httpClk() ) {
    local S = httpData()
    local Data = jsonDecode(S)
    
    if ( Running == 2 ) {
        
        #print(S)
        #print(S)
        if (Data:count() ) {
            
            IN_DATA:pushTable(Data)
        } else {
            if ( S:length() > 2 & S:left(2) != "^^" ) {                
                print(S)
            }
        }
        Running = 0
        stoptimer("rtimeout")
    } else {    
        if (Data:count() ) {
            
            IN_DATA_MSG:pushTable(Data)
        } else {    
            if ( S:left(2) != "^^" ) {                
                print( S)
                
            }
        }         
    }
} elseif ( clk("poll_rcvd")) {
    timer("poll_rcvd", 400)
    if ( IN_DATA:count() ) {
        local TD = IN_DATA[1, table]
        
        if ( TD:count() ) {
            local T = TD[1, table]    
   
            if ( hostname() != T[6, string]) {
                NameS = T[5, string] + " ( " + T[6, string]:left(15) + " )"     
            } else {
                if ( Owner["chat_mode", number] == 2) {
                    TD:remove(1)
                    exit()
                } 
                NameS = T[5, string] 
            } 
          
            process_package(NameS, T)
            TD:remove(1)
        } else {            
            IN_DATA:remove(1)            
        }
    } elseif ( IN_DATA_MSG:count() ) {
        local TD = IN_DATA_MSG[1, table]
        
        if ( TD:count() ) {
            local T = TD[1, table] 
            if ( T["ptype", number] == 1 ) {
                local D = T["data", table]
                
                local Col = vec(255,255,255)
                
                if ( D["code", number] == 1 ) {               
                    message_print(vec(170,170,25), "NOTICE", D["msg", string])
                } elseif ( D["code", number] == 9 ) {  
                    message_print(vec(15,150,250), "ONLINE", D["msg", string]) 
                    ONLINE = 1
                } else {                
                    message_print_b(vec(255,255,255),  D["msg", string])
                }
            } else {
                print("WARNING: payload type not recognized")
            }
            
            TD:remove(1)
        } else {            
            IN_DATA_MSG:remove(1)            
        }
    }  elseif ( MSG_Q:count() ) {
        local TD = MSG_Q[1, table]
        
        printColor(TD["col", vector], TD["msg", string])
        MSG_Q:remove(1)
    }    
} elseif ( clk("rtimeout")) {
    if ( Running ) {
        printColor(vec(175,35,25),"WARNING:",vec(255,255,255)," HTTP request timed out")
        Running = 0
    } 
    
} elseif ( chatClk(owner()) ) {
    local R = owner():lastSaid():explode(" ")
        
    if ( R[1,string]:sub(1,1) == "!" ) {
        hideChat(1)
        local CMD  = R[1,string]:sub(2)
        if ( CMD == "chat" ) {
            if ( R[2,string] == "bc" ) {
                if ( Owner["chat_mode", number] == 2 ) {
                    Owner["chat_mode", number] = 1
                } else {
                    Owner["chat_mode", number] = 2
                }
            } else {
                if ( Owner["chat_mode", number] == 0 ) {
                    Owner["chat_mode", number] = 1 
                } else {
                    Owner["chat_mode", number] = 0
                }                
            }
            notify_ecomm_mode(0)        
        } elseif ( CMD == "ping" ) {  
            if ( R[2,string]:length() ) {          
                push_to_sendq(R[2,string], 1, 2, 0, "global", "") 
                message_print(vec(170,170,25), "NOTICE", "Pinging " + R[2,string])
            }
        } elseif ( CMD == "gping" ) {            
            push_to_sendq("", 1, 3, 0, "global", "") 
            message_print(vec(170,170,25), "NOTICE", "Sending broadcast ping..")
        } elseif ( CMD == "cmdlist" ) {
            print_help()
        }
    } elseif ( Owner["chat_mode", number] & 
                Chip["channel", string] ) {        
        local LS = owner():lastSaid()
        if ( Owner["chat_mode", number] != 2 ) {
            hideChat(1)
            message_print_in(vec(25,175,2), owner():name(), Chip["channel", string], LS)        
        }
        
        push_to_sendq(LS, 1, 0, 0, Chip["channel", string], "")
    } elseif ( R[1,string]:sub(1,1) == "." &
                R[1,string]:sub(2,2) != "." &
                 R[1,string]:length()  > 1 &
                    Chip["channel", string] ) {
        hideChat(1)
        local S = owner():lastSaid():sub(2)
        if ( S:length() ) {
            message_print_in(vec(25,175,2), owner():name(), Chip["channel", string], S)      
            push_to_sendq(S, 1, 0, 0, Chip["channel", string], "")
        }
    } elseif ( R[1,string]:sub(1,1) == "-" &
                R[1,string]:sub(2,2) != "-" &
                 R[1,string]:length()  > 1 ) {
        hideChat(1)
        local R = owner():lastSaid():sub(2):explode(" ")
        if ( R:count() == 2 ) {
            if ( R[2,string]:length() ) {
                message_print_out_p(vec(25,175,2), owner():name(), R[1,string], R[2,string])      
                push_to_sendq(R[2,string], 1, 17, 0, "pvdummy", R[1,string])
            }
        }
    } 
} elseif ( chatClk() ) { 
    if ( Mode ) {
        local LS = lastSpoke():lastSaid() 
        local LN = lastSpoke():name()
        ds_hud_send_message_p("[ " + LN + " ] " + LS,  1, Owner) 
        if ( Mode == 2 ) { # don't use this mode
            push_to_sendq("[ " + LN + " ] "  + LS, 1, 0, 0, Chip["channel", string], "")
        }
    }
} elseif ( signalClk() & signalSender():owner() == entity():owner()) {
    if ( signalClk("SIGREEC") )  {
        reset()   
    }
}

