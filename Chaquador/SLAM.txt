@name SLAM
@persist E:entity
@model models/weapons/w_slam.mdl
if(first())
{
    runOnTick(1)
    
    E = entity()
    
    holoCreate(1)
    holoModel(1,"hq_cylinder")
    holoColor(1,vec4(255,0,0,50))
}

RD = rangerOffset(100,E:pos(),E:up())

holoPos(1,(RD:pos()+E:pos())/2)

holoAng(1,E:angles())

holoScale(1,vec(0.05,0.05,RD:distance()/12))

if(RD:entity():health()>0|RD:entity():isVehicle())
{
    propSpawn("models/props_phx/amraam.mdl",E:pos(),1):propBreak()
    selfDestruct()
}
