@name Mr. Oversized Chess Piece
@persist Walk Jump Ded S Diff
@persist [V O]:vector
@persist [A]:angle
@persist [T E]:entity
@persist Sound:array
@model models/props_phx/games/chess/black_knight.mdl
if(first())
{
    entity():propFreeze(0)
    
    runOnTick(1)
    runOnChat(1)
    
    timer("s",100)
    timer("snd",2000)
    
    E = entity()
    
    Diff = 25
    
    T = owner()
    
    Sound[1,string]="/vo/heavy_award01.wav"
    Sound[2,string]="/vo/heavy_award02.wav"
    Sound[3,string]="/vo/heavy_award03.wav"
    Sound[4,string]="/vo/heavy_award04.wav"
    Sound[5,string]="/vo/heavy_award05.wav"
    Sound[6,string]="/vo/heavy_award06.wav"
    Sound[7,string]="/vo/heavy_award07.wav"
    Sound[8,string]="/vo/heavy_award08.wav"
    Sound[9,string]="/vo/heavy_award09.wav"
    Sound[10,string]="/vo/heavy_award010.wav"
    Sound[11,string]="/vo/heavy_award011.wav"
    Sound[12,string]="/vo/heavy_award012.wav"
    Sound[13,string]="/vo/heavy_award013.wav"
    Sound[14,string]="/vo/heavy_award014.wav"
    Sound[15,string]="/vo/heavy_award015.wav"
    Sound[16,string]="/vo/heavy_award016.wav"
    Sound[17,string]="/vo/heavy_battlecry01.wav"
    Sound[18,string]="/vo/heavy_battlecry02.wav"
    Sound[19,string]="/vo/heavy_battlecry03.wav"
    Sound[20,string]="/vo/heavy_battlecry04.wav"
    Sound[21,string]="/vo/heavy_battlecry05.wav"
    Sound[22,string]="/vo/heavy_battlecry06.wav"
    Sound[23,string]="/vo/heavy_cheers01.wav"
    Sound[24,string]="/vo/heavy_cheers02.wav"
    Sound[25,string]="/vo/heavy_cheers03.wav"
    Sound[26,string]="/vo/heavy_cheers04.wav"
    Sound[27,string]="/vo/heavy_cheers05.wav"
    Sound[28,string]="/vo/heavy_cheers06.wav"
    Sound[29,string]="/vo/heavy_cheers07.wav"
    Sound[30,string]="/vo/heavy_cheers08.wav"
    Sound[31,string]="/vo/heavy_cloakedspyidentify07.wav"
    Sound[32,string]="/vo/heavy_cloakedspy01.wav"
    Sound[33,string]="/vo/heavy_cloakedspy02.wav"
    Sound[34,string]="/vo/heavy_cloakedspy03.wav"
    Sound[35,string]="/vo/heavy_cloakedspy04.wav"
    Sound[36,string]="/vo/heavy_generic01.wav"
    Sound[37,string]="/vo/heavy_medicfollow01.wav"
    Sound[38,string]="/vo/heavy_medicfollow02.wav"
    Sound[39,string]="/vo/heavy_medicfollow03.wav"
    Sound[40,string]="/vo/heavy_medicfollow04.wav"
    Sound[41,string]="/vo/heavy_medicfollow05.wav"
    Sound[42,string]="/vo/heavy_medicfollow06.wav"
    Sound[43,string]="/vo/heavy_medicfollow07.wav"
    Sound[44,string]="/vo/heavy_sandwichtaunt01.wav"
    Sound[45,string]="/vo/heavy_sandwichtaunt02.wav"
    Sound[46,string]="/vo/heavy_sandwichtaunt03.wav"
    Sound[47,string]="/vo/heavy_sandwichtaunt04.wav"
    Sound[48,string]="/vo/heavy_sandwichtaunt05.wav"
    Sound[49,string]="/vo/heavy_sandwichtaunt06.wav"
    Sound[50,string]="/vo/heavy_sandwichtaunt07.wav"
    Sound[51,string]="/vo/heavy_sandwichtaunt08.wav"
    Sound[52,string]="/vo/heavy_sandwichtaunt09.wav"
    Sound[53,string]="/vo/heavy_sandwichtaunt10.wav"
    Sound[54,string]="/vo/heavy_sandwichtaunt11.wav"
    Sound[55,string]="/vo/heavy_sandwichtaunt12.wav"
    Sound[56,string]="/vo/heavy_sandwichtaunt13.wav"
    Sound[57,string]="/vo/heavy_sandwichtaunt14.wav"
    Sound[58,string]="/vo/heavy_sandwichtaunt15.wav"
    Sound[59,string]="/vo/heavy_sandwichtaunt16.wav"
    Sound[60,string]="/vo/heavy_sandwichtaunt17.wav"
    Sound[61,string]="/vo/heavy_thanksfortheheal01.wav"
    Sound[62,string]="/vo/heavy_thanksfortheheal02.wav"
    Sound[64,string]="/vo/heavy_thanksfortheheal03.wav"
    Sound[65,string]="/vo/heavy_specialcompleted01.wav"
    
    function entity:turnTo(Tarang:angle){
        V = This:toLocal(rotationVector(quat(Tarang)/quat(This))+This:pos())
        This:applyTorque((50*V - 10*This:angVelVector())*This:inertia()/2)
    }
    function entity:goto(Vector:vector){
        This:applyForce(((Vector-This:pos())*25-entity():vel())*entity():mass())   
    }
}

if(clk("s"))
{
    stoptimer("s")
    S = 1
}

if(S&&!Ded)
{
    if(chatClk(owner()))
    {
        LS = owner():lastSaid():lower():explode(" ")
        if(LS[1,string]=="!t")
        {
            hideChat(1)
            local TE = findPlayerByName(LS[2,string])
            if(TE:isValid())
            {
                T = TE
                hint("Found "+T:name()+".",3)
            }else
            {
                hint("Player not found.",3)
            }
        }
    }
    
    RD = rangerOffset(Diff,E:pos(),vec(0,0,-1))
    rangerFilter(T)
    JR = rangerOffset(100,E:pos()-vec(0,0,3),E:forward():setZ(0))
    Jump = changed(JR:hit())&JR:hit()&RD:hit()
    
    if(Jump)
    {
        applyForce(vec(0,0,45000)+E:forward()*5*E:mass())
        soundPlay("jmp",1.5,"mvm/giant_common/giant_common_step_0"+randint(1,8)+".wav")
    }
    
    if(T:isValid())
    {
        Walk = E:pos():distance(T:pos())>75
        O = E:forward()*Walk*clamp(E:pos():distance(T:pos())/60,7.5,50)
        A = (T:pos()-E:pos()):normalized():toAngle():setPitch(0)
    }
    
    V = O+(RD:pos()+vec(0,0,Diff))
    
    if(RD:hit()&!Jump)
    {
        E:goto(V)
        E:turnTo(A)
    }
    
    if(clk("snd"))
    {
        stoptimer("snd")
        local Str = Sound[randint(Sound:count()),string]
        soundPlay("snd",5,Str)
        timer("snd",random(5000,15000))
    }
    
    Ded = !inrange(E:angles(),ang(-95,-360,-95),ang(95,360,95))
}

if(changed(Ded)&Ded)
{
    soundStop("snd")
    soundPlay("ded",5,"vo/heavy_paincrticialdeath0"+randint(1,3)+".wav")
    timer("ded",300)
}

if(clk("ded"))
{
    stoptimer("ded")
    B = propSpawn("models/props_phx/amraam.mdl",E:pos(),1)
    B:propBreak()
    selfDestruct()
}
