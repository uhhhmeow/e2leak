@name STOP
@persist N T:entity
if(first())
{
    runOnChat(1)
    runOnTick(1)
    
    holoCreate(1,entity():toWorld(vec(0,0,30)))
    holoAng(1,ang(0,-90,0))
    holoAlpha(1,0)
    
    holoCreate(2,holoEntity(1):toWorld(vec(0,15,0)))
    holoModel(2,"models/sprops/misc/alphanum/alphanum_f.mdl")
    holoColor(2,vec(255,0,0))
    holoDisableShading(2,1)
    holoMaterial(2,"models/debug/debugwhite")
    holoParent(2,1)
    
    holoCreate(3,holoEntity(1):toWorld(vec(0,5,0)))
    holoModel(3,"models/sprops/misc/alphanum/alphanum_a.mdl")
    holoColor(3,vec(255,0,0))
    holoDisableShading(3,1)
    holoMaterial(3,"models/debug/debugwhite")
    holoParent(3,1)
    
    holoCreate(4,holoEntity(1):toWorld(vec(0,-5,0)))
    holoModel(4,"models/sprops/misc/alphanum/alphanum_g.mdl")
    holoColor(4,vec(255,0,0))
    holoDisableShading(4,1)
    holoMaterial(4,"models/debug/debugwhite")
    holoParent(4,1)
    
    holoCreate(5,holoEntity(1):toWorld(vec(0,-15,0)))
    holoModel(5,"models/sprops/misc/alphanum/alphanum_t.mdl")
    holoColor(5,vec(255,0,0))
    holoDisableShading(5,1)
    holoMaterial(5,"models/debug/debugwhite")
    holoParent(5,1)
}

if(T:isValid())
{
    holoPos(1,T:shootPos()+T:eye()*50)
    holoAng(1,T:eyeAngles())
}else
{
    N++
    holoAng(1,ang(0,N,0))
}

if(chatClk(owner()))
{
    LS = owner():lastSaid():lower():explode(" ")
    if(LS[1,string]=="-t")
    {
        hideChat(1)
        local TE = findPlayerByName(LS[2,string])
        if(TE:isValid())
        {
            T = TE
            hint("Found "+T:name(),3)
            for(I=1,5)
            {
                #holoVisible(I,players(),0)
                holoVisible(I,T,1)
            }
        }else
        {
            hint("Player not found.",3)
        }
    }
}
