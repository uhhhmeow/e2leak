@name The Pyro
@persist Walk Jump AllowMore Diff Spawned Dead Owned FootStepRate #Numbers
@persist [O]:vector #Vectors
@persist [Target]:entity #Entities
@persist [FootSteps Sounds]:array #Arrays
@persist [Weapon]:string #Strings
@model models/props_borealis/bluebarrel001.mdl
if(first()){
#Array handling
 FootSteps[1,string]="/mvm/player/footsteps/robostep_01.wav"
 FootSteps[2,string]="/mvm/player/footsteps/robostep_02.wav"
 FootSteps[3,string]="/mvm/player/footsteps/robostep_03.wav"
 FootSteps[4,string]="/mvm/player/footsteps/robostep_04.wav"
 FootSteps[5,string]="/mvm/player/footsteps/robostep_05.wav"
 FootSteps[6,string]="/mvm/player/footsteps/robostep_06.wav"
 FootSteps[7,string]="/mvm/player/footsteps/robostep_07.wav"
 FootSteps[8,string]="/mvm/player/footsteps/robostep_08.wav"
 FootSteps[9,string]="/mvm/player/footsteps/robostep_09.wav"
 FootSteps[10,string]="/mvm/player/footsteps/robostep_10.wav"
 FootSteps[11,string]="/mvm/player/footsteps/robostep_11.wav"
 FootSteps[12,string]="/mvm/player/footsteps/robostep_12.wav"
 FootSteps[13,string]="/mvm/player/footsteps/robostep_13.wav"
 FootSteps[14,string]="/mvm/player/footsteps/robostep_14.wav"
 FootSteps[15,string]="/mvm/player/footsteps/robostep_15.wav"
 FootSteps[16,string]="/mvm/player/footsteps/robostep_16.wav"
 FootSteps[17,string]="/mvm/player/footsteps/robostep_17.wav"
 FootSteps[18,string]="/mvm/player/footsteps/robostep_18.wav"
    
 entity():setColor(0,0,0,0)
 entity():propFreeze(0)
 runOnTick(1)   
 Diff = 52
 FootStepRate = 312.5
 Weapon = "SECONDARY"

 timer("start",250)

 holoCreate(1)
 holoModel(1,"models/bots/pyro/bot_pyro.mdl")
 holoPos(1,entity():toWorld(vec(0,0,-Diff)))
 holoParent(1,entity())
 holoAnim(1,"Stand_"+Weapon)
 holoSkin(1,round(random(0,1)))
 
 holoCreate(2)
 holoModel(2,"models/weapons/w_models/w_shotgun.mdl")
 holoMaterial(2,"models/player/shared/gold_player")
 holoPos(2,holoEntity(1):attachmentPos("weapon_bone"))
 holoAng(2,holoEntity(1):attachmentAng("weapon_bone")-ang(5,90,90))
 holoParentAttachment(2,holoEntity(1),"weapon_bone")

 soundPlay(2,0,"/mvm/giant_pyro/giant_pyro_loop.wav")

 timer("startup",250)

if(hostname():find("TylerB")|isSinglePlayer()){
 AllowMore = 1 
}else{AllowMore=1}
 if(!AllowMore){
 setName("@pyro_spawned")
 findByClass(entity():type())
 E2s = findToArray()
 for(I=1,E2s:count()){
 E2 = E2s[I,entity]
 if(E2:getName()=="@pyro_spawned"&E2:owner()==owner()){
 print("[PyroBot #"+entity():id()+"]: You already have a pyro spawned")
 selfDestruct()
 }   
 }
}

function entity:turnTo(Tarang:angle){
V = This:toLocal(rotationVector(quat(Tarang)/quat(This))+This:pos())
This:applyTorque((100*V - 30*This:angVelVector())*This:inertia()/3)
}
function entity:goto(Vector:vector){
 This:applyForce(((Vector-This:pos())*25-This:vel())*This:mass())   
}

}

if(clk("start")){
 stoptimer("start")
 Spawned = 1   
}

if(Spawned){
 if(!Dead){
 #Timers
 if(clk("snd")){
    stoptimer("snd")
    timer("snd",random(5000,7500))
    soundPlay(2,0,Sounds[round(random(1,Sounds:count())),string])
 }

 #Entity Detection
 if(!Owned){
    if(findCanQuery()){
        findByClass("player")
        findSortByDistance(entity():pos())
        if(find():pos():distance(entity():pos())<375){
         Owned = 1
         Target = find()
         print("[PyroBot #"+entity():id()+"]: found target, "+Target:name())   
        }
    }
 }

 #Death handling
 if(entity():isOnFire()|entity():angles():pitch()>85||entity():angles():pitch()<-85||entity():angles():roll()>85||entity():angles():roll()<-85|entity():isUnderWater()){
  Dead = 1   
 }
    
 #Rangers, applefork, and angling
 Ranger = rangerOffset(Diff,entity():pos(),vec(0,0,-1))
 if(Ranger:hit()&!Jump){
  V = Ranger:pos()+vec(0,0,Diff)+O
  if(Owned){
    Ang = ang(0,(Target:pos()-entity():pos()):toAngle():yaw(),0)
 }else{Ang = ang(0,entity():angles():yaw(),0)}
 entity():goto(V)
 entity():turnTo(Ang)
 }
 
 if(changed(Ranger:hit())){
    if(Ranger:hit()){
     soundPlay(3,0,FootSteps[round(random(1,FootSteps:count())),string])
    }
    if(!Ranger:hit()){
     
    }
 }

 #Walking & holoSetPose(INDEX,STRING,NUMBER)
 if(Owned){
    Distance = entity():pos():distance(Target:pos())
    if(Distance>100){
     Walk = 1   
    }else{ Walk = 0 }
    
    if(changed(Walk)){
     if(Walk){
        holoAnim(1,"Run_"+Weapon)
        timer("step1",FootStepRate)
    }
     if(!Walk){
        holoAnim(1,"Stand_"+Weapon)   
        stoptimer("step1")
        stoptimer("step2")
        stoptimer("step3")
    }   
    }
    
    if(Walk){
    holoSetPose(1,"move_x",entity():vel():length()/250)
    holoSetPose(1,"move_scale",entity():vel():length()/250)
    O = entity():forward()*10
    if(clk("step1")){
    stoptimer("step1")
    timer("step2",FootStepRate)
    if(Ranger:hit()){
    soundPlay(10,0,FootSteps[round(random(1,FootSteps:count())),string]) 
    }  
    }
    if(clk("step2")){
    stoptimer("step2")
    timer("step3",FootStepRate)
    if(Ranger:hit()){
    soundPlay(11,0,FootSteps[round(random(1,FootSteps:count())),string])
    }
    }
    if(clk("step3")){
    stoptimer("step3")
    timer("step1",FootStepRate)
    if(Ranger:hit()){
    soundPlay(12,0,FootSteps[round(random(1,FootSteps:count())),string])
    }
    }
    }else{O=entity():forward()*0}
 }

 #Jumping
 rangerFilter(players())
 FRang = rangerOffset(100,Ranger:pos()+vec(0,0,30),entity():forward())
 if(FRang:hit()){
    Jump = 1
 }else{Jump = 0}

 if(changed(Jump)){
    if(Jump){
     soundPlay(4,0,"/mvm/sentrybuster/mvm_sentrybuster_step_0"+round(random(1,4))+".wav")   
     applyForce(entity():up()*35000)
    }
 } 

 #Extra
 

}
if(changed(Dead)){
 if(Dead){
    setName("@pyro_dead")
    stoptimer("snd")
    soundStop(1)
    soundStop(2)
    soundStop(3)
    soundPlay(5,0,"/vo/mvm/norm/pyro_mvm_paincrticialdeath0"+round(random(1,3))+".mp3")
    holoSetPose(1,"mode_scale",0)
    holoAnim(1,"dieviolent",0,0.5)
    timer("delete",3000)
    holoDelete(2)
    holoCreate(3)
    holoEntity(3):removeTrails()
    holoModel(3,"models/buildables/sapper_dispenser.mdl")
    holoAlpha(3,0)
    holoParent(3,entity())
    holoCreate(4)
    holoModel(4,"models/buildables/sapper_sentry3.mdl")
    holoPos(4,holoEntity(3):toWorld(vec(0,0,-10)))
    holoAlpha(4,0)
    holoParent(4,entity())     
}   
}
   
if(Dead){
  if(clk("delete")){
 B=propSpawn("models/props_phx/ww2bomb.mdl",entity():pos(),1)
 B:propBreak()
 selfDestruct()  
}    
}
}

if(duped()||dupefinished())
{reset()}
