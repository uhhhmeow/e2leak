@name Balloonicorn
@persist E:entity A:angle
@model models/hunter/blocks/cube025x05x025.mdl
if(first())
{
    holoCreate(1, entity():toWorld(vec(2 - entity():radius() / 2, 2.5, -3)))
    holoModel(1, "models / player / items / pyro / taunticorn.mdl")
    holoSkin(1, randint(1, 2))
    holoAng(1, entity():toWorld(ang(0, 90, 0)))
    holoParent(1, entity())
    entity():setColor(0, 0, 0, 0)
    entity():propGravity(0)
    entity():propFreeze(0)
    entity():setMass(5)
    A = ang(random(-3, 3), random(-3, 3), random(-3, 3))
    applyAngForce(A * 50)
    function entity:slowGo(V:vector)
    {
        This:applyForce(clamp((V - This:pos()), vec(-100), vec(100)) * 0.025-This:vel() * This:mass() * 0.0125)
    }
    runOnTick(1)
    E = players()[random(numPlayers()), entity]
    timer("new", random(50000, 120000))
    timer("a", 500)
    soundPlay("lel", 60.6, "items / tf_music_upgrade_machine.wav")
    timer("hue", 60400)
}
if(clk("new")||E:shootPos():distance(entity():pos())<75||!E:isValid())
{
    stoptimer("new")
    E = players()[random(numPlayers()), entity]
    timer("new", random(50000, 300000))
}
entity():slowGo(E:shootPos())
if(clk("a"))
{
    stoptimer("a")
    timer("a", 500)
    A = ang(random(-1, 1), random(-1, 1), random(-1, 1)) * 10
    applyAngForce(A)
}
if(clk("hue"))
{
    stoptimer("hue")
    timer("hue", 60600)
    soundStop("hue")
    soundPlay("lel", 60.6, "items / tf_music_upgrade_machine.wav")
}
