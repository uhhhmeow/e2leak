@name Chaqua Heli
@inputs [EP EGP]:wirelink
@persist Dri:entity Act W A S D R KZm Spc Alt Shift M1 M2
@persist [E P ATg M]:entity
@persist ISt IMl FMl FMs Lnk Zm ETN BMO
@model models/hunter/plates/plate1x1.mdl
if(first())
{
    function number entity:cm()
    {
        local Mass = 0
        for(I=1,This:getConstraints():count())
        {
            Mass+=This:getConstraints()[I,entity]:mass()
        }
        return This:mass()+Mass
    }
    
    function number angle:nearBy(A:angle,R)
    {
        return inrange(angnorm(round(This-A)),ang(-R),ang(R))
    }
    
    function camCreate(Var)
    {
        if(Var)
        {
            cameraCreate(1,Dri:shootPos()+E:up()*10)
            cameraParent(1,E)
            cameraToggle(1,1,P)
        }else
        {
            cameraToggle(1,0,P)
            cameraRemove(1)
        }
    }
    
    soundPlay("Ign",0,"NPC_AttackHelicopter.Rotors")
    soundPitch("Ign",0)
    
    runOnTick(1)
    E = entity()
    
    E:propFreeze(0)
    print(4,toString(E:mass()))
    E:setMass(2000)
    print(E:mass())
    FMs = E:cm()
    
    FMl = 8.35
    #FMl = 15.25
    
    Zm = 80
    
    holoCreate(1,E:toWorld(vec(0,0,100)),vec(0.65,0.075,22),E:toWorld(ang(0,0,90)),vec(50))
    holoModel(1,"hq_cylinder")
    holoParent(1,E)
    
    holoCreate(4,holoEntity(1):toWorld(vec()),vec(50,50,0.005))
    holoModel(4,"hq_cylinder")
    holoMaterial(4,"models/airboat/airboat_blur02")
    holoParent(4,E)
    
    holoCreate(2,E:toWorld(vec(-250,-2,35)),vec(0.35,0.075,10),E:toWorld(ang(90,0,0)),vec(50))
    holoModel(2,"hq_cylinder")
    holoParent(2,E)
    
    holoCreate(5,holoEntity(2):toWorld(vec()),vec(10,10,0.005),E:toWorld(ang(0,0,90)))
    holoModel(5,"hq_cylinder")
    holoMaterial(5,"models/airboat/airboat_blur02")
    holoParent(5,E)
    
    holoCreate(3,E:toWorld(vec(0,0,5)))
    holoAng(3,E:toWorld(ang(0,-90,0)))
    holoAlpha(3,150)
    holoModel(3,"models/nova/airboat_seat.mdl")
    holoParent(3,E)
}

if(changed(E:cm()))
{
    FMs = E:cm()
}

if(Lnk)
{
    Dri = P:driver()
    Act = P:driver():isValid()
    W = Dri:keyForward()
    A = Dri:keyLeft()
    S = Dri:keyBack()
    D = Dri:keyRight()
    R = Dri:keyReload()
    KZm = Dri:keyZoom()
    Spc = Dri:keyJump()
    Alt = Dri:keyWalk()
    Shift = Dri:keySprint()
    M1 = Dri:keyAttack1()
    M2 = Dri:keyAttack2()
	
    if(Act)
    {
        if(changed(R)&R)
        {
            ISt = !ISt
            EP["Enable",number] = ISt
            camCreate(ISt)
        }
    }
    if(changed(Act))
    {
        camCreate(Act&ISt)
    }
    if(M2)
    {
        cameraAng(1,Dri:eyeAngles())
    }else
    {
        cameraAng(1,E:angles())
    }
    cameraZoom(1,90-(Zm*KZm))
    
    if(ISt)
    {
        IMl += 0.00125
        EP["Enable",number] = !M2
    }else
    {
        IMl -= 0.00125
    }
    IMl = clamp(IMl,0,1)
    if(changed(IMl))
    {
        holoAlpha(4,IMl*255)
        holoAlpha(5,IMl*255)
    }

    holoAng(1,holoEntity(1):toWorld(ang(IMl*100,0,0)))
    holoAng(2,holoEntity(2):toWorld(ang(IMl*50,0,0)))
    holoAng(4,holoEntity(4):toWorld(ang(0,IMl*-10,0)))
    holoAng(5,holoEntity(5):toWorld(ang(0,IMl*10,0)))

    applyForce(((E:up()*(FMl+((W-S)*(1+Shift)*2)))-(E:vel()*(0.0025*(1+((BMO|Spc)*5)))))*FMs*IMl)

    Ang1 = ang(EP["Y",number]*1,0,EP["X",number]*1)
    Ang2 = -E:angles():setYaw(0)*0.75-(E:angVel()/5)
    
    if(E:angVelVector():length()>300)
    {
        P:printDriver(4,"Warning: Fast angular velocity detected! Automatic balance activated.")
        BMO = 1
        timer("BMO-off",2000)
    }
    
    if(changed(BMO))
    {
        if(BMO)
        {
            soundPlay("BMO-on",0.71,"hl1/fvox/warning.wav")
        }else
        {
            soundPlay("BMO-off",0.7,"hl1/fvox/bell.wav")
        }
    }
    
    if(clk("BMO-off"))
    {
        stoptimer("BMO-off")
        BMO = 0
    }
    
    applyAngForce(((Ang1+(Ang2*(Spc|BMO|!Act))+ang(0,(A-D)*8,0))-E:angVel()/15)*FMs*IMl)

    soundPitch("Ign",IMl*115)
    
    if(EGP:entity():isValid())
    {
        if(changed(EGP:entity():isValid()))
        {
            timer("tick",250)
        }
        
        if(clk("tick"))
        {
            stoptimer("tick")
            timer("tick",250)
            
            EGP:egpClear()
            
            ETN+=100
            Alw = 1
            
            if(findCanQuery())
            {
                findByClass("player")
                findSortByDistance(Dri:shootPos()+E:up()*10)
                Ar = findToArray()
            }
            
            foreach(K, V:entity = Ar)
            {
                local I = K*10
                local I2 = I+1
                local I3 = I+2
                if(V != Dri)
                {
                    EGP:egp3DTracker(I,V:boxCenterW())
                    EGP:egpTriangleOutline(I2,vec2(cos(ETN)*20,sin(ETN)*20),vec2(cos(ETN+120)*20,sin(ETN+120)*20),vec2(cos(ETN+240)*20,sin(ETN+240)*20))
                    EGP:egpText(I3,V:name(),EGP:egpPos(I)+vec2(0,25))
                    EGP:egpAlign(I3,1)
                    if((V:boxCenterW()-(Dri:shootPos()+E:up()*10)):toAngle():nearBy(E:angles(),10))
                    {
                        if(V:team()==Dri:team())
                        {
                            EGP:egpColor(I2,vec(0,0,255))
                            EGP:egpColor(I3,vec(0,0,255))
                        }else
                        {
                            if(Alw)
                            {
                                EGP:egpColor(I2,vec(255,0,0))
                                EGP:egpColor(I3,vec(255,0,0))
                                ATg = V
                            }else
                            {
                                EGP:egpColor(I2,vec(255,255,0))
                                EGP:egpColor(I3,vec(255,255,0))
                            }
                            Alw = 0
                        }
                    }else
                    {
                        if(V:team()==Dri:team())
                        {
                            EGP:egpColor(I2,vec(0,0,255))
                            EGP:egpColor(I3,vec(0,0,255))
                        }else
                        {
                            EGP:egpColor(I2,vec(0,255,0))
                            EGP:egpColor(I3,vec(0,255,0))
                        }
                    }
                    EGP:egpParent(I2,I)
                    EGP:egpParent(I3,I)
                }
            }
        }
    }
    
    if(ATg:isValid())
    {
        if(M:isValid())
        {
            M:setPos(M:toWorld(vec(15,0,0)))
            local A = M:toLocal((ATg:boxCenterW()-M:pos()):toAngle())
            M:setAng(M:toWorld(A/10))
            if(rangerOffset(60,M:pos(),M:forward()):hit()|M:pos():distance(ATg:boxCenterW())<40)
            {
                M:propBreak()
            }
        }else
        {
            if(changed(M1)&M1)
            {
                M = propSpawn("models/props_phx/ww2bomb.mdl",E:toWorld(vec(0,0,60)),1)
                M:setTrails(10,10,10,"trails/smoke",vec(200),255)
            }
        }
    }
}else
{
    if(findCanQuery())
    {
        findByClass("prop_vehicle_prisoner_pod")
        findSortByDistance(E:pos())
        if(find():pos():distance(E:pos())<175&&find():owner()==owner()&&find():model()=="models/nova/airboat_seat.mdl")
        {
            P = find()
            P:setPos(holoEntity(3):pos())
            P:setAng(holoEntity(3):angles())
            P:propFreeze(1)
            timer("pr",15)
        }
    }
    if(clk("pr"))
    {
        stoptimer("pr")
        P:parentTo(E)
        holoDelete(3)
        Lnk = 1
    }
}
