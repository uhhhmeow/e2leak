@name Flappy Bird
@persist [B E P]:entity C:wirelink
@persist N Plg
@persist Dri:entity Act W A S D R KZm Spc Alt Shift M1 M2
@persist PlA FlV FlA GmO CCl MVl
if(first())
{
    runOnTick(1)
    
    E = entity()
    E:setColor(0,0,0,0)
    
    P = E:isConstrainedTo()
    if(!P:isVehicle())
    {
        print(4, "Please spawn this chip on a vehicle.")
        selfDestruct()
    }
    
    #Base Body
    B = holoCreate(10, P:toWorld(vec(0, 65, 20)), vec(1.5,1.2,1.2), P:toWorld(ang(0, 90, 0)):setPitch(0):setRoll(0))
    holoModel(10, "hq_sphere")
    local R = array(vec(255, 255, 75), vec(255, 75, 75), vec(75, 75, 255), vec(255, 175, 0), vec())
    CCl = randint(1, 5)
    holoColor(10, R[CCl, vector])
    
    #Right Wing Base
    holoCreate(20, B:toWorld(vec(-3.7, -5, 0)), vec(), B:toWorld(ang()))
    holoParent(20, 10)
    
    #Right Wing Feathers
    holoCreate(21,holoEntity(20):toWorld(vec(0,-2,0)), vec(1.25, CCl == 5 ? 1 : 0.7, 0.15), B:toWorld(ang(5,15,0)))
    holoModel(21, "hq_rcylinder_thick")
    local R = array(vec(255, 255, 230), vec(255, 230, 230), vec(230, 230, 255), vec(255, 225, 175), vec())
    holoColor(21, R[CCl, vector])
    holoParent(21, 20)
    
    #Left Wing Base
    holoCreate(30,B:toWorld(vec(-3.7, 5, 0)), vec(), B:toWorld(ang()))
    holoParent(30, 10)
    
    #Left Wing Feathers
    holoCreate(31,holoEntity(30):toWorld(vec(0,2,0)), vec(1.2, CCl == 5 ? 1 : 0.7, 0.15), B:toWorld(ang(5,-15,0)))
    holoModel(31, "hq_rcylinder_thick")
    local R = array(vec(255, 255, 230), vec(255, 230, 230), vec(230, 230, 255), vec(255, 225, 175), vec())
    holoColor(31, R[CCl, vector])
    holoParent(31, 30)
    
    #Right Eye White
    holoCreate(40, B:toWorld(vec(5.5, -2, 2.5)), vec(0.5), B:toWorld(ang()))
    holoModel(40, "hq_sphere")
    holoParent(40, 10)
    
    #Right Eye Black
    holoCreate(41, holoEntity(40):toWorld(vec(1,-0.5,0.5)), vec(0.35), B:toWorld(ang()), vec())
    holoModel(41, "hq_sphere")
    holoParent(41, 40)
    
    #Left Eye White
    holoCreate(50, B:toWorld(vec(5.5, 2, 2.5)), vec(0.5), B:toWorld(ang()))
    holoModel(50, "hq_sphere")
    holoParent(50, 10)
    
    #Left Eye Black
    holoCreate(51, holoEntity(50):toWorld(vec(1,0.5,0.5)), vec(0.35), B:toWorld(ang()), vec())
    holoModel(51, "hq_sphere")
    holoParent(51, 50)
    
    #Lip Color Array
    local R = array(vec(255, 150, 0), vec(255, 150, 0), vec(255, 100, 0), vec(255, 0, 0), vec(255, 0, 0))
    
    #Bottom Lip
    holoCreate(60, B:toWorld(vec(4, 0, -1)), vec(1, 1, 0.2), B:toWorld(ang(5, 0, 0)), R[CCl, vector])
    holoModel(60, "hq_sphere")
    holoParent(60, 10)
    
    #Top Lip
    holoCreate(70, B:toWorld(vec(4, 0, -0.5)), vec(1, 1, 0.2), B:toWorld(ang(5, 0, 0)), R[CCl, vector])
    holoModel(70, "hq_sphere")
    holoParent(70, 10)

    for(I=10,70)
    {
        holoDisableShading(I,1)
    }
}

N+=30
holoAng(20,B:toWorld(ang(0, 0, cos(N) * 45)))
holoAng(30,B:toWorld(ang(0, 0, -cos(N) * 45)))

Dri = P:driver()
Act = P:driver():isValid()
W = Dri:keyForward()
A = Dri:keyLeft()
S = Dri:keyBack()
D = Dri:keyRight()
R = Dri:keyReload()
KZm = Dri:keyZoom()
Spc = Dri:keyJump()
Alt = Dri:keyWalk()
Shift = Dri:keySprint()
M1 = Dri:keyAttack1()
M2 = Dri:keyAttack2()

if(Plg)
{
    if(Alt)
    {
        cameraPos(1, Dri:shootPos()+vec(0, 0, 10))
        cameraAng(1, (B:pos() - (Dri:shootPos()+vec(0, 0, 10))):toAngle())
    }else
    {
        cameraPos(1, B:pos() - Dri:eye()*100)
        cameraAng(1, Dri:eyeAngles())
    }
    
    Jmp = (changed(Spc)&Spc)|(changed(M1)&M1)
    
    if(PlA)
    {
        if(GmO)
        {
            if(clk("rst"))
            {
                reset()
            }
        }else
        {
            local R = rangerOffset(B:pos() - vec(15), B:pos() + vec(15))
            
            if(R:hit())
            {   
                P:printDriver(4,"Game over!")
                timer("rst",3000)
                FlV = 0
                GmO = 1
            }else
            {
                if(Jmp)
                {
                    if(FlV < 5)
                    {
                        FlV = CCl == 5 ? 10 : 5
                    }
                    B:soundPlay("jmp", 0.26, "ui/hint.wav")
                    soundPitch("jmp", 50)
                }else
                {
                    FlV -= 0.25
                }
                FlV = clamp(FlV, -50, 25)
                
                FlA -= FlV*3
                FlA = clamp(FlA, -35, 90)
                
                holoPos(10, B:pos() + vec(cos(B:angles():yaw()) * MVl, sin(B:angles():yaw()) * MVl, FlV))
                holoAng(10, ang(FlA, B:angles():yaw() + ((A-D)*3), 0))
            }
        }
    }else
    {
        if(changed(Plg))
        {
            timer("Sec3", 1000)
            
            P:printDriver(4, "Starting in 3...")
            
            holoPos(10, P:toWorld(vec(0, 65, 65)))
            holoAng(10, P:toWorld(ang(0, 90, 0)))
            
            cameraCreate(1, B:pos() - Dri:eye()*100, Dri:eyeAngles())
            cameraToggle(1, 1, P)
            
            FlV = 10
            
            MVl = CCl == 5 ? 12.5 : 7.5
        }
        if(clk("Sec3"))
        {
            stoptimer("Sec3")
            timer("Sec2", 1000)
            P:printDriver(4, "Starting in 2...")
        }
        if(clk("Sec2"))
        {
            stoptimer("Sec2")
            timer("Sec1", 1000)
            P:printDriver(4, "Starting in 1...")
        }
        if(clk("Sec1"))
        {
            stoptimer("Sec3")
            soundPlay("gns",2,"training/bell_impact.wav")
            soundVolume("gns",0.3)
            PlA = 1
        }
    }
    if(!Act)
    {
        reset()
    }
}else
{
    holoPos(10,P:toWorld(vec(0, 65, 20 + cos(N/7) * 5)))
    holoAng(10,P:toWorld(ang(0, 90 + (N/15), 0)):setPitch(0):setRoll(0))
    
    if(Act)
    {
        Plg = R
        if(changed(Act))
        {
            soundPlay("hint",0.5,"UI/beepclear.wav")
            P:printColorDriver(vec(0,255,0),"Welcome to Flappy Bird!")
            P:printColorDriver(vec(0,255,0),"This is best played on a 66 tick server.")
            P:printColorDriver(vec(0,255,0),"Space or Left Click: Jump")
            P:printColorDriver(vec(0,255,0),"A and D: Turn left and right")
            P:printColorDriver(vec(0,255,255),"Press R to start!")
            P:printDriver(4,"PRESS R TO START")
        }
    }
}

if(duped() | dupefinished())
{
    reset()
}
