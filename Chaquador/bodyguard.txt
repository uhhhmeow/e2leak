@name BodyGuard
@persist Diff Rdy Walk Jump Lock
@persist [T E]:entity
@persist [V O P]:vector
@persist [A]:angle
@persist Ra:array
@model models/props_borealis/bluebarrel001.mdl
if(first())
{
    runOnTick(1)
    
    Diff = 52
    holoCreate(1)
    holoModel(1,"models/bots/soldier/bot_soldier.mdl")
    holoPos(1,entity():toWorld(vec(0,0,-Diff)))
    holoParent(1,entity())
    holoAnim(1,"taunt02",0,2)
    holoSkin(1,round(random(0,1)))
    soundPlay("start",5,"vo/soldier_headleft03.wav")
    timer("ready",10+holoAnimLength(1)*500)
    
    entity():setColor(0,0,0,0)
    entity():propFreeze(0)
    E = entity()
    
    function entity:turnTo(Tarang:angle)
    {
        V = This:toLocal(rotationVector(quat(Tarang)/quat(This))+This:pos())
        This:applyTorque((300*V - 30*This:angVelVector())*This:inertia()/3)
    }
    function entity:goto(Vector:vector)
    {
        This:applyForce(((Vector-This:pos())*25-This:vel())*This:mass())   
    }
    
    function fireRocket(TarPos:vector)
    {
        if(!Lock)
        {
            soundPlay("fire",1,"vo/soldier_incoming01.wav")
            soundPlay("fire1",1,"weapons/rocket_blackbox_shoot.wav")
            Rocket = propSpawn("models/props_phx/amraam.mdl",holoEntity(2):toWorld(vec(50,0,0)),(TarPos-E:pos()):toAngle(),1)
            Rocket:setTrails(10,10,3,"trails/smoke",vec(255),255)
            Rocket:setColor(0,0,0,0)
            holoCreate(Rocket:id())
            holoModel(Rocket:id(),"models/weapons/w_models/w_rocket.mdl")
            holoPos(Rocket:id(),Rocket:pos())
            holoAng(Rocket:id(),Rocket:angles())
            holoParent(Rocket:id(),Rocket)
            Ra:pushEntity(Rocket)
            Lock = 1
            timer("unlock",1050)
        }
    }
}

if(clk("unlock"))
{
    stoptimer("unlock")
    Lock = 0
}

if(clk("ready"))
{
    stoptimer("ready")
    holoAnim(1,"Stand_PRIMARY")
    holoCreate(2)
    holoModel(2,"models/weapons/w_models/w_rocketlauncher.mdl")
    holoPos(2,holoEntity(1):attachmentPos("weapon_bone"))
    holoPos(2,holoEntity(2):toWorld(vec(2,0,2)))
    holoAng(2,holoEntity(1):attachmentAng("weapon_bone")-ang(0,90,90))
    holoParentAttachment(2,holoEntity(1),"weapon_bone")
    timer("ready1",500)
}
if(clk("ready1"))
{
    stoptimer("ready1")
    timer("chk",250)
    Rdy = 1
}

R = rangerOffset(Diff+5,E:pos(),vec(0,0,-1))
if(R:hit()&!Jump)
{
    V = (R:pos()+vec(0,0,Diff))+O
    E:goto(V)
    E:turnTo(A:setPitch(0):setRoll(0))
}
rangerFilter(owner())
JR = rangerOffset(100,E:toWorld(vec(0,0,Diff-75)),E:forward():setZ(0))
Jump = changed(JR:hit())&JR:hit()
if(Jump)
{
    applyForce(E:up()*14000)
}

if(Rdy)
{
    P = owner():pos()
    Walk = P:distance(E:pos())>200
    
    if(Walk)
    {
        A = (P-E:pos()):toAngle()
    }else
    {
        if(!T:isValid())
        {
            A = (owner():aimPos()-E:pos()):toAngle()
        }
    }
    O = entity():forward()*Walk*10
    if(!Walk)
    {
        holoSetPose(1,"body_pitch",-A:pitch()) 
    }
    
    if(changed(Walk))
    {
        if(Walk)
        {
            holoAnim(1,"Run_PRIMARY")
        }else
        {
            holoAnim(1,"Stand_PRIMARY")
        }
    }
    holoSetPose(1,"move_scale",entity():vel():length()/250)
    
    if(clk("chk"))
    {
        stoptimer("chk")
        timer("chk",1000)
        T = noentity()
        for(I=1,players():count())
        {
            TP = players()[I,entity]
            if(TP:aimEntity()==owner())
            {
                T = TP
                break
            }
        }
    }
    
    if(T:isValid()&!Walk)
    {
        A = (T:pos()+vec(0,0,45)-E:pos()):toAngle()
        if(changed(T:keyAttack1())&T:keyAttack1())
        {
            fireRocket(T:attachmentPos("eyes")-vec(0,0,45))
        }
    }else
    {
        if(changed(owner():keyReload())&owner():keyReload()&!Walk)
        {
            fireRocket(owner():aimPos())
        }
    }
}

if(Ra:count()>0)
{
    for(I=1,Ra:count())
    {
        TP = Ra[I,entity]
        TP:setPos(TP:toWorld(vec(50,0,0)))
        RR = rangerOffset(225,TP:pos(),TP:forward())
        findInSphere(TP:pos(),300)
        findSortByDistance(TP:pos())
        if(RR:hit()||find():type()==owner():type())
        {
            Ra:remove(I)
            TP:propBreak()
        }
    }
}

#print(_HUD_PRINTCENTER,ops():toString())
