@name Chaqua PLayer
@persist [Holos Clips]:table HolosSpawned HolosStep LastHolo TotalHolos
@persist E:entity
#####


if (first() | duped())
{
    E = entity()

    function number addHolo(Pos:vector, Scale:vector, Colour:vector4, Angles:angle, Model:string, Material:string, ParentNo:number)
    {        
        local Parent = (ParentNo > 0) ? holoEntity(ParentNo) : E

        holoCreate(LastHolo, Parent:toWorld(Pos), Scale, Parent:toWorld(Angles))
        holoModel(LastHolo, Model)
        holoMaterial(LastHolo, Material)
        holoColor(LastHolo, vec(Colour), Colour:w())
        holoParent(LastHolo, Parent)

        local Key = LastHolo + "_"
        local I=1
        while (Clips:exists(Key + I))
        {
            holoClipEnabled(LastHolo, 1)
            local ClipArr = Clips[Key+I, array]
            local ThisEnt = holoEntity(LastHolo)
            holoClip(LastHolo, I, ClipArr[1, vector], ClipArr[2, vector], 0)
            I++
        }
        
        return LastHolo
    }

    ##########
    # HOLOGRAMS
    

    #[   ]#    Holos[1, array] = array(vec(32.0000, 0.0000, 0.0000), vec(0.6000, 0.6000, 0.6000), vec4(255, 255, 255, 255), ang(90.0000, -0.0000, 0.0000), "hq_torus", "phoenix_storms/fender_chrome", 0)
    #[   ]#    Holos[2, array] = array(vec(0.7210, 0.0000, 0.0000), vec(0.6000, 0.6000, 0.3470), vec4(255, 255, 255, 255), ang(90.0000, -0.0000, 0.0000), "hq_dome", "models/props_combine/metal_combinebridge001", 0)
    #[   ]#    Holos[3, array] = array(vec(5.5932, 0.0000, 5.3222), vec(0.0610, 0.0610, 1.0000), vec4(255, 255, 255, 255), ang(-90.0000, -0.0000, 0.0000), "hq_cylinder", "phoenix_storms/fender_chrome", 0)
    #[   ]#    Holos[4, array] = array(vec(-4.4867, 0.0000, 4.5214), vec(0.0610, 0.0610, 0.7095), vec4(255, 255, 255, 255), ang(79.0840, 0.0000, 0.0000), "hq_cylinder", "phoenix_storms/fender_chrome", 0)
    #[   ]#    Holos[5, array] = array(vec(-14.2969, 0.0000, 6.4829), vec(0.3099, 0.3099, 0.3099), vec4(255, 255, 255, 255), ang(-45.0000, 0.0000, 0.0000), "hq_cylinder", "models/alyx/emptool_glow", 0)
    #[   ]#    Holos[6, array] = array(vec(17.7340, 0.0000, 0.0000), vec(0.4500, 0.4500, 2.6000), vec4(255, 255, 255, 255), ang(90.0000, -0.0000, 0.0000), "hq_cylinder", "models/props_combine/metal_combinebridge001", 0)
    #[   ]#    Holos[7, array] = array(vec(28.0000, 0.0000, 0.0000), vec(0.7500, 0.7500, 0.7500), vec4(255, 255, 255, 255), ang(90.0000, -0.0000, 0.0000), "hq_torus", "phoenix_storms/fender_chrome", 0)
    #[   ]#    Holos[8, array] = array(vec(-7.6779, 0.0000, 0.0000), vec(0.7570, 0.7570, 1.4180), vec4(255, 255, 255, 255), ang(90.0000, -0.0000, 0.0000), "hexagon", "models/props_combine/metal_combinebridge001", 0)
    #[   ]#    Holos[9, array] = array(vec(19.3017, -0.0000, 3.8349), vec(0.0610, 0.0610, 1.3200), vec4(255, 255, 255, 255), ang(-79.0836, 0.0000, -0.0000), "hq_cylinder", "phoenix_storms/fender_chrome", 0)
    #[   ]#    Holos[10, array] = array(vec(-11.6028, 0.0000, 3.7889), vec(0.3284, 0.3284, 0.3284), vec4(255, 255, 255, 255), ang(-45.0000, 0.0000, 0.0000), "hq_cubinder", "models/props_combine/metal_combinebridge001", 0)
    #[   ]#    Holos[11, array] = array(vec(-15.7150, 0.0000, 7.9010), vec(0.3280, 0.3280, 0.0276), vec4(255, 255, 255, 255), ang(-45.0000, 0.0000, 0.0000), "hq_cylinder", "models/props_combine/metal_combinebridge001", 0)
    
    ##########
    
    TotalHolos = Holos:count()
    if (0 > holoClipsAvailable()) {error("A holo has too many clips to spawn on this server! (Max per holo is " + holoClipsAvailable() + ")")}
}


#You may place code here if it doesn't require all of the holograms to be spawned.


if (HolosSpawned)
{
    #Your code goes here if it needs all of the holograms to be spawned!
}
else
{
    while (LastHolo <= Holos:count() & holoCanCreate() & perf())
    {
        local Ar = Holos[LastHolo, array]
        addHolo(Ar[1, vector], Ar[2, vector], Ar[3, vector4], Ar[4, angle], Ar[5, string], Ar[6, string], Ar[7, number])
        LastHolo++
    }
    
    if (LastHolo > Holos:count())
    {
        Holos:clear()
        Clips:clear()
        HolosSpawned = 1
        E:setAlpha(0)
    }
}
