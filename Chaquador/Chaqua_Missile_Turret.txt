@name Chaqua Missile Turret
@persist VM RN PA
@persist HR:ranger
@persist AP:vector
@persist [E B P M]:entity
@persist [WL WP]:array
@autoupdate "Chaqua Missile Turret.txt"
if(duped()|dupefinished())
{
    reset()
}

if(first())
{
    runOnTick(1)
    runOnChat(1)
    
    E = entity()
    B = E:isConstrainedTo()
    
    VM = B:isVehicle()
    soundPlay("vm", 1, VM ? "hl1/fvox/bell.wav" : "hl1/fvox/beep.wav")
    
    holoCreate(1, E:toWorld(vec()), vec(0.75))
    holoModel(1, "models/props_combine/combine_mortar01b.mdl")
    holoMaterial(1, "models/shiny")
    holoColor(1, vec(40, 54, 24))
    holoParent(1, E)
    
    holoCreate(2, E:toWorld(vec(0, 0, 41.25)), vec(0.75))
    holoModel(2, "hq_sphere")
    holoMaterial(2, "models/shiny")
    holoColor(2, vec(40, 54, 24))
    holoParent(2, E)
    
    holoCreate(3, holoEntity(2):toWorld(vec(2, 0, 0)), vec(0.75), E:toWorld(ang(-90, 0, 0)))
    holoModel(3, "models/Items/combine_rifle_ammo01.mdl")
    holoMaterial(3, "models/shiny")
    holoColor(3, vec(25))
    holoParent(3, 2)
    
    holoCreate(4, holoEntity(2):toWorld(vec(18, 0, 0)), vec(0.75, 0.75, 3), E:toWorld(ang(90, 0, 0)))
    holoModel(4, "hq_tube_thin")
    holoMaterial(4, "models/shiny")
    holoColor(4, vec(40, 54, 24))
    holoParent(4, 2)
    
    if(VM)
    {
        holoCreate(5)
        holoModel(5, "icosphere3")
        holoColor(5, vec4(255, 0, 255, 150))
    }
    
    rangerFilter(B)
    rangerPersist(1)
    
    WL = array(
    "STEAM_0:0:32759932", 
    "STEAM_0:0:33397564", 
    "STEAM_0:0:33994222", 
    "STEAM_0:0:38933156", 
    "STEAM_0:1:22885263", 
    "STEAM_0:1:43508393", 
    "STEAM_0:1:63782001", 
    "STEAM_0:0:75800655", 
    "STEAM_0:1:19680709",
    "STEAM_0:0:56021974"
    )
    
    function update()
    {
        WP:clear()
        foreach(I, Ply:entity = players())
        {
            foreach(J, Str:string = WL)
            {
                if(Ply:steamID() == Str)
                {
                    WP:pushEntity(Ply)
                    hint("Updated whitelist. Check whitelist to see who is there.", 4)
                    print(2, format("Addding %s to array.", Ply:name()))
                    break
                }
            }
        }
    }
    
    function entity:playerInfo()
    {
        if(This:isPlayer())
        {
            print(format("%s: %s", This:name(), This:steamID()))
        }
    }
}

if(chatClk(owner()))
{
    local LS = lastSaid():lower():explode(" ")
    if(LS[1, string][1]=="-")
    {
        hideChat(1)
        switch(LS[1, string])
        {
            case "-g", 
                local TE = findPlayerByName(LS[2, string])
                if(TE:isValid())
                {
                    TE:playerInfo()
                }
                else
                {
                    print("Player not found.")
                }
                break
                
            case "-a", 
                local TE = findPlayerByName(LS[2, string])
                if(TE:isValid())
                {
                    WL:pushString(TE:steamID())
                    print("Player found. Updating the list...")
                    
                    TE:playerInfo()
                    update()
                }
                else
                {
                    print("Player not found.")
                }
                break
            
            case "-r", 
                reset()
                break
        }
    }
}

if(changed(numPlayers()))
{
    update()
}

RN -= 0.125
RN = max(RN, 3)
if(changed(RN))
{
    holoColor(3, RN <= 5 ? vec(0, 100, 0) : vec(100, 0, 0))
}

holoAng(3, holoEntity(3):toWorld(ang(0, RN, 0)))

####

if(VM)
{
    P = B:driver()
    if(changed(P))
    {
        PA = P:team() == owner():team()
        
        if(!PA)
        {
            B:ejectPod()
        }
    }
    
    if(P:isValid())
    {
        HR = rangerOffset(1000000, P:shootPos() + P:eye() * 150, P:eye())
        
        AP = HR:pos()
        holoPos(5, AP)
    }
}else
{
    if(findCanQuery())
    {
        findByClass("player")
        findIncludeEntities(WP)
        findSortByDistance(E:pos())
        P = find()
        
        PA = P:pos():distance(E:pos())<100
        
        AP = P:aimPos()
    }
}

####

if(changed(PA))
{
    soundPlay("pa", 1, PA ? "buttons/button18.wav" : "buttons/button19.wav")
    
    holoColor(2, PA ? vec(0, 100, 0) : vec(100, 0, 0))
}
if(PA)
{
    holoAng(2, (AP-holoEntity(2):pos()):toAngle())
}

####

if(M:isValid())
{
    rangerFilter(M)
    local R = rangerOffset(175, M:pos(), M:forward())
    if(R:hit() | P:keyAttack2())
    {
        M:propBreak()
    }
    else
    {
        M:setPos(M:toWorld(vec(VM ? 100 : 150, 0, 0)))
        
        if(VM)
        {
            local R = 0.125
            M:setAng(M:toWorld(ang(random(-R, R),random(-R, R),random(-R, R))))
        }
    }
}else
{
    if(PA & P:keyAttack1() & RN<=3 & P:aimEntity()!=E & P:isAlive())
    {
        M = propSpawn("models/props_phx/oildrum001_explosive.mdl", holoEntity(2):toWorld(vec(300, 0, 0)), (AP-holoEntity(2):pos()):toAngle(), 1)
        M:setColor(0, 0, 0, 0)
        M:setTrails(10, 100, 3, "trails/smoke", vec(200), 255)
        M:soundPlay("rckt", 0, "weapons/rpg/rocket1.wav")
        
        soundPlay("sh", 1.792, "weapons/sentry_rocket.wav")
        
        RN = 20
    }
}
