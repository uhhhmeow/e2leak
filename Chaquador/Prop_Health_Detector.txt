@name Prop Health Detector
if(first())
{
    runOnTick(1)
    holoCreate(1,entity():toWorld(vec(0,0,3)))
    holoModel(1,"hq_torus_thin")
    holoScale(1,vec(200/6,200/6,25))
    holoParent(1,entity())
}

if(findCanQuery())
{
    findByClass("prop_physics")
    findSortByDistance(entity():pos())
    InR = find():pos():distance(entity():pos())<200
    if(changed(InR))
    {
        holoColor(1,vec(!(find():health()>0)*255,(find():health()>0)*255,0))
    }elseif(!InR)
    {
        holoColor(1,vec(255,100,0))
    }
}
