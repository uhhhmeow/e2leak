@name Spawn Point
@persist [E Ply1 Ply2 S1 S2]:entity
if(first())
{
    runOnTick(1)
    
    E = entity()
    
    Ply1 = owner()
    Ply2 = findPlayerByName("bjcooper2004")
}

if(S1:isValid())
{
    S1:setAng(ang())
    if(S1:driver():isValid())
    {
        if(S1:driver()==Ply1)
        {
            S1:setPos(E:pos()+vec(0,0,35))
        }else
        {
            S1:killPod()
        }
    }else
    {
        if(Ply1:pos():distance(E:pos())>300)
        {
            S1:setPos(Ply1:pos()+vec(0,0,100))
        }else
        {
            S1:setPos(E:pos()+vec(0,0,35))
        }
    }
    
    
    if(S2:isValid())
    {
        S2:setAng(ang())
        if(S2:driver():isValid()&S2:driver()==Ply2)
        {
            if(S2:driver()==Ply2)
            {
                S2:setPos(E:pos()+vec(0,0,35))
            }else
            {
                S2:killPod()
            }
        }else
        {
            if(Ply2:pos():distance(E:pos())>500)
            {
                S2:setPos(Ply2:pos()+vec(0,0,100))
            }else
            {
                S2:setPos(E:pos()+vec(0,0,35))
            }
        }
    }else
    {
        if(findCanQuery())
        {
            findByClass("prop_vehicle_prisoner_pod")
            findSortByDistance(E:pos())
            if(find():model()=="models/nova/airboat_seat.mdl"&&find():pos():distance(E:pos())<150&&S1!=find())
            {
                S2 = find()
                S2:setColor(0,0,0,0)
                S2:propFreeze(1)
            }
        }
    }
}else
{
    if(findCanQuery())
    {
        findByClass("prop_vehicle_prisoner_pod")
        findSortByDistance(E:pos())
        if(find():model()=="models/nova/airboat_seat.mdl"&&find():pos():distance(E:pos())<150)
        {
            S1 = find()
            S1:setColor(0,0,0,0)
            S1:propFreeze(1)
        }
    }
}

