@name Spooke Scary Companion
@persist [E O B]:entity [AA AV]:angle SM [Anim]:string
@model models/props_c17/gravestone002a.mdl
interval(300)
if(first())
{
    runOnChat(1)
    
    E = entity()
    O = owner()
    
    E:propGravity(0)
    E:propFreeze(0)
    E:setColor(0,0,0,0)
    E:propNotSolid(1)
    
    SM = randint(1,6) == 1
    Anim = SM ? "ACT_GMOD_TAUNT_MUSCLE" : "ACT_GMOD_TAUNT_DANCE"
    
    B = holoCreate(1,E:toWorld(vec(0,0,-40)))
    holoModel(1,"models/player/skeleton.mdl")
    holoParent(1,E)
    holoAnim(1,Anim)
    timer("rpt",holoAnimLength(1)*1000)
    if(SM)
    {
        holoColor(1,vec(255,180,180))
    }
}

#Timers

if(clk("rpt"))
{
    stoptimer("rpt")
    holoAnim(1,Anim)
    timer("rpt",holoAnimLength(1)*1000)
}

if(clk("snd") | (changed(E:pos():isInWorld()) & E:pos():isInWorld()))
{
    stoptimer("snd")
    timer("snd",63390)
    
    soundPlay(1,63.39,"chaq/Spoopy Scary Skeletons.mp3")
    if(SM)
    {
        soundPitch(1,90)
    }
}

#Forces

#E:angTo(AA)
local R = 62.5
applyAngForce((ang(random(-R,R),random(-R,R),random(-R,R))-E:angVel()/3)*E:mass())
applyForce(((O:shootPos()+((O:shootPos()-E:pos()):normalized()*-25)-E:pos())/3)*E:mass())
applyForce(randvec(-12.5,12.5)*E:mass())

#Passing Ownership
if(O:keyUse())
{
    if(findCanQuery())
    {
        findByClass("player")
        findSortByDistance(E:pos())
        local TE = findResult(2)
        if(TE:isValid() & TE != O)
        {
            O = TE
            soundPlay("bip",0.081,"buttons/button17.wav")
            soundPitch("bip",200)
        }
    }else
    {
        print("okay")
    }
}

#Chat Selection
if(chatClk(owner()))
{
    local LS = lastSaid():lower():explode(" ")
    if(LS[1,string]=="-t" | LS[1,string] == "spook")
    {
        hideChat(1)
        local TE = findPlayerByName(LS[2,string])
        if(TE:isValid())
        {
            O = TE
        }else
        {
            hint("Player not found.",3)
        }
    }
}
