@name Shield E2
@persist [Prop T]:entity
@persist Radius N Alw
@persist [R]:array
@model models/props_c17/pottery02a.mdl
if(first())
{
    runOnTick(1)
    runOnChat(1)
    Radius = 750
    holoCreate(1)
    holoModel(1,"hq_sphere")
    holoColor(1,vec(0,255,0))
    holoScale(1,vec(1.5))
    holoEntity(1):setTrails(200,0,10,"trails/laser",vec(0,255,0),255)
    holoDisableShading(1,1)
    holoCreate(2)
    holoModel(2,"hq_sphere")
    holoColor(2,vec(0,255,0))
    holoScale(2,vec(1.5))
    holoEntity(2):setTrails(200,0,10,"trails/laser",vec(0,255,0),255)
    holoDisableShading(2,1)
    timer("check",15)
    
    R = array("STEAM_0:1:39390072","STEAM_0:0:14689799","STEAM_0:0:31038565","STEAM_0:1:19680709","STEAM_0:0:32759932","STEAM_0:1:41580573","STEAM_0:0:22662178")
    
    function entity:goto(Vector:vector)
    {
        This:applyForce(((Vector-This:pos())*25-This:vel())*This:mass())   
    }
}

N+=3*Radius/2000
holoPos(1,entity():toWorld(vec(cos(N)*Radius,sin(N)*Radius,30)))
holoPos(2,entity():toWorld(vec(0,cos(N)*Radius,30+sin(N)*Radius)))


if(clk("check"))
{
    stoptimer("check")
    timer("check",1500)
    
    findByClass("player")
    findSortByDistance(entity():pos())
    for(I=1,findCount())
    {
        if(findToArray()[I,entity]:pos():distance(entity():pos())<Radius)
        {
            for(F=1,R:count())
            {
                if(R[F,string]==findToArray()[I,entity]:steamID())
                {
                    Alw = 1
                    break
                }else
                {
                    Alw = 0
                }
            }
            if(!Alw&findToArray()[I,entity]:isAlive()&findToArray()[I,entity]:getMaterial()!="models/props_combine/com_shield001a")
            {
                T = propSpawn("models/props_phx/ww2bomb.mdl",findToArray()[I,entity]:pos()+vec(0,0,30),1)
                T:propBreak()
            }
        }
    }
}

if(chatClk(owner()))
{
    LS = owner():lastSaid():lower():explode(" ")
    if(LS[1,string]=="/add")
    {
        hideChat(1)
        if(findPlayerByName(LS[2,string]):isValid())
        {
            TID = findPlayerByName(LS[2,string]):steamID()
            for(I=1,R:count())
            {
                if(TID==R[I,string])
                {
                    V = 0
                    break
                }else
                {
                    V = 1
                }
            }
            if(V)
            {
                hint("Added "+findPlayerByName(LS[2,string]):name()+" to the whitelist",5)
                R:pushString(findPlayerByName(LS[2,string]):steamID())
            }else
            {
                hint(findPlayerByName(LS[2,string]):name()+" is already in the whitelist",5)
            }
        }else
        {
            hint("Player not found",3)
        }
    }
    if(LS[1,string]=="/remove")
    {
        hideChat(1)
        if(findPlayerByName(LS[2,string]):isValid())
        {
            TID = findPlayerByName(LS[2,string]):steamID()
            for(I=1,R:count())
            {
                if(TID==R[I,string])
                {
                    R:remove(I)
                    hint("Removed "+findPlayerByName(LS[2,string]):name()+" from the whitelist",5)
                    break
                }
            }
        }else
        {
            hint("Player not found",3)
        }
    }
}
